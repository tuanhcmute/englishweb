# FROM maven:3.8.3-openjdk-17 as BUILD
# COPY server/src /server/src
# COPY server/pom.xml /server
# RUN mvn -f /server/pom.xml -B -DskipTests clean install

# FROM openjdk:17-jdk
# WORKDIR /app
# ARG JAR_FILE_NAME=english_web-0.0.1-SNAPSHOT.jar
# COPY --from=BUILD /server/target/${JAR_FILE_NAME} /app/app.jar
# EXPOSE 8080
# CMD ["java", "-jar", "/app/app.jar"]

# Use an official Maven image as the build environment
FROM maven:3.8.3-openjdk-17 AS build

# Set the working directory
WORKDIR /build

# Copy the Maven project descriptor files
COPY server/pom.xml .

# Download the project dependencies
RUN mvn dependency:go-offline -B

# Copy the project source code
COPY server/src ./src

# Build the application
RUN mvn clean install -DskipTests -B

# Use an official OpenJDK JRE image as the runtime environment
FROM openjdk:17-jdk

# Set the working directory
WORKDIR /app

# Argument to specify the JAR file name
ARG JAR_FILE_NAME=english_web-0.0.1-SNAPSHOT.jar

# Copy the built JAR file from the build stage
COPY --from=build /build/target/${JAR_FILE_NAME} /app/app.jar

# Expose the application port
EXPOSE 8080

# Run the application
CMD ["java", "-jar", "/app/app.jar"]
