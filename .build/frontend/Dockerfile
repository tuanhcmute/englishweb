# Use an official Node.js runtime as the build environment
FROM node:18-alpine AS build

# Set the working directory
WORKDIR /app/ui

# Copy package.json and package-lock.json to leverage Docker layer caching
COPY ui-v2/package.json ./

# Install dependencies and update package-lock.json
RUN npm install --force

# Copy the rest of the application files
COPY ui-v2/ .

# Install local dependencies
RUN npm install file:./ckeditor5

# Build the application
RUN npm run build

# Use a smaller base image for the final stage
FROM node:18-alpine

# Set the working directory
WORKDIR /app/ui

# Copy the built files from the build stage
COPY --from=build /app/ui/.next ./.next
COPY --from=build /app/ui/public ./public

# Copy only necessary configuration files
COPY ui-v2/package*.json ./

# Install only production dependencies
RUN npm i --production

# Expose the application port
EXPOSE 3000

# Command to run the application
CMD ["npm", "start"]
