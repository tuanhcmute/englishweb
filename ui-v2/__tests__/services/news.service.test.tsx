import { newsService } from "@/services";
import { ICreateNewsRequest, IUpdateNewsRequest } from "@/types";

jest.mock("@/lib");

describe("NewsServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllNewsByNewsCategory", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(newsService.getAllNewsByNewsCategory("")).rejects.toThrow();
    });
  });

  describe("getAllNews", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(newsService.getAllNews()).rejects.toThrow();
    });
  });

  describe("getAllTopNews", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(newsService.getAllTopNews(4, 0)).rejects.toThrow();
    });
  });

  describe("getFullNewsById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(newsService.getFullNewsById("")).rejects.toThrow();
    });
  });

  describe("createNews", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateNewsRequest = {
        newsCategoryId: "",
        newsDescription: "",
        newsImage: "",
        newsTitle: "",
        status: "",
        userCredentialId: "",
      };

      await expect(newsService.createNews(mockRequest)).rejects.toThrow();
    });
  });

  describe("updateNews", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateNewsRequest = {
        id: "",
        newsContentHtml: "",
        newsDescription: "",
        newsImage: "",
        newsTitle: "",
        status: "",
      };

      await expect(newsService.updateNews(mockRequest)).rejects.toThrow();
    });
  });
});
