import { flashcardService } from "@/services";
import { ICreateFlashcardRequest, IUpdateFlashcardRequest } from "@/types";

jest.mock("@/lib");

describe("FlashcardServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/flashcard";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllFlashcards", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(flashcardService.getAllFlashcards()).rejects.toThrow();
    });
  });

  describe("getAllFlashcardsByUserCredential", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        flashcardService.getAllFlashcardsByUserCredential("")
      ).rejects.toThrow();
    });
  });

  describe("getFlashcardById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(flashcardService.getFlashcardById("")).rejects.toThrow();
    });
  });

  describe("createFlashcard", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateFlashcardRequest = {
        flashcardDescription: "",
        flashcardTitle: "",
        flashcardType: "",
        totalFlashcardItem: 0,
        userCredentialId: "",
      };

      await expect(
        flashcardService.createFlashcard(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateFlashcard", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateFlashcardRequest = {
        flashcardDescription: "",
        flashcardTitle: "",
        id: "",
      };

      await expect(
        flashcardService.updateFlashcard(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("deleteFlashcard", () => {
    it("should throw an error if deletion fails", async () => {
      await expect(
        flashcardService.deleteFlashcard("flashcardId")
      ).rejects.toThrow();
    });
  });
});
