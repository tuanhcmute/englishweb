import { courseCategoryService } from "@/services";
import { ICreateCourseCategoryRequest } from "@/types";

jest.mock("@/lib");

describe("CourseCategoryServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/course-category";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllCourseCategories", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        courseCategoryService.getAllCourseCategories()
      ).rejects.toThrow();
    });
  });

  describe("createCourseCategory", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateCourseCategoryRequest = {
        courseCategoryName: "",
      };

      await expect(
        courseCategoryService.createCourseCategory(mockRequest)
      ).rejects.toThrow();
    });
  });
});
