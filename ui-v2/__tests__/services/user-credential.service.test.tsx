import { userCerdentialService } from "@/services";
import { IUpdateRoleRequest, IUpdateUserCredentialRequest } from "@/types";

jest.mock("@/lib");

describe("UserCerdentialServiceTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getUserCredentialByUsername", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        userCerdentialService.getUserCredentialByUsername({ username: "" })
      ).rejects.toThrow();
    });
  });

  describe("getUserCredentialById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        userCerdentialService.getUserCredentialById("")
      ).rejects.toThrow();
    });
  });

  describe("getAllUserCredentials", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        userCerdentialService.getAllUserCredentials()
      ).rejects.toThrow();
    });
  });

  describe("updateUserCredential", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateUserCredentialRequest = {
        id: "",
        address: "",
        dob: "",
        fullName: "",
        gender: "",
        major: "",
        phoneNumber: "",
      };
      await expect(
        userCerdentialService.updateUserCredential(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateUserRole", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateRoleRequest = {
        roleId: "",
        userCredentialId: "",
      };
      await expect(
        userCerdentialService.updateUserRole(mockRequest)
      ).rejects.toThrow();
    });
  });
});
