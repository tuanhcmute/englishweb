import { lessonNoteService, newsCategoryService } from "@/services";
import {
  ICreateNewsCategoryRequest,
  IUpdateNewsCategoryRequest,
} from "@/types";

jest.mock("@/lib");

describe("NewsCategoryServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllNewsCategories", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        newsCategoryService.getAllNewsCategories("")
      ).rejects.toThrow();
    });
  });

  describe("createNewsCatgory", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateNewsCategoryRequest = {
        newsCategoryName: "",
      };

      await expect(
        newsCategoryService.createNewsCatgory(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateNewsCatgory", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateNewsCategoryRequest = {
        id: "",
        newsCategoryName: "",
        status: "",
      };

      await expect(
        newsCategoryService.updateNewsCatgory(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("deleteLessonNote", () => {
    it("should throw an error if deletion fails", async () => {
      await expect(lessonNoteService.deleteLessonNote("")).rejects.toThrow();
    });
  });
});
