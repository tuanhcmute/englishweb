import { flashcardItemService } from "@/services";
import {
  ICreateFlashcardItemByKeywordRequest,
  ICreateFlashcardItemRequest,
  IUpdateFlashcardItemRequest,
} from "@/types";

jest.mock("@/lib");

describe("FlashcardItemServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/flashcard-item";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getFlashcardItemById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        flashcardItemService.getFlashcardItemById("flashcardId")
      ).rejects.toThrow();
    });
  });

  describe("createFlashcardItem", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateFlashcardItemRequest = {
        examples: "",
        flashcardId: "",
        meaning: "",
        phonetic: "",
        tags: "",
        word: "",
      };

      await expect(
        flashcardItemService.createFlashcardItem(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("createFlashcardItemByKeyword", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateFlashcardItemByKeywordRequest = {
        flashcardId: "",
        keyword: "",
        limit: 0,
      };

      await expect(
        flashcardItemService.createFlashcardItemByKeyword(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateFlashcardItem", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateFlashcardItemRequest = {
        examples: "",
        flashcardItemId: "",
        meaning: "",
        phonetic: "",
        tags: "",
        word: "",
      };

      await expect(
        flashcardItemService.updateFlashcardItem(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("deleteFlashcardItem", () => {
    it("should throw an error if deletion fails", async () => {
      await expect(
        flashcardItemService.deleteFlashcardItem("")
      ).rejects.toThrow();
    });
  });
});
