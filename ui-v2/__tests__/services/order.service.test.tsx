import { newsService, orderService } from "@/services";
import { ICreateNewsRequest, IUpdateNewsRequest } from "@/types";

jest.mock("@/lib");

describe("OrderServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllOrders", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(orderService.getAllOrders()).rejects.toThrow();
    });
  });
});
