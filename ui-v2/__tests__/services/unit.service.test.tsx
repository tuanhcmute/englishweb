import { testService, unitService } from "@/services";
import { ICreateUnitRequest, IUpdateUnitRequest } from "@/types";

jest.mock("@/lib");

describe("UnitServiceTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllUnitsByCourse", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(unitService.getAllUnitsByCourse("")).rejects.toThrow();
    });
  });

  describe("createSkilledAgentVersion", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateUnitRequest = {
        title: "",
      };
      await expect(unitService.createUnit(mockRequest)).rejects.toThrow();
    });
  });

  describe("updateUnit", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: IUpdateUnitRequest = {
        id: "",
        title: "",
      };
      await expect(unitService.updateUnit(mockRequest)).rejects.toThrow();
    });
  });
});
