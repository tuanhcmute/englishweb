import { courseDetailService } from "@/services";
import { IUpdateCourseDetailRequest } from "@/types";

jest.mock("@/lib");

describe("CourseDetailServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/course-detail";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getCourseDetailByCourse", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        courseDetailService.getCourseDetailByCourse("courseId")
      ).rejects.toThrow();
    });
  });

  describe("createCourseCategory", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: IUpdateCourseDetailRequest = {
        courseGuide: "",
        courseInformation: "",
        courseIntroduction: "",
        courseTarget: "",
      };

      await expect(
        courseDetailService.updateCourseDetail(mockRequest)
      ).rejects.toThrow();
    });
  });
});
