import { lessonNoteService } from "@/services";
import { ICreateLessonNoteRequest, IUpdateLessonNoteRequest } from "@/types";

jest.mock("@/lib");

describe("LessonNoteServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllLessonNotes", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(lessonNoteService.getAllLessonNotes()).rejects.toThrow();
    });
  });

  describe("createLessonNote", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateLessonNoteRequest = {
        content: "",
        lessonId: "",
        noteTime: 0,
        userCredentialId: "",
      };

      await expect(
        lessonNoteService.createLessonNote(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateLessonNote", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateLessonNoteRequest = {
        content: "",
        id: "",
      };

      await expect(
        lessonNoteService.updateLessonNote(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("deleteLessonNote", () => {
    it("should throw an error if deletion fails", async () => {
      await expect(lessonNoteService.deleteLessonNote("")).rejects.toThrow();
    });
  });
});
