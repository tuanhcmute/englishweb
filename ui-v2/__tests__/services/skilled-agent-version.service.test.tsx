import { skilledAgentVersionService } from "@/services";
import {
  ICreateSkilledAgentVersionRequest,
  IUpdateSkilledAgentVersionRequest,
} from "@/types";

jest.mock("@/lib");

describe("SkilledAgentVersionServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getSkilledAgentVersionById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        skilledAgentVersionService.getSkilledAgentVersionById("")
      ).rejects.toThrow();
    });
  });

  describe("getAllSkilledAgentVersions", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        skilledAgentVersionService.getAllSkilledAgentVersions()
      ).rejects.toThrow();
    });
  });

  describe("createSkilledAgentVersion", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateSkilledAgentVersionRequest = {
        llm_model_ids: [],
        setup_message: "",
        skilled_agent_id: "",
        tool_ids: [],
        version: "",
      };
      await expect(
        skilledAgentVersionService.createSkilledAgentVersion(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateSkilledAgentVersion", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateSkilledAgentVersionRequest = {
        id: "",
        llm_model_ids: [],
        setup_message: "",
        skilled_agent_id: "",
        tool_ids: [],
        version: "",
      };

      await expect(
        skilledAgentVersionService.updateSkilledAgentVersion(mockRequest)
      ).rejects.toThrow();
    });
  });
});
