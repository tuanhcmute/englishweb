import { testService } from "@/services";

jest.mock("@/lib");

describe("TestServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllTests", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(testService.getAllTests()).rejects.toThrow();
    });
  });

  describe("getTestById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(testService.getTestById("")).rejects.toThrow();
    });
  });

  describe("getFullTestById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(testService.getFullTestById("")).rejects.toThrow();
    });
  });

  describe("deleteTest", () => {
    it("should throw an error if deletion fails", async () => {
      await expect(testService.deleteTest("")).rejects.toThrow();
    });
  });
});
