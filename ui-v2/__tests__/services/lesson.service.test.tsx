import { lessonService } from "@/services";
import { ICreateLessonRequest, IUpdateLessonRequest } from "@/types";

jest.mock("@/lib");

describe("LessonServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getLessonById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(lessonService.getLessonById("lessonId")).rejects.toThrow();
    });
  });

  describe("createLesson", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateLessonRequest = {
        title: "",
        unitId: "",
      };

      await expect(lessonService.createLesson(mockRequest)).rejects.toThrow();
    });
  });

  describe("updateLesson", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateLessonRequest = {
        content: "",
        id: "",
        title: "",
        video: "",
      };

      await expect(lessonService.updateLesson(mockRequest)).rejects.toThrow();
    });
  });
});
