import { partInformationService } from "@/services";
import {
  IPartInformationRequest,
  IUpdatePartInformationRequest,
} from "@/types";

jest.mock("@/lib");

describe("PartInformationServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllPartInformationByTestCategory", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        partInformationService.getAllPartInformationByTestCategory("")
      ).rejects.toThrow();
    });
  });

  describe("createPartInformation", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: IPartInformationRequest = {
        partInformationDescription: "",
        partInformationName: "",
        partSequenceNumber: 0,
        partType: "",
        testCategoryId: "",
        totalQuestion: 0,
      };

      await expect(
        partInformationService.createPartInformation(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updatePartInformation", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdatePartInformationRequest = {
        id: "",
        partInformationDescription: "",
        partInformationName: "",
        partSequenceNumber: 0,
        partType: "",
        totalQuestion: 0,
      };

      await expect(
        partInformationService.updatePartInformation(mockRequest)
      ).rejects.toThrow();
    });
  });
});
