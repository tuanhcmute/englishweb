import { questionGroupService } from "@/services";
import { IQuestionGroupRequest, IUpdateQuestionGroupRequest } from "@/types";

jest.mock("@/lib");

describe("QuestionGroupServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllQuestionGroupByPart", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        questionGroupService.getAllQuestionGroupByPart("")
      ).rejects.toThrow();
    });
  });

  describe("deleteQuestionGroup", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        questionGroupService.deleteQuestionGroup("")
      ).rejects.toThrow();
    });
  });

  describe("createNewQuestionGroup", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: IQuestionGroupRequest = {
        partOfTestId: "",
        questionContentEn: "",
        questionContentTranscript: "",
        image: "",
      };

      await expect(
        questionGroupService.createNewQuestionGroup(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateQuestionGroup", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateQuestionGroupRequest = {
        id: "",
        image: "",
        questionContentEn: "",
        questionContentTranscript: "",
      };

      await expect(
        questionGroupService.updateQuestionGroup(mockRequest)
      ).rejects.toThrow();
    });
  });
});
