import { courseService } from "@/services";
import { ICreateCourseRequest, IUpdateCourseRequest } from "@/types";

jest.mock("@/lib");

describe("CourseDetailServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/course";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllCourses", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(courseService.getAllCourses()).rejects.toThrow();
    });
  });

  describe("createCourse", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateCourseRequest = {
        courseImg: "",
        courseName: "",
        isActive: true,
        newPrice: 0,
        oldPrice: 0,
        percentDiscount: 0,
      };

      await expect(courseService.createCourse(mockRequest)).rejects.toThrow();
    });
  });

  describe("updateCourse", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateCourseRequest = {
        courseImg: "",
        courseName: "",
        isActive: true,
        newPrice: 0,
        oldPrice: 0,
        percentDiscount: 0,
        id: "",
      };

      await expect(courseService.updateCourse(mockRequest)).rejects.toThrow();
    });
  });

  describe("getCourseById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(courseService.getCourseById("courseId")).rejects.toThrow();
    });
  });
});
