import { partInformationService, partOfTestService } from "@/services";
import {
  IPartInformationRequest,
  IUpdatePartInformationRequest,
} from "@/types";

jest.mock("@/lib");

describe("PartOfTestServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllPartOfTestByTest", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        partOfTestService.getAllPartOfTestByTest("")
      ).rejects.toThrow();
    });
  });

  describe("updatePartOfTest", () => {
    it("should throw an error if updation fails", async () => {
      await expect(partOfTestService.updatePartOfTest("")).rejects.toThrow();
    });
  });
});
