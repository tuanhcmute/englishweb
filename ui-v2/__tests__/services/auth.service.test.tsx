import { authService } from "@/services";
import { IForgotPasswordRequest, IUserCredentialRequest } from "@/types";

jest.mock("@/lib");

describe("AuthServiceTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("login", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        authService.login({ username: "", password: "" })
      ).rejects.toThrow();
    });
  });

  describe("signUp", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUserCredentialRequest = {
        confirmPassword: "",
        email: "",
        fullName: "",
        password: "",
        phoneNumber: "",
        provider: "",
        username: "",
      };
      await expect(authService.signUp(mockRequest)).rejects.toThrow();
    });
  });

  describe("loginSocial", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: {
        avatar: string;
        email: string;
        name: string;
        provider: string;
        username: string;
      } = {
        avatar: "",
        email: "",
        name: "",
        provider: "",
        username: "",
      };
      await expect(authService.loginSocial(mockRequest)).rejects.toThrow();
    });
  });

  describe("refreshToken", () => {
    it("should throw an error if refresh token fails", async () => {
      await expect(authService.refreshToken("")).rejects.toThrow();
    });
  });

  describe("sendOTPForgotPassword", () => {
    it("should throw an error if send OTP fails", async () => {
      await expect(authService.sendOTPForgotPassword("")).rejects.toThrow();
    });
  });

  describe("sendLinkVerification", () => {
    it("should throw an error if send link fails", async () => {
      await expect(authService.sendLinkVerification("")).rejects.toThrow();
    });
  });

  describe("forgotPassword", () => {
    it("should throw an error if send link fails", async () => {
      const mockRequest: IForgotPasswordRequest = {
        confirmPassword: "",
        email: "",
        newPassword: "",
        otp: "",
      };

      await expect(authService.forgotPassword(mockRequest)).rejects.toThrow();
    });
  });
});
