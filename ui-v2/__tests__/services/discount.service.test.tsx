import { discountService } from "@/services";
import {
  IAssignDiscountRequest,
  ICreateDiscountRequest,
  IUpdateDiscountRequest,
} from "@/types";

jest.mock("@/lib");

describe("DiscountServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/course";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllDiscounts", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(discountService.getAllDiscounts()).rejects.toThrow();
    });
  });

  describe("createDiscount", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateDiscountRequest = {
        discountName: "",
        endDate: "",
        startDate: "",
        status: "",
        percentDiscount: 0,
      };

      await expect(
        discountService.createDiscount(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateDiscount", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateDiscountRequest = {
        discountName: "",
        endDate: "",
        id: "",
        percentDiscount: 0,
        startDate: "",
        status: "",
      };

      await expect(
        discountService.updateDiscount(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateDiscount", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IAssignDiscountRequest = {
        courseIds: [],
        discountId: "",
      };

      await expect(
        discountService.assignDiscount(mockRequest)
      ).rejects.toThrow();
    });
  });
});
