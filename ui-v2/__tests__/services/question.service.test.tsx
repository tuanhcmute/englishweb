import { questionGroupService, questionService } from "@/services";
import {
  ICreateQuestionRequest,
  IQuestionGroupRequest,
  IUpdateQuestionGroupRequest,
  IUpdateQuestionRequest,
} from "@/types";

jest.mock("@/lib");

describe("QuestionServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/lesson-note";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllQuestionGroupByPart", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        questionGroupService.getAllQuestionGroupByPart("")
      ).rejects.toThrow();
    });
  });

  describe("deleteQuestion", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(questionService.deleteQuestion("")).rejects.toThrow();
    });
  });

  describe("createNewQuestion", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateQuestionRequest = {
        questionContent: "",
        questionGroupId: "",
        questionGuide: "",
        questionNumber: 0,
        testAnswers: [],
      };

      await expect(
        questionService.createNewQuestion(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateQuestion", () => {
    it("should throw an error if updation fails", async () => {
      const mockRequest: IUpdateQuestionRequest = {
        id: "",
        questionContent: "",
        questionGuide: "",
        questionNumber: 0,
        testAnswers: [],
      };

      await expect(
        questionService.updateQuestion(mockRequest)
      ).rejects.toThrow();
    });
  });
});
