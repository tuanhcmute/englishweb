import { unitService, userAnswerService } from "@/services";
import { IUpdateUnitRequest, IUserAnswerRequest } from "@/types";

jest.mock("@/lib");

describe("UserAnswerServiceTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllUserAnswerByUserCredential", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        userAnswerService.getAllUserAnswerByUserCredential("")
      ).rejects.toThrow();
    });
  });

  describe("getAllUserAnswerById", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(
        userAnswerService.getAllUserAnswerById("")
      ).rejects.toThrow();
    });
  });

  describe("countByTest", () => {
    it("should throw an error if fetching fails", async () => {
      await expect(userAnswerService.countByTest("")).rejects.toThrow();
    });
  });

  describe("createNewUserAnswer", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: IUserAnswerRequest = {
        completionTime: 0,
        practiceType: "",
        results: "",
        testId: "",
        userCredentialId: "",
      };
      await expect(
        userAnswerService.createNewUserAnswer(mockRequest)
      ).rejects.toThrow();
    });
  });

  describe("updateUnit", () => {
    it("should throw an error if creation fails", async () => {
      const mockRequest: IUpdateUnitRequest = {
        id: "",
        title: "",
      };
      await expect(unitService.updateUnit(mockRequest)).rejects.toThrow();
    });
  });
});
