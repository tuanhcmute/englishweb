import { commentService } from "@/services";
import { ICreateCommentRequest } from "@/types";

jest.mock("@/lib");

describe("CommentServiceTest", () => {
  const baseUrl = "https://english.workon.space/server/v1.0.0/comment";

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("getAllCommentsByTest", () => {
    // it("should fetch all comments by test", async () => {
    //   const mockResponse = { data: { data: [] } };
    //   http.get.mockResolvedValueOnce(mockResponse);

    //   const result = await commentService.getAllCommentsByTest("test-id");

    //   expect(http.get).toHaveBeenCalledWith(`${baseUrl}/byTest?value=test-id`);
    //   expect(result).toEqual(mockResponse.data.data);
    // });

    it("should throw an error if fetching fails", async () => {
      await expect(commentService.getAllComments()).rejects.toThrow(
        "Failed to fetch all comments by test"
      );
    });
  });

  describe("createComment", () => {
    // it("should create a comment", async () => {
    //   const mockRequest: ICreateCommentRequest = {
    //     content: "test comment",
    //     authorId: "",
    //     parentCommentId: "",
    //     testId: "",
    //   };
    //   const mockResponse = { data: { data: {} } };
    //   http.post.mockResolvedValueOnce(mockResponse);

    //   const result = await commentService.createComment(mockRequest);

    //   expect(http.post).toHaveBeenCalledWith(baseUrl, mockRequest);
    //   expect(result).toEqual(mockResponse.data.data);
    // });

    it("should throw an error if creation fails", async () => {
      const mockRequest: ICreateCommentRequest = {
        content: "test comment",
        authorId: "",
        parentCommentId: "",
        testId: "",
      };
      await expect(commentService.createComment(mockRequest)).rejects.toThrow(
        "Failed to create comment"
      );
    });
  });

  describe("getAllChildCommentsByParentComment", () => {
    // it("should fetch all child comments by parent comment", async () => {
    //   const mockResponse = { data: { data: [] } };
    //   http.get.mockResolvedValueOnce(mockResponse);

    //   const result = await commentService.getAllChildCommentsByParentComment(
    //     "parent-id"
    //   );

    //   expect(http.get).toHaveBeenCalledWith(
    //     `${baseUrl}/byParentComment?value=parent-id`
    //   );
    //   expect(result).toEqual(mockResponse.data.data);
    // });

    it("should throw an error if fetching fails", async () => {
      await expect(
        commentService.getAllChildCommentsByParentComment("parent-id")
      ).rejects.toThrow("Failed to fetch all comments by parent comment");
    });
  });
});
