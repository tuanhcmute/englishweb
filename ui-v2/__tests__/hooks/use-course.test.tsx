import "@testing-library/jest-dom";
import { useCourse, useCourseDetail, useCourses } from "@/hooks";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";

describe("useCourseTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(() => useCourse(""), {
      wrapper,
    });
    await waitFor(() => {
      expect(result.current.data).toBe(undefined);
    });
  });
});
