import "@testing-library/jest-dom";
import { useSkilledAgents, useSkilledAgentVersions } from "@/hooks";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";

describe("useSkilledAgentsTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(() => useSkilledAgents(), {
      wrapper: wrapper,
    });
    await waitFor(() => {
      expect(result.current.data).toBe(undefined);
    });
  });
});
