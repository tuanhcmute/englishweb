import "@testing-library/jest-dom";
import { useTestSuite, useTestSuites } from "@/hooks";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";

describe("useTestSuitesTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(() => useTestSuites(), {
      wrapper: wrapper,
    });
    await waitFor(() => {
      expect(result.current.data).toBe(undefined);
    });
  });
});
