import "@testing-library/jest-dom";
import { useCreateFlashcardItemByKeyword } from "@/hooks";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateFlashcardItemByKeywordRequest } from "@/types";

describe("useCreateFlashcardItemByKeywordTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateFlashcardItemByKeyword(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateFlashcardItemByKeywordRequest = {
        flashcardId: "",
        keyword: "",
        limit: 0,
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
