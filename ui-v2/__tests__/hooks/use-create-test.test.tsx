import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ITestRequest } from "@/types";
import { useCreateTest } from "@/hooks";

describe("useCreateTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateTest(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ITestRequest = {
        testName: "Physics 101 Final Exam",
        testDescription:
          "Comprehensive final exam covering all topics discussed in Physics 101.",
        numberOfPart: 4,
        numberOfQuestion: 50,
        testCategoryId: "cat123",
        thumbnail: new File([""], "thumbnail.png", { type: "image/png" }),
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
