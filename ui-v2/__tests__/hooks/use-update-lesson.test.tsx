import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateFlashcardRequest, IUpdateLessonRequest } from "@/types";
import { useUpdateLesson } from "@/hooks";

describe("useUpdateLessonTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateLesson(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateLessonRequest = {
        content: "",
        id: "",
        title: "",
        video: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
