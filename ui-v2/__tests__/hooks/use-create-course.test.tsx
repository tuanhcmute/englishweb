import "@testing-library/jest-dom";
import { useCreateCourse } from "@/hooks";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateCourseRequest } from "@/types";

describe("useCreateCourseTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateCourse(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateCourseRequest = {
        courseImg: "",
        courseName: "",
        isActive: true,
        newPrice: 0,
        oldPrice: 0,
        percentDiscount: 0,
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
