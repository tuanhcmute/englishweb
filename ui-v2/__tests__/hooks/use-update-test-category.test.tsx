import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import {
  IUpdateSkilledAgentVersionRequest,
  IUpdateTestCategoryRequest,
} from "@/types";
import { useUpdateSkilledAgentVersion, useUpdateTestCategory } from "@/hooks";

describe("useUpdateTestCategoryTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateTestCategory(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateTestCategoryRequest = {
        id: "",
        numberOfPart: 0,
        testCategoryName: "",
        totalQuestion: 0,
        totalTime: 0,
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
