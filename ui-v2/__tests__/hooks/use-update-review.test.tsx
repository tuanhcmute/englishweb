import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateReviewRequest } from "@/types";
import { useUpdateReview } from "@/hooks";

describe("useUpdateReviewTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateReview(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateReviewRequest = {
        id: "",
        content: "",
        isPublic: true,
        rating: 4,
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
