import "@testing-library/jest-dom";
import { useCreateDiscount } from "@/hooks";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateDiscountRequest } from "@/types";

describe("useCreateDiscountTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateDiscount(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateDiscountRequest = {
        discountName: "",
        percentDiscount: 0,
        endDate: "",
        startDate: "",
        status: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
