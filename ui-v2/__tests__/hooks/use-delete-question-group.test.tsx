import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { useDeleteQuestionGroup } from "@/hooks";

describe("useDeleteQuestionGroupTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when deletion fails", async () => {
    const { result } = renderHook(() => useDeleteQuestionGroup(), {
      wrapper,
    });
    await act(async () => {
      await expect(
        result.current.mutateAsync("questionGroupId")
      ).rejects.toThrow();
    });
  });
});
