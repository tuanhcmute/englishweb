import "@testing-library/jest-dom";
import { useCommentsByParentComment } from "@/hooks";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";

describe("useCommentsByParentCommentTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(
      () => useCommentsByParentComment("commentId"),
      {
        wrapper: wrapper,
      }
    );
    await waitFor(() => {
      expect(result.current.data).toBe(undefined);
    });
  });
});
