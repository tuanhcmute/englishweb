import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateCourseRequest } from "@/types";
import { useUpdateCourse, useUpdateFlashcardItemImage } from "@/hooks";

describe("useUpdateCourseTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateFlashcardItemImage(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: { id: string; file: File } = {
        file: new File([], "test"),
        id: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
