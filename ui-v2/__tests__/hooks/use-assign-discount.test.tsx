import "@testing-library/jest-dom";
import { useAssignDiscount } from "@/hooks";
import { IAssignDiscountRequest } from "@/types";
import { renderHook } from "@testing-library/react";
import { act } from "react";
import { wrapper } from "../../__mocks__/utils/wapper";

describe("useAssignDiscountTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should call error toast on error", async () => {
    const { result } = renderHook(useAssignDiscount, {
      wrapper,
    });

    await act(async () => {
      const mockRequest: IAssignDiscountRequest = {
        courseIds: [],
        discountId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
