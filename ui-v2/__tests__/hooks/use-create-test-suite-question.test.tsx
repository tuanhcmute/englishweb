import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateTestSuiteQuestionRequest } from "@/types";
import { useCreateTestSuiteQuestion } from "@/hooks";

describe("useCreateTestSuiteQuestionTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateTestSuiteQuestion(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateTestSuiteQuestionRequest = {
        expected_answer: "",
        question_name: "",
        test_suite_id: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
