import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { useDeleteTest } from "@/hooks";

describe("useDeleteTestTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when deletion fails", async () => {
    const { result } = renderHook(() => useDeleteTest(), {
      wrapper,
    });
    await act(async () => {
      await expect(result.current.mutateAsync("testId")).rejects.toThrow();
    });
  });
});
