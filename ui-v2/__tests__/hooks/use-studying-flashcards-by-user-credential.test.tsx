import "@testing-library/jest-dom";
import { useStudyingFlashcardsByUserCredential } from "@/hooks";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";

describe("useStudyingFlashcardsByUserCredentialTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(
      () => useStudyingFlashcardsByUserCredential(""),
      {
        wrapper: wrapper,
      }
    );
    await waitFor(() => {
      expect(result.current.data).toBe(undefined);
    });
  });
});
