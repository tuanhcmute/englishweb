import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateQuestionRequest, ICreateReviewRequest } from "@/types";
import { useCreateQuestion, useCreateReview } from "@/hooks";

describe("useCreateQuestionTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateQuestion(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateQuestionRequest = {
        questionContent: "",
        questionGroupId: "",
        questionGuide: "",
        questionNumber: 0,
        testAnswers: [],
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
