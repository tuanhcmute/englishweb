import "@testing-library/jest-dom";
import { useCreateComment } from "@/hooks";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateCommentRequest } from "@/types";

describe("useCreateCommentTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(() => useCreateComment(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateCommentRequest = {
        authorId: "",
        content: "",
        parentCommentId: "",
        testId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
