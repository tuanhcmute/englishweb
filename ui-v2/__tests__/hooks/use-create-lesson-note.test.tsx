import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateLessonNoteRequest } from "@/types";
import { useCreateLessonNote } from "@/hooks/use-create-lesson-note";

describe("useCreateFlashcardTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateLessonNote(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateLessonNoteRequest = {
        content: "",
        lessonId: "",
        noteTime: 0,
        userCredentialId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
