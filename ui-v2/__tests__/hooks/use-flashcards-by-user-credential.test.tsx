import "@testing-library/jest-dom";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { useFlashcardsByUserCredential } from "@/hooks";

describe("useFlashcardsByUserCredentialTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(() => useFlashcardsByUserCredential(""), {
      wrapper,
    });
    await waitFor(() => {
      expect(result.current.data).toBe(undefined);
    });
  });
});
