import "@testing-library/jest-dom";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import {
  useFlashcardsByUserCredential,
  useReRunSkilledAgentTest,
} from "@/hooks";
import { act } from "react";
import { ICreateSkilledAgentTestRequest } from "@/types";

describe("useReRunSkilledAgentTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when fetching fails", async () => {
    const { result } = renderHook(() => useReRunSkilledAgentTest(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateSkilledAgentTestRequest = {
        skilled_agent_version_id: "",
        test_suite_run_id: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
