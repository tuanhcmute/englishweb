import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateNewsCategoryRequest, ICreatePaymentRequest } from "@/types";
import { useCreateNewsCategory, useCreatePayment } from "@/hooks";

describe("useCreatePaymentTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreatePayment(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreatePaymentRequest = {
        courseId: "",
        paymentMethod: "",
        totalAmount: 0,
        userCredentialId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
