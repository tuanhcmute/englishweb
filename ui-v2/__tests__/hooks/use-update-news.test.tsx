import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateLessonRequest, IUpdateNewsRequest } from "@/types";
import { useUpdateNews } from "@/hooks";

describe("useUpdateNewsTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateNews(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateNewsRequest = {
        id: "",
        newsContentHtml: "",
        newsDescription: "",
        newsImage: "",
        newsTitle: "",
        status: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
