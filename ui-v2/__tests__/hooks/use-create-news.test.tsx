import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateNewsCategoryRequest, ICreateNewsRequest } from "@/types";
import { useCreateNews, useCreateNewsCategory } from "@/hooks";

describe("useCreateNewsTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateNews(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateNewsRequest = {
        newsCategoryId: "",
        newsDescription: "",
        newsImage: "",
        newsTitle: "",
        status: "",
        userCredentialId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
