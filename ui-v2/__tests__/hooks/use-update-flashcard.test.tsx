import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateCourseRequest, IUpdateFlashcardRequest } from "@/types";
import {
  useUpdateCourse,
  useUpdateFlashcard,
  useUpdateFlashcardItemImage,
} from "@/hooks";

describe("useUpdateFlashcardTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateFlashcard(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateFlashcardRequest = {
        flashcardDescription: "",
        flashcardTitle: "",
        id: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
