import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateSkilledAgentRequest, IForgotPasswordRequest } from "@/types";
import {
  useCreateSkilledAgent,
  useForgotPassword,
  useSendOTPForgotPassword,
} from "@/hooks";

describe("useForgotPasswordTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useForgotPassword(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IForgotPasswordRequest = {
        confirmPassword: "",
        email: "",
        newPassword: "",
        otp: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
