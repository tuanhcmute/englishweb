import "@testing-library/jest-dom";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { useFullTestAnswer } from "@/hooks";

describe("useFullTestAnswerTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useFullTestAnswer(""), {
      wrapper,
    });
    await waitFor(() => {
      expect(result.current.data).toBe(undefined);
    });
  });
});
