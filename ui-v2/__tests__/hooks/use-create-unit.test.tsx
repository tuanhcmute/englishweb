import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateUnitRequest } from "@/types";
import { useCreateUnit } from "@/hooks";

describe("useCreateUnitTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateUnit(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateUnitRequest = {
        title: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
