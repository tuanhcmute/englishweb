import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateSkilledAgentVersionRequest } from "@/types";
import { useCreateSkilledAgentVersion } from "@/hooks";

describe("useCreateSkilledAgentVersionTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateSkilledAgentVersion(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateSkilledAgentVersionRequest = {
        llm_model_ids: [],
        setup_message: "",
        skilled_agent_id: "",
        tool_ids: [],
        version: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
