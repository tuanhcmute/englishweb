import "@testing-library/jest-dom";
import { useRunSkilledAgentTest } from "@/hooks";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { ICreateSkilledAgentTestRequest } from "@/types";

describe("useRunSkilledAgentTestTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when running fails", async () => {
    const { result } = renderHook(() => useRunSkilledAgentTest(), {
      wrapper: wrapper,
    });
    await waitFor(async () => {
      const mockRequest: ICreateSkilledAgentTestRequest = {
        skilled_agent_version_id: "",
        test_suite_run_id: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
