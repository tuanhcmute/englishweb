import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateReviewRequest } from "@/types";
import { useCreateReview } from "@/hooks";

describe("useCreateReviewTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateReview(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateReviewRequest = {
        image: "",
        content: "",
        courseId: "",
        rating: 2,
        userCredentialId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
