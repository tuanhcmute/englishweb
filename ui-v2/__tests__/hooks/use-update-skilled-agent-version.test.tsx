import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateSkilledAgentVersionRequest } from "@/types";
import { useUpdateSkilledAgentVersion } from "@/hooks";

describe("useUpdateSkilledAgentVersionTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateSkilledAgentVersion(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateSkilledAgentVersionRequest = {
        llm_model_ids: [],
        setup_message: "",
        skilled_agent_id: "",
        id: "",
        tool_ids: [],
        version: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
