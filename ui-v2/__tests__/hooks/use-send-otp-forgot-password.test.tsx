import "@testing-library/jest-dom";
import { useRunSkilledAgentTest, useSendOTPForgotPassword } from "@/hooks";
import { renderHook, waitFor } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { ICreateSkilledAgentTestRequest } from "@/types";

describe("useSendOTPForgotPasswordTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when sending fails", async () => {
    const { result } = renderHook(() => useSendOTPForgotPassword(), {
      wrapper: wrapper,
    });
    await waitFor(async () => {
      await expect(result.current.mutateAsync("")).rejects.toThrow();
    });
  });
});
