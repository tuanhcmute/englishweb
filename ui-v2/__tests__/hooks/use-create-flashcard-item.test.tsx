import "@testing-library/jest-dom";
import {
  useCreateFlashcardItem,
  useCreateFlashcardItemByKeyword,
} from "@/hooks";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import {
  ICreateFlashcardItemByKeywordRequest,
  ICreateFlashcardItemRequest,
} from "@/types";

describe("useCreateFlashcardItemTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateFlashcardItem(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateFlashcardItemRequest = {
        examples: "",
        flashcardId: "",
        meaning: "",
        phonetic: "",
        tags: "",
        word: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
