import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateUserCredentialRequest } from "@/types";
import { useUpdateUserCredential } from "@/hooks";

describe("useUpdateUserCredentialTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateUserCredential(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateUserCredentialRequest = {
        id: "",
        address: "",
        dob: "",
        fullName: "",
        gender: "",
        major: "",
        phoneNumber: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
