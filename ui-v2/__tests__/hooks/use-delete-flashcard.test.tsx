import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { useDeleteFlashcard } from "@/hooks";

describe("useDeleteFlashcardTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when deletion fails", async () => {
    const { result } = renderHook(() => useDeleteFlashcard(), {
      wrapper,
    });
    await act(async () => {
      await expect(result.current.mutateAsync("flashcardId")).rejects.toThrow();
    });
  });
});
