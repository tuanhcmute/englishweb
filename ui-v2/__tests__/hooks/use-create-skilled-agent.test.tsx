import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import {
  ICreateSkilledAgentRequest,
  ICreateSkilledAgentVersionRequest,
} from "@/types";
import { useCreateSkilledAgent, useCreateSkilledAgentVersion } from "@/hooks";

describe("useCreateSkilledAgentTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateSkilledAgent(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateSkilledAgentRequest = {
        setup_message: "",
        agent_name: "",
        description: "",
        test_score: 0,
        test_suite_ids: [],
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
