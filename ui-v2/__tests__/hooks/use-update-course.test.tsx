import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateUnitRequest, IUpdateCourseRequest } from "@/types";
import { useCreateUnit, useUpdateCourse } from "@/hooks";

describe("useUpdateCourseTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateCourse(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateCourseRequest = {
        courseImg: "",
        courseName: "",
        id: "",
        isActive: true,
        newPrice: 0,
        oldPrice: 0,
        percentDiscount: 0,
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
