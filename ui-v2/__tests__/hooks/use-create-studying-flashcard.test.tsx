import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import {
  ICreateSkilledAgentRequest,
  ICreateStudyingFlashcardRequest,
} from "@/types";
import { useCreateSkilledAgent, useCreateStudyingFlashcard } from "@/hooks";

describe("useCreateStudyingFlashcardTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateStudyingFlashcard(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateStudyingFlashcardRequest = {
        flashcardId: "",
        userCredentialId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
