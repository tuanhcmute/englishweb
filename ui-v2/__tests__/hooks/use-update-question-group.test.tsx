import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IUpdateQuestionGroupRequest } from "@/types";
import { useUpdateQuestionGroup } from "@/hooks";

describe("useUpdateQuestionGroupTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when updation fails", async () => {
    const { result } = renderHook(() => useUpdateQuestionGroup(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IUpdateQuestionGroupRequest = {
        id: "",
        image: "",
        questionContentEn: "",
        questionContentTranscript: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
