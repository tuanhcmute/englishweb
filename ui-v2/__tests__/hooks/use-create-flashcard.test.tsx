import "@testing-library/jest-dom";
import { useCreateFlashcard, useCreateFlashcardItem } from "@/hooks";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateFlashcardItemRequest, ICreateFlashcardRequest } from "@/types";

describe("useCreateFlashcardTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateFlashcard(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateFlashcardRequest = {
        flashcardDescription: "",
        flashcardTitle: "",
        flashcardType: "",
        totalFlashcardItem: 0,
        userCredentialId: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
