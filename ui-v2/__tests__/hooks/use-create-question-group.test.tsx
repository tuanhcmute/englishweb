import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { IQuestionGroupRequest } from "@/types";
import { useCreateQuestionGroup } from "@/hooks";

describe("useCreatePaymentTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateQuestionGroup(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: IQuestionGroupRequest = {
        image: "",
        partOfTestId: "",
        questionContentEn: "",
        questionContentTranscript: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
