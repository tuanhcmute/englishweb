import "@testing-library/jest-dom";
import { renderHook } from "@testing-library/react";
import { wrapper } from "../../__mocks__/utils/wapper";
import { act } from "react";
import { ICreateLLMModelRequest } from "@/types";
import { useCreateLLMModel } from "@/hooks";

describe("useCreateLLMModelTest", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("Should return an error when creation fails", async () => {
    const { result } = renderHook(() => useCreateLLMModel(), {
      wrapper,
    });
    await act(async () => {
      const mockRequest: ICreateLLMModelRequest = {
        llm_model_name: "",
        param: "",
      };
      await expect(result.current.mutateAsync(mockRequest)).rejects.toThrow();
    });
  });
});
