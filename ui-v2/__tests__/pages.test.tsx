import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import HomePage from "@/app/page";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

// Testing
describe("HomePageTest", () => {
  it("Should render Home Page", () => {
    const queryClient = new QueryClient();
    render(
      <QueryClientProvider client={queryClient}>
        <HomePage />
      </QueryClientProvider>
    );
  });
});
