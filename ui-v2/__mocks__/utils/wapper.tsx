import React, { ReactNode } from "react";
import { QueryClientProvider } from "@/providers";

export const wrapper = ({ children }: { children: ReactNode }) => (
  <QueryClientProvider>{children}</QueryClientProvider>
);
