beforeEach(() => {
  const mockIntersectionObserver = jest.fn().mockReturnValue({
    observe: jest.fn(),
    unobserve: jest.fn(),
    disconnect: jest.fn(),
  });
  // IntersectionObserver isn't available in test environment
  window.IntersectionObserver = mockIntersectionObserver;

  // ResizeObserver is not availabel in test env
  window.ResizeObserver = window.ResizeObserver || mockIntersectionObserver;

  // Fix Arrar.map is not a function in Carousel
  Object.defineProperty(window, "matchMedia", {
    writable: true,
    value: jest.fn().mockImplementation((query) => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    })),
  });
});

// Mock useRouter:
jest.mock("next/navigation", () => ({
  // __esModule: true,
  useRouter() {
    return {
      push: jest.fn(),
      replace: jest.fn(),
      prefetch: jest.fn(),
    };
  },
  usePathname: jest.fn().mockReturnValue(""),
  useServerInsertedHTML: jest.fn(),
}));
