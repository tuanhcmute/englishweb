/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    turbo: {
      resolveExtensions: [
        ".mdx",
        ".tsx",
        ".ts",
        ".jsx",
        ".js",
        ".mjs",
        ".json",
      ],
    },
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "toeicez.com",
        port: "",
        pathname: "/images/web/**",
      },
      {
        protocol: "https",
        hostname: "ielts.com.vn",
        port: "",
        pathname: "/uploads/banner/**",
      },
    ],
  },
};

export default nextConfig;
