"use client";

import { ReactElement, ReactNode } from "react";
import { LearningNavbar } from "@/components/shared/navbar";
import { LearningSidebar } from "@/components/shared/sidebar";
import { twMerge } from "tailwind-merge";
import {
  LearningProvider,
  SidebarProvider,
  useSidebarContext,
} from "@/providers";

export function LearningLayout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return (
    <SidebarProvider>
      <LearningLayoutContent>{children}</LearningLayoutContent>
    </SidebarProvider>
  );
}

function LearningLayoutContent({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  const { isCollapsed } = useSidebarContext();
  return (
    <LearningProvider>
      <LearningNavbar />
      <div className='mt-16 flex items-start'>
        <LearningSidebar />
        <div
          id='main-content'
          className={twMerge(
            "relative w-full overflow-y-auto bg-gray-100 dark:bg-slate-950",
            isCollapsed ? "lg:px-0" : "lg:pl-72"
          )}
        >
          {children}
        </div>
      </div>
    </LearningProvider>
  );
}
