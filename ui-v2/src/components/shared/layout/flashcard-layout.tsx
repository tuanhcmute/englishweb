"use client";

import { FlashcardSidebar } from "@/components/shared/sidebar";
import { ReactNode } from "react";

export const FlashcardLayout: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  return (
    <section id='flashcards' className='py-10 bg-gray-100 dark:bg-slate-950'>
      {/* Flashcard container */}
      <div className='container flex items-start gap-5 flex-col md:flex-row mt-5'>
        <div className='p-2 w-full border rounded-md bg-white dark:bg-slate-900 order-2'>
          {children}
        </div>
        <FlashcardSidebar />
      </div>
    </section>
  );
};
