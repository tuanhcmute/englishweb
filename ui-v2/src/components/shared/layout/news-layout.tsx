"use client";

import React, { ReactNode } from "react";
import { NewsSidebar } from "@/components/shared/sidebar";

export const NewsLayout: React.FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <div className="py-10 bg-gray-100 dark:bg-slate-950" id="news">
      <div className="container flex items-start gap-5 flex-col md:flex-row">
        <div className="p-2 w-full border rounded-md dark:bg-slate-900 order-2 bg-white">
          {children}
        </div>
        <NewsSidebar />
      </div>
    </div>
  );
};
