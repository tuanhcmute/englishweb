"use client";
import { ReactElement, ReactNode } from "react";
import { AppNavbar } from "@/components/shared/navbar";
import { AppFooter } from "@/components/shared/footer";
import { ChatBox } from "@/components/shared/chatbox";

export function AppLayout({ children }: { children: ReactNode }): ReactElement {
  return (
    <div className='relative'>
      <AppNavbar />
      <main className='mt-16'>{children}</main>
      <AppFooter />
      <ChatBox />
    </div>
  );
}
