"use client";

import { ReactElement, ReactNode } from "react";
import { AdminNavbar } from "@/components/shared/navbar";
import { AdminSidebar } from "@/components/shared/sidebar";
import { twMerge } from "tailwind-merge";
import { SidebarProvider, useSidebarContext } from "@/providers";

export function AdminLayout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return (
    <SidebarProvider>
      <AdminLayoutContent>{children}</AdminLayoutContent>
    </SidebarProvider>
  );
}

function AdminLayoutContent({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  const { isCollapsed } = useSidebarContext();
  return (
    <>
      <AdminNavbar />
      <div className='mt-16 flex items-start'>
        <AdminSidebar />
        <div
          id='main-content'
          className={twMerge(
            "relative w-full overflow-y-auto bg-gray-100 dark:bg-slate-950",
            isCollapsed ? "lg:px-5" : "lg:pl-[300px]"
          )}
        >
          {children}
        </div>
      </div>
    </>
  );
}
