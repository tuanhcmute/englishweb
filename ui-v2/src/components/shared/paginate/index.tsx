import { cn } from "@/lib";
import React, { ReactNode, useEffect, useState } from "react";
import ReactPaginate from "react-paginate";

export function PaginatedItems<T>({
  itemsPerPage,
  children,
  items,
  setCurrentItems,
}: {
  itemsPerPage: number;
  items: T[];
  children: ReactNode;
  setCurrentItems: (currentItems: T[]) => void;
}) {
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);

  // Simulate fetching items from another resources.
  // (This could be items from props; or items loaded in a local state
  // from an API endpoint with useEffect and useState)
  // const endOffset = itemOffset + itemsPerPage;
  // console.log(`Loading items from ${itemOffset} to ${endOffset}`);
  // const currentItems = items.slice(itemOffset, endOffset);

  // Invoke when user click to request another page.
  const handlePageClick = (event: { selected: number }) => {
    const newOffset = (event.selected * itemsPerPage) % items.length;
    console.log(
      `User requested page number ${event.selected}, which is offset ${newOffset}`
    );
    setItemOffset(newOffset);
  };

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    console.log(`Loading items from ${itemOffset} to ${endOffset}`);
    const currentItems = items.slice(itemOffset, endOffset);
    const pageCount = Math.ceil(items.length / itemsPerPage);
    setCurrentItems(currentItems);
    setPageCount(pageCount);
  }, [itemOffset, items, itemsPerPage, setCurrentItems]);

  return (
    <>
      {children}
      <ReactPaginate
        className='mt-4 flex items-center gap-3 justify-center '
        breakLabel='...'
        nextLabel='Trang sau >'
        onPageChange={(e) => handlePageClick(e)}
        pageRangeDisplayed={5}
        pageCount={pageCount}
        previousLabel='< Trang trước'
        previousClassName='px-3 h-10 rounded-md bg-transparent border border-gray-400 hover:border-purple-800 flex items-center flex-col justify-center cursor-pointer hover:bg-blue-200 transision-all text-sm min-w-[100px]'
        nextClassName='px-3 h-10 rounded-md bg-transparent border border-gray-400 hover:border-purple-800 flex items-center flex-col justify-center cursor-pointer hover:bg-blue-200 transision-all text-sm min-w-[100px]'
        pageClassName={cn(
          "w-10 h-10 rounded-md border border-gray-400 bg-transparent hover:text-purple-800 flex items-center flex-col justify-center cursor-pointer hover:bg-blue-100 hover:border-purple-800 transision-all tex-sm"
        )}
        activeClassName='border-purple-800 text-purple-800'
        renderOnZeroPageCount={null}
      />
    </>
  );
}
