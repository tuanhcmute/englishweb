"use client";
import { Skeleton } from "@/components/ui";

export function Fallback() {
  return (
    <Skeleton className='h-screen w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
  );
}
