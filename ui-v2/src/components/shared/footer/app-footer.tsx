import { GitHubLogoIcon, InstagramLogoIcon } from "@radix-ui/react-icons";
import { CopyrightIcon } from "lucide-react";
import Link from "next/link";
import { ReactElement } from "react";

export function AppFooter(): ReactElement {
  return (
    <footer
      id='app-footer'
      className='bg-gray-50 border-t dark:bg-gray-800 border-gray-300 dark:border-transparent'
    >
      <div className='flex justify-center'>
        <div className='grid w-full justify-between sm:flex sm:justify-between md:flex md:grid-cols-1 py-3 container border-b'>
          <div>
            <Link
              href='/'
              className='mb-4 flex items-center sm:mb-0 dark:text-white'
            >
              <span
                data-testid='flowbite-footer-brand-span'
                className='self-center whitespace-nowrap text-2xl font-semibold transition-all hover:text-purple-800 dark:text-white'
              >
                English Academy
              </span>
            </Link>
          </div>
          <div className='grid grid-cols-2 gap-8 sm:mt-4 sm:grid-cols-3 sm:gap-6'>
            <div className='block w-fit'>
              <p className='uppercase font-bold text-purple-800 dark:text-white'>
                Về chúng tôi
              </p>
              <div className='mt-3 flex flex-col gap-2'>
                <Link
                  href='/about-us'
                  className='text-sm hover:text-purple-800 dark:hover:text-gray-300'
                >
                  Giới thiệu
                </Link>
              </div>
            </div>
            <div className='block w-fit'>
              <p className='uppercase font-bold text-purple-800 dark:text-white'>
                Tài nguyên
              </p>
              <div className='mt-3 flex flex-col gap-2'>
                <Link
                  href='/courses'
                  className='text-sm hover:text-purple-800 dark:hover:text-gray-300'
                >
                  Khóa học online
                </Link>
                <Link
                  href='/tests'
                  className='text-sm hover:text-purple-800 dark:hover:text-gray-300'
                >
                  Đề thi online
                </Link>
                <Link
                  href='/flashcards'
                  className='text-sm hover:text-purple-800 dark:hover:text-gray-300'
                >
                  Flashcards
                </Link>
              </div>
            </div>
            <div className='block w-fit'>
              <p className='uppercase font-bold text-purple-800 dark:text-white'>
                Điều khoản
              </p>
              <div className='mt-3 flex flex-col gap-2'>
                <Link
                  href='/policy'
                  className='text-sm hover:text-purple-800 dark:hover:text-gray-300'
                >
                  Chính sách & điều khoản
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className='w-full px-4 py-6 sm:flex sm:items-center sm:justify-center'>
          <div className='container flex justify-between'>
            <div className='flex items-center gap-1 text-purple-800 text-[12px]'>
              <CopyrightIcon className='w-5 h-5' />
              English Academy™ 2024
            </div>
            <div className='flex space-x-6 sm:mt-0 sm:justify-center'>
              <Link href='#'>
                <InstagramLogoIcon />
              </Link>
              <Link href='#'>
                <GitHubLogoIcon />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
