import { IconLoader2 } from "@tabler/icons-react";
import { ReactElement } from "react";

export function Loading(): ReactElement {
  return (
    <div className='flex justify-center h-screen'>
      <div className='flex items-center gap-2'>
        <p>Đang tải dữ liệu</p>
        <IconLoader2 className='animate-spin' />
      </div>
    </div>
  );
}
