import { DoubleArrowRightIcon } from "@radix-ui/react-icons";
import Link from "next/link";
import { ReactElement, ReactNode } from "react";

export function Breadcrumb({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return <div className='flex items-center gap-1 flex-wrap'>{children}</div>;
}

export function BreadcrumbItem({
  icon,
  title,
  href,
  isCurrent = false,
  className,
}: {
  icon?: any;
  title: string;
  href?: string;
  isCurrent?: boolean;
  className?: string;
}): ReactElement {
  const Icon = icon;
  return (
    <Link
      href={href || "#"}
      className={`flex items-center gap-1 text-sm ${
        isCurrent ? "text-purple-800 cursor-text" : ""
      } ${className || ""}`}
    >
      {icon && <Icon />}
      <p>{title}</p>
    </Link>
  );
}

export function BreadcrumbArrow({
  className,
}: {
  className?: string;
}): ReactElement {
  return <DoubleArrowRightIcon className={className || ""} />;
}
