import { CKEditor } from "@ckeditor/ckeditor5-react";
import _defaults from "ckeditor5-custom-build";
import { ReactElement } from "react";

const { Editor } = _defaults;

interface IProps {
  initialData: string;
  handleChange: ({
    data,
    editor,
  }: {
    data: string;
    editor: any;
    event: any;
  }) => void;
}

const editorConfiguration = {
  toolbar: [
    "heading",
    "|",
    "bold",
    "italic",
    "link",
    "bulletedList",
    "numberedList",
    "|",
    "outdent",
    "indent",
    "|",
    "imageUpload",
    "blockQuote",
    "insertTable",
    "mediaEmbed",
    "undo",
    "redo",
    "accessibilityHelp",
    "alignment",
    "code",
    "codeBlock",
    "findAndReplace",
    "fontBackgroundColor",
    "fontColor",
    "fontFamily",
    "fontSize",
    "highlight",
    "htmlEmbed",
    "imageInsert",
    "pageBreak",
    "removeFormat",
    "selectAll",
    "showBlocks",
    "sourceEditing",
    "specialCharacters",
    "restrictedEditingException",
    "strikethrough",
    "style",
    "subscript",
    "superscript",
    "textPartLanguage",
    "underline",
  ],
};

export function CustomEditor({
  initialData,
  handleChange,
}: IProps): ReactElement {
  return (
    <CKEditor
      editor={Editor}
      config={editorConfiguration}
      data={initialData}
      onChange={(event, editor) => {
        const data = editor.getData();
        handleChange({ data, editor, event });
      }}
      onReady={(editor) => {
        const root = editor.editing.view.document.getRoot();
        if (root) {
          editor.editing.view.change((writer) => {
            writer.setStyle("min-height", "200px", root);
            writer.setStyle("max-height", "700px", root);
            writer.setStyle("overflow-y", "scroll", root);
          });
        }
      }}
    />
  );
}
