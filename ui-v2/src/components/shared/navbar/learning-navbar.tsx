"use client";

import {
  Avatar,
  AvatarFallback,
  AvatarImage,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui";
import Link from "next/link";
import { ReactElement } from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { isSmallScreen } from "@/lib";
import { useSidebarContext } from "@/providers";
import { BarChartIcon } from "@radix-ui/react-icons";
import { XIcon } from "lucide-react";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import { useCourse, useUserCredential } from "@/hooks";
import { getCookie } from "cookies-next";
import { LearningTheme } from "@/components/app/theme";
import { NoteSheet } from "@/components/app/sheet";
import { useParams } from "next/navigation";

const FormSchema = z.object({
  theme_setting: z.boolean().default(false).optional(),
  security_emails: z.boolean(),
});

export function LearningNavbar(): ReactElement {
  const { isCollapsed: isSidebarCollapsed, setCollapsed: setSidebarCollapsed } =
    useSidebarContext();

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      security_emails: true,
    },
  });

  function onSubmit() {}

  const params: { courseId: string } = useParams();
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const userCredentialQuery = useUserCredential(userCredentialId || "");
  const courseQuery = useCourse(params.courseId);

  return (
    <header>
      <nav className='fixed top-0 z-30 w-full border-b border-gray-300 bg-white p-0 dark:border-gray-700 dark:bg-gray-800 sm:p-0'>
        <div className='w-full p-3 pr-4'>
          <div className='flex items-center justify-between'>
            <div className='flex items-center'>
              <button
                className='mr-2 cursor-pointer rounded p-2 hover:bg-blue-100 focus:bg-gray-100 focus:ring-2 focus:ring-blue-100 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white dark:focus:bg-gray-700 dark:focus:ring-gray-700 hover:text-purple-800'
                onClick={() => setSidebarCollapsed(!isSidebarCollapsed)}
              >
                {isSidebarCollapsed || !isSmallScreen() ? (
                  <BarChartIcon className='h-[1.2rem] w-[1.2rem]' />
                ) : (
                  <XIcon className='h-[1.2rem] w-[1.2rem]' />
                )}
              </button>
              <p className='w-full px-3 text-md font-semibold hidden md:block'>
                {courseQuery.data?.courseName}
              </p>
            </div>
            <div className='flex items-center gap-4 flex-wrap'>
              <NoteSheet />
              <LearningTheme />
              <DropdownMenu>
                <DropdownMenuTrigger>
                  <Avatar>
                    <AvatarImage
                      src={userCredentialQuery.data?.data.avatar}
                      alt='@shadcn'
                    />
                    <AvatarFallback className='border dark:bg-blue-600 bg-gray-300 border-gray-400 dark:border-blue-700'>
                      DT
                    </AvatarFallback>
                  </Avatar>
                </DropdownMenuTrigger>
                <DropdownMenuContent className='min-w-52'>
                  <DropdownMenuLabel className='text-center text-sm'>
                    Xin chào, {userCredentialQuery.data?.data.fullName}
                  </DropdownMenuLabel>
                  <DropdownMenuSeparator />
                  <DropdownMenuItem className='w-full cursor-pointer'>
                    <Link href={ROUTES.HOME} className='block w-full'>
                      Quay lại trang chủ
                    </Link>
                  </DropdownMenuItem>
                </DropdownMenuContent>
              </DropdownMenu>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
}
