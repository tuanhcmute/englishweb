import {
  Avatar,
  AvatarFallback,
  AvatarImage,
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuShortcut,
  DropdownMenuTrigger,
} from "@/components/ui";
import React, { ReactElement } from "react";
import Link from "next/link";
import { Theme } from "@/components/app/theme";
import { BarChartIcon } from "@radix-ui/react-icons";
import { useTheme } from "next-themes";
import { ROUTES, USER_CREDENTIAL, USER_CREDENTIAL_ID } from "@/constants";
import { useLogout, useUserCredential } from "@/hooks";
import { getCookie } from "cookies-next";
import { usePathname } from "next/navigation";
import { cn } from "@/lib";

const components: {
  title: string;
  href: string;
  description: string;
  icon?: any;
}[] = [
  {
    title: "Khóa học online",
    href: ROUTES.COURSES,
    description:
      "For sighted users to preview content available behind a link.",
  },
  {
    title: "Đề thi online",
    href: ROUTES.TESTS,
    description:
      "Displays an indicator showing the completion progress of a task, typically displayed as a progress bar.",
  },
  {
    title: "Flashcards",
    href: `${ROUTES.FLASHCARDS}/me`,
    description: "Visually or semantically separates content.",
  },
  {
    title: "Tin tức",
    href: ROUTES.NEWS,
    description:
      "A set of layered sections of content—known as tab panels—that are displayed one at a time.",
  },
  {
    title: "Về chúng tôi",
    href: ROUTES.ABOUT_US,
    description:
      "A popup that displays information related to an element when the element receives keyboard focus or the mouse hovers over it.",
  },
];

function MobileMenu({ className }: { className: string }): ReactElement {
  const userInfoJSON = getCookie(USER_CREDENTIAL);
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const userCredentialQuery = useUserCredential(userCredentialId || "");
  const { mutateAsync: handleLogout } = useLogout();

  return (
    <div className={className || ""}>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant='outline' size='icon'>
            <BarChartIcon className='h-[1.2rem] w-[1.2rem]' />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent align='end'>
          {components.map((item) => {
            const Icon = item.icon;
            return (
              <DropdownMenuItem key={item.title}>
                <Link
                  href={item.href}
                  className='cursor-pointer transition-colors w-full flex items-center gap-5 min-w-40'
                >
                  {item.title}
                  {Icon && (
                    <DropdownMenuShortcut>
                      <Icon />
                    </DropdownMenuShortcut>
                  )}
                </Link>
              </DropdownMenuItem>
            );
          })}
          <DropdownMenuSeparator />
          {/* <DropdownMenuSeparator /> */}
          <DropdownMenuItem className=''>
            {userInfoJSON ? (
              <DropdownMenu>
                <DropdownMenuTrigger>
                  <div className='flex gap-2 items-center'>
                    <img
                      className='w-6 h-6 object-cover rounded-full'
                      src={userCredentialQuery.data?.data.avatar}
                      alt='@shadcn'
                    />
                    <p>{userCredentialQuery.data?.data.username}</p>
                  </div>
                </DropdownMenuTrigger>
                <DropdownMenuContent className='min-w-52'>
                  <DropdownMenuLabel className='text-center'>
                    Tài khoản của tôi
                  </DropdownMenuLabel>
                  <DropdownMenuSeparator />
                  <DropdownMenuItem className='w-full cursor-pointer'>
                    <Link href='/my-account/courses' className='block w-full'>
                      Khóa học của tôi
                    </Link>
                  </DropdownMenuItem>
                  <DropdownMenuItem className='w-full cursor-pointer'>
                    <Link
                      href={ROUTES.MY_TEST_RESULTS}
                      className='block w-full'
                    >
                      Kết quả luyện thi
                    </Link>
                  </DropdownMenuItem>
                  <DropdownMenuSeparator />
                  <DropdownMenuItem className='w-full cursor-pointer'>
                    <Link href='/my-account/profile' className='block w-full'>
                      Trang cá nhân
                    </Link>
                  </DropdownMenuItem>
                  <DropdownMenuItem className='w-full cursor-pointer'>
                    <div
                      className='block w-full'
                      onClick={async () => await handleLogout()}
                    >
                      Đăng xuất tài khoản
                    </div>
                  </DropdownMenuItem>
                </DropdownMenuContent>
              </DropdownMenu>
            ) : (
              <Link
                href={ROUTES.LOGIN}
                className={`block w-full ${userInfoJSON && "hidden"}`}
              >
                Đăng nhập
              </Link>
            )}
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </div>
  );
}

function DesktopMenu({ className }: { className: string }) {
  const userInfoJSON = getCookie(USER_CREDENTIAL);
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const userCredentialQuery = useUserCredential(userCredentialId || "");
  const { mutateAsync: handleLogout } = useLogout();
  const pathname = usePathname();

  return (
    <div className={className || ""}>
      <div className='flex justify-between items-center lg:order-2 gap-3 text-[15px]'>
        {/* Desktop menu */}
        <div className='hidden justify-between items-center w-full lg:flex lg:w-auto list-none gap-5'>
          <Link
            href='/'
            className={cn(
              "cursor-pointer transition-colors hover:dark:text-purple-800 hover:text-purple-800 text-sm",
              pathname === "/" ? "text-purple-800" : ""
            )}
          >
            Trang chủ
          </Link>

          {components.map((item) => {
            return (
              <Link
                key={item.title}
                href={item.href}
                className={cn(
                  "cursor-pointer transition-colors hover:dark:text-purple-800 hover:text-purple-800 text-sm",
                  pathname.includes(item.href) ? "text-purple-800" : ""
                )}
              >
                {item.title}
              </Link>
            );
          })}
        </div>
        <Theme />
        {userInfoJSON ? (
          <DropdownMenu>
            <DropdownMenuTrigger>
              <Avatar>
                <AvatarImage
                  src={userCredentialQuery.data?.data.avatar}
                  alt='@shadcn'
                />
                <AvatarFallback>
                  <div className='w-10 h-10 rounded-full border border-gray-400 flex flex-col justify-center items-center'>
                    {userCredentialQuery.data?.data.username
                      .charAt(0)
                      .toUpperCase()}
                  </div>
                </AvatarFallback>
              </Avatar>
            </DropdownMenuTrigger>
            <DropdownMenuContent className='min-w-52'>
              <DropdownMenuLabel className='text-center'>
                Tài khoản của tôi
              </DropdownMenuLabel>
              <DropdownMenuSeparator />
              <DropdownMenuItem className='w-full cursor-pointer'>
                <Link href='/my-account/courses' className='block w-full'>
                  Khóa học của tôi
                </Link>
              </DropdownMenuItem>
              <DropdownMenuItem className='w-full cursor-pointer'>
                <Link href={ROUTES.MY_TEST_RESULTS} className='block w-full'>
                  Kết quả luyện thi
                </Link>
              </DropdownMenuItem>
              <DropdownMenuSeparator />
              <DropdownMenuItem className='w-full cursor-pointer'>
                <Link href='/my-account/profile' className='block w-full'>
                  Trang cá nhân
                </Link>
              </DropdownMenuItem>
              <DropdownMenuItem className='w-full cursor-pointer'>
                <div
                  className='block w-full'
                  onClick={async () => await handleLogout()}
                >
                  Đăng xuất tài khoản
                </div>
              </DropdownMenuItem>
            </DropdownMenuContent>
          </DropdownMenu>
        ) : (
          <Link href={ROUTES.LOGIN} className=''>
            <Button
              variant='outline'
              className='border border-purple-800 text-purple-800 hover:bg-purple-800 hover:text-white dark:text-white'
            >
              Đăng nhập
            </Button>
          </Link>
        )}
      </div>
    </div>
  );
}

export function AppNavbar(): ReactElement {
  return (
    <header>
      <nav className='fixed top-0 z-30 w-full bg-gray-50 border-b border-gray-300 dark:bg-gray-800 dark:border-gray-700 py-3'>
        <div className='flex justify-between items-center w-full container'>
          <div className='flex items-center justify-between'>
            <Link className='flex items-center [&>p]:px-0' href='/'>
              <p className='self-center whitespace-nowrap px-3 text-xl font-semibold dark:text-white hover:text-purple-800 transition-all'>
                English Academy
              </p>
            </Link>
          </div>
          <DesktopMenu className='hidden lg:block' />
          <MobileMenu className='block lg:hidden' />
        </div>
      </nav>
    </header>
  );
}
