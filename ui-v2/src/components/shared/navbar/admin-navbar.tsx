"use client";

import {
  Avatar,
  AvatarFallback,
  AvatarImage,
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
  Switch,
} from "@/components/ui";
import Link from "next/link";
import { ReactElement } from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { isSmallScreen } from "@/lib";
import { useSidebarContext } from "@/providers";
import { BarChartIcon } from "@radix-ui/react-icons";
import { XIcon } from "lucide-react";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import { useLogout, useUserCredential } from "@/hooks";
import { getCookie } from "cookies-next";
import { LearningTheme } from "@/components/app/theme";

const FormSchema = z.object({
  theme_setting: z.boolean().default(false).optional(),
  security_emails: z.boolean(),
});

export function AdminNavbar(): ReactElement {
  const { isCollapsed: isSidebarCollapsed, setCollapsed: setSidebarCollapsed } =
    useSidebarContext();

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
    defaultValues: {
      security_emails: true,
    },
  });

  function onSubmit() {}

  const { mutateAsync: handleLogout } = useLogout();
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const userCredentialQuery = useUserCredential(userCredentialId || "");

  return (
    <header>
      <nav className='fixed top-0 z-30 w-full border-b border-gray-300 bg-white p-0 dark:border-gray-700 dark:bg-gray-800 sm:p-0'>
        <div className='w-full p-3 pr-4'>
          <div className='flex items-center justify-between'>
            <div className='flex items-center'>
              <button
                className='mr-2 cursor-pointer rounded p-2 hover:bg-blue-100 focus:bg-gray-100 focus:ring-2 focus:ring-blue-100 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white dark:focus:bg-gray-700 dark:focus:ring-gray-700 hover:text-purple-800'
                onClick={() => setSidebarCollapsed(!isSidebarCollapsed)}
              >
                {isSidebarCollapsed || !isSmallScreen() ? (
                  <BarChartIcon className='h-[1.2rem] w-[1.2rem]' />
                ) : (
                  <XIcon className='h-[1.2rem] w-[1.2rem]' />
                )}
              </button>
              <Link href={ROUTES.ADMIN_DASHBOARD}>
                <span className='uppercase self-center whitespace-nowrap px-3 text-xl font-semibold dark:text-white hover:text-purple-800 transition-all'>
                  Administrator
                </span>
              </Link>
            </div>
            <div className='flex items-center gap-4'>
              <LearningTheme />
              <Sheet>
                <DropdownMenu>
                  <DropdownMenuTrigger>
                    <Avatar>
                      <AvatarImage
                        src={userCredentialQuery.data?.data.avatar}
                        alt='@shadcn'
                      />
                      <AvatarFallback>CN</AvatarFallback>
                    </Avatar>
                  </DropdownMenuTrigger>
                  <DropdownMenuContent className='min-w-52'>
                    <DropdownMenuLabel className='text-center'>
                      Xin chào, {userCredentialQuery.data?.data.fullName}
                    </DropdownMenuLabel>
                    <DropdownMenuSeparator />
                    <DropdownMenuItem className='w-full cursor-pointer'>
                      <Link href='/my-account/courses' className='block w-full'>
                        Thông tin cá nhân
                      </Link>
                    </DropdownMenuItem>
                    <DropdownMenuItem className='w-full cursor-pointer'>
                      <Link
                        href='/my-account/test-results'
                        className='block w-full'
                      >
                        Đổi mật khẩu
                      </Link>
                    </DropdownMenuItem>
                    <DropdownMenuSeparator />
                    <SheetTrigger asChild>
                      <DropdownMenuItem className='w-full cursor-pointer'>
                        Cài đặt
                      </DropdownMenuItem>
                    </SheetTrigger>
                    <DropdownMenuItem className='w-full cursor-pointer'>
                      <div
                        onClick={async () => await handleLogout()}
                        className='block w-full'
                      >
                        Đăng xuất
                      </div>
                    </DropdownMenuItem>
                  </DropdownMenuContent>
                </DropdownMenu>
                <SheetContent>
                  <SheetHeader>
                    <SheetTitle>Cài đặt</SheetTitle>
                  </SheetHeader>
                  <div className='grid gap-4 py-4'>
                    <Form {...form}>
                      <form
                        onSubmit={form.handleSubmit(onSubmit)}
                        className='w-full space-y-6'
                      >
                        <div>
                          <div className='space-y-4'>
                            <FormField
                              control={form.control}
                              name='theme_setting'
                              render={({ field }) => (
                                <FormItem className='flex flex-row items-center justify-between rounded-lg border p-3 shadow-sm'>
                                  <div className='space-y-0.5'>
                                    <FormLabel>Tùy chỉnh giao diện</FormLabel>
                                  </div>
                                  <FormControl>
                                    <Switch
                                      checked={field.value}
                                      onCheckedChange={field.onChange}
                                    />
                                  </FormControl>
                                </FormItem>
                              )}
                            />
                            {/* <FormField
                              control={form.control}
                              name='security_emails'
                              render={({ field }) => (
                                <FormItem className='flex flex-row items-center justify-between rounded-lg border p-3 shadow-sm'>
                                  <div className='space-y-0.5'>
                                    <FormLabel>Tùy chỉnh ngôn ngữ</FormLabel>
                                  </div>
                                  <FormControl>
                                    <Switch
                                      checked={field.value}
                                      onCheckedChange={field.onChange}
                                      disabled
                                      aria-readonly
                                    />
                                  </FormControl>
                                </FormItem>
                              )}
                            /> */}
                          </div>
                        </div>
                        <Button
                          type='submit'
                          className='bg-blue-500 hover:bg-blue-400'
                        >
                          Submit
                        </Button>
                      </form>
                    </Form>
                  </div>
                </SheetContent>
              </Sheet>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
}
