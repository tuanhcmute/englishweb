"use client";

import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  Input,
} from "@/components/ui";
import { cn } from "@/lib";
import { BotIcon, MessageCircleIcon, XIcon } from "lucide-react";
import { useEffect, useState } from "react";
import { z } from "zod";
import { ChatMessage, IChatMessage } from "./chat-message";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { ICreateSkilledAgentChatRequest } from "@/types";
import {
  useActivatedSkilledAgentVersion,
  useCreateSkilledAgentChat,
} from "@/hooks";

const formSchema = z.object({
  user_id: z.string().trim().min(1),
  message: z.string().trim().min(1),
  is_kguru_msg: z.boolean().default(false),
  skilled_agent_version_id: z.string().trim(),
});

export const ChatBox = () => {
  const [isOpen, setOpen] = useState<boolean>(false);
  const [isClosing, setIsClosing] = useState(false);
  const [conversation, setConversation] = useState<IChatMessage[]>([
    {
      isBotMessage: true,
      messageContent: "Xin chào, tôi có thể giúp gì cho bạn?",
    },
  ]);
  const skilledAgentVersionQuery = useActivatedSkilledAgentVersion();
  console.log({ skilledAgentVersion: skilledAgentVersionQuery.data?.data.id });
  const { mutateAsync: handleCreateSkilledAgentChat, isPending } =
    useCreateSkilledAgentChat();

  const handleOpenChat = () => {
    setOpen(true);
  };

  const handleCloseChat = () => {
    setIsClosing(true);
  };

  const toggleChat = () => {
    if (isOpen) {
      handleCloseChat();
    } else {
      handleOpenChat();
    }
  };

  useEffect(() => {
    if (isClosing) {
      setOpen(false);
      setIsClosing(false);
    }
  }, [isClosing]);

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      is_kguru_msg: false,
      message: "",
      skilled_agent_version_id: skilledAgentVersionQuery.data?.data.id || "",
      user_id: "f8c76d63-9589-44e3-90c9-85b07198908b",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
      skilled_agent_version_id: skilledAgentVersionQuery.data?.data.id,
    } as ICreateSkilledAgentChatRequest;
    setConversation((prev) => {
      return [...prev, { isBotMessage: false, messageContent: values.message }];
    });
    const response = await handleCreateSkilledAgentChat(data);
    setConversation((prev) => {
      return [
        ...prev,
        {
          isBotMessage: response.is_kguru_msg,
          messageContent: response.message,
        },
      ];
    });
  }

  return (
    <>
      <div
        id="chat-icon"
        onClick={toggleChat}
        className={cn(
          "select-none fixed bottom-5 right-5 bg-blue-500 text-white rounded-full w-12 h-12 flex items-center flex-col justify-center cursor-pointer shadow-lg hover:scale-110 transition-transform transform duration-200",
          isOpen ? "invisible" : "visible"
        )}
      >
        <MessageCircleIcon />
      </div>
      <div
        id="chat-box"
        className={`fixed bottom-0 right-0 w-full sm:bottom-5 sm:right-5 sm:w-96 bg-white border border-gray-300 dark:border-gray-600 rounded-lg shadow-lg transition-transform duration-300 ${
          isOpen ? "visible scale-1 origin-bottom-right" : "invisible scale-0"
        }`}
      >
        <div className="bg-blue-500 text-white p-3 rounded-t-lg flex items-center justify-between h-14">
          <div className="flex items-center gap-1">
            <BotIcon className="w-5 h-5" />
            Asistant
          </div>
          <XIcon onClick={toggleChat} className="w-6 h-6 cursor-pointer" />
        </div>
        <ul className="space-y-5 p-4 overflow-y-auto h-96 bg-blue-50 dark:bg-gray-900">
          {conversation.map((item) => {
            return <ChatMessage {...item} key={item.messageContent} />;
          })}
        </ul>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <FormField
              name="message"
              render={({ field }) => (
                <FormItem className="">
                  <FormControl>
                    <div className="w-full sm:w-96 flex items-center gap-1 p-4 border-t border-gray-300 dark:bg-gray-900 dark:border-gray-600 h-16 rounded-b-lg">
                      <Input placeholder="Type a message" {...field} />
                      <Button
                        className="bg-blue-500 hover:bg-blue-600 text-white"
                        type="submit"
                        disabled={isPending}
                        onClick={() => console.log(form.getValues())}
                      >
                        Gửi
                      </Button>
                    </div>
                  </FormControl>
                </FormItem>
              )}
            />
          </form>
        </Form>
      </div>
    </>
  );
};
