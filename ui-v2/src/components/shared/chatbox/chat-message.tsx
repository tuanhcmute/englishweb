import { cn } from "@/lib";

export interface IChatMessage {
  isBotMessage: boolean;
  messageContent: string;
  createdDate?: string;
}

export const ChatMessage: React.FC<IChatMessage> = ({
  isBotMessage,
  messageContent,
}) => {
  return (
    <li className='block'>
      <div
        className={cn(
          "flex gap-x-2 sm:gap-x-4 items-end",
          isBotMessage ? "" : "justify-end"
        )}
      >
        <div
          className={cn("w-3/4", isBotMessage ? "order-2" : "flex justify-end")}
        >
          <div
            className={cn(
              "inline-block rounded-2xl p-4 shadow-sm",
              isBotMessage ? "bg-white dark:bg-slate-800" : "bg-blue-600"
            )}
          >
            <p
              className={cn(
                "text-sm dark:text-white",
                isBotMessage ? "text-black" : "text-white"
              )}
            >
              {messageContent}
            </p>
          </div>
        </div>
        <span
          className={cn(
            "flex-shrink-0 inline-flex items-center justify-center size-[38px] rounded-full bg-gray-600",
            isBotMessage ? "order-1" : ""
          )}
        >
          <span className='text-sm text-white leading-none'>
            {isBotMessage ? "Bot" : "You"}
          </span>
        </span>
      </div>
    </li>
  );
};
