"use client";

import { useUnitsByCourse } from "@/hooks";
import { useSidebarContext } from "@/providers";
import { useParams } from "next/navigation";
import { ReactElement, useState } from "react";
import { twMerge } from "tailwind-merge";
import { LearningSidebarLinkItem } from "./learning-sidebar-link-item";
import { SidebarLinkGroup } from "./sidebar-link-group";

export function LearningSidebar(): ReactElement {
  const { isCollapsed } = useSidebarContext();
  const params: { courseId: string } = useParams();
  const unitsByCourseQuery = useUnitsByCourse(params.courseId);

  if (unitsByCourseQuery.error) {
    return (
      <div className='font-bold text-center w-full'>
        {unitsByCourseQuery.error.message}
      </div>
    );
  }

  return (
    <aside
      id='sidebar'
      className={twMerge(
        "fixed inset-y-0 left-0 z-20 mt-16 flex shrink-0 flex-col border-r border-gray-300 duration-75 dark:border-gray-700 lg:flex [&>div]:rounded-none transition-all w-72 bg-white dark:bg-gray-800",
        isCollapsed && "hidden w-0"
      )}
    >
      <div className='overflow-y-auto overflow-x-hidden'>
        <div className='pt-1'>
          <div className='border-t border-gray-200 pt-3 first:mt-0 first:border-t-0 first:pt-0 dark:border-gray-700 mb-10'>
            {unitsByCourseQuery.data?.data
              .sort((a, b) => a.sequence - b.sequence)
              .map((item, index) => {
                if (item.lessons && item.lessons.length > 0)
                  return <SidebarLinkGroup key={index} item={item} />;
                return <LearningSidebarLinkItem key={index} item={item} />;
              })}
          </div>
        </div>
      </div>
    </aside>
  );
}
