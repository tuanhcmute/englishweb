"use client";

import { Tooltip, TooltipContent, TooltipTrigger } from "@/components/ui";
import { cn } from "@/lib";
import { IUnitResponse } from "@/types";
import { usePathname } from "next/navigation";

export const LearningSidebarLinkItem: React.FC<{
  item: IUnitResponse;
}> = ({ item }) => {
  const pathname = usePathname();

  return (
    <div
      className={cn(
        "flex items-center justify-between cursor-pointer py-3 hover:bg-gray-200 dark:hover:bg-gray-700 px-2 pl-4 hover:border-l-8 border-purple-800 hover:pl-2 select-none",
        pathname.includes("aaa")
          ? "bg-gray-200 dark:bg-gray-700 border-l-8 border-purple-800 pl-2 text-purple-800 dark:text-white"
          : ""
      )}
    >
      <Tooltip>
        <TooltipTrigger asChild>
          <h4 className='text-sm font-medium truncate gap-1'>{item.title}</h4>
        </TooltipTrigger>
        <TooltipContent>
          <p>{item.title}</p>
        </TooltipContent>
      </Tooltip>
    </div>
  );
};
