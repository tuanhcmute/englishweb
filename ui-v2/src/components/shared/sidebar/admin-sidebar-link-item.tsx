"use client";

import { cn } from "@/lib";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { ILink } from "./links";

export const AdminSidebarLinkItem: React.FC<{
  item: ILink;
  onClick?: () => void;
}> = ({ item, onClick }) => {
  const Icon = item.icon;
  const pathname = usePathname();

  return (
    <Link
      onClick={onClick}
      href={item.href}
      className={cn(
        "flex items-center justify-between cursor-pointer py-3 hover:bg-gray-200 dark:hover:bg-gray-700 px-2 pl-4 hover:border-l-8 border-purple-800 hover:pl-2 select-none",
        pathname.includes(item.href)
          ? "bg-gray-200 dark:bg-gray-700 border-l-8 border-purple-800 pl-2 text-purple-800 dark:text-white"
          : ""
      )}
    >
      <h4 className='text-sm font-medium truncate flex items-center gap-1'>
        <Icon className='w-5 h-5' />
        {item.title}
      </h4>
    </Link>
  );
};
