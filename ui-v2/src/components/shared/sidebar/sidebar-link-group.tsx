"use client";

import {
  Collapsible,
  CollapsibleContent,
  CollapsibleTrigger,
  Tooltip,
  TooltipContent,
  TooltipTrigger,
} from "@/components/ui";
import { IUnitResponse } from "@/types";
import { ChevronDownIcon, VideoIcon } from "@radix-ui/react-icons";
import { useParams, useRouter } from "next/navigation";
import { useState } from "react";

export const SidebarLinkGroup: React.FC<{ item: IUnitResponse }> = ({
  item,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  const params: { courseId: string } = useParams();
  const ytplayer = document.getElementById("movie_player");

  return (
    <Collapsible open={isOpen} onOpenChange={setIsOpen} className='w-full'>
      <CollapsibleTrigger asChild>
        <div className='flex items-center justify-between cursor-pointer py-3 hover:bg-gray-200 dark:hover:bg-gray-700 px-2 pl-4 hover:border-l-8 border-purple-800 hover:pl-2 select-none'>
          <Tooltip>
            <TooltipTrigger asChild>
              <h4 className='text-sm font-medium truncate'>{item.title}</h4>
            </TooltipTrigger>
            <TooltipContent>
              <p>{item.title}</p>
            </TooltipContent>
          </Tooltip>
          <ChevronDownIcon className='h-4 w-4' />
        </div>
      </CollapsibleTrigger>
      <CollapsibleContent className=''>
        {item.lessons
          .sort((a, b) => a.sequence - b.sequence)
          .map((item, index) => {
            return (
              <div
                key={index}
                className='px-6 py-3 text-sm shadow-sm cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-700 hover:border-l-8 border-purple-800 hover:pl-4 select-none'
                onClick={() =>
                  router.push(`${params.courseId}?lessonId=${item.id}`)
                }
              >
                <div className='flex items-center gap-1'>
                  <VideoIcon />
                  <Tooltip>
                    <TooltipTrigger asChild>
                      <p className='truncate'>{item.title}</p>
                    </TooltipTrigger>
                    <TooltipContent>
                      <p>{item.title}</p>
                    </TooltipContent>
                  </Tooltip>
                </div>
              </div>
            );
          })}
      </CollapsibleContent>
    </Collapsible>
  );
};
