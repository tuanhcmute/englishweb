"use client";

import {
  Collapsible,
  CollapsibleContent,
  CollapsibleTrigger,
  Tooltip,
  TooltipTrigger,
} from "@/components/ui";
import { cn } from "@/lib";
import { ArrowRightIcon, ChevronDownIcon } from "@radix-ui/react-icons";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import { useState } from "react";
import { ILink } from "./links";

export const AdminSidebarLinkGroup: React.FC<{ item: ILink }> = ({ item }) => {
  const [isOpen, setIsOpen] = useState(false);
  const Icon = item.icon;
  const pathname = usePathname();
  const router = useRouter();

  return (
    <>
      <Collapsible open={isOpen} onOpenChange={setIsOpen} className='w-full'>
        <CollapsibleTrigger asChild>
          <div
            className={cn(
              "flex items-center justify-between cursor-pointer py-3 hover:bg-gray-200 dark:hover:bg-gray-700 px-2 pl-4 hover:border-l-8 border-purple-800 hover:pl-2 select-none",
              pathname.includes(item.href)
                ? "bg-gray-200 dark:bg-gray-700 border-l-8 border-purple-800 pl-2 text-purple-800 dark:text-white"
                : ""
            )}
          >
            <h4 className='text-sm font-medium truncate flex items-center gap-1'>
              <Icon className='w-5 h-5' />
              {item.title}
            </h4>
            <ChevronDownIcon
              className={cn(
                "h-4 w-4 transition-all ease-linear",
                isOpen ? "rotate-180" : "rotate-0"
              )}
            />
          </div>
        </CollapsibleTrigger>
        <CollapsibleContent className=''>
          {item?.subLink &&
            item.subLink.map((item) => {
              return (
                <Link
                  href={item.href}
                  key={item.title}
                  className={cn(
                    "flex items-center gap-1 px-6 py-3 text-sm shadow-sm cursor-pointer hover:bg-gray-200 dark:hover:bg-gray-700 hover:border-l-8 hover:pl-4 select-none",
                    pathname === item.href || pathname.includes(item.href)
                      ? "text-purple-800 dark:text-white"
                      : ""
                  )}
                >
                  {/* {pathname === item.href ? <ArrowRightIcon /> : ""} */}
                  <ArrowRightIcon
                    className={cn(
                      "invisible transition-all ease-linear",
                      pathname === item.href ? "visible" : ""
                    )}
                  />
                  <div className='flex items-center gap-1'>
                    <Tooltip>
                      <TooltipTrigger asChild>
                        <p className='truncate'>{item.title}</p>
                      </TooltipTrigger>
                    </Tooltip>
                  </div>
                </Link>
              );
            })}
        </CollapsibleContent>
      </Collapsible>
    </>
  );
};
