"use client";

import {
  Button,
  Collapsible,
  CollapsibleContent,
  CollapsibleTrigger,
  Skeleton,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { NewsCategoryType } from "@/enums";
import { useNewsCategories } from "@/hooks";
import { cn } from "@/lib";
import { useNewsContext } from "@/providers";
import { CaretSortIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import React from "react";

export const NewsSidebar = () => {
  const { newsCategoryId, setNewsCategoryId } = useNewsContext();
  const [isOpen, setIsOpen] = React.useState(true);
  const { data, isLoading } = useNewsCategories(NewsCategoryType.VISIBLE);
  const router = useRouter();

  const handleClick = (id: string) => {
    setNewsCategoryId(id);
    router.push(ROUTES.NEWS);
  };

  return (
    <div className="border top-20 md:sticky w-full md:w-3/5 lg:w-1/3 rounded-md dark:bg-slate-900 bg-white p-2">
      <Collapsible
        open={isOpen}
        onOpenChange={setIsOpen}
        className="w-full space-y-2"
      >
        <div className="flex items-center justify-between">
          <h4 className="font-semibold">Danh mục</h4>
          <CollapsibleTrigger asChild>
            <Button variant="ghost" size="sm">
              <CaretSortIcon className="h-4 w-4" />
              <span className="sr-only">Toggle</span>
            </Button>
          </CollapsibleTrigger>
        </div>
        <CollapsibleContent className="space-y-2">
          {isLoading ? (
            <div className="flex flex-col gap-2">
              {Array.from({ length: 4 }).map((_, index) => {
                return (
                  <div key={index}>
                    <Skeleton className=" h-10 w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
                  </div>
                );
              })}
            </div>
          ) : (
            <>
              <div
                className={cn(
                  "px-3 py-1 text-sm hover:shadow-sm transition-all hover:text-purple-800 dark:text-white cursor-pointer",
                  newsCategoryId === "" ? "text-purple-800" : ""
                )}
                onClick={() => setNewsCategoryId("")}
              >
                <div className="block">Tất cả</div>
              </div>
              {data?.data.map((item) => {
                return (
                  <div
                    className={cn(
                      "px-3 py-1 text-sm hover:shadow-sm transition-all hover:text-purple-800 dark:text-white cursor-pointer",
                      newsCategoryId === item.id ? "text-purple-800" : ""
                    )}
                    key={item.id}
                    onClick={() => handleClick(item.id)}
                  >
                    <div className="block">{item.newsCategoryName}</div>
                  </div>
                );
              })}
            </>
          )}
        </CollapsibleContent>
      </Collapsible>
    </div>
  );
};
