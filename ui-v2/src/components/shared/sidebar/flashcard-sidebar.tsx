import {
  Button,
  Collapsible,
  CollapsibleContent,
  CollapsibleTrigger,
} from "@/components/ui";
import { cn } from "@/lib";
import { CaretSortIcon } from "@radix-ui/react-icons";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";

export const FlashcardSidebar = () => {
  const [isOpen, setIsOpen] = React.useState(true);
  const pathname = usePathname();

  return (
    <div className='border top-20 md:sticky w-full md:w-3/5 lg:w-1/3 rounded-md dark:bg-slate-900 bg-white p-2'>
      <Collapsible
        open={isOpen}
        onOpenChange={setIsOpen}
        className='w-full space-y-2'
      >
        <div className='flex items-center justify-between'>
          <h4 className='font-semibold'>Danh mục</h4>
          <CollapsibleTrigger asChild>
            <Button variant='ghost' size='sm'>
              <CaretSortIcon className='h-4 w-4' />
              <span className='sr-only'>Toggle</span>
            </Button>
          </CollapsibleTrigger>
        </div>
        <CollapsibleContent className='space-y-2'>
          <div
            className={cn(
              "px-4 py-2 text-sm hover:shadow-sm transition-all hover:text-purple-800",
              pathname.includes("/flashcards/me") ? "text-purple-800" : ""
            )}
          >
            <Link href='/flashcards/me' className='block'>
              Danh sách từ của tôi
            </Link>
          </div>
          <div
            className={cn(
              "px-4 py-2 text-sm hover:shadow-sm transition-all hover:text-purple-800",
              pathname.includes("/flashcards/studying") ? "text-purple-800" : ""
            )}
          >
            <Link href='/flashcards/studying' className='block'>
              Từ đang học
            </Link>
          </div>
          <div
            className={cn(
              "px-4 py-2 text-sm hover:shadow-sm transition-all hover:text-purple-800",
              pathname.includes("/flashcards/suggestion")
                ? "text-purple-800"
                : ""
            )}
          >
            <Link href='/flashcards/suggestion' className='block'>
              Danh sách từ gợi ý
            </Link>
          </div>
        </CollapsibleContent>
      </Collapsible>
    </div>
  );
};
