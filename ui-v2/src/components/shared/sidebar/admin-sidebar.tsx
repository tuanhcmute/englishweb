import { Skeleton } from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { Role } from "@/enums";
import { useUserCredential } from "@/hooks";
import { useSidebarContext } from "@/providers";
import { getCookie } from "cookies-next";
import { ReactElement } from "react";
import { twMerge } from "tailwind-merge";
import { AdminSidebarLinkGroup } from "./admin-sidebar-link-group";
import { AdminSidebarLinkItem } from "./admin-sidebar-link-item";
import { links, teacherLinks } from "./links";

export function AdminSidebar(): ReactElement {
  const { isCollapsed } = useSidebarContext();
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const userCredentialQuery = useUserCredential(userCredentialId || "");

  return (
    <aside
      id='sidebar'
      className={twMerge(
        "fixed inset-y-0 left-0 z-20 mt-16 flex shrink-0 flex-col border-r border-gray-300 duration-75 dark:border-gray-700 lg:flex [&>div]:rounded-none transition-all w-72 bg-white dark:bg-gray-800",
        isCollapsed && "hidden w-0"
      )}
    >
      <div className='overflow-y-auto overflow-x-hidden'>
        <div className=''>
          {userCredentialQuery.isLoading ? (
            <div className='my-4 flex flex-col gap-2'>
              {Array.from({ length: 10 }).map((_, index) => {
                return (
                  <div key={index} className='px-2'>
                    <Skeleton className=' h-[40px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
                  </div>
                );
              })}
            </div>
          ) : (
            <>
              {userCredentialQuery.data?.data.userAuthorities.find(
                (item) => item.role.roleName === Role.ROLE_ADMIN
              ) && (
                <div className='border-t border-gray-200 pt-3 first:mt-0 first:border-t-0 first:pt-0 dark:border-gray-700'>
                  {links.map((item) => {
                    if (item.subLink && item.subLink.length > 0)
                      return (
                        <AdminSidebarLinkGroup key={item.title} item={item} />
                      );
                    return (
                      <AdminSidebarLinkItem item={item} key={item.title} />
                    );
                  })}
                </div>
              )}

              {userCredentialQuery.data?.data.userAuthorities.find(
                (item) => item.role.roleName === Role.ROLE_TEACHER
              ) && (
                <div className='border-t border-gray-200 pt-3 first:mt-0 first:border-t-0 first:pt-0 dark:border-gray-700'>
                  {teacherLinks.map((item) => {
                    if (item.subLink && item.subLink.length > 0)
                      return (
                        <AdminSidebarLinkGroup key={item.title} item={item} />
                      );
                    return (
                      <AdminSidebarLinkItem item={item} key={item.title} />
                    );
                  })}
                </div>
              )}
            </>
          )}
        </div>
      </div>
    </aside>
  );
}
