import { ROUTES } from "@/constants";
import {
  IconBook,
  IconBooks,
  IconBrandDatabricks,
  IconBrowser,
  IconChartLine,
  IconFlipVertical,
  IconMapMinus,
  IconNews,
  IconRobot,
  IconTicket,
} from "@tabler/icons-react";
import {
  RectangleHorizontalIcon,
  ShoppingCartIcon,
  UserIcon,
} from "lucide-react";

export interface ILink {
  title: string;
  href: string;
  description: string;
  icon?: any;
  subLink?: {
    title: string;
    href: string;
  }[];
}

export const links: ILink[] = [
  // Admin
  {
    title: "Tổng quan",
    href: ROUTES.ADMIN_DASHBOARD,
    description: "",
    icon: IconChartLine,
  },
  {
    title: "Quản lý người dùng",
    href: ROUTES.ADMIN_USER_MANAGE,
    description:
      "Displays an indicator showing the completion progress of a task, typically displayed as a progress bar.",
    icon: UserIcon,
  },
  {
    title: "Quản lý tin tức",
    href: ROUTES.ADMIN_NEWS_MANAGE,
    description:
      "Displays an indicator showing the completion progress of a task, typically displayed as a progress bar.",
    icon: IconNews,
    subLink: [
      {
        title: "Danh mục",
        href: ROUTES.ADMIN_NEWS_CATEGORY_MANAGE,
      },
      {
        title: "Tin tức",
        href: ROUTES.ADMIN_NEWS_MANAGE,
      },
    ],
  },
  {
    title: "Quản lý bài thi",
    href: ROUTES.ADMIN_TEST_MANAGE,
    description:
      "A modal dialog that interrupts the user with important content and expects a response.",
    icon: IconBook,
    subLink: [
      {
        title: "Danh mục",
        href: ROUTES.ADMIN_TEST_CATEGORY_MANAGE,
      },
      {
        title: "Bài thi",
        href: ROUTES.ADMIN_TEST_MANAGE,
      },
    ],
  },
  {
    title: "Quản lý khóa học",
    href: ROUTES.ADMIN_COURSE_MANAGE,
    description:
      "For sighted users to preview content available behind a link.",
    icon: IconBooks,
    subLink: [
      {
        title: "Danh mục",
        href: ROUTES.ADMIN_COURSE_CATEGORY_MANAGE,
      },
      {
        title: "Khóa học",
        href: ROUTES.ADMIN_COURSE_MANAGE,
      },
    ],
  },
  {
    title: "Quản lý flashcard",
    href: ROUTES.ADMIN_FLASHCARD_MANAGE,
    description:
      "For sighted users to preview content available behind a link.",
    icon: RectangleHorizontalIcon,
  },
  {
    title: "Quản lý đơn hàng",
    href: ROUTES.ADMIN_ORDER_MANAGE,
    description:
      "A modal dialog that interrupts the user with important content and expects a response.",
    icon: ShoppingCartIcon,
  },
  {
    title: "Quản lý khuyễn mãi",
    href: ROUTES.ADMIN_PROMOTION_MANAGE,
    description:
      "A modal dialog that interrupts the user with important content and expects a response.",
    icon: IconTicket,
    subLink: [
      {
        title: "Chương trình khuyễn mãi",
        href: ROUTES.ADMIN_PROMOTION_MANAGE,
      },
      // {
      //   title: "Voucher",
      //   href: ROUTES.ADMIN_VOUCHER_MANAGE,
      // },
    ],
  },
  {
    title: "Quản lý webistes",
    href: ROUTES.ADMIN_STATIC_PAGE_MANAGE,
    description:
      "For sighted users to preview content available behind a link.",
    icon: IconBrowser,
    subLink: [
      {
        title: "Quản lý trang tĩnh",
        href: ROUTES.ADMIN_STATIC_PAGE_MANAGE,
      },
      {
        title: "Quản lý banner",
        href: ROUTES.ADMIN_BANNER_MANAGE,
      },
    ],
  },
  {
    title: "Quản lý chatbot",
    href: ROUTES.ADMIN_CHAT_BOT_MANAGE,
    description:
      "For sighted users to preview content available behind a link.",
    icon: IconRobot,
    subLink: [
      {
        title: "Agent",
        href: ROUTES.ADMIN_SKILLED_AGENT_MANAGE,
      },
      {
        title: "Test suite",
        href: ROUTES.ADMIN_TEST_SUITE_MANAGE,
      },
    ],
  },
];

export const teacherLinks: ILink[] = [
  {
    title: "Quản lý tin tức",
    href: ROUTES.ADMIN_NEWS_MANAGE,
    description:
      "Displays an indicator showing the completion progress of a task, typically displayed as a progress bar.",
    icon: IconMapMinus,
  },
  {
    title: "Quản lý bài thi",
    href: ROUTES.ADMIN_TEST_MANAGE,
    description:
      "A modal dialog that interrupts the user with important content and expects a response.",
    icon: IconBrandDatabricks,
  },
  {
    title: "Quản lý khóa học",
    href: ROUTES.ADMIN_COURSE_MANAGE,
    description:
      "For sighted users to preview content available behind a link.",
    icon: IconBooks,
  },
  {
    title: "Quản lý flashcard",
    href: ROUTES.ADMIN_FLASHCARD_MANAGE,
    description:
      "For sighted users to preview content available behind a link.",
    icon: IconFlipVertical,
  },
];
