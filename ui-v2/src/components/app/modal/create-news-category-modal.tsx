import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { ICreateNewsCategoryRequest } from "@/types";
import { IconCategory2, IconLoader2 } from "@tabler/icons-react";
import { useCreateNewsCategory } from "@/hooks";

const formSchema = z.object({
  newsCategoryName: z.string().trim().min(1, {
    message: "Danh mục bài viết là bắt buộc",
  }),
});

export const CreateNewsCategoryModal: React.FC<{}> = () => {
  const { mutateAsync: handleCreateNewsCategory, isPending } =
    useCreateNewsCategory();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      newsCategoryName: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = { ...values } as ICreateNewsCategoryRequest;
    await handleCreateNewsCategory(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className='cursor-pointer'
          onSelect={(e) => e.preventDefault()}
        >
          <div>Tạo danh mục bài viết</div>
          <DropdownMenuShortcut>
            <IconCategory2 className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='overflow-y-auto max-h-full '>
        <DialogHeader>
          <DialogTitle>Thêm danh mục bài viết mới</DialogTitle>
          <DialogDescription>
            Tạo thêm danh mục bài viết. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='newsCategoryName'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='courseCategoryName'>
                        Danh mục bài viết *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='courseCategoryName'
                          placeholder='Nhập tên danh mục khóa học'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
