import { useCreateTestCategory, useUpdateTestCategory } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  ITestCategoryRequest,
  ITestCategoryResponse,
  IUpdateTestCategoryRequest,
} from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { IconCategory2, IconLoader2 } from "@tabler/icons-react";
import { Pencil1Icon } from "@radix-ui/react-icons";

const formSchema = z.object({
  id: z.string().trim().min(1),
  testCategoryName: z.string().trim().min(1, {
    message: "Tên danh mục bài thi là bắt buộc",
  }),
  numberOfPart: z.number().min(1, {
    message: "Số phần thi là bắt buộc",
  }),
  totalQuestion: z.number().min(1, {
    message: "Số câu hỏi là bắt buộc",
  }),
  totalTime: z.number().min(1, {
    message: "Tổng thời gian là bắt buộc",
  }),
});

export const UpdateTestCategoryModal: React.FC<{
  category: ITestCategoryResponse;
}> = ({ category }) => {
  const { mutateAsync: handleUpdateTestCategory, isPending } =
    useUpdateTestCategory();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: category.id,
      testCategoryName: category.testCategoryName,
      numberOfPart: category.numberOfPart,
      totalQuestion: category.totalQuestion,
      totalTime: category.totalTime,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = { ...values } as IUpdateTestCategoryRequest;
    console.log({ data });
    await handleUpdateTestCategory(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className='cursor-pointer'
        >
          <div>Chỉnh sửa danh mục bài thi</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[600px] overflow-y-auto max-h-full'>
        <DialogHeader>
          <DialogTitle>Chỉnh sửa danh mục bài thi</DialogTitle>
          <DialogDescription>
            Chỉnh sửa danh mục bài thi. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='testCategoryName'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='testName'>
                        Danh mục đề thi *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='testCategoryName'
                          placeholder='Nhập danh mục đê thi'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='numberOfPart'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='testDescription'>
                        Số lượng phần thi *
                      </FormLabel>
                      <FormControl>
                        <Input
                          id='numberOfPart'
                          type='number'
                          placeholder='Số lượng phần thi'
                          className='col-span-3'
                          onChange={(e) => {
                            form.setValue("numberOfPart", +e.target.value);
                          }}
                          value={form.getValues("numberOfPart")}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='totalQuestion'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='thumbnail'>
                        Số lượng câu hỏi *
                      </FormLabel>
                      <FormControl>
                        <Input
                          id='totalQuestion'
                          placeholder='Tổng số lượng câu hỏi'
                          className='col-span-3'
                          type='number'
                          onChange={(e) => {
                            form.setValue("totalQuestion", +e.target.value);
                          }}
                          value={form.getValues("totalQuestion")}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='totalTime'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='totalTime'>
                        Tổng thời gian *
                      </FormLabel>
                      <FormControl>
                        <Input
                          id='totalTime'
                          placeholder='Nhập tổng thời gian'
                          className='col-span-3'
                          type='number'
                          onChange={(e) => {
                            form.setValue("totalTime", +e.target.value);
                          }}
                          value={form.getValues("totalTime")}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
