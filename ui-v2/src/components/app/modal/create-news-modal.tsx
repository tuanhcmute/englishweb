import { useCreateNews, useNewsCategories } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ICreateNewsRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
  Skeleton,
} from "@/components/ui";

const formSchema = z.object({
  status: z.string().default(NewsType.APPROVED),
  newsCategoryId: z.string().trim().min(1, {
    message: "Danh mục bài viết là bắt buộc",
  }),
  newsTitle: z.string().trim().min(1, {
    message: "Tiêu đề bài viết là bắt buộc",
  }),
  newsDescription: z.string().trim().min(1, {
    message: "Mô tả bài viết là bắt buộc",
  }),
  newsImage: z.string(),
  userCredentialId: z.string().trim().min(1),
});

import dynamic from "next/dynamic";
import { IconLoader2, IconNews } from "@tabler/icons-react";
import { NewsType } from "@/enums";
import { getCookie } from "cookies-next";
import { USER_CREDENTIAL_ID } from "@/constants";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

export const CreateNewsModal: React.FC<{}> = () => {
  const [selectedImage, setSelectedImage] = useState<any>(undefined);
  const newsCategoriesQuery = useNewsCategories();
  const { mutateAsync: handleCreateNews, isPending } = useCreateNews();
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      newsImage: undefined,
      newsTitle: "",
      status: NewsType.APPROVED,
      newsDescription: "",
      userCredentialId: userCredentialId || "",
      newsCategoryId: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = {
      ...values,
    } as ICreateNewsRequest;
    console.log({ requestData });
    await handleCreateNews(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className='cursor-pointer'
        >
          <div>Thêm bài viết</div>
          <DropdownMenuShortcut>
            <IconNews className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[600px] overflow-y-auto max-h-full'>
        <DialogHeader>
          <DialogTitle>Thêm bài viết mới</DialogTitle>
          <DialogDescription>
            Tạo thêm bài viết. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='newsCategoryId'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='newsCategoryId'>
                        Danh mục bài viết *
                      </FormLabel>
                      <FormControl>
                        <Select
                          onValueChange={(value: string) => {
                            form.setValue("newsCategoryId", value);
                          }}
                        >
                          <SelectTrigger className='w-full'>
                            <SelectValue placeholder='Lựa chọn danh mục bài thi' />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectLabel>Danh mục bài viết</SelectLabel>
                              {newsCategoriesQuery.isLoading ? (
                                <SelectItem value='no-item'>
                                  <Skeleton className='h-[30px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 mt-4' />
                                </SelectItem>
                              ) : (
                                newsCategoriesQuery.data?.data.map((item) => {
                                  return (
                                    <SelectItem value={item.id} key={item.id}>
                                      {item.newsCategoryName}
                                    </SelectItem>
                                  );
                                })
                              )}
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='newsTitle'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='newsTitle'>
                        Tiêu đề bài viết *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='newsTitle'
                          placeholder='Nhập tên đề bài viết'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='newsDescription'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='newsDescription'>
                        Mô tả bài viết *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='newsDescription'
                          placeholder='Nhập mô tả bài viết'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='status'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='percentDiscount'>
                        Trạng thái hiển thị
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("status")}
                          onValueChange={(value: string) => {
                            form.setValue("status", value);
                          }}
                        >
                          <SelectTrigger className='w-full'>
                            <SelectValue placeholder='Trạng thái' />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectItem value={NewsType.APPROVED}>
                                Hiển thị
                              </SelectItem>
                              <SelectItem value={NewsType.REJECTED}>
                                Ẩn
                              </SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='newsImage'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='newsImage'>
                        Hình ảnh *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("newsImage")}
                          handleChange={({ data }) => {
                            form.setValue("newsImage", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              {selectedImage && (
                <div className='grid grid-cols-1 gap-4'>
                  <img
                    src={URL.createObjectURL(selectedImage)}
                    alt='courseImg'
                    className='w-96'
                    height={100}
                  />
                </div>
              )}
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
