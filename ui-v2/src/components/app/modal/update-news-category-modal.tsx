import { useUpdateNewsCategory } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { INewsCategoryResponse, IUpdateNewsCategoryRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { IconEdit, IconLoader2 } from "@tabler/icons-react";
import { NewsCategoryType, NewsType } from "@/enums";

const formSchema = z.object({
  status: z.string().default(NewsCategoryType.VISIBLE),
  id: z.string().trim().min(1),
  newsCategoryName: z.string().trim().min(1, {
    message: "Tiêu đề danh mục là bắt buộc",
  }),
});

export const UpdateNewsCatgoryModal: React.FC<{
  category: INewsCategoryResponse;
}> = ({ category }) => {
  console.log({ category });
  const { mutateAsync: handleUpdateNewsCategory, isPending } =
    useUpdateNewsCategory();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: category.id,
      newsCategoryName: category.newsCategoryName,
      status: category.status,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = {
      ...values,
    } as IUpdateNewsCategoryRequest;
    console.log({ requestData });
    await handleUpdateNewsCategory(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Chỉnh sửa danh mục bài viết</div>
          <DropdownMenuShortcut>
            <IconEdit className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa danh mục bài viết</DialogTitle>
          <DialogDescription>
            Chỉnh sửa danh mục bài viết. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="newsCategoryName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="newsTitle">Tiêu đề *</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="newsTitle"
                          placeholder="Nhập tên đề bài viết"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="status"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="percentDiscount">
                        Trạng thái hiển thị
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("status")}
                          onValueChange={(value: string) => {
                            form.setValue("status", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Trạng thái" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectItem value={NewsCategoryType.VISIBLE}>
                                Hiển thị
                              </SelectItem>
                              <SelectItem value={NewsCategoryType.INVISIBLE}>
                                Ẩn
                              </SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white flex items-center gap-1"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
