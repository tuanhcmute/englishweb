import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui";
import { PlusIcon } from "@radix-ui/react-icons";
import { CreateUserCredentialForm } from "@/components/app/forms";

export const CreateUserCredentialModal = () => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant="outline"
          size="sm"
          className="flex items-center h-8 gap-1 bg-white dark:bg-transparent dark:hover:bg-purple-800 border-purple-800 text-purple-800 border hover:bg-purple-800 hover:text-white dark:text-white"
        >
          <PlusIcon />
          Tạo người dùng mới
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Thêm người dùng mới</DialogTitle>
          <DialogDescription>
            Tạo thêm người dùng. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <CreateUserCredentialForm />
      </DialogContent>
    </Dialog>
  );
};
