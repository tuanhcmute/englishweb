import { useUpdateTest } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ITestResponse, IUpdateTestRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Textarea,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import { Pencil1Icon } from "@radix-ui/react-icons";
import dynamic from "next/dynamic";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  id: z.string().trim().min(1),
  testName: z.string().trim().min(1, {
    message: "Tên bài thi là bắt buộc",
  }),
  testDescription: z.string(),
  thumbnail: z.string(),
});

export const UpdateTestModal: React.FC<{ test: ITestResponse }> = ({
  test,
}) => {
  const { mutateAsync: handleUpdateTest, isPending } = useUpdateTest();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: test.id,
      testName: test.testName,
      testDescription: test.testDescription,
      thumbnail: test.thumbnail,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = { ...values } as IUpdateTestRequest;
    console.log({ data });
    await handleUpdateTest(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Chỉnh sửa bài thi</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa bài thi</DialogTitle>
          <DialogDescription>
            Chỉnh sửa bài thi. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="testName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="testName">Tên đề thi *</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="testName"
                          placeholder="Nhập tên đê thi"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="testDescription"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="testDescription">
                        Mô tả đề thi
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          {...field}
                          id="testDescription"
                          placeholder="Nhập mô tả đề thi"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="thumbnail"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="thumbnail">
                        Hình ảnh *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("thumbnail")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("thumbnail", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
                onClick={() => console.log(form.getValues())}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
