import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import { useCreateLLMModel, useLLMModelModules } from "@/hooks";
import { Pencil1Icon } from "@radix-ui/react-icons";
import Select, { ActionMeta } from "react-select";
import makeAnimated from "react-select/animated";
import { ICreateLLMModelRequest } from "@/types";
import { toast } from "react-hot-toast";

const animatedComponents = makeAnimated();

const formSchema = z
  .object({
    llm_model_name: z.string().trim().min(1),
    param: z.string().trim().min(1),
  })
  .refine(
    (data) => {
      try {
        // Attempt to parse the JSON string
        console.log({ param: data.param });
        JSON.parse(data.param);
        return true;
      } catch (error) {
        return false;
      }
    },
    {
      message: "Param must be json format",
      path: ["param"],
    }
  );

export const CreateLLMModelModal: React.FC<{}> = () => {
  const llmModelModulesQuery = useLLMModelModules();
  const { mutateAsync: handleCreateLLMModel, isPending } = useCreateLLMModel();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      llm_model_name: "",
      param: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      const jsonValue = JSON.parse(values.param);
      const jsonValueStr = JSON.stringify(jsonValue, null, 4);
      const requestData = {
        ...values,
        param: jsonValueStr,
      } as ICreateLLMModelRequest;
      await handleCreateLLMModel(requestData);
    } catch (e) {
      toast.error("Can not convert param to json format");
    }
  }

  // [newValue: MultiValue<unknown>, actionMeta: ActionMeta<unknown>]
  const handleSelect = ({
    newValue,
  }: {
    newValue: { label: string; value: string };
    actionMeta: ActionMeta<{ label: string; value: string }>;
  }) => {
    form.setValue("llm_model_name", newValue.value);
  };

  const loadModelOptions = () => {
    if (!llmModelModulesQuery.data) return [];
    return llmModelModulesQuery.data.map((item) => {
      return {
        label: item.llm_model_name,
        value: item.id,
      };
    });
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>New LLM Model</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="overflow-y-auto max-h-full max-w-full w-4/5">
        <DialogHeader>
          <DialogTitle>New LLM Model</DialogTitle>
          <DialogDescription>
            New LLM Model. Press the save button to save the data
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="param"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel htmlFor="param">
                        Setup message (Only accept json format) *
                      </FormLabel>
                      <FormControl>
                        <Input
                          id="param"
                          placeholder="Type a param"
                          {...field}
                          // onChange={handleChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="llm_model_name"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel htmlFor="llm_model_ids">
                        LLM model module *
                      </FormLabel>
                      <Select
                        className="text-sm"
                        components={animatedComponents}
                        options={loadModelOptions()}
                        placeholder="Select LLM model module"
                        onChange={(...e) =>
                          handleSelect({
                            newValue: e[0] as {
                              label: string;
                              value: string;
                            },
                            actionMeta: e[1] as ActionMeta<{
                              label: string;
                              value: string;
                            }>,
                          })
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Save
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
