import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui";
import { useReportComments } from "@/hooks";
import { ICommentResponse } from "@/types";
import { Pencil1Icon } from "@radix-ui/react-icons";
import React from "react";

export const GetReportCommentsModal: React.FC<{
  comment: ICommentResponse;
}> = ({ comment }) => {
  const reportCommentsQuery = useReportComments({ commentId: comment.id });

  console.log({ data: reportCommentsQuery.data?.data });

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Xem danh sách báo cáo</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle className="text-center">
            Báo cáo bình luận: {comment.id}
          </DialogTitle>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-1 gap-4">
            <Table>
              <TableHeader>
                <TableRow>
                  <TableHead className="w-[100px]">Người báo cáo</TableHead>
                  <TableHead>Nội dung báo cáo</TableHead>
                </TableRow>
              </TableHeader>
              <TableBody>
                {reportCommentsQuery.data?.data.map((item) => {
                  return (
                    <TableRow key={item.id}>
                      <TableCell>
                        <div className="flex items-center gap-1">
                          {item.userCredential?.avatar}
                          {item.userCredential?.username}
                        </div>
                      </TableCell>
                      <TableCell>{item.reportContent}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
};
