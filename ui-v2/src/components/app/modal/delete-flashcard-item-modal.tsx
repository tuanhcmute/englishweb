import { useDeleteFlashcardItem } from "@/hooks";
import React from "react";
import { Button } from "@/components/ui";
import Confirm from "./confirm";

export const DeleteFlashcardItemModal: React.FC<{
  flashcardItemId: string;
}> = ({ flashcardItemId }) => {
  const { mutateAsync: handleDeleteFlashcardItem } = useDeleteFlashcardItem();

  return (
    <Confirm
      description='Thao tác này không thể hoàn tác. Từ sẽ bị xóa vĩnh viễn. Nhấn hủy bỏ để hủy thao tác.'
      title='Xóa từ vựng'
      textConfirm='Xóa'
      onConfirm={async (close) => {
        await handleDeleteFlashcardItem(flashcardItemId);
        close?.();
      }}
    >
      <Button variant='link' className='p-0 italic text-red-500'>
        Xóa từ này
      </Button>
    </Confirm>
  );
};
