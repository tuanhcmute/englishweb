import { Button } from "@/components/ui";
import { useUpdatePartOfTest } from "@/hooks/use-update-part-of-test";
import { Pencil2Icon, UpdateIcon } from "@radix-ui/react-icons";
import Confirm from "./confirm";

export const UpdatePartOfTestModal: React.FC<{ testId: string }> = ({
  testId,
}) => {
  const { mutateAsync: handleUpdatePartOfTest } = useUpdatePartOfTest();
  return (
    <Confirm
      title='Cập nhật danh sách phần thi'
      description='Cập nhật danh sách phần thi. Danh sách phần thi có thể sẽ bị thay đổi.'
      textConfirm='Đồng ý'
      onConfirm={async (close) => {
        await handleUpdatePartOfTest(testId);
        close?.();
      }}
    >
      <Button
        variant='outline'
        size='sm'
        className='h-8 lg:flex gap-1 items-center bg-transparent text-purple-800 hover:bg-purple-800 border-purple-800 hover:text-white'
      >
        <Pencil2Icon />
        Cập nhật danh sách phần thi
      </Button>
    </Confirm>
  );
};
