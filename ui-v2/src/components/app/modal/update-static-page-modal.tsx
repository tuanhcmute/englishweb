import { useUpdateStaicPage } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Textarea,
} from "@/components/ui";
import { UpdateIcon } from "@radix-ui/react-icons";
import dynamic from "next/dynamic";
import { IStaticPageResponse, IUpdateStaticPageRequest } from "@/types";
import { Label } from "@radix-ui/react-dropdown-menu";
import { IconLoader2 } from "@tabler/icons-react";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  pageCode: z.string().trim().min(1, {
    message: "Mã trang là bắt buộc",
  }),
  pageName: z.string().trim().min(1, {
    message: "Tên trang tĩnh là bắt buộc",
  }),
  pagePreview: z.string().trim().min(1),
  pageUrlVideo: z.string().trim().min(1),
  pageContent: z.string().trim(),
  id: z.string().trim().min(1),
});

export const UpdateStaticPageModal: React.FC<{
  staticPage: IStaticPageResponse;
}> = ({ staticPage }) => {
  const [contentPreview, setContentPreview] = useState<string>(
    staticPage.pageContent || ""
  );
  const { mutateAsync: handleUpdateStaticPage, isPending } =
    useUpdateStaicPage();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      pageCode: staticPage.pageCode,
      pageName: staticPage.pageName,
      pageContent: staticPage.pageContent,
      id: staticPage.id,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateStaticPageRequest;
    await handleUpdateStaticPage(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant="outline"
          size="sm"
          className="h-8 gap-1 items-center bg-purple-800 text-white hover:bg-purple-900 hover:text-white"
        >
          <UpdateIcon />
          Chỉnh sửa trang tĩnh
        </Button>
      </DialogTrigger>
      <DialogContent className="max-w-full w-4/5 max-h-full overflow-y-auto">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa trang tĩnh</DialogTitle>
          <DialogDescription>
            Chỉnh sửa trang tĩnh. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  name="pageCode"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="pageCode">
                        Mã trang tĩnh *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="pageCode"
                          placeholder="Nhập mã trang tĩnh"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="pageName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="pageName">
                        Tên trang tĩnh *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="pageName"
                          placeholder="Nhập tên trang tĩnh"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="pageUrlVideo"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="pageUrlVideo">
                        Url video (Youtube) *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="pageUrlVideo"
                          placeholder="Nhập tên địa chỉ video: (tDESZpWW3vM)"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="pagePreview"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="pagePreview">
                        Nội dung xem trước *
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          {...field}
                          id="pagePreview"
                          placeholder="Nhập tên nội dung xem trước"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-2 gap-4">
                <FormField
                  control={form.control}
                  name="pageContent"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="pageContent">
                        Nội dung
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("pageContent")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("pageContent", data);
                            setContentPreview(data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <div>
                  <Label>Xem trước</Label>
                  <div
                    dangerouslySetInnerHTML={{ __html: contentPreview }}
                  ></div>
                </div>
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
