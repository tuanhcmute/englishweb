import {
  Button,
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
} from "@/components/ui";
import { useDeleteFlashcard } from "@/hooks";
import { IFlashcardResponse } from "@/types";
import { IconLoader2, IconTrash } from "@tabler/icons-react";
import React from "react";

export const RemoveFlashcardModal: React.FC<{
  flashcard: IFlashcardResponse;
}> = ({ flashcard }) => {
  const { mutateAsync: handleDeleteFlashcard, isPending } =
    useDeleteFlashcard();

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer flex items-center gap-1"
        >
          <div>Xóa flashcard</div>
          <DropdownMenuShortcut>
            <IconTrash className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Xóa flashcard</DialogTitle>
          <DialogDescription>
            Sau khi xóa, dữ liệu sẽ bị mất, nhấn hủy để dừng thao tác này
          </DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button
            variant={"destructive"}
            disabled={isPending}
            onClick={() => handleDeleteFlashcard(flashcard.id)}
          >
            {isPending && <IconLoader2 className="animate-spin" />}
            Xóa
          </Button>
          <DialogClose asChild>
            <Button
              type="button"
              variant="default"
              className="bg-purple-800 hover:bg-purple-900 text-white hover:text-white"
            >
              Hủy bỏ
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
