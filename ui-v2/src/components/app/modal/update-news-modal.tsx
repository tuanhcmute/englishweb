import { useUpdateNews } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { INewsResponse, IUpdateNewsRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";

const formSchema = z.object({
  status: z.string().default(NewsType.APPROVED),
  id: z.string().trim().min(1, {
    message: "Mã bài viết là bắt buộc",
  }),
  newsTitle: z.string().trim().min(1, {
    message: "Tiêu đề bài viết là bắt buộc",
  }),
  newsDescription: z.string().trim().min(1, {
    message: "Mô tả bài viết là bắt buộc",
  }),
  newsImage: z.string(),
});

import dynamic from "next/dynamic";
import { IconEdit, IconLoader2 } from "@tabler/icons-react";
import { NewsType } from "@/enums";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

export const UpdateNewsModal: React.FC<{ news: INewsResponse }> = ({
  news,
}) => {
  const { mutateAsync: handleUpdateNews, isPending } = useUpdateNews();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: news.id,
      newsImage: news.newsImage,
      newsTitle: news.newsTitle,
      status: news.status,
      newsDescription: news.newsDescription,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = {
      ...values,
      newsContentHtml: "",
    } as IUpdateNewsRequest;
    console.log({ requestData });
    await handleUpdateNews(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Chỉnh sửa bài viết</div>
          <DropdownMenuShortcut>
            <IconEdit className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa bài viết</DialogTitle>
          <DialogDescription>
            Chỉnh sửa bài viết. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="newsTitle"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="newsTitle">
                        Tiêu đề bài viết *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="newsTitle"
                          placeholder="Nhập tên đề bài viết"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="newsDescription"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="newsDescription">
                        Mô tả bài viết *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="newsDescription"
                          placeholder="Nhập mô tả bài viết"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="status"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="percentDiscount">
                        Trạng thái hiển thị
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("status")}
                          onValueChange={(value: string) => {
                            form.setValue("status", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Trạng thái" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectItem value={NewsType.APPROVED}>
                                Hiển thị
                              </SelectItem>
                              <SelectItem value={NewsType.REJECTED}>
                                Ẩn
                              </SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="newsImage"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="newsImage">
                        Hình ảnh *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("newsImage")}
                          handleChange={({ data }) => {
                            form.setValue("newsImage", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
                onClick={() => console.log(form.getValues())}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
