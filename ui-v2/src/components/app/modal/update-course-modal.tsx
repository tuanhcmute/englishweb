import { useCourseCategories, useUpdateCourse } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ICourseResponse, IUpdateCourseRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { Pencil1Icon } from "@radix-ui/react-icons";

const formSchema = z.object({
  id: z.string().trim().min(1),
  isActive: z.string().default("1"),
  courseName: z.string().trim().min(1, {
    message: "Tên khóa học là bắt buộc",
  }),
  oldPrice: z.number().min(1, {
    message: "Số tiền cũ là bắt buộc",
  }),
  courseImg: z.string(),
});

import dynamic from "next/dynamic";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

export const UpdateCourseModal: React.FC<{ course: ICourseResponse }> = ({
  course,
}) => {
  const [newPricePreview, setNewPricePreview] = useState<number>(
    course.newPrice
  );
  const { mutateAsync: handleUpdateCourse } = useUpdateCourse();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: course.id,
      courseImg: course.courseImg,
      courseName: course.courseName,
      isActive: course.isActive ? "1" : "0",
      oldPrice: course.oldPrice,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const isActive = values.isActive === "1";
    const data = {
      ...values,
      isActive,
    } as IUpdateCourseRequest;
    console.log({ data });
    await handleUpdateCourse(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer flex items-center gap-1"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Chỉnh sửa khóa học</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa khóa học</DialogTitle>
          <DialogDescription>
            Chỉnh sửa khóa học. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="courseName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="courseName">
                        Tên khóa học *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="courseName"
                          placeholder="Nhập tên khóa học"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="oldPrice"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="oldPrice">
                        Giá tiền(VNĐ)
                      </FormLabel>
                      <FormControl>
                        <Input
                          type="number"
                          placeholder="Nhập giá tiền"
                          className="col-span-3"
                          onChange={(e) => {
                            form.setValue("oldPrice", +e.target.value);
                          }}
                          value={form.getValues("oldPrice")}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="isActive"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="percentDiscount">
                        Trạng thái hiển thị
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("isActive")}
                          onValueChange={(value: string) => {
                            form.setValue("isActive", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Lựa chọn danh mục bài thi" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectLabel>Danh mục khóa học</SelectLabel>
                              <SelectItem value="1">Hiển thị</SelectItem>
                              <SelectItem value="0">Ẩn</SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="courseImg"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="courseImg">
                        Hình ảnh *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("courseImg")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("courseImg", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-500 hover:bg-green-600 text-white hover:text-white"
              >
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
