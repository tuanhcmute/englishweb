import { useCreateFlashcardItemByKeyword } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ICreateFlashcardItemByKeywordRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { PlusIcon } from "@radix-ui/react-icons";
import { Loading } from "@/components/shared/loading";
import { IconLoader2 } from "@tabler/icons-react";
import { cn } from "@/lib";

const formSchema = z.object({
  keyword: z.string().default("1"),
  flashcardId: z.string().trim().min(1, {
    message: "Từ mới là bắt buộc",
  }),
  limit: z.number().min(10).default(10),
});

export const CreateAutoFlashcardItemModal: React.FC<{
  flashcardId?: string;
}> = ({ flashcardId }) => {
  const { mutateAsync: handleCreateFlashcardItemByKeyword, isPending } =
    useCreateFlashcardItemByKeyword();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      flashcardId: flashcardId,
      keyword: "",
      limit: 10,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as ICreateFlashcardItemByKeywordRequest;
    console.log({ requestData: data });
    await handleCreateFlashcardItemByKeyword(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='default'
          size='sm'
          className='h-8 lg:flex gap-1 items-center bg-green-500 hover:bg-green-700 text-[13px]'
        >
          <PlusIcon />
          Thêm từ theo từ khóa
        </Button>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[600px] overflow-y-auto max-h-full'>
        <DialogHeader>
          <DialogTitle>Thêm từ theo từ khóa</DialogTitle>
          <DialogDescription>
            Thêm từ theo từ khóa. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='keyword'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='keyword'>
                        Nhập từ khóa tiếng anh liên quan *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='keyword'
                          placeholder='Nhập từ mới'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='limit'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='limit'>Giới hạn từ</FormLabel>
                      <FormControl>
                        <Input
                          id='limit'
                          placeholder='Nhập giới hạn từ'
                          className='col-span-3'
                          type='number'
                          onChange={(e) => {
                            form.setValue("limit", +e.target.value);
                          }}
                          value={form.getValues("limit")}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className={cn(
                  "bg-transparent border-green-700 text-green-700 border hover:bg-green-700 hover:text-white"
                )}
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
