import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { IconLoader2, IconTicket } from "@tabler/icons-react";
import { useCreateDiscount } from "@/hooks";
import { ICreateDiscountRequest } from "@/types";
import { DiscountStatus } from "@/enums";

const formSchema = z.object({
  discountName: z.string().trim().min(1, {
    message: "Tên khóa học là bắt buộc",
  }),
  startDate: z.string().trim().min(1, {
    message: "Thời gian bắt đầu khuyễn mãi là bắt buộc",
  }),
  endDate: z.string().trim().min(1, {
    message: "Thời gian kết thúc khuyễn mãi là bắt buộc",
  }),
  status: z.string().trim().min(1, {
    message: "Trạng thái là bắt buộc",
  }),
});

export const CreateDiscountModal: React.FC<{}> = () => {
  const { mutateAsync: handleCreateDiscount, isPending } = useCreateDiscount();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      discountName: "",
      startDate: "",
      endDate: "",
      status: DiscountStatus.SCHEDULED,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as ICreateDiscountRequest;
    console.log({ requestData: data });
    await handleCreateDiscount(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Thêm chương trình khuyến mãi</div>
          <DropdownMenuShortcut>
            <IconTicket className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="max-w-full w-4/5 overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Thêm chương trình khuyến mãi</DialogTitle>
          <DialogDescription>
            chương trình khuyến mãi. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="discountName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="discountName">
                        Tiêu đề (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="discountName"
                          placeholder="Nhập tên chương trình khuyến mãi"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              {/* <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="status"
                  render={() => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="status">
                        Trạng thái (*)
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("status")}
                          onValueChange={(value: string) => {
                            form.setValue("status", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Trạng thái" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectItem value={DiscountStatus.SCHEDULED}>
                                Lên lịch
                              </SelectItem>
                              <SelectItem value={DiscountStatus.RUNNING}>
                                Đang hoạt động
                              </SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div> */}
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="startDate"
                  render={() => (
                    <FormItem className="flex flex-col">
                      <FormLabel>Ngày bắt đầu giảm</FormLabel>
                      <FormControl>
                        <Input
                          type="datetime-local"
                          placeholder="Nhập ngày bắt đầu giảm giá"
                          className="col-span-3"
                          onChange={(e) => {
                            console.log(e.target.value);
                            form.setValue("startDate", e.target.value);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="endDate"
                  render={() => (
                    <FormItem className="flex flex-col">
                      <FormLabel>Ngày kết thúc giảm giá</FormLabel>
                      <FormControl>
                        <Input
                          type="datetime-local"
                          placeholder="Nhập ngày kết thúc giảm giá"
                          className="col-span-3"
                          onChange={(e) => {
                            console.log(e.target.value);
                            form.setValue("endDate", e.target.value);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
