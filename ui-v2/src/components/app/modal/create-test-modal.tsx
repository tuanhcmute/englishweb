import { useCreateTest, useTestCategories } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ITestRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
  Textarea,
} from "@/components/ui";
import { IconLoader2, IconTextGrammar } from "@tabler/icons-react";
import dynamic from "next/dynamic";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  testCategoryId: z.string().trim().min(1, {
    message: "Danh mục bài thi là bắt buộc",
  }),
  testName: z.string().trim().min(1, {
    message: "Tên bài thi là bắt buộc",
  }),
  testDescription: z.string(),
  numberOfPart: z.number().min(1, {
    message: "Số phần thi là bắt buộc",
  }),
  numberOfQuestion: z.number(),
  thumbnail: z.string(),
});

export const CreateTestModal: React.FC<{}> = () => {
  const testCategoriesQuery = useTestCategories();
  const { mutateAsync: handleCreateTest, isPending } = useCreateTest();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      testCategoryId: "",
      testName: "",
      testDescription: "",
      numberOfPart: 0,
      numberOfQuestion: 0,
      thumbnail: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = { ...values } as ITestRequest;
    await handleCreateTest(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Thêm bài thi</div>
          <DropdownMenuShortcut>
            <IconTextGrammar className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Thêm bài thi mới</DialogTitle>
          <DialogDescription>
            Tạo thêm bài thi. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  name="testCategoryId"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="testCategoryId">
                        Loại đề thi *
                      </FormLabel>
                      <FormControl>
                        <Select
                          onValueChange={(value: string) => {
                            form.setValue("testCategoryId", value);
                            form.setValue(
                              "numberOfPart",
                              testCategoriesQuery.data?.data.find(
                                (item) => item.id === value
                              )?.numberOfPart || 0
                            );
                            form.setValue(
                              "numberOfQuestion",
                              testCategoriesQuery.data?.data.find(
                                (item) => item.id === value
                              )?.totalQuestion || 0
                            );
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Lựa chọn danh mục bài thi" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectLabel>Bài thi</SelectLabel>
                              {testCategoriesQuery.isLoading ? (
                                <SelectItem value="no-item">
                                  Loading...
                                </SelectItem>
                              ) : (
                                testCategoriesQuery.data?.data.map((item) => {
                                  return (
                                    <SelectItem value={item.id} key={item.id}>
                                      {item.testCategoryName}
                                    </SelectItem>
                                  );
                                })
                              )}
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="testName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="testName">Tên đề thi *</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="testName"
                          placeholder="Nhập tên đê thi"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="testDescription"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="testDescription">
                        Mô tả đề thi
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          {...field}
                          id="testDescription"
                          placeholder="Nhập mô tả đề thi"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="thumbnail"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="thumbnail">
                        Hình ảnh *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("thumbnail")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("thumbnail", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
