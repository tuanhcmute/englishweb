import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui";
import {
  useSkilledAgentVersions,
  useUpdateStatusSkilledAgentVersion,
} from "@/hooks";
import { Pencil1Icon } from "@radix-ui/react-icons";
import Select, { ActionMeta, MultiValue } from "react-select";
import makeAnimated from "react-select/animated";
import json from "highlight.js/lib/languages/json";
import hljs from "highlight.js";
import { useParams } from "next/navigation";
import { IUpdateStatusSkilledAgentVersionRequest } from "@/types";
import { IconLoader2 } from "@tabler/icons-react";

// Then register the languages you need
hljs.registerLanguage("json", json);

const animatedComponents = makeAnimated();

const formSchema = z.object({
  skilled_agent_version_id: z
    .object({
      label: z.string(),
      value: z.string().trim().min(1),
    })
    .required(),
  is_active: z
    .object({
      label: z.string(),
      value: z.string().trim().min(1),
    })
    .required(),
});

export const UpdateStatusSkilledAgentVersionModal: React.FC<{}> = () => {
  const params = useParams<{ agentId: string }>();
  const skilledAgentVersionsQuery = useSkilledAgentVersions(params.agentId);
  const { mutateAsync: handleUpdateStatusSkilledAgentVersion, isPending } =
    useUpdateStatusSkilledAgentVersion();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      skilled_agent_version_id: {},
      is_active: {},
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = {
      skilled_agent_version_id: values.skilled_agent_version_id.value,
      is_active: values.is_active.value === "1",
    } as IUpdateStatusSkilledAgentVersionRequest;
    console.log({ requestData });
    await handleUpdateStatusSkilledAgentVersion(requestData);
  }

  // [newValue: MultiValue<unknown>, actionMeta: ActionMeta<unknown>]
  const handleSelectVersion = ({
    actionMeta,
    newValue,
  }: {
    newValue: MultiValue<{ label: string; value: string }>;
    actionMeta: ActionMeta<{ label: string; value: string }>;
  }) => {
    if (actionMeta.action === "select-option") {
      const item = newValue as unknown as { label: string; value: string };
      form.setValue("skilled_agent_version_id", item);
    }
  };

  const handleSelectStatus = ({
    actionMeta,
    newValue,
  }: {
    newValue: MultiValue<{ label: string; value: string }>;
    actionMeta: ActionMeta<{ label: string; value: string }>;
  }) => {
    console.log({ actionMeta, newValue });
    if (actionMeta.action === "select-option") {
      const item = newValue as unknown as { label: string; value: string };
      form.setValue("is_active", item);
    }
  };

  const loadSkilledAgentVersionOptions = () => {
    if (!skilledAgentVersionsQuery.data) return [];
    return skilledAgentVersionsQuery.data.map((item) => {
      return {
        label: `${item.version}`,
        value: item.id,
      };
    });
  };

  const loadStatusOptions = (): { label: string; value: string }[] => {
    return [
      { label: "Deactive", value: "0" },
      { label: "Active", value: "1" },
    ];
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Uupdate status Agent Version</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="overflow-y-auto max-h-full w-5/6 max-w-full">
        <DialogHeader>
          <DialogTitle>Update status Agent Version</DialogTitle>
          <DialogDescription>
            Update status Agent Version. Press the save button to save the data
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="skilled_agent_version_id"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel htmlFor="llm_model_ids">
                        Agent version *
                      </FormLabel>
                      <Select
                        className="text-sm"
                        closeMenuOnSelect
                        components={animatedComponents}
                        options={loadSkilledAgentVersionOptions()}
                        placeholder="Search agent version"
                        onChange={(...e) =>
                          handleSelectVersion({
                            newValue: e[0] as MultiValue<{
                              label: string;
                              value: string;
                            }>,
                            actionMeta: e[1] as ActionMeta<{
                              label: string;
                              value: string;
                            }>,
                          })
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="is_active"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel htmlFor="tool_ids">Status *</FormLabel>
                      <Select
                        className="text-sm"
                        closeMenuOnSelect
                        components={animatedComponents}
                        options={loadStatusOptions()}
                        placeholder="Update status"
                        onChange={(...e) =>
                          handleSelectStatus({
                            newValue: e[0] as MultiValue<{
                              label: string;
                              value: string;
                            }>,
                            actionMeta: e[1] as ActionMeta<{
                              label: string;
                              value: string;
                            }>,
                          })
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                // disabled={isPending}
                onClick={() => console.log(form.getValues())}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Save
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
