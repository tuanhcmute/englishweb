import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { useUpdateDiscount } from "@/hooks";
import { IDiscountResponse, IUpdateDiscountRequest } from "@/types";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { DiscountStatus } from "@/enums";
const moment = require("moment");

const formSchema = z.object({
  id: z.string().trim(),
  discountName: z.string().trim().min(1, {
    message: "Tên khóa học là bắt buộc",
  }),
  startDate: z.string().trim().min(1, {
    message: "Thời gian bắt đầu giảm giá là bắt buộc",
  }),
  endDate: z.string().trim().min(1, {
    message: "Thời gian kết thúc giảm giá là bắt buộc",
  }),
  status: z.string().min(1, {
    message: "Trạng thái là bắt buộc",
  }),
});

export const UpdateDiscountModal: React.FC<{ discount: IDiscountResponse }> = ({
  discount,
}) => {
  const { mutateAsync: handleUpdateDiscount, isPending } = useUpdateDiscount();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: discount.id,
      discountName: discount.discountName,
      startDate: moment(discount.startDate, "DD-MM-YYYY HH:mm:ss").format(
        "YYYY-MM-DD[T]HH:mm"
      ),
      endDate: moment(discount.endDate, "DD-MM-YYYY HH:mm:ss").format(
        "YYYY-MM-DD[T]HH:mm"
      ),
      status: discount.status,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateDiscountRequest;
    console.log({ requestData: data });
    await handleUpdateDiscount(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Chỉnh sửa khuyến mãi</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa khuyến mãi</DialogTitle>
          <DialogDescription>
            Chỉnh sửa khuyến mãi. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="discountName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="discountName">Tiêu đề *</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="discountName"
                          placeholder="Nhập tên giảm giá"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="status"
                  render={() => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="status">
                        Trạng thái (*)
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("status")}
                          onValueChange={(value: string) => {
                            form.setValue("status", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Trạng thái" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectItem value={DiscountStatus.SCHEDULED}>
                                Lên lịch
                              </SelectItem>
                              <SelectItem value={DiscountStatus.RUNNING}>
                                Đang hoạt động
                              </SelectItem>
                              <SelectItem value={DiscountStatus.TERMINATED}>
                                Kết thúc
                              </SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="startDate"
                  render={() => (
                    <FormItem className="flex flex-col">
                      <FormLabel>Ngày bắt đầu giảm</FormLabel>
                      <FormControl>
                        <Input
                          type="datetime-local"
                          placeholder="Nhập ngày bắt đầu giảm giá"
                          className="col-span-3"
                          onChange={(e) => {
                            form.setValue("startDate", e.target.value);
                          }}
                          value={form.getValues("startDate")}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="endDate"
                  render={() => (
                    <FormItem className="flex flex-col">
                      <FormLabel>Ngày kết thúc giảm giá</FormLabel>
                      <FormControl>
                        <Input
                          type="datetime-local"
                          placeholder="Nhập ngày kết thúc giảm giá"
                          className="col-span-3"
                          onChange={(e) => {
                            console.log(e.target.value);
                            form.setValue("endDate", e.target.value);
                          }}
                          value={form.getValues("endDate")}
                          // value={moment(
                          //   form.getValues("endDate"),
                          //   "DD-MM-YYYY HH:mm:ss"
                          // ).format("YYYY-MM-DD[T]HH:mm")}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-700 hover:bg-green-800 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
