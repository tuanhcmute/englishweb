import {
  Button,
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui";
import { BASE_URL } from "@/constants";
import { useUpdateDiscount } from "@/hooks";
import { convertNumberToMoney } from "@/lib";
import { IInvoiceResponse } from "@/types";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import Link from "next/link";
import React from "react";

export const GetInvoiceModal: React.FC<{
  invoice: IInvoiceResponse;
  orderId: string;
}> = ({ invoice, orderId }) => {
  const { mutateAsync: handleUpdateDiscount, isPending } = useUpdateDiscount();

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className='cursor-pointer'
          onSelect={(e) => e.preventDefault()}
        >
          <div>Xem hóa đơn</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[600px] overflow-y-auto max-h-full'>
        <DialogHeader>
          <DialogTitle className='text-center'>
            {invoice.invoiceTitle}
          </DialogTitle>
        </DialogHeader>
        <div className='grid gap-4 py-4'>
          <div className='grid grid-cols-1 gap-4'>
            <Table>
              <TableHeader>
                <TableRow>
                  <TableHead className='w-[100px]'>Số lượng</TableHead>
                  <TableHead>Đơn giá</TableHead>
                  <TableHead>Khuyến mãi</TableHead>
                  <TableHead className='text-right'>Tổng số tiền</TableHead>
                </TableRow>
              </TableHeader>
              <TableBody>
                {invoice?.invoiceLines?.map((item) => (
                  <TableRow key={item.quantity}>
                    <TableCell className='font-medium flex justify-center'>
                      <div className='w-[100px]'>{item.quantity} khóa học</div>
                    </TableCell>
                    <TableCell>
                      {convertNumberToMoney(item.unitPrice)}
                    </TableCell>
                    <TableCell>{item.discount}%</TableCell>
                    <TableCell className='text-right'>
                      {convertNumberToMoney(item.totalPrice)}
                    </TableCell>
                  </TableRow>
                ))}
                <TableRow>
                  <TableCell colSpan={3}>Thuế</TableCell>
                  <TableCell className='text-right'>{invoice.tax}%</TableCell>
                </TableRow>
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TableCell colSpan={3}>Tổng tiền</TableCell>
                  <TableCell className='text-right'>
                    {convertNumberToMoney(invoice.totalPrice)}
                  </TableCell>
                </TableRow>
              </TableFooter>
            </Table>
          </div>
        </div>
        <DialogFooter>
          <Link href={`${BASE_URL}/invoice/exportPDF/${orderId}`}>
            <Button className='bg-green-700 hover:bg-green-800 text-white hover:text-white'>
              {isPending && <IconLoader2 className='animate-spin' />}
              Xuất hóa đơn
            </Button>
          </Link>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
