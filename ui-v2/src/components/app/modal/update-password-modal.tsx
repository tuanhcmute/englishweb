import { useUpdateStaicPage } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  PasswordInput,
} from "@/components/ui";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IUpdateStaticPageRequest } from "@/types";
import { IconLoader2 } from "@tabler/icons-react";

const formSchema = z
  .object({
    currentPassword: z.string().trim().min(1),
    newPassword: z.string().trim().min(1),
    confirmPassword: z.string().trim().min(1),
    id: z.string().trim().min(1),
  })
  .refine((data) => data.newPassword === data.confirmPassword, {
    message: "Xác nhận mật khẩu không khớp",
    path: ["confirmPassword"],
  });

export const UpdatePasswordModal: React.FC<{}> = ({}) => {
  const [contentPreview, setContentPreview] = useState<string>(
    // staticPage.pageContent || ""

    ""
  );
  const { mutateAsync: handleUpdateStaticPage, isPending } =
    useUpdateStaicPage();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      confirmPassword: "",
      currentPassword: "",
      newPassword: "",
      id: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    // const data = {
    //   ...values,
    // } as IUpdateStaticPageRequest;
    // await handleUpdateStaticPage(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='outline'
          size='sm'
          className='lg:flex h-10 mt-1 gap-1 items-center border-purple-800 hover:bg-purple-800 text-purple-800 hover:text-white'
        >
          <Pencil1Icon />
          Thay đổi mật khẩu
        </Button>
      </DialogTrigger>
      <DialogContent className=''>
        <DialogHeader>
          <DialogTitle>Thay đổi mật khẩu</DialogTitle>
          <DialogDescription>
            Thay đổi mật khẩu. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='currentPassword'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='currentPassword'>
                        Mật khẩu cũ *
                      </FormLabel>
                      <FormControl>
                        <PasswordInput
                          {...field}
                          id='currentPassword'
                          placeholder='Nhập mật khẩu cũ'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='newPassword'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='newPassword'>
                        Mật khẩu mới *
                      </FormLabel>
                      <FormControl>
                        <PasswordInput
                          {...field}
                          id='newPassword'
                          placeholder='Nhập mật khẩu mới'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='confirmPassword'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='confirmPassword'>
                        Xác nhận khẩu mới *
                      </FormLabel>
                      <FormControl>
                        <PasswordInput
                          {...field}
                          id='confirmPassword'
                          placeholder='Nhập xác nhận mật khẩu'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
