import { useCreateStaticPage } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { PlusIcon } from "@radix-ui/react-icons";
import { ICreateStaticPageRequest, IUserCredentialResponse } from "@/types";
import { getCookie } from "cookies-next";
import { USER_CREDENTIAL } from "@/constants";
import { toast } from "react-hot-toast";
import { IconLoader2 } from "@tabler/icons-react";

import dynamic from "next/dynamic";
const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  pageCode: z.string().trim().min(1, {
    message: "Mã trang là bắt buộc",
  }),
  pageName: z.string().trim().min(1, {
    message: "Tên trang tĩnh là bắt buộc",
  }),
  pageContent: z.string().trim(),
  userCredentialId: z.string().trim(),
});

export const CreateStaticPageModal: React.FC<{}> = () => {
  const { mutateAsync: handleCreateStaticPage, isPending } =
    useCreateStaticPage();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      pageCode: "",
      pageName: "",
      pageContent: "",
      userCredentialId: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const userInfoJSON = getCookie(USER_CREDENTIAL)?.toString();
    if (!userInfoJSON) {
      toast.error("Thông tin người dùng không tồn tại");
      return;
    }
    const userCredential = JSON.parse(userInfoJSON) as IUserCredentialResponse;
    const data = {
      ...values,
      userCredentialId: userCredential.id,
    } as ICreateStaticPageRequest;
    await handleCreateStaticPage(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant="outline"
          size="sm"
          className="h-8 lg:flex gap-1 items-center border-green-500 hover:bg-green-500 hover:text-white"
        >
          <PlusIcon />
          Thêm trang tĩnh mới
        </Button>
      </DialogTrigger>
      <DialogContent className="max-w-full w-3/5 max-h-full overflow-y-auto">
        <DialogHeader>
          <DialogTitle>Thêm trang tĩnh mới</DialogTitle>
          <DialogDescription>
            Tạo thêm trang tĩnh. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  name="pageCode"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="pageCode">
                        Mã trang tĩnh *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="pageCode"
                          placeholder="Nhập mã trang tĩnh"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="pageName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="pageName">
                        Tên trang tĩnh *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="pageName"
                          placeholder="Nhập tên trang tĩnh"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="pageContent"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="pageContent">
                        Nội dung
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("pageContent")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("pageContent", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
