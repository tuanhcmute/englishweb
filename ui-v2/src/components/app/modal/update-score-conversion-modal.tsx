import { useUpdateScoreConversion } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ITestResponse, IUpdateScoreConversionRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Textarea,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import { Pencil1Icon } from "@radix-ui/react-icons";

const formSchema = z
  .object({
    id: z.string().trim().min(1),
    jsonScore: z.string().trim().min(1, {
      message: "Bảng quy đổi điểm là bắt buộc",
    }),
  })
  .refine(
    (data) => {
      try {
        // Attempt to parse the JSON string
        console.log({ param: data.jsonScore });
        JSON.parse(data.jsonScore);
        return true;
      } catch (error) {
        return false;
      }
    },
    {
      message: "Bảng quy đổi phải đúng dạng JSON",
      path: ["jsonScore"],
    }
  );

export const UpdateScoreConversionModal: React.FC<{ test: ITestResponse }> = ({
  test,
}) => {
  const { mutateAsync: handleUpdateScoreConversion, isPending } =
    useUpdateScoreConversion();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: test.scoreConversion?.id || "",
      jsonScore: test.scoreConversion?.jsonScore || "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = { ...values } as IUpdateScoreConversionRequest;
    console.log({ data });
    await handleUpdateScoreConversion(data);
  }

  const handleKeyDown = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Tab") {
      e.preventDefault();
      const target = e.target as HTMLTextAreaElement;
      const { selectionStart, selectionEnd } = target;
      const text = form.getValues("jsonScore");
      const newValue =
        text.substring(0, selectionStart) + "\t" + text.substring(selectionEnd);

      form.setValue("jsonScore", newValue);

      // Move the cursor to the right position after the tab character
      setTimeout(() => {
        target.selectionStart = target.selectionEnd = selectionStart + 1;
      }, 0);
    }
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer flex items-center gap-1"
        >
          <div>Chỉnh sửa bảng quy đổi điểm</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa bảng quy đổi điểm</DialogTitle>
          <DialogDescription>
            Chỉnh sửa bảng quy đổi điểm. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="jsonScore"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="jsonScore">
                        Bảng quy đổi điểm *
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          onKeyDown={handleKeyDown}
                          {...field}
                          id="jsonScore"
                          placeholder="Nhập quy đổi điểm"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
                onClick={() => console.log(form.getValues())}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
