import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import { useCreateSkilledAgentVersion, useLLMModels, useTools } from "@/hooks";
import { Pencil1Icon } from "@radix-ui/react-icons";
import Select, { MultiValue, ActionMeta } from "react-select";
import makeAnimated from "react-select/animated";
import json from "highlight.js/lib/languages/json";
import hljs from "highlight.js";
import { ICreateSkilledAgentVersionRequest } from "@/types";
import { useParams } from "next/navigation";

// Then register the languages you need
hljs.registerLanguage("json", json);

const animatedComponents = makeAnimated();

const formSchema = z.object({
  skilled_agent_id: z.string().trim().min(1),
  setup_message: z.array(
    z.object({
      type: z.string().trim().min(1),
      message: z.string().trim(),
    })
  ),
  version: z.string().trim().min(1),
  llm_model_ids: z.array(
    z.object({
      label: z.string(),
      value: z.string().trim().min(1),
    })
  ),
  tool_ids: z.array(
    z.object({
      label: z.string(),
      value: z.string().trim().min(1),
    })
  ),
});

export const CreateSkilledAgentVersionModal: React.FC<{}> = () => {
  const params = useParams<{ agentId: string }>();
  const toolsQuery = useTools();
  const llmModelsQuery = useLLMModels();
  const { mutateAsync: handleCreateSkilledAgentVersion, isPending } =
    useCreateSkilledAgentVersion();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      skilled_agent_id: params.agentId,
      llm_model_ids: [],
      setup_message: [],
      tool_ids: [],
      version: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const setup_message = JSON.stringify(values.setup_message);
    const llm_model_ids = values.llm_model_ids.map((item) => item.value);
    const tool_ids = values.tool_ids.map((item) => item.value);
    const requestData = {
      ...values,
      setup_message,
      llm_model_ids,
      tool_ids,
    } as ICreateSkilledAgentVersionRequest;
    console.log({ requestData });
    await handleCreateSkilledAgentVersion(requestData);
  }

  // [newValue: MultiValue<unknown>, actionMeta: ActionMeta<unknown>]
  const handleSelectLLMModels = ({
    actionMeta,
  }: {
    newValue: MultiValue<{ label: string; value: string }>;
    actionMeta: ActionMeta<{ label: string; value: string }>;
  }) => {
    const values = form.getValues("llm_model_ids");
    if (actionMeta.action === "remove-value") {
      const value = actionMeta.removedValue.value;
      if (value)
        form.setValue("llm_model_ids", [
          ...values.filter((item) => item.value !== value),
        ]);
    } else if (actionMeta.action === "select-option") {
      const item = actionMeta.option;
      if (item) form.setValue("llm_model_ids", [...values, item]);
    }
  };

  const handleSelectTools = ({
    actionMeta,
  }: {
    newValue: MultiValue<{ label: string; value: string }>;
    actionMeta: ActionMeta<{ label: string; value: string }>;
  }) => {
    const values = form.getValues("tool_ids");
    if (actionMeta.action === "remove-value") {
      const value = actionMeta.removedValue.value;
      if (value)
        form.setValue("tool_ids", [
          ...values.filter((item) => item.value !== value),
        ]);
    } else if (actionMeta.action === "select-option") {
      const item = actionMeta.option;
      if (item) form.setValue("tool_ids", [...values, item]);
    }
  };

  const loadToolOptions = () => {
    if (!toolsQuery.data) return [];
    return toolsQuery.data.map((item) => {
      return {
        label: `${item.tool_name}(${item.param})`,
        value: item.id,
      };
    });
  };

  const loadModelOptions = () => {
    if (!llmModelsQuery.data) return [];
    return llmModelsQuery.data.map((item) => {
      return {
        label: `${item.llm_model_name}(${item.param})`,
        value: item.id,
      };
    });
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>New Agent Version</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="overflow-y-auto max-h-full w-5/6 max-w-full">
        <DialogHeader>
          <DialogTitle>New Agent Version</DialogTitle>
          <DialogDescription>
            New Agent Version. Press the save button to save the data
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="version"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel htmlFor="version">Version *</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="version"
                          className="col-span-3"
                          placeholder="Type version"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="llm_model_ids"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel htmlFor="llm_model_ids">LLM model *</FormLabel>
                      <Select
                        className="text-sm"
                        closeMenuOnSelect={false}
                        components={animatedComponents}
                        isMulti
                        options={loadModelOptions()}
                        placeholder="Search LLM models"
                        onChange={(...e) =>
                          handleSelectLLMModels({
                            newValue: e[0] as MultiValue<{
                              label: string;
                              value: string;
                            }>,
                            actionMeta: e[1] as ActionMeta<{
                              label: string;
                              value: string;
                            }>,
                          })
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="tool_ids"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel htmlFor="tool_ids">Tool *</FormLabel>
                      <Select
                        className="text-sm"
                        closeMenuOnSelect={false}
                        components={animatedComponents}
                        isMulti
                        options={loadToolOptions()}
                        placeholder="Search tools"
                        onChange={(...e) =>
                          handleSelectTools({
                            newValue: e[0] as MultiValue<{
                              label: string;
                              value: string;
                            }>,
                            actionMeta: e[1] as ActionMeta<{
                              label: string;
                              value: string;
                            }>,
                          })
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
                onClick={() => console.log(form.getValues())}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Save
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
