import { useDeleteTest } from "@/hooks";
import React from "react";
import { ITestResponse } from "@/types";
import {
  Button,
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
} from "@/components/ui";
import { IconDeselect, IconLoader2, IconTrash } from "@tabler/icons-react";

export const RemoveTestModal: React.FC<{ test: ITestResponse }> = ({
  test,
}) => {
  const { mutateAsync: handleDeleteTest, isPending } = useDeleteTest();

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Xóa bài thi</div>
          <DropdownMenuShortcut>
            <IconTrash className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Xóa bài thi</DialogTitle>
          <DialogDescription>
            Sau khi xóa, dữ liệu sẽ bị mất, nhấn hủy để dừng thao tác này
          </DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button
            variant={"destructive"}
            disabled={isPending}
            onClick={() => handleDeleteTest(test.id)}
          >
            {isPending && <IconLoader2 className="animate-spin" />}
            Xóa
          </Button>
          <DialogClose asChild>
            <Button
              type="button"
              variant="default"
              className="bg-purple-800 hover:bg-purple-900 text-white hover:text-white"
            >
              Hủy bỏ
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
