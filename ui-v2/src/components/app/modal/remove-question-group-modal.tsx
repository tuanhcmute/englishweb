import { Button } from "@/components/ui";
import { useDeleteQuestionGroup } from "@/hooks";
import { IQuestionGroupResponse } from "@/types";
import { ResetIcon } from "@radix-ui/react-icons";
import React from "react";
import Confirm from "./confirm";

export const RemoveQuestionGroupModal: React.FC<{
  questionGroup: IQuestionGroupResponse;
}> = ({ questionGroup }) => {
  const { mutateAsync: handleDeleteQuestionGroup } = useDeleteQuestionGroup();

  return (
    <Confirm
      title={"Xóa nhóm câu hỏi"}
      description='Sau khi xóa, dữ liệu sẽ bị mất, nhấn xóa để tiếp tục'
      textConfirm='Xóa'
      onConfirm={async (close) => {
        await handleDeleteQuestionGroup(questionGroup.id);
        close?.();
      }}
    >
      <Button
        variant='destructive'
        size='sm'
        className='h-8 lg:flex gap-1 items-center w-fit mt-4'
      >
        <ResetIcon />
        Xóa nhóm câu hỏi
      </Button>
    </Confirm>
  );
};
