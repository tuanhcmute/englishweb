import { useUpdateComment, useUpdateTest } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ICommentResponse, IUpdateCommentRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import { Pencil1Icon } from "@radix-ui/react-icons";

const formSchema = z.object({
  id: z.string().trim().min(1),
  isNegative: z.string().default("0"),
});

export const UpdateCommentModal: React.FC<{
  comment: ICommentResponse;
}> = ({ comment }) => {
  const { mutateAsync: handleUpdateComment, isPending } = useUpdateComment();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: comment.id,
      isNegative: comment.isNegative ? "1" : "0",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const isNegative = values.isNegative === "1";
    const data = {
      ...values,
      isNegative,
      content: comment.content,
      isReview: true,
    } as IUpdateCommentRequest;
    console.log({ data });
    await handleUpdateComment(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Kiểm duyệt bình luận</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Kiểm duyệt bình luận</DialogTitle>
          <DialogDescription>
            Kiểm duyệt bình luận. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="isNegative"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="isNegative">
                        Đánh dấu bình luận
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("isNegative")}
                          onValueChange={(value: string) => {
                            form.setValue("isNegative", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Lựa chọn loại đánh dấu" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectLabel>Đánh dấu bình luận</SelectLabel>
                              <SelectItem value="0">Tích cực</SelectItem>
                              <SelectItem value="1">Tiêu cực</SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Kiểm duyệt
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
