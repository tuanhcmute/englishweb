import { useUpdateBanner, useUpdateNewsCategory } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  IBannerResponse,
  INewsCategoryResponse,
  IUpdateBannerRequest,
  IUpdateNewsCategoryRequest,
} from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { IconEdit, IconLoader2 } from "@tabler/icons-react";
import { NewsCategoryType, NewsType } from "@/enums";
import dynamic from "next/dynamic";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  image: z.string().trim().min(1),
  id: z.string().trim().min(1),
});

export const UpdateBannerModal: React.FC<{
  banner: IBannerResponse;
}> = ({ banner }) => {
  const { mutateAsync: handleUpdateBanner, isPending } = useUpdateBanner();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: banner.id,
      image: banner.image,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = {
      ...values,
    } as IUpdateBannerRequest;
    console.log({ requestData });
    await handleUpdateBanner(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Chỉnh sửa banner</div>
          <DropdownMenuShortcut>
            <IconEdit className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="max-w-full w-1/2 max-h-full overflow-y-auto">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa banner</DialogTitle>
          <DialogDescription>
            Chỉnh sửa banner. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="image"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="image">Hình ảnh *</FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("image")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("image", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white flex items-center gap-1"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
