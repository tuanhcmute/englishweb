import { useUpdateFlashcardItemImage } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { ACCEPTED_IMAGE_TYPES, MAX_FILE_SIZE } from "@/constants";
import { IconLoader2 } from "@tabler/icons-react";
import dynamic from "next/dynamic";
const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  image: z.string().trim().min(1),
});

export const UpdateFlashcardItemImageModal: React.FC<{
  flashcardItemId: string;
}> = ({ flashcardItemId }) => {
  const { mutateAsync: handleUpdateFlashcardItemImage, isPending } =
    useUpdateFlashcardItemImage();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      image: undefined,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data: { image: string; id: string } = {
      image: values.image,
      id: flashcardItemId,
    };
    await handleUpdateFlashcardItemImage(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="link" className="p-0 italic text-yellow-500">
          Thay đổi hình ảnh minh họa
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Thay đổi hình ảnh minh họa</DialogTitle>
          <DialogDescription>
            Thay đổi hình ảnh minh họa. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="image"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="image">
                        Hình ảnh *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("image")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("image", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
