import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import {
  useCreateTestSuiteRun,
  useSkilledAgentVersions,
  useTestSuite,
} from "@/hooks";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { useParams } from "next/navigation";
import { TEST_SUITE_RUN_STATUS } from "@/constants";
import Select, { SingleValue } from "react-select";
import { ICreateTestSuiteRunRequest } from "@/types";

const formSchema = z.object({
  version: z.string().trim().min(1),
  score: z.number().min(0),
  run_status: z.string().trim().min(1),
  skilled_agent_version_id: z.string().trim().min(1),
  test_suite_id: z.string().trim().min(1),
  the_number_of_question: z.number().min(0),
});

export const CreateTestSuiteRunModal: React.FC<{}> = () => {
  const [agentId, setAgentId] = useState<string | undefined>(undefined);
  const { mutateAsync: handleCreateTestSuiteRun, isPending } =
    useCreateTestSuiteRun();
  const params = useParams<{ testSuiteId: string }>();
  const testSuiteQuery = useTestSuite(params.testSuiteId);
  const skilledAgentVersionsQuery = useSkilledAgentVersions(agentId);

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      run_status: TEST_SUITE_RUN_STATUS.NOT_RUN,
      score: 0,
      version: "",
      test_suite_id: params.testSuiteId,
      skilled_agent_version_id: "",
      the_number_of_question: testSuiteQuery.data?.questions.length || 0,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = { ...values } as ICreateTestSuiteRunRequest;
    await handleCreateTestSuiteRun(requestData);
  }

  const loadSkilledAgentOptions = (): { value: string; label: string }[] => {
    if (testSuiteQuery.data) {
      return testSuiteQuery.data.skilled_agents.map((item) => ({
        label: item.agent_name,
        value: item.id,
      }));
    }
    return [];
  };

  const loadAgentVersionOptions = (): { value: string; label: string }[] => {
    if (skilledAgentVersionsQuery.data) {
      return skilledAgentVersionsQuery.data.map((item) => ({
        label: item.version,
        value: item.id,
      }));
    }
    return [];
  };

  const handleSelectAgent = (
    singleValue: SingleValue<{ label: string; value: string }>
  ) => {
    setAgentId(singleValue?.value);
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className='cursor-pointer'
          onSelect={(e) => e.preventDefault()}
        >
          <div>New Test Suite Run</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='overflow-y-auto max-h-full max-w-full w-4/5'>
        <DialogHeader>
          <DialogTitle>New Test Suite Run</DialogTitle>
          <DialogDescription>
            New Test Suite Run. Press the save button to save the data
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='version'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='version'>Version *</FormLabel>
                      <FormControl>
                        <Input
                          id='version'
                          {...field}
                          placeholder='Type new version'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormLabel htmlFor='skilled_agent_version_id'>
                  Agent *
                </FormLabel>
                <FormControl>
                  <Select
                    className='text-sm'
                    options={loadSkilledAgentOptions()}
                    placeholder='Select a agent'
                    onChange={(
                      singleValue: SingleValue<{ label: string; value: string }>
                    ) => {
                      handleSelectAgent(singleValue);
                    }}
                  />
                </FormControl>
              </div>
              {agentId && (
                <div className='grid grid-cols-1 gap-4'>
                  <FormField
                    control={form.control}
                    name='skilled_agent_version_id'
                    render={({ field }) => (
                      <FormItem className=''>
                        <FormLabel htmlFor='skilled_agent_version_id'>
                          Agent Versions *
                        </FormLabel>
                        <FormControl>
                          <Select
                            className='text-sm'
                            options={loadAgentVersionOptions()}
                            placeholder='Select a agent version'
                            onChange={(singleValue) => {
                              if (singleValue?.value)
                                form.setValue(
                                  "skilled_agent_version_id",
                                  singleValue.value
                                );
                            }}
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
              )}
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Save
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
