import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { QuestionGroupFileType } from "@/enums";
import { useUploadQuestionGroupFile } from "@/hooks";
import { IUploadQuestionGroupFileRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { UploadIcon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { ReactNode, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import ReactPlayer from "react-player";
import { z } from "zod";

const MAX_FILE_SIZE = 10000000;

const ACCEPTED_AUDIO_TYPES = ["audio/mpeg"];

export const questionGroupAudioSchema = z.object({
  id: z.string().trim(),
  type: z.string().trim(),
  file: z
    .any()
    .refine(
      (file: File) => file.size <= MAX_FILE_SIZE,
      `Max audio size is 10MB.`
    )
    .refine((file: File) => {
      console.log({ file });
      return ACCEPTED_AUDIO_TYPES.includes(file.type);
    }, "Only .mp3 format is supported."),
});

function ReactPlayerWrapper({ children }: { children: ReactNode }) {
  return <div className='w-full h-[40px] mt-4'>{children}</div>;
}

export const UploadQuestionGroupAudioModal: React.FC<{
  questionGroupId: string;
}> = ({ questionGroupId }) => {
  const { mutateAsync: handleUploadQuestionGroupFile, isPending } =
    useUploadQuestionGroupFile();
  const [selectedAudio, setSelectedAudio] = useState<any>(null);
  const playerRef = useRef<ReactPlayer | null>(null);

  // 1. Define your form.
  const questionGroupForm = useForm<z.infer<typeof questionGroupAudioSchema>>({
    resolver: zodResolver(questionGroupAudioSchema),
    defaultValues: {
      file: null,
      type: QuestionGroupFileType.AUDIO,
      id: questionGroupId,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof questionGroupAudioSchema>) {
    const requesData = { ...values } as IUploadQuestionGroupFileRequest;
    await handleUploadQuestionGroupFile(requesData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='outline'
          size='sm'
          className='h-8 lg:flex gap-1 items-center w-fit mt-4 bg-green-500 hover:bg-green-600 text-white hover:text-white'
        >
          <UploadIcon />
          Upload audio
        </Button>
      </DialogTrigger>
      <DialogContent className='max-w-full w-3/5 max-h-full'>
        <DialogHeader>
          <DialogTitle>Upload audio</DialogTitle>
          <DialogDescription>
            Upload audio. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...questionGroupForm}>
          <form onSubmit={questionGroupForm.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className=''>
                <FormField
                  control={questionGroupForm.control}
                  name='file'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='audio'>Audio</FormLabel>
                      <FormControl>
                        <Input
                          id='audio'
                          placeholder='Upload audio'
                          className='col-span-3'
                          type='file'
                          onChange={(e) => {
                            const audioFiles = e.target?.files;
                            questionGroupForm.setValue(
                              "file",
                              audioFiles?.item(0)
                            );
                            setSelectedAudio(audioFiles?.item(0));
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                {selectedAudio && (
                  <ReactPlayer
                    url={URL.createObjectURL(selectedAudio)}
                    controls
                    height={50}
                    // playing={true}
                    ref={playerRef}
                    wrapper={ReactPlayerWrapper}
                  />
                )}
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
