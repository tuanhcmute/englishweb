import { useCreateUnit, useRoles, useUpdateRole } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { IUpdateRoleRequest, IUserCredentialResponse } from "@/types";

const formSchema = z.object({
  roleId: z.string().trim().min(1, {
    message: "Mã quyền là bắt buộc",
  }),
  userCredentialId: z.string().trim().min(1, {
    message: "Mã người dùng là bắt buộc",
  }),
});

export const UpdateRoleModal: React.FC<{ user: IUserCredentialResponse }> = ({
  user,
}) => {
  const { mutateAsync: handleUpdateRole, isPending } = useUpdateRole();
  const rolesQuery = useRoles();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      roleId:
        user.userAuthorities.length > 0 ? user.userAuthorities[0].role.id : "",
      userCredentialId: user.id,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateRoleRequest;
    await handleUpdateRole(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer flex items-center gap-1"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Thay đổi quyền</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Thay đổi quyền</DialogTitle>
          <DialogDescription>
            Thay đổi quyền. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  name="roleId"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="roleId">
                        Quyền hạn (*)
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("roleId") || ""}
                          onValueChange={(value: string) => {
                            form.setValue("roleId", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Lựa chọn quyền" />
                          </SelectTrigger>
                          <SelectContent>
                            {rolesQuery.data?.data.map((item) => {
                              return (
                                <SelectItem
                                  value={item.id}
                                  className="cursor-pointer"
                                  key={item.id}
                                >
                                  {item.roleName}
                                </SelectItem>
                              );
                            })}
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
