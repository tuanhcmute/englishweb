import { useCreateLesson, useCreateUnit, useUnitsByCourse } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { ICreateLessonRequest, ICreateUnitRequest } from "@/types";

const formSchema = z.object({
  title: z.string().trim().min(1, {
    message: "Tiêu đề là bắt buộc",
  }),
  unitId: z.string().trim().min(1, {
    message: "Mã chương là bắt buộc",
  }),
});

export const CreateLessonModal: React.FC<{ courseId: string }> = ({
  courseId,
}) => {
  const unitsByCourseQuery = useUnitsByCourse(courseId);
  const { mutateAsync: handleCreateLesson, isPending } = useCreateLesson();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: "",
      unitId: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as ICreateLessonRequest;
    console.log({ data });
    await handleCreateLesson(data);
  }

  if (unitsByCourseQuery.error) {
    return (
      <div className='font-bold text-center w-full'>
        {unitsByCourseQuery.error.message}
      </div>
    );
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button className='flex items-center h-8 gap-1 bg-white dark:bg-transparent dark:hover:bg-purple-800 border-purple-800 text-purple-800 border hover:bg-purple-800 hover:text-white dark:text-white'>
          <Pencil1Icon className='w-5 h-5' />
          <p>Thêm bài học mới</p>
        </Button>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[425px]'>
        <DialogHeader>
          <DialogTitle>Thêm bài học mới</DialogTitle>
          <DialogDescription>
            Thêm bài học mới. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='unitId'
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Chương</FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("unitId") || ""}
                          onValueChange={(value: string) => {
                            form.setValue("unitId", value);
                          }}
                        >
                          <SelectTrigger className='w-full'>
                            <SelectValue placeholder='Lựa chọn chương' />
                          </SelectTrigger>
                          <SelectContent>
                            {unitsByCourseQuery.data?.data.map((item) => {
                              return (
                                <SelectItem
                                  value={item.id}
                                  className='cursor-pointer'
                                  key={item.id}
                                >
                                  {item.title}
                                </SelectItem>
                              );
                            })}
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='title'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='title'>
                        Tiêu đề (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='title'
                          placeholder='Nhập tiêu đề'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
