import { useUpdatePartInformation } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  IPartInformationResponse,
  IUpdatePartInformationRequest,
} from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
  Textarea,
} from "@/components/ui";
import { Pencil1Icon, UpdateIcon } from "@radix-ui/react-icons";
import { PrartType } from "@/enums";
import { IconLoader2 } from "@tabler/icons-react";

const formSchema = z.object({
  id: z.string().trim().min(1, {
    message: "Mã phần thi là bắt buộc",
  }),
  partInformationName: z.string().trim().min(1, {
    message: "Tên phần thi là bắt buộc",
  }),
  totalQuestion: z.number().min(1, {
    message: "Số lượng câu hỏi phần thi là bắt buộc",
  }),
  partInformationDescription: z.string().trim().min(1, {
    message: "Mô tả phần thi là bắt buộc",
  }),
  partSequenceNumber: z.number().min(1, {
    message: "Số thứ tự phần thi là bắt buộc",
  }),
  partType: z.string().trim().min(1, {
    message: "Loại phần thi là bắt buộc",
  }),
});

export const UpdatePartInformationModal: React.FC<{
  partInformation: IPartInformationResponse;
}> = ({ partInformation }) => {
  const { mutateAsync: handleUpdatePartInformation, isPending } =
    useUpdatePartInformation();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: partInformation.id,
      partInformationName: partInformation.partInformationName,
      totalQuestion: partInformation.totalQuestion,
      partInformationDescription: partInformation.partInformationDescription,
      partSequenceNumber: partInformation.partSequenceNumber,
      partType: partInformation.partType,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = { ...values } as IUpdatePartInformationRequest;
    await handleUpdatePartInformation(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className='cursor-pointer'
        >
          <div> Chỉnh sửa thông tin phần thi</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[600px] overflow-y-auto max-h-full'>
        <DialogHeader>
          <DialogTitle>Chỉnh sửa thông tin phần thi</DialogTitle>
          <DialogDescription>
            Chỉnh sửa thông tin phần thi. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='partInformationName'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='partInformationName'>
                        Tên phần thi *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='partInformationName'
                          placeholder='Nhập phần thi'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='partInformationDescription'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel
                        className=''
                        htmlFor='partInformationDescription'
                      >
                        Mô tả phần thi *
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          {...field}
                          id='partInformationDescription'
                          placeholder='Mô tả phần thi'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='totalQuestion'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='totalQuestion'>
                        Số lượng câu hỏi của phần thi *
                      </FormLabel>
                      <FormControl>
                        <Input
                          id='totalQuestion'
                          onChange={(e) => {
                            form.setValue("totalQuestion", +e.target.value);
                          }}
                          placeholder='Số lượng câu hỏi của phần thi'
                          className='col-span-3'
                          type='number'
                          value={form.getValues("totalQuestion").toString()}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='partSequenceNumber'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='totalQuestion'>
                        Số thứ tự của phần thi *
                      </FormLabel>
                      <FormControl>
                        <Input
                          id=''
                          onChange={(e) => {
                            form.setValue(
                              "partSequenceNumber",
                              +e.target.value
                            );
                          }}
                          value={form
                            .getValues("partSequenceNumber")
                            .toString()}
                          placeholder='Số lượng thứ tự của phần thi'
                          className='col-span-3'
                          type='number'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='partType'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='partType'>
                        Loại phần thi *
                      </FormLabel>
                      <FormControl>
                        <Select
                          onValueChange={(value: string) => {
                            form.setValue("partType", value);
                          }}
                          defaultValue={form.getValues("partType")}
                        >
                          <SelectTrigger className='w-full'>
                            <SelectValue placeholder='Lựa chọn danh mục bài thi' />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectItem value={PrartType.LISTENING}>
                                Phần nghe
                              </SelectItem>
                              <SelectItem value={PrartType.READING}>
                                Phần đọc
                              </SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
