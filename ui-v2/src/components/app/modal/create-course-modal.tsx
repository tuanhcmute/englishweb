import { useCourseCategories } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ICourseCategoryResponse, ICreateCourseRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { useCreateCourse } from "@/hooks/use-create-course";

const formSchema = z.object({
  isActive: z.string().default("1"),
  courseCategoryId: z.string().trim().min(1, {
    message: "Danh mục khóa học là bắt buộc",
  }),
  courseName: z.string().trim().min(1, {
    message: "Tên khóa học là bắt buộc",
  }),
  oldPrice: z.number().min(1, {
    message: "Số tiền cũ là bắt buộc",
  }),
  courseImg: z.string(),
});

import dynamic from "next/dynamic";
import { IconBook, IconLoader2 } from "@tabler/icons-react";
import { convertNumberToMoney } from "@/lib";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

export const CreateCourseModal: React.FC<{}> = () => {
  const [selectedImage, setSelectedImage] = useState<any>(undefined);
  const [oldPrice, setOldPrice] = useState<number>(100000);
  const courseCategoriesQuery = useCourseCategories();
  const { mutateAsync: handleCreateCourse, isPending } = useCreateCourse();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      courseImg: undefined,
      courseName: "",
      isActive: "1",
      oldPrice: 100000,
      courseCategoryId: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const isActive = values.isActive === "1";
    const data = {
      ...values,
      isActive,
    } as ICreateCourseRequest;
    console.log({ requestData: data });
    await handleCreateCourse(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Tạo khóa học mới</div>
          <DropdownMenuShortcut>
            <IconBook className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Thêm khóa học mới</DialogTitle>
          <DialogDescription>
            Tạo thêm khóa học. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  name="courseCategoryId"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="courseCategoryId">
                        Danh mục khóa học *
                      </FormLabel>
                      <FormControl>
                        <Select
                          onValueChange={(value: string) => {
                            form.setValue("courseCategoryId", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Lựa chọn danh mục bài thi" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectLabel>Danh mục khóa học</SelectLabel>
                              {courseCategoriesQuery.isLoading ? (
                                <SelectItem value="no-item">
                                  Loading...
                                </SelectItem>
                              ) : (
                                courseCategoriesQuery.data?.data.map(
                                  (item: ICourseCategoryResponse) => {
                                    return (
                                      <SelectItem value={item.id} key={item.id}>
                                        {item.courseCategoryName}
                                      </SelectItem>
                                    );
                                  }
                                )
                              )}
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="courseName"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="courseName">
                        Tên khóa học *
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="courseName"
                          placeholder="Nhập tên khóa học"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-2 gap-4">
                <FormField
                  control={form.control}
                  name="oldPrice"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="oldPrice">
                        Giá tiền
                      </FormLabel>
                      <FormControl>
                        <Input
                          type="number"
                          placeholder="Nhập giá tiền"
                          className="col-span-3"
                          value={form.getValues("oldPrice")}
                          onChange={(e) => {
                            form.setValue("oldPrice", +e.target.value);
                            setOldPrice(+e.target.value);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <div className="mt-auto">
                  <Input
                    type="text"
                    disabled
                    className="font-bold"
                    value={convertNumberToMoney(oldPrice)}
                  />
                </div>
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="courseImg"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="courseImg">
                        Hình ảnh *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("courseImg")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("courseImg", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              {selectedImage && (
                <div className="grid grid-cols-1 gap-4">
                  <img
                    src={URL.createObjectURL(selectedImage)}
                    alt="courseImg"
                    className="w-96"
                    height={100}
                  />
                </div>
              )}
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
