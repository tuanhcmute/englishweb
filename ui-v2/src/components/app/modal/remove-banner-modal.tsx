import {
  Button,
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
} from "@/components/ui";
import { useDeleteBanner } from "@/hooks";
import { IBannerResponse } from "@/types";
import React from "react";
import { IconTrash } from "@tabler/icons-react";
import { IconLoader2 } from "@tabler/icons-react";

export const RemoveBannerModal: React.FC<{ banner: IBannerResponse }> = ({
  banner,
}) => {
  const { mutateAsync: handleDeleteBanner, isPending } = useDeleteBanner();

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => e.preventDefault()}
          className="cursor-pointer"
        >
          <div>Xóa banner</div>
          <DropdownMenuShortcut>
            <IconTrash className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Xóa banner</DialogTitle>
          <DialogDescription>
            Sau khi xóa, dữ liệu sẽ bị mất, nhấn hủy để dừng thao tác này
          </DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button
            variant={"destructive"}
            disabled={isPending}
            onClick={() => handleDeleteBanner(banner.id)}
          >
            {isPending && <IconLoader2 className="animate-spin" />}
            Xóa
          </Button>
          <DialogClose asChild>
            <Button
              type="button"
              variant="default"
              className="bg-purple-800 hover:bg-purple-900 text-white hover:text-white"
            >
              Hủy bỏ
            </Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
