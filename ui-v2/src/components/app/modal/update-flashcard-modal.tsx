import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { IFlashcardResponse, IUpdateFlashcardRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { useUpdateFlashcard } from "@/hooks";
import { IconLoader2 } from "@tabler/icons-react";
import { Pencil1Icon } from "@radix-ui/react-icons";

const formSchema = z.object({
  flashcardTitle: z.string().trim().min(1, {
    message: "Tiêu đề là bắt buộc",
  }),
  flashcardDescription: z.string().trim().min(1, {
    message: "Mô tả là bắt buộc",
  }),
  id: z.string().trim(),
});

export const UpdateFlashcardModal: React.FC<{
  flashcard: IFlashcardResponse;
}> = ({ flashcard }) => {
  const { mutateAsync: handleUpdateFlashcard, isPending } =
    useUpdateFlashcard();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      flashcardTitle: flashcard.flashcardTitle,
      flashcardDescription: flashcard.flashcardDescription,
      id: flashcard.id,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateFlashcardRequest;
    await handleUpdateFlashcard(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Chỉnh sửa</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa danh sách từ</DialogTitle>
          <DialogDescription>
            Chỉnh sửa danh sách từ cho riêng bạn. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  name="flashcardTitle"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="flashcardTitle">
                        Tiêu đề (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="flashcardTitle"
                          placeholder="Nhập tiêu đề"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  name="flashcardDescription"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="flashcardDescription">
                        Mô tả (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="flashcardDescription"
                          placeholder="Nhập mô tả"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
