import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { QuestionGroupFileType } from "@/enums";
import { useUploadQuestionGroupFile } from "@/hooks";
import { IUploadQuestionGroupFileRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { UploadIcon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

const MAX_FILE_SIZE = 10000000;
const ACCEPTED_IMAGE_TYPES = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/webp",
];

export const questionGroupImageSchema = z.object({
  id: z.string().trim(),
  type: z.string().trim(),
  file: z
    .any()
    .refine(
      (file: File) => file?.size <= MAX_FILE_SIZE,
      `Max image size is 10MB.`
    )
    .refine(
      (file: File) => ACCEPTED_IMAGE_TYPES.includes(file?.type),
      "Only .jpg, .jpeg, .png and .webp formats are supported."
    ),
});

export const UploadQuestionGroupImageModal: React.FC<{
  questionGroupId: string;
}> = ({ questionGroupId }) => {
  const { mutateAsync: handleUploadQuestionGroupFile, isPending } =
    useUploadQuestionGroupFile();
  const [selectedImage, setSelectedImage] = useState<any>(null);

  // 1. Define your form.
  const questionGroupForm = useForm<z.infer<typeof questionGroupImageSchema>>({
    resolver: zodResolver(questionGroupImageSchema),
    defaultValues: {
      file: null,
      id: questionGroupId,
      type: QuestionGroupFileType.IMAGE,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof questionGroupImageSchema>) {
    const requesData = { ...values } as IUploadQuestionGroupFileRequest;
    await handleUploadQuestionGroupFile(requesData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='outline'
          size='sm'
          className='h-8 lg:flex gap-1 items-center w-fit mt-4 bg-green-500 hover:bg-green-600 text-white hover:text-white'
        >
          <UploadIcon />
          Upload image
        </Button>
      </DialogTrigger>
      <DialogContent className='max-w-full w-3/5 max-h-full overflow-y-auto'>
        <DialogHeader>
          <DialogTitle>Upload image</DialogTitle>
          <DialogDescription>
            Upload image. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...questionGroupForm}>
          <form onSubmit={questionGroupForm.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <div className=''>
                  <FormField
                    control={questionGroupForm.control}
                    name='file'
                    render={({ field }) => (
                      <FormItem className=''>
                        <FormLabel className='image'>Image</FormLabel>
                        <FormControl>
                          <Input
                            id='image'
                            placeholder='Upload image'
                            className='col-span-3'
                            type='file'
                            onChange={(e) => {
                              const imageFile = e.target?.files;
                              questionGroupForm.setValue(
                                "file",
                                imageFile?.item(0)
                              );
                              setSelectedImage(imageFile?.item(0));
                            }}
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  {selectedImage && (
                    <img
                      className='mt-2'
                      src={URL.createObjectURL(selectedImage)}
                      alt='Image'
                    />
                  )}
                </div>
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
