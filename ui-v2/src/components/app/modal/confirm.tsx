"use client";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui";
import { useDisclosure } from "@/hooks";
import React from "react";

type Props = {
  children: React.ReactNode;
  title: string;
  description: string;
  onConfirm?: (close?: () => void) => void;
  textConfirm?: string;
};

export const Confirm = ({
  children,
  title,
  description,
  onConfirm,
  textConfirm = "Confirm",
}: Props) => {
  const [isOpenConfirm, { onClose: onCloseConfirm, onOpen: onOpenConfirm }] =
    useDisclosure();

  return (
    <Dialog
      open={isOpenConfirm}
      onOpenChange={(open: boolean) => {
        if (open) {
          onOpenConfirm();
        } else {
          onCloseConfirm();
        }
      }}
    >
      <DialogTrigger asChild>{children}</DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>{title}</DialogTitle>
          <DialogDescription>{description}</DialogDescription>
        </DialogHeader>
        <DialogFooter className='gap-2'>
          <Button
            onClick={onCloseConfirm}
            variant='destructive'
            className='bg-gray-100 hover:bg-gray-200 text-black border border-gray-300 dark:text-white dark:bg-gray-800 dark:hover:bg-gray-900 dark:border-gray-600'
          >
            Hủy bỏ
          </Button>
          <Button
            variant='default'
            className='bg-transparent border border-purple-800  hover:bg-purple-900 bg-purple-800 text-white dark:text-white'
            onClick={() => {
              onConfirm?.(onCloseConfirm);
            }}
          >
            {textConfirm}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default Confirm;
