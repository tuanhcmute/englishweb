import { useCreateFlashcard } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { ICreateFlashcardRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { PlusIcon } from "@radix-ui/react-icons";
import { getCookie } from "cookies-next";
import { USER_CREDENTIAL_ID } from "@/constants";
import { FlashcardType } from "@/enums";
import { IconLoader2 } from "@tabler/icons-react";

const formSchema = z.object({
  flashcardTitle: z.string().trim().min(1, {
    message: "Tiêu đề là bắt buộc",
  }),
  flashcardDescription: z.string().trim().min(1, {
    message: "Mô tả là bắt buộc",
  }),
  userCredentialId: z.string().trim().min(1, {
    message: "Mã người dùng là bắt buộc",
  }),
  totalFlashcardItem: z.number().min(0).default(0),
  flashcardType: z
    .string()
    .trim()
    .min(1, {
      message: "Mã người dùng là bắt buộc",
    })
    .default(FlashcardType.PERSONAL),
});

export const CreateFlashcardModal: React.FC<{}> = () => {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID)
    ?.toString()
    .replaceAll(`"`, "");
  const { mutateAsync: handleCreateFlashcard, isPending } =
    useCreateFlashcard();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      flashcardTitle: "",
      flashcardDescription: "",
      totalFlashcardItem: 0,
      userCredentialId: userCredentialId,
      flashcardType: FlashcardType.PERSONAL,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as ICreateFlashcardRequest;
    await handleCreateFlashcard(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <div className='flex items-center gap-1 flex-wrap hover:text-purple-800 transition-colors cursor-pointer text-sm'>
          <PlusIcon className='w-5 h-5' />
          <p>Thêm danh sách từ</p>
        </div>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[425px]'>
        <DialogHeader>
          <DialogTitle>Thêm danh sách từ</DialogTitle>
          <DialogDescription>
            Tạo danh sách từ cho riêng bạn. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='flashcardTitle'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='flashcardTitle'>
                        Tiêu đề (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='flashcardTitle'
                          placeholder='Nhập tiêu đề'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='flashcardDescription'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='flashcardDescription'>
                        Mô tả (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='flashcardDescription'
                          placeholder='Nhập mô tả'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
