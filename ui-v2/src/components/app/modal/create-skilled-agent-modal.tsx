import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Textarea,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import { useCreateSkilledAgent, useTestSuites } from "@/hooks";
import { Pencil1Icon } from "@radix-ui/react-icons";
import Select, { MultiValue, ActionMeta } from "react-select";
import makeAnimated from "react-select/animated";
import { ICreateSkilledAgentRequest } from "@/types";

const animatedComponents = makeAnimated();

const formSchema = z.object({
  agent_name: z.string().trim().min(1),
  setup_message: z.string().trim().min(1),
  test_score: z.number().default(0),
  description: z.string().trim().min(1),
  test_suite_ids: z.array(z.string()),
});

export const CreateSkilledAgentModal: React.FC<{}> = () => {
  const testSuitesQuery = useTestSuites();
  const { mutateAsync: handleCreateSkilledAgent, isPending } =
    useCreateSkilledAgent();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      agent_name: "",
      setup_message: "",
      test_score: 0,
      description: "",
      test_suite_ids: [],
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = { ...values } as ICreateSkilledAgentRequest;
    await handleCreateSkilledAgent(requestData);
  }

  // [newValue: MultiValue<unknown>, actionMeta: ActionMeta<unknown>]
  const handleSelect = ({
    actionMeta,
  }: {
    newValue: MultiValue<{ label: string; value: string }>;
    actionMeta: ActionMeta<{ label: string; value: string }>;
  }) => {
    const values = form.getValues("test_suite_ids");

    if (actionMeta.action === "remove-value") {
      const id = actionMeta.removedValue.value;
      if (id)
        form.setValue("test_suite_ids", [
          ...values.filter((item) => item !== id),
        ]);
    } else if (actionMeta.action === "select-option") {
      const id = actionMeta.option?.value;
      if (id) form.setValue("test_suite_ids", [...values, id]);
    }
  };

  const loadOptions = () => {
    if (!testSuitesQuery.data) return [];
    return testSuitesQuery.data.map((item) => {
      return {
        label: item.test_suite_name,
        value: item.id,
      };
    });
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className='cursor-pointer'
          onSelect={(e) => e.preventDefault()}
        >
          <div>New Agent</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='overflow-y-auto max-h-full'>
        <DialogHeader>
          <DialogTitle>New Agent</DialogTitle>
          <DialogDescription>
            New Agent. Press the save button to save the data
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='agent_name'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='agent_name'>Agent name *</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='agent_name'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='setup_message'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='agent_name'>
                        Setup message *
                      </FormLabel>
                      <FormControl>
                        <Textarea {...field} id='setup_message' />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='description'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='description'>Description *</FormLabel>
                      <FormControl>
                        <Textarea {...field} id='description' />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='description'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='description'>Test suite *</FormLabel>
                      <Select
                        className='text-sm'
                        closeMenuOnSelect={false}
                        components={animatedComponents}
                        isMulti
                        options={loadOptions()}
                        placeholder='Search test suite'
                        onChange={(...e) =>
                          handleSelect({
                            newValue: e[0] as MultiValue<{
                              label: string;
                              value: string;
                            }>,
                            actionMeta: e[1] as ActionMeta<{
                              label: string;
                              value: string;
                            }>,
                          })
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Save
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
