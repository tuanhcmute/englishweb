import { useDeleteUserCredential } from "@/hooks";
import React from "react";
import {
  Button,
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
} from "@/components/ui";
import { TrashIcon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { IUserCredentialResponse } from "@/types";

export const DeleteUserCredentialModal: React.FC<{
  user: IUserCredentialResponse;
}> = ({ user }) => {
  const { mutateAsync: handleDeleteUserCredential, isPending } =
    useDeleteUserCredential();

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer flex items-center gap-1"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Xóa người dùng</div>
          <DropdownMenuShortcut>
            <TrashIcon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>Xóa người dùng</DialogTitle>
          <DialogDescription>
            Xóa người dùng. Nhấn hủy để hủy thao tác
          </DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button
            className="bg-white hover:bg-white text-black hover:text-black"
            disabled={isPending}
            onClick={async () => await handleDeleteUserCredential(user.id)}
          >
            {isPending && <IconLoader2 className="animate-spin" />}
            Xóa
          </Button>
          <DialogClose asChild>
            <Button variant="secondary">Hủy</Button>
          </DialogClose>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
