import { useCreateUnit } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { ICreateUnitRequest } from "@/types";

const formSchema = z.object({
  title: z.string().trim().min(1, {
    message: "Tiêu đề là bắt buộc",
  }),
  courseId: z.string().trim().min(1, {
    message: "Mã khóa học là bắt buộc",
  }),
});

export const CreateUnitModal: React.FC<{ courseId: string }> = ({
  courseId,
}) => {
  const { mutateAsync: handleCreateUnit, isPending } = useCreateUnit();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      title: "",
      courseId,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as ICreateUnitRequest;
    await handleCreateUnit(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button className='flex items-center h-8 gap-1 bg-white dark:bg-transparent dark:hover:bg-purple-800 border-purple-800 text-purple-800 border hover:bg-purple-800 hover:text-white dark:text-white'>
          <Pencil1Icon className='w-5 h-5' />
          <p>Thêm chương mới</p>
        </Button>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[425px]'>
        <DialogHeader>
          <DialogTitle>Thêm chương mới</DialogTitle>
          <DialogDescription>
            Thêm chương mới. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='title'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='title'>
                        Tiêu đề (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id='title'
                          placeholder='Nhập tiêu đề'
                          className='col-span-3'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
