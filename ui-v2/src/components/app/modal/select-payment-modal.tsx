import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui";
import {
  BASE_URL,
  ROUTES,
  USER_CREDENTIAL,
  USER_CREDENTIAL_ID,
} from "@/constants";
import { PaymentMethod } from "@/enums";
import { getCookie } from "cookies-next";
import { useRouter } from "next/navigation";
import React from "react";
import { usePayOS, PayOSConfig } from "payos-checkout";

const paymentMethods: {
  id: string;
  method: string;
  iconLink: string;
  textColor: string;
  href: string;
}[] = [
  {
    id: "1",
    method: PaymentMethod.VNPAY,
    iconLink:
      "https://cdn.haitrieu.com/wp-content/uploads/2022/10/Logo-VNPAY-QR-1.png",
    textColor: "text-red-500",
    href: `${BASE_URL}/payment/redirect?paymentMethod=${PaymentMethod.VNPAY}`,
  },
  // {
  //   id: "2",
  //   method: PaymentMethod.PAYPAL,
  //   iconLink:
  //     "https://cdn.iconscout.com/icon/free/png-256/free-paypal-58-711793.png?f=webp",
  //   textColor: "text-blue-400",
  //   href: `${BASE_URL}/payment/redirect?paymentMethod=${PaymentMethod.PAYPAL}`,
  // },
];

export const SelectPaymentModal: React.FC<{
  courseId: string;
  totalAmount: number;
}> = ({ courseId, totalAmount }) => {
  const router = useRouter();
  const userCredential = getCookie(USER_CREDENTIAL);
  const userCredentialId = getCookie(USER_CREDENTIAL_ID)
    ?.toString()
    .replaceAll(`"`, "");

  const handleClickPayment = (url: string) => {
    const originUrl = window.location.origin;
    const successUrl = `${originUrl}/checkout/success`;
    const failureUrl = `${originUrl}/checkout/failure`;
    console.log({ successUrl, failureUrl });
    if (!userCredential) {
      console.log("Unauthorized");
      router.push(ROUTES.LOGIN);
      return;
    }
    window.location.href = `${url}&successUrl=${successUrl}&failureUrl=${failureUrl}&baseUrl=${BASE_URL}`;
  };

  const payOSConfig: PayOSConfig = {
    RETURN_URL: "", // required
    ELEMENT_ID: "", // required
    CHECKOUT_URL: "", // required
    onSuccess: (event: any) => {
      //TODO: Hành động sau khi người dùng thanh toán đơn hàng thành công
    },
    onCancel: (event: any) => {
      //TODO: Hành động sau khi người dùng Hủy đơn hàng
    },
    onExit: (event: any) => {
      //TODO: Hành động sau khi người dùng tắt Pop up
    },
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="destructive" className="block w-full mt-2">
          Đăng ký học ngay
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Lựa chọn phương thức thanh toán</DialogTitle>
          <DialogDescription>
            Lựa chọn phương thức thanh toán. Nhấn hủy để hủy bỏ.
          </DialogDescription>
        </DialogHeader>
        <div className="flex flex-col gap-4">
          {paymentMethods.map((item) => {
            return (
              <Button
                variant="secondary"
                key={item.id}
                className={`${item.textColor} text-lg font-bold py-6 hover:shadow-sm hover:bg-gray-200 transition-all`}
                onClick={() =>
                  handleClickPayment(
                    `${item.href}&courseId=${courseId}&userCredentialId=${userCredentialId}&totalAmount=${totalAmount}`
                  )
                }
              >
                <img src={item.iconLink} alt={item.iconLink} className="w-20" />
                {item.method}
              </Button>
            );
          })}
        </div>
      </DialogContent>
    </Dialog>
  );
};
