import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
  Textarea,
} from "@/components/ui";
import { useCreateQuestionGroup, usePartOfTest } from "@/hooks";
import { IPartOfTest, IQuestionGroupRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { PlusIcon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import dynamic from "next/dynamic";
import { useForm } from "react-hook-form";
import { z } from "zod";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

export const questionGroupSchema = z.object({
  // Question group
  partOfTestId: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
  questionContentEn: z.string().trim(),
  questionContentTranscript: z.string().trim(),
  image: z.string().trim(),
});

export const CreateQuestionGroupModal: React.FC<{
  testId: string;
  partId: string;
}> = ({ testId, partId }) => {
  const partOfTestQuery = usePartOfTest(testId);
  const { mutateAsync: handleCreateQuestionGroup, isPending } =
    useCreateQuestionGroup();

  // 1. Define your form.
  const questionGroupForm = useForm<z.infer<typeof questionGroupSchema>>({
    resolver: zodResolver(questionGroupSchema),
    defaultValues: {
      partOfTestId: partId,
      questionContentEn: "",
      questionContentTranscript: "",
      image: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof questionGroupSchema>) {
    const requesData = { ...values } as IQuestionGroupRequest;
    await handleCreateQuestionGroup(requesData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='outline'
          size='sm'
          className='ml-auto h-8 lg:flex gap-1 items-center w-fit bg-green-500 hover:bg-green-600 text-white hover:text-white'
        >
          <PlusIcon />
          Tạo nhóm câu hỏi
        </Button>
      </DialogTrigger>
      <DialogContent className='max-w-full w-3/5 max-h-full overflow-y-auto'>
        <DialogHeader>
          <DialogTitle>Thêm nhóm câu hỏi mới</DialogTitle>
          <DialogDescription>
            Tạo nhóm câu hỏi mới. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...questionGroupForm}>
          <form onSubmit={questionGroupForm.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='partOfTestId'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='testCategoryId'>
                        Phần thi
                      </FormLabel>
                      <FormControl>
                        <Select
                          onValueChange={(value: string) => {
                            questionGroupForm.setValue("partOfTestId", value);
                          }}
                          defaultValue={partId}
                        >
                          <SelectTrigger className='w-full'>
                            <SelectValue placeholder='Lựa chọn phần thi' />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectLabel>Phần thi</SelectLabel>
                              {partOfTestQuery.isLoading ? (
                                <SelectItem value='no-item'>
                                  Loading...
                                </SelectItem>
                              ) : (
                                partOfTestQuery.data?.data.map(
                                  (item: IPartOfTest) => {
                                    return (
                                      <SelectItem value={item.id} key={item.id}>
                                        {
                                          item.partInformation
                                            .partInformationName
                                        }
                                      </SelectItem>
                                    );
                                  }
                                )
                              )}
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-2 gap-4'>
                <FormField
                  control={questionGroupForm.control}
                  name='questionContentEn'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='questionContentEn'>
                        Nhập bài đọc tiếng anh
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          placeholder='Nhập nội dung tiếng anh'
                          {...field}
                          className='col-span-3 h-[200px]'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={questionGroupForm.control}
                  name='questionContentTranscript'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='questionContentTranscript'>
                        Nhập bài đọc tiếng việt
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          placeholder='Nhập nội dung tiếng việt'
                          {...field}
                          className='col-span-3 h-[200px]'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={questionGroupForm.control}
                  name='image'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='image'>
                        Upload hình ảnh
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={questionGroupForm.getValues("image")}
                          handleChange={({ data }) => {
                            questionGroupForm.setValue("image", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
