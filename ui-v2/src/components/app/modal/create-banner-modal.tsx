"use client";
import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { IconCategory2, IconLoader2 } from "@tabler/icons-react";
import { useCreateBanner, useCreateNewsCategory } from "@/hooks";

import dynamic from "next/dynamic";
import { ICreateBannerRequest } from "@/types";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  image: z.string().trim().min(1, {
    message: "Hình ảnh là bắt buộc",
  }),
});

export const CreateBannerModal: React.FC<{}> = () => {
  const { mutateAsync: handleCreateBanner, isPending } = useCreateBanner();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      image: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = { ...values } as ICreateBannerRequest;
    await handleCreateBanner(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Tạo banner mới</div>
          <DropdownMenuShortcut>
            <IconCategory2 className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="max-w-full w-1/2 max-h-full overflow-y-auto">
        <DialogHeader>
          <DialogTitle>Thêm banner mới</DialogTitle>
          <DialogDescription>
            Tạo banner. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="image"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="image">Hình ảnh *</FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("image")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("image", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
