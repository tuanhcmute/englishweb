import { useTestCategories } from "@/hooks";
import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { IPartInformationRequest, ITestCategory, ITestRequest } from "@/types";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
  Textarea,
} from "@/components/ui";
import { ExternalLinkIcon, PlusIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import { IconCategory2, IconInfoSquare } from "@tabler/icons-react";

export const GetPartInformationModal: React.FC<{ categoryId: string }> = ({
  categoryId,
}) => {
  const router = useRouter();

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          onSelect={(e) => {
            e.preventDefault();
            router.push(`test-categories/${categoryId}/part-information`);
          }}
          className='cursor-pointer'
        >
          <div>Xem thông tin phần thi</div>
          <DropdownMenuShortcut>
            <IconInfoSquare className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
    </Dialog>
  );
};
