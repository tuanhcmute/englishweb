import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Label,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import React from "react";

import { ICreateQuestionRequest } from "@/types";
import { useForm, useFieldArray } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { PlusIcon } from "@radix-ui/react-icons";
import { useCreateQuestion } from "@/hooks";
import dynamic from "next/dynamic";
import { IconLoader2 } from "@tabler/icons-react";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

export const testAnswerSchema = z.object({
  answerContent: z.string().trim().min(1, {
    message: "Nội dung đáp án là bắt buộc",
  }),
  answerTranslate: z.string().trim().min(1, {
    message: "Nội dung giải thích đáp án là bắt buộc",
  }),
  answerChoice: z.string().trim().min(1, {
    message: "Lựa chọn là bắt buộc",
  }),
  isCorrect: z.boolean().default(false),
});

export const testAnswersSchema = z.array(testAnswerSchema);

export const questionSchema = z.object({
  questionContent: z.string().trim().min(1, {
    message: "Nội dung câu hỏi là bắt buộc",
  }),
  questionNumber: z.number().min(1, {
    message: "Số thứ tự phải lớn hơn 0",
  }),
  // rightAnswer: z.string().trim().min(1, {
  //   message: "Đáp án đúng là bắt buộc",
  // }),
  questionGuide: z.string().trim().min(1, {
    message: "Hướng dẫn câu hỏi là bắt buộc",
  }),
  testAnswers: z.array(testAnswerSchema),
});

export const CreateQuestionModal: React.FC<{ questionGroupId: string }> = ({
  questionGroupId,
}) => {
  const { mutateAsync: handleCreateQuestion, isPending } = useCreateQuestion();

  const questionForm = useForm<z.infer<typeof questionSchema>>({
    resolver: zodResolver(questionSchema),
    defaultValues: {
      questionContent: "",
      questionNumber: 1,
      questionGuide: "",
      testAnswers: [],
    },
  });

  const { append, remove, fields } = useFieldArray({
    name: "testAnswers",
    control: questionForm.control,
  });

  async function onSubmit(
    values: z.infer<typeof questionSchema>,
    questionGroupId: string
  ) {
    const requestData = {
      ...values,
      questionGroupId,
    } as ICreateQuestionRequest;
    await handleCreateQuestion(requestData);
    // questionForm.reset(values);
  }

  function handleAddTestAnswer() {
    append({
      answerContent: "",
      answerChoice: "",
      answerTranslate: "",
      isCorrect: false,
    });
  }

  function handleRemoveTestAnswer() {
    const testAnswersSize = questionForm.getValues("testAnswers").length;
    remove(testAnswersSize - 1);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='outline'
          size='sm'
          className='h-8 lg:flex gap-1 items-center w-fit mt-4 bg-green-500 hover:bg-green-600 text-white hover:text-white'
        >
          <PlusIcon />
          Thêm câu hỏi
        </Button>
      </DialogTrigger>
      <DialogContent className='max-w-full w-4/5 max-h-full overflow-y-auto'>
        <DialogHeader>
          <DialogTitle>Thêm câu hỏi mới</DialogTitle>
          <DialogDescription>
            Tạo thêm câu hỏi mới. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...questionForm}>
          <form
            onSubmit={questionForm.handleSubmit((values) =>
              onSubmit(values, questionGroupId)
            )}
          >
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={questionForm.control}
                  name='questionNumber'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel
                        className='questionNumber'
                        htmlFor='questionNumber'
                      >
                        Thứ tự câu hỏi
                      </FormLabel>
                      <FormControl>
                        <Input
                          id='questionNumber'
                          placeholder='Nhập thứ tự câu hỏi'
                          className='col-span-3'
                          type='number'
                          onChange={(e) => {
                            questionForm.setValue(
                              "questionNumber",
                              +e.target.value
                            );
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={questionForm.control}
                  name='questionContent'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='questionContent'>
                        Nội dung câu hỏi
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData=''
                          handleChange={({ data, editor }) => {
                            // console.log({ data });
                            questionForm.setValue("questionContent", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={questionForm.control}
                  name='questionGuide'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='questionGuide'>
                        Giải thích câu hỏi
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData=''
                          handleChange={({ data, editor }) => {
                            // console.log({ data });
                            questionForm.setValue("questionGuide", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                {/* <FormField
                  control={questionForm.control}
                  name='rightAnswer'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className=''htmlFor='rightAnswer'>
                        Đáp án đúng
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          id='rightAnswer'
                          placeholder='Nhập đáp án đúng'
                          className='col-span-3'
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                /> */}
              </div>
            </div>
            <div className='grid gap-4 py-4'>
              {fields.map((field, index) => {
                const errorForField =
                  questionForm.formState.errors?.testAnswers?.[index];
                return (
                  <div className='grid grid-cols-4 gap-4' key={index}>
                    <div className='flex flex-col gap-2'>
                      <Label>Nhập lựa chọn</Label>
                      <Input
                        {...questionForm.register(
                          `testAnswers.${index}.answerChoice` as const
                        )}
                        placeholder='Nhập lựa chọn'
                        defaultValue={field.answerChoice}
                      />
                      <p className='text-destructive text-sm'>
                        {errorForField?.answerChoice?.message ?? <>&nbsp;</>}
                      </p>
                    </div>
                    <div className='flex flex-col gap-2'>
                      <Label>Nội dung đáp án EN</Label>
                      <Input
                        {...questionForm.register(
                          `testAnswers.${index}.answerContent` as const
                        )}
                        placeholder='Nhập đáp án EN'
                        defaultValue={field.answerContent}
                      />
                      <p className='text-destructive text-sm'>
                        {errorForField?.answerContent?.message ?? <>&nbsp;</>}
                      </p>
                    </div>
                    <div className='flex flex-col gap-2'>
                      <Label>Nội dung đáp án VN</Label>
                      <Input
                        {...questionForm.register(
                          `testAnswers.${index}.answerTranslate` as const
                        )}
                        placeholder='Nhập đáp án VI'
                        defaultValue={field.answerTranslate}
                      />
                      <p className='text-destructive text-sm'>
                        {errorForField?.answerTranslate?.message ?? <>&nbsp;</>}
                      </p>
                    </div>
                    <div className='flex flex-col gap-2'>
                      <Label>Đáp án</Label>
                      <Select
                        onValueChange={(value: string) => {
                          questionForm.setValue(
                            `testAnswers.${index}.isCorrect`,
                            value === "1"
                          );
                        }}
                        defaultValue={field.isCorrect ? "1" : "0"}
                      >
                        <SelectTrigger className='w-full'>
                          <SelectValue placeholder='Lựa chọn phần thi' />
                        </SelectTrigger>
                        <SelectContent>
                          <SelectGroup>
                            <SelectItem value={"1"}>Đáp án đúng</SelectItem>
                            <SelectItem value={"0"}>Đáp án sai</SelectItem>
                          </SelectGroup>
                        </SelectContent>
                      </Select>
                      <p className='text-destructive text-sm'>
                        {errorForField?.answerTranslate?.message ?? <>&nbsp;</>}
                      </p>
                    </div>
                  </div>
                );
              })}
            </div>
            <div className='flex items-center justify-center gap-2'>
              <Button
                variant='outline'
                size='sm'
                className='h-8 lg:flex gap-1 items-center w-fit '
                onClick={handleRemoveTestAnswer}
              >
                <PlusIcon />
                Xóa đáp án
              </Button>
              <Button
                variant='outline'
                size='sm'
                className='h-8 lg:flex gap-1 items-center w-fit '
                onClick={handleAddTestAnswer}
              >
                <PlusIcon />
                Thêm đáp án
              </Button>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
