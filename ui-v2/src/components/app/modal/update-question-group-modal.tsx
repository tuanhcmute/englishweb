import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Textarea,
} from "@/components/ui";
import { useUpdateQuestionGroup } from "@/hooks";
import { IQuestionGroupResponse, IUpdateQuestionGroupRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { PlusIcon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import dynamic from "next/dynamic";
import { useForm } from "react-hook-form";
import { z } from "zod";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const questionGroupSchema = z.object({
  // Question group
  id: z.string().trim(),
  questionContentEn: z.string().trim(),
  questionContentTranscript: z.string().trim(),
  image: z.string().trim(),
});

export const UpdateQuestionGroupModal: React.FC<{
  questionGroup: IQuestionGroupResponse;
  testId: string;
}> = ({ testId, questionGroup }) => {
  const { mutateAsync: handleUpdateQuestionGroup, isPending } =
    useUpdateQuestionGroup();

  // 1. Define your form.
  const questionGroupForm = useForm<z.infer<typeof questionGroupSchema>>({
    resolver: zodResolver(questionGroupSchema),
    defaultValues: {
      id: questionGroup.id,
      questionContentEn: questionGroup.questionContentEn,
      questionContentTranscript: questionGroup.questionContentTranscript,
      image: questionGroup.image,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof questionGroupSchema>) {
    const requesData = { ...values } as IUpdateQuestionGroupRequest;
    await handleUpdateQuestionGroup(requesData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='outline'
          size='sm'
          className='mt-4   h-8 lg:flex gap-1 items-center w-fit bg-orange-500 hover:bg-orange-600 text-white hover:text-white'
        >
          <PlusIcon />
          Chỉnh sửa nhóm câu hỏi
        </Button>
      </DialogTrigger>
      <DialogContent className='max-w-full w-3/5 max-h-full overflow-y-auto'>
        <DialogHeader>
          <DialogTitle>Chỉnh sửa câu hỏi</DialogTitle>
          <DialogDescription>
            Chỉnh sửa câu hỏi. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...questionGroupForm}>
          <form onSubmit={questionGroupForm.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-2 gap-4'>
                <FormField
                  control={questionGroupForm.control}
                  name='questionContentEn'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='questionContentEn'>
                        Nhập bài đọc tiếng anh
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          placeholder='Nhập nội dung tiếng anh'
                          {...field}
                          className='col-span-3 h-[200px]'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={questionGroupForm.control}
                  name='questionContentTranscript'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='questionContentTranscript'>
                        Nhập bài đọc tiếng việt
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          placeholder='Nhập nội dung tiếng việt'
                          {...field}
                          className='col-span-3 h-[200px]'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={questionGroupForm.control}
                  name='image'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='image'>
                        Upload hình ảnh
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={questionGroupForm.getValues("image")}
                          handleChange={({ data }) => {
                            questionGroupForm.setValue("image", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
