import React from "react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
} from "@/components/ui";
import { IconListCheck } from "@tabler/icons-react";
import { IDiscountResponse } from "@/types";
import { AssignDiscountForm } from "@/components/app/forms";

export const AssignDiscountModal: React.FC<{ discount: IDiscountResponse }> = ({
  discount,
}) => {
  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Chỉnh sửa điều kiện khuyến mãi</div>
          <DropdownMenuShortcut>
            <IconListCheck className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="max-w-full w-5/6 overflow-y-auto max-h-screen">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa điều kiện khuyến mãi</DialogTitle>
          <DialogDescription>
            <p>
              - Chương trình khuyến mãi được áp dụng khi thỏa 1 trong các điều
              kiên
            </p>
            <p>
              - Một chương trình khuyến mãi không được có 2 đối tượng áp dụng
              giống nhau
            </p>
          </DialogDescription>
        </DialogHeader>
        <AssignDiscountForm discount={discount} />
      </DialogContent>
    </Dialog>
  );
};
