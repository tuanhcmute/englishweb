import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui";
import React from "react";

import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { Pencil1Icon, UpdateIcon } from "@radix-ui/react-icons";
import { ICourseDetailResponse, IUpdateCourseDetailRequest } from "@/types";
import { useUpdateCourseDetail } from "@/hooks/use-update-course-detail";
import dynamic from "next/dynamic";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  courseIntroduction: z.string().trim().min(1, {
    message: "Giới thiệu khóa học là bắt buộc",
  }),
  courseTarget: z.string().trim().min(1, {
    message: "Kết quả khóa học là bắt buộc",
  }),
  courseInformation: z.string().trim().min(1, {
    message: "Thông tin khóa học là bắt buộc",
  }),
  courseGuide: z.string().trim().min(1, {
    message: "Hướng dẫn khóa học là bắt buộc",
  }),
  courseId: z.string().trim().min(1, {
    message: "Mã khóa học là bắt buộc",
  }),
  courseDetailId: z.string().trim().min(1, {
    message: "Mã chi tiết khoa học là bắt buộc",
  }),
});

export const UpdateCourseDetailModal: React.FC<{
  courseDetail?: ICourseDetailResponse;
}> = ({ courseDetail }) => {
  console.log(courseDetail);
  const { mutateAsync: handleUpdateCourseDetail } = useUpdateCourseDetail();

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      courseDetailId: courseDetail?.id,
      courseInformation: courseDetail?.courseInformation,
      courseGuide: courseDetail?.courseGuide,
      courseId: courseDetail?.course.id,
      courseIntroduction: courseDetail?.courseIntroduction,
      courseTarget: courseDetail?.courseTarget,
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    console.log({ values });
    const requestData = {
      ...values,
    } as IUpdateCourseDetailRequest;
    console.log({ requestData });
    await handleUpdateCourseDetail(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='destructive'
          size='sm'
          className='h-8 lg:flex gap-1 items-center w-fit mt-4 bg-transparent border border-purple-800 hover:bg-purple-800 hover:text-white dark:text-white text-purple-800'
        >
          <Pencil1Icon />
          Chỉnh sửa chi tiết khóa học
        </Button>
      </DialogTrigger>
      <DialogContent className='max-w-full w-3/5 max-h-full overflow-y-auto'>
        <DialogHeader>
          <DialogTitle>Chỉnh sửa chi tiết khóa học</DialogTitle>
          <DialogDescription>
            Chỉnh sửa chi tiết khóa học. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='courseIntroduction'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='courseIntroduction'>
                        Giới thiệu khóa học
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("courseIntroduction")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("courseIntroduction", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='courseInformation'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='courseInformation'>
                        Thông tin khóa học
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("courseInformation")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("courseInformation", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='courseGuide'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='courseGuide'>
                        Hướng dẫn khóa học
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("courseGuide")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("courseGuide", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name='courseTarget'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='courseTarget'>
                        Kết quả khóa học
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("courseTarget")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("courseTarget", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-500 hover:bg-green-600 text-white hover:text-white'
              >
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
