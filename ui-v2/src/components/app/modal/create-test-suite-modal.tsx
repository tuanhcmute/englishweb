import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Textarea,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import { useCreateTestSuite, useSkilledAgents } from "@/hooks";
import { Pencil1Icon } from "@radix-ui/react-icons";
import Select, { ActionMeta } from "react-select";
import makeAnimated from "react-select/animated";
import json from "highlight.js/lib/languages/json";
import hljs from "highlight.js";
import { ICreateTestSuiteRequest } from "@/types";

// Then register the languages you need
hljs.registerLanguage("json", json);

const animatedComponents = makeAnimated();

const formSchema = z.object({
  test_suite_name: z.string().trim().min(1),
  description: z.string().trim().min(1),
  skilled_agent_ids: z.array(
    z.object({
      label: z.string(),
      value: z.string().trim().min(1),
    })
  ),
});

export const CreateTestSuiteModal: React.FC<{}> = () => {
  const skilledAgentsQuery = useSkilledAgents();
  const { mutateAsync: handleCreateTestSuite, isPending } =
    useCreateTestSuite();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      description: "",
      skilled_agent_ids: [],
      test_suite_name: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const skilled_agent_ids = values.skilled_agent_ids.map(
      (item) => item.value
    );
    const requestData = {
      ...values,
      skilled_agent_ids,
    } as ICreateTestSuiteRequest;
    await handleCreateTestSuite(requestData);
  }

  // [newValue: MultiValue<unknown>, actionMeta: ActionMeta<unknown>]
  const handleSelect = ({
    actionMeta,
    newValue,
  }: {
    newValue: { label: string; value: string };
    actionMeta: ActionMeta<{ label: string; value: string }>;
  }) => {
    const values = form.getValues("skilled_agent_ids");
    if (actionMeta.action === "remove-value") {
      const value = actionMeta.removedValue.value;
      if (value)
        form.setValue("skilled_agent_ids", [
          ...values.filter((item) => item.value !== value),
        ]);
    } else if (actionMeta.action === "select-option") {
      const item = actionMeta.option;
      if (item) form.setValue("skilled_agent_ids", [...values, item]);
    }
  };

  const loadAgentOptions = () => {
    if (!skilledAgentsQuery.data) return [];
    return skilledAgentsQuery.data.map((item) => {
      return {
        label: item.agent_name,
        value: item.id,
      };
    });
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className='cursor-pointer'
          onSelect={(e) => e.preventDefault()}
        >
          <div>New Test Suite</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='overflow-y-auto max-h-full w-3/4 max-w-full'>
        <DialogHeader>
          <DialogTitle>New Test Suite</DialogTitle>
          <DialogDescription>
            New Test Suite. Press the save button to save the data
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='test_suite_name'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='test_suite_name'>
                        Test suite name *
                      </FormLabel>
                      <FormControl>
                        <Input
                          id='test_suite_name'
                          {...field}
                          placeholder='Type test suite name'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='description'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='description'>Description *</FormLabel>
                      <FormControl>
                        <Textarea
                          id='description'
                          {...field}
                          placeholder={"Type description"}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='skilled_agent_ids'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='skilled_agent_ids'>
                        Agents *
                      </FormLabel>
                      <Select
                        className='text-sm'
                        components={animatedComponents}
                        options={loadAgentOptions()}
                        placeholder='Select Agents'
                        onChange={(...e) =>
                          handleSelect({
                            newValue: e[0] as {
                              label: string;
                              value: string;
                            },
                            actionMeta: e[1] as ActionMeta<{
                              label: string;
                              value: string;
                            }>,
                          })
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Save
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
