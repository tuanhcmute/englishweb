import { Button } from "@/components/ui";
import { SymbolIcon } from "@radix-ui/react-icons";
import React from "react";
import Confirm from "./confirm";

export const SyncFullTestAnswerModal: React.FC<{ testId: string }> = ({
  testId,
}) => {
  return (
    <Confirm
      title='Đồng bộ đáp án'
      textConfirm='Đồng bộ'
      description='Thực hiện đồng bộ đáp án sau khi thực hiện thêm, sửa, xóa các câu hỏi trong các phần thi'
      onConfirm={(close) => {
        console.log("hello");
        close?.();
      }}
    >
      <Button
        variant='outline'
        size='sm'
        className='h-8 lg:flex gap-1 items-center'
      >
        <SymbolIcon />
        Đồng bộ đáp án
      </Button>
    </Confirm>
  );
};
