import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Label,
} from "@/components/ui";
import React, { useState } from "react";

import { useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { IFullNewsResponse, IUpdateNewsRequest } from "@/types";
import dynamic from "next/dynamic";
import { useUpdateNews } from "@/hooks";
import { IconEdit, IconLoader2 } from "@tabler/icons-react";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  newsContentHtml: z.string().trim().min(1, {
    message: "Nội dung bài viết là bắt buộc",
  }),
  id: z.string().trim().min(1, {
    message: "Mã bài viết là bắt buộc",
  }),
});

export const UpdateNewsContentModal: React.FC<{
  newsItem: IFullNewsResponse;
}> = ({ newsItem }) => {
  const { mutateAsync: handleUpdateNews, isPending } = useUpdateNews();
  const [content, setContent] = useState<string>(newsItem.newsContentHtml);

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: newsItem.id,
      newsContentHtml: newsItem.newsContentHtml,
    },
  });

  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = {
      ...values,
      newsDescription: newsItem?.newsDescription,
      newsImage: newsItem?.newsImage,
      newsTitle: newsItem?.newsTitle,
      status: newsItem?.status,
    } as IUpdateNewsRequest;
    console.log({ requestData });
    await handleUpdateNews(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='destructive'
          size='sm'
          className='h-8 lg:flex gap-1 items-center w-fit mt-4 bg-transparent border border-purple-800 text-purple-800 dark:text-white hover:bg-purple-800 hover:text-white'
        >
          <IconEdit className='w-4' />
          Chỉnh sửa bài viết
        </Button>
      </DialogTrigger>
      <DialogContent className='max-w-full w-[95%] max-h-full overflow-y-auto'>
        <DialogHeader>
          <DialogTitle> Chỉnh sửa nội dung bài viết</DialogTitle>
          <DialogDescription>
            Chỉnh sửa nội dung bài viết. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-2 gap-4'>
                <FormField
                  control={form.control}
                  name='newsContentHtml'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='newsContentHtml'>
                        Nội dung bài viết
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("newsContentHtml")}
                          handleChange={({ data }) => {
                            form.setValue("newsContentHtml", data);
                            setContent(data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <div>
                  <Label>Nội dung hiển thị</Label>
                  <div
                    dangerouslySetInnerHTML={{ __html: content }}
                    className='max-h-[750px] overflow-y-scroll'
                  ></div>
                </div>
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
