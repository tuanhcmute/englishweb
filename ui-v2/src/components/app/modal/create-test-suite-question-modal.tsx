import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Textarea,
} from "@/components/ui";
import { IconLoader2 } from "@tabler/icons-react";
import {
  useCreateLLMModel,
  useCreateTestSuiteQuestion,
  useLLMModelModules,
} from "@/hooks";
import { Pencil1Icon } from "@radix-ui/react-icons";
import Select, { ActionMeta } from "react-select";
import makeAnimated from "react-select/animated";
import json from "highlight.js/lib/languages/json";
import hljs from "highlight.js";
import { useParams } from "next/navigation";
import { ICreateTestSuiteQuestionRequest } from "@/types";

// Then register the languages you need
hljs.registerLanguage("json", json);

const animatedComponents = makeAnimated();

const formSchema = z.object({
  question_name: z.string().trim().min(1),
  expected_answer: z.string().trim().min(1),
  test_suite_id: z.string().trim().min(1),
});

export const CreateTestSuiteQuestionModal: React.FC<{}> = () => {
  const llmModelModulesQuery = useLLMModelModules();
  const { mutateAsync: handleCreateTestSuiteQuestion, isPending } =
    useCreateTestSuiteQuestion();
  const params = useParams<{ testSuiteId: string }>();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      question_name: "",
      expected_answer: "",
      test_suite_id: params.testSuiteId,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const requestData = { ...values } as ICreateTestSuiteQuestionRequest;
    await handleCreateTestSuiteQuestion(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className='cursor-pointer'
          onSelect={(e) => e.preventDefault()}
        >
          <div>New Test Suite Question</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className='w-4' />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className='overflow-y-auto max-h-full max-w-full w-4/5'>
        <DialogHeader>
          <DialogTitle>New Test Suite Question</DialogTitle>
          <DialogDescription>
            New Test Suite Question. Press the save button to save the data
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='question_name'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='question_name'>Question *</FormLabel>
                      <FormControl>
                        <Textarea
                          id='question_name'
                          {...field}
                          placeholder='Type question'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='expected_answer'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel htmlFor='expected_answer'>
                        Excepted answer *
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          id='expected_answer'
                          {...field}
                          placeholder='Type excepted answer'
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {isPending && <IconLoader2 className='animate-spin' />}
                Save
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
