import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenuItem,
  DropdownMenuShortcut,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui";
import { useUpdateReview } from "@/hooks";
import { IReviewResponse, IUpdateReviewRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import React from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

const formSchema = z.object({
  id: z.string().trim(),
  isPublic: z.boolean().default(false),
});

export const UpdateReviewModal: React.FC<{ review: IReviewResponse }> = ({
  review,
}) => {
  const { mutateAsync: handleUpdateReview, isPending } = useUpdateReview();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: review.id,
      isPublic: review.isPublic,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateReviewRequest;
    console.log({ requestData: data });
    await handleUpdateReview(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <DropdownMenuItem
          className="cursor-pointer flex items-center gap-1"
          onSelect={(e) => e.preventDefault()}
        >
          <div>Duyệt đánh giá</div>
          <DropdownMenuShortcut>
            <Pencil1Icon className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DialogTrigger>
      <DialogContent className="overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Duyệt đánh giá</DialogTitle>
          <DialogDescription>
            Duyệt đánh giá. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="isPublic"
                  render={() => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="status">
                        Trạng thái (*)
                      </FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("isPublic") ? "1" : "0"}
                          onValueChange={(value: string) => {
                            form.setValue("isPublic", value === "1");
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Trạng thái" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectGroup>
                              <SelectItem value={"1"}>Hiển thị</SelectItem>
                              <SelectItem value={"0"}>Ẩn</SelectItem>
                            </SelectGroup>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-700 hover:bg-green-800 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
