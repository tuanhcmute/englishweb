import { zodResolver } from "@hookform/resolvers/zod";
import React from "react";
import { z } from "zod";
import { useForm } from "react-hook-form";
import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Textarea,
} from "@/components/ui";
import { IconFlag, IconLoader2 } from "@tabler/icons-react";
import { getCookie } from "cookies-next";
import { USER_CREDENTIAL_ID } from "@/constants";
import { ICreateReportCommentRequest } from "@/types";
import { useCreateReportComment } from "@/hooks";

const formSchema = z.object({
  commentId: z.string().trim().min(1),
  reportContent: z.string().trim().min(1),
  userCredentialId: z.string().trim().min(1),
});

export const CreateReportCommentModal: React.FC<{ commentId: string }> = ({
  commentId,
}) => {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const { mutateAsync: handleCreateReportComment, isPending } = useCreateReportComment();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      commentId,
      userCredentialId,
      reportContent: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    console.log({ values });
    const requestData = {
      ...values
    } as ICreateReportCommentRequest;

    console.log({ requestData });
    await handleCreateReportComment(requestData);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <div className="flex items-center gap-1">
          Báo cáo
          <IconFlag className="w-4" />
        </div>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Báo cáo bình luận</DialogTitle>
          <DialogDescription>
            Báo cáo bình luận. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="reportContent"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="reportContent">
                        Nội dung báo cáo *
                      </FormLabel>
                      <FormControl>
                        <Textarea
                          {...field}
                          id="reportContent"
                          placeholder="Nhập nội dung báo cáo"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Báo cáo
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
