import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { useUpdateFlashcardItem } from "@/hooks";
import { IFlashcardItemResponse, IUpdateFlashcardItemRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { IconLoader2 } from "@tabler/icons-react";
import React from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

import dynamic from "next/dynamic";
const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  word: z.string().default("1"),
  flashcardItemId: z.string().trim().min(1, {
    message: "Từ mới là bắt buộc",
  }),
  phonetic: z.string().trim(),
  meaning: z.string().trim().min(1, {
    message: "Phiên âm là bắt buộc",
  }),
  tags: z.string().trim().min(1, {
    message: "Loại từ là bắt buộc",
  }),
  examples: z.string().trim().min(1, {
    message: "Tên khóa học là bắt buộc",
  }),
});

export const UpdateFlashcardItemModal: React.FC<{
  flashcardItem?: IFlashcardItemResponse;
}> = ({ flashcardItem }) => {
  const { mutateAsync: handleUpdateFlashcardItem, isPending } =
    useUpdateFlashcardItem();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      examples: flashcardItem?.examples,
      flashcardItemId: flashcardItem?.id,
      meaning: flashcardItem?.meaning,
      phonetic: flashcardItem?.phonetic,
      tags: flashcardItem?.tags,
      word: flashcardItem?.word,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateFlashcardItemRequest;
    console.log({ requestData: data });
    await handleUpdateFlashcardItem(data);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant="link"
          className="p-0 italic text-yellow-500   h-8 lg:flex gap-1 items-center"
        >
          Chỉnh sửa từ này
        </Button>
      </DialogTrigger>
      <DialogContent className="sm:max-w-[600px] overflow-y-auto max-h-full">
        <DialogHeader>
          <DialogTitle>Chỉnh sửa từ</DialogTitle>
          <DialogDescription>
            Chỉnh sửa từ. Nhấn lưu để hoàn thành
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className="grid gap-4 py-4">
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="word"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="word">Từ mới *</FormLabel>
                      <FormControl>
                        <Input
                          {...field}
                          id="word"
                          placeholder="Nhập từ mới"
                          className="col-span-3"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="phonetic"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="phonetic">
                        Phiên âm (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          placeholder="Nhập phiên âm"
                          className="col-span-3"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="meaning"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="meaning">
                        Định nghĩa
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("meaning")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("meaning", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="tags"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="tags">
                        Loại từ (*)
                      </FormLabel>
                      <FormControl>
                        <Input
                          id="tags"
                          placeholder="Nhập loại từ"
                          className="col-span-3"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid grid-cols-1 gap-4">
                <FormField
                  control={form.control}
                  name="examples"
                  render={({ field }) => (
                    <FormItem className="">
                      <FormLabel className="" htmlFor="examples">
                        Ví dụ *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("examples")}
                          handleChange={({ data }) => {
                            form.setValue("examples", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type="submit"
                className="bg-green-600 hover:bg-green-700 text-white hover:text-white"
                disabled={isPending}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
