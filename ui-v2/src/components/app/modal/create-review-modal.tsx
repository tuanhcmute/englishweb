import {
  Button,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { useCreateReview } from "@/hooks";
import { ICreateReviewRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2, IconStar, IconStarFilled } from "@tabler/icons-react";
import { getCookie } from "cookies-next";
import dynamic from "next/dynamic";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);
export const formSchema = z.object({
  content: z.string().trim().min(1, {
    message: "Nội dung không được bỏ trống",
  }),
  courseId: z.string().trim().min(1, {
    message: "Mã khóa học là bắt buộc",
  }),
  userCredentialId: z.string().trim().min(1, {
    message: "Mã người dùng là bắt buộc",
  }),
  rating: z.number().min(1).max(5),
  image: z.string().trim(),
});

export const CreateReviewModal: React.FC<{ courseId: string }> = ({
  courseId,
}) => {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const { mutateAsync: handleCreateReview, isPending } = useCreateReview();
  const [rating, setRating] = useState<number>(0);
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      userCredentialId,
      content: "",
      courseId,
      rating: 1,
      image: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as ICreateReviewRequest;
    console.log({ requestData: data });
    await handleCreateReview(data);
  }

  function onClick(rating: number) {
    setRating(rating);
    form.setValue("rating", rating + 1);
  }

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          variant='default'
          size='sm'
          className='h-8 lg:flex gap-1 items-center bg-transparent hover:bg-purple-800 text-purple-800 dark:text-white hover:text-white border border-purple-800 text-[13px] mt-2'
        >
          <Pencil1Icon />
          Viết đánh giá
        </Button>
      </DialogTrigger>
      <DialogContent className='sm:max-w-[600px] overflow-y-auto max-h-full'>
        <DialogHeader>
          <DialogTitle>Đánh giá khóa học</DialogTitle>
          <DialogDescription>
            Đánh giá khóa học. Nhấn lưu để gửi
          </DialogDescription>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='rating'
                  render={({ field }) => (
                    <FormItem className='mx-auto'>
                      <FormLabel className='flex justify-center font-bold'>
                        Khóa học này có hữu ích *
                      </FormLabel>
                      <FormControl>
                        <div className='flex items-center gap-5 justify-center'>
                          {Array.from({ length: 5 }).map((_, index) => {
                            const Icon =
                              index <= rating ? IconStarFilled : IconStar;
                            return (
                              <Icon
                                onClick={() => onClick(index)}
                                key={index}
                                className='text-yellow-500 cursor-pointer w-14 h-14'
                              />
                            );
                          })}
                        </div>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='content'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='examples'>
                        Chia sẻ cảm nhận *
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("content")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("content", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  control={form.control}
                  name='image'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='image'>
                        Hình ảnh
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("image")}
                          handleChange={({ data }) => {
                            form.setValue("image", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <DialogFooter>
              <Button
                type='submit'
                className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
                disabled={isPending}
              >
                {false && <IconLoader2 className='animate-spin' />}
                Lưu
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};
