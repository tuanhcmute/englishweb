import { Button } from "@/components/ui";
import { useDeleteQuestion } from "@/hooks";
import { IQuestionResponse } from "@/types";
import { ResetIcon } from "@radix-ui/react-icons";
import React from "react";
import Confirm from "./confirm";

export const RemoveQuestionModal: React.FC<{ question: IQuestionResponse }> = ({
  question,
}) => {
  const { mutateAsync: handleDeleteQuestion } = useDeleteQuestion();

  return (
    <Confirm
      title={"Xóa câu hỏi"}
      description='Sau khi xóa, dữ liệu sẽ bị mất, nhấn xóa để tiếp tục'
      textConfirm='Xóa'
      onConfirm={async (close) => {
        await handleDeleteQuestion(question.id);
        close?.();
      }}
    >
      <Button
        variant='destructive'
        size='sm'
        className='h-8 lg:flex gap-1 items-center w-fit mt-4'
      >
        <ResetIcon />
        Xóa câu hỏi {question.questionNumber}
      </Button>
    </Confirm>
  );
};
