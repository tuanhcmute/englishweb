"use client";

import {
  CreateTestCategoryModal,
  CreateTestModal,
  GetPartInformationModal,
} from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { Pencil2Icon } from "@radix-ui/react-icons";

export const TestCategoryActionDropdown = () => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button
            variant='outline'
            className=' h-8 lg:flex gap-1 items-center text-purple-800 dark:text-white bg-transparent border border-purple-800 hover:bg-purple-800 hover:text-white'
          >
            <Pencil2Icon />
            Thao tác
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <CreateTestCategoryModal />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
