import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuShortcut,
  DropdownMenuTrigger,
} from "@/components/ui";
import { IFlashcardResponse } from "@/types";
import { DotsVerticalIcon } from "@radix-ui/react-icons";
import { IconChecklist } from "@tabler/icons-react";
import { useRouter } from "next/navigation";
import React from "react";
import { UpdateFlashcardModal } from "../modal";
import { RemoveFlashcardModal } from "../modal/remove-flashcard-modal";

export const FlashcardItemActionDropdown: React.FC<{
  flashcard: IFlashcardResponse;
}> = ({ flashcard }) => {
  const router = useRouter();

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <DotsVerticalIcon className="cursor-pointer" />
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <UpdateFlashcardModal flashcard={flashcard} />
        <DropdownMenuSeparator />
        <RemoveFlashcardModal flashcard={flashcard} />
        <DropdownMenuSeparator />
        <DropdownMenuItem
          className="cursor-pointer"
          onClick={() => {
            router.push(`flashcards/${flashcard.id}`);
          }}
        >
          Xem từ vựng
          <DropdownMenuShortcut>
            <IconChecklist className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
