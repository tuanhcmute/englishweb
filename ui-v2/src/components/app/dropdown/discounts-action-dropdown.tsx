"use client";

import { CreateDiscountModal } from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";

export const DiscountsActionDropdown = () => {
  const router = useRouter();

  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button
            variant='outline'
            className='h-8 lg:flex gap-1 items-center text-purple-800 dark:text-white bg-transparent border border-purple-800 hover:bg-purple-800 hover:text-white'
          >
            <Pencil1Icon />
            Thao tác
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <CreateDiscountModal />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
