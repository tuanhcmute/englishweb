"use client";

import {
  CreateNewsCategoryModal,
  CreateNewsModal,
} from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { IconEdit } from "@tabler/icons-react";

export const NewsCategoriesActionDropdown = () => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button
            variant='outline'
            className='h-8 lg:flex gap-1 items-center text-purple-800 dark:text-white bg-transparent border border-purple-800 hover:bg-purple-800 hover:text-white'
          >
            <IconEdit className='w-4' />
            Thao tác
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <CreateNewsCategoryModal />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
