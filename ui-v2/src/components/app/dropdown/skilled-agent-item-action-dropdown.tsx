"use client";

import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui";
import { ISkilledAgentResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";
import { useRouter } from "next/navigation";

export const SkilledAgentItemActionDropdown = ({
  item,
}: {
  item: ISkilledAgentResponse;
}) => {
  const router = useRouter();

  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant='ghost' className='h-8 w-8 p-0'>
            <MoreHorizontal className='h-4 w-4' />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <DropdownMenuItem
            className='cursor-pointer'
            onClick={() => {
              router.push(`skilled-agents/${item.id}`);
            }}
          >
            Agent Detail
          </DropdownMenuItem>
          <DropdownMenuSeparator />
          <DropdownMenuItem
            className='cursor-pointer'
            onClick={() => {
              router.push(`skilled-agents/${item.id}/test-chat`);
            }}
          >
            Test Chat With Agent
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
