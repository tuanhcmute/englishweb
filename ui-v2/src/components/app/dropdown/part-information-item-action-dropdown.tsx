"use client";

import {
  GetPartInformationModal,
  UpdateNewsCatgoryModal,
  UpdatePartInformationModal,
  UpdateTestCategoryModal,
} from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { IPartInformationResponse, ITestCategoryResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";

export const PartInformationItemActionDropdown = ({
  item,
}: {
  item: IPartInformationResponse;
}) => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant='ghost' className='h-8 w-8 p-0'>
            <MoreHorizontal className='h-4 w-4' />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <UpdatePartInformationModal partInformation={item} />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
