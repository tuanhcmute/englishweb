import { IDiscountResponse } from "@/types";
import React from "react";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { DotsVerticalIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import {
  AssignDiscountModal,
  UpdateDiscountModal,
} from "@/components/app/modal";

export const DiscountItemActionDropdown: React.FC<{
  discount: IDiscountResponse;
}> = ({ discount }) => {
  const router = useRouter();
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <DotsVerticalIcon className='cursor-pointer' />
      </DropdownMenuTrigger>
      <DropdownMenuContent className='w-56'>
        <UpdateDiscountModal discount={discount} />
        <AssignDiscountModal discount={discount} />
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
