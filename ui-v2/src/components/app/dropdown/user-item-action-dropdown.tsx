import { IUserCredentialResponse } from "@/types";
import React from "react";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui";
import { DotsVerticalIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import { DeleteUserCredentialModal, UpdateRoleModal } from "../modal";

export const UserItemActionDropdown: React.FC<{
  user: IUserCredentialResponse;
}> = ({ user }) => {
  const router = useRouter();
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <DotsVerticalIcon className="cursor-pointer" />
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <UpdateRoleModal user={user} />
        <DeleteUserCredentialModal user={user} />
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
