import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { IReviewResponse } from "@/types";
import { DotsVerticalIcon } from "@radix-ui/react-icons";
import React from "react";
import { UpdateReviewModal } from "../modal";

export const ReviewItemActionDropdown: React.FC<{
  review: IReviewResponse;
}> = ({ review }) => {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <DotsVerticalIcon className='cursor-pointer' />
      </DropdownMenuTrigger>
      <DropdownMenuContent align='end'>
        <UpdateReviewModal review={review} />
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
