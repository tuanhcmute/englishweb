"use client";

import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { INewsResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";
import { UpdateNewsModal } from "../modal";

export const NewsItemActionDropdown = ({ news }: { news: INewsResponse }) => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className="w-56">
          <UpdateNewsModal news={news} />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
