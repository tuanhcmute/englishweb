"use client";

import {
  GetPartInformationModal,
  UpdateNewsCatgoryModal,
  UpdateTestCategoryModal,
} from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { ITestCategoryResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";

export const TestCategoriesItemActionDropdown = ({
  category,
}: {
  category: ITestCategoryResponse;
}) => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant='ghost' className='h-8 w-8 p-0'>
            <MoreHorizontal className='h-4 w-4' />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <UpdateTestCategoryModal category={category} />
          <GetPartInformationModal categoryId={category.id} />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
