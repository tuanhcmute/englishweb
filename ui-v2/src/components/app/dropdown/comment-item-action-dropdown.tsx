import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui";
import { ICommentResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";
import React from "react";
import { GetReportCommentsModal, UpdateCommentModal } from "../modal";

export const CommentItemActionDropdown = ({
  comment,
}: {
  comment: ICommentResponse;
}) => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className="w-56">
          <UpdateCommentModal comment={comment} />
          <DropdownMenuSeparator />
          <GetReportCommentsModal comment={comment} />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
