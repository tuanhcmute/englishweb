"use client";

import { RemoveBannerModal, UpdateBannerModal } from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui";
import { IBannerResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";

export const BannerItemActionDropdown = ({
  banner,
}: {
  banner: IBannerResponse;
}) => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className="w-56">
          <UpdateBannerModal banner={banner} />
          <DropdownMenuSeparator />
          <RemoveBannerModal banner={banner} />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
