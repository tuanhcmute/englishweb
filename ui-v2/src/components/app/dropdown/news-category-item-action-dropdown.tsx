"use client";

import {
  UpdateNewsCatgoryModal,
  UpdateNewsModal,
} from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { INewsCategoryResponse, INewsResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";

export const NewsCategoriesItemActionDropdown = ({
  category,
}: {
  category: INewsCategoryResponse;
}) => {
  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant='ghost' className='h-8 w-8 p-0'>
            <MoreHorizontal className='h-4 w-4' />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <UpdateNewsCatgoryModal category={category} />
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
