"use client";

import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui";
import { ITestSuiteResponse } from "@/types";
import { MoreHorizontal } from "lucide-react";
import { useRouter } from "next/navigation";

export const TestSuiteItemActionDropdown = ({
  item,
}: {
  item: ITestSuiteResponse;
}) => {
  const router = useRouter();

  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant='ghost' className='h-8 w-8 p-0'>
            <MoreHorizontal className='h-4 w-4' />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className='w-56'>
          <DropdownMenuItem
            className='cursor-pointer'
            onClick={() => {
              router.push(`test-suites/${item.id}`);
            }}
          >
            Test suite details
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
