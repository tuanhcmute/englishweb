import { GetInvoiceModal } from "@/components/app/modal";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui";
import { IInvoiceResponse } from "@/types";
import { DotsVerticalIcon } from "@radix-ui/react-icons";
import { useRouter } from "next/navigation";
import React from "react";

export const OrderItemActionDropdown: React.FC<{
  invoice: IInvoiceResponse;
  orderId: string;
}> = ({ invoice, orderId }) => {
  const router = useRouter();
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <DotsVerticalIcon className='cursor-pointer' />
      </DropdownMenuTrigger>
      <DropdownMenuContent className='w-56'>
        <GetInvoiceModal invoice={invoice} orderId={orderId} />
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
