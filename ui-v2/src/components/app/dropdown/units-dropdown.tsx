import { Button } from "@/components/ui";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuPortal,
  DropdownMenuSeparator,
  DropdownMenuSub,
  DropdownMenuSubContent,
  DropdownMenuSubTrigger,
  DropdownMenuTrigger,
} from "@/components/ui";
import { useUnitsByCourse } from "@/hooks";
import { useRouter } from "next/navigation";
import React from "react";

export const UnitsDropdown: React.FC<{ courseId: string }> = ({ courseId }) => {
  const unitsByCourseQuery = useUnitsByCourse(courseId);
  const router = useRouter();

  if (unitsByCourseQuery.error) {
    return (
      <div className='font-bold text-center w-full'>
        {unitsByCourseQuery.error.message}
      </div>
    );
  }

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant='outline' className='border-purple-800 h-8'>
          Danh sách chương
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent className='w-64 overflow-y-scroll max-h-[500px]'>
        <DropdownMenuGroup>
          {unitsByCourseQuery.data?.data.map((item) => {
            if (item.lessons && item.lessons.length > 0) {
              return (
                <DropdownMenuSub key={item.id}>
                  <DropdownMenuSubTrigger className='cursor-pointer'>
                    {item.title}
                  </DropdownMenuSubTrigger>
                  <DropdownMenuPortal>
                    <DropdownMenuSubContent className='overflow-y-auto max-h-[300px] w-64'>
                      {item.lessons
                        .sort((a, b) => a.sequence - b.sequence)
                        .map((item) => {
                          return (
                            <div key={item.id}>
                              <DropdownMenuItem
                                className='cursor-pointer'
                                onClick={() =>
                                  router.push(`units?lessonId=${item.id}`)
                                }
                              >
                                {item.title}
                              </DropdownMenuItem>
                              <DropdownMenuSeparator />
                            </div>
                          );
                        })}
                    </DropdownMenuSubContent>
                  </DropdownMenuPortal>
                </DropdownMenuSub>
              );
            }
            return (
              <DropdownMenuItem key={item.id} className='cursor-pointer'>
                {item.title}
              </DropdownMenuItem>
            );
          })}
        </DropdownMenuGroup>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
