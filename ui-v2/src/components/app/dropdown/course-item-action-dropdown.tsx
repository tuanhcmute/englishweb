import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuShortcut,
  DropdownMenuTrigger,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { ICourseResponse } from "@/types";
import { DotsVerticalIcon } from "@radix-ui/react-icons";
import {
  IconChecklist,
  IconInfoCircle,
  IconMessage,
} from "@tabler/icons-react";
import { useRouter } from "next/navigation";
import React from "react";
import { UpdateCourseModal } from "../modal";

export const CourseItemActionDropdown: React.FC<{
  course: ICourseResponse;
}> = ({ course }) => {
  const router = useRouter();
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <DotsVerticalIcon className="cursor-pointer" />
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <UpdateCourseModal course={course} />
        <DropdownMenuSeparator />
        <DropdownMenuItem
          className="cursor-pointer flex items-center gap-1"
          onClick={() => {
            router.push(`courses/${course.id}`);
          }}
        >
          Thông tin chi tiết khóa học
          <DropdownMenuShortcut>
            <IconInfoCircle className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
        <DropdownMenuSeparator />
        <DropdownMenuItem
          className="cursor-pointer"
          onClick={() => {
            router.push(`courses/${course.id}/${ROUTES.UNITS}`);
          }}
        >
          Danh sách các bài học
          <DropdownMenuShortcut>
            <IconChecklist className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
        <DropdownMenuItem
          className="cursor-pointer"
          onClick={() => {
            router.push(`courses/${course.id}/${ROUTES.REVIEWS}`);
          }}
        >
          Quản lý đánh giá
          <DropdownMenuShortcut>
            <IconMessage className="w-4" />
          </DropdownMenuShortcut>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
