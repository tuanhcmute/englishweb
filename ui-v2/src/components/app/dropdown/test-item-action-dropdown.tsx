"use client";

import {
  RemoveTestModal,
  UpdateScoreConversionModal,
  UpdateTestModal,
} from "@/components/app/modal";
import {
  Button,
  Dialog,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuShortcut,
  DropdownMenuTrigger,
} from "@/components/ui";
import { ITestResponse } from "@/types";
import { IconFile3d, IconMessage } from "@tabler/icons-react";
import { MoreHorizontal } from "lucide-react";
import { useRouter } from "next/navigation";

export const TestItemActionDropdown = ({ test }: { test: ITestResponse }) => {
  const router = useRouter();

  return (
    <Dialog>
      <DropdownMenu>
        <DropdownMenuTrigger asChild>
          <Button variant="ghost" className="h-8 w-8 p-0">
            <MoreHorizontal className="h-4 w-4" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent className="w-56">
          <UpdateScoreConversionModal test={test} />
          <DropdownMenuSeparator />
          <UpdateTestModal test={test} />
          <DropdownMenuSeparator />
          <RemoveTestModal test={test} />
          <DropdownMenuSeparator />
          <DropdownMenuItem
            className="cursor-pointer"
            onClick={() => {
              router.push(`tests/${test.id}/comments`);
            }}
          >
            Quản lý bình luận
            <DropdownMenuShortcut>
              <IconMessage className="w-4" />
            </DropdownMenuShortcut>
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </Dialog>
  );
};
