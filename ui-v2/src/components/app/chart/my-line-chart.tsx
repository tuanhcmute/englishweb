"use client";
import { convertNumberToMoney } from "@/lib";
import { IRevenueResponse } from "@/types";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  Tooltip,
  PointElement,
  LineElement,
} from "chart.js";
import { Line } from "react-chartjs-2";

// Register ChartJS components using ChartJS.register
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip
);

// components/MyLineChart.tsx
// ...
ChartJS.register(CategoryScale /* ... */);
// ...
export const MyLineChart: React.FC<{ revenues: IRevenueResponse[] }> = ({
  revenues,
}) => {
  const getLabels = () => {
    if (!revenues) return [];
    return revenues.map((item) => item.createdDate);
  };
  const getValues = () => {
    if (!revenues) return [];
    return revenues.map((item) => item.totalAmount);
  };
  return (
    <Line
      data={{
        labels: getLabels(),
        datasets: [
          {
            data: getValues(),
            backgroundColor: "purple",
          },
        ],
      }}
    />
  );
};
