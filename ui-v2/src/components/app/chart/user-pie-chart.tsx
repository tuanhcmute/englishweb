"use client";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  Tooltip,
  PointElement,
  LineElement,
  ArcElement,
} from "chart.js";
import { Pie } from "react-chartjs-2";

// Register ChartJS components using ChartJS.register
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  ArcElement
);

// components/MyLineChart.tsx
// ...
ChartJS.register(LinearScale /* ... */);
// ...
export const UserPieChart: React.FC<{
  data: number[];
  labels: string[];
}> = ({ data, labels }) => {
  return (
    <Pie
      data={{
        labels,
        datasets: [
          {
            label: "User Pie chart",
            data,
            backgroundColor: [
              "rgb(255, 99, 132)",
              "rgb(54, 162, 235)",
              "rgb(255, 205, 86)",
            ],
            hoverOffset: 4,
          },
        ],
      }}
      options={{
        plugins: {
          tooltip: {
            callbacks: {
              label: (ctx) => {
                const value = ctx.raw || 0;
                return `${value}`;
              },
            },
          },
        },
      }}
    />
  );
};
