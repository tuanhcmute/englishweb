export * from "./banner";
export * from "./about-us";
export * from "./courses";
export * from "./understand";
export * from "./tests";
export * from "./reviews";
export * from "./register-advise";
export * from "./news";
