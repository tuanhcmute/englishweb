import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
  Button,
} from "@/components/ui"
import Link from "next/link"
import { ROUTES } from "@/constants"

export const Understand = () => {
  return (
    <section id="understand" className="bg-gray-50 py-10 dark:bg-slate-950">
      <div className="container">
        <div className="flex w-full items-start justify-between flex-col md:flex-row">
          <div className="w-full order-2 lg:w-1/2">
            <img
              src="https://toeicez.com/images/web/section1.png"
              alt="section1"
              className=""
            />
          </div>
          <div className="md:w-1/2 flex flex-col gap-3 md:order-2">
            <p className="text-3xl font-bold text-purple-800 uppercase dark:text-white">
              Vì Chúng Tôi Hiểu Bạn
            </p>
            <Accordion type="single" collapsible className="w-full text-sm">
              <AccordionItem value="item-1">
                <AccordionTrigger>
                  Bạn có nhiều thời gian rảnh nhưng không liên tục, cố định.
                </AccordionTrigger>
                <AccordionContent>
                  Học tập trên đa nền tảng Smartphone, Máy tính. Học tập mọi
                  lúc, mọi nơi, với những lộ trình cá thể hoá cho riêng bạn.
                </AccordionContent>
              </AccordionItem>
              <AccordionItem value="item-2">
                <AccordionTrigger>
                  Bạn đã từng học nhưng không có kết quả tốt.
                </AccordionTrigger>
                <AccordionContent>
                  Giải thích cặn kẽ, dịch Tiếng Việt. Giúp bạn thống kê điểm yếu
                  của mình, từ đó giúp bạn cải thiện nhanh hơn.
                </AccordionContent>
              </AccordionItem>
              <AccordionItem value="item-3">
                <AccordionTrigger>
                  Bạn muốn tìm một giải pháp giúp bạn học hiệu quả mọi lúc, mọi
                  nơi.
                </AccordionTrigger>
                <AccordionContent>
                  Giao diện làm bài thi TOEIC trên máy tính mô phỏng đề thi
                  thật, giúp bạn dễ dàng làm bài thi với trải nghiệm tốt nhất.
                  Hệ thống AI được thiết kế để giúp bạn đi theo lộ trình hiệu
                  quả nhất, từ dễ đến khó căn cứ vào trình độ hiện tại của bạn.
                </AccordionContent>
              </AccordionItem>
            </Accordion>
            <div className="flex justify-center lg:justify-start mb-5">
              <Link href={ROUTES.LOGIN}>
                <Button
                  variant="destructive"
                  className="w-fit bg-purple-700 text-white hover:bg-purple-800 hover:text-white"
                >
                  Trải nghiệm ngay
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
