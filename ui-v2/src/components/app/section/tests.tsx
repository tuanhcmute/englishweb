import {
  Button,
  Card,
  CardContent,
  CardDescription,
  CardTitle,
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
  Skeleton,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useTests } from "@/hooks";
import Link from "next/link";
import { useRouter } from "next/navigation";

export const Tests = () => {
  const { data, isLoading } = useTests();
  const router = useRouter();

  return (
    <section id="tests" className="">
      <div className="container">
        <div className="flex items-start md:justify-between justify-center">
          <p className="text-center text-3xl font-extrabold text-purple-800 dark:text-white">
            ĐỀ THI ONLINE
          </p>
          <Link href={ROUTES.TESTS} className="hidden md:block">
            <Button
              variant="destructive"
              className="bg-purple-700 text-white hover:bg-purple-800 hover:text-white"
            >
              Xem tất cả
            </Button>
          </Link>
        </div>
        <div className="mt-2">
          {isLoading ? (
            <div className="flex gap-2">
              {Array.from({ length: 4 }).map((_, index) => {
                return (
                  <div key={index} className="lg:basis-1/4 md:basis-1/3">
                    <Skeleton className=" h-[300px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
                  </div>
                );
              })}
            </div>
          ) : (
            <Carousel className="w-full">
              <CarouselContent>
                {data?.data.map((item) => {
                  return (
                    <CarouselItem
                      key={item.id}
                      className="lg:basis-1/4 md:basis-1/3"
                    >
                      <Card className="shadow-lg">
                        <div className="w-full flex justify-center">
                          <div
                            dangerouslySetInnerHTML={{ __html: item.thumbnail }}
                            className="w-full rounded-t-sm"
                          ></div>
                        </div>
                        <CardTitle
                          className="dark:text-gray-300 text-lg hover:text-purple-800 cursor-pointer px-3"
                          onClick={() => {
                            router.push(`${ROUTES.TESTS}/${item.id}`);
                          }}
                        >
                          {item.testName}
                        </CardTitle>
                        <CardContent className="px-3">
                          <CardDescription className="">
                            <div className="py-1 px-2 bg-blue-100 text-purple-800 w-fit rounded-full text-[11px]">
                              #{item.testCategory.testCategoryName}
                            </div>
                            <div className="mt-1 text-[12px] dark:text-white">
                              Số phần thi: {item.testCategory.numberOfPart} phần
                            </div>
                            <div className="mt-1 text-[12px] dark:text-white">
                              Số câu hỏi: {item.testCategory.totalQuestion} câu
                              hỏi
                            </div>
                            <div className="mt-1 text-[12px] dark:text-white">
                              Thời gian làm bài: {item.testCategory.totalTime}{" "}
                              phút
                            </div>
                          </CardDescription>
                        </CardContent>
                      </Card>
                    </CarouselItem>
                  );
                })}
              </CarouselContent>
              <CarouselPrevious className="hidden 2xl:block border border-purple-800 text-purple-800 hover:text-purple-800 dark:text-white dark:border-white" />
              <CarouselNext className="hidden 2xl:block border border-purple-800 text-purple-800 hover:text-purple-800 dark:text-white dark:border-white" />
            </Carousel>
          )}
        </div>
      </div>
    </section>
  );
};
