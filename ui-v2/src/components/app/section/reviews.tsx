"use client";

import {
  Avatar,
  AvatarFallback,
  Button,
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui";
import Link from "next/link";
import { AvatarImage } from "@radix-ui/react-avatar";
import { StarFilledIcon } from "@radix-ui/react-icons";
import { useReviews } from "@/hooks";

export const Reviews = () => {
  const { data } = useReviews({ isPublic: "1", page: "0", limit: 10 });

  return (
    <section id="comments" className="py-10 bg-gray-50 dark:bg-slate-950">
      <div className="container">
        <p className="text-3xl uppercase text-purple-800 font-bold text-center dark:text-white">
          CẢM NHẬN CỦA HỌC VIÊN
        </p>
        <div className="mt-5">
          <Carousel className="w-full">
            <CarouselContent>
              {data?.map((item, index) => (
                <CarouselItem key={index}>
                  <Card className="border-purple-500 dark:border-gray-700">
                    <CardHeader>
                      <CardTitle className="dark:text-gray-300 text-lg">
                        {item.courseName}
                      </CardTitle>
                    </CardHeader>
                    <CardContent className="pb-3">
                      <div className="flex items-start gap-2">
                        <Avatar>
                          <AvatarImage
                            src={item.userCredential.avatar}
                            alt={item.userCredential.avatar}
                          />
                          <AvatarFallback>CN</AvatarFallback>
                        </Avatar>
                        <div className="dark:text-gray-200">
                          <p className="font-bold">
                            {item.userCredential.fullName}
                          </p>
                          <p className="text-sm">{item.userCredential.major}</p>
                        </div>
                      </div>
                      <div className="flex items-center gap-2 mt-2">
                        {Array.from({ length: item.rating }).map((_, index) => {
                          return (
                            <StarFilledIcon
                              key={index}
                              className="text-yellow-500 w-6 h-6"
                            />
                          );
                        })}
                      </div>
                      <CardDescription className="mt-2">
                        <div
                          dangerouslySetInnerHTML={{ __html: item.content }}
                        ></div>
                      </CardDescription>
                    </CardContent>
                    <CardFooter className="">
                      <Link href={`/courses/${item.courseId}#course-reviews`}>
                        <Button
                          variant="outline"
                          className="dark:text-gray-200 border-purple-800 border text-purple-800 hover:text-white hover:bg-purple-800"
                        >
                          Xem chi tiết
                        </Button>
                      </Link>
                    </CardFooter>
                  </Card>
                </CarouselItem>
              ))}
            </CarouselContent>
            <CarouselPrevious className="hidden 2xl:block border border-purple-800 text-purple-800 hover:text-purple-800 dark:text-white dark:border-white" />
            <CarouselNext className="hidden 2xl:block border border-purple-800 text-purple-800 hover:text-purple-800 dark:text-white dark:border-white" />
          </Carousel>
        </div>
      </div>
    </section>
  );
};
