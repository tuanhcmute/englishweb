import {
  Badge,
  Button,
  Card,
  CardContent,
  CardHeader,
  CardTitle,
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
  Skeleton,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useCourses } from "@/hooks";
import { convertNumberToMoney } from "@/lib";
import { StackIcon } from "@radix-ui/react-icons";
import { IconMoneybag } from "@tabler/icons-react";
import Link from "next/link";
import { useRouter } from "next/navigation";

export const Courses = () => {
  const { data, isLoading } = useCourses({ isActive: "1" });
  const router = useRouter();

  return (
    <section id="courses" className="">
      <div className="container">
        <div className="flex items-start md:justify-between justify-center">
          <p className="text-center text-3xl font-extrabold text-purple-800 dark:text-white">
            KHÓA HỌC ONLINE
          </p>
          <Link href={ROUTES.COURSES} className="hidden md:block">
            <Button
              variant="destructive"
              className="bg-purple-700 text-white hover:bg-purple-800 hover:text-white dark:text-white"
            >
              Xem tất cả
            </Button>
          </Link>
        </div>
        <div className="mt-2">
          {isLoading ? (
            <div className="flex gap-2">
              {Array.from({ length: 4 }).map((_, index) => {
                return (
                  <div key={index} className="lg:basis-1/4 md:basis-1/3">
                    <Skeleton className=" h-[300px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
                  </div>
                );
              })}
            </div>
          ) : (
            <Carousel className="w-full">
              <CarouselContent className="">
                {data?.data.map((item) => {
                  return (
                    <CarouselItem
                      key={item.id}
                      className="lg:basis-1/4 md:basis-1/3"
                    >
                      <Card
                        key={item.id}
                        className="transition-shadow border-purple-300 dark:border-gray-700"
                      >
                        <CardHeader className="p-0">
                          <div
                            className="[&_*]:rounded-t-md"
                            dangerouslySetInnerHTML={{
                              __html: item.courseImg,
                            }}
                          ></div>
                          <CardTitle
                            onClick={() =>
                              router.push(`${ROUTES.COURSES}/${item.id}`)
                            }
                            className="hover:text-purple-800 transition-all cursor-pointer text-[16px] truncate px-5"
                          >
                            {item.courseName}
                          </CardTitle>
                        </CardHeader>
                        <CardContent className="flex flex-col gap-2 mt-2">
                          <p className="flex items-center gap-1 text-sm">
                            <IconMoneybag className="w-4" />
                            <span className="">Giá tiền cũ :</span>{" "}
                            <Badge variant={"destructive"}>
                              <div className="line-through text-[10px]">
                                {convertNumberToMoney(item.oldPrice)}
                              </div>
                            </Badge>
                          </p>
                          <p className="flex items-center gap-1 text-sm">
                            <StackIcon />
                            <span className="">Giảm giá:</span>{" "}
                            <Badge
                              variant="default"
                              className="bg-blue-100 text-purple-800 hover:text-purple-800 hover:bg-blue-100"
                            >
                              {item.percentDiscount}%
                            </Badge>
                          </p>
                          <p className="flex items-center gap-1 text-sm">
                            <IconMoneybag className="w-4" />
                            <span className="">Giá tiền hiện tại:</span>{" "}
                            <div className="py-1 px-3 rounded-full bg-green-500 text-white text-[12px]">
                              {convertNumberToMoney(item.newPrice)}
                            </div>
                          </p>
                        </CardContent>
                      </Card>
                    </CarouselItem>
                  );
                })}
              </CarouselContent>
              <CarouselPrevious className="hidden 2xl:block border border-purple-800 text-purple-800 hover:text-purple-800 dark:text-white dark:border-white" />
              <CarouselNext className="hidden 2xl:block border border-purple-800 text-purple-800 hover:text-purple-800 dark:text-white dark:border-white" />
            </Carousel>
          )}
        </div>
      </div>
    </section>
  );
};
