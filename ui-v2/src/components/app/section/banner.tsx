import { Carousel, CarouselContent, CarouselItem } from "@/components/ui";
import { useBanners } from "@/hooks";

export const Banner = () => {
  const bannersQuery = useBanners({ page: 0, limit: 5 });

  return (
    <section id="banner">
      <Carousel className="w-full">
        <CarouselContent>
          {bannersQuery.data?.data.map((item, index) => (
            <CarouselItem key={item.id}>
              <div
                className="h-[500px] flex justify-center"
                dangerouslySetInnerHTML={{ __html: item.image }}
              ></div>
            </CarouselItem>
          ))}
        </CarouselContent>
      </Carousel>
    </section>
  );
};
