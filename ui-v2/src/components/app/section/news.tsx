"use client";

import Link from "next/link";
import { Button, Skeleton } from "@/components/ui";
import { useNews } from "@/hooks";
import { IconCalendar } from "@tabler/icons-react";

export const News = () => {
  const top1NewsQuery = useNews({ page: "0", limit: "1" });
  const top4NewsQuery = useNews({ page: "1", limit: "3" });

  return (
    <section id="news" className="pb-10">
      <div className="container">
        <p className="text-3xl uppercase text-purple-800 font-bold text-center dark:text-white">
          TIN TỨC - BÀI VIẾT
        </p>
        <div className="flex items-start gap-3 justify-between mt-5 flex-col lg:flex-row">
          <div className="lg:w-1/2">
            {top1NewsQuery?.isLoading ? (
              <Skeleton className=" h-[500px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
            ) : (
              top1NewsQuery.data?.data.map((item) => {
                return (
                  <div className="" key={item.id}>
                    <div
                      className=""
                      dangerouslySetInnerHTML={{ __html: item.newsImage }}
                    ></div>
                    <div className="flex gap-1 mt-2 italic items-center">
                      <IconCalendar className="w-4" />
                      <p className="text-sm dark:text-gray-200">
                        {item.createdDate} {item.author.fullName}
                      </p>
                    </div>
                    <Link
                      href={`news/${item.id}`}
                      className="font-bold mt-2 block hover:text-purple-800 transition-colors w-fit dark:text-white"
                    >
                      {item.newsTitle}
                    </Link>
                    <p className="mt-1 text-[12px] italic">
                      {item.newsDescription}
                    </p>
                  </div>
                );
              })
            )}
          </div>
          <div className="lg:w-1/2">
            {top4NewsQuery?.isLoading ? (
              <div className="flex flex-col gap-2">
                {Array.from({ length: 4 }).map((_, index) => {
                  return (
                    <div key={index}>
                      <Skeleton className=" h-[150px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
                    </div>
                  );
                })}
              </div>
            ) : (
              top4NewsQuery.data?.data.map((item) => (
                <div
                  className="flex items-start gap-2 mb-5 border-b pb-3"
                  key={item.id}
                >
                  <div
                    className="w-1/3"
                    dangerouslySetInnerHTML={{ __html: item.newsImage }}
                  ></div>
                  <div className="w-2/3">
                    <div className="flex items-center gap-1 italic">
                      <IconCalendar className="w-4" />
                      <p className="dark:text-gray-200 text-sm">
                        {item.createdDate} {item.author.fullName}
                      </p>
                    </div>
                    <Link
                      href={`news/${item.id}`}
                      className="font-bold block hover:text-purple-800 transition-colors w-fit dark:text-white"
                    >
                      {item.newsTitle}
                    </Link>
                    <p className="mt-1 dark:text-gray-200 text-[12px] italic">
                      {item.newsDescription}
                    </p>
                  </div>
                </div>
              ))
            )}
          </div>
        </div>
        <div className="flex items-center justify-center mt-2">
          <Link href="/news">
            <Button
              variant="destructive"
              className="border-purple-800 bg-purple-700 border text-white hover:text-white hover:bg-purple-800"
            >
              Xem thêm
            </Button>
          </Link>
        </div>
      </div>
    </section>
  );
};
