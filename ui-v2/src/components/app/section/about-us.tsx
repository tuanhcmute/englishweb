import { Button } from "@/components/ui";
import { Skeleton } from "@/components/ui";
import { StaticPageCode } from "@/enums";
import { useStaticPageByCode } from "@/hooks";
import Link from "next/link";
import ReactPlayer from "react-player";

export const AboutUs = () => {
  const { data, isLoading } = useStaticPageByCode(StaticPageCode.ABOUT_US);

  return (
    <section id="about-us" className="py-10 bg-gray-50 dark:bg-slate-950">
      <div className="container flex items-start justify-between w-full gap-3 flex-col lg:flex-row">
        <div className="lg:w-1/2 w-full">
          <p className="uppercase text-3xl font-bold text-purple-800 text-center dark:text-white">
            Về chúng tôi
          </p>
          {isLoading ? (
            <Skeleton className="h-[300px] w-full mt-5 mb-2 rounded-xl bg-gray-200 dark:bg-slate-900" />
          ) : (
            <p className="mt-5 dark:text-gray-200 leading-6 text-sm text-justify">
              {data?.data.pagePreview}
            </p>
          )}
          <div className="flex justify-center lg:justify-start">
            <Link href="/about-us" className="block">
              <Button
                variant="ghost"
                className="w-fit bg-purple-700 text-white hover:bg-purple-800 hover:text-white"
              >
                Tìm hiểu thêm
              </Button>
            </Link>
          </div>
        </div>
        <div className="lg:w-1/2 w-full h-[400px]">
          <ReactPlayer
            url={`https://www.youtube.com/watch?v=${data?.data.pageUrlVideo}`}
            playing={false}
            controls
            width="100%"
            height="100%"
          />
        </div>
      </div>
    </section>
  );
};
