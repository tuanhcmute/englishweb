"use client";

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
  Avatar,
  AvatarFallback,
  Badge,
  Button,
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Textarea,
} from "@/components/ui";
import { ReactElement, ReactNode } from "react";
import { AppLayout } from "@/components/shared/layout";
import Image from "next/image";
import Link from "next/link";
import * as z from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { AvatarImage } from "@radix-ui/react-avatar";
import { CalendarIcon, StackIcon, StarIcon } from "@radix-ui/react-icons";
import { ROUTES } from "@/constants";
import { useCourses, useStaticPageByCode, useTests } from "@/hooks";
import { StaticPageCode } from "@/enums";
import { Loading } from "@/components/shared/loading";
import { DollarSign, DollarSignIcon, SigmaIcon } from "lucide-react";
import { convertNumberToMoney } from "@/lib";
import { useRouter } from "next/navigation";
import { IconMoneybag } from "@tabler/icons-react";
import {
  AboutUs,
  Banner,
  Courses,
  Tests,
  Understand,
} from "@/components/app/section";

const formSchema = z.object({
  username: z.string().min(2, {
    message: "Username must be at least 2 characters.",
  }),
  phonenumber: z.string().min(10).max(10),
  course: z.string(),
  description: z.string(),
});

export const RegisterAdvise = () => {
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: "",
      phonenumber: "",
      course: "",
      description: "",
    },
  });

  // 2. Define a submit handler.
  function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    console.log(values);
  }

  return (
    <section
      id='register-advise'
      className='py-10 bg-gray-50 dark:bg-slate-950'
    >
      <div className='container'>
        <p className='text-3xl uppercase text-blue-400 font-bold text-center'>
          ĐĂNG KÝ TƯ VẤN - NHẬN NGAY ƯU ĐÃI
        </p>
        <div className=''>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className='space-y-8'>
              <FormField
                control={form.control}
                name='username'
                render={({ field }) => (
                  <div className='mt-5'>
                    <div className='grid grid-cols-1 gap-5 lg:grid-cols-2'>
                      <FormItem>
                        <FormLabel>Họ và tên (*)</FormLabel>
                        <FormControl>
                          <Input placeholder='Nhập họ và tên' {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                      <FormItem>
                        <FormLabel>Họ và tên (*)</FormLabel>
                        <FormControl>
                          <Input placeholder='Nhập họ và tên' {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                      <FormItem>
                        <FormLabel>Họ và tên (*)</FormLabel>
                        <FormControl>
                          <Input placeholder='Nhập họ và tên' {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                      <FormItem>
                        <FormLabel>Họ và tên (*)</FormLabel>
                        <FormControl>
                          <Input placeholder='Nhập họ và tên' {...field} />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    </div>
                    <Textarea className='mt-5' />
                  </div>
                )}
              />
              <div className='flex justify-center'>
                <Button type='submit' variant='destructive'>
                  Gửi thông tin
                </Button>
              </div>
            </form>
          </Form>
        </div>
      </div>
    </section>
  );
};
