"use client";

import { MoonIcon, SunIcon } from "@radix-ui/react-icons";
import { useTheme } from "next-themes";

export function LearningTheme() {
  const { setTheme, theme } = useTheme();
  return (
    <label
      className={`relative m-0 block h-7 w-14 rounded-full ${
        theme === "dark" ? "bg-blue-600" : "bg-gray-200"
      }`}
    >
      <input
        type='checkbox'
        onChange={() => {
          if (typeof setTheme === "function") {
            setTheme(theme === "light" ? "dark" : "light");
          }
        }}
        className='dur absolute top-0 z-50 m-0 h-full w-full cursor-pointer opacity-0'
      />
      <span
        className={`absolute left-[3px] top-1/2 flex h-6 w-6 -translate-y-1/2 translate-x-0 items-center justify-center rounded-full bg-white shadow-switcher duration-75 ease-linear ${
          theme === "dark" && "!right-[3px] !translate-x-full"
        }`}
      >
        <span className='dark:hidden'>
          <SunIcon />
        </span>
        <span className='hidden dark:inline-block'>
          <MoonIcon className='text-gray-600' />
        </span>
      </span>
    </label>
  );
}
