import {
  Badge,
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui";
import { useDeleteLessonNote, useUpdateLessonNote } from "@/hooks";
import { millisToMinutesAndSeconds } from "@/lib";
import { useLearning } from "@/providers";
import { ILessonNoteResponse, IUpdateLessonNoteRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { Pencil1Icon, TrashIcon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import dynamic from "next/dynamic";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  id: z.string().trim().min(1),
  content: z.string().trim().min(1, {
    message: "Nội dung bài học là bắt buộc",
  }),
});

export const LessonNotesItem: React.FC<{ item: ILessonNoteResponse }> = ({
  item,
}) => {
  const { playerRef } = useLearning();
  const [edit, setEdit] = useState<boolean>(false);
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: item.id,
      content: item.content,
    },
  });
  const { mutateAsync: handleUpdateLessonNote, isPending } =
    useUpdateLessonNote();

  const { mutateAsync: handleDeleteLessonNote, isPending: isPendingDelete } =
    useDeleteLessonNote();

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateLessonNoteRequest;
    console.log({ data });
    await handleUpdateLessonNote(data);
  }

  return (
    <div className='' key={item.id}>
      <div className='flex items-center justify-between'>
        <div className='flex items-center gap-1'>
          <Badge
            className='bg-purple-800 text-white hover:bg-purple-900 transition-all hover:text-white cursor-pointer'
            onClick={() => {
              playerRef.current?.seekTo(item.noteTime);
            }}
          >
            {millisToMinutesAndSeconds(item.noteTime * 1000)}
          </Badge>
          <p>{item.lesson.title}</p>
        </div>
        <div className='flex items-center gap-2'>
          <Pencil1Icon
            onClick={() => setEdit(!edit)}
            className='w-5 h-5 text-purple-800 cursor-pointer hover:text-purple-900 transition-all'
          />
          <DropdownMenu>
            <DropdownMenuTrigger asChild>
              <TrashIcon className='w-5 h-5 text-purple-800 cursor-pointer hover:text-purple-900 transition-all' />
            </DropdownMenuTrigger>
            <DropdownMenuContent className='w-56'>
              <DropdownMenuLabel className='text-center'>
                Bạn muốn xóa ghi chú này
              </DropdownMenuLabel>
              <DropdownMenuSeparator />
              <div className='flex items-center justify-between p-4'>
                <Button className='h-8 bg-white text-black border-gray-200 border hover:bg-white hover:text-black'>
                  Hủy
                </Button>
                <Button
                  onClick={async () => {
                    await handleDeleteLessonNote(item.id);
                  }}
                  className='h-8'
                  variant='destructive'
                >
                  {isPendingDelete && <IconLoader2 className='animate-spin' />}
                  Xóa
                </Button>
              </div>
            </DropdownMenuContent>
          </DropdownMenu>
        </div>
      </div>
      {!edit ? (
        <div
          dangerouslySetInnerHTML={{ __html: item.content }}
          className='bg-gray-100 p-4 mt-2 rounded text-sm dark:bg-gray-700 dark:text-white'
        ></div>
      ) : (
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <div className='grid gap-4 py-4'>
              <div className='grid grid-cols-1 gap-4'>
                <FormField
                  name='content'
                  render={({ field }) => (
                    <FormItem className=''>
                      <FormLabel className='' htmlFor='content'>
                        Nội dung (*)
                      </FormLabel>
                      <FormControl>
                        <CustomEditor
                          initialData={form.getValues("content")}
                          handleChange={({ data }) => {
                            console.log({ data });
                            form.setValue("content", data);
                          }}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
            </div>
            <div className='flex items-center gap-3'>
              <Button variant='outline' onClick={() => setEdit(false)}>
                Hủy bỏ
              </Button>
              <Button className='bg-purple-800 text-white hover:bg-purple-900'>
                {isPending && <IconLoader2 className='animate-spin' />}
                Lưu lại
              </Button>
            </div>
          </form>
        </Form>
      )}
    </div>
  );
};
