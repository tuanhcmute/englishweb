import {
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui";
import { IconMessage } from "@tabler/icons-react";

export const QASheet = () => {
  return (
    <Sheet>
      <SheetTrigger asChild>
        <div className='flex items-center gap-1 fixed right-14 bottom-14 px-4 py-2 cursor-pointer hover:bg-green-700 transition-all ease-linear rounded-full text-sm bg-green-600 text-white'>
          <IconMessage className='w-4 h-4' />
          Hỏi đáp
        </div>
      </SheetTrigger>
      <SheetContent className='md:min-w-[600px] lg:min-w-[800px] min-w-full'>
        <SheetHeader>
          <SheetTitle>Hỏi đáp</SheetTitle>
        </SheetHeader>
        <div className='grid gap-4 py-4'></div>
      </SheetContent>
    </Sheet>
  );
};
