"use client";

import {
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { useLessonNotes } from "@/hooks";
import { IconNotebook } from "@tabler/icons-react";
import { getCookie } from "cookies-next";
import { useSearchParams } from "next/navigation";
import { LessonNotesItem } from "./lesson-note-item";

export const NoteSheet = () => {
  const searchParams = useSearchParams();
  const lessonId = searchParams.get("lessonId") as string;
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const lessonNotesQuery = useLessonNotes(userCredentialId, lessonId);

  return (
    <Sheet>
      <SheetTrigger asChild>
        <div className='flex items-center gap-1 text-sm cursor-pointer hover:text-purple-800'>
          <IconNotebook className='w-4 h-4' />
          Ghi chú
        </div>
      </SheetTrigger>
      <SheetContent className='md:min-w-[600px] lg:min-w-[800px] min-w-full overflow-y-auto'>
        <SheetHeader>
          <SheetTitle>Ghi chú của bạn</SheetTitle>
        </SheetHeader>
        <div className='grid gap-4 py-4'>
          {lessonNotesQuery.data?.data.map((item) => {
            return <LessonNotesItem item={item} key={item.id} />;
          })}
        </div>
      </SheetContent>
    </Sheet>
  );
};
