import { zodResolver } from "@hookform/resolvers/zod";
import React, { useState } from "react";
import { z } from "zod";
import { useForm, useFieldArray } from "react-hook-form";
import {
  Button,
  DialogFooter,
  Form,
  Input,
  Label,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
  Select as SystemSelect,
} from "@/components/ui";
import Select, { MultiValue, ActionMeta } from "react-select";
import makeAnimated from "react-select/animated";
import {
  useAssignDiscount,
  useCourseCategories,
  useCourses,
  useDiscountConditions,
} from "@/hooks";
import { IAssignDiscountRequest, IDiscountResponse } from "@/types";
import { MinusIcon, PlusIcon } from "@radix-ui/react-icons";
import { ConditionType } from "@/enums";

const animatedComponents = makeAnimated();

const conditionsSchema = z.object({
  type: z.string().trim().min(1, {
    message: "Loại điều kiện là bắt buộc",
  }),
  percentDiscount: z.number().min(1, {
    message: "Số phần trăm giảm là bắt buộc",
  }),
  courseIds: z.array(z.string()),
  categoryIds: z.array(z.string()),
});

const formSchema = z.object({
  discountId: z.string().trim().min(1, {
    message: "Mã giả giá là bắt buộc",
  }),
  conditions: z.array(conditionsSchema).max(2).min(0),
});

export const AssignDiscountForm: React.FC<{ discount: IDiscountResponse }> = ({
  discount,
}) => {
  const { mutateAsync: handleAssignDiscount } = useAssignDiscount();
  const coursesQuery = useCourses();
  const coursesCategoriesQuery = useCourseCategories();
  const discountConditionsQuery = useDiscountConditions(discount.id);
  const [types, setTypes] = useState<string[]>([]);

  // const loadConditions = () => {
  //   if (discountConditionsQuery.data?.data) {
  //     return discountConditionsQuery.data.data.map((condition) => {
  //       return condition.conditionTargets;
  //     });
  //   }
  //   return [];
  // };

  const loadOptions = (type: string) => {
    if (type === ConditionType.COURSE) {
      if (!coursesQuery.data?.data) return [];
      return coursesQuery.data.data.map((item) => {
        return {
          label: item.courseName,
          value: item.id,
        };
      });
    } else {
      if (!coursesCategoriesQuery.data?.data) return [];
      return coursesCategoriesQuery.data.data.map((item) => {
        return {
          label: item.courseCategoryName,
          value: item.id,
        };
      });
    }
  };
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      discountId: discount.id,
      conditions: discountConditionsQuery.data?.data || [],
    },
  });

  console.log(form.getValues());

  // [newValue: MultiValue<unknown>, actionMeta: ActionMeta<unknown>]
  const handleSelect = ({
    actionMeta,
    type,
    index,
  }: {
    newValue: MultiValue<{ label: string; value: string }>;
    actionMeta: ActionMeta<{ label: string; value: string }>;
    type: string;
    index: number;
  }) => {
    const key = type === ConditionType.COURSE ? "courseIds" : "categoryIds";
    const values = form.getValues(`conditions.${index}.${key}`);

    if (actionMeta.action === "remove-value") {
      const removedId = actionMeta.removedValue.value;
      if (removedId) {
        form.setValue(
          `conditions.${index}.${key}`,
          values.filter((item) => item !== removedId)
        );
      }
    } else if (actionMeta.action === "select-option") {
      const id = actionMeta.option?.value;
      if (id) {
        form.setValue(`conditions.${index}.${key}`, [...values, id]);
      }
    }
  };

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IAssignDiscountRequest;
    console.log({ requestData: data });
    await handleAssignDiscount(data);
  }

  const { append, remove, fields } = useFieldArray({
    name: "conditions",
    control: form.control,
  });

  function handleAddCondition() {
    append({
      type: "",
      percentDiscount: 1,
      courseIds: [],
      categoryIds: [],
    });
    setTypes([...types, ""]);
  }

  function handleRemoveCondition() {
    const size = form.getValues("conditions").length;
    remove(size - 1);
    setTypes([...types.slice(0, size - 1)]);
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-1 gap-4">
            <Label className="discountName">Tiêu đề (*)</Label>
            <Input
              id="discountName"
              placeholder="Nhập tên giảm giá"
              className="col-span-3 border-purple-800 border-2 text-gray-700"
              value={discount.discountName}
              disabled
            />
          </div>
          <div className="grid grid-cols-1 gap-4">
            <div className="flex items-center gap-2">
              <Button
                variant="outline"
                size="sm"
                className="h-8 lg:flex gap-1 items-center w-fit font-normal bg-gray-100 hover:bg-gray-200"
                onClick={handleRemoveCondition}
              >
                <MinusIcon />
                Xóa điều kiện
              </Button>
              <Button
                variant="outline"
                size="sm"
                className="h-8 lg:flex gap-1 items-center w-fit bg-purple-800 text-white font-normal hover:bg-purple-900 hover:text-white"
                onClick={handleAddCondition}
              >
                <PlusIcon />
                Thêm điều kiện
              </Button>
            </div>
          </div>
          <div className="grid gap-4 py-4">
            {fields.map((field, index) => {
              const errorForField = form.formState.errors?.conditions?.[index];
              const type = form.watch(`conditions.${index}.type`);

              return (
                <div className="grid grid-cols-1 gap-4" key={index}>
                  <p className="font-medium">Điều kiện {index + 1}</p>
                  <div className="pl-3 flex flex-col gap-4">
                    <div className="flex flex-col gap-2 w-full">
                      <Label>Loại</Label>
                      <SystemSelect
                        onValueChange={(value: string) => {
                          form.setValue(`conditions.${index}.type`, value);
                        }}
                        defaultValue={field.type}
                      >
                        <SelectTrigger className="w-full">
                          <SelectValue placeholder="Lựa chọn loại" />
                        </SelectTrigger>
                        <SelectContent>
                          <SelectGroup>
                            <SelectItem value={ConditionType.COURSE}>
                              Khóa học
                            </SelectItem>
                            <SelectItem value={ConditionType.COURSE_CATEGORY}>
                              Danh mục
                            </SelectItem>
                          </SelectGroup>
                        </SelectContent>
                      </SystemSelect>
                      {/* <p className='text-destructive text-sm'>
                          {errorForField?.type ?? <>&nbsp;</>}
                        </p> */}
                    </div>
                    <div className="flex flex-col gap-2">
                      <Label>Nhập giá trị giảm (%)</Label>
                      <Input
                        // {...form.register(
                        //   `conditions.${index}.percentDiscount` as const
                        // )}
                        placeholder="Nhập giá trị giảm"
                        defaultValue={field.percentDiscount}
                        type="number"
                        onChange={(e) => {
                          form.setValue(
                            `conditions.${index}.percentDiscount`,
                            +e.target.value
                          );
                        }}
                      />
                      <p className="text-destructive text-sm">
                        {errorForField?.percentDiscount?.message ?? <></>}
                      </p>
                    </div>
                    {type !== "" && (
                      <div className="flex flex-col gap-2 w-full">
                        <Label>Áp dụng khuyến mãi</Label>
                        <Select
                          className="text-sm"
                          closeMenuOnSelect={false}
                          components={animatedComponents}
                          isMulti
                          options={loadOptions(type)}
                          placeholder="Tìm kiếm"
                          onChange={(...e) =>
                            handleSelect({
                              newValue: e[0] as MultiValue<{
                                label: string;
                                value: string;
                              }>,
                              actionMeta: e[1] as ActionMeta<{
                                label: string;
                                value: string;
                              }>,
                              type,
                              index,
                            })
                          }
                        />
                      </div>
                    )}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <DialogFooter>
          <Button
            type="submit"
            className="bg-green-700 hover:bg-green-800 text-white hover:text-white"
            onClick={() => form.getValues()}
          >
            Lưu
          </Button>
        </DialogFooter>
      </form>
    </Form>
  );
};
