"use client";

import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import dynamic from "next/dynamic";
import { useUpdateLesson } from "@/hooks";
import { ILessonResponse, IUpdateLessonRequest } from "@/types";
import { IconLoader2 } from "@tabler/icons-react";
import { useState } from "react";

const formSchema = z.object({
  id: z.string().trim().min(1, {
    message: "Mã bài học là bắt buộc",
  }),
  title: z.string().trim().min(1, {
    message: "Tiêu đề là bắt buộc",
  }),
  content: z.string().trim().min(1, {
    message: "Nội dung bài học là bắt buộc",
  }),
  video: z.string().trim().min(1, {
    message: "Mã video là bắt buộc",
  }),
});

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

export const UpdateLessonForm: React.FC<{ lesson: ILessonResponse }> = ({
  lesson,
}) => {
  const [selectedVideo, setSelectedVideo] = useState<any>(lesson.video);
  const { mutateAsync: handleUpdateLesson, isPending } = useUpdateLesson();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: lesson.id || "",
      title: lesson.title || "",
      content: lesson.content || "",
      video: lesson.video || "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUpdateLessonRequest;
    console.log({ data });
    await handleUpdateLesson(data);
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className='grid gap-4 py-4'>
          <div className='grid grid-cols-1 gap-4'>
            <FormField
              name='title'
              render={({ field }) => (
                <FormItem className=''>
                  <FormLabel className='' htmlFor='title'>
                    Tiêu đề (*)
                  </FormLabel>
                  <FormControl>
                    <Input
                      {...field}
                      id='title'
                      placeholder='Nhập tiêu đề'
                      className='col-span-3'
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className='grid grid-cols-1 gap-4'>
            <FormField
              name='content'
              render={({ field }) => (
                <FormItem className=''>
                  <FormLabel className='' htmlFor='content'>
                    Nội dung bài học (*)
                  </FormLabel>
                  <FormControl>
                    <CustomEditor
                      initialData={form.getValues("content")}
                      handleChange={({ data }) => {
                        console.log({ data });
                        form.setValue("content", data);
                      }}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <div className='grid grid-cols-1 gap-4'>
            <FormField
              name='video'
              render={({ field }) => (
                <FormItem className=''>
                  <FormLabel className='' htmlFor='video'>
                    Video bài giảng (Youtube) (*)
                  </FormLabel>
                  <FormControl>
                    <Input
                      id='video'
                      placeholder='Nhập đường dẫn video. VD: (NESs1KPmtKM)'
                      className='col-span-3'
                      {...field}
                      // type='file'
                      onChange={(e) => {
                        form.setValue("video", e.target.value);
                        setSelectedVideo(e.target.value);
                      }}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            {selectedVideo && (
              <div className='grid grid-cols-1 gap-4'>
                <iframe
                  width={560}
                  height={315}
                  src={`https://www.youtube.com/embed/${selectedVideo}?si=kmdD3arPq4cs4a5D`}
                  title='YouTube video player'
                  allow='accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share'
                  referrerPolicy='strict-origin-when-cross-origin'
                  allowFullScreen
                />
              </div>
            )}
          </div>
        </div>
        <Button
          type='submit'
          className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
          // disabled={isPending}
          onClick={() => console.log(form.getValues())}
        >
          {isPending && <IconLoader2 className='animate-spin' />}
          Lưu
        </Button>
      </form>
    </Form>
  );
};
