"use client";
import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  PasswordInput,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { Provider } from "@/enums";
import { useCreateUserCredeatial, useSignUp } from "@/hooks";
import { IUserCredentialRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { IconLoader2 } from "@tabler/icons-react";
import { useForm } from "react-hook-form";
import { z } from "zod";

const UserCredentialSchema = z
  .object({
    username: z
      .string()
      .min(2, {
        message: "Tên đăng nhập gồm ít nhất 2 ký tự",
      })
      .max(100, {
        message: "Tên đăng nhập không được quá 100 ký tự",
      }),
    password: z
      .string()
      .min(6, { message: "Mật khẩu ít nhất 6 ký tự" })
      .regex(/[a-zA-Z]/, { message: "Mật khẩu phải chứa ít nhất 1 ký tự" })
      .regex(/[0-9]/, { message: "Mật khẩu phải chứa ít nhất một số" })
      .regex(/[^a-zA-Z0-9]/, {
        message: "Mật khẩu phải chứa ít nhất một ký tự đặc biệt",
      })
      .trim(),
    provider: z.string().trim(),
    confirmPassword: z.string().trim().min(1, {
      message: "Xác nhận mật khẩu là bắt buộc",
    }),
    phoneNumber: z.string().trim().min(1, {
      message: "Số điện thoại là bắt buộc",
    }),
    email: z.string().email({ message: "Email không hợp lệ" }).trim(),
    fullName: z.string().trim().min(1, {
      message: "Tên là bắt buộc",
    }),
  })
  .refine((data) => data.password === data.confirmPassword, {
    message: "Xác nhận mật khẩu không khớp",
    path: ["confirmPassword"],
  });

export const CreateUserCredentialForm = () => {
  const { mutateAsync: handleSignUp, isPending } = useCreateUserCredeatial();
  const form = useForm<z.infer<typeof UserCredentialSchema>>({
    resolver: zodResolver(UserCredentialSchema),
    defaultValues: {
      username: "",
      password: "",
      email: "",
      phoneNumber: "",
      confirmPassword: "",
      fullName: "",
      provider: Provider.LOCAL,
    },
  });

  async function onSubmit(values: z.infer<typeof UserCredentialSchema>) {
    const requestData = {
      ...values,
    } as IUserCredentialRequest;
    console.log({ requestData });
    await handleSignUp(requestData);
  }
  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="flex flex-col gap-2"
      >
        <FormField
          control={form.control}
          name="username"
          render={({ field }) => (
            <FormItem>
              <FormLabel htmlFor="username">Tên đăng nhập</FormLabel>
              <FormControl>
                <Input placeholder="Tên đăng nhập" {...field} id="username" />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Mật khẩu</FormLabel>
              <FormControl>
                <PasswordInput placeholder="Nhập mật khẩu" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="confirmPassword"
          render={({ field }) => (
            <FormItem>
              <FormLabel htmlFor="confirmPassword">Xác nhận mật khẩu</FormLabel>
              <FormControl>
                <PasswordInput
                  id="confirmPassword"
                  placeholder="Xác nhận mật khẩu"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="phoneNumber"
          render={({ field }) => (
            <FormItem>
              <FormLabel htmlFor="phoneNumber">Số điện thoại</FormLabel>
              <FormControl>
                <Input
                  placeholder="Số điện thoại"
                  {...field}
                  id="phoneNumber"
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="email"
          render={({ field }) => (
            <FormItem>
              <FormLabel htmlFor="email">Email</FormLabel>
              <FormControl>
                <Input
                  placeholder="Địa chỉ email"
                  {...field}
                  type="email"
                  id="email"
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="fullName"
          render={({ field }) => (
            <FormItem>
              <FormLabel htmlFor="fullName">Họ tên</FormLabel>
              <FormControl>
                <Input placeholder="Họ và tên" {...field} id="fullName" />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button
          type="submit"
          variant="destructive"
          className="w-fit text-white bg-purple-700 hover:bg-purple-800"
          disabled={isPending}
        >
          <div className="flex items-center gap-1">
            {isPending && <IconLoader2 className="animate-spin" />}
            Lưu lại
          </div>
        </Button>
      </form>
    </Form>
  );
};
