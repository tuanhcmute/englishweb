import React, { useState } from "react";
import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
} from "@/components/ui";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { ACCEPTED_VIDEO_TYPES, MAX_VIDEO_FILE_SIZE } from "@/constants";
import { ILessonResponse, IUploadVideoLessonRequest } from "@/types";
import { useUploadVideoLesson } from "@/hooks";
import { IconLoader2 } from "@tabler/icons-react";

const formSchema = z.object({
  id: z.string().trim().min(1, {
    message: "Mã bài học là bắt buộc",
  }),
  video: z
    .any()
    .refine(
      (file: File) => file?.size <= MAX_VIDEO_FILE_SIZE,
      `Hỗ trợ video tối đa 2GB.`
    )
    .refine((file: File) => {
      return ACCEPTED_VIDEO_TYPES.includes(file?.type);
    }, "Chỉ hỗ trợ video dạng " + ACCEPTED_VIDEO_TYPES.join(", ")),
});

export const UploadVideoLessForm: React.FC<{ lesson: ILessonResponse }> = ({
  lesson,
}) => {
  const [selectedVideo, setSelectedVideo] = useState<any>(undefined);
  const { mutateAsync: handleUploadVideoLesson, isPending } =
    useUploadVideoLesson();
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      id: lesson.id || "",
      video: lesson.video || "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as IUploadVideoLessonRequest;
    console.log({ data });
    await handleUploadVideoLesson(data);
  }

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className='grid gap-4 py-4'>
          <div className='grid grid-cols-1 gap-4'>
            <video controls className='w-2/3'>
              <source src={lesson.video} />
            </video>
          </div>
          <div className='grid grid-cols-1 gap-4'>
            <FormField
              name='video'
              render={({ field }) => (
                <FormItem className=''>
                  <FormLabel className='' htmlFor='video'>
                    Video bài giảng (*)
                  </FormLabel>
                  <FormControl>
                    <Input
                      id='video'
                      placeholder='Lựa chọn video'
                      className='col-span-3'
                      type='file'
                      onChange={(e) => {
                        const files = e.target?.files;
                        form.setValue("video", files?.item(0));
                        setSelectedVideo(files?.item(0));
                      }}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            {selectedVideo && (
              <div className='grid grid-cols-1 gap-4'>
                <video controls className='w-2/3'>
                  <source src={URL.createObjectURL(selectedVideo)} />
                </video>
              </div>
            )}
          </div>
        </div>
        <Button
          type='submit'
          className='bg-green-600 hover:bg-green-700 text-white hover:text-white'
          disabled={isPending}
        >
          {isPending && <IconLoader2 className='animate-spin' />}
          Lưu
        </Button>
      </form>
    </Form>
  );
};
