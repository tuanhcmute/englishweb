"use client";

import {
  Button,
  Drawer,
  DrawerClose,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { useCreateLessonNote } from "@/hooks";
import { millisToMinutesAndSeconds } from "@/lib";
import { ICreateLessonNoteRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { getCookie } from "cookies-next";
import dynamic from "next/dynamic";
import React, { RefObject } from "react";
import { useForm } from "react-hook-form";
import ReactPlayer from "react-player";
import { z } from "zod";

const CustomEditor = dynamic(
  async () => {
    return import("@/components/shared/editor").then((x) => x.CustomEditor);
  },
  { ssr: false }
);

const formSchema = z.object({
  userCredentialId: z.string().trim().min(1),
  noteTime: z.number().min(0),
  content: z.string().trim().min(1, {
    message: "Nội dung bài học là bắt buộc",
  }),
  lessonId: z.string().trim().min(1, {
    message: "Mã bài học là bắt buộc",
  }),
});

export const CreateNoteDrawer: React.FC<{
  currentTime: number;
  playerRef: RefObject<ReactPlayer>;
  setPlaying: (isPlaying: boolean) => void;
  lessonId: string;
}> = ({ currentTime, setPlaying, lessonId }) => {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const { mutateAsync: handleCreateLessonNote, isPending } =
    useCreateLessonNote();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      content: "",
      lessonId: lessonId,
      userCredentialId: userCredentialId,
      noteTime: currentTime,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const data = {
      ...values,
    } as ICreateLessonNoteRequest;
    console.log({ data });
    await handleCreateLessonNote(data);
  }

  return (
    <Drawer>
      <DrawerTrigger>
        <div
          onClick={() => {
            // playerRef.current?.props.playing = false;
            // console.log({ playerRef: playerRef.current });
            setPlaying(false);
            form.setValue("noteTime", currentTime);
          }}
          className='flex items-center gap-1 text-sm mr-20 py-2 px-4 rounded-full bg-gray-200 cursor-pointer hover:bg-gray-300 transition-all ease-linear dark:bg-gray-700 dark:hover:bg-gray-800'
        >
          <Pencil1Icon />
          Thêm ghi chú tại {millisToMinutesAndSeconds(currentTime * 1000)}
        </div>
      </DrawerTrigger>
      <DrawerContent>
        <div className='min-h-96 overflow-y-auto'>
          <DrawerHeader>
            <DrawerTitle>
              Thêm ghi chú tại {millisToMinutesAndSeconds(currentTime * 1000)}
            </DrawerTitle>
          </DrawerHeader>
          <DrawerFooter>
            <Form {...form}>
              <form onSubmit={form.handleSubmit(onSubmit)}>
                <div className='grid gap-4 py-4'>
                  <div className='grid grid-cols-1 gap-4'>
                    <FormField
                      name='content'
                      render={({ field }) => (
                        <FormItem className=''>
                          <FormLabel className='' htmlFor='content'>
                            Nội dung (*)
                          </FormLabel>
                          <FormControl>
                            <CustomEditor
                              initialData={form.getValues("content")}
                              handleChange={({ data }) => {
                                console.log({ data });
                                form.setValue("content", data);
                              }}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </div>
                </div>
                <div className='flex items-center gap-3'>
                  <DrawerClose>
                    <Button variant='outline'>Hủy bỏ</Button>
                  </DrawerClose>
                  <Button
                    className='bg-purple-800 text-white hover:bg-purple-900'
                    onClick={() => console.log(form.getValues())}
                  >
                    {isPending && <IconLoader2 className='animate-spin' />}
                    Tạo ghi chú
                  </Button>
                </div>
              </form>
            </Form>
          </DrawerFooter>
        </div>
      </DrawerContent>
    </Drawer>
  );
};
