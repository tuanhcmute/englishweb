export enum Role {
  ROLE_STUDENT = "ROLE_STUDENT",
  ROLE_ADMIN = "ROLE_ADMIN",
  ROLE_TEACHER = "ROLE_TEACHER",
}

export enum UserCredentialStatus {
  ACTIVE = "ACTIVE",
  DEACTIVE = "DEACTIVE",
}

export enum StaticPageCode {
  ABOUT_US = "ABOUT_US",
  POLICY = "POLICY",
}

export enum ErrorResponse {
  REDIS_UNCONNECTED = 1999,
  EMAIL_EXIST = 2000,
  USERNAME_EXIST = 2001,
  PASSWORD_NOT_MATCH = 2002,
  PROVIDER_NOT_FOUND = 2003,
  ROLE_NOT_FOUND = 2004,
  USER_EXIST = 2005,
  USERNAME_NOT_EXIST = 2007,
}

export enum PaymentMethod {
  VNPAY = "vnpay",
  PAYPAL = "paypal",
}

export enum FlashcardType {
  PERSONAL = "personal",
  SUGGESTION = "suggestion",
}

export enum QuestionGroupFileType {
  AUDIO = "audio",
  IMAGE = "image",
}

export enum PracticeType {
  // partials
  PARTIALS = "partials",
  FULL_TEST = "full_test",
}

export enum PrartType {
  LISTENING = "listening",
  READING = "reading",
}

export enum Provider {
  LOCAL = "local",
  GOOGLE = "google.com",
}

export enum NewsType {
  PENDING = "pending",
  REJECTED = "rejected",
  APPROVED = "approved",
}

export enum NewsCategoryType {
  VISIBLE = "visible",
  INVISIBLE = "invisible",
}

export enum DiscountStatus {
  SCHEDULED = "scheduled",
  RUNNING = "running",
  TERMINATED = "terminated",
  STOPPED = "stopped",
}

export enum ConditionType {
  COURSE = "course",
  COURSE_CATEGORY = "course_category",
}
