import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const millisToMinutesAndSeconds = (millis: number) => {
  const minutes = Math.floor(millis / 60000);
  const seconds = Number(((millis % 60000) / 1000).toFixed(0));
  return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
};

export function isBrowser(): boolean {
  return typeof window !== "undefined";
}

export function isSmallScreen(): boolean {
  return isBrowser() && window.innerWidth < 768;
}

export const parseJsonToObject = <T>(json: string) => {
  try {
    return JSON.parse(json) as T;
  } catch (error) {
    return null;
  }
};

export function convertNumberToMoney(number: number): string {
  return `${number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} VNĐ`;
}
