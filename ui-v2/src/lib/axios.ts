import { ACCESS_TOKEN_KEY, BASE_URL, CHATBOT_BASE_URL } from "@/constants";
import { authService } from "@/services";
import { IBaseResponse } from "@/types";
import axios, { AxiosInstance } from "axios";
import { getCookie } from "cookies-next";

const http: AxiosInstance = axios.create({
  baseURL: BASE_URL,
});

const httpChatbotServer: AxiosInstance = axios.create({
  baseURL: CHATBOT_BASE_URL,
});

http.interceptors.request.use(
  (config) => {
    const accessToken = getCookie(ACCESS_TOKEN_KEY);
    // console.log({ accessToken });
    if (accessToken)
      http.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// http.interceptors.request.use(
//   function (response) {
//     return response.data;
//   },
//   async function (error) {
//     const originalRequest = error.config;

//     if (error.response?.status === 401 && !originalRequest._retry) {
//       originalRequest._retry = true;

//       const refreshToken = getCookie(REFRESH_TOKEN_KEY);
//       try {
//         const data = await authService.refreshToken(refreshToken as string);
//         setCookie(ACCESS_TOKEN_KEY, data.data.accessToken);
//         setCookie(REFRESH_TOKEN_KEY, data.data.refreshToken);

//         http.defaults.headers.common[
//           "Authorization"
//         ] = `Bearer ${data.data.accessToken}`;
//         return http(originalRequest);
//       } catch (error) {
//         return Promise.reject(error);
//       }
//     }
//     return Promise.reject(error);
//   }
// );

const httpCallApi = async <T = unknown>(
  url: string,
  accessToken: string,
  query?: any
) => {
  const res: IBaseResponse<T> = await axios
    .get(`${BASE_URL}${url}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
      withCredentials: true,
      params: query,
    })
    .then((r) => r.data);

  return res;
};

const httpServer = async <T = unknown>(
  {
    accessToken,
    refreshToken,
  }: {
    accessToken: string | undefined;
    refreshToken: string | undefined;
  },
  url: string,
  query?: Record<string, any>
) => {
  try {
    console.log({ accessToken });
    const res = await httpCallApi<T>(url, accessToken as string, query);
    return res;
  } catch (error: any) {
    if (error.response?.status === 401) {
      const data = await authService.refreshToken(refreshToken as string);

      const res = await httpCallApi<T>(url, data.data.accessToken, query);

      return res;
    }

    return Promise.reject(error);
  }
};
export { http, httpServer, httpChatbotServer };
