"use client";

import { IResultChoice, IUserChoice, IUserMark } from "@/types";
import { createContext, useContext, useState } from "react";

interface IPracticeContext {
  choices: IUserChoice[];
  handleChoose: (choice: IUserChoice) => void;
  handleSubmit: () => void;
  isQuestionChose: (choice: IUserChoice) => boolean;
  marks: IUserMark[];
  isMarked: (mark: IUserMark) => boolean;
  toggleMark: (mark: IUserMark) => void;
  handleGetChoicesResult: () => IResultChoice[];
  isChecked: (choice: IUserChoice) => boolean;
  handleRemoveChoice: (choice: IUserChoice) => void;
}

interface Props {
  children: React.ReactNode;
  testId: string;
}

export const PracticeContext = createContext<IPracticeContext>({
  choices: [],
  handleChoose(choice) {},
  handleSubmit() {},
  isQuestionChose(choice) {
    return false;
  },
  marks: [],
  isMarked(mark) {
    return false;
  },
  isChecked(choice: IUserChoice) {
    return false;
  },
  toggleMark(mark) {},
  handleGetChoicesResult() {
    return [];
  },
  handleRemoveChoice(choice: IUserChoice) {},
});

export const PracticeProvider = ({ children, testId }: Props) => {
  const [choices, setChoices] = useState<IUserChoice[]>([]);
  const [marks, setMarks] = useState<IUserMark[]>([]);
  // const [choicesResult, setChoicesResult] = useState<IResultChoice[]>([]);

  const isQuestionChose = (userChoice: IUserChoice) => {
    return choices.some(
      (c) =>
        c.partId === userChoice.partId &&
        c.questionGroupId === userChoice.questionGroupId &&
        c.questionId === userChoice.questionId
    );
  };

  const isMarked = (mark: IUserMark) => {
    return marks.some(
      (m) =>
        m.partId === mark.partId &&
        m.questionGroupId === mark.questionGroupId &&
        m.questionId === mark.questionId
    );
  };

  const handleChoose = (choice: IUserChoice) => {
    if (isQuestionChose(choice)) {
      setChoices((prev) => {
        return prev.map((c) => {
          if (c.questionId === choice.questionId) {
            return {
              ...c,
              answerId: choice.answerId,
            };
          }
          return c;
        });
      });
    } else {
      setChoices((prev) => {
        return [...prev, choice];
      });
    }
  };

  const toggleMark = (mark: IUserMark) => {
    setMarks((prev) => {
      if (isMarked(mark)) {
        return prev.filter((m) => m.questionId !== mark.questionId);
      } else {
        return [...prev, mark];
      }
    });
  };
  const isChecked = (choice: IUserChoice) => {
    return choices.some(
      (c) =>
        c.partId === choice.partId &&
        c.questionGroupId === choice.questionGroupId &&
        c.questionId === choice.questionId &&
        c.answerId === choice.answerId
    );
  };

  const handleSubmit = () => {};

  const handleGetChoicesResult = () => {
    const results: IResultChoice[] = [];
    choices.forEach((choice) => {
      const result = results.find((part) => part.partId === choice.partId);
      if (!result)
        results.push({
          partId: choice.partId,
          questionGroups: [
            {
              questionGroupId: choice.questionGroupId,
              questions: [
                { questionId: choice.questionId, answerId: choice.answerId },
              ],
            },
          ],
        });
      else {
        const questionGroup = result.questionGroups.find(
          (questionGroup) =>
            questionGroup.questionGroupId === choice.questionGroupId
        );
        if (!questionGroup) {
          result.questionGroups.push({
            questionGroupId: choice.questionGroupId,
            questions: [
              { questionId: choice.questionId, answerId: choice.answerId },
            ],
          });
        } else {
          questionGroup?.questions.push({
            questionId: choice.questionId,
            answerId: choice.answerId,
          });
          const index = result.questionGroups.findIndex(
            (questionGroup) =>
              questionGroup.questionGroupId === choice.questionGroupId
          );
          result.questionGroups.splice(index, 1, questionGroup);
        }
      }
    });
    return results;
  };

  const handleRemoveChoice = (choice: IUserChoice) => {
    setChoices((pre) => {
      return pre.filter((c) => c.questionId !== choice.questionId);
    });
  };

  return (
    <PracticeContext.Provider
      value={{
        handleGetChoicesResult,
        choices,
        handleChoose,
        handleSubmit,
        isQuestionChose,
        marks,
        isMarked,
        toggleMark,
        isChecked,
        handleRemoveChoice,
      }}
    >
      {children}
    </PracticeContext.Provider>
  );
};

export const usePractice = () => {
  const ctx = useContext(PracticeContext);
  if (ctx === undefined) {
    throw new Error("Ctx without provider");
  }

  return ctx;
};
