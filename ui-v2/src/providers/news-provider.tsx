"use client";

import { ReactNode, createContext, useContext, useState } from "react";

export interface INewsContext {
  newsCategoryId: string;
  setNewsCategoryId: (newsCategoryId: string) => void;
}
export interface INewsProviderProps {
  children: ReactNode;
}

export const NewsContext = createContext<INewsContext>({
  newsCategoryId: "",
  setNewsCategoryId: (newsCategoryId: string) => {},
});

export const NewsProvider: React.FC<INewsProviderProps> = ({ children }) => {
  const [newsCategoryId, setNewsCategoryId] = useState<string>("");
  return (
    <NewsContext.Provider value={{ newsCategoryId, setNewsCategoryId }}>
      {children}
    </NewsContext.Provider>
  );
};

export const useNewsContext = () => {
  const { newsCategoryId, setNewsCategoryId } = useContext(NewsContext);
  // if (!newsCategoryId || !setNewsCategoryId)
  //   throw new Error("useNewsContext must be used within a NewsProvider");
  return { newsCategoryId, setNewsCategoryId };
};
