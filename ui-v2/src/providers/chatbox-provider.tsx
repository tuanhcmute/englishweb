"use client";

export interface IChatMessage {
  isBotMessage: boolean;
  messageContent: string;
  createdDate?: string;
}

export interface IChatboxContext {
  isOpen: boolean;
  setOpen: (isOpen: boolean) => void;
  isClosing: boolean;
  setIsClosing: (isClosing: boolean) => void;
  conversation: IChatMessage[];
  setConversation: (chatMessage: IChatMessage) => void;
}
