"use client";

import { createContext, ReactNode, useContext, useRef } from "react";
import ReactPlayer from "react-player";

interface ILearningContext {
  playerRef: React.RefObject<ReactPlayer> | null;
  setPlayerRef: (ref: React.RefObject<ReactPlayer>) => void;
}

export const LearningContext = createContext<ILearningContext>({
  playerRef: null,
  setPlayerRef: () => {},
});

export const LearningProvider = ({ children }: { children: ReactNode }) => {
  const playerRef = useRef<ReactPlayer | null>(null);

  const setPlayerRef = (ref: React.RefObject<ReactPlayer>) => {
    playerRef.current = ref.current;
  };

  return (
    <LearningContext.Provider value={{ playerRef, setPlayerRef }}>
      {children}
    </LearningContext.Provider>
  );
};

export const useLearning = () => {
  const { playerRef, setPlayerRef } = useContext(LearningContext);

  if (!playerRef || !setPlayerRef) {
    throw new Error("useLearning must be used within a LearningProvider");
  }

  return { playerRef, setPlayerRef };
};
