import { PROTECTED_ROUTES, ROUTES, USER_CREDENTIAL } from "@/constants";
import { NextRequest, NextResponse } from "next/server";
import { Role } from "./enums";
import { parseJsonToObject } from "./lib";
import { IUserCredentialResponse } from "./types";

export function middleware(request: NextRequest) {
  const userCredentialJson = request.cookies.get(USER_CREDENTIAL)?.value;
  const prevPath = request.headers.get("Referer") || ROUTES.HOME;
  const response = NextResponse.next();
  // Set prev path
  response.cookies.set({
    name: "prevPath",
    value: prevPath,
    path: "/",
  });

  if (!userCredentialJson) {
    // Redirect login page if user has not authenticated yet
    const isMatch = PROTECTED_ROUTES.some((r) =>
      request.nextUrl.pathname.startsWith(r)
    );
    if (isMatch) return Response.redirect(new URL(ROUTES.LOGIN, request.url));
  } else {
    // Redirect if user authenticated
    if (request.nextUrl.pathname.startsWith(ROUTES.LOGIN))
      return Response.redirect(new URL(prevPath, request.url));

    // Redirect when student try to access admin page
    const userCredential = parseJsonToObject(
      userCredentialJson
    ) as IUserCredentialResponse;
    if (
      request.nextUrl.pathname.startsWith(ROUTES.ADMIN) &&
      userCredential.userAuthorities.find(
        (item) => item.role.roleName === Role.ROLE_STUDENT
      )
    )
      return Response.redirect(new URL(ROUTES.HOME, request.url));

    return response;
  }
}

export const config = {
  unstable_allowDynamic: [
    '/node_modules/lodash/lodash.js',
  ],
}
