import { ICreateConditionRequest } from "./discount-condition.type";

export interface IDiscountResponse {
  id: string;
  discountName: string;
  startDate: string;
  endDate: string;
  createdDate: string;
  lastModifiedDate: string;
  status: string;
}

export interface ICreateDiscountRequest {
  discountName: string;
  startDate: string;
  endDate: string;
  percentDiscount: number;
  status: string;
}

export interface IUpdateDiscountRequest {
  id: string;
  discountName: string;
  startDate: string;
  endDate: string;
  percentDiscount: number;
  status: string;
}

export interface IAssignDiscountRequest {
  discountId: string;
  conditions: ICreateConditionRequest[];
}
