import { IInvoiceResponse } from "./invoice.type";
import { IOrderLineResponse } from "./order-line.type";
import { IUserCredentialResponse } from "./user-credential.type";

export interface IOrderResponse {
  id: string;
  orderDate: string;
  paymentStatus: boolean;
  userCredential: IUserCredentialResponse;
  invoice: IInvoiceResponse;
  orderLines: IOrderLineResponse[];
}
