export interface ITestSuiteQuestionResponse {
  expected_answer: string;
  id: string;
  question_name: string;
  test_suite_id: string;
}

export interface ICreateTestSuiteQuestionRequest {
  expected_answer: string;
  question_name: string;
  test_suite_id: string;
}
