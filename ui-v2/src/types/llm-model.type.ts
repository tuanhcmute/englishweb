export interface ILLMModelResponse {
  llm_model_name: string;
  param: string;
  id: string;
}

export interface ICreateLLMModelRequest {
  llm_model_name: string;
  param: string;
}
