import { IPartInformation } from "./part-information.type";

export interface ITestCategory {
  id: string;
  testCategoryName: string;
  numberOfPart: number;
  totalQuestion: number;
  partInformation: IPartInformation[];
}

export interface ITestCategoryResponse {
  id: string;
  testCategoryName: string;
  numberOfPart: number;
  totalQuestion: number;
  totalTime: number;
  createdDate: string;
  lastModifiedDate: string;
}

export interface ITestCategoryRequest {
  testCategoryName: string;
  numberOfPart: number;
  totalQuestion: number;
  totalTime: number;
}

export interface IUpdateTestCategoryRequest {
  id: string;
  testCategoryName: string;
  numberOfPart: number;
  totalQuestion: number;
  totalTime: number;
}
