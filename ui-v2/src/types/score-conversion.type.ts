export interface IScoreConversionResponse {
  id: string;
  jsonScore: string;
}

export interface IUpdateScoreConversionRequest {
  id: string;
  jsonScore: string;
}
