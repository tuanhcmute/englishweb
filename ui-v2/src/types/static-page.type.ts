import { IUserCredentialResponse } from "./user-credential.type";

export interface ICreateStaticPageRequest {
  pageCode: string;
  pageName: string;
  pageContent: string;
  userCredentialId: string;
}

export interface IStaticPageResponse {
  pageCode: string;
  pageName: string;
  pageContent: string;
  createdBy: IUserCredentialResponse;
  id: string;
  lastModifiedDate: string;
  createdDate: string;
  pagePreview: string;
  pageUrlVideo: string;
}

export interface IUpdateStaticPageRequest {
  id: string;
  pageCode: string;
  pageName: string;
  pageContent: string;
  pagePreview: string;
  pageUrlVideo: string;
}
