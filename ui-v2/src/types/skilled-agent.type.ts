import { ITestSuiteResponse } from "./test-suite.type";

export interface ISkilledAgentResponse {
  id: string;
  setup_message: string;
  agent_name: string;
  description: string;
  test_score: number;
  test_suites: ITestSuiteResponse[];
}

export interface ICreateSkilledAgentRequest {
  setup_message: string;
  agent_name: string;
  description: string;
  test_score: number;
  test_suite_ids: string[];
}
