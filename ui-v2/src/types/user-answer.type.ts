import { IResultChoice } from "./answer-json.type";
import { ITestResponse } from "./test.type";

export interface IUserAnswerRequest {
  testId: string;
  userCredentialId: string;
  results: string;
  practiceType: string;
  completionTime: number;
}

export interface IUserAnswerResponse {
  id: string;
  answerJson: string;
  testDate: string;
  listeningCorrectAnsCount: number;
  readingCorrectAnsCount: number;
  listeningWrongAnsCount: number;
  readingWrongAnsCount: number;
  ignoreAnsCount: number;
  completionTime: number;
  totalSentences: number;
  totalScore: number;
  test: ITestResponse;
  practiceType: string;
}
