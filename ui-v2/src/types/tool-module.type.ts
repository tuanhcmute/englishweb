export interface IToolModuleResponse {
  tool_name: string;
  param: null;
  id: string;
}
