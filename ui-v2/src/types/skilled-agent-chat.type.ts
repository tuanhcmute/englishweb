export interface ISkilledAgentChatResponse {
  id: string;
  message: string;
  is_kguru_msg: boolean;
  skilled_agent_version_id: string;
  user_id: string;
}

export interface ICreateSkilledAgentChatRequest {
  message: string;
  is_kguru_msg: boolean;
  skilled_agent_version_id: string;
  user_id: string;
}
