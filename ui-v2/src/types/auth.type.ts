export interface IForgotPasswordRequest {
  email: string;
  newPassword: string;
  confirmPassword: string;
  otp: string;
}
