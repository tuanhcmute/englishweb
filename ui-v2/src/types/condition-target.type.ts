import { ICourseCategoryResponse } from "./course-category.type";

export interface IConditionTargetResponse {
  id: string;
  course: IDiscountCourseResponse;
  courseCategory: ICourseCategoryResponse;
}

export interface IDiscountCourseResponse {
  id: string;
  courseName: string;
  createdDate: string;
  lastModifiedDate: string;
}
