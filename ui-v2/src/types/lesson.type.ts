export interface ILessonResponse {
  id: string;
  title: string;
  time: number;
  video: string;
  content: string;
  createdDate: string;
  lastModifiedDate: string;
  sequence: number;
}

export interface ICreateLessonRequest {
  title: string;
  unitId: string;
}

export interface IUpdateLessonRequest {
  id: string;
  title: string;
  content: string;
  video: string;
}

export interface IUploadVideoLessonRequest {
  id: string;
  video: File;
}
