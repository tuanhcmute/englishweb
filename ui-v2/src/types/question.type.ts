import {
  ITestAnswer,
  ICreateTestAnswerRequest,
  ITestAnswerResponse,
  IUpdateTestAnswerRequest,
} from "./test-answer.type";

export interface IQuestion {
  id?: string;
  questionContent: string;
  questionNumber: string;
  rightAnswer: string;
  questionGuide: string;
  testAnswers: ITestAnswer[];
}

export interface ICreateQuestionRequest {
  questionContent: string;
  questionNumber: number;
  // rightAnswer: string;
  questionGuide: string;
  questionGroupId: string;
  testAnswers: ICreateTestAnswerRequest[];
}

export interface IUpdateQuestionRequest {
  id: string;
  questionContent: string;
  questionNumber: number;
  questionGuide: string;
  testAnswers: IUpdateTestAnswerRequest[];
}

export interface IQuestionResponse {
  id: string;
  questionContent: string;
  questionNumber: number;
  rightAnswer: string;
  questionGuide: string;
  testAnswers: ITestAnswerResponse[];
}
