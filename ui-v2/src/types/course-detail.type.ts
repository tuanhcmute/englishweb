import { ICourseResponse } from "./course.type";

export interface ICourseDetail {
  id: string;
  courseIntroduction: string;
  courseTarget: string;
  courseInformation: string;
  courseGuide: string;
  course: ICourseResponse;
}

export interface ICourseDetailResponse extends ICourseDetail {}
export interface IUpdateCourseDetailRequest {
  courseIntroduction: string;
  courseTarget: string;
  courseInformation: string;
  courseGuide: string;
}
