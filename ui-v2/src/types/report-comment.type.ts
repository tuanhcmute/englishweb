export interface IReportCommentResponse {
  id: string;
  reportContent: string;
  userCredential: {
    username: string;
    id:string;
    avatar:string;
  }
}

export interface IGetReportCommentRequest {
  commentId?: string;
}

export interface ICreateReportCommentRequest {
  commentId:string;
  reportContent:string;
  userCredentialId:string;
}
