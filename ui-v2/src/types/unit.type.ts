import { ILessonResponse } from "./lesson.type";

export interface IUnitResponse {
  id: string;
  title: string;
  totalLesson: number;
  totalHours: number;
  lessons: ILessonResponse[];
  createdDate: string;
  lastModifiedDate: string;
  sequence: number;
}

export interface ICreateUnitRequest {
  title: string;
}

export interface IUpdateUnitRequest extends ICreateUnitRequest {
  id: string;
}
