import { IQuestion } from "./question.type";

export interface ITestAnswer {
  id?: string;
  answerContent: string;
  answerTranslate: string;
  answerChoice: string;
  question?: IQuestion;
}

export interface ICreateTestAnswerRequest {
  answerContent: string;
  answerTranslate: string;
  answerChoice: string;
}

export interface IUpdateTestAnswerRequest {
  id: string;
  answerContent: string;
  answerTranslate: string;
  answerChoice: string;
  isCorrect: boolean;
}

export interface ITestAnswerResponse {
  id: string;
  answerContent: string;
  answerTranslate: string;
  answerChoice: string;
  isCorrect: boolean;
}
