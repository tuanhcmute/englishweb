import { ITestResponse } from "./test.type";
import { IUserCredentialResponse } from "./user-credential.type";

export interface ICommentResponse {
  id: string;
  content: string;
  hasSubComment: boolean;
  countSubComment: number;
  author: IUserCredentialResponse;
  test: ITestResponse;
  createdDate: string;
  lastModifiedDate: string;
  isNegative: boolean;
  isReview: boolean;
  countReportComments: number;
}

export interface IGetCommentRequest {
  testId?: string;
  isRoot?: string;
  isFiltered?: string;
}

export interface ICreateCommentRequest {
  content: string;
  testId: string;
  authorId: string;
  parentCommentId: string;
}

export interface IUpdateCommentRequest {
  id: string;
  content: string;
  isNegative: boolean;
  isReview: boolean;
}
