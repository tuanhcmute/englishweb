export interface IInvoiceLineResponse {
  id: string;
  quantity: number;
  unitPrice: number;
  discount: number;
  totalPrice: number;
}
