export interface IDashboardResponse {
  userDashboard: IUserDashboardResponse;
  newsDashboard: INewsDashboardResponse;
  orderDashboard: IOrderDashboardResponse;
  courseDashboard: ICourseDashboardResponse;
  testDashboard: ITestDashboardResponse;
  flashcardDashboard: IFlashcardDashboardResponse;
}

export interface IUserDashboardResponse {
  totalUser: number;
  totalStudent: number;
  totalTeacher: number;
  totalAdmin: number;
}

export interface INewsDashboardResponse {
  totalNews: number;
}

export interface IOrderDashboardResponse {
  totalOrders: number;
}

export interface ICourseDashboardResponse {
  totalCourses: number;
}

export interface ITestDashboardResponse {
  totalTests: number;
}

export interface IFlashcardDashboardResponse {
  totalFlashcards: number;
}
