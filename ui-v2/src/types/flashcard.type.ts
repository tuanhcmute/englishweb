import { IFlashcardItemResponse } from "./flashcard-item.type";
import { IUserCredentialResponse } from "./user-credential.type";

export interface IFlashcardResponse {
  id: string;
  flashcardTitle: string;
  flashcardDescription: string;
  totalFlashcardItem: number;
  userCredential: IUserCredentialResponse;
  flashcardType: string;
}

export interface IFullFlashcardResponse extends IFlashcardResponse {
  flashcardItems: IFlashcardItemResponse[];
}

export interface ICreateFlashcardRequest {
  flashcardTitle: string;
  flashcardDescription: string;
  totalFlashcardItem: number;
  userCredentialId: string;
  flashcardType: string;
}

export interface IUpdateFlashcardRequest {
  id: string;
  flashcardTitle: string;
  flashcardDescription: string;
}
