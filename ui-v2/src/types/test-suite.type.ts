import { ISkilledAgentResponse } from "./skilled-agent.type";
import { ITestSuiteQuestionResponse } from "./test-suite-question.type";
import { ITestSuiteRunResponse } from "./test-suite-run.type";

export interface ITestSuiteResponse {
  id: string;
  test_suite_name: string;
  description: string;
  questions: ITestSuiteQuestionResponse[];
  test_suite_runs: ITestSuiteRunResponse[];
  skilled_agents: ISkilledAgentResponse[];
}

export interface ICreateTestSuiteRequest {
  test_suite_name: string;
  description: string;
  skilled_agent_ids: string[];
}
