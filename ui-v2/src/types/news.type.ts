import { INewsCategoryResponse } from "./news-category.type";
import { IUserCredentialResponse } from "./user-credential.type";

export interface INewsResponse {
  id: string;
  newsTitle: string;
  newsDescription: string;
  newsImage: string;
  status: string;
  createdDate: string;
  lastModifiedDate: string;
  newsCategory: INewsCategoryResponse;
  author: IUserCredentialResponse;
}

export interface IFullNewsResponse extends INewsResponse {
  newsContentHtml: string;
}

export interface ICreateNewsRequest {
  newsTitle: string;
  newsDescription: string;
  newsImage: string;
  newsCategoryId: string;
  status: string;
  userCredentialId: string;
}

export interface IUpdateNewsRequest {
  id: string;
  newsTitle: string;
  newsDescription: string;
  newsImage: string;
  status: string;
  newsContentHtml: string;
}

export interface IGetNewsRequest {
  categoryId?:string;
  page?:string;
  limit?:string;
}
