export interface ILLMModelModuleResponse {
  llm_model_name: string;
  param: string;
  id: string;
}
