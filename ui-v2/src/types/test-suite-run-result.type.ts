import { ITestSuiteQuestionResponse } from "./test-suite-question.type";

export interface ITestSuiteRunResultResponse {
  test_suite_run_id: string;
  skilled_agent_version_id: string;
  is_pass: boolean;
  speed: number;
  run_question_id: string;
  answer_id: string;
  created_at: string;
  id: string;
  run_question_result: ITestSuiteQuestionResponse;
  answer_result: {
    answer_content: string;
    id: string;
  };
}

// export interface ICreateTestSuiteRunRequest {
//   version: string;
//   score: number;
//   run_status: string;
//   test_suite_id: string;
//   skilled_agent_version_id: string;
//   the_number_of_question: number;
// }
