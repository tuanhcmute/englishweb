import { ICourseCategoryResponse } from "./course-category.type";
import { IDiscountResponse } from "./discount.type";
import { IReviewResponse } from "./review.type";

export interface ICourse {
  id: string;
  courseImg: string;
  courseName: string;
  newPrice: number;
  oldPrice: number;
  percentDiscount: number;
  isActive: boolean;
}

export interface IGetCourseRequest {
  isActive: string;
}

export interface ICourseResponse extends ICourse {
  courseCategory: ICourseCategoryResponse;
  discount: IDiscountResponse;
  isDiscount: boolean;
  reviews: IReviewResponse[];
  createdDate: string;
  lastModifiedDate: string;
  countLessons: number;
  countUnits: number;
  countReviews: number;
  overallRating: number;
}

export interface ICreateCourseRequest {
  courseImg: string;
  courseName: string;
  oldPrice: number;
  isActive: boolean;
}

export interface IUpdateCourseRequest extends ICreateCourseRequest {
  id: string;
}
