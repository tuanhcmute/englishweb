import { ILLMModelResponse } from "./llm-model.type";
import { ITestSuiteRunResponse } from "./test-suite-run.type";
import { IToolResponse } from "./tool.type";

export interface ISkilledAgentVersionResponse {
  id: string;
  skilled_agent_id: string;
  version: string;
  setup_message: string;
  test_suite_runs: ITestSuiteRunResponse[];
  tools: IToolResponse[];
  llm_model_result: ILLMModelResponse[];
  skilled_agent_name: string;
}

export interface ICreateSkilledAgentVersionRequest {
  skilled_agent_id: string;
  version: string;
  setup_message: string;
  tool_ids: string[];
  llm_model_ids: string[];
}

export interface IUpdateStatusSkilledAgentVersionRequest {
  skilled_agent_version_id: string;
  is_active: boolean;
}

export interface IUpdateSkilledAgentVersionRequest {
  id: string;
  skilled_agent_id: string;
  version: string;
  setup_message: string;
  tool_ids: string[];
  llm_model_ids: string[];
}
