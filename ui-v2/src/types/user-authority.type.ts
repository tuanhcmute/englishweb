import { IRoleResponse } from "./role.type";

export interface IUserAuthorityResponse {
  role: IRoleResponse;
}
