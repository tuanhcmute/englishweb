export interface IToolResponse {
  tool_name: string;
  param: null;
  id: string;
}

export interface ICreateToolRequest {
  tool_name: string;
  param: string;
}
