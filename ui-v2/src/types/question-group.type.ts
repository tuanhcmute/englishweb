import { IPartOfTest } from "./part-of-test.type";
import { IQuestion, IQuestionResponse } from "./question.type";

export interface IQuestionGroup {
  id: string;
  audio: string;
  image: string;
  questionContentEn: string;
  questionContentTranscript: string;
  partOfTest: IPartOfTest;
  questions: IQuestion[];
}

export interface IQuestionGroupRequest {
  questionContentEn: string;
  questionContentTranscript: string;
  partOfTestId: string;
  image: string;
}

export interface IUpdateQuestionGroupRequest {
  questionContentEn: string;
  questionContentTranscript: string;
  id: string;
  image: string;
}

export interface IUploadQuestionGroupFileRequest {
  file: File;
  id: string;
  type: string;
}

export interface IQuestionGroupResponse {
  id: string;
  audio: string;
  image: string;
  questionContentEn: string;
  questionContentTranscript: string;
  questions: IQuestionResponse[];
  createdDate: string;
  lastModifiedDate: string;
}
