export interface ITestSuiteRunResponse {
  id: string;
  run_status: string;
  test_suite_id: string;
  version: string;
  score: number;
  the_number_of_question: number;
  skilled_agent_version_id: string;
}

export interface ICreateTestSuiteRunRequest {
  version: string;
  score: number;
  run_status: string;
  test_suite_id: string;
  skilled_agent_version_id: string;
  the_number_of_question: number;
}

export interface IUpdateTestSuiteRunRequest {
  id: string;
  version: string;
  score: number;
  run_status: string;
  test_suite_id: string;
  skilled_agent_version_id: string;
  the_number_of_question: number;
}
