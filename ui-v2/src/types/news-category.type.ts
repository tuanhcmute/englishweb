export interface INewsCategoryResponse {
  id: string;
  newsCategoryName: string;
  createdDate: string;
  lastModifiedDate: string;
  status: string;
}

export interface ICreateNewsCategoryRequest {
  newsCategoryName: string;
}

export interface IUpdateNewsCategoryRequest extends ICreateNewsCategoryRequest {
  id: string;
  status: string;
}
