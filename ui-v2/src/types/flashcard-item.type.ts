export interface IFlashcardItemResponse {
  id: string;
  word: string;
  phonetic: string;
  meaning: string;
  tags: string;
  audio: string;
  examples: string;
  image: string;
  createdDate: string;
  lastModifiedDate: string;
}

export interface ICreateFlashcardItemRequest {
  word: string;
  phonetic: string;
  meaning: string;
  tags: string;
  examples: string;
  flashcardId: string;
}

export interface ICreateFlashcardItemByKeywordRequest {
  keyword: string;
  flashcardId: string;
  limit: number;
}

export interface IUpdateFlashcardItemRequest {
  word: string;
  phonetic: string;
  meaning: string;
  tags: string;
  examples: string;
  flashcardItemId: string;
}
