import { IRoleResponse } from "./role.type";
import { IUserAuthorityResponse } from "./user-authority.type";

export interface IUserCredential {
  id: string;
  username: string;
  password: string;
  email: string;
  phoneNumber: string;
  fullName: string;
  isActive: boolean;
}

export interface IUserCredentialRequest {
  username: string;
  password: string;
  confirmPassword: string;
  email: string;
  phoneNumber: string;
  fullName: string;
  provider: string;
}

export interface IUpdateUserCredentialRequest {
  dob: string;
  address: string;
  gender: string;
  major: string;
  phoneNumber: string;
  fullName: string;
  id: string;
}

export interface IUserCredentialResponse {
  id: string;
  username: string;
  email: string;
  phoneNumber: string;
  fullName: string;
  roleName: string;
  verified: boolean;
  avatar: string;
  provider: string;
  userAuthorities: IUserAuthorityResponse[];
  dob: string;
  address: string;
  gender: string;
  major: string;
}

export interface ITokenResponse {
  accessToken: string;
  refreshToken: string;
}
