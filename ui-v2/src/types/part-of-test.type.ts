import { IPartInformation } from "./part-information.type";
import { IQuestionGroupResponse } from "./question-group.type";

export interface IPartOfTest {
  id: string;
  partInformation: IPartInformation;
  questionGroups: IQuestionGroupResponse[];
  partSequenceNumber: number;
}

export interface IFullPartOfTestResponse {
  id: string;
  questionGroups: IQuestionGroupResponse[];
  partInformation: IPartInformation;
  partSequenceNumber: number;
}
