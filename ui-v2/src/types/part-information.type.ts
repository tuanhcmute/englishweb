import { ITestCategoryResponse } from "./test-category.type";

export interface IPartInformation {
  id: string;
  partInformationName: string;
  totalQuestion: number;
  partInformationDescription: string;
}

export interface IPartInformationRequest {
  partInformationName: string;
  totalQuestion: number;
  partInformationDescription: string;
  testCategoryId: string;
  partSequenceNumber: number;
  partType: string;
}

export interface IUpdatePartInformationRequest {
  partInformationName: string;
  totalQuestion: number;
  partInformationDescription: string;
  id: string;
  partSequenceNumber: number;
  partType: string;
}

export interface IFullPartInformationResponse {
  id: string;
  partInformationName: string;
  totalQuestion: number;
  partInformationDescription: string;
  testCategory: ITestCategoryResponse;
  partSequenceNumber: number;
}

export interface IPartInformationResponse {
  id: string;
  partInformationName: string;
  totalQuestion: number;
  partInformationDescription: string;
  partSequenceNumber: number;
  partType: string;
}
