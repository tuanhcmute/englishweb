export interface ICourseCategory {
  id: string;
  courseCategoryName: string;
}

export interface ICourseCategoryResponse extends ICourseCategory {}

export interface ICreateCourseCategoryRequest {
  courseCategoryName: string;
}
