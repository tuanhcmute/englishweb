export interface ICreateSkilledAgentTestRequest {
  test_suite_run_id: string;
  skilled_agent_version_id: string;
}
