export interface IRevenueResponse {
  id: string;
  totalAmount: number;
  createdDate: string;
  lastModifiedDate: string;
}

export interface IGetRevenueRequest {
  page?: number;
  limit?: number;
  from?: string;
  to?: string;
  sort?: string;
}
