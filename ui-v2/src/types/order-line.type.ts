import { ICourseResponse } from "./course.type";

export interface IOrderLineResponse {
  id: string;
  course: ICourseResponse;
}
