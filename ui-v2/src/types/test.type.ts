import { IFullPartOfTestResponse, IPartOfTest } from "./part-of-test.type";
import { IScoreConversionResponse } from "./score-conversion.type";
import { ITestCategoryResponse } from "./test-category.type";

export interface ITest {
  id?: string;
  testName?: string;
  testDescription?: string;
  numberOfPart?: number;
  numberOfQuestion?: number;
  listOfPart?: IPartOfTest[];
  testCategoryId?: string;
  thumbnail?: string;
}

export interface ITestResponse {
  id: string;
  testName: string;
  testDescription: string;
  numberOfPart: number;
  numberOfQuestion: number;
  testCategory: ITestCategoryResponse;
  thumbnail: string;
  createdDate: string;
  lastModifiedDate: string;
  countUserAnswers: number;
  countTestComments: number;
  scoreConversion: IScoreConversionResponse;
}

export interface IFullTestResponse {
  id: string;
  testName: string;
  testDescription: string;
  numberOfPart: number;
  numberOfQuestion: number;
  thumbnail: string;
  listOfPart: IFullPartOfTestResponse[];
  countUserAnswers: number;
  countTestComments: number;
}

export interface ITestRequest {
  testName: string;
  testDescription?: string;
  numberOfPart: number;
  numberOfQuestion: number;
  testCategoryId: string;
  thumbnail: string;
}

export interface IUpdateTestRequest {
  id: string;
  testName: string;
  testDescription?: string;
  thumbnail: string;
}
