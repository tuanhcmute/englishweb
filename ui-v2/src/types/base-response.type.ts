export interface IBaseResponse<T> {
  statusCode: number;
  message: string;
  data: T;
  errors: IErrorResponse;
}

export interface IErrorResponse {
  errorCode: number;
  errorMessage: string;
}
