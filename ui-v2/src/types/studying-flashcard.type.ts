import { IFlashcardItemResponse } from "./flashcard-item.type";
import { IFlashcardResponse, IFullFlashcardResponse } from "./flashcard.type";
import { IUserCredentialResponse } from "./user-credential.type";

export interface IStudyingFlashcardResponse {
  id: string;
  userCredential: IUserCredentialResponse;
  flashcard: IFlashcardResponse;
  status: string;
  createdDate: string;
  lastModifiedDate: string;
}

export interface IFullStudyingFlashcardResponse {
  id: string;
  userCredential: IUserCredentialResponse;
  flashcard: IFullFlashcardResponse;
  status: string;
  createdDate: string;
  lastModifiedDate: string;
}

export interface ICreateStudyingFlashcardRequest {
  userCredentialId: string;
  flashcardId: string;
}

export interface IUpdateStudyingFlashcardRequest {
  id: string;
  status: string;
}
