export interface IRoleResponse {
  id: string;
  roleName: string;
}

export interface IUpdateRoleRequest {
  roleId: string;
  userCredentialId: string;
}
