import { IInvoiceLineResponse } from "./invoice-line.type";
import { IPaymentResponse } from "./payment.type";

export interface IInvoiceResponse {
  id: string;
  amountPrice: number;
  totalPrice: number;
  tax: number;
  invoiceTitle: string;
  invoiceDescription: string;
  createdDate: string;
  invoiceLines: IInvoiceLineResponse[];
  payment: IPaymentResponse;
}
