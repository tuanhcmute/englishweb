import { ILessonResponse } from "./lesson.type";

export interface ILessonNoteResponse {
  id: string;
  content: string;
  noteTime: number;
  lesson: ILessonResponse;
  createdDate: string;
  lastModifiedDate: string;
}

export interface ICreateLessonNoteRequest {
  content: string;
  noteTime: number;
  lessonId: string;
  userCredentialId: string;
}

export interface IUpdateLessonNoteRequest {
  id: string;
  content: string;
}
