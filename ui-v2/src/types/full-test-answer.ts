import { ITestResponse } from "./test.type";

export interface IFullTestAnswerResponse {
  id: string;
  fullTestAnswerJson: string;
  test: ITestResponse;
}
