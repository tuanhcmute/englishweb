export interface ICreatePaymentRequest {
  totalAmount: number;
  userCredentialId: string;
  courseId: string;
  paymentMethod: string;
}

export interface IPaymentResponse {}
