import { IConditionTargetResponse } from "./condition-target.type";

export interface ICreateConditionRequest {
  type: string;
  percentDiscount: number;
  courseIds: string[];
  categoryIds: string[];
}

export interface IDiscountConditionResponse {
  type: string;
  percentDiscount: number;
  conditionTargets: IConditionTargetResponse[];
}
