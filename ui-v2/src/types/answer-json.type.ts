export interface IQuestionChoice {
  questionId: string;
  answerId: string;
}

export interface IQuestionGroupChoice {
  questionGroupId: string;
  questions: IQuestionChoice[];
}

export interface IResultChoice {
  partId: string;
  questionGroups: IQuestionGroupChoice[];
}

export interface IUserChoice {
  partId: string;
  questionGroupId: string;
  questionId: string;
  answerId: string;
}

export interface IUserMark extends IUserChoice {}
