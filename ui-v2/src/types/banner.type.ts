export interface IBannerResponse {
  id: string;
  image: string;
  createdDate: string;
  lastModifiedDate: string;
}

export interface IGetBannerRequest {
  page?: number;
  limit?: number;
}

export interface ICreateBannerRequest {
  image: string;
}

export interface IUpdateBannerRequest {
  id: string;
  image: string;
}
