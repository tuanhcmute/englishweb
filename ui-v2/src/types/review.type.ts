import { IUserCredentialResponse } from "./user-credential.type";

export interface IReviewResponse {
  id: string;
  userCredential: IUserCredentialResponse;
  content: string;
  rating: number;
  createdDate: string;
  lastModifiedDate: string;
  isPublic: boolean;
  courseId: string;
  courseName: string;
  image: string;
}

export interface ICreateReviewRequest {
  userCredentialId: string;
  content: string;
  rating: number;
  courseId: string;
  image: string;
}

export interface IUpdateReviewRequest {
  id: string;
  content?: string;
  rating?: number;
  isPublic?: boolean;
}

export interface IGetReviewRequest {
  courseId?: string;
  isPublic?: string;
  page?: string;
  limit?: number;
}
