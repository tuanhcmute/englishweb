import { KEY_QUERIES } from "@/constants";
import { lessonNoteService } from "@/services";
import { IUpdateLessonNoteRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateLessonNote() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateLessonNoteRequest) => {
      return await lessonNoteService.updateLessonNote(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa ghi chú thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa ghi chú thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.LESSONS_NOTES],
      });
    },
  });
}
