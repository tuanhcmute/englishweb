import { KEY_QUERIES } from "@/constants";
import {
  partInformationService,
  testCategoryService,
  testService,
} from "@/services";
import {
  IPartInformationRequest,
  ITest,
  ITestCategoryRequest,
  ITestRequest,
} from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreatePartInformation() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IPartInformationRequest) => {
      return await partInformationService.createPartInformation(item);
    },
    onSuccess(data) {
      toast.success("Tạo danh mục bài thi mới thành công");
    },
    onError(error) {
      toast.error("Tạo danh mục bài thi mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.PART_INFORMATION],
      });
    },
  });
}
