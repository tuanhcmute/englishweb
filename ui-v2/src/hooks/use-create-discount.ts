import { KEY_QUERIES } from "@/constants";
import { discountService } from "@/services";
import { ICreateDiscountRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateDiscount() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateDiscountRequest) => {
      return await discountService.createDiscount(item);
    },
    onSuccess(data) {
      toast.success("Tạo discount mới thành công");
    },
    onError(error) {
      toast.error("Tạo discount mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.DISCOUNTS] });
    },
  });
}
