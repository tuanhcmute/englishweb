import { KEY_QUERIES } from "@/constants";
import { testService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useFullTest = (testId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.TEST, testId],
    queryFn: async () => {
      const res = await testService.getFullTestById(testId);
      return res.data.data;
    },
    enabled: !!testId,
    // initialData: init,
  });
};
