import { unitService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useUnitsByCourse(courseId: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.UNITS, courseId],
    queryFn: async () => await unitService.getAllUnitsByCourse(courseId),
    enabled: !!courseId,
  });
}
