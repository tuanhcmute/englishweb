import { KEY_QUERIES } from "@/constants";
import { discountService } from "@/services";
import { IUpdateDiscountRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateDiscount() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateDiscountRequest) => {
      return await discountService.updateDiscount(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa discount mới thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa discount mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.DISCOUNTS] });
    },
  });
}
