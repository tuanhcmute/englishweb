import { KEY_QUERIES } from "@/constants";
import {
  bannerService,
  newsCategoryService,
  skilledAgentVersionService,
} from "@/services";
import {
  IUpdateBannerRequest,
  IUpdateNewsCategoryRequest,
  IUpdateStatusSkilledAgentVersionRequest,
} from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateStatusSkilledAgentVersion() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateStatusSkilledAgentVersionRequest) => {
      return await skilledAgentVersionService.updateStatusSkilledAgentVersion(
        item
      );
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.SKILLED_AGENTS_VERSIONS],
      });
    },
  });
}
