import { KEY_QUERIES } from "@/constants";
import { questionService } from "@/services";
import { ICreateQuestionRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateQuestion() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateQuestionRequest) => {
      return await questionService.createNewQuestion(item);
    },
    onSuccess(data) {
      toast.success("Tạo câu hỏi thành công");
    },
    onError(error) {
      toast.error("Tạo câu hỏi thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.QUESTION_GROUPS],
      });
    },
  });
}
