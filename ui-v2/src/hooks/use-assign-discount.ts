import { KEY_QUERIES } from "@/constants";
import { discountService } from "@/services";
import { IAssignDiscountRequest, ICreateDiscountRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useAssignDiscount() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IAssignDiscountRequest) => {
      return await discountService.assignDiscount(item);
    },
    onSuccess(data) {
      toast.success("Áp dụng discount thành công");
    },
    onError(error) {
      toast.error("Áp dụng discount thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.DISCOUNTS] });
    },
  });
}
