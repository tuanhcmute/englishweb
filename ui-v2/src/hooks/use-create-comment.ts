import { KEY_QUERIES } from "@/constants";
import { commentService } from "@/services";
import { ICreateCommentRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateComment() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateCommentRequest) => {
      return await commentService.createComment(item);
    },
    onSuccess(data) {
      toast.success("Bình luận thành công");
    },
    onError(error) {
      toast.error("Bình luận thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.COMMENTS] });
    },
  });
}
