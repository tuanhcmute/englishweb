import { bannerService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { IGetBannerRequest } from "@/types";

export function useBanners(requestData?: IGetBannerRequest) {
  return useQuery({
    queryKey: [KEY_QUERIES.BANNERS, JSON.stringify(requestData)],
    queryFn: async () => await bannerService.getAllBanners(requestData),
  });
}
