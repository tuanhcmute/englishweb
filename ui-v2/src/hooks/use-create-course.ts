import { KEY_QUERIES } from "@/constants";
import { courseService } from "@/services";
import { ICreateCourseRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateCourse() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateCourseRequest) => {
      return await courseService.createCourse(item);
    },
    onSuccess(data) {
      toast.success("Tạo khóa học mới thành công");
    },
    onError(error) {
      toast.error("Tạo khóa học mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.COURSES] });
    },
  });
}
