import { KEY_QUERIES } from "@/constants";
import { courseDetailService, partOfTestService } from "@/services";
import { IUpdateCourseDetailRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export const useUpdateCourseDetail = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (requestData: IUpdateCourseDetailRequest) => {
      const res = await courseDetailService.updateCourseDetail(requestData);

      return res;
    },
    onSuccess: () => {
      toast.success("Cập nhật thành công khóa học");
    },
    onError: (error: any) => {
      toast.error("Cập nhật khóa học thất bại");
    },
    onSettled(data, error, variables, context) {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.COURSE_DETAIL] });
    },
  });
};
