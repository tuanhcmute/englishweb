import { KEY_QUERIES } from "@/constants";
import { testService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useTest = (testId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.TEST, testId],
    queryFn: async () => {
      const res = await testService.getTestById(testId);
      return res.data.data;
    },
  });
};
