import { KEY_QUERIES } from "@/constants";
import { skilledAgentVersionService } from "@/services";
import { IUpdateSkilledAgentVersionRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateSkilledAgentVersion() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateSkilledAgentVersionRequest) => {
      return await skilledAgentVersionService.updateSkilledAgentVersion(item);
    },
    onSuccess(data) {
      toast.success("Cập nhật thành công");
    },
    onError(error) {
      toast.error("Cập nhật thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.SKILLED_AGENTS_VERSIONS],
      });
    },
  });
}
