import { KEY_QUERIES } from "@/constants";
import { dashboardService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useDashboard() {
  return useQuery({
    queryKey: [KEY_QUERIES.DASHBOARD],
    queryFn: async () => await dashboardService.getDashboard(),
  });
}
