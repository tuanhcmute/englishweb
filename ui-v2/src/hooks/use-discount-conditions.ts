import { KEY_QUERIES } from "@/constants";
import { discountConditionService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useDiscountConditions(discountId?: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.DISCOUNT_CONDITIONS, discountId],
    queryFn: async () => {
      return await discountConditionService.getAllDiscountConditions(
        discountId
      );
    },
  });
}
