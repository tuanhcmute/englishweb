import { KEY_QUERIES } from "@/constants";
import { lessonService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useLesson = (id: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.LESSONS, id],
    queryFn: async () => {
      const res = await lessonService.getLessonById(id);
      return res.data;
    },
    enabled: !!id,
  });
};
