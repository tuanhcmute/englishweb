import { KEY_QUERIES } from "@/constants";
import { skilledAgentVersionService } from "@/services";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";

export function useActivatedSkilledAgentVersion() {
  return useQuery({
    queryKey: [KEY_QUERIES.SKILLED_AGENTS_VERSIONS],
    queryFn: async () => {
      return await skilledAgentVersionService.getActivatedSkilledAgentVersion();
    },
  });
}
