import { commentService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useCommentsByParentComment(commentId: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.COMMENTS, commentId],
    queryFn: async () =>
      await commentService.getAllChildCommentsByParentComment(commentId),
    enabled: !!commentId,
  });
}
