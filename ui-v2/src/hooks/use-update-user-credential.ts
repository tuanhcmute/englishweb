import { KEY_QUERIES } from "@/constants";
import { userCerdentialService } from "@/services";
import { IUpdateUserCredentialRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateUserCredential() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateUserCredentialRequest) => {
      return await userCerdentialService.updateUserCredential(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thông tin thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thông tin thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.USER_CREDENTIALS],
      });
    },
  });
}
