import { KEY_QUERIES } from "@/constants";
import { courseDetailService, testService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useCourseDetail = (courseId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.COURSE_DETAIL, courseId],
    queryFn: async () => {
      const res = await courseDetailService.getCourseDetailByCourse(courseId);
      return res.data;
    },
  });
};
