import { KEY_QUERIES } from "@/constants";
import { newsService } from "@/services";
import { IUpdateNewsRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateNews() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateNewsRequest) => {
      return await newsService.updateNews(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa bài viết thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa bài viết thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.NEWS],
      });
    },
  });
}
