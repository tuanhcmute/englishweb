import { KEY_QUERIES } from "@/constants";
import { llmModelService, toolService } from "@/services";
import { ICreateLLMModelRequest, ICreateToolRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateTool() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateToolRequest) => {
      return await toolService.createTool(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.LLM_MODELS],
      });
    },
  });
}
