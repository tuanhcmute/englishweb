import { KEY_QUERIES } from "@/constants";
import { userAnswerService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useUserAnswers = (userCredentialId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.USER_ANSWERS, userCredentialId],
    queryFn: async () => {
      return await userAnswerService.getAllUserAnswerByUserCredential(
        userCredentialId
      );
    },
  });
};
