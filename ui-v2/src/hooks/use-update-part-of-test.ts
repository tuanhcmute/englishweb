import { KEY_QUERIES } from "@/constants";
import { partOfTestService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export const useUpdatePartOfTest = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (testId: string) => {
      const res = await partOfTestService.updatePartOfTest(testId);

      return res;
    },
    onSuccess: () => {
      toast.success("Cập nhật thành công danh sách phần thi");
    },
    onError: (error: any) => {
      toast.error("Cập nhật danh sách bài thi thất bại");
    },
    onSettled(data, error, variables, context) {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.PART_OF_TEST] });
    },
  });
};
