import { KEY_QUERIES } from "@/constants";
import { userCerdentialService } from "@/services";
import { IUpdateRoleRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateRole() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateRoleRequest) => {
      return await userCerdentialService.updateUserRole(item);
    },
    onSuccess(data) {
      toast.success("Cập nhật quyền thành công");
    },
    onError(error) {
      toast.error("Cập nhật quyền thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.USER_CREDENTIALS],
      });
    },
  });
}
