import { KEY_QUERIES } from "@/constants";
import { commentService, testService } from "@/services";
import { IUpdateCommentRequest, IUpdateTestRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateComment() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateCommentRequest) => {
      return await commentService.updateComment(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.COMMENTS] });
    },
  });
}
