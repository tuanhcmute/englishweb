import { flashcardService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { IUpdateFlashcardRequest } from "@/types";
import { toast } from "react-hot-toast";

export function useUpdateFlashcard() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateFlashcardRequest) => {
      return await flashcardService.updateFlashcard(item);
    },
    onSuccess(data) {
      toast.success("Tạo flashcard thành công");
    },
    onError(error) {
      toast.error("Tạo flashcard thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.FLASHCARDS] });
    },
  });
}
