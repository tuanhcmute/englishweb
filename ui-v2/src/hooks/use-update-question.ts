import { KEY_QUERIES } from "@/constants";
import { questionService } from "@/services";
import { IUpdateQuestionRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateQuestion() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateQuestionRequest) => {
      return await questionService.updateQuestion(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa câu hỏi thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa câu hỏi thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.QUESTION_GROUPS],
      });
    },
  });
}
