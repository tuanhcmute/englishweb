import { KEY_QUERIES } from "@/constants";
import { userAnswerService } from "@/services";
import { useQueries } from "@tanstack/react-query";

export const useUserAnswerCounts = (testIds?: string[]) => {
  return useQueries({
    queries: testIds
      ? testIds.map((testId) => {
          return {
            queryKey: [KEY_QUERIES.USER_ANSWERS, testId],
            queryFn: async () => {
              const res = await userAnswerService
                .countByTest(testId)
                .then((res) => res.data);
              return res;
            },
          };
        })
      : [],
  });
};
