import { KEY_QUERIES } from "@/constants";
import { courseService } from "@/services";
import { IUpdateCourseRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateCourse() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateCourseRequest) => {
      return await courseService.updateCourse(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa khóa học thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa khóa học thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.COURSES] });
    },
  });
}
