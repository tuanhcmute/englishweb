import { useEffect, useState } from "react";

export const useCountDownTime = (TIME: number) => {
  const [time, setTime] = useState<number>(TIME);

  useEffect(() => {
    const interval = setInterval(() => {
      setTime((time) => {
        if (time === 0) {
          return 0;
        }
        return time - 1000;
      });
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return {
    time,
    isFinished: time === 0,
  };
};
