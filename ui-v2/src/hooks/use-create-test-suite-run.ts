import { KEY_QUERIES } from "@/constants";
import { testSuiteRunService } from "@/services";
import { ICreateTestSuiteRunRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateTestSuiteRun() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateTestSuiteRunRequest) => {
      return await testSuiteRunService.createTestSuiteRun(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TEST_SUITE_RUNS],
      });
    },
  });
}
