import { flashcardItemService, flashcardService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useFlashcardItems(flashcardId?: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.FLASHCARD_ITEMS, flashcardId],
    queryFn: async () =>
      await flashcardItemService.getAllFlashcardItems(flashcardId),
  });
}
