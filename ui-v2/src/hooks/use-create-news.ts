import { KEY_QUERIES } from "@/constants";
import { newsService } from "@/services";
import { ICreateNewsRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateNews() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateNewsRequest) => {
      return await newsService.createNews(item);
    },
    onSuccess(data) {
      toast.success("Tạo bài viết mới thành công");
    },
    onError(error) {
      toast.error("Tạo bài viết mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.NEWS],
      });
    },
  });
}
