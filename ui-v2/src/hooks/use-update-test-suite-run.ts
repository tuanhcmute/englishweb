import { KEY_QUERIES } from "@/constants";
import { testSuiteRunService } from "@/services";
import { IUpdateTestSuiteRunRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateTestSuiteRun() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateTestSuiteRunRequest) => {
      return await testSuiteRunService.updateTestSuiteRun(item);
    },
    onSuccess(data) {
      toast.success("Cập nhật thành công");
    },
    onError(error) {
      toast.error("Cập nhật thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TEST_SUITE_RUNS],
      });
    },
  });
}
