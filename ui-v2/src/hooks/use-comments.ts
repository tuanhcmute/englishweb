import { commentService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { IGetCommentRequest } from "@/types";

export function useComments(requestData?: IGetCommentRequest) {
  return useQuery({
    queryKey: [KEY_QUERIES.COMMENTS, JSON.stringify(requestData)],
    queryFn: async () => await commentService.getAllComments(requestData),
  });
}
