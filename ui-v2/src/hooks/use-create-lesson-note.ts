import { KEY_QUERIES } from "@/constants";
import { lessonNoteService } from "@/services";
import { ICreateLessonNoteRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateLessonNote() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateLessonNoteRequest) => {
      return await lessonNoteService.createLessonNote(item);
    },
    onSuccess(data) {
      toast.success("Tạo ghi chú mới thành công");
    },
    onError(error) {
      toast.error("Tạo ghi chú thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.LESSONS_NOTES],
      });
    },
  });
}
