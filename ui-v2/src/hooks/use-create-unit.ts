import { KEY_QUERIES } from "@/constants";
import { unitService } from "@/services";
import { ICreateUnitRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateUnit() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateUnitRequest) => {
      return await unitService.createUnit(item);
    },
    onSuccess(data) {
      toast.success("Tạo chương mới thành công");
    },
    onError(error) {
      toast.error("Tạo chương mới mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.UNITS],
      });
    },
  });
}
