import { revenueService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { IGetRevenueRequest } from "@/types";

export function useRevenues(requestData?: IGetRevenueRequest) {
  return useQuery({
    queryKey: [KEY_QUERIES.BANNERS, JSON.stringify(requestData)],
    queryFn: async () => await revenueService.getAllRevenues(requestData),
  });
}
