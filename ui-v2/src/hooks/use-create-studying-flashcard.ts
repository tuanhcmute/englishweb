import { KEY_QUERIES } from "@/constants";
import { studyingFlashcardService } from "@/services";
import { ICreateStudyingFlashcardRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export function useCreateStudyingFlashcard() {
  const queryClient = useQueryClient();
  const router = useRouter();

  return useMutation({
    mutationFn: async (item: ICreateStudyingFlashcardRequest) => {
      return await studyingFlashcardService.createStudyingFlashcard(item);
    },
    onSuccess(data) {
      toast.success("Thêm flashcard thành công");
      router.push("/flashcards/studying");
    },
    onError(error) {
      toast.error("Thêm flashcard thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.STUDYING_FLASHCARDS],
      });
    },
  });
}
