import { KEY_QUERIES } from "@/constants";
import { flashcardService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useDeleteFlashcard = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (flashcardId: string) => {
      const res = await flashcardService.deleteFlashcard(flashcardId);
      return res;
    },
    onSuccess: () => {
      toast.success("Xóa flashcard thành công");
      router.refresh();
    },
    onError: () => {
      toast.error("Xóa flashcard thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.FLASHCARDS],
      });
    },
  });
};
