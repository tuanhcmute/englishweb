import { KEY_QUERIES } from "@/constants";
import { testSuiteRunResultService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useTestSuiteRunResults = (
  testSuiteId: string,
  agentVersionId: string
) => {
  return useQuery({
    queryKey: [KEY_QUERIES.TEST_SUITE_RUN_RESULTS, testSuiteId, agentVersionId],
    queryFn: async () => {
      const res = await testSuiteRunResultService.getTestSuiteRunResults(
        testSuiteId,
        agentVersionId
      );
      return res;
    },
  });
};
