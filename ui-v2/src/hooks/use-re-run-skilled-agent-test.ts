import { KEY_QUERIES } from "@/constants";
import { skilledAgentTestService } from "@/services";
import { ICreateSkilledAgentTestRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export const useReRunSkilledAgentTest = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateSkilledAgentTestRequest) => {
      return await skilledAgentTestService.reRunSkilledAgentTest(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TEST_SUITE_RUN_RESULTS],
      });
    },
  });
};
