import { KEY_QUERIES } from "@/constants";
import { staticPageService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useStaticPages = () => {
  return useQuery({
    queryKey: [KEY_QUERIES.STATIC_PAGES],
    queryFn: async () => {
      const res = await staticPageService.getAllStaticPages();
      console.log({ res });
      return res;
    },
  });
};
