import { KEY_QUERIES } from "@/constants";
import { courseService } from "@/services";
import { IGetCourseRequest } from "@/types";
import { useQuery } from "@tanstack/react-query";

export function useCourses(requestData?: IGetCourseRequest) {
  return useQuery({
    queryKey: [KEY_QUERIES.COURSES, JSON.stringify(requestData)],
    queryFn: async () => await courseService.getAllCourses(requestData),
  });
}
