import { KEY_QUERIES } from "@/constants";
import { testService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useDeleteTest() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (testId: string) => {
      return await testService.deleteTest(testId);
    },
    onSuccess: (data) => {
      toast.success(`Xóa bài thi thành công`);
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.TESTS] });
    },
    onError(err) {
      toast.error("Đã có lỗi xảy ra, vui lòng thử lại");
    },
  });
}
