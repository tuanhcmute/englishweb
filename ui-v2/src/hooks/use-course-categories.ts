import { KEY_QUERIES } from "@/constants";
import { courseCategoryService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useCourseCategories() {
  return useQuery({
    queryKey: [KEY_QUERIES.COURSE_CATEGORIES],
    queryFn: async () => await courseCategoryService.getAllCourseCategories(),
  });
}
