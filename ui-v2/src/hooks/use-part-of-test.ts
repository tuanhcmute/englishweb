import { KEY_QUERIES } from "@/constants";
import { partOfTestService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function usePartOfTest(testId: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.PART_OF_TEST],
    queryFn: async () => await partOfTestService.getAllPartOfTestByTest(testId),
  });
}
