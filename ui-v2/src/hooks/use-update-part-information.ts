import { KEY_QUERIES } from "@/constants";
import {
  partInformationService,
  testCategoryService,
  testService,
} from "@/services";
import {
  IPartInformationRequest,
  ITest,
  ITestCategoryRequest,
  ITestRequest,
  IUpdatePartInformationRequest,
} from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdatePartInformation() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdatePartInformationRequest) => {
      return await partInformationService.updatePartInformation(item);
    },
    onSuccess(data) {
      toast.success("Cập nhật danh mục bài thi mới thành công");
    },
    onError(error) {
      toast.error("Cập nhật danh mục bài thi mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.PART_INFORMATION],
      });
    },
  });
}
