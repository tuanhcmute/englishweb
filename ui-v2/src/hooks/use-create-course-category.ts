import { KEY_QUERIES } from "@/constants";
import { courseCategoryService, courseService } from "@/services";
import { ICreateCourseCategoryRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateCourseCategory() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateCourseCategoryRequest) => {
      return await courseCategoryService.createCourseCategory(item);
    },
    onSuccess(data) {
      toast.success("Tạo danh mục khóa học mới thành công");
    },
    onError(error) {
      toast.error("Tạo danh mục khóa học mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.COURSE_CATEGORIES],
      });
    },
  });
}
