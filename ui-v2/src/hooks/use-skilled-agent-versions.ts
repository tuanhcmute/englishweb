import { KEY_QUERIES } from "@/constants";
import { skilledAgentVersionService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useSkilledAgentVersions = (agentId?: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.SKILLED_AGENTS_VERSIONS, agentId],
    queryFn: async () => {
      return await skilledAgentVersionService.getAllSkilledAgentVersions(
        agentId
      );
    },
  });
};
