import { testService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useTests() {
  return useQuery({
    queryKey: [KEY_QUERIES.TESTS],
    queryFn: async () => await testService.getAllTests(),
  });
}
