import { llmModelService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useLLMModelModules() {
  return useQuery({
    queryKey: [KEY_QUERIES.LLM_MODEL_MODULES],
    queryFn: async () => await llmModelService.getAllLLMModelModules(),
  });
}
