import { KEY_QUERIES } from "@/constants";
import { staticPageService } from "@/services";
import { ICreateStaticPageRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export const useCreateStaticPage = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateStaticPageRequest) => {
      return await staticPageService.createStaticPage(item);
    },
    onSuccess(data) {
      toast.success("Tạo trang tĩnh thành công");
    },
    onError(error) {
      toast.error("Tạo trang tĩnh thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.STATIC_PAGES] });
    },
  });
};
