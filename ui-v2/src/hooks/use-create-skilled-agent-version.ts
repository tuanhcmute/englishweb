import { KEY_QUERIES } from "@/constants";
import { skilledAgentVersionService } from "@/services";
import { ICreateSkilledAgentVersionRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateSkilledAgentVersion() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateSkilledAgentVersionRequest) => {
      return await skilledAgentVersionService.createSkilledAgentVersion(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.SKILLED_AGENTS],
      });
    },
  });
}
