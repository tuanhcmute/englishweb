import { KEY_QUERIES } from "@/constants";
import { questionGroupService } from "@/services";
import { IUploadQuestionGroupFileRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUploadQuestionGroupFile() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUploadQuestionGroupFileRequest) => {
      return await questionGroupService.uploadQuestionGroupFile(item);
    },
    onSuccess(data) {
      toast.success(`Upload file thành công`);
    },
    onError(error) {
      toast.error("Upload file thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.QUESTION_GROUPS],
      });
    },
  });
}
