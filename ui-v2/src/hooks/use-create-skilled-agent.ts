import { KEY_QUERIES } from "@/constants";
import { skilledAgentService } from "@/services";
import { ICreateSkilledAgentRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateSkilledAgent() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateSkilledAgentRequest) => {
      return await skilledAgentService.createSkilledAgent(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.SKILLED_AGENTS],
      });
    },
  });
}
