import { KEY_QUERIES } from "@/constants";
import { newsService } from "@/services";
import { IGetNewsRequest } from "@/types";
import { useQuery } from "@tanstack/react-query";

export function useNews(requestData?:IGetNewsRequest) {
  return useQuery({
    queryKey: [KEY_QUERIES.NEWS, JSON.stringify(requestData)],
    queryFn: async () => await newsService.getAllNews(requestData),
  });
}
