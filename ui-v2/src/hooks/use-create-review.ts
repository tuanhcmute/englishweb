import { KEY_QUERIES } from "@/constants";
import { reviewService } from "@/services";
import { ICreateReviewRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateReview() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateReviewRequest) => {
      return await reviewService.createReview(item);
    },
    onSuccess(data) {
      toast.success(
        "Đánh giá khóa học thành công. Vui lòng chờ được xét duyệt"
      );
    },
    onError(error) {
      toast.error("Đánh giá thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.REVIEWS] });
    },
  });
}
