import { ROUTES } from "@/constants";
import { authService } from "@/services";
import { IForgotPasswordRequest } from "@/types";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export function useForgotPassword() {
  const router = useRouter();

  return useMutation({
    mutationFn: async (requestData: IForgotPasswordRequest) => {
      return await authService.forgotPassword(requestData);
    },
    onSuccess(data) {
      toast.success("Đổi mật khẩu thành công");
      router.push(ROUTES.LOGIN);
    },
    onError(error) {
      toast.dismiss("Đổi mật khẩu thất bại");
    },
  });
}
