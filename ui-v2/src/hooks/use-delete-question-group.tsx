import { KEY_QUERIES } from "@/constants";
import { questionGroupService, questionService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useDeleteQuestionGroup = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (questionGroupId: string) => {
      const res = await questionGroupService.deleteQuestionGroup(
        questionGroupId
      );
      return res;
    },
    onSuccess: () => {
      toast.success("Xóa nhóm câu hỏi thành công");
      router.refresh();
    },
    onError: () => {
      toast.error("Xóa nhóm câu hỏi thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.QUESTION_GROUPS],
      });
    },
  });
};
