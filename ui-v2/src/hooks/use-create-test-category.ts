import { KEY_QUERIES } from "@/constants";
import { testCategoryService, testService } from "@/services";
import { ITest, ITestCategoryRequest, ITestRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateTestCategory() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ITestCategoryRequest) => {
      return await testCategoryService.createTestCategory(item);
    },
    onSuccess(data) {
      toast.success("Tạo danh mục bài thi mới thành công");
    },
    onError(error) {
      toast.error("Tạo danh mục bài thi mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TEST_CATEGORIES],
      });
    },
  });
}
