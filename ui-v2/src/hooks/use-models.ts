import { llmModelService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useLLMModels() {
  return useQuery({
    queryKey: [KEY_QUERIES.LLM_MODELS],
    queryFn: async () => await llmModelService.getAllLLMModels(),
  });
}
