import { paymentService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { ICreatePaymentRequest } from "@/types";
import { toast } from "react-hot-toast";

export function useCreatePayment() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreatePaymentRequest) => {
      return await paymentService.createPayment(item);
    },
    onSuccess(data) {
      toast.success("Thanh toán thành công");
    },
    onError(error) {
      toast.error("Thanh toán thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.PAYMENTS] });
    },
  });
}
