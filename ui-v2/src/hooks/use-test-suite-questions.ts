import { KEY_QUERIES } from "@/constants";
import {
  courseCategoryService,
  courseService,
  testSuiteQuestionService,
} from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useTestSuiteQuestions() {
  return useQuery({
    queryKey: [KEY_QUERIES.TEST_SUITE_QUESTIONS],
    queryFn: async () =>
      await testSuiteQuestionService.getAllTestSuiteQuestions(),
  });
}
