import { KEY_QUERIES } from "@/constants";
import { testCategoryService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useTestCategories() {
  return useQuery({
    queryKey: [KEY_QUERIES.TEST_CATEGORIES],
    queryFn: async () => await testCategoryService.getAllTestCategories(),
  });
}
