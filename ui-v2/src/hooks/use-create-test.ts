import { KEY_QUERIES } from "@/constants";
import { testService } from "@/services";
import { ITestRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateTest() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ITestRequest) => {
      return await testService.createTest(item);
    },
    onSuccess(data) {
      toast.success("Tạo bài thi mới thành công");
    },
    onError(error) {
      toast.error("Tạo bài thi mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.TESTS] });
    },
  });
}
