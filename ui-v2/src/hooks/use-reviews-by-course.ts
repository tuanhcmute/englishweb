import { KEY_QUERIES } from "@/constants";
import { reviewService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useReviewsByCourse = (courseId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.REVIEWS, courseId],
    queryFn: async () => {
      const res = await reviewService.getAllReviewsByCourse(courseId);
      return res.data;
    },
  });
};
