import { KEY_QUERIES } from "@/constants";
import { bannerService } from "@/services";
import { ICreateBannerRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateBanner() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateBannerRequest) => {
      return await bannerService.createBanner(item);
    },
    onSuccess(data) {
      toast.success("Tạo thành công");
    },
    onError(error) {
      toast.error("Tạo thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.BANNERS],
      });
    },
  });
}
