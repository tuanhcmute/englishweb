import { KEY_QUERIES } from "@/constants";
import { bannerService, newsCategoryService } from "@/services";
import { IUpdateBannerRequest, IUpdateNewsCategoryRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateBanner() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateBannerRequest) => {
      return await bannerService.updateBanner(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.BANNERS],
      });
    },
  });
}
