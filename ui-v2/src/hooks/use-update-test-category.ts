import { KEY_QUERIES } from "@/constants";
import { testCategoryService } from "@/services";
import { IUpdateTestCategoryRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateTestCategory() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateTestCategoryRequest) => {
      return await testCategoryService.updateTestCategory(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TEST_CATEGORIES],
      });
    },
  });
}
