import { KEY_QUERIES } from "@/constants";
import { userCerdentialService } from "@/services/user-credential.service";
import { useQuery } from "@tanstack/react-query";

export const useUserCredentials = () => {
  return useQuery({
    queryKey: [KEY_QUERIES.USER_CREDENTIALS],
    queryFn: async () => await userCerdentialService.getAllUserCredentials(),
  });
};
