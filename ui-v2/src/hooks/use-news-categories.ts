import { KEY_QUERIES } from "@/constants";
import { newsCategoryService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useNewsCategories(status?: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.NEWS_CATEGORIES, status],
    queryFn: async () => await newsCategoryService.getAllNewsCategories(status),
  });
}
