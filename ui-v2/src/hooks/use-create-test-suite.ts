import { KEY_QUERIES } from "@/constants";
import { testSuiteService, toolService } from "@/services";
import { ICreateTestSuiteRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateTestSuite() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateTestSuiteRequest) => {
      return await testSuiteService.createTestSuite(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TEST_SUITES],
      });
    },
  });
}
