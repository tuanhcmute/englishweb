import { KEY_QUERIES } from "@/constants";
import { questionGroupService } from "@/services";
import { IQuestionGroupRequest, ITest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateQuestionGroup() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IQuestionGroupRequest) => {
      return await questionGroupService.createNewQuestionGroup(item);
    },
    onSuccess(data) {
      toast.success("Tạo nhóm câu hỏi thành công");
    },
    onError(error) {
      toast.error("Tạo nhóm câu hỏi thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.QUESTION_GROUPS],
      });
    },
  });
}
