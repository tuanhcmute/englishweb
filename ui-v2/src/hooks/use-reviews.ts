import { KEY_QUERIES } from "@/constants";
import { reviewService } from "@/services";
import { IGetReviewRequest } from "@/types";
import { useQuery } from "@tanstack/react-query";

export const useReviews = (requestData?: IGetReviewRequest) => {
  return useQuery({
    queryKey: [KEY_QUERIES.REVIEWS, JSON.stringify(requestData)],
    queryFn: async () => {
      const res = await reviewService.getAllReviews(requestData);
      return res.data;
    },
  });
};
