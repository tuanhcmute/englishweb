import { KEY_QUERIES } from "@/constants";
import { newsService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useNewsById(id: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.NEWS, id],
    queryFn: async () => await newsService.getFullNewsById(id),
  });
}
