import { KEY_QUERIES } from "@/constants";
import { flashcardItemService, testService } from "@/services";
import { IUpdateFlashcardItemRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateFlashcardItem() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateFlashcardItemRequest) => {
      return await flashcardItemService.updateFlashcardItem(item);
    },
    onSuccess(data) {
      toast.success("Cập nhật từ vựng mới thành công");
    },
    onError(error) {
      toast.error("Cập nhật từ vựng mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.FLASHCARD_ITEMS],
      });
    },
  });
}
