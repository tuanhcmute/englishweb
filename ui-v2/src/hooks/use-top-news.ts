import { KEY_QUERIES } from "@/constants";
import { newsService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useTopNews(count: number, start: number) {
  return useQuery({
    queryKey: [KEY_QUERIES.NEWS, count, start],
    queryFn: async () => await newsService.getAllTopNews(count, start),
  });
}
