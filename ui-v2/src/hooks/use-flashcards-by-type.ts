import { flashcardService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useFlashcardsByType(type: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.FLASHCARDS, type],
    queryFn: async () => await flashcardService.getAllFlashcardsByType(type),
    enabled: !!type,
  });
}
