import { toolService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useTools() {
  return useQuery({
    queryKey: [KEY_QUERIES.TOOLS],
    queryFn: async () => await toolService.getAllTools(),
  });
}
