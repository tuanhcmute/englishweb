import { KEY_QUERIES } from "@/constants";
import { staticPageService } from "@/services";
import { IUpdateStaticPageRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export const useUpdateStaicPage = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateStaticPageRequest) => {
      return await staticPageService.updateStaticPage(item);
    },
    onSuccess(data) {
      toast.success("Cập nhật trang tĩnh thành công");
    },
    onError(error) {
      console.log({ error });
      toast.error("Cập nhật trang tĩnh thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.STATIC_PAGES] });
    },
  });
};
