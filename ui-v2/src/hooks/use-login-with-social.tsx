import { ROUTES, USER_CREDENTIAL } from "@/constants";
import { ErrorResponse, Role } from "@/enums";
import { parseJsonToObject } from "@/lib";
import { auth } from "@/lib/firebase";
import { authService } from "@/services";
import { IErrorResponse, IUserCredentialResponse } from "@/types";
import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";
import { getCookie } from "cookies-next";
import { AuthProvider, signInWithPopup } from "firebase/auth";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useLoginWithSocial = () => {
  const router = useRouter();

  return useMutation({
    mutationFn: async (authProvider: AuthProvider) => {
      const res = await signInWithPopup(auth, authProvider);
      return await authService.loginSocial({
        avatar: res.user?.photoURL || "",
        email: res.user?.email || "",
        name: res.user?.displayName || "",
        provider: res?.providerId || "local",
        username: res.user?.email?.split("@")[0] || "",
      });
    },
    onSuccess(data) {
      toast.success("Đăng nhập thành công");
      const previousPath = getCookie("prevPath") || ROUTES.HOME;
      const userCredentialJson = getCookie(USER_CREDENTIAL);
      const userCredential = parseJsonToObject(
        userCredentialJson || ""
      ) as IUserCredentialResponse;
      if (userCredential) {
        // Check student user and previous path
        if (
          userCredential.userAuthorities.find(
            (item) => item.role.roleName === Role.ROLE_STUDENT
          )
        ) {
          if (previousPath.includes(ROUTES.ADMIN)) router.push(ROUTES.HOME);
          else router.push(previousPath);
        }
        // Teacher
        if (
          userCredential.userAuthorities.find(
            (item) => item.role.roleName === Role.ROLE_TEACHER
          )
        ) {
          router.push(ROUTES.ADMIN_NEWS_MANAGE);
        }
        // Admin
        if (
          userCredential.userAuthorities.find(
            (item) => item.role.roleName === Role.ROLE_ADMIN
          )
        ) {
          router.push(ROUTES.ADMIN_DASHBOARD);
        }
      }
    },
    onError(axiosError: AxiosError) {
      const data = axiosError.response?.data as IErrorResponse;
      if (data) {
        switch (data.errorCode) {
          case ErrorResponse.USER_EXIST:
            toast.error("Đăng nhập thất bại. Tài khoản đã tổn tại");
            return;
          case ErrorResponse.EMAIL_EXIST:
            toast.error("Đăng nhập thất bại. Email đã tổn tại");
            return;
          case ErrorResponse.USERNAME_EXIST:
            toast.error("Đăng nhập thất bại. Tên đăng nhập đã tổn tại");
            return;
          case ErrorResponse.ROLE_NOT_FOUND:
            toast.error("Đăng ký thất bại. Quyền hạn không hợp lệ");
            return;
          case ErrorResponse.PROVIDER_NOT_FOUND:
            toast.error("Đăng ký thất bại. Phương thức đăng ký không hợp lệ");
            return;
          case ErrorResponse.REDIS_UNCONNECTED:
            toast.error(
              "Đăng ký thất bại. Không thể kết nối với Redis service"
            );
            return;
        }
      }
      toast.error("Đăng nhập thất bại. Vui lòng kiểm tra lại");
    },
  });
};
