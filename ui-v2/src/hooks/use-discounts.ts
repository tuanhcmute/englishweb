import { KEY_QUERIES } from "@/constants";
import { discountService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useDiscounts() {
  return useQuery({
    queryKey: [KEY_QUERIES.DISCOUNTS],
    queryFn: async () => await discountService.getAllDiscounts(),
  });
}
