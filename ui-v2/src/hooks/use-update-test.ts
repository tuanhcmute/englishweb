import { KEY_QUERIES } from "@/constants";
import { testService } from "@/services";
import { IUpdateTestRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateTest() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateTestRequest) => {
      return await testService.updateTest(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa bài thi thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa bài thi mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.TESTS] });
    },
  });
}
