import { KEY_QUERIES } from "@/constants";
import { flashcardItemService } from "@/services";
import { ICreateFlashcardItemRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateFlashcardItem() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateFlashcardItemRequest) => {
      return await flashcardItemService.createFlashcardItem(item);
    },
    onSuccess(data) {
      toast.success("Tạo từ vựng mới thành công");
    },
    onError(error) {
      toast.error("Tạo từ vựng mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.FLASHCARDS],
      });
    },
  });
}
