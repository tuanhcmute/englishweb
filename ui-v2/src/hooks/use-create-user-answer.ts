import { KEY_QUERIES, ROUTES } from "@/constants";
import { userAnswerService } from "@/services";
import { IUserAnswerRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useCreateUserAnswer = () => {
  const queryClient = useQueryClient();
  const router = useRouter();
  return useMutation({
    mutationFn: async (requestData: IUserAnswerRequest) => {
      return await userAnswerService.createNewUserAnswer(requestData);
    },
    onSuccess(data) {
      toast.success("Nộp bài thi thành công");
      router.push(ROUTES.MY_TEST_RESULTS);
    },
    onError(error) {
      toast.error("Nộp bài thi thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.USER_ANSWERS] });
    },
  });
};
