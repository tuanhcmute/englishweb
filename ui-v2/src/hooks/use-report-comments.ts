import { reportCommentService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { IGetReportCommentRequest } from "@/types";

export function useReportComments(requestData?: IGetReportCommentRequest) {
  return useQuery({
    queryKey: [KEY_QUERIES.REPORT_COMMENTS],
    queryFn: async () =>
      await reportCommentService.getAllReportComments(requestData),
  });
}
