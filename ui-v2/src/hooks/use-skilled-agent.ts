import { KEY_QUERIES } from "@/constants";
import { skilledAgentService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useSkilledAgent = (agentId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.SKILLED_AGENTS, agentId],
    queryFn: async () => {
      const res = await skilledAgentService.getSkilledAgentById(agentId);
      return res;
    },
  });
};
