import { authService } from "@/services";
import { useMutation } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useSendOTPForgotPassword() {
  return useMutation({
    mutationFn: async (requestData: string) => {
      return await authService.sendOTPForgotPassword(requestData);
    },
    onSuccess(data) {
      toast.success("Gửi OTP thành công. Vui lòng kiểm tra email");
    },
    onError(error) {
      toast.dismiss(error.message);
    },
  });
}
