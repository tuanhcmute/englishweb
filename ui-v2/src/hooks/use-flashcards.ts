import { flashcardService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useFlashcards() {
  return useQuery({
    queryKey: [KEY_QUERIES.FLASHCARDS],
    queryFn: async () => await flashcardService.getAllFlashcards(),
  });
}
