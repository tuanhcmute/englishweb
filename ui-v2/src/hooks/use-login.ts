import { ROUTES, USER_CREDENTIAL } from "@/constants";
import { ErrorResponse, Role } from "@/enums";
import { parseJsonToObject } from "@/lib";
import { authService } from "@/services";
import { IErrorResponse, IUserCredentialResponse } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { AxiosError } from "axios";
import { getCookie } from "cookies-next";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export function useLogin() {
  // const queryClient = useQueryClient();
  const router = useRouter();

  return useMutation({
    mutationFn: async (requestData: { username: string; password: string }) => {
      return await authService.login(requestData);
    },
    onSuccess(data) {
      toast.success("Đăng nhập thành công");
      const previousPath = getCookie("prevPath") || ROUTES.HOME;
      const userCredentialJson = getCookie(USER_CREDENTIAL);
      const userCredential = parseJsonToObject(
        userCredentialJson || ""
      ) as IUserCredentialResponse;
      if (userCredential) {
        // Check student user and previous path
        if (
          userCredential.userAuthorities.find(
            (item) => item.role.roleName === Role.ROLE_STUDENT
          )
        ) {
          if (previousPath.includes(ROUTES.ADMIN)) router.push(ROUTES.HOME);
          else router.push(previousPath);
        }
        // Teacher
        if (
          userCredential.userAuthorities.find(
            (item) => item.role.roleName === Role.ROLE_TEACHER
          )
        ) {
          router.push(ROUTES.ADMIN_NEWS_MANAGE);
        }
        // Admin
        if (
          userCredential.userAuthorities.find(
            (item) => item.role.roleName === Role.ROLE_ADMIN
          )
        ) {
          router.push(ROUTES.ADMIN_DASHBOARD);
        }
      }
    },
    onError(error) {
      if (error instanceof AxiosError) {
        const errorResponse = error.response?.data as IErrorResponse;
        if (errorResponse) {
          switch (errorResponse.errorCode) {
            case ErrorResponse.USERNAME_NOT_EXIST:
              toast.error("Đăng nhập thất bại. Tên đăng nhập không hợp lệ");
              return;
            case ErrorResponse.PASSWORD_NOT_MATCH:
              toast.error("Đăng nhập thất bại. Mật khẩu không đúng");
              return;
          }
        }
        return;
      }
      toast.error("Đăng nhập thất bại. Vui lòng thử lại");
    },
  });
}
