import { KEY_QUERIES } from "@/constants";
import { flashcardService, studyingFlashcardService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useDeleteStudyingFlashcard = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (id: string) => {
      const res = await studyingFlashcardService.deleteStudyingFlashcard(id);
      return res;
    },
    onSuccess: () => {
      toast.success("Xóa flashcard thành công");
      router.refresh();
    },
    onError: () => {
      toast.error("Xóa flashcard thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.STUDYING_FLASHCARDS],
      });
    },
  });
};
