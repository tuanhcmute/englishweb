import { authService } from "@/services";
import { useMutation } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export const useSendLinkVerification = () => {
  return useMutation({
    mutationFn: async (requestData: string) => {
      return await authService.sendLinkVerification(requestData);
    },
    onSuccess(data) {
      toast.success(
        "Link xác nhận email đã được gửi tới email của bạn. Vui lòng kiểm tra email để xác thực"
      );
    },
    onError(error) {
      toast.dismiss(error.message);
    },
  });
};
