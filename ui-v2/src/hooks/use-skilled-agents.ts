import { KEY_QUERIES } from "@/constants";
import { skilledAgentService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useSkilledAgents = () => {
  return useQuery({
    queryKey: [KEY_QUERIES.SKILLED_AGENTS],
    queryFn: async () => {
      return await skilledAgentService.getAllSkilledAgent();
    },
  });
};
