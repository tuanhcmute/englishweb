import { KEY_QUERIES } from "@/constants";
import { newsCategoryService } from "@/services";
import { IUpdateNewsCategoryRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateNewsCategory() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateNewsCategoryRequest) => {
      return await newsCategoryService.updateNewsCatgory(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.NEWS_CATEGORIES],
      });
    },
  });
}
