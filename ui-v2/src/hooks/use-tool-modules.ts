import { toolService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useToolModules() {
  return useQuery({
    queryKey: [KEY_QUERIES.TOOL_MODULES],
    queryFn: async () => await toolService.getAllToolModules(),
  });
}
