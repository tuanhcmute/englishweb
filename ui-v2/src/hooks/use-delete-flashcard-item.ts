import { KEY_QUERIES } from "@/constants";
import { flashcardItemService, testService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useDeleteFlashcardItem() {
  // const router = useRouter();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (flashcardItemId: string) => {
      return await flashcardItemService.deleteFlashcardItem(flashcardItemId);
    },
    onSuccess: (data) => {
      toast.success(`Xóa từ vựng thành công`);
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.FLASHCARD_ITEMS],
      });
    },
    onError(err) {
      toast.error("Đã có lỗi xảy ra, vui lòng thử lại");
    },
  });
}
