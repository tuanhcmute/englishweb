import { KEY_QUERIES } from "@/constants";
import { reviewService } from "@/services";
import { IUpdateReviewRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateReview() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateReviewRequest) => {
      return await reviewService.updateReview(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.REVIEWS] });
    },
  });
}
