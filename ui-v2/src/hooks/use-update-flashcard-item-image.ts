import { KEY_QUERIES } from "@/constants";
import { flashcardItemService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateFlashcardItemImage() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (requestData: { id: string; image: string }) => {
      return await flashcardItemService.updateFlashcardItemImage(
        requestData.id,
        requestData.image
      );
    },
    onSuccess(data) {
      toast.success("Cập nhật từ vựng mới thành công");
    },
    onError(error) {
      toast.error("Cập nhật từ vựng mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.FLASHCARDS],
      });
    },
  });
}
