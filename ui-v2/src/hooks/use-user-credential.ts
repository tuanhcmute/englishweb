import { KEY_QUERIES } from "@/constants";
import { userCerdentialService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useUserCredential(id: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.USER_CREDENTIALS, id],
    queryFn: async () => {
      return await userCerdentialService.getUserCredentialById(id);
    },
    enabled: !!id,
  });
}
