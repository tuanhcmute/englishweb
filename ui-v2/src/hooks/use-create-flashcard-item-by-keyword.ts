import { KEY_QUERIES } from "@/constants";
import { flashcardItemService } from "@/services";
import { ICreateFlashcardItemByKeywordRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateFlashcardItemByKeyword() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateFlashcardItemByKeywordRequest) => {
      return await flashcardItemService.createFlashcardItemByKeyword(item);
    },
    onSuccess(data) {
      toast.success("Tạo từ vựng mới thành công");
    },
    onError(error) {
      toast.error("Tạo từ vựng mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.FLASHCARDS],
      });
    },
  });
}
