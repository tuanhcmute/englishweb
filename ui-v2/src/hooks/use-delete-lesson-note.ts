import { KEY_QUERIES } from "@/constants";
import { lessonNoteService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useDeleteLessonNote() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (id: string) => {
      return await lessonNoteService.deleteLessonNote(id);
    },
    onSuccess: (data) => {
      toast.success(`Xóa ghi chú thành công`);
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.LESSONS_NOTES] });
    },
    onError(err) {
      toast.error("Xóa ghi chú thất bại, vui lòng thử lại");
    },
  });
}
