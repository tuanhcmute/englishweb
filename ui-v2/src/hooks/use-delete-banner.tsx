import { KEY_QUERIES } from "@/constants";
import { bannerService, questionService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useDeleteBanner = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (bannerId: string) => {
      const res = await bannerService.deleteBanner(bannerId);
      return res;
    },
    onSuccess: () => {
      toast.success("Xóa thành công");
      router.refresh();
    },
    onError: () => {
      toast.error("Xóa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.BANNERS],
      });
    },
  });
};
