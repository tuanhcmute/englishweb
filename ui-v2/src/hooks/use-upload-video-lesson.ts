import { lessonService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { IUploadVideoLessonRequest } from "@/types";
import { toast } from "react-hot-toast";

export function useUploadVideoLesson() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUploadVideoLessonRequest) => {
      return await lessonService.uploadLesson(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa video bài học thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa video bài học thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.LESSONS] });
    },
  });
}
