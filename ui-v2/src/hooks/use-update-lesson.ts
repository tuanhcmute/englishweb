import { lessonService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";
import { IUpdateLessonRequest } from "@/types";
import { toast } from "react-hot-toast";

export function useUpdateLesson() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateLessonRequest) => {
      return await lessonService.updateLesson(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thông tin bài học thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thông tin bài học thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.LESSONS] });
    },
  });
}
