import { KEY_QUERIES } from "@/constants";
import { staticPageService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useStaticPageByCode = (staticPageCode: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.STATIC_PAGES, staticPageCode],
    queryFn: async () =>
      await staticPageService.getStaticPageByCode(staticPageCode),
  });
};
