import { KEY_QUERIES } from "@/constants";
import { staticPageService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useStaticPage = (staticPageId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.STATIC_PAGES, staticPageId],
    queryFn: async () =>
      await staticPageService.getStaticPageById(staticPageId),
  });
};
