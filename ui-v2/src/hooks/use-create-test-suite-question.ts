import { KEY_QUERIES } from "@/constants";
import { testSuiteQuestionService } from "@/services";
import { ICreateTestSuiteQuestionRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateTestSuiteQuestion() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateTestSuiteQuestionRequest) => {
      return await testSuiteQuestionService.createTestSuiteQuestion(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TEST_SUITE_QUESTIONS],
      });
    },
  });
}
