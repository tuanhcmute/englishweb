import { studyingFlashcardService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useStudyingFlashcardsByUserCredential(
  userCredentialId: string
) {
  return useQuery({
    queryKey: [KEY_QUERIES.STUDYING_FLASHCARDS, userCredentialId],
    queryFn: async () =>
      await studyingFlashcardService.getAllStudyingFlashcardByUserCredential(
        userCredentialId
      ),
    enabled: !!userCredentialId,
  });
}
