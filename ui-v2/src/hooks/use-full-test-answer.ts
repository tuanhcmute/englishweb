import { KEY_QUERIES } from "@/constants";
import { fullTestAnswerService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useFullTestAnswer = (testId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.FULL_TEST_ANSWER, testId],
    queryFn: async () => {
      const res = await fullTestAnswerService.getFullTestAnswerByTest(testId);
      return res.data;
    },
    // initialData: init,
  });
};
