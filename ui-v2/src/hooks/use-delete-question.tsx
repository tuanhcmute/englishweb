import { KEY_QUERIES } from "@/constants";
import { questionService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useDeleteQuestion = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (questionId: string) => {
      const res = await questionService.deleteQuestion(questionId);

      return res;
    },
    onSuccess: () => {
      toast.success("Xóa câu hỏi thành công");
      router.refresh();
    },
    onError: () => {
      toast.error("Xóa câu hỏi thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.QUESTION_GROUPS],
      });
    },
  });
};
