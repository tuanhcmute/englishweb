import { KEY_QUERIES } from "@/constants";
import { userAnswerService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useUserAnswer = (id: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.USER_ANSWER, id],
    queryFn: async () => {
      return await userAnswerService.getAllUserAnswerById(id);
    },
  });
};
