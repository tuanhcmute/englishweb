import { KEY_QUERIES } from "@/constants";
import {
  courseCategoryService,
  courseService,
  lessonNoteService,
} from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useLessonNotes(userCredentialId?: string, lessonId?: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.LESSONS_NOTES, userCredentialId, lessonId],
    queryFn: async () =>
      await lessonNoteService.getAllLessonNotes(userCredentialId, lessonId),
  });
}
