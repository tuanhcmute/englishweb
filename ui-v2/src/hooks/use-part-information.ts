import { KEY_QUERIES } from "@/constants";
import {
  partInformationService,
  testCategoryService,
  testService,
} from "@/services";
import { useQuery } from "@tanstack/react-query";

export function usePartInformation(testCategoryId: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.PART_INFORMATION],
    queryFn: async () =>
      await partInformationService.getAllPartInformationByTestCategory(
        testCategoryId
      ),
    enabled: !!testCategoryId,
  });
}
