import { KEY_QUERIES } from "@/constants";
import { orderService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useOrders = (
  userCredentialId?: string,
  paymentStatus?: string
) => {
  return useQuery({
    queryKey: [KEY_QUERIES.ORDERS, userCredentialId, paymentStatus],
    queryFn: async () => {
      const res = await orderService.getAllOrders(
        userCredentialId,
        paymentStatus
      );
      return res.data;
    },
  });
};
