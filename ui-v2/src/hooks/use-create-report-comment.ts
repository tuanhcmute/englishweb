import { KEY_QUERIES } from "@/constants";
import { reportCommentService } from "@/services";
import { ICreateReportCommentRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateReportComment() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateReportCommentRequest) => {
      return await reportCommentService.createReportComment(item);
    },
    onSuccess(data) {
      toast.success("Tạo báo cáo thành công");
    },
    onError(error) {
      toast.error("Tạo báo cáo thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.REPORT_COMMENTS] });
    },
  });
}
