import { KEY_QUERIES } from "@/constants";
import { questionGroupService, testService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export function useQuestionGroups(partId: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.QUESTION_GROUPS],
    queryFn: async () =>
      await questionGroupService.getAllQuestionGroupByPart(partId),
  });
}
