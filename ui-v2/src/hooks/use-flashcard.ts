import { flashcardService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useFlashcard(flashcardId: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.FLASHCARDS, flashcardId],
    queryFn: async () => await flashcardService.getFlashcardById(flashcardId),
  });
}
