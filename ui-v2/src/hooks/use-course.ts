import { KEY_QUERIES } from "@/constants";
import { courseService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useCourse = (courseId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.COURSES, courseId],
    queryFn: async () => {
      const res = await courseService.getCourseById(courseId);
      return res.data;
    },
  });
};
