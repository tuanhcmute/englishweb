import { flashcardService } from "@/services";
import { useQuery } from "@tanstack/react-query";
import { KEY_QUERIES } from "@/constants";

export function useFlashcardsByUserCredential(userCredentialId: string) {
  return useQuery({
    queryKey: [KEY_QUERIES.FLASHCARDS, userCredentialId],
    queryFn: async () =>
      await flashcardService.getAllFlashcardsByUserCredential(userCredentialId),
    enabled: !!userCredentialId,
  });
}
