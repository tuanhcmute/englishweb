import { ROUTES } from "@/constants";
import { ErrorResponse } from "@/enums";
import { authService } from "@/services";
import { IBaseResponse, IUserCredentialRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { AxiosError } from "axios";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export function useSignUp() {
  const router = useRouter();
  return useMutation({
    mutationFn: async (requestData: IUserCredentialRequest) => {
      return await authService.signUp(requestData);
    },
    onSuccess(data) {
      toast.success("Đăng ký tài khoản thành công");
      router.push(ROUTES.LOGIN);
    },
    onError(error) {
      console.log({ error });
      if (error instanceof AxiosError) {
        const errorResponse = error.response?.data as IBaseResponse<void>;
        if (errorResponse?.errors) {
          switch (errorResponse.errors.errorCode) {
            case ErrorResponse.EMAIL_EXIST:
              toast.error("Đăng ký thất bại. Email đã được sử dụng");
              return;
            case ErrorResponse.USERNAME_EXIST:
              toast.error("Đăng ký thất bại. Tên đăng nhập đã được sử dụng");
              return;
            case ErrorResponse.ROLE_NOT_FOUND:
              toast.error("Đăng ký thất bại. Quyền hạn không hợp lệ");
              return;
            case ErrorResponse.PROVIDER_NOT_FOUND:
              toast.error("Đăng ký thất bại. Phương thức đăng ký không hợp lệ");
              return;
            case ErrorResponse.REDIS_UNCONNECTED:
              toast.error(
                "Đăng ký thất bại. Không thể kết nối với Redis service"
              );
              return;
          }
        }
        return;
      }
      toast.error("Đăng ký thất bại. Vui lòng thử lại");
    },
    // onSettled() {
    //   queryClient.invalidateQueries({ queryKey: [KEY_QUERIES.LOGIN] });
    // },
  });
}
