import { KEY_QUERIES } from "@/constants";
import { newsCategoryService } from "@/services";
import { ICreateNewsCategoryRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateNewsCategory() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateNewsCategoryRequest) => {
      return await newsCategoryService.createNewsCatgory(item);
    },
    onSuccess(data) {
      toast.success("Tạo danh mục bài viết mới thành công");
    },
    onError(error) {
      toast.error("Tạo danh mục bài viết mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.NEWS_CATEGORIES],
      });
    },
  });
}
