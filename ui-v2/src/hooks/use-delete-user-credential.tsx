import { KEY_QUERIES } from "@/constants";
import { questionService, userCerdentialService } from "@/services";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export const useDeleteUserCredential = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (id: string) => {
      const res = await userCerdentialService.deleteUserCredential(id);
      return res;
    },
    onSuccess: () => {
      toast.success("Xóa thành công");
      router.refresh();
    },
    onError: () => {
      toast.error("Xóa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.USER_CREDENTIALS],
      });
    },
  });
};
