import { KEY_QUERIES } from "@/constants";
import { skilledAgentChatService } from "@/services";
import {
  ICreateSkilledAgentChatRequest,
  ISkilledAgentChatResponse,
} from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";

export function useCreateSkilledAgentChat() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (
      item: ICreateSkilledAgentChatRequest
    ): Promise<ISkilledAgentChatResponse> => {
      return await skilledAgentChatService.createSkilledAgentChat(item);
    },
    onSuccess(data) {},
    onError(error) {},
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.SKILLED_AGENT_CHATS],
      });
    },
  });
}
