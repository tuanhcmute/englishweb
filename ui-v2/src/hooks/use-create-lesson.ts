import { KEY_QUERIES } from "@/constants";
import { lessonService } from "@/services";
import { ICreateLessonRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateLesson() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateLessonRequest) => {
      return await lessonService.createLesson(item);
    },
    onSuccess(data) {
      toast.success("Tạo bài học mới thành công");
    },
    onError(error) {
      toast.error("Tạo bài học mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.UNITS],
      });
    },
  });
}
