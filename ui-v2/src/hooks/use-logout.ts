import { ROUTES } from "@/constants";
import { useMutation } from "@tanstack/react-query";
import axios from "axios";
import { useRouter } from "next/navigation";
import { toast } from "react-hot-toast";

export function useLogout() {
  const router = useRouter();
  return useMutation({
    mutationFn: async () => {
      await axios.post("/api/logout");
    },
    onSuccess() {
      toast.success("Đăng xuất tài khoản thành công");
      router.push(ROUTES.LOGIN);
    },
    onError() {
      toast.success("Đăng xuất tài khoản thất bại");
    },
  });
}
