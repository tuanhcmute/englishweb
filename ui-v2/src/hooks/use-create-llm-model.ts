import { KEY_QUERIES } from "@/constants";
import { llmModelService } from "@/services";
import { ICreateLLMModelRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useCreateLLMModel() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: ICreateLLMModelRequest) => {
      return await llmModelService.createLLMModel(item);
    },
    onSuccess(data) {
      toast.success("Tạo mới thành công");
    },
    onError(error) {
      toast.error("Tạo mới thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.LLM_MODELS],
      });
    },
  });
}
