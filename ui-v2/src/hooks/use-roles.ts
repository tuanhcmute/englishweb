import { KEY_QUERIES } from "@/constants";
import { roleService, staticPageService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useRoles = () => {
  return useQuery({
    queryKey: [KEY_QUERIES.ROLES],
    queryFn: async () => {
      const res = await roleService.getAllRoles();
      return res;
    },
  });
};
