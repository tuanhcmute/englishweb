import { KEY_QUERIES } from "@/constants";
import { scoreConversionService } from "@/services";
import { IUpdateScoreConversionRequest } from "@/types";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

export function useUpdateScoreConversion() {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (item: IUpdateScoreConversionRequest) => {
      return await scoreConversionService.updateScoreConversion(item);
    },
    onSuccess(data) {
      toast.success("Chỉnh sửa thành công");
    },
    onError(error) {
      toast.error("Chỉnh sửa thất bại");
    },
    onSettled() {
      queryClient.invalidateQueries({
        queryKey: [KEY_QUERIES.TESTS],
      });
    },
  });
}
