import { KEY_QUERIES } from "@/constants";
import { testSuiteService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useTestSuites = () => {
  return useQuery({
    queryKey: [KEY_QUERIES.TEST_SUITES],
    queryFn: async () => {
      return await testSuiteService.getAllTestSuites();
    },
  });
};
