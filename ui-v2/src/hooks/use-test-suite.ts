import { KEY_QUERIES } from "@/constants";
import { testSuiteService } from "@/services";
import { useQuery } from "@tanstack/react-query";

export const useTestSuite = (testSuiteId: string) => {
  return useQuery({
    queryKey: [KEY_QUERIES.TEST_SUITES, testSuiteId],
    queryFn: async () => {
      const res = await testSuiteService.getTestSuiteById(testSuiteId);
      return res;
    },
  });
};
