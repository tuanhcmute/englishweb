"use client";

import { ReactElement, ReactNode } from "react";
import { AppLayout } from "@/components/shared/layout";
import {
  AboutUs,
  Banner,
  Courses,
  News,
  Reviews,
  Tests,
  Understand,
} from "@/components/app/section";
import { ErrorBoundary } from "react-error-boundary";

function Layout({ children }: { children: ReactNode }): ReactElement {
  return <AppLayout>{children}</AppLayout>;
}

export default function HomePage(): ReactElement {
  return (
    <Layout>
      {/* Banner */}
      <ErrorBoundary fallback={<div>Something went wrong</div>}>
        <Banner />
      </ErrorBoundary>
      {/* About us */}
      <AboutUs />
      {/* Courses */}
      <ErrorBoundary fallback={<div>Something went wrong</div>}>
        <Courses />
      </ErrorBoundary>
      {/* Understand */}
      <Understand />
      {/* Tests */}
      <Tests />
      {/* comments */}
      <Reviews />
      {/* News */}
      <News />
      {/* Register - Advise */}
      {/* <RegisterAdvise /> */}
    </Layout>
  );
}
