"use client";

import React, { ReactNode } from "react";
import {
  Avatar,
  AvatarFallback,
  AvatarImage,
  Collapsible,
  CollapsibleContent,
} from "@/components/ui";
import Link from "next/link";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import { cn } from "@/lib";
import { usePathname } from "next/navigation";
import { getCookie } from "cookies-next";
import { useUserCredential } from "@/hooks";

export const MyAccountLayout: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const pathname = usePathname();
  const [isOpen, setIsOpen] = React.useState(true);
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const userCredentialQuery = useUserCredential(userCredentialId || "");

  return (
    <div id='my-account' className='py-10 bg-gray-100 dark:bg-slate-950'>
      <div className='container flex items-start gap-5 flex-col md:flex-row'>
        {/* Sidebar */}
        <div className='border top-20 md:sticky w-full md:w-3/5 lg:w-1/3 rounded-md dark:bg-slate-900 bg-white'>
          <div className='p-2'>
            <Collapsible
              open={isOpen}
              onOpenChange={setIsOpen}
              className='w-full space-y-2'
            >
              <div className='flex items-center gap-2'>
                <Avatar>
                  <AvatarImage
                    src={userCredentialQuery.data?.data.avatar}
                    alt='@shadcn'
                  />
                  <AvatarFallback>CN</AvatarFallback>
                </Avatar>
                <div>
                  <p className='font-bold'>
                    {userCredentialQuery.data?.data.username}
                  </p>
                  <p className='break-words text-sm'>
                    {userCredentialQuery.data?.data.email}
                  </p>
                </div>
              </div>
              <CollapsibleContent className='space-y-2'>
                <div
                  className={cn(
                    "px-4 py-2 text-sm hover:shadow-sm transition-all hover:text-blue-400",
                    pathname.includes("/my-account/profile")
                      ? "text-blue-400"
                      : ""
                  )}
                >
                  <Link href='/my-account/profile' className='block'>
                    Thông tin cá nhân
                  </Link>
                </div>
                <div
                  className={cn(
                    "px-4 py-2 text-sm hover:shadow-sm transition-all hover:text-blue-400",
                    pathname.includes("/my-account/course-bought")
                      ? "text-blue-400"
                      : ""
                  )}
                >
                  <Link href='/my-account/course-bought' className='block'>
                    Lịch sử mua khóa học
                  </Link>
                </div>
                <div
                  className={cn(
                    "px-4 py-2 text-sm hover:shadow-sm transition-all hover:text-blue-400",
                    pathname.includes("/my-account/courses")
                      ? "text-blue-400"
                      : ""
                  )}
                >
                  <Link href='/my-account/courses' className='block'>
                    Khóa học của tôi
                  </Link>
                </div>
                <div
                  className={cn(
                    "px-4 py-2 text-sm hover:shadow-sm transition-all hover:text-blue-400",
                    pathname.includes(ROUTES.MY_TEST_RESULTS)
                      ? "text-blue-400"
                      : ""
                  )}
                >
                  <Link href={ROUTES.MY_TEST_RESULTS} className='block'>
                    Kết quả luyện thi
                  </Link>
                </div>
              </CollapsibleContent>
            </Collapsible>
          </div>
        </div>
        {/* Content */}
        <div className='w-full border rounded-md bg-white dark:bg-slate-900'>
          {children}
        </div>
      </div>
    </div>
  );
};
