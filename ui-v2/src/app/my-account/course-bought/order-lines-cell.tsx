import { IOrderLineResponse, IOrderResponse } from "@/types"
import { Row } from "@tanstack/react-table"
import { useRouter } from "next/navigation"
import React from "react"

export const OrderLinesCell: React.FC<{ row: Row<IOrderResponse> }> = ({
  row,
}) => {
  const router = useRouter()
  const orderLines = row.getValue("orderLines") as IOrderLineResponse[]
  const orderLine = orderLines?.[0]
  return (
    <div
      className="flex justify-center font-medium w-[150px] hover:underline cursor-pointer"
      onClick={() => router.push(`/courses/${orderLine?.course?.id}`)}
    >
      {orderLine && orderLine?.course?.courseName}
    </div>
  )
}
