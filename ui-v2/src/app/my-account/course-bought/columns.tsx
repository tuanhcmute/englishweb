"use client"

import {
  Button,
  Checkbox,
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui"
import React from "react"
import { MoreHorizontal } from "lucide-react"

import { ColumnDef } from "@tanstack/react-table"
import { IInvoiceResponse, IOrderLineResponse, IOrderResponse } from "@/types"
import { convertNumberToMoney } from "@/lib"
import { useRouter } from "next/navigation"
import { OrderLinesCell } from "./order-lines-cell"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-center w-fit">Mã đơn hàng</div>,
    cell: ({ row }) => {
      const value = row.getValue("id") as string
      return <div className="font-medium">{value.slice(0, 8)}</div>
    },
  },
  {
    accessorKey: "orderDate",
    header: () => <div className="">Thời gian mua</div>,
    cell: ({ row }) => {
      const orderDate = row.getValue("orderDate") as string
      return (
        <div className="flex justify-center font-medium w-36">{orderDate}</div>
      )
    },
  },
  {
    accessorKey: "paymentStatus",
    header: () => <div className="text-center">Trạng thái thanh toán</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center font-medium w-36">
          <div
            className={`py-1 px-2 rounded-full w-fit text-gray-100 ${
              row.getValue("paymentStatus") ? "bg-green-500" : "bg-yellow-500"
            }`}
          >
            {row.getValue("paymentStatus")
              ? "Đã thanh toán"
              : "Chưa thanh toán"}
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "invoice",
    header: () => <div className="text-center">Số tiền</div>,
    cell: ({ row }) => {
      const invoice = row.getValue("invoice") as IInvoiceResponse
      return (
        <div className="flex justify-center font-medium w-24">
          {invoice?.totalPrice && convertNumberToMoney(invoice?.totalPrice)}
        </div>
      )
    },
  },
  {
    accessorKey: "orderLines",
    header: () => <div className="text-center">Khóa học</div>,
    cell: OrderLinesCell,
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      return (
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="ghost" className="h-8 w-8 p-0">
              <MoreHorizontal className="h-4 w-4" />
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            <DropdownMenuLabel>Thao tác</DropdownMenuLabel>
          </DropdownMenuContent>
        </DropdownMenu>
      )
    },
  },
] as ColumnDef<IOrderResponse>[]
