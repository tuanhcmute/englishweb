"use client";

import { Loading } from "@/components/shared/loading";
import { DataTable } from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { useOrders } from "@/hooks";
import { getCookie } from "cookies-next";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function CourseBoughtPage(): ReactElement {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const ordersByUserCredentialQuery = useOrders(userCredentialId);

  if (ordersByUserCredentialQuery.isLoading) return <Loading />;
  if (ordersByUserCredentialQuery.error)
    return <div>{ordersByUserCredentialQuery.error.message}</div>;

  return (
    <div className='p-2'>
      <div className='mt-4'>
        {ordersByUserCredentialQuery.data && (
          <DataTable
            key={"id"}
            actionButtons={<div></div>}
            columns={columns}
            filterByColumn='id'
            data={ordersByUserCredentialQuery.data}
          />
        )}
      </div>
    </div>
  );
}
