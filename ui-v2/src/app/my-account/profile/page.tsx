"use client";

import { UpdatePasswordModal } from "@/components/app/modal";
import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
  Separator,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import {
  useSendLinkVerification,
  useUpdateUserCredential,
  useUserCredential,
} from "@/hooks";
import { cn } from "@/lib";
import { IUpdateUserCredentialRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { getCookie } from "cookies-next";
import { MailWarningIcon, VerifiedIcon } from "lucide-react";
import React, { ReactElement } from "react";
import { useForm } from "react-hook-form";
import * as z from "zod";

const formSchema = z.object({
  fullName: z.string().min(2, {
    message: "Username must be at least 2 characters.",
  }),
  dob: z.string().min(1),
  gender: z.string().min(1),
  phoneNumber: z.string().min(1),
  major: z.string().min(1),
  address: z.string().min(1),
  id: z.string().min(1),
});

export default function ProfilePage(): ReactElement {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const userCredentialQuery = useUserCredential(userCredentialId || "");
  const { mutateAsync: handler, isPending } = useUpdateUserCredential();
  const { mutateAsync: handleSendLinkVerification } = useSendLinkVerification();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      fullName: userCredentialQuery.data?.data.fullName || "",
      dob: userCredentialQuery.data?.data.dob || "",
      gender: userCredentialQuery.data?.data.gender || "",
      phoneNumber: userCredentialQuery.data?.data.phoneNumber,
      major: userCredentialQuery.data?.data.major || "",
      address: userCredentialQuery.data?.data.address || "",
      id: userCredentialId,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    console.log(values);
    const requestData = values as IUpdateUserCredentialRequest;
    await handler(requestData);
  }

  if (userCredentialQuery.error)
    return (
      <div className="text-center w-full font-bold text-red-500">
        {userCredentialQuery.error.message}
      </div>
    );

  return (
    <div className="p-2" id="profile">
      <p className="font-bold text-xl pb-1">Thông tin cá nhân</p>
      <Separator />
      <div className="mt-2">
        <p className="font-bold">Thông tin cơ bản</p>
        <div className="pl-2 mt-2">
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
              <div className="grid grid-cols-1 lg:grid-cols-2 gap-2">
                <FormField
                  control={form.control}
                  name="fullName"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Họ và tên (*)</FormLabel>
                      <FormControl>
                        <Input placeholder="Nhập họ và tên" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="dob"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Ngày sinh (*)</FormLabel>
                      <FormControl>
                        <Input
                          placeholder="Nhập ngày sinh"
                          {...field}
                          type="date"
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="gender"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Giới tính</FormLabel>
                      <FormControl>
                        <Select
                          defaultValue={form.getValues("gender") || ""}
                          onValueChange={(value) => {
                            form.setValue("gender", value);
                          }}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Lựa chọn giới tính" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectItem value="male" className="cursor-pointer">
                              Nam
                            </SelectItem>
                            <SelectItem
                              value="female"
                              className="cursor-pointer"
                            >
                              Nữ
                            </SelectItem>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="phoneNumber"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Số điện thoại</FormLabel>
                      <FormControl>
                        <Input placeholder="Nhập số điện thoại" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="major"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Nghề nghiệp</FormLabel>
                      <FormControl>
                        <Input placeholder="Nhập nghề nghiệp" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="address"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Địa chỉ</FormLabel>
                      <FormControl>
                        <Input placeholder="Nhập địa chỉ" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="flex justify-center">
                <Button
                  type="submit"
                  variant="destructive"
                  className="border border-purple-800 bg-transparent hover:bg-purple-800 text-purple-800 hover:text-white dark:text-white"
                  onClick={() => console.log(form.getValues())}
                  disabled={isPending}
                >
                  Cập nhật
                </Button>
              </div>
            </form>
          </Form>
        </div>
      </div>
      <div className="mt-2">
        <p className="font-bold">Xác thực email</p>
        <div className="text-sm mt-1">
          <Table>
            <TableHeader>
              <TableRow>
                <TableHead className="w-[100px]">Email</TableHead>
                <TableHead className="">Trạng thái</TableHead>
                <TableHead className="text-right"></TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              <TableRow>
                <TableCell className="font-medium">
                  {userCredentialQuery.data?.data.email}
                </TableCell>
                <TableCell className="font-medium">
                  <div className="w-[200px]">
                    <div
                      className={cn(
                        "py-1 px-2 border rounded-full text-gray-200 w-fit",
                        userCredentialQuery.data?.data.verified
                          ? "bg-green-600"
                          : "bg-yellow-500"
                      )}
                    >
                      {userCredentialQuery.data?.data.verified ? (
                        <div className="flex items-center gap-1">
                          <VerifiedIcon /> Đã xác thực
                        </div>
                      ) : (
                        <div className="flex items-center gap-1">
                          <MailWarningIcon className="w-4 h-4" /> Chưa xác thực
                        </div>
                      )}
                    </div>
                  </div>
                </TableCell>
                <TableCell className="font-medium text-right">
                  <div className="flex items-center gap-2 justify-end">
                    {/* <p className="hover:text-purple-800 cursor-pointer italic transition-all min-w-[100px] hover:underline">
                      Thay đổi email
                    </p> */}
                    <p
                      className="hover:text-purple-800 cursor-pointer italic transition-all min-w-[100px] hover:underline"
                      onClick={async () =>
                        await handleSendLinkVerification(
                          userCredentialQuery.data?.data.email || ""
                        )
                      }
                    >
                      Xác thực email
                    </p>
                  </div>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </div>
      </div>
      <div className="mt-2">
        <p className="font-bold">Mật khẩu</p>
        <UpdatePasswordModal />
      </div>
    </div>
  );
}
