export const description = `
Chào mừng bạn đến với trang Quản lý tài khoản của English Academy - nơi bạn có thể quản lý thông tin cá nhân, tiến trình học tập và tương tác với cộng đồng học tập của chúng tôi.

Trang Quản lý tài khoản của English Academy cung cấp cho bạn một giao diện dễ sử dụng và tiện lợi để quản lý các thông tin cá nhân và hoạt động học tập của mình. Dưới đây là những tính năng chính và lợi ích mà trang Quản lý tài khoản mang lại:

1. Thông tin cá nhân: Bạn có thể cập nhật và quản lý thông tin cá nhân của mình, bao gồm tên, địa chỉ email, mật khẩu và hình đại diện. Điều này giúp đảm bảo rằng thông tin cá nhân của bạn luôn được cập nhật và an toàn.

2. Tiến trình học tập: Trang Quản lý tài khoản cung cấp cho bạn một cái nhìn tổng quan về tiến trình học tập của bạn. Bạn có thể xem các khóa học đã đăng ký, bài tập đã hoàn thành, bài kiểm tra đã làm và đánh giá điểm số của mình. Điều này giúp bạn theo dõi tiến bộ và chủ động trong việc quản lý học tập của mình.

3. Quản lý thanh toán: Trang Quản lý tài khoản cho phép bạn quản lý thông tin thanh toán của mình, bao gồm cập nhật thông tin thẻ tín dụng hoặc phương thức thanh toán khác, xem lịch sử thanh toán và quản lý các gói dịch vụ hoặc khóa học đã mua.

4. Giao tiếp với cộng đồng: Trang Quản lý tài khoản kết nối bạn với cộng đồng học tập của chúng tôi. Bạn có thể tham gia vào các diễn đàn thảo luận, chia sẻ kinh nghiệm học tập và tương tác với các thành viên khác. Điều này tạo ra một môi trường học tập sôi động và khích lệ sự trao đổi thông tin và hỗ trợ giữa các thành viên.

5. Hỗ trợ khách hàng: Trang Quản lý tài khoản cung cấp cơ chế hỗ trợ khách hàng thuận tiện. Bạn có thể gửi yêu cầu hỗ trợ, đặt câu hỏi hoặc báo lỗi liên quan đến tài khoản và học tập của mình. Đội ngũ hỗ trợ của chúng tôi sẽ nhanh chóng giải quyết và đáp ứng các yêu cầu của bạn.

Trang Quản lý tài khoản của English Academy giúp bạn có quyền kiểm soát và quản lý thông tin cá nhân và hoạt động học tập của mình một cách thuận tiện và an toàn. Hãy truy cập và tận dụng các tính năng để tạo trải nghiệm học tập tốt nhất và tương tác với cộng đồng học tập của chúng tôi.
`;
