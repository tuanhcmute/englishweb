"use client";

import { Loading } from "@/components/shared/loading";
import {
  Button,
  Separator,
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import { PracticeType } from "@/enums";
import { useUserAnswers } from "@/hooks";
import { millisToMinutesAndSeconds } from "@/lib";
import { IUserAnswerResponse } from "@/types";
import { getCookie } from "cookies-next";
import Link from "next/link";
import { ReactElement } from "react";

export default function TestResultsPage(): ReactElement {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const userAnswerQuery = useUserAnswers(userCredentialId);
  if (userAnswerQuery.isLoading) return <Loading />;

  return (
    <div className='p-2' id='test-results'>
      <p className='font-bold text-xl pb-1'>Kết quả luyện thi</p>
      <Separator />
      <div className='mt-5'>
        <Table>
          <TableCaption>Danh sách kết quả làm bài của bạn</TableCaption>
          <TableHeader>
            <TableRow>
              <TableHead className='max-w-[200px] text-center'>
                Ngày làm
              </TableHead>
              <TableHead className='max-w-[100px] text-center'>
                Loại bài thi
              </TableHead>
              <TableHead>Tên bài thi</TableHead>
              <TableHead className='max-w-[100px] text-center'>
                Kết quả
              </TableHead>
              <TableHead className='max-w-[100px] text-center'>
                Thời gian làm bài
              </TableHead>
              <TableHead className='text-center max-w-[100px]'>
                Chi tiết
              </TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {userAnswerQuery.data?.data.map((item: IUserAnswerResponse) => {
              return (
                <TableRow key={item.id}>
                  <TableCell className='font-medium w-[200px]'>
                    {item.testDate}
                  </TableCell>
                  <TableCell className='font-medium w-[200px]'>
                    <div className='py-1 px-3 rounded-full bg-green-500 w-fit text-white'>
                      {item.practiceType === PracticeType.FULL_TEST
                        ? "Thi thử"
                        : "Luyện tập"}
                    </div>
                  </TableCell>
                  <TableCell className='font-medium'>
                    <Link href={`${ROUTES.TESTS}/${item.test.id}`}>
                      <Button variant='link'>{item.test.testName}</Button>
                    </Link>
                  </TableCell>
                  <TableCell>{item.totalScore} điểm</TableCell>
                  <TableCell>
                    {item.completionTime &&
                      millisToMinutesAndSeconds(
                        item.completionTime * 60000 || 0
                      )}
                  </TableCell>
                  <TableCell className='text-center'>
                    <Link href={`${ROUTES.PRACTICE_RESULTS}/${item.id}`}>
                      <Button variant='link'>Xem chi tiết</Button>
                    </Link>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    </div>
  );
}
