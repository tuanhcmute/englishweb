"use client";

import { DataTable, Skeleton } from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { useOrders } from "@/hooks";
import { getCookie } from "cookies-next";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function OrderedCoursePage(): ReactElement {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const ordersByUserCredentialQuery = useOrders(userCredentialId, "1");

  if (ordersByUserCredentialQuery.error)
    return <div>{ordersByUserCredentialQuery.error.message}</div>;

  return (
    <div className='p-2'>
      <div className='mt-4'>
        {ordersByUserCredentialQuery.isLoading ||
        !ordersByUserCredentialQuery.data ? (
          <div className='my-4 flex flex-col gap-2'>
            {Array.from({ length: 5 }).map((_, index) => {
              return (
                <div key={index} className=''>
                  <Skeleton className=' h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
                </div>
              );
            })}
          </div>
        ) : (
          <DataTable
            key={"id"}
            actionButtons={<div></div>}
            columns={columns}
            filterByColumn='id'
            data={ordersByUserCredentialQuery.data}
          />
        )}
      </div>
    </div>
  );
}
