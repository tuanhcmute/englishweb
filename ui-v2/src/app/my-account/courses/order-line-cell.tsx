"use client";

import React from "react";
import { MoreHorizontal } from "lucide-react";

import { ColumnDef, Row } from "@tanstack/react-table";
import { IOrderLineResponse, IOrderResponse } from "@/types";
import { useRouter } from "next/navigation";

export const OrderLineCell = ({ row }: { row: Row<IOrderResponse> }) => {
  const router = useRouter();
  const orderLines = row.getValue("orderLines") as IOrderLineResponse[];
  const orderLine = orderLines?.[0];

  const handleClick = () => {
    router.push(`/courses/${orderLine?.course?.id}`);
  };

  return (
    <div
      className="font-medium hover:underline cursor-pointer"
      onClick={handleClick}
    >
      {orderLine && orderLine?.course?.courseName}
    </div>
  );
};
