"use client";

import {
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuLabel,
  DropdownMenuTrigger,
} from "@/components/ui";
import React from "react";
import { MoreHorizontal } from "lucide-react";

import { ColumnDef } from "@tanstack/react-table";
import { IOrderResponse } from "@/types";
import { OrderLineCell } from "./order-line-cell";

export const columns = [
  {
    accessorKey: "orderLines",
    header: () => <div className="text-center">Tên khóa học</div>,
    cell: OrderLineCell,
  },
  {
    accessorKey: "actions",
    header: () => <div className=""></div>,
    cell: ({ row }) => {
      return (
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button variant="ghost" className="h-8 w-8 p-0">
              <MoreHorizontal className="h-4 w-4" />
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
            <DropdownMenuLabel>Thao tác</DropdownMenuLabel>
          </DropdownMenuContent>
        </DropdownMenu>
      );
    },
  },
] as ColumnDef<IOrderResponse>[];
