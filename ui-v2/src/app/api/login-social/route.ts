import {
  ACCESS_TOKEN_KEY,
  BASE_URL,
  REFRESH_TOKEN_KEY,
  USER_CREDENTIAL,
  USER_CREDENTIAL_ID,
} from "@/constants";
import { httpServer } from "@/lib";
import {
  IBaseResponse,
  ITokenResponse,
  IUserCredentialResponse,
} from "@/types";
import axios, { AxiosError } from "axios";
import { cookies } from "next/headers";
import { NextResponse } from "next/server";

export async function POST(req: Request, res: Response) {
  try {
    if (req.method === "POST") {
      const requestData: {
        avatar: string;
        email: string;
        name: string;
        provider: string;
        username: string;
      } = await req.json();

      // Call API
      const { data, errors, message }: IBaseResponse<{ data: ITokenResponse }> =
        await axios
          .post(`${BASE_URL}/auth/loginWithSocial`, requestData)
          .then((r) => {
            return r.data;
          });

      console.log(errors);
      if (errors) {
        return NextResponse.json({ ...errors }, { status: 400 });
      }

      // Fetch user crendential
      const res: IBaseResponse<{ data: IUserCredentialResponse }> =
        await httpServer(
          {
            accessToken: data.data.accessToken as string,
            refreshToken: data.data.accessToken as string,
          },
          `/user-credential/credential?username=${requestData.username}`
        );

      // save token
      cookies().set({
        name: ACCESS_TOKEN_KEY,
        value: data.data.accessToken,
        maxAge: 60 * 60 * 23, // 23 hours
        path: "/",
      });
      cookies().set({
        name: REFRESH_TOKEN_KEY,
        value: data.data.accessToken,
        maxAge: 60 * 60 * 24 * 6, // 6 days
        path: "/",
      });
      // Save user credential
      cookies().set({
        name: USER_CREDENTIAL,
        value: JSON.stringify(res.data.data),
        maxAge: 60 * 60 * 23, // 23 hours
        path: "/",
      });
      cookies().set({
        name: USER_CREDENTIAL_ID,
        value: res.data.data.id,
        maxAge: 60 * 60 * 23, // 23 hours
        path: "/",
      });
      return NextResponse.json({ message }, { status: 200 });
    }
    return NextResponse.json(
      { message: "Method not allowed" },
      { status: 405 }
    );
  } catch (error) {
    if (error instanceof AxiosError) {
      console.log({ error: error.response?.data });
      return NextResponse.json(
        {
          message: error.response?.data.message || "Internal server error",
          error: error.response?.data.error || null,
        },
        { status: error.response?.status || 500 }
      );
    }
    return NextResponse.json(
      { message: "Internal server error" },
      { status: 500 }
    );
  }
}
