import {
  ACCESS_TOKEN_KEY,
  REFRESH_TOKEN_KEY,
  USER_CREDENTIAL,
  USER_CREDENTIAL_ID,
} from "@/constants";
import { cookies } from "next/headers";
import { NextResponse } from "next/server";

export async function POST(req: Request, res: Response) {
  try {
    if (req.method === "POST") {
      // delete token
      cookies().delete(ACCESS_TOKEN_KEY);
      cookies().delete(REFRESH_TOKEN_KEY);
      // Delete user credential
      cookies().delete(USER_CREDENTIAL);
      cookies().delete(USER_CREDENTIAL_ID);
      return NextResponse.json(
        { message: "Logout successfully" },
        { status: 200 }
      );
    }
    return NextResponse.json(
      { message: "Method not allowed" },
      { status: 405 }
    );
  } catch (error) {
    console.log({ error });
    return NextResponse.json(
      { message: "Internal server error" },
      { status: 500 }
    );
  }
}
