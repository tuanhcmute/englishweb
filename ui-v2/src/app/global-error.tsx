"use client";

import { ROUTES } from "@/constants";
import Link from "next/link";
import { useEffect } from "react";

export default function GlobalError({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error);
  }, [error]);

  return (
    <html>
      <body>
        <section className='flex items-center h-full p-16 dark:bg-gray-50 dark:text-gray-800'>
          <div className='container flex flex-col items-center justify-center px-5 mx-auto my-8'>
            <div className='max-w-md text-center'>
              <h2 className='mb-8 font-extrabold text-9xl dark:text-gray-400'>
                <span className='sr-only'>Error</span>
              </h2>
              <p className='text-2xl font-semibold md:text-3xl'>
                {error.message}
              </p>
              <p className='mt-4 mb-8 dark:text-gray-600'>
                But dont worry, you can find plenty of other things on our
                homepage.
              </p>
              <p onClick={() => reset()}>Try Again</p>
              <Link
                href={ROUTES.HOME}
                className='px-8 py-3 font-semibold rounded dark:bg-violet-600 dark:text-gray-50'
              >
                Back to homepage
              </Link>
            </div>
          </div>
        </section>
      </body>
    </html>
  );
}
