"use client";

import React, { ReactNode } from "react";
import { TooltipProvider } from "@/components/ui";

export const TooltipLayout: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  return <TooltipProvider>{children}</TooltipProvider>;
};
