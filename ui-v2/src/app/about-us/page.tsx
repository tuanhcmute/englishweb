"use client";

import { Loading } from "@/components/shared/loading";
import { ROUTES } from "@/constants";
import { StaticPageCode } from "@/enums";
import { useStaticPageByCode } from "@/hooks";
import { cn } from "@/lib";
import { HomeIcon } from "@radix-ui/react-icons";
import { ReactElement } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Skeleton,
} from "@/components/ui";
import Link from "next/link";

export default function AboutUsPage(): ReactElement {
  const staticPageByCodeQuery = useStaticPageByCode(StaticPageCode.ABOUT_US);

  if (staticPageByCodeQuery.error)
    return (
      <div className='text-center'>{staticPageByCodeQuery.error.message}</div>
    );

  return (
    <div className={cn("bg-gray-100 dark:bg-slate-950")}>
      <div className='container py-4'>
        {/* Path */}
        <div className='pt-4'>
          <Breadcrumb>
            <BreadcrumbList>
              <BreadcrumbItem>
                <BreadcrumbLink>
                  <Link href={ROUTES.HOME} className='flex items-center gap-1'>
                    <HomeIcon className='w-4' />
                    Trang chủ
                  </Link>
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbSeparator className='flex items-center' />
              <BreadcrumbItem>
                <BreadcrumbPage className='flex items-center gap-1'>
                  Về chúng tôi
                </BreadcrumbPage>
              </BreadcrumbItem>
            </BreadcrumbList>
          </Breadcrumb>
        </div>

        {staticPageByCodeQuery.isLoading ? (
          <div className='my-5'>
            <Skeleton className=' h-[500px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
          </div>
        ) : (
          <div
            className='my-5 leading-8'
            dangerouslySetInnerHTML={{
              __html: staticPageByCodeQuery.data?.data.pageContent || "",
            }}
          ></div>
        )}
      </div>
    </div>
  );
}
