export const description = `
Chào mừng bạn đến với trang "Về chúng tôi" của English Academy! Chúng tôi rất vui được chia sẻ với bạn về giá trị, sứ mệnh và cam kết của chúng tôi trong lĩnh vực luyện thi tiếng Anh.

English Academy được thành lập với mục tiêu cung cấp một môi trường học tập chất lượng cao, đáng tin cậy và tiện lợi cho người học tiếng Anh. Chúng tôi tin rằng tiếng Anh là một công cụ quan trọng để mở ra cơ hội mới, nâng cao sự tự tin và đạt được thành công trong các kỳ thi quan trọng. Vì vậy, chúng tôi cam kết mang đến cho bạn những khóa học và dịch vụ tốt nhất để giúp bạn đạt được mục tiêu của mình.

Sứ mệnh của chúng tôi là cung cấp một nền tảng giáo dục trực tuyến đáng tin cậy và chất lượng, giúp người học tiếng Anh phát triển kỹ năng ngôn ngữ, rèn luyện khả năng thi và đạt kết quả cao trong các kỳ thi tiếng Anh quốc tế. Chúng tôi tin rằng một giáo dục tốt có thể thay đổi cuộc sống và mở ra nhiều cơ hội mới cho mỗi người học.

Chúng tôi xây dựng các khóa học tiếng Anh dựa trên các phương pháp học tập hiệu quả, tài liệu chất lượng và đội ngũ giảng viên giàu kinh nghiệm. Chúng tôi đảm bảo rằng mỗi khóa học của chúng tôi được thiết kế một cách kỹ lưỡng, linh hoạt và phù hợp với nhu cầu và mục tiêu riêng của từng người học. Chúng tôi tận dụng công nghệ hiện đại để mang đến trải nghiệm học tập tương tác, tiện lợi và truy cập được từ mọi nơi.

Chất lượng và sự hài lòng của người học luôn là ưu tiên hàng đầu của chúng tôi. Chúng tôi cam kết mang lại cho bạn một môi trường học tập đáng tin cậy, hỗ trợ tận tâm từ đội ngũ giảng viên và nhân viên của chúng tôi. Chúng tôi không ngừng cải thiện và phát triển dịch vụ của mình để đáp ứng tốt nhất nhu cầu và mong muốn của bạn.

Hãy trải nghiệm English Academy và khám phá những khóa học chất lượng cao và trang thiết bị học tập tiên tiến của chúng tôi. Chúng tôi sẽ đồng hành cùng bạn trên con đường học tập và đạt được thành công trong việc luyện thi tiếng Anh. Hãy bắt đầu hôm nay và cho phép chúng tôi giúp bạn phát triển kỹ năng và đạt được mục tiêu của mình trong lĩnh vực tiếng Anh.
`;
