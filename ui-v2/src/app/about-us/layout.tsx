import { ReactElement, ReactNode } from "react";
import { AppLayout } from "@/components/shared/layout";
import { Metadata } from "next";
import { description } from "./metadata";

export const metadata: Metadata = {
  title: "Về chúng tôi - English Academy",
  description: description,
};

export default function Layout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return <AppLayout>{children}</AppLayout>;
}
