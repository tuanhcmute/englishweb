import { ReactElement, ReactNode, Suspense } from "react";
import { AppLayout } from "@/components/shared/layout";
import { Metadata } from "next";
import { description } from "./metadata";
import { Fallback } from "@/components/shared/fallback";

export const metadata: Metadata = {
  title: "Đề thi online - English Academy",
  description: description,
};

export default function Layout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return (
    <AppLayout>
      <Suspense fallback={<Fallback />}>{children}</Suspense>
    </AppLayout>
  );
}
