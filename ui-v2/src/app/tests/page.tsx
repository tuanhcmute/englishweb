"use client";

import { PaginatedItems } from "@/components/shared/paginate";
import {
  Card,
  CardContent,
  Skeleton,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useTestCategories, useTests } from "@/hooks";
import { cn } from "@/lib";
import { ITestResponse } from "@/types";
import { HomeIcon, QuestionMarkCircledIcon } from "@radix-ui/react-icons";
import { IconMessage } from "@tabler/icons-react";
import { SigmaSquareIcon, UserIcon } from "lucide-react";
import Link from "next/link";
import { useRouter, useSearchParams } from "next/navigation";
import { ReactElement, useCallback, useState } from "react";

export default function TestsPage(): ReactElement {
  const { data, isLoading, error } = useTests();
  const testCategoriesApi = useTestCategories();
  const [currentTestCategory, setCurrentTestCategory] = useState<string>("");
  const [currentItems, setCurrentItems] = useState<ITestResponse[]>([]);
  const searchParams = useSearchParams();
  const router = useRouter();
  const updateCurrentItems = useCallback((newItems: ITestResponse[]) => {
    setCurrentItems(newItems);
  }, []);

  if (error)
    return (
      <div className="text-center w-full font-bold text-red-500">
        {error.message}
      </div>
    );
  if (testCategoriesApi.error)
    return (
      <div className="text-center w-full font-bold text-red-500">
        {testCategoriesApi.error.message}
      </div>
    );

  return (
    <div className="bg-gray-100 dark:bg-slate-950">
      <div className="container">
        {/* Path */}
        {/* Path */}
        <div className="pt-4 pl-2">
          <Breadcrumb>
            <BreadcrumbList>
              <BreadcrumbItem>
                <BreadcrumbLink>
                  <Link href={ROUTES.HOME} className="flex items-center gap-1">
                    <HomeIcon className="w-4" />
                    Trang chủ
                  </Link>
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbSeparator className="flex items-center" />
              <BreadcrumbItem>
                <BreadcrumbPage className="flex items-center gap-1">
                  Thư viện đề thi
                </BreadcrumbPage>
              </BreadcrumbItem>
            </BreadcrumbList>
          </Breadcrumb>
        </div>
        {/* Test category */}
        <section id="test-category" className="mt-4">
          {isLoading ? (
            <Skeleton className="h-[30px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
          ) : (
            <div className="">
              <h3 className="text-2xl font-semibold">Thư viện đề thi</h3>
              <div className="flex items-center flex-wrap gap-3 mt-5">
                <div
                  className={cn(
                    "border border-gray-400 rounded-full py-1 px-3 text-sm cursor-pointer hover:bg-blue-100 hover:border-purple-800 hover:text-purple-800",
                    currentTestCategory === ""
                      ? "bg-blue-100 border-purple-800 text-purple-800"
                      : ""
                  )}
                  onClick={() => {
                    setCurrentTestCategory("");
                    router.push(ROUTES.TESTS);
                  }}
                >
                  Tất cả
                </div>
                {testCategoriesApi.isSuccess &&
                  testCategoriesApi.data.data.map(
                    (item, index: number): ReactElement => {
                      return (
                        <Link
                          href={`/tests?c=${item.id}&s=${
                            searchParams.get("s") || ""
                          }`}
                          key={index}
                          className={cn(
                            "border border-gray-400 rounded-full py-1 px-3 text-sm cursor-pointer hover:bg-blue-100 hover:border-purple-800 hover:text-purple-800",
                            currentTestCategory === item.id
                              ? "bg-blue-100 border-purple-800 text-purple-800"
                              : ""
                          )}
                          onClick={() => setCurrentTestCategory(item.id)}
                        >
                          {item.testCategoryName}
                        </Link>
                      );
                    }
                  )}
              </div>
            </div>
          )}
        </section>
        {/* Card list*/}
        <section id="card-list">
          {isLoading ? (
            <Skeleton className="h-[400px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 my-5" />
          ) : (
            <div className="flex-1 mt-5">
              {data?.data?.length && data.data?.length > 0 ? (
                <div id="pagination" className="py-10">
                  <PaginatedItems<ITestResponse>
                    itemsPerPage={16}
                    items={data.data}
                    setCurrentItems={updateCurrentItems}
                  >
                    <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
                      {currentItems
                        .filter((item) => {
                          if (searchParams.get("c")) {
                            return (
                              item.testCategory.id === searchParams.get("c")
                            );
                          }
                          return true;
                        })
                        .map((test: ITestResponse) => {
                          return (
                            <Card className="" key={test.testName}>
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: test.thumbnail,
                                }}
                                className="w-full h-36 rounded-md"
                              ></div>
                              <CardContent>
                                <Link
                                  href={`tests/${test.id}`}
                                  className="text-lg hover:underline cursor-pointer hover:text-blue-400 font-bold mt-1 block"
                                >
                                  {test.testName}
                                </Link>
                                <p className="text-sm mt-2">
                                  {test.testDescription}
                                </p>
                                <p className="flex items-center gap-1 text-sm mt-2">
                                  <SigmaSquareIcon className="w-4" />
                                  <span className="">
                                    Số lượng phần thi:
                                  </span>{" "}
                                  {test.numberOfPart} phần thi
                                </p>
                                <p className="flex items-center gap-1 text-sm mt-2">
                                  <QuestionMarkCircledIcon className="" />
                                  <span className="">
                                    Số lượng câu hỏi:
                                  </span>{" "}
                                  {test.numberOfQuestion} câu hỏi
                                </p>
                                <p className="flex items-center gap-1 text-sm mt-2">
                                  <IconMessage className="w-4 h-4" />
                                  <span className="">Bình luận:</span>{" "}
                                  {test.countTestComments} bình luận
                                </p>
                                <p className="flex items-center gap-1 text-sm mt-2">
                                  <UserIcon className="w-4 h-4" />
                                  <span className="">Đã thực hiện:</span>{" "}
                                  {test.countUserAnswers} lượt thực hiện
                                </p>
                              </CardContent>
                            </Card>
                          );
                        })}
                    </div>
                  </PaginatedItems>
                </div>
              ) : (
                <div className="flex items-center justify-center h-full">
                  <span className="font-medium">Không có bài thi nào</span>
                </div>
              )}
            </div>
          )}
        </section>
      </div>
    </div>
  );
}
