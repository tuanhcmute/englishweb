import { httpServer } from "@/lib";
import { IBaseResponse, ITestResponse } from "@/types";
import type { Metadata, ResolvingMetadata } from "next";
import { ReactNode } from "react";
import { cookies } from "next/headers";
import { ACCESS_TOKEN_KEY, REFRESH_TOKEN_KEY } from "@/constants";

export async function generateMetadata(
  { params }: { params: { testId: string } },
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const id = params.testId;
  const cookieStore = cookies();
  const accessToken = cookieStore.get(ACCESS_TOKEN_KEY)?.value;
  const refreshToken = cookieStore.get(REFRESH_TOKEN_KEY)?.value;
  // fetch data
  const res: IBaseResponse<{ data: ITestResponse }> = await httpServer(
    {
      accessToken: accessToken as string,
      refreshToken: refreshToken as string,
    },
    `/test/${id}`
  );

  return {
    title: `${res.data.data.testName} - English Academy`,
    description: `${res.data.data.testName} - English Academy`,
  };
}

export default function Layout({ children }: { children: ReactNode }) {
  return <>{children}</>;
}
