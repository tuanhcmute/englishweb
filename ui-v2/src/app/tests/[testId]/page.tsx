"use client";

import {
  Alert,
  AlertDescription,
  AlertTitle,
  Button,
  Checkbox,
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  Popover,
  PopoverContent,
  PopoverTrigger,
  Skeleton,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui";
import {
  ClockIcon,
  HomeIcon,
  LightningBoltIcon,
  PaddingIcon,
  QuestionMarkCircledIcon,
} from "@radix-ui/react-icons";
import * as Tabs from "@radix-ui/react-tabs";
import { Check, ChevronsUpDown, MessageCircleWarningIcon } from "lucide-react";
import React, { ReactElement, useState } from "react";
import { cn } from "@/lib/utils";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { Confirm } from "@/components/app/modal";
import { usePartOfTest, useTest } from "@/hooks";
import { ROUTES } from "@/constants";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { limitedTimes } from "./limited-times";
import { toast } from "react-hot-toast";
import { Comments } from "./comments";

const CheckedPartsSchema = z.object({
  checkedParts: z.string().array(),
});

export default function TestDetailPage({
  params,
}: {
  params: { testId: string };
}): ReactElement {
  const [open, setOpen] = React.useState<boolean>(false);
  const [limitTime, setLimitTime] = React.useState<string>("");
  const [currentTab, setCurrentTab] = useState<string>("nav-practice");
  const router = useRouter();
  const testApi = useTest(params.testId);
  const partOfTestQuery = usePartOfTest(params.testId);
  const form = useForm<z.infer<typeof CheckedPartsSchema>>({
    resolver: zodResolver(CheckedPartsSchema),
    defaultValues: {
      checkedParts: [],
    },
  });

  async function onsubmit(values: z.infer<typeof CheckedPartsSchema>) {
    console.log({ values });
  }
  function handleCheckboxChange(checkedValue: string) {
    const currentValue = form.getValues("checkedParts") || [];
    if (currentValue.includes(checkedValue)) {
      // If the checkbox is currently checked, remove it from the array
      form.setValue(
        "checkedParts",
        currentValue.filter((value) => value !== checkedValue)
      );
    } else {
      // If the checkbox is not checked, add it to the array
      form.setValue("checkedParts", [...currentValue, checkedValue]);
    }
  }

  if (testApi.error)
    return (
      <div className='text-center w-full font-bold text-red-500'>
        {testApi.error.message}
      </div>
    );

  if (partOfTestQuery.error)
    return (
      <div className='text-center w-full font-bold text-red-500'>
        {partOfTestQuery.error.message}
      </div>
    );

  return (
    <section id='test-detail' className='py-10 bg-gray-50 dark:bg-slate-950'>
      <div className='container'>
        {/* Path */}
        <div className=''>
          <Breadcrumb>
            <BreadcrumbList>
              <BreadcrumbItem>
                <BreadcrumbLink>
                  <Link href={ROUTES.HOME} className='flex items-center gap-1'>
                    <HomeIcon className='w-4' />
                    Trang chủ
                  </Link>
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbSeparator className='flex items-center' />
              <BreadcrumbItem>
                <BreadcrumbLink>
                  <Link href={ROUTES.TESTS} className='flex items-center gap-1'>
                    Thư viện đề thi
                  </Link>
                </BreadcrumbLink>
                <BreadcrumbPage className='flex items-center gap-1'></BreadcrumbPage>
              </BreadcrumbItem>
              <BreadcrumbSeparator className='flex items-center' />
              <BreadcrumbItem>
                <BreadcrumbPage className='flex items-center gap-1'>
                  {testApi.isLoading ? (
                    <Skeleton className='h-[30px] w-96 rounded-xl bg-gray-200 dark:bg-slate-900' />
                  ) : (
                    testApi.data?.testName
                  )}
                </BreadcrumbPage>
              </BreadcrumbItem>
            </BreadcrumbList>
          </Breadcrumb>
        </div>

        <p className='text-3xl font-bold dark:text-gray-200 mt-4'>
          {testApi.isLoading ? (
            <Skeleton className='h-[30px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
          ) : (
            testApi.data?.testName
          )}
        </p>
        <div className='mt-5 flex flex-col gap-2 text-sm dark:text-gray-200'>
          <div className='flex gap-1 items-center'>
            <ClockIcon className='h-5 w-5' />
            <p>
              <b>Thời gian làm bài thi:</b> 120 phút.
            </p>
          </div>
          <div className='flex gap-1 items-center'>
            <PaddingIcon className='h-5 w-5' />
            <p>
              <b>Số phần thi:</b> {testApi.data?.numberOfPart} phần.
            </p>
          </div>
          <div className='flex gap-1 items-center'>
            <QuestionMarkCircledIcon className='h-5 w-5' />
            <p>
              <b>Số lượng câu hỏi:</b> {testApi.data?.numberOfQuestion} câu hỏi.
            </p>
          </div>
        </div>
        {/* Test detail */}
        <Tabs.Root
          defaultValue='nav-practice'
          className='mt-5 dark:text-gray-200'
        >
          <Tabs.List className='flex items-center gap-5'>
            <Tabs.Trigger
              value='nav-practice'
              className={
                currentTab === "nav-practice" ? "font-bold text-purple-800" : ""
              }
              onClick={() => setCurrentTab("nav-practice")}
            >
              Luyện tập
            </Tabs.Trigger>
            <Tabs.Trigger
              value='nav-fulltest'
              className={
                currentTab === "nav-fulltest" ? "font-bold text-purple-800" : ""
              }
              onClick={() => setCurrentTab("nav-fulltest")}
            >
              Làm full test
            </Tabs.Trigger>
            <Link href={`#test-comments`} scroll>
              Bình luận
            </Link>
          </Tabs.List>

          <div className='mt-5'>
            <Tabs.Content value='nav-practice'>
              {/* Alert */}
              <div className='w-fit'>
                <Alert
                  variant='default'
                  className='border-green-500 bg-green-100 dark:bg-transparent dark:text-gray-300'
                >
                  <LightningBoltIcon className='h-4 w-4' />
                  <AlertTitle>Pro tips:</AlertTitle>
                  <AlertDescription>
                    Hình thức luyện tập từng phần và chọn mức thời gian phù hợp
                    sẽ giúp bạn tập trung vào giải đúng các câu hỏi thay vì phải
                    chịu áp lực hoàn thành bài thi.
                  </AlertDescription>
                </Alert>
              </div>
              {/* <p className='font-semibold mt-5'>Chọn phần thi bạn muốn làm</p> */}
              <Form {...form}>
                <form onSubmit={form.handleSubmit(onsubmit)}>
                  <FormField
                    name='checkedParts'
                    control={form.control}
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel className='font-semibold mt-5 block'>
                          Chọn phần thi bạn muốn làm
                        </FormLabel>
                        <FormControl>
                          {partOfTestQuery.isLoading ? (
                            <Skeleton className='h-[600px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 mt-2' />
                          ) : (
                            <div className='flex flex-col gap-4 mt-2'>
                              {partOfTestQuery.isSuccess &&
                                partOfTestQuery.data.data.map((item) => (
                                  <div key={item.id}>
                                    <div className='flex items-center gap-2'>
                                      <Checkbox
                                        id={item.id}
                                        value={item.id}
                                        checked={field.value.includes(item.id)}
                                        onCheckedChange={() =>
                                          handleCheckboxChange(item.id)
                                        }
                                      />
                                      <p className='font-medium text-[14px] dark:text-gray-200'>
                                        {
                                          item.partInformation
                                            .partInformationName
                                        }{" "}
                                        ({item.partInformation.totalQuestion}{" "}
                                        câu hỏi)
                                      </p>
                                    </div>
                                    <div className='mt-2 flex items-center flex-wrap gap-2 text-sm'>
                                      #
                                      {
                                        item.partInformation
                                          .partInformationDescription
                                      }
                                    </div>
                                  </div>
                                ))}
                            </div>
                          )}
                        </FormControl>
                      </FormItem>
                    )}
                  />
                </form>
              </Form>

              <p className='mt-5 font-semibold dark:text-gray-300'>
                Giới hạn thời gian
              </p>
              <div className='mt-2'>
                <Popover open={open} onOpenChange={setOpen}>
                  <PopoverTrigger asChild>
                    <Button
                      variant='outline'
                      role='combobox'
                      aria-expanded={open}
                      className='w-[200px] justify-between'
                    >
                      {limitTime
                        ? limitedTimes.find((item) => item.value === limitTime)
                            ?.label
                        : "Lựa chọn thời gian..."}
                      <ChevronsUpDown className='ml-2 h-4 w-4 shrink-0 opacity-50' />
                    </Button>
                  </PopoverTrigger>
                  <PopoverContent className='w-[200px] p-0'>
                    <Command>
                      <CommandInput placeholder='Tìm thời gian...' />
                      <CommandEmpty>Không tìm thấy thời gian.</CommandEmpty>
                      <CommandGroup>
                        {limitedTimes.map((item) => (
                          <CommandItem
                            key={item.value}
                            value={item.value}
                            onSelect={(currentValue) => {
                              setLimitTime(
                                currentValue === limitTime ? "" : currentValue
                              );
                              setOpen(false);
                            }}
                          >
                            <Check
                              className={cn(
                                "mr-2 h-4 w-4",
                                limitTime === item.value
                                  ? "opacity-100"
                                  : "opacity-0"
                              )}
                            />
                            {item.label}
                          </CommandItem>
                        ))}
                      </CommandGroup>
                    </Command>
                  </PopoverContent>
                </Popover>
              </div>
              <div className='mt-5'>
                <Confirm
                  title='Bắt đầu luyện tập'
                  description='Tập trung vào giải đúng các câu hỏi thay vì phải
                    chịu áp lực hoàn thành bài thi.'
                  textConfirm='Bắt đầu'
                  onConfirm={() => {
                    const checkedParts = form.getValues("checkedParts");
                    if (checkedParts.length === 0) {
                      toast.error("Vui lòng lựa chọn phần thi");
                      return;
                    }
                    if (limitTime === "") {
                      toast.error("Vui lòng lựa chọn thời gian làm bài");
                      return;
                    }
                    // OK
                    router.push(
                      `/practice/${params.testId}?parts=${checkedParts.join(
                        ","
                      )}&timeLimit=${limitTime}`
                    );
                  }}
                >
                  <Button
                    variant='destructive'
                    className='bg-transparent border-purple-800 text-purple-800 dark:text-white hover:bg-purple-800 hover:text-white border'
                  >
                    Luyện tập
                  </Button>
                </Confirm>
              </div>
            </Tabs.Content>
            <Tabs.Content value='nav-fulltest'>
              {/* Alert */}
              <div className='w-fit'>
                <Alert
                  variant='default'
                  className='border-yellow-400 bg-yellow-100 dark:bg-transparent'
                >
                  <MessageCircleWarningIcon className='h-4 w-4' />
                  <AlertTitle>Pro tips:</AlertTitle>
                  <AlertDescription>
                    Sẵn sàng để bắt đầu làm full test? Để đạt được kết quả tốt
                    nhất, bạn cần dành ra 120 phút cho bài test này.
                  </AlertDescription>
                </Alert>
              </div>
              <div className='mt-5'>
                <Confirm
                  title='Bắt đầu làm bài'
                  description='Bạn cần dành 120 phút để hoàn thành bài thi này. Hãy tập trung làm bài để có kết quả tốt nhất.'
                  textConfirm='Bắt đầu'
                  onConfirm={() => {
                    router.push(`/practice/${params.testId}`);
                  }}
                >
                  <Button
                    variant='destructive'
                    className='bg-transparent border-purple-800 text-purple-800 dark:text-white hover:bg-purple-800 hover:text-white border'
                  >
                    Bắt đầu thi
                  </Button>
                </Confirm>
              </div>
            </Tabs.Content>
          </div>
        </Tabs.Root>
        <div className='w-full h-[1px] bg-gray-400 mt-5'></div>
        {/* Test comments */}
        <Comments testId={params.testId} />
      </div>
    </section>
  );
}
