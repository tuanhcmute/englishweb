"use client";

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
  Alert,
  AlertDescription,
  AlertTitle,
  Avatar,
  AvatarFallback,
  AvatarImage,
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
  Input,
} from "@/components/ui";
import React, { useState } from "react";
import { useComments, useCreateComment } from "@/hooks";
import { USER_CREDENTIAL_ID } from "@/constants";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  IconFlag,
  IconLoader2,
  IconReport,
  IconSend,
} from "@tabler/icons-react";
import { formCommentSchema } from "./formSchema";
import { getCookie } from "cookies-next";
import { SubComment } from "./sub-comment";
import { ICreateCommentRequest } from "@/types";
import { MessageSquareWarningIcon } from "lucide-react";
import { CreateReportCommentModal } from "@/components/app/modal";

export const Comments: React.FC<{ testId: string }> = ({ testId }) => {
  const commentsByTestQuery = useComments({ testId, isRoot: "1" });
  const { mutateAsync: handleCreateComment, isPending } = useCreateComment();
  const userCredentialId = getCookie(USER_CREDENTIAL_ID);
  const [showComments, setShowComments] = useState<boolean>(false);

  // 1. Define your form.
  const form = useForm<z.infer<typeof formCommentSchema>>({
    resolver: zodResolver(formCommentSchema),
    defaultValues: {
      authorId: userCredentialId,
      content: "",
      parentCommentId: "",
      testId,
    },
  });

  const formSubComment = useForm<z.infer<typeof formCommentSchema>>({
    resolver: zodResolver(formCommentSchema),
    defaultValues: {
      authorId: userCredentialId,
      content: "",
      parentCommentId: "",
      testId,
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formCommentSchema>) {
    console.log({ values });
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    const requestData = { ...values } as ICreateCommentRequest;
    await handleCreateComment(requestData);
  }

  if (commentsByTestQuery.error)
    return (
      <div className="text-center w-full font-bold text-red-500">
        {commentsByTestQuery.error.message}
      </div>
    );

  return (
    <div className="bg-white p-4 dark:bg-transparent">
      <div id="test-comments" className="text-2xl mt-5 font-bold">
        #Bình luận
      </div>
      <div>
        {/* Input */}
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(onSubmit)}
            className="flex flex-col gap-2"
          >
            <FormField
              control={form.control}
              name="content"
              render={({ field }) => (
                <div className="flex items-start gap-1 w-4/5">
                  <FormItem className="w-full">
                    <FormControl>
                      <Input
                        placeholder="Chia sẻ bình luận của bạn..."
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                  <Button
                    type="submit"
                    variant="destructive"
                    className="w-fit border-purple-800 border text-white bg-purple-700 hover:bg-purple-800 basis-1/12"
                    disabled={isPending}
                  >
                    {isPending ? (
                      <IconLoader2 className="animate-spin" />
                    ) : (
                      <IconSend className="dark:text-white" />
                    )}
                    Gửi
                  </Button>
                </div>
              )}
            />
          </form>
        </Form>
        {/* Comments */}
        <div className="flex flex-col gap-2 mt-4">
          {commentsByTestQuery.data?.data.map((item) => {
            return item.isNegative ? (
              <Alert className="w-full flex items-start gap-2 bg-transparent p-0 border-none">
                <Avatar>
                  <AvatarImage
                    src={item.author.avatar}
                    alt="@shadcn"
                    className=""
                  />
                  <AvatarFallback className="border border-gray-300">
                    CN
                  </AvatarFallback>
                </Avatar>
                <div className="w-full">
                  <AlertTitle className="flex items-end gap-1 text-purple-800 dark:text-white">
                    <p className="font-bold">{item.author.fullName},</p>
                  </AlertTitle>
                  <AlertDescription className="italic text-red-500 flex items-center gap-1">
                    <MessageSquareWarningIcon className="w-4 h-4" />
                    Bình luận này đã bị ẩn do sử dụng ngôn từ không phù hợp.
                  </AlertDescription>
                </div>
              </Alert>
            ) : (
              <div key={item.id} className="text-sm">
                <Alert className="w-full flex items-start gap-2 bg-transparent p-0 border-none">
                  <Avatar>
                    <AvatarImage
                      src={item.author.avatar}
                      alt="@shadcn"
                      className=""
                    />
                    <AvatarFallback className="border border-gray-300">
                      CN
                    </AvatarFallback>
                  </Avatar>
                  <div className="w-full">
                    <AlertTitle className="flex items-end gap-1 text-purple-800 dark:text-white">
                      <p className="font-bold">{item.author.fullName}</p>
                      <p className="italic text-[12px]">{item.createdDate}</p>
                    </AlertTitle>
                    <AlertDescription>{item.content}</AlertDescription>
                    <div className="flex flex-col">
                      <div className="w-full">
                        <Accordion
                          type="single"
                          collapsible
                          className="w-full border-none mt-1"
                        >
                          <AccordionItem value="item-1" className="border-none">
                            <AccordionTrigger className="text-[12px] p-0 hover:text-purple-800 hover:cursor-pointer hover:underline [&>svg]:hidden">
                              Trả lời
                            </AccordionTrigger>
                            <AccordionContent>
                              <Form {...formSubComment}>
                                <form
                                  onSubmit={formSubComment.handleSubmit(
                                    onSubmit
                                  )}
                                  className="flex flex-col gap-2"
                                >
                                  <FormField
                                    control={formSubComment.control}
                                    name="content"
                                    render={({ field }) => (
                                      <div className="flex items-start gap-1 w-4/5">
                                        <FormItem className="w-full">
                                          <FormControl>
                                            <Input
                                              placeholder="Chia sẻ bình luận của bạn..."
                                              // {...field}
                                              onChange={(e) => {
                                                formSubComment.setValue(
                                                  "content",
                                                  e.target.value
                                                );
                                                // formSubComment.setValue(
                                                //   "parentCommentId",
                                                //   item.id
                                                // );
                                              }}
                                            />
                                          </FormControl>
                                          <FormMessage />
                                        </FormItem>
                                        <Button
                                          type="submit"
                                          variant="destructive"
                                          className="w-fit border-purple-800 border text-white bg-purple-700 hover:bg-purple-800 basis-1/12"
                                          disabled={isPending}
                                          onClick={() => {
                                            console.log("hello");
                                            formSubComment.setValue(
                                              "parentCommentId",
                                              item.id
                                            );
                                          }}
                                        >
                                          {isPending ? (
                                            <IconLoader2 className="animate-spin" />
                                          ) : (
                                            <IconSend className="dark:text-white" />
                                          )}
                                          Gửi
                                        </Button>
                                      </div>
                                    )}
                                  />
                                </form>
                              </Form>
                            </AccordionContent>
                          </AccordionItem>
                          <AccordionItem value="item-2" className="border-none">
                            <AccordionTrigger className="text-[12px] p-0 hover:text-purple-800 hover:cursor-pointer hover:underline [&>svg]:hidden">
                              <div className="flex items-center gap-1">
                                <CreateReportCommentModal commentId={item.id} />
                              </div>
                            </AccordionTrigger>
                          </AccordionItem>
                        </Accordion>
                        {item.hasSubComment && (
                          <Accordion
                            type="single"
                            collapsible
                            className="w-full border-none mt-1"
                          >
                            <AccordionItem
                              value="item-1"
                              className="border-none"
                            >
                              <AccordionTrigger
                                className="text-[12px] p-0 hover:text-purple-800 hover:cursor-pointer hover:underline [&>svg]:hidden"
                                onClick={() => setShowComments(!showComments)}
                              >
                                {showComments ? "Ẩn" : "Hiển thị"}{" "}
                                {item.countSubComment} bình luận
                              </AccordionTrigger>
                              <AccordionContent>
                                <SubComment comment={item} />
                              </AccordionContent>
                            </AccordionItem>
                          </Accordion>
                        )}
                      </div>
                    </div>
                  </div>
                </Alert>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
