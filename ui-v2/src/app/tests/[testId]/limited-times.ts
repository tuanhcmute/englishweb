export const limitedTimes = [
  {
    value: "15",
    label: "15 phút",
  },
  {
    value: "20",
    label: "20 phút",
  },
  {
    value: "25",
    label: "25 phút",
  },
  {
    value: "30",
    label: "30 phút",
  },
  {
    value: "35",
    label: "35 phút",
  },
  {
    value: "40",
    label: "40 phút",
  },
  {
    value: "45",
    label: "45 phút",
  },
  {
    value: "50",
    label: "50 phút",
  },
  {
    value: "55",
    label: "55 phút",
  },
  {
    value: "60",
    label: "60 phút",
  },
];
