import { z } from "zod";

export const formCommentSchema = z.object({
  content: z.string().trim().min(1, {
    message: "Bình luận không được bỏ trống",
  }),
  testId: z.string().trim().min(1, {
    message: "Mã bài thi là bắt buộc",
  }),
  authorId: z.string().trim().min(1, {
    message: "Mã người dùng là bắt buộc",
  }),
  parentCommentId: z.string().trim(),
});
