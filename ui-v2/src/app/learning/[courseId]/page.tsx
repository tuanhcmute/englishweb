"use client";

import { CreateNoteDrawer } from "@/components/app/drawer";
import { QASheet } from "@/components/app/sheet";
import { Button } from "@/components/ui";
import { useLesson, useUnitsByCourse } from "@/hooks";
import { cn } from "@/lib";
import { useLearning } from "@/providers";
import { ChevronLeftIcon, ChevronRightIcon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";
import { useRouter, useSearchParams } from "next/navigation";
import { ReactElement, useRef, useState } from "react";
import ReactPlayer from "react-player";
import YouTubePlayer from "./youtube-player";

export default function LearningPage({
  params,
}: {
  params: { courseId: string };
}): ReactElement {
  const { playerRef } = useLearning();
  const searchParams = useSearchParams();
  const lessonId = searchParams.get("lessonId");
  const lessonQuery = useLesson(lessonId || "");
  const unitsQuery = useUnitsByCourse(params.courseId);
  const [currentTime, setCurrentTime] = useState<number>(0);
  const [isPlaying, setPlaying] = useState<boolean>(true);
  const [duration, setDuration] = useState<number>(0);
  const router = useRouter();

  const handleProgress = (progress: { played: number }) => {
    setCurrentTime(progress.played * duration);
  };

  const handleDuration = (duration: number) => {
    setDuration(duration);
    // playerRef.current?.seekTo(60);
  };

  const handlePreviousLesson = () => {
    if (unitsQuery.data?.data) {
      // unitsQuery.data.data.find(item => item.lessons.find(item => item.id === lessonId));
      const lessonIds = unitsQuery.data.data
        .map((item) => {
          return item.lessons.map((item) => item.id);
        })
        .flat(Infinity);

      const index = lessonIds.findIndex((item) => item === lessonId);
      if (index > 0) {
        router.push(`${params.courseId}?lessonId=${lessonIds[index - 1]}`);
      }
    }
  };

  const handleNextLesson = () => {
    if (unitsQuery.data?.data) {
      // unitsQuery.data.data.find(item => item.lessons.find(item => item.id === lessonId));
      const lessonIds = unitsQuery.data.data
        .map((item) => {
          return item.lessons.map((item) => item.id);
        })
        .flat(Infinity);

      const index = lessonIds.findIndex((item) => item === lessonId);
      if (index < lessonIds.length - 1) {
        router.push(`${params.courseId}?lessonId=${lessonIds[index + 1]}`);
      }
      // console.log({ lessonIds: lessonIds.flat(Infinity) });
    }
  };

  return (
    <div className={cn("")}>
      {/* Video section */}
      <div>
        <div className="flex items-center bg-black justify-center">
          {!lessonId && (
            <div className="w-full h-[600px] lg:w-2/3 flex items-center flex-col justify-center">
              <div className="text-white dark:text-gray-400">
                Vui lòng chọn bài học
              </div>
            </div>
          )}
          {lessonQuery.data?.video && (
            <YouTubePlayer
              onPlay={setPlaying}
              playing={isPlaying}
              videoId={lessonQuery.data?.video}
              onProgress={handleProgress}
              onDuration={handleDuration}
              ref={playerRef}
            />
          )}
        </div>
        <div className="pl-4">
          <div className="flex justify-center gap-2 mt-4">
            <Button
              className="bg-transparent border border-purple-800 hover:bg-purple-800 hover:text-white text-purple-800 min-w-32 dark:text-white"
              onClick={handlePreviousLesson}
            >
              <ChevronLeftIcon />
              Bài trước
            </Button>
            <Button
              className="min-w-32 bg-transparent hover:bg-purple-800 hover:text-white dark:text-white text-purple-800 border border-purple-800"
              onClick={handleNextLesson}
            >
              Bài tiếp theo
              <ChevronRightIcon />
            </Button>
          </div>
          <div className="mt-2 flex items-start justify-between flex-wrap gap-2">
            <div className="">
              <p className="text-xl font-semibold">
                {lessonQuery.data?.title && lessonQuery.data.title}
              </p>
              <p className="text-[12px] italic">
                Đăng ngày:{" "}
                {lessonQuery.data?.createdDate && lessonQuery.data.createdDate}
              </p>
            </div>

            {lessonId && (
              <CreateNoteDrawer
                currentTime={currentTime}
                playerRef={playerRef}
                setPlaying={setPlaying}
                lessonId={lessonId}
              />
            )}
          </div>
          <div className="mt-2 mb-10">
            <p className="text-lg font-semibold">Nội dung bài học</p>
            <p
              className="text-sm leading-6 text-justify pr-4"
              dangerouslySetInnerHTML={{
                __html: lessonQuery.data?.content || "",
              }}
            ></p>
          </div>
        </div>
      </div>
    </div>
  );
}
