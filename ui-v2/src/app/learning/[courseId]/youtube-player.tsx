// // components/YouTubePlayer.js

import React from "react";
import ReactPlayer from "react-player";

interface IYouTubePlayerProps {
  videoId?: string;
  playing?: boolean | true;
  onProgress?: (progress: { played: number }) => void;
  onDuration?: (duration: number) => void;
  onPlay?: (isPlaying: boolean) => void;
}

export const YouTubePlayer: React.ForwardRefRenderFunction<
  ReactPlayer,
  IYouTubePlayerProps
> = ({ videoId, playing, onPlay, onDuration, onProgress }, ref) => {
  return (
    <div className='w-full h-[600px] lg:w-2/3 py-2'>
      {/* <h1>YouTube Video Player</h1> */}
      <ReactPlayer
        url={`https://www.youtube.com/watch?v=${videoId}`}
        playing={playing}
        controls
        ref={ref}
        width='100%'
        height='100%'
        onProgress={onProgress}
        onDuration={onDuration}
        onPlay={() => {
          if (!playing) {
            onPlay?.(true);
          }
        }}
      />
    </div>
  );
};

export default React.forwardRef(YouTubePlayer);
