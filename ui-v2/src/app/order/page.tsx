"use client";
import { cn } from "@/lib";
import { ReactElement } from "react";

export default function OrderPage(): ReactElement {
  const isLoading = true;
  return (
    <div
      className={cn(
        "bg-gray-100 dark:bg-slate-950",
        isLoading ? "h-screen" : ""
      )}
    >
      Order page
    </div>
  );
}
