"use client";

import { Loading } from "@/components/shared/loading";
import {
  Card,
  CardContent,
  CardFooter,
  Carousel,
  CarouselContent,
  CarouselItem,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useFlashcard, useFlashcardItems } from "@/hooks";
import { HomeIcon, RotateCounterClockwiseIcon } from "@radix-ui/react-icons";
import { IconVolume } from "@tabler/icons-react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { ReactElement, useState } from "react";
import ReactCardFlip from "react-card-flip";

export default function FlashcardPracticePage({
  params,
}: {
  params: {
    flashcardId: string;
  };
}): ReactElement {
  const flashcardQuery = useFlashcard(params.flashcardId);
  const flashcardItemsQuery = useFlashcardItems(params.flashcardId);
  const [isFlipped, setFlipped] = useState(false);

  if (flashcardQuery.isLoading) return <Loading />;
  if (flashcardQuery.error)
    return (
      <div className="text-center w-full font-bold text-red-500">
        {flashcardQuery.error.message}
      </div>
    );

  const handleClick = () => {
    setFlipped((prevState) => !prevState);
  };

  return (
    <div className="bg-gray-100 dark:bg-slate-950">
      <div className="container mt-16">
        {/* Path */}
        <div className="pt-4">
          <Breadcrumb>
            <BreadcrumbList>
              <BreadcrumbItem>
                <BreadcrumbLink>
                  <Link href={ROUTES.HOME} className="flex items-center gap-1">
                    <HomeIcon className="w-4" />
                    Trang chủ
                  </Link>
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbSeparator className="flex items-center" />
              <BreadcrumbItem>
                <BreadcrumbPage className="flex items-center gap-1">
                  Luyện tập {flashcardQuery.data?.data.flashcardTitle}
                </BreadcrumbPage>
              </BreadcrumbItem>
            </BreadcrumbList>
          </Breadcrumb>
        </div>
        {/* Practice flashcard*/}
        <section id="test-category" className="py-5">
          <div className="">
            <h3 className="text-2xl font-semibold">
              Luyện tập: {flashcardQuery.data?.data.flashcardTitle}
            </h3>
            <div className="flex items-center flex-wrap gap-3 mt-5">
              {flashcardItemsQuery.isSuccess && (
                <Carousel className="w-full">
                  <CarouselContent>
                    {flashcardItemsQuery.data.data.map((item) => {
                      return (
                        <CarouselItem
                          key={item.id}
                          className="flex justify-center"
                        >
                          <Card className="w-[550px]">
                            <CardContent>
                              <ReactCardFlip
                                isFlipped={isFlipped}
                                flipDirection="vertical"
                              >
                                {/* Left content */}
                                <div className="min-h-[400px] flex flex-col items-center justify-center">
                                  <div className="flex items-center gap-2">
                                    <p>{item.word}</p>
                                    <p>{item.phonetic}</p>
                                    <p
                                      className="flex items-center gap-1 cursor-pointer"
                                      onClick={() => {
                                        const audio = new Audio(item.audio);
                                        console.log({ audio });
                                        audio.play();
                                      }}
                                    >
                                      <IconVolume className="w-5" />{" "}
                                      <p className="text-sm italic text-blue-500">
                                        (Nhấn để nghe phát âm)
                                      </p>
                                    </p>
                                  </div>
                                </div>
                                <div className="min-h-[400px] flex flex-col">
                                  {/* Left content */}
                                  <div className="">
                                    <div className="flex items-center gap-2">
                                      <p>{item.word}</p>
                                      <p>{item.phonetic}</p>
                                      <p
                                        className="flex items-center gap-1 cursor-pointer"
                                        onClick={() => {
                                          const audio = new Audio(item.audio);
                                          console.log({ audio });
                                          audio.play();
                                        }}
                                      >
                                        <IconVolume className="w-5" />{" "}
                                        <p className="text-sm italic text-blue-500">
                                          (Nhấn để nghe phát âm)
                                        </p>
                                      </p>
                                    </div>
                                    <div className="">
                                      <p className="mt-2 italic w-fit">
                                        Loại từ:
                                      </p>
                                      <p className="pl-3">{item.tags}</p>
                                    </div>
                                    <div className="">
                                      <p className="mt-2 italic w-fit">
                                        Định nghĩa:
                                      </p>
                                      <p
                                        className="pl-3"
                                        dangerouslySetInnerHTML={{
                                          __html: item.meaning || "",
                                        }}
                                      ></p>
                                    </div>
                                    <div className="mt-2">
                                      <p className="italic">Ví dụ:</p>
                                      <p
                                        dangerouslySetInnerHTML={{
                                          __html: item.examples || "",
                                        }}
                                      ></p>
                                    </div>
                                  </div>
                                </div>
                              </ReactCardFlip>
                            </CardContent>
                            <CardFooter className="flex justify-end">
                              <RotateCounterClockwiseIcon
                                className="w-4 cursor-pointer hover:text-blue-400 transition-all"
                                onClick={handleClick}
                              />
                            </CardFooter>
                          </Card>
                        </CarouselItem>
                      );
                    })}
                  </CarouselContent>
                </Carousel>
              )}
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
