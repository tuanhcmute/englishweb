import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
  Label,
  RadioGroup,
  RadioGroupItem,
} from "@/components/ui";
import { cn } from "@/lib";
import { usePractice } from "@/providers";
import {
  IFullPartOfTestResponse,
  IQuestionChoice,
  IQuestionGroupResponse,
  IQuestionResponse,
} from "@/types";
import { ReactElement } from "react";

interface IProps {
  question: IQuestionResponse;
  questionGroup: IQuestionGroupResponse;
  part: IFullPartOfTestResponse;
  questionUserAns?: IQuestionChoice;
}

export function Question({
  question,
  // questionGroup,
  // part,
  questionUserAns,
}: IProps): ReactElement {
  return (
    <div key={question.id} className=' rounded-md p-2'>
      <div className='mt-3'>
        {/* Question content */}
        <p className='flex gap-2 items-start font-bold'>
          <p className='rounded-full w-8 h-8 bg-blue-100 text-purple-800 font-bold text-[14px] flex items-center flex-col justify-center'>
            {question.questionNumber}
          </p>
          <p dangerouslySetInnerHTML={{ __html: question.questionContent }}></p>
        </p>
        <div className='flex items-start gap-5'>
          {/* Answer */}
          <div className='grid grid-cols-1 gap-3'>
            <RadioGroup>
              {question.testAnswers
                .sort((a, b) => a.answerChoice.localeCompare(b.answerChoice))
                .map((ans) => {
                  return (
                    <div
                      key={ans.id}
                      className={cn(
                        "ml-10 flex gap-2 items-center text-[13px]",
                        questionUserAns?.answerId == ans.id
                          ? "bg-yellow-300 w-fit"
                          : ""
                      )}
                    >
                      <RadioGroupItem
                        value={ans.id}
                        id={ans.id}
                        checked={ans.id === questionUserAns?.answerId}
                        disabled
                      />
                      <Label
                        htmlFor={ans.id}
                        className={`flex gap-2 items-center w-fit `}
                      >
                        <p className=''>({ans.answerChoice})</p>
                        <p className=''>{ans.answerContent}</p>
                      </Label>
                      {questionUserAns?.answerId == ans.id &&
                        "=> Đáp án bạn chọn"}
                    </div>
                  );
                })}
            </RadioGroup>
          </div>
        </div>
        <div className='mt-2 text-[13px] text-red-500 font-bold'>
          {!questionUserAns && "Chưa trả lời"}
        </div>
        <div className='text-red-500 text-[13px] mt-2'>
          Đáp án đúng:{" "}
          {
            question.testAnswers.find(
              (item) => question.rightAnswer === item.id
            )?.answerChoice
          }
        </div>
        <div className='max-w-[400px]'>
          <Accordion type='single' collapsible className='w-full text-sm'>
            <AccordionItem value='item-1'>
              <AccordionTrigger>Giải thích</AccordionTrigger>
              <AccordionContent>
                <div
                  dangerouslySetInnerHTML={{ __html: question.questionGuide }}
                ></div>
              </AccordionContent>
            </AccordionItem>
          </Accordion>
        </div>
      </div>
    </div>
  );
}
