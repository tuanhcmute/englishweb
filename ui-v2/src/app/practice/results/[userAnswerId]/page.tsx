"use client";

import { Loading } from "@/components/shared/loading";
import {
  Badge,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Button,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { PracticeType } from "@/enums";
import { useFullTest, useUserAnswer } from "@/hooks";
import { millisToMinutesAndSeconds } from "@/lib";
import { IResultChoice } from "@/types";
import {
  ArchiveIcon,
  CheckCircledIcon,
  CheckIcon,
  DashIcon,
  HomeIcon,
  TimerIcon,
} from "@radix-ui/react-icons";
import { XIcon } from "lucide-react";
import moment from "moment";
import Link from "next/link";
import { ReactElement, ReactNode, useRef } from "react";
import ReactPlayer from "react-player";
import { Question } from "./question";

interface IProps {
  params: { userAnswerId: string };
}

function ReactPlayerWrapper({ children }: { children: ReactNode }) {
  return <div className="w-full h-[40px] mt-4">{children}</div>;
}

export default function PracticeResultDetail({ params }: IProps): ReactElement {
  const userAnswerQuery = useUserAnswer(params.userAnswerId);
  const fullTestQuery = useFullTest(userAnswerQuery.data?.data.test.id || "");
  const playerRef = useRef<ReactPlayer | null>(null);

  if (userAnswerQuery.isLoading) return <Loading />;
  const userAns = JSON.parse(
    userAnswerQuery.data?.data.answerJson || ""
  ) as IResultChoice[];

  const listOfParts =
    PracticeType.FULL_TEST === userAnswerQuery.data?.data.practiceType
      ? fullTestQuery.data?.listOfPart
      : fullTestQuery.data?.listOfPart?.filter((item) =>
          userAns.find((p) => p.partId == item.id)
        );

  return (
    <section id="test-result" className="py-10 bg-gray-50 dark:bg-slate-950">
      <div className="container">
        {/* Path */}
        <Breadcrumb>
          <BreadcrumbList>
            <BreadcrumbItem>
              <BreadcrumbLink>
                <Link
                  href={ROUTES.MY_ACCOUNT}
                  className="flex items-center gap-1"
                >
                  <HomeIcon className="w-4" />
                  Tài khoản của tôi
                </Link>
              </BreadcrumbLink>
            </BreadcrumbItem>
            <BreadcrumbSeparator className="flex items-center" />
            <BreadcrumbItem>
              <BreadcrumbPage className="flex items-center gap-1">
                Kết quả thi {userAnswerQuery.data?.data.test.testName}
              </BreadcrumbPage>
            </BreadcrumbItem>
          </BreadcrumbList>
        </Breadcrumb>

        <div className="text-3xl font-bold dark:text-gray-200 mt-5">
          <Badge
            variant="outline"
            className="text-gray-500 font-normal bg-yellow-100"
          >
            #{userAnswerQuery.data?.data.test.testCategory.testCategoryName}
          </Badge>
          <p className="mt-2 flex gap-2 items-center">
            Kết quả thi: {userAnswerQuery.data?.data.test.testName}{" "}
            <CheckCircledIcon className="h-6 w-6 text-green-500" />
          </p>
        </div>
        <div className="mt-5 flex flex-col gap-2 text-sm dark:text-gray-200">
          <div className="">
            <Link className="font-bold text-lg" href="#general" scroll>
              #Phân tích tổng quan
            </Link>
            <div className="flex items-start gap-5 flex-wrap">
              <div className="border border-gray-300 rounded-sm p-4 flex flex-col gap-3 bg-gray-100">
                <div className="flex items-center gap-10">
                  <p className="flex items-center gap-1">
                    <CheckIcon />
                    Kết quả làm bài:
                  </p>
                  <p>
                    {userAnswerQuery.data?.data
                      ? userAnswerQuery.data.data.listeningCorrectAnsCount +
                        userAnswerQuery.data.data.readingCorrectAnsCount
                      : 0}
                    /{userAnswerQuery.data?.data.totalSentences} (câu hỏi)
                  </p>
                </div>
                <div className="flex items-center gap-10">
                  <p className="flex items-center gap-1">
                    <ArchiveIcon />
                    Độ chính xác
                  </p>
                  <p>
                    {userAnswerQuery.data?.data
                      ? Math.round(
                          ((userAnswerQuery.data.data.listeningCorrectAnsCount +
                            userAnswerQuery.data.data.readingCorrectAnsCount) /
                            userAnswerQuery.data?.data.totalSentences) *
                            100
                        )
                      : 0}
                    % (#đúng/#tổng)
                  </p>
                </div>
                <div className="flex items-center gap-10">
                  <p className="flex items-center gap-1">
                    <TimerIcon />
                    Thời gian hoàn thành
                  </p>
                  <p>
                    {userAnswerQuery.data?.data &&
                      millisToMinutesAndSeconds(
                        userAnswerQuery.data.data.completionTime * 60000 || 0
                      )}
                  </p>
                </div>
              </div>
              <div className="border w-[150px] border-gray-100 bg-green-200 rounded-sm py-3 px-6 gap-2 flex flex-col items-center">
                <CheckCircledIcon className="w-6 h-6 text-green-600" />
                <p className="text-green-600 font-bold">
                  Trả lời đúng phần reading
                </p>
                <p>
                  <span className="font-bold text-lg">
                    {userAnswerQuery.data?.data.readingCorrectAnsCount}
                  </span>{" "}
                  câu hỏi
                </p>
              </div>
              <div className="border w-[150px] border-gray-100 bg-red-200 rounded-sm py-3 px-6 gap-2 flex flex-col items-center">
                <XIcon className="w-6 h-6 text-red-600" />
                <p className="text-red-600 font-bold">
                  Trả lời sai phần reading
                </p>
                <p>
                  <span className="font-bold text-lg">
                    {userAnswerQuery.data?.data.readingWrongAnsCount}
                  </span>{" "}
                  câu hỏi
                </p>
              </div>
              <div className="border w-[150px] border-gray-100 bg-green-200 rounded-sm py-3 px-6 gap-2 flex flex-col items-center">
                <CheckCircledIcon className="w-6 h-6 text-green-600" />
                <p className="text-green-600 font-bold">
                  Trả lời đúng phần listening
                </p>
                <p>
                  <span className="font-bold text-lg">
                    {userAnswerQuery.data?.data.listeningCorrectAnsCount}
                  </span>{" "}
                  câu hỏi
                </p>
              </div>

              <div className="border w-[150px] border-gray-100 bg-red-200 rounded-sm py-3 px-6 gap-2 flex flex-col items-center">
                <XIcon className="w-6 h-6 text-red-600" />
                <p className="text-red-600 font-bold">
                  Trả lời sai phần listening
                </p>
                <p>
                  <span className="font-bold text-lg">
                    {userAnswerQuery.data?.data.listeningWrongAnsCount}
                  </span>{" "}
                  câu hỏi
                </p>
              </div>
              <div className="border w-[150px] border-gray-100 bg-slate-200 rounded-sm py-3 px-6 gap-2 flex flex-col items-center">
                <DashIcon className="w-6 h-6 text-slate-600" />
                <p className="text-slate-600 font-bold">Chưa trả lời</p>
                <p>
                  <span className="font-bold text-lg">
                    {userAnswerQuery.data?.data.ignoreAnsCount}
                  </span>{" "}
                  câu hỏi
                </p>
              </div>
            </div>
          </div>
          <div className="mt-4">
            <Link className="font-bold text-lg" href="#detail" scroll>
              #Phân tích chi tiết
            </Link>
            <div className="flex flex-col gap-4">
              {listOfParts
                ?.sort((a, b) => a.partSequenceNumber - b.partSequenceNumber)
                ?.map((part) => {
                  const partUserAns = userAns?.find(
                    (item) => item.partId === part.id
                  );
                  return (
                    <div key={part.id} className="mt-2">
                      <div className="font-bold py-1 px-3 rounded-full bg-blue-100 text-purple-800 w-fit">
                        {part.partInformation.partInformationName}
                      </div>
                      <div className="grid grid-cols-1 gap-2 mt-2">
                        {part.questionGroups
                          ?.sort((a, b) => {
                            return (
                              moment(a.createdDate, "DD-MM-YYYY HH:mm:ss")
                                .toDate()
                                .getTime() -
                              moment(b.createdDate, "DD-MM-YYYY HH:mm:ss")
                                .toDate()
                                .getTime()
                            );
                          })
                          .map((questionGroup) => {
                            const qgUserAns = partUserAns?.questionGroups.find(
                              (item) =>
                                item.questionGroupId === questionGroup.id
                            );
                            return (
                              <div
                                key={questionGroup.id}
                                className="border rounded-md p-2 border-gray-300"
                              >
                                <p className="font-bold inline">
                                  {questionGroup.questionContentEn}
                                </p>
                                {questionGroup.image &&
                                  questionGroup.image !== "<p>&nbsp;</p>" && (
                                    <div
                                      dangerouslySetInnerHTML={{
                                        __html: questionGroup.image,
                                      }}
                                    ></div>
                                  )}
                                {/* Audio */}
                                {questionGroup.audio &&
                                  questionGroup.audio !== "" && (
                                    <ReactPlayer
                                      url={questionGroup.audio}
                                      controls
                                      height={50}
                                      ref={playerRef}
                                      wrapper={ReactPlayerWrapper}
                                    />
                                  )}
                                <div className="mt-4">
                                  <div className="grid grid-cols-1">
                                    {questionGroup.questions
                                      ?.sort(
                                        (a, b) =>
                                          a.questionNumber - b.questionNumber
                                      )
                                      ?.map((question) => {
                                        const qUserAns =
                                          qgUserAns?.questions.find(
                                            (item) =>
                                              item.questionId === question.id
                                          );
                                        // Question
                                        return (
                                          <Question
                                            key={question.id}
                                            part={part}
                                            questionGroup={questionGroup}
                                            question={question}
                                            questionUserAns={qUserAns}
                                          />
                                        );
                                      })}
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
