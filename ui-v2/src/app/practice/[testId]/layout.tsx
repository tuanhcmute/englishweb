import { ReactElement, ReactNode } from "react";
import { AppLayout } from "@/components/shared/layout";
import { Metadata, ResolvingMetadata } from "next";
import { cookies } from "next/headers";
import { ACCESS_TOKEN_KEY, REFRESH_TOKEN_KEY } from "@/constants";
import { IBaseResponse, ITestResponse } from "@/types";
import { httpServer } from "@/lib";

export async function generateMetadata(
  { params }: { params: { testId: string } },
  parent: ResolvingMetadata
): Promise<Metadata> {
  const cookieStore = cookies();
  const accessToken = cookieStore.get(ACCESS_TOKEN_KEY)?.value;
  const refreshToken = cookieStore.get(REFRESH_TOKEN_KEY)?.value;
  // fetch data
  const res: IBaseResponse<{ data: ITestResponse }> = await httpServer(
    {
      accessToken: accessToken as string,
      refreshToken: refreshToken as string,
    },
    `/test/${params.testId}`
  );

  return {
    title: `${res.data.data.testName} - English Academy`,
    description: `${res.data.data.testName} - English Academy`,
  };
}

export default function Layout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return <AppLayout>{children}</AppLayout>;
}
