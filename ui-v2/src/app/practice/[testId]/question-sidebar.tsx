import { Confirm } from "@/components/app/modal";
import { Button, Skeleton } from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { PracticeType } from "@/enums";
import { useCountDownTime, useCreateUserAnswer } from "@/hooks";
import { millisToMinutesAndSeconds } from "@/lib";
import { usePractice } from "@/providers";
import { IFullTestResponse, IUserAnswerRequest } from "@/types";
import * as Tabs from "@radix-ui/react-tabs";
import { UseQueryResult } from "@tanstack/react-query";
import { getCookie } from "cookies-next";
import { useSearchParams } from "next/navigation";
import { useEffect } from "react";
import { toast } from "react-hot-toast";
import { QuestionNumber } from "./question-number";

export const QuestionSidebar: React.FC<{
  fullTestQuery: UseQueryResult<IFullTestResponse, Error>;
  timeLimit?: string;
  handleClick: (partId: string) => void;
}> = ({ fullTestQuery, timeLimit, handleClick }) => {
  const searchParams = useSearchParams();
  const { isFinished, time } = useCountDownTime(
    timeLimit ? Number.parseInt(timeLimit) * 60000 : 7200000
  );
  const { handleGetChoicesResult } = usePractice();
  const { mutateAsync: handleCreateNewUserAnswer } = useCreateUserAnswer();
  const parts = searchParams.get("parts");

  async function handleSubmit() {
    const choicesResult = handleGetChoicesResult();
    const userCredentialId = getCookie(USER_CREDENTIAL_ID)
      ?.toString()
      .replaceAll('"', "");
    if (!userCredentialId) {
      toast.error("Thông tin người dùng không tồn tại");
      return;
    }
    if (!fullTestQuery.data?.id) {
      toast.error("Thông tin bài thi không tồn tại");
      return;
    }

    const completionTime = timeLimit
      ? Number.parseInt(timeLimit) - Math.round(time / 60000) // 60000 = 60 minutes * 1000 (ms)
      : 0;

    const requestData: IUserAnswerRequest = {
      userCredentialId,
      results: JSON.stringify(choicesResult),
      testId: fullTestQuery.data.id,
      practiceType: parts ? PracticeType.PARTIALS : PracticeType.FULL_TEST,
      completionTime: completionTime,
    };
    await handleCreateNewUserAnswer(requestData);
  }

  useEffect(() => {
    if (isFinished) {
      handleSubmit();
    }
  }, [isFinished]);

  return (
    <div
      className='2xl:w-1/4 lg:w-1/4 w-full border rounded-sm p-2 sticky flex flex-col gap-2 top-20'
      id='question-sidebar'
    >
      <div className=''>
        <div className='flex flex-wrap items-center gap-2'>
          <p>Thời gian còn lại:</p>
          <p className='font-bold'>{millisToMinutesAndSeconds(time)}</p>
        </div>
        <Confirm
          title='Nộp bài làm'
          description='Hãy chắc chắn các câu hỏi đã được trả lời.'
          textConfirm='Nộp bài'
          onConfirm={(close) => {
            handleSubmit();
            close?.();
          }}
        >
          <Button
            variant='ghost'
            className='w-full block mt-2 text-purple-800 font-bold uppercase border border-purple-800 hover:text-white hover:bg-purple-800 transition-all'
          >
            Nộp bài
          </Button>
        </Confirm>
      </div>
      {fullTestQuery?.isLoading ? (
        <Skeleton className='h-screen w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
      ) : (
        <div className={`w-full mt-2 overflow-y-auto h-screen`}>
          <div className='flex flex-col gap-3'>
            {parts
              ? fullTestQuery.data?.listOfPart
                  ?.filter((item) =>
                    parts.split(",").find((p) => p === item.id)
                  )
                  ?.sort((a, b) => a.partSequenceNumber - b.partSequenceNumber)
                  ?.map((part) => {
                    return (
                      <div className='flex flex-col gap-2' key={part.id}>
                        <p className='font-semibold'>
                          {part.partInformation.partInformationName}
                        </p>
                        {part.questionGroups
                          .filter((item) => item.questions.length > 0)
                          .map((qg) => {
                            return (
                              <div
                                className='flex items-center gap-2 flex-wrap'
                                key={qg.id}
                              >
                                {qg.questions.map((question) => {
                                  return (
                                    <QuestionNumber
                                      key={question.id}
                                      part={part}
                                      question={question}
                                      questionGroup={qg}
                                      handleClick={handleClick}
                                    />
                                  );
                                })}
                              </div>
                            );
                          })}
                      </div>
                    );
                  })
              : fullTestQuery.data?.listOfPart
                  ?.sort((a, b) => a.partSequenceNumber - b.partSequenceNumber)
                  ?.map((part) => {
                    return (
                      <div className='flex flex-col gap-2' key={part.id}>
                        <p className='font-semibold'>
                          {part.partInformation.partInformationName}
                        </p>
                        {part.questionGroups
                          .filter((item) => item.questions.length > 0)
                          .map((qg) => {
                            return (
                              <div
                                className='flex items-center gap-2 flex-wrap'
                                key={qg.id}
                              >
                                {qg.questions.map((question) => {
                                  return (
                                    <Tabs.List key={question.id}>
                                      <Tabs.Trigger value={part.id}>
                                        <QuestionNumber
                                          handleClick={handleClick}
                                          key={question.id}
                                          part={part}
                                          question={question}
                                          questionGroup={qg}
                                        />
                                      </Tabs.Trigger>
                                    </Tabs.List>
                                  );
                                })}
                              </div>
                            );
                          })}
                      </div>
                    );
                  })}
          </div>
        </div>
      )}
    </div>
  );
};
