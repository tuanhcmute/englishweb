import { Button, Label, RadioGroup, RadioGroupItem } from "@/components/ui";
import { BASE_URL } from "@/constants";
import { usePractice } from "@/providers";
import {
  IFullPartOfTestResponse,
  IQuestionGroupResponse,
  IQuestionResponse,
} from "@/types";
import { ReactElement } from "react";

interface IProps {
  question: IQuestionResponse;
  questionGroup: IQuestionGroupResponse;
  part: IFullPartOfTestResponse;
}

export function Question({
  question,
  questionGroup,
  part,
}: IProps): ReactElement {
  const { handleChoose, isChecked, handleRemoveChoice, toggleMark } =
    usePractice();
  return (
    <div key={question.id} className='rounded-md p-2' id={question.id}>
      <div className='mt-3'>
        {/* Question content */}
        <p className='flex gap-2 items-start font-bold'>
          <p className='rounded-full w-8 h-8 bg-blue-100 text-purple-800 font-bold text-[14px] flex items-center flex-col justify-center'>
            {question.questionNumber}
          </p>
          <p dangerouslySetInnerHTML={{ __html: question.questionContent }}></p>
        </p>
        <div className='flex items-start gap-5'>
          {/* Answer */}
          <div className='grid grid-cols-1 gap-3'>
            <RadioGroup
              onValueChange={(value) => {
                handleChoose({
                  answerId: value,
                  partId: part.id,
                  questionId: question.id,
                  questionGroupId: questionGroup.id,
                });
              }}
            >
              {question.testAnswers
                .sort((a, b) => a.answerChoice.localeCompare(b.answerChoice))
                .map((ans) => {
                  return (
                    <div key={ans.id} className='pl-10 flex gap-2 items-center'>
                      <RadioGroupItem
                        value={ans.id}
                        id={ans.id}
                        checked={isChecked({
                          answerId: ans.id,
                          partId: part.id,
                          questionGroupId: questionGroup.id,
                          questionId: question.id,
                        })}
                      />
                      <Label
                        htmlFor={ans.id}
                        className={`flex gap-2 items-center w-fit `}
                      >
                        <p className=''>({ans.answerChoice})</p>
                        <p>{ans.answerContent} </p>
                      </Label>
                    </div>
                  );
                })}
            </RadioGroup>
          </div>
        </div>
        <div className='flex items-center gap-4'>
          <Button
            variant='link'
            type='button'
            onClick={() => {
              toggleMark({
                answerId: "",
                partId: part.id,
                questionGroupId: questionGroup.id,
                questionId: question.id,
              });
            }}
          >
            Đánh dấu
          </Button>
          <Button
            variant='link'
            type='button'
            onClick={() =>
              handleRemoveChoice({
                answerId: "",
                partId: part.id,
                questionGroupId: questionGroup.id,
                questionId: question.id,
              })
            }
          >
            Xóa câu trả lời
          </Button>
        </div>
      </div>
    </div>
  );
}
