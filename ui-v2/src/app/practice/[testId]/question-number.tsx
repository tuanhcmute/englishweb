import { cn } from "@/lib";
import { usePractice } from "@/providers";
import {
  IFullPartOfTestResponse,
  IQuestionGroupResponse,
  IQuestionResponse,
} from "@/types";
import Link from "next/link";

export const QuestionNumber: React.FC<{
  part: IFullPartOfTestResponse;
  questionGroup: IQuestionGroupResponse;
  question: IQuestionResponse;
  handleClick: (partId: string) => void;
}> = ({ part, questionGroup, question, handleClick }) => {
  const { isQuestionChose, isMarked } = usePractice();

  return (
    <Link
      className={cn(
        "relative bg-gray-200 rounded w-9 h-9 text-[14px] flex items-center flex-col justify-center",
        isQuestionChose({
          answerId: "",
          partId: part.id,
          questionGroupId: questionGroup.id,
          questionId: question.id,
        })
          ? "bg-purple-800 text-white"
          : ""
      )}
      href={`#${question.id}`}
      key={question.id}
      onClick={() => handleClick(part.id)}
    >
      <div
        style={{ clipPath: "polygon(100% 100%,0 0,100% 0)" }}
        className={cn(
          isMarked({
            answerId: "",
            partId: part.id,
            questionGroupId: questionGroup.id,
            questionId: question.id,
          })
            ? "absolute top-0 right-0 w-4 h-4 bg-gradient-to-tr from-yellow-500 to-yellow-500 rounded-tr"
            : "hidden"
        )}
      ></div>
      {question.questionNumber}
    </Link>
  );
};
