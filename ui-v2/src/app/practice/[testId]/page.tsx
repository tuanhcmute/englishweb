"use client";

import { Confirm } from "@/components/app/modal";
import { Loading } from "@/components/shared/loading";
import { Button, Skeleton } from "@/components/ui";
import { useFullTest, usePartOfTest, useTest } from "@/hooks";
import { cn } from "@/lib";
import { PracticeProvider } from "@/providers";
import * as Tabs from "@radix-ui/react-tabs";
import { useRouter, useSearchParams } from "next/navigation";
import { ReactElement, ReactNode, useEffect, useRef, useState } from "react";
import ReactPlayer from "react-player";
import { Question } from "./question";
import { QuestionSidebar } from "./question-sidebar";
import moment from "moment";
import { IPartOfTest } from "@/types";

function ReactPlayerWrapper({ children }: { children: ReactNode }) {
  return <div className='w-full h-[40px] mt-4'>{children}</div>;
}

export default function PracticePage({
  params,
}: {
  params: { testId: string };
}): ReactElement {
  const searchParams = useSearchParams();
  const fullTestQuery = useFullTest(params.testId);
  const testQuery = useTest(params.testId);
  const partOfTestQuery = usePartOfTest(params.testId);
  const playerRef = useRef<ReactPlayer | null>(null);
  const [currentPart, setCurrentPart] = useState<string>("");
  const router = useRouter();
  const partsParam = searchParams.get("parts");
  const timeLimit =
    searchParams.get("timeLimit") ||
    testQuery.data?.testCategory?.totalTime?.toString() ||
    "120";

  const handler = (ev: BeforeUnloadEvent) => {
    ev.preventDefault();
  };
  useEffect(() => {
    window.addEventListener("beforeunload", (e) => handler(e));

    return () => window.removeEventListener("beforeunload", handler);
  }, []);

  const parts: IPartOfTest[] =
    (partsParam
      ? partOfTestQuery.data?.data.filter((item) =>
          partsParam?.split(",").find((p: string) => p === item.id)
        )
      : partOfTestQuery.data?.data) || [];

  return (
    <PracticeProvider testId={fullTestQuery.data?.id || ""}>
      <section id='practice' className='py-10 bg-gray-50 dark:bg-slate-950'>
        <div className=''>
          <div className='flex items-center justify-center gap-2 container'>
            <h3 className='text-2xl font-semibold'>
              {fullTestQuery.isSuccess && fullTestQuery.data?.testName}
            </h3>
            <Confirm
              title='Thoát bài thi'
              description='Tất cả dữ liệu sẽ bị mất sau khi thoát'
              textConfirm='Thoát'
              onConfirm={() => {
                router.replace("/tests");
              }}
            >
              <Button variant='destructive'>Thoát bài thi</Button>
            </Confirm>
          </div>
          {/* Test editor */}
          <Tabs.Root
            defaultValue={parts[0]?.id}
            className='mt-5 dark:text-gray-200'
          >
            <div
              id='test-panel'
              className='flex items-start gap-4 mx-5 mt-5 flex-col lg:flex-row'
            >
              <div className='w-full border rounded-sm p-2' id='questions'>
                {fullTestQuery.isLoading ? (
                  <Skeleton className='h-[100px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
                ) : (
                  <Tabs.List className='flex items-center gap-5 flex-wrap'>
                    {parts
                      .sort(
                        (a, b) => a.partSequenceNumber - b.partSequenceNumber
                      )
                      .map((part) => {
                        return (
                          <Tabs.Trigger
                            key={part.id}
                            value={part.id}
                            className={cn(
                              "py-1 px-2 rounded-full border border-gray-300 text-sm",
                              currentPart === part.id
                                ? "bg-blue-100 text-purple-800 font-semibold"
                                : ""
                            )}
                            onClick={() => setCurrentPart(part.id)}
                          >
                            {part.partInformation.partInformationName}
                          </Tabs.Trigger>
                        );
                      })}
                  </Tabs.List>
                )}
                {fullTestQuery.isLoading ? (
                  <Skeleton className='mt-5 h-screen w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
                ) : (
                  <div className='mt-5 h-screen'>
                    {fullTestQuery.data?.listOfPart?.map((part) => {
                      return (
                        <Tabs.Content
                          value={part.id}
                          className='overflow-y-auto h-full scroll-smooth'
                          key={part.id}
                        >
                          <div className='grid grid-cols-1 gap-2'>
                            {part.questionGroups
                              ?.sort((a, b) => {
                                return (
                                  moment(a.createdDate, "DD-MM-YYYY HH:mm:ss")
                                    .toDate()
                                    .getTime() -
                                  moment(b.createdDate, "DD-MM-YYYY HH:mm:ss")
                                    .toDate()
                                    .getTime()
                                );
                              })
                              .map((questionGroup) => (
                                <div
                                  key={questionGroup.id}
                                  className='border rounded-md p-2 border-gray-300'
                                >
                                  <p className='font-bold inline'>
                                    {questionGroup.questionContentEn}
                                  </p>
                                  {questionGroup.image &&
                                    questionGroup.image !== "<p>&nbsp;</p>" && (
                                      <div
                                        dangerouslySetInnerHTML={{
                                          __html: questionGroup.image,
                                        }}
                                      ></div>
                                    )}
                                  {/* Audio */}
                                  {questionGroup.audio &&
                                    questionGroup.audio !== "" && (
                                      <ReactPlayer
                                        url={questionGroup.audio}
                                        controls
                                        height={50}
                                        ref={playerRef}
                                        wrapper={ReactPlayerWrapper}
                                      />
                                    )}
                                  <div className='mt-4'>
                                    <div className='grid grid-cols-1'>
                                      {questionGroup.questions
                                        ?.sort(
                                          (a, b) =>
                                            a.questionNumber - b.questionNumber
                                        )
                                        ?.map((question) => {
                                          // Question
                                          return (
                                            <Question
                                              key={question.id}
                                              part={part}
                                              questionGroup={questionGroup}
                                              question={question}
                                            />
                                          );
                                        })}
                                    </div>
                                  </div>
                                </div>
                              ))}
                          </div>
                        </Tabs.Content>
                      );
                    })}
                  </div>
                )}
              </div>
              {/* Question sidebar */}
              <QuestionSidebar
                handleClick={setCurrentPart}
                fullTestQuery={fullTestQuery}
                timeLimit={timeLimit}
              />
            </div>
          </Tabs.Root>
        </div>
      </section>
    </PracticeProvider>
  );
}
