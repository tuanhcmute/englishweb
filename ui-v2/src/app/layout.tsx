import { cn } from "@/lib";
import { QueryClientProvider, ThemeProvider } from "@/providers";
import type { Metadata } from "next";
import { Roboto, Inter } from "next/font/google";
import { Toaster } from "react-hot-toast";
import "./globals.css";
import { description } from "./metadata";
import { TooltipLayout } from "./tooltip-layout";
import { WebVitals } from "./_components";

const font = Roboto({
  subsets: ["latin"],
  weight: ["100", "300", "400", "500", "700", "900"],
  display: "swap",
});
// const font = Inter({ subsets: ["latin"] });
// const font = Poppins({
//   subsets: ["latin"],
//   weight: ["100", "200", "400", "700"],
//   display: "swap",
// });

export const metadata: Metadata = {
  title: "Trang chủ - English Academy",
  description: description,
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={cn(font.className)}>
        <WebVitals />
        <ThemeProvider
          attribute="class"
          defaultTheme="system"
          enableSystem
          disableTransitionOnChange
        >
          <TooltipLayout>
            <QueryClientProvider>{children}</QueryClientProvider>
          </TooltipLayout>
        </ThemeProvider>
        <Toaster />
      </body>
    </html>
  );
}
