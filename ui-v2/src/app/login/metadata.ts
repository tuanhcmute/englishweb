export const description = `
Chào mừng bạn đến với trang đăng nhập của English Academy! Đây là nơi bạn có thể truy cập vào tài khoản cá nhân của mình để tiếp tục học tập và sử dụng các dịch vụ của chúng tôi. Vui lòng nhập thông tin đăng nhập của bạn để tiếp tục:

[Form đăng nhập]

Nếu bạn chưa có tài khoản với chúng tôi, hãy nhấp vào liên kết "Đăng ký tài khoản" để tạo một tài khoản mới. Chúng tôi đảm bảo rằng việc đăng ký là dễ dàng và nhanh chóng.

Nếu bạn quên mật khẩu của mình, hãy sử dụng chức năng "Quên mật khẩu" để khôi phục lại mật khẩu của bạn. Chúng tôi sẽ gửi cho bạn hướng dẫn để thiết lập lại mật khẩu mới.

Với tài khoản của bạn, bạn sẽ có quyền truy cập vào tất cả các khóa học, bài thi và tài liệu học tập của chúng tôi. Bạn cũng có thể theo dõi tiến trình học tập của mình, nhận phản hồi từ giảng viên và tham gia vào các hoạt động học tập cộng đồng.

Chúng tôi cam kết bảo mật thông tin cá nhân của bạn và sử dụng nó chỉ để cung cấp dịch vụ và cải thiện trải nghiệm học tập của bạn. Vui lòng đọc chính sách bảo mật của chúng tôi để biết thêm chi tiết về việc xử lý thông tin cá nhân.

Hãy đăng nhập ngay và bắt đầu hành trình học tập tiếng Anh của bạn cùng English Academy! Nếu bạn có bất kỳ câu hỏi hoặc vấn đề nào, đừng ngần ngại liên hệ với chúng tôi qua phần liên hệ trên trang web. Chúng tôi rất sẵn lòng hỗ trợ bạn.
`;
