import { z } from "zod";

export const formSchema = z.object({
  username: z
    .string()

    .trim()
    .min(1, {
      message: "Tên đăng nhập không được bỏ trống",
    })
    .max(100, {
      message: "Tên đăng nhập không được quá 100 ký tự",
    }),
  password: z.string().trim().min(1, {
    message: "Mật khẩu là bắt buộc",
  }),
});
