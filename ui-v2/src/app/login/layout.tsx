import type { Metadata } from "next";
import { ReactNode } from "react";
import { description } from "./metadata";

export const metadata: Metadata = {
  title: "Đăng nhập - English Academy",
  description: description,
};

export default function Layout({ children }: { children: ReactNode }) {
  return <>{children}</>;
}
