"use client";

import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  PasswordInput,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useLogin, useLoginWithSocial } from "@/hooks";
import { ILoginRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { IconBrandGoogle, IconLoader2 } from "@tabler/icons-react";
import { GoogleAuthProvider } from "firebase/auth";
import Link from "next/link";
import { ReactElement } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { formSchema } from "./formSchema";

export default function LoginPage(): ReactElement {
  const { mutateAsync: handleLogin, isPending } = useLogin();
  const { mutateAsync: handleLoginWithSocial, isPending: isPendingProvider } =
    useLoginWithSocial();

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    const requestData = { ...values } as ILoginRequest;
    await handleLogin(requestData);
  }

  return (
    <div className="flex flex-col items-center justify-center px-6 pt-8 mx-auto md:h-screen pt:mt-0 dark:bg-gray-900 h-screen">
      <Link
        href={ROUTES.HOME}
        className="flex items-center justify-center mb-8 text-2xl font-semibold lg:mb-10 dark:text-white hover:text-purple-800 transition-all"
      >
        <span>English Academy</span>
      </Link>
      <div className="w-full max-w-xl p-6 space-y-8 sm:p-8 bg-white rounded-lg dark:bg-gray-800 border border-gray-300">
        <h2 className="text-2xl font-bold text-gray-900 dark:text-white">
          Đăng nhập
        </h2>
        <div>
          <Form {...form}>
            <form
              onSubmit={form.handleSubmit(onSubmit)}
              className="flex flex-col gap-2"
            >
              <FormField
                control={form.control}
                name="username"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Tên đăng nhập</FormLabel>
                    <FormControl>
                      <Input placeholder="Tên đăng nhập" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Mật khẩu</FormLabel>
                    <FormControl>
                      <PasswordInput placeholder="Nhập mật khẩu" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <div className="flex justify-between align-middle">
                <Link
                  href={ROUTES.FORGOT_PASSWORD}
                  className="italic text-sm hover:underline hover:text-purple-800 transition-all"
                >
                  Quên mật khẩu?
                </Link>
                <Link
                  href={ROUTES.SIGN_UP}
                  className="text-sm hover:text-purple-800 transition-all"
                >
                  Tạo tài khoản mới
                </Link>
              </div>
              <Button
                type="submit"
                variant="destructive"
                className="w-fit border-purple-800 border text-white bg-purple-700 hover:bg-purple-800 flex items-center gap-1"
                disabled={isPending || isPendingProvider}
              >
                {isPending && <IconLoader2 className="animate-spin" />}
                Đăng nhập
              </Button>
            </form>
          </Form>
          <div className="mt-4 flex flex-col gap-2">
            <div className="text-center">Hoặc đăng nhập với</div>
            <div className="flex flex-col gap-5">
              <Button
                onClick={() => handleLoginWithSocial(new GoogleAuthProvider())}
                type="button"
                className="flex items-center justify-center w-full text-red-500 text-[16px] border border-red-500 hover:text-red-500"
                variant="ghost"
                disabled={isPendingProvider || isPending}
              >
                <IconBrandGoogle />
                <p className="ml-3 flex items-center gap-1">
                  {isPendingProvider && (
                    <IconLoader2 className="animate-spin" />
                  )}
                  Đăng nhập với Google
                </p>
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
