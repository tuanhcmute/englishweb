"use client";

import { Loading } from "@/components/shared/loading";
import {
  Button,
  Card,
  CardContent,
  CardDescription,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import { FlashcardType } from "@/enums";
import {
  useCreateStudyingFlashcard,
  useFlashcardsByType,
  useStudyingFlashcardsByUserCredential,
} from "@/hooks";
import { ICreateStudyingFlashcardRequest } from "@/types";
import { HomeIcon, RotateCounterClockwiseIcon } from "@radix-ui/react-icons";
import { getCookie } from "cookies-next";
import { UserIcon } from "lucide-react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import React, { ReactElement } from "react";

export default function FlashcardsPage(): ReactElement {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const flashcardsQuery = useFlashcardsByType(FlashcardType.SUGGESTION);
  const studyingFlashcardsQuery =
    useStudyingFlashcardsByUserCredential(userCredentialId);
  const { mutateAsync: handleCreateStudyingFlashcard } =
    useCreateStudyingFlashcard();
  const router = useRouter();

  if (flashcardsQuery.isLoading || studyingFlashcardsQuery.isLoading)
    return <Loading />;
  if (flashcardsQuery.error) return <div>{flashcardsQuery.error.message}</div>;

  const handleClick = async (flashcardId: string) => {
    const requestData: ICreateStudyingFlashcardRequest = {
      userCredentialId: userCredentialId || "",
      flashcardId: flashcardId,
    };
    await handleCreateStudyingFlashcard(requestData);
  };

  return (
    <>
      <div className=''>
        <Breadcrumb>
          <BreadcrumbList>
            <BreadcrumbItem>
              <BreadcrumbLink>
                <Link href={ROUTES.HOME} className='flex items-center gap-1'>
                  <HomeIcon className='w-4' />
                  Trang chủ
                </Link>
              </BreadcrumbLink>
            </BreadcrumbItem>
            <BreadcrumbSeparator className='flex items-center' />
            <BreadcrumbItem>
              <BreadcrumbPage className='flex items-center gap-1'>
                Flashcard gợi ý
              </BreadcrumbPage>
            </BreadcrumbItem>
          </BreadcrumbList>
        </Breadcrumb>
      </div>
      <div className='mt-5'>
        <div className='grid lg:grid-cols-4 gap-3'>
          {flashcardsQuery.data?.data.map((item) => {
            const isMatch: boolean =
              studyingFlashcardsQuery.data?.data?.some(
                (studyingFlashcard) =>
                  studyingFlashcard.flashcard.id === item.id
              ) || false;
            return (
              <Card
                className={`hover:shadow-md transition-all border  ${
                  isMatch ? "border-purple-500" : "border-green-500"
                }`}
                key={item.id}
              >
                <CardContent className='p-3'>
                  <CardTitle className='text-[1rem] cursor-pointer'>
                    <Link
                      href={`me/${item.id}`}
                      className='block hover:text-purple-800 transition-colors'
                    >
                      {item.flashcardTitle}
                    </Link>
                  </CardTitle>
                  <CardDescription className='text-sm mt-2'>
                    {item.flashcardDescription}
                  </CardDescription>
                  <CardDescription className='text-sm flex items-center gap-1 mt-2'>
                    <RotateCounterClockwiseIcon />{" "}
                    <p>{item.totalFlashcardItem} từ</p>
                  </CardDescription>
                  <CardDescription className='text-sm flex items-center gap-1 mt-2'>
                    <UserIcon className='w-4' />{" "}
                    <p>{item.userCredential.username}</p>
                  </CardDescription>
                  {isMatch ? (
                    <Button
                      onClick={() =>
                        router.push(`${ROUTES.FLASHCARDS}/studying`)
                      }
                      className='text-[13px] mt-2 w-full border border-purple-800 text-purple-800 hover:text-white dark:text-white hover:bg-purple-800 bg-transparent'
                    >
                      Học tiếp flashcard này
                    </Button>
                  ) : (
                    <Button
                      onClick={() => handleClick(item.id)}
                      className='text-[13px] mt-2 w-full border border-green-500 text-green-700 hover:text-white dark:text-white hover:bg-green-700 bg-transparent'
                    >
                      Thêm vào danh sách đang học
                    </Button>
                  )}
                </CardContent>
              </Card>
            );
          })}
        </div>
      </div>
    </>
  );
}
