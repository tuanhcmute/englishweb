"use client";

import { HomeIcon } from "@radix-ui/react-icons";
import React, { ReactElement } from "react";
import { ROUTES } from "@/constants";
import { useFlashcard, useFlashcardItems } from "@/hooks";
import { IconVolume } from "@tabler/icons-react";
import { IFlashcardItemResponse } from "@/types";
import {
  CreateFlashcardItemModal,
  DeleteFlashcardItemModal,
  UpdateFlashcardItemImageModal,
  UpdateFlashcardItemModal,
} from "@/components/app/modal";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Button,
  Skeleton,
} from "@/components/ui";
import { useRouter } from "next/navigation";
import Link from "next/link";

export default function FlashcardItemPage({
  params,
}: {
  params: { listId: string };
}): ReactElement {
  const flashcardQuery = useFlashcard(params.listId);
  const flashcardItemsQuery = useFlashcardItems(params.listId);
  const router = useRouter();

  return (
    <>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href={ROUTES.ME_FLASHCARDS}
                className="flex items-center gap-1"
              >
                <HomeIcon className="w-4" />
                Flashcards của tôi
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center" />
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              {flashcardQuery.data?.data.flashcardTitle}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {/* Flashcard container */}
      <div className="mt-5">
        <p className="font-bold text-xl mb-3">
          {flashcardQuery.data?.data.flashcardTitle}{" "}
          <Button
            variant="ghost"
            className="bg-blue-100 text-purple-800 hover:bg-blue-200 hover:text-purple-800 h-fit py-1 px-3"
            onClick={() => {
              router.push(
                `/practice/flashcard/${flashcardQuery.data?.data.id}`
              );
            }}
          >
            Luyện tập
          </Button>
        </p>
        <CreateFlashcardItemModal flashcardId={params.listId} />
        {flashcardItemsQuery.isLoading ? (
          <Skeleton className="h-screen mt-5 w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
        ) : (
          <div className="mt-5 flex flex-col gap-4">
            {flashcardItemsQuery.data?.data.map(
              (item: IFlashcardItemResponse) => {
                return (
                  <div className="border rounded-md px-3 py-1" key={item.id}>
                    <div className="flex items-start gap-2">
                      {/* Left content */}
                      <div className="">
                        <div className="flex items-center gap-2">
                          <p>{item.word}</p>
                          <p>{item.phonetic}</p>
                          <p
                            className="flex items-center gap-1 cursor-pointer"
                            onClick={() => {
                              const audio = new Audio(item.audio);
                              audio.play();
                            }}
                          >
                            <IconVolume className="w-5" />{" "}
                            <p className="text-sm italic text-blue-500">
                              (Nhấn để nghe phát âm)
                            </p>
                          </p>
                        </div>
                        <div className="">
                          <p className="mt-2 italic w-fit">Loại từ:</p>
                          <p className="pl-3">{item.tags}</p>
                        </div>
                        <div className="">
                          <p className="mt-2 italic w-fit">Định nghĩa:</p>
                          <p
                            className="pl-3"
                            dangerouslySetInnerHTML={{
                              __html: item.meaning || "",
                            }}
                          ></p>
                        </div>
                        <div className="mt-2">
                          <p className="italic">Ví dụ:</p>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: item.examples || "",
                            }}
                          ></p>
                        </div>
                        <div className="flex items-center gap-3">
                          <UpdateFlashcardItemModal flashcardItem={item} />
                          <DeleteFlashcardItemModal flashcardItemId={item.id} />
                        </div>
                      </div>
                      {/* Right content */}
                      <div className="w-1/2 py-4">
                        <div
                          dangerouslySetInnerHTML={{ __html: item.image }}
                        ></div>
                        <UpdateFlashcardItemImageModal
                          flashcardItemId={item.id}
                        />
                      </div>
                    </div>
                  </div>
                );
              }
            )}
          </div>
        )}
      </div>
    </>
  );
}
