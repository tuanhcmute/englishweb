"use client";

import { FlashcardsActionDropdown } from "@/components/app/dropdown";
import { FlashcardItemActionDropdown } from "@/components/app/dropdown/flashcard-item-action-dropdown";
import {
  CreateFlashcardModal,
  UpdateFlashcardModal,
} from "@/components/app/modal";
import { RemoveFlashcardModal } from "@/components/app/modal";
import {
  Button,
  Card,
  CardContent,
  CardDescription,
  CardTitle,
  Skeleton,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbPage,
  DropdownMenu,
  DropdownMenuTrigger,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
} from "@/components/ui";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import {
  useCreateStudyingFlashcard,
  useFlashcardsByUserCredential,
  useStudyingFlashcardsByUserCredential,
} from "@/hooks";
import { ICreateStudyingFlashcardRequest } from "@/types";
import { HomeIcon, RotateCounterClockwiseIcon } from "@radix-ui/react-icons";
import { getCookie } from "cookies-next";
import { UserIcon } from "lucide-react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import React, { ReactElement } from "react";

export default function FlashcardsPage(): ReactElement {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const flashcardsQuery = useFlashcardsByUserCredential(userCredentialId);
  const studyingFlashcardsQuery =
    useStudyingFlashcardsByUserCredential(userCredentialId);
  const { mutateAsync: handleCreateStudyingFlashcard } =
    useCreateStudyingFlashcard();
  const router = useRouter();

  if (flashcardsQuery.error) return <div>{flashcardsQuery.error.message}</div>;

  const handleClick = async (flashcardId: string) => {
    const requestData: ICreateStudyingFlashcardRequest = {
      userCredentialId: userCredentialId || "",
      flashcardId: flashcardId,
    };
    await handleCreateStudyingFlashcard(requestData);
  };

  return (
    <>
      {/* Path */}
      <div className="">
        <Breadcrumb>
          <BreadcrumbList>
            <BreadcrumbItem>
              <BreadcrumbPage>
                <Link
                  href={ROUTES.ME_FLASHCARDS}
                  className="flex items-center gap-1"
                >
                  <HomeIcon className="w-4" />
                  Flashcard của tôi
                </Link>
              </BreadcrumbPage>
            </BreadcrumbItem>
          </BreadcrumbList>
        </Breadcrumb>
      </div>
      {flashcardsQuery.isLoading ? (
        <Skeleton className="mt-5 w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
      ) : (
        <div className="mt-5">
          <div className="grid lg:grid-cols-4 gap-3">
            <Card className="hover:shadow-md transition-all w-full h-full border border-purple-500">
              <CardContent className="p-3 h-full">
                <CardTitle className="text-[1rem] cursor-pointer h-full">
                  <div className="flex items-center flex-col justify-center h-full">
                    <CreateFlashcardModal />
                  </div>
                </CardTitle>
              </CardContent>
            </Card>
            {flashcardsQuery.data?.data.map((item) => {
              const isMatch: boolean =
                studyingFlashcardsQuery.data?.data?.some(
                  (studyingFlashcard) =>
                    studyingFlashcard.flashcard.id === item.id
                ) || false;
              return (
                <Card
                  className={`hover:shadow-md transition-all border  ${
                    isMatch ? "border-purple-500" : "border-green-500"
                  }`}
                  key={item.id}
                >
                  <CardContent className="p-3">
                    <CardTitle className="text-[1rem] cursor-pointer">
                      <Link
                        href={`me/${item.id}`}
                        className="block hover:text-purple-800 transition-colors"
                      >
                        {item.flashcardTitle}
                      </Link>
                    </CardTitle>
                    <CardDescription className="text-sm mt-2">
                      {item.flashcardDescription}
                    </CardDescription>
                    <CardDescription className="text-sm flex items-center gap-1 mt-2">
                      <RotateCounterClockwiseIcon />{" "}
                      <p>{item.totalFlashcardItem} từ</p>
                    </CardDescription>
                    <CardDescription className="text-sm flex items-center gap-1 mt-2">
                      <UserIcon className="w-4" />{" "}
                      <p>{item.userCredential.username}</p>
                    </CardDescription>
                    <div className="flex items-center gap-2 mt-2">
                      <FlashcardItemActionDropdown flashcard={item} />
                    </div>
                    {isMatch ? (
                      <Button
                        onClick={() =>
                          router.push(`${ROUTES.FLASHCARDS}/studying`)
                        }
                        className="text-[13px] mt-2 w-full border border-purple-800 text-purple-800 hover:text-white dark:text-white hover:bg-purple-800 bg-transparent"
                      >
                        Học tiếp flashcard này
                      </Button>
                    ) : (
                      <Button
                        onClick={() => handleClick(item.id)}
                        className="text-[13px] mt-2 w-full border border-green-500 text-green-700 hover:text-white dark:text-white hover:bg-green-700 bg-transparent"
                      >
                        Thêm vào danh sách đang học
                      </Button>
                    )}
                  </CardContent>
                </Card>
              );
            })}
          </div>
        </div>
      )}
    </>
  );
}
