"use client";

import {
  Skeleton,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { HomeIcon } from "@radix-ui/react-icons";
import Link from "next/link";
import React, { ReactElement } from "react";

export default function FlashcardsPage(): ReactElement {
  return (
    <>
      {/* Path */}
      <div className=''>
        <Breadcrumb>
          <BreadcrumbList>
            <BreadcrumbItem>
              <BreadcrumbLink>
                <Link href={ROUTES.HOME} className='flex items-center gap-1'>
                  <HomeIcon className='w-4' />
                  Trang chủ
                </Link>
              </BreadcrumbLink>
            </BreadcrumbItem>

            <BreadcrumbSeparator className='flex items-center' />
            <BreadcrumbItem>
              <BreadcrumbPage className='flex items-center gap-1'>
                Danh sách flashcard
              </BreadcrumbPage>
            </BreadcrumbItem>
          </BreadcrumbList>
        </Breadcrumb>
      </div>
      {/* Flashcard container */}
      <div className='mt-5'>
        <div className='h-screen text-center font-bold'>
          Vui lòng chọn danh mục
        </div>
      </div>
    </>
  );
}
