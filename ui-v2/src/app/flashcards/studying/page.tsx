"use client";

import { Loading } from "@/components/shared/loading";
import {
  Button,
  Card,
  CardContent,
  CardDescription,
  CardTitle,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import { useStudyingFlashcardsByUserCredential } from "@/hooks";
import { HomeIcon, RotateCounterClockwiseIcon } from "@radix-ui/react-icons";
import { IconStatusChange } from "@tabler/icons-react";
import { getCookie } from "cookies-next";
import Link from "next/link";
import { useRouter } from "next/navigation";
import React, { ReactElement } from "react";

export default function StudyingFlashcardsPage(): ReactElement {
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const flashcardsQuery =
    useStudyingFlashcardsByUserCredential(userCredentialId);
  const router = useRouter();

  if (flashcardsQuery.isLoading || flashcardsQuery.isLoading)
    return <Loading />;
  if (flashcardsQuery.error) return <div>{flashcardsQuery.error.message}</div>;

  return (
    <>
      {/* Path */}
      <div className=''>
        <Breadcrumb>
          <BreadcrumbList>
            <BreadcrumbItem>
              <BreadcrumbLink>
                <Link href={ROUTES.HOME} className='flex items-center gap-1'>
                  <HomeIcon className='w-4' />
                  Trang chủ
                </Link>
              </BreadcrumbLink>
            </BreadcrumbItem>
            <BreadcrumbSeparator className='flex items-center' />
            <BreadcrumbItem>
              <BreadcrumbPage className='flex items-center gap-1'>
                Flashcard đang học
              </BreadcrumbPage>
            </BreadcrumbItem>
          </BreadcrumbList>
        </Breadcrumb>
      </div>
      <div className='mt-5'>
        <div className='grid lg:grid-cols-4 gap-3'>
          {flashcardsQuery.data?.data.map((item) => {
            return (
              <Card
                className='hover:shadow-md transition-all border border-purple-500'
                key={item.id}
              >
                <CardContent className='p-3'>
                  <CardTitle className='text-[1rem] cursor-pointer'>
                    <Link
                      href={`me/${item.flashcard.id}`}
                      className='block hover:text-purple-800 transition-colors'
                    >
                      {item.flashcard.flashcardTitle}
                    </Link>
                  </CardTitle>
                  <CardDescription>
                    <div className='text-[12px] flex items-center gap-1 mt-2'>
                      <RotateCounterClockwiseIcon />{" "}
                      <p>{item.flashcard.totalFlashcardItem} từ</p>
                    </div>
                    <div className='text-[12px] flex items-center gap-1 mt-2'>
                      <IconStatusChange className='w-4' />{" "}
                      <p className='flex items-center gap-1'>
                        Trạng thái:{" "}
                        <div className='w-fit bg-blue-100 text-purple-800 px-2 rounded-full'>
                          {item.status === "studying"
                            ? "Đang học"
                            : "Đã hoàn thành"}
                        </div>
                      </p>
                    </div>
                  </CardDescription>
                  <Button
                    className='text-[13px] mt-2 w-full border border-purple-800 text-purple-800 hover:text-white dark:text-white hover:bg-purple-800 bg-transparent'
                    onClick={() =>
                      router.push(
                        `${ROUTES.PRACTICE}/flashcard/${item.flashcard.id}`
                      )
                    }
                  >
                    Tiếp tục học
                  </Button>
                </CardContent>
              </Card>
            );
          })}
        </div>
      </div>
    </>
  );
}
