import { ROUTES } from "@/constants";
import { IconCheck } from "@tabler/icons-react";
import Link from "next/link";

export default function CheckoutSuccess() {
  return (
    <div className="bg-gray-100 dark:bg-slate-950">
      <div className="container">
        <div className="py-32 md:h-screen">
          <div className="flex items-center gap-4 flex-col">
            <div className="w-14 h-14 rounded-full flex items-center justify-center flex-col bg-green-500">
              <IconCheck className="text-white w-10 h-10" />
            </div>
            <h2 className="text-2xl font-bold">Thanh toán thành công</h2>
            <Link
              href={ROUTES.MY_COURSES}
              className="cursor-pointer hover:pb-1 hover:border-b-2 text-sm transition-all"
            >
              Đi tới trang quản lý khóa học
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
