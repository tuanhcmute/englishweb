import { z } from "zod";

export const formSendOTPSchema = z.object({
  email: z
    .string()
    .trim()
    .min(1, {
      message: "Email không được bỏ trống",
    })
    .max(100, {
      message: "Email không được quá 100 ký tự",
    })
    .email("Email không hợp lệ"),
});

export const formSchema = z
  .object({
    email: z
      .string()
      .trim()
      .min(1, {
        message: "Email không được bỏ trống",
      })
      .max(100, {
        message: "Email không được quá 100 ký tự",
      })
      .email("Email không hợp lệ"),
    newPassword: z
      .string()
      .min(6, { message: "Mật khẩu ít nhất 6 ký tự" })
      .regex(/[a-zA-Z]/, { message: "Mật khẩu phải chứa ít nhất 1 ký tự" })
      .regex(/[0-9]/, { message: "Mật khẩu phải chứa ít nhất một số" })
      .regex(/[^a-zA-Z0-9]/, {
        message: "Mật khẩu phải chứa ít nhất một ký tự đặc biệt",
      })
      .trim(),
    confirmPassword: z.string().trim().min(1, {
      message: "Xác nhận mật khẩu là bắt buộc",
    }),
    otp: z
      .string()
      .trim()
      .min(6, {
        message: "Xác nhận mật khẩu là bắt buộc",
      })
      .max(6),
  })
  .refine((data) => data.newPassword === data.confirmPassword, {
    message: "Xác nhận mật khẩu không khớp",
    path: ["confirmPassword"],
  });
