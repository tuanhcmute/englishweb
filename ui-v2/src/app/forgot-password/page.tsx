"use client";

import { ReactElement } from "react";
import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  PasswordInput,
  Separator,
} from "@/components/ui";
import Link from "next/link";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { ROUTES } from "@/constants";
import { useForgotPassword, useSendOTPForgotPassword } from "@/hooks";
import { formSchema, formSendOTPSchema } from "./formSchema";
import { IconClick, IconLoader2, IconSend2 } from "@tabler/icons-react";
import { IForgotPasswordRequest } from "@/types";

export default function ForgotPasswordPage(): ReactElement {
  const sendOTPForgotPasswordQuery = useSendOTPForgotPassword();
  const forgotPasswordQuery = useForgotPassword();

  // 1. Define your form.
  const formSendOTP = useForm<z.infer<typeof formSendOTPSchema>>({
    resolver: zodResolver(formSendOTPSchema),
    defaultValues: {
      email: "",
    },
  });
  const formForgotPassword = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      confirmPassword: "",
      newPassword: "",
      otp: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmitSendOTP(values: z.infer<typeof formSendOTPSchema>) {
    console.log(values);
    formForgotPassword.setValue("email", values.email);
    await sendOTPForgotPasswordQuery.mutateAsync(values.email);
  }
  async function onSubmitForgotPassword(values: z.infer<typeof formSchema>) {
    console.log({ values });
    const requestData = values as IForgotPasswordRequest;
    await forgotPasswordQuery.mutateAsync(requestData);
  }

  return (
    <div className='flex flex-col items-center justify-center px-6 pt-8 mx-auto md:h-screen pt:mt-0 dark:bg-gray-900'>
      <Link
        href={ROUTES.HOME}
        className='flex items-center justify-center mb-8 text-2xl font-semibold lg:mb-10 dark:text-white hover:text-purple-800 transition-all'
      >
        <span>English Academy</span>
      </Link>
      <div className='w-full max-w-xl p-6 space-y-8 sm:p-8 bg-white rounded-lg dark:bg-gray-800 border border-gray-300'>
        <h2 className='text-2xl font-bold text-gray-900 dark:text-white'>
          Quên mật khẩu
        </h2>
        <div>
          <Form {...formSendOTP}>
            <form
              onSubmit={formSendOTP.handleSubmit(onSubmitSendOTP)}
              className='flex flex-col gap-2'
            >
              <FormField
                control={formSendOTP.control}
                name='email'
                render={({ field }) => (
                  <div className='flex items-start gap-1'>
                    <FormItem className='basis-5/6'>
                      <FormControl>
                        <Input
                          placeholder='Nhập email nhận OTP...'
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                    <Button
                      type='submit'
                      variant='destructive'
                      className='w-fit border-purple-800 border text-white bg-purple-700 hover:bg-purple-800 basis-1/6'
                      disabled={sendOTPForgotPasswordQuery.isPending}
                    >
                      {!sendOTPForgotPasswordQuery.isPending ? (
                        <IconSend2 className='w-4 h-4 -rotate-45' />
                      ) : (
                        <IconLoader2 className='animate-spin' />
                      )}
                      Gửi
                    </Button>
                  </div>
                )}
              />
            </form>
          </Form>
          <Separator className='mt-3' />
          <Form {...formForgotPassword}>
            <form
              onSubmit={formForgotPassword.handleSubmit(onSubmitForgotPassword)}
            >
              <div className='grid gap-4 py-4'>
                <div className='grid grid-cols-1 gap-4'>
                  <FormField
                    control={formForgotPassword.control}
                    name='otp'
                    render={({ field }) => (
                      <FormItem className=''>
                        <FormLabel className='word'>Mã xác nhận (*)</FormLabel>
                        <FormControl>
                          <PasswordInput
                            {...field}
                            id='otp'
                            placeholder='Nhập mã xác nhận'
                            className='col-span-3'
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
                <div className='grid grid-cols-1 gap-4'>
                  <FormField
                    control={formForgotPassword.control}
                    name='newPassword'
                    render={({ field }) => (
                      <FormItem className=''>
                        <FormLabel className='word'>Mật khẩu mới (*)</FormLabel>
                        <FormControl>
                          <PasswordInput
                            {...field}
                            id='word'
                            placeholder='Nhập mật khẩu mới'
                            className='col-span-3'
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
                <div className='grid grid-cols-1 gap-4'>
                  <FormField
                    control={formForgotPassword.control}
                    name='confirmPassword'
                    render={({ field }) => (
                      <FormItem className=''>
                        <FormLabel className='confirmPassword'>
                          Xác nhận mật khẩu mới (*)
                        </FormLabel>
                        <FormControl>
                          <PasswordInput
                            {...field}
                            id='confirmPassword'
                            placeholder='Nhập xác nhận mật khẩu mới'
                            className='col-span-3'
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </div>
              </div>
              <div className='flex justify-center'>
                <Button
                  onClick={() => console.log(formForgotPassword.getValues())}
                  type='submit'
                  variant='destructive'
                  className='w-fit border-purple-800 border text-white bg-purple-700 hover:bg-purple-800 basis-1/6'
                  disabled={
                    forgotPasswordQuery.isPending ||
                    sendOTPForgotPasswordQuery.isPending
                  }
                >
                  <div className='flex items-center gap-1'>
                    {!sendOTPForgotPasswordQuery.isPending ? (
                      <IconClick className='w-4 h-4 -rotate-45' />
                    ) : (
                      <IconLoader2 className='animate-spin' />
                    )}
                    Đổi mật khẩu
                  </div>
                </Button>
              </div>
            </form>
          </Form>
          <div className='mt-4 flex flex-col gap-2'>
            <Link
              href={ROUTES.LOGIN}
              className='text-center text-sm hover:underline hover:text-purple-800 transition-all'
            >
              Quay về trang đăng nhập
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
