"use client";

import { usePathname } from "next/navigation";
import { useEffect, useRef, useState } from "react";

export function RouteChangeListener() {
  const pathname = usePathname();
  const prevPathnameRef = useRef<string>("");
  const currentPathnameRef = useRef<string>("");
  const [isLoading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    // Update previous pathname when the current pathname changes
    prevPathnameRef.current = currentPathnameRef.current;
    // Update current pathname
    currentPathnameRef.current = pathname;

    // Example: You can trigger other actions or updates based on route changes here
  }, [pathname]);

  // You can access the previous and current pathnames using prevPathnameRef.current and currentPathnameRef.current respectively
  // console.log("Previous Pathname:", prevPathnameRef.current);
  // console.log("Current Pathname:", currentPathnameRef.current);

  return null;
}
