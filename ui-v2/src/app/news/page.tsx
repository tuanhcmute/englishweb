"use client";

import {
  Card,
  CardTitle,
  CardDescription,
  CardContent,
  Skeleton,
  Breadcrumb,
  BreadcrumbList,
  BreadcrumbItem,
  BreadcrumbLink,
} from "@/components/ui";
import { useNews } from "@/hooks";
import { useNewsContext } from "@/providers";
import { IconNews } from "@tabler/icons-react";
import Link from "next/link";
import React, { ReactElement } from "react";

export default function NewsPage(): ReactElement {
  const { newsCategoryId } = useNewsContext();
  const newsQuery = useNews({ categoryId: newsCategoryId });

  return (
    <>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href="/admin/manage/news"
                className="flex items-center gap-1"
              >
                <IconNews className="w-4" />
                Quản lý bài viết
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>

      {/* News list */}
      {newsQuery?.isLoading ? (
        <div className="grid lg:grid-cols-2 gap-3 mt-3">
          {Array.from({ length: 4 }).map((_, index) => {
            return (
              <div key={index}>
                <Skeleton className=" h-[150px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
              </div>
            );
          })}
        </div>
      ) : (
        <div className="grid lg:grid-cols-2 gap-3 mt-3">
          {newsQuery.data?.data.map((item) => {
            return (
              <Card className="hover:shadow-md transition-all" key={item.id}>
                <CardContent>
                  <div
                    dangerouslySetInnerHTML={{ __html: item.newsImage }}
                  ></div>
                  <CardTitle className="text-[1rem] cursor-pointer mt-2">
                    <Link
                      href={`news/${item.id}`}
                      className="block hover:text-purple-800 transition-colors"
                    >
                      {item.newsTitle}
                    </Link>
                  </CardTitle>
                  <CardDescription className="text-sm mt-2">
                    {item.newsDescription}
                  </CardDescription>
                  <p className="text-sm italic mt-1">
                    {item.createdDate} bởi {item.author.fullName}
                  </p>
                </CardContent>
              </Card>
            );
          })}
        </div>
      )}
    </>
  );
}
