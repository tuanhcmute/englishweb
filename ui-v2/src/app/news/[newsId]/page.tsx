"use client";

import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Skeleton,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useNewsById } from "@/hooks";
import { DoubleArrowRightIcon, HomeIcon } from "@radix-ui/react-icons";
import { IconNews } from "@tabler/icons-react";
import Link from "next/link";
import React, { ReactElement } from "react";

export default function NewsContentPage({
  params,
}: {
  params: { newsId: string };
}): ReactElement {
  const newsQuery = useNewsById(params.newsId);

  return (
    <>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList className=''>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link href={ROUTES.NEWS} className='flex items-center gap-1'>
                <IconNews className='w-4 h-4' />
                Quản lý bài viết
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage>
              {newsQuery.isLoading ? (
                <Skeleton className=' h-4 w-[100px] rounded-xl bg-gray-200 dark:bg-slate-900' />
              ) : (
                newsQuery.data?.data.newsTitle
              )}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>

      {newsQuery?.isLoading ? (
        <Skeleton className=' h-[500px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
      ) : (
        <div>
          <div
            className='text-sm px-2 leading-6'
            dangerouslySetInnerHTML={{
              __html: newsQuery.data?.data.newsContentHtml || "",
            }}
          ></div>
        </div>
      )}
    </>
  );
}
