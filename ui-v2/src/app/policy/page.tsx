"use client";

import { Loading } from "@/components/shared/loading";
import { ROUTES } from "@/constants";
import { StaticPageCode } from "@/enums";
import { useStaticPageByCode } from "@/hooks";
import { cn } from "@/lib";
import { HomeIcon } from "@radix-ui/react-icons";
import { ReactElement } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui";
import Link from "next/link";

export default function PolicyPage(): ReactElement {
  const { isLoading, data } = useStaticPageByCode(StaticPageCode.POLICY);
  if (isLoading) return <Loading />;

  return (
    <div className={cn("bg-gray-100 dark:bg-slate-950")}>
      <div className='container py-4'>
        {/* Path */}
        <div className=''>
          <Breadcrumb>
            <BreadcrumbList>
              <BreadcrumbItem>
                <BreadcrumbLink>
                  <Link href={ROUTES.HOME} className='flex items-center gap-1'>
                    <HomeIcon className='w-4' />
                    Trang chủ
                  </Link>
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbSeparator className='flex items-center' />
              <BreadcrumbItem>
                <BreadcrumbPage className='flex items-center gap-1'>
                  Chính sách & điều khoản
                </BreadcrumbPage>
              </BreadcrumbItem>
            </BreadcrumbList>
          </Breadcrumb>
        </div>
        <div
          className='my-5 leading-8 text-sm'
          dangerouslySetInnerHTML={{
            __html: data?.data.pageContent || "",
          }}
        ></div>
      </div>
    </div>
  );
}
