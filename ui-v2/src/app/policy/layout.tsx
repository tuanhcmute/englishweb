import { ReactElement, ReactNode } from "react";
import { AppLayout } from "@/components/shared/layout";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Chính sách & điều khoản - English Academy",
  description: "Generated by create next app",
};

export default function Layout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return <AppLayout>{children}</AppLayout>;
}
