"use client"

import React from "react"

import { ColumnDef } from "@tanstack/react-table"
import {
  ISkilledAgentResponse,
  ITestSuiteQuestionResponse,
  ITestSuiteResponse,
  ITestSuiteRunResponse,
} from "@/types"
import { TestSuiteItemActionDropdown } from "@/components/app/dropdown"
import { IconCopy } from "@tabler/icons-react"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-center">Test suite id</div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "test_suite_name",
    header: () => <div className="text-center">Test suite name</div>,
    cell: ({ row }) => {
      return (
        <div className="font-bold hover:text-purple-800 cursor-pointer w-[150px]">
          {row.getValue("test_suite_name")}
        </div>
      )
    },
  },
  {
    accessorKey: "skilled_agents",
    header: () => <div className="text-center">Agents</div>,
    cell: ({ row }) => {
      const skilled_agents = row.getValue(
        "skilled_agents"
      ) as ISkilledAgentResponse[]
      return (
        <div className="w-[300px]">
          <div className="flex flex-wrap gap-1">
            {skilled_agents.map((item) => {
              return (
                <div
                  className="px-2 py-1 bg-green-500 rounded text-white"
                  key={item.id}
                >
                  {item.agent_name}
                </div>
              )
            })}
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "questions",
    header: () => <div className="text-center">Questions</div>,
    cell: ({ row }) => {
      const questions = row.getValue(
        "questions"
      ) as ITestSuiteQuestionResponse[]
      return <div className="">{questions?.length} (questions)</div>
    },
  },
  {
    accessorKey: "test_suite_runs",
    header: () => <div className="text-center">test_suite_runs</div>,
    cell: ({ row }) => {
      const test_suites_run = row.getValue(
        "test_suite_runs"
      ) as ITestSuiteRunResponse[]
      return <div className="">{test_suites_run?.length} (runs)</div>
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const skilledAgent = row._valuesCache as unknown as ITestSuiteResponse
      const id = row.getValue("id") as string
      return <TestSuiteItemActionDropdown item={{ ...skilledAgent, id }} />
    },
  },
] as ColumnDef<ITestSuiteResponse>[]
