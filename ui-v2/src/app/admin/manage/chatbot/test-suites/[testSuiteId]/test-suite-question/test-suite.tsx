"use client";

import { TestSuiteQuestionActionDropdown } from "@/components/app/dropdown";
import { DataTable } from "@/components/ui";
import { useTestSuite } from "@/hooks";
import { useParams } from "next/navigation";
import { columns } from "./columns";

export const TestSuiteTab = () => {
  const params = useParams<{ testSuiteId: string }>();
  const testSuiteQuery = useTestSuite(params.testSuiteId);

  return (
    <div className='flex flex-wrap items-start gap-3 mt-4'>
      <div className='w-full'>
        <DataTable
          columns={columns}
          actionButtons={<TestSuiteQuestionActionDropdown />}
          data={testSuiteQuery.data?.questions || []}
          filterByColumn='id'
        />
      </div>
    </div>
  );
};
