"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import {
  ITestSuiteQuestionResponse,
  ITestSuiteRunResultResponse,
} from "@/types";
import { IconCopy } from "@tabler/icons-react";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className='text-center'>Id</div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className='flex justify-center'>
          <div className='flex items-center gap-1 '>
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className='w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer'
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "run_question_result",
    header: () => <div className='text-center'>Question</div>,
    cell: ({ row }) => {
      const run_question_result = row.getValue(
        "run_question_result"
      ) as ITestSuiteQuestionResponse;
      return (
        <div className='hover:text-purple-800 cursor-pointer w-[200px]'>
          {run_question_result?.question_name}
        </div>
      );
    },
  },
  {
    accessorKey: "run_question_result",
    header: () => <div className='text-center'>Expected answer</div>,
    cell: ({ row }) => {
      const run_question_result = row.getValue(
        "run_question_result"
      ) as ITestSuiteQuestionResponse;
      return (
        <div className='hover:text-purple-800 cursor-pointer w-[300px]'>
          {run_question_result?.expected_answer}
        </div>
      );
    },
  },
  {
    accessorKey: "answer_result",
    header: () => <div className='text-center'>Expected answer</div>,
    cell: ({ row }) => {
      const answer_result = row.getValue("answer_result") as {
        answer_content: string;
        id: string;
      };
      return (
        <div className='hover:text-purple-800 cursor-pointer w-[300px]'>
          {answer_result?.answer_content}
        </div>
      );
    },
  },
  // {
  //   accessorKey: "actions",
  //   header: () => <div className='text-center'></div>,
  //   cell: ({ row }) => {
  //     const skilledAgent = row._valuesCache as unknown as ITestSuiteResponse;
  //     const id = row.getValue("id") as string;
  //     return <TestSuiteItemActionDropdown item={{ ...skilledAgent, id }} />;
  //   },
  // },
] as ColumnDef<ITestSuiteRunResultResponse>[];
