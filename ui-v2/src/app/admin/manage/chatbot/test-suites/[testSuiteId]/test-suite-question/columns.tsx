"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import { ITestSuiteQuestionResponse, ITestSuiteResponse } from "@/types";
import { TestSuiteItemActionDropdown } from "@/components/app/dropdown";
import { IconCopy } from "@tabler/icons-react";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className='text-center'>Question id</div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className='flex justify-center'>
          <div className='flex items-center gap-1 '>
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className='w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer'
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "question_name",
    header: () => <div className='text-center'>Question</div>,
    cell: ({ row }) => {
      return (
        <div className='hover:text-purple-800 cursor-pointer w-[300px]'>
          {row.getValue("question_name")}
        </div>
      );
    },
  },
  {
    accessorKey: "expected_answer",
    header: () => <div className='text-center'>Expected answer</div>,
    cell: ({ row }) => {
      return (
        <div className='w-[300px] text-center'>
          {row.getValue("expected_answer")}
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className='text-center'></div>,
    cell: ({ row }) => {
      const skilledAgent = row._valuesCache as unknown as ITestSuiteResponse;
      const id = row.getValue("id") as string;
      return <TestSuiteItemActionDropdown item={{ ...skilledAgent, id }} />;
    },
  },
] as ColumnDef<ITestSuiteQuestionResponse>[];
