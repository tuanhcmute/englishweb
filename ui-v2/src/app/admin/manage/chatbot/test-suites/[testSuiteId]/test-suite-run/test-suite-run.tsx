"use client";
import {
  Button,
  DataTable,
  Separator,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui";
import {
  useReRunSkilledAgentTest,
  useRunSkilledAgentTest,
  useTestSuite,
  useTestSuiteRunResults,
  useUpdateTestSuiteRun,
} from "@/hooks";
import { ICreateSkilledAgentTestRequest, ITestSuiteRunResponse } from "@/types";
import { useParams } from "next/navigation";
import { useState } from "react";
import Select, { SingleValue } from "react-select";
import { columns } from "./columns";
import { TEST_SUITE_RUN_STATUS } from "@/constants";

export const TestSuiteRunTab = () => {
  const [testSuiteRun, setTestSuiteRun] = useState<
    ITestSuiteRunResponse | undefined
  >(undefined);
  const params = useParams<{ testSuiteId: string }>();
  const testSuiteQuery = useTestSuite(params.testSuiteId);
  const testSuiteRunResultsQuery = useTestSuiteRunResults(
    testSuiteRun?.id || "",
    testSuiteRun?.skilled_agent_version_id || ""
  );
  const { mutateAsync: handleRunSkilledAgentTest, isPending: isPendingRun } =
    useRunSkilledAgentTest();
  const {
    mutateAsync: handleReRunSkilledAgentTest,
    isPending: isPendingReRun,
  } = useReRunSkilledAgentTest();
  const { mutateAsync: handleUpdateTestSuiteRun, isPending: isPendingUpdate } =
    useUpdateTestSuiteRun();

  const handleTest = async (type: string) => {
    if (testSuiteRun) {
      const requestData = {
        skilled_agent_version_id: testSuiteRun.skilled_agent_version_id,
        test_suite_run_id: testSuiteRun.id,
      } as ICreateSkilledAgentTestRequest;
      if (type === TEST_SUITE_RUN_STATUS.NOT_RUN) {
        await handleRunSkilledAgentTest(requestData);
        await handleUpdateTestSuiteRun({
          ...testSuiteRun,
          run_status: TEST_SUITE_RUN_STATUS.RE_RUN,
        });
      } else {
        await handleReRunSkilledAgentTest(requestData);
      }
    }
  };

  const loadTestSuiteOptions = () => {
    if (testSuiteQuery.data)
      return testSuiteQuery.data.test_suite_runs.map((item) => ({
        value: item.id,
        label: item.version,
      }));
    return [];
  };

  return (
    // <Form>
    <form>
      <div className='flex items-center gap-5 flex-wrap'>
        <p className='uppercase font-bold'>Test Suite Run info</p>
        <Select
          isDisabled={isPendingReRun || isPendingRun}
          className='text-sm w-1/5'
          isSearchable
          options={loadTestSuiteOptions()}
          placeholder='Search test suite run'
          onChange={(
            singleValue: SingleValue<{ label: string; value: string }>
          ) => {
            if (testSuiteQuery.data?.test_suite_runs) {
              const result = testSuiteQuery.data.test_suite_runs.find(
                (item) => item.id === singleValue?.value
              );
              setTestSuiteRun(result);
            }
          }}
        />
      </div>
      <div className='py-4'>
        {/* Info */}
        {testSuiteRun && (
          <Table>
            <TableHeader>
              <TableRow>
                <TableHead className='w-[100px]'>Status</TableHead>
                <TableHead>Score</TableHead>
                <TableHead>Version</TableHead>
                <TableHead className='text-right'>Questions</TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              <TableRow>
                <TableCell className='w-[200px] flex items-center'>
                  <Button
                    type='button'
                    disabled={isPendingRun || isPendingReRun}
                    className='select-none py-1 px-3 bg-purple-800 rounded text-white w-fit cursor-pointer hover:bg-purple-900'
                    onClick={async () => {
                      await handleTest(testSuiteRun.run_status);
                    }}
                  >
                    {testSuiteRun.run_status === TEST_SUITE_RUN_STATUS.NOT_RUN
                      ? TEST_SUITE_RUN_STATUS.RUN
                      : TEST_SUITE_RUN_STATUS.RE_RUN}
                  </Button>
                </TableCell>
                <TableCell>{testSuiteRun.score}</TableCell>
                <TableCell>{testSuiteRun.version}</TableCell>
                <TableCell className='text-right'>
                  {testSuiteRun.the_number_of_question}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        )}
        <Separator className='bg-gray-400 my-4' />
        <div className='uppercase font-bold'>Test Suite Run Results</div>
        {testSuiteRun && (
          <div className='w-full'>
            <DataTable
              columns={columns}
              actionButtons={<></>}
              data={testSuiteRunResultsQuery.data || []}
              filterByColumn='id'
            />
          </div>
        )}
      </div>
    </form>
    // </Form>
  );
};
