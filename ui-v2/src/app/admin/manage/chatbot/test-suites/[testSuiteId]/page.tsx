"use client";

import { TestSuiteQuestionActionDropdown } from "@/components/app/dropdown";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Skeleton,
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useTestSuite } from "@/hooks";
import { IconNews } from "@tabler/icons-react";
import Link from "next/link";
import { ReactElement } from "react";
import { TestSuiteTab } from "./test-suite-question/test-suite";
import { TestSuiteRunTab } from "./test-suite-run/test-suite-run";

export default function TestSuiteDetailManage({
  params,
}: {
  params: { testSuiteId: string };
}): ReactElement {
  const testSuiteQuery = useTestSuite(params.testSuiteId);
  if (testSuiteQuery.error)
    return (
      <div className='font-bold text-center'>
        {testSuiteQuery.error.message}
      </div>
    );

  return (
    <div className='mt-4 mr-4 mb-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href={ROUTES.ADMIN_TEST_SUITE_MANAGE}
                className='flex items-center gap-1'
              >
                <IconNews className='w-4' />
                Test Suites
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center gap-1' />
          <BreadcrumbItem>
            <BreadcrumbPage>
              {testSuiteQuery.data?.test_suite_name}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {testSuiteQuery.isLoading ? (
        <div className='my-4 flex flex-col gap-2'>
          <Skeleton className=' h-[500px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
        </div>
      ) : (
        <div className='flex flex-wrap items-start gap-3 mt-4'>
          {!testSuiteQuery.data ? (
            <div className='text-center w-full font-bold'>Not found Agent</div>
          ) : (
            <div className='w-full'>
              <div className=''>
                <TestSuiteQuestionActionDropdown />
              </div>
              <div className='mt-4'>
                <Tabs defaultValue='test-suite' className='w-full'>
                  <TabsList className='grid w-full grid-cols-2'>
                    <TabsTrigger value='test-suite'>Test Suite</TabsTrigger>
                    <TabsTrigger value='test-suite-run'>
                      Test Suite Run
                    </TabsTrigger>
                  </TabsList>
                  <TabsContent value='test-suite'>
                    <TestSuiteTab />
                  </TabsContent>
                  <TabsContent value='test-suite-run'>
                    <TestSuiteRunTab />
                  </TabsContent>
                </Tabs>
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
}
