"use client";

import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Textarea,
} from "@/components/ui";
import {
  useLLMModels,
  useSkilledAgentVersions,
  useTools,
  useUpdateSkilledAgentVersion,
} from "@/hooks";
import {
  ISkilledAgentVersionResponse,
  IUpdateSkilledAgentVersionRequest,
} from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useState } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import Select, { SingleValue } from "react-select";
import { z } from "zod";
import makeAnimated from "react-select/animated";
import { DashIcon, Pencil1Icon } from "@radix-ui/react-icons";
import { IconLoader2 } from "@tabler/icons-react";

const animatedComponents = makeAnimated();

const formSchema = z.object({
  id: z.string().trim().min(1),
  skilled_agent_id: z.string().trim().min(1),
  setup_message: z.array(
    z.object({
      type: z.string().trim().min(1, { message: "Type is required" }),
      message: z.string().trim(),
    })
  ),
  version: z.string().trim().min(1),
  llm_model_ids: z.array(
    z.object({
      label: z.string(),
      value: z.string().trim().min(1),
    })
  ),
  tool_ids: z.array(
    z.object({
      label: z.string(),
      value: z.string().trim().min(1),
    })
  ),
});

export const AgentVersionTab = ({ agentId }: { agentId: string }) => {
  const [agentVersionId, setAgentVersionId] = useState<string | undefined>(
    undefined
  );
  const [currentAgentVersion, setCurrentAgentVersion] = useState<
    ISkilledAgentVersionResponse | undefined
  >(undefined);

  const skilledAgentVersionsQuery = useSkilledAgentVersions(agentId);
  const toolsQuery = useTools();
  const llmModelsQuery = useLLMModels();
  const { mutateAsync: handleUpdateSkilledAgentVersion, isPending } =
    useUpdateSkilledAgentVersion();

  const loadVersions = (): { label: string; value: string }[] => {
    if (!skilledAgentVersionsQuery.data) return [];
    return skilledAgentVersionsQuery.data.map((item) => {
      return { label: item.version, value: item.id };
    });
  };

  const loadDefaultModels = (): {
    label: string;
    value: string;
  }[] => {
    if (currentAgentVersion) {
      return currentAgentVersion.llm_model_result.map((item) => ({
        label: item.llm_model_name,
        value: item.id,
      }));
    }
    return [];
  };

  const loadDefaultTools = (): {
    label: string;
    value: string;
  }[] => {
    if (currentAgentVersion) {
      return currentAgentVersion.tools.map((item) => {
        return {
          value: item.id,
          label: item.tool_name,
        };
      });
    }
    return [];
  };

  const loadTools = (): {
    label: string;
    value: string;
  }[] => {
    if (toolsQuery.data) {
      return toolsQuery.data.map((item) => {
        return {
          value: item.id,
          label: item.tool_name,
        };
      });
    }
    return [];
  };

  const loadLLMModels = (): {
    label: string;
    value: string;
  }[] => {
    if (llmModelsQuery.data) {
      return llmModelsQuery.data.map((item) => {
        return {
          value: item.id,
          label: item.llm_model_name,
        };
      });
    }
    return [];
  };

  const loadMessageTypes = (): {
    label: string;
    value: string;
  }[] => {
    const types = ["SystemMessage", "HumanMessage", "AIMessage"];
    return types.map((item) => {
      return {
        value: item,
        label: item.replace("Message", ""),
      };
    });
  };

  useEffect(() => {
    if (agentVersionId) {
      if (skilledAgentVersionsQuery.data) {
        setCurrentAgentVersion(
          skilledAgentVersionsQuery.data.find(
            (item) => item.id === agentVersionId
          )
        );
      }
    }
  }, [agentVersionId, skilledAgentVersionsQuery.data]);

  useEffect(() => {
    if (currentAgentVersion) {
      const setup_message = JSON.parse(currentAgentVersion.setup_message) as {
        type: string;
        message: string;
      }[];
      form.setValue("version", currentAgentVersion.version);
      form.setValue("setup_message", setup_message);
      form.setValue("llm_model_ids", loadDefaultModels());
      form.setValue("tool_ids", loadDefaultTools());
      form.setValue("id", currentAgentVersion.id);
    }
  }, [currentAgentVersion]);

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      skilled_agent_id: agentId,
      llm_model_ids: [],
      setup_message: [],
      tool_ids: [],
      version: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const setup_message = JSON.stringify(values.setup_message);
    const llm_model_ids = values.llm_model_ids.map((item) => item.value);
    const tool_ids = values.tool_ids.map((item) => item.value);
    const requestData = {
      ...values,
      setup_message,
      llm_model_ids,
      tool_ids,
    } as IUpdateSkilledAgentVersionRequest;
    console.log({ requestData });
    await handleUpdateSkilledAgentVersion(requestData);
  }

  const { append, remove, fields } = useFieldArray({
    name: "setup_message",
    control: form.control,
  });

  const handleAppend = () => {
    append({
      type: "",
      message: "",
    });
  };

  const handleRemove = (index: number) => {
    remove(index);
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className="flex items-center gap-5 flex-wrap">
          <p className="uppercase font-bold">Version info</p>
          <Select
            className="text-sm w-1/5"
            isSearchable
            options={loadVersions()}
            placeholder="Search test suite"
            onChange={(
              singleValue: SingleValue<{ label: string; value: string }>
            ) => {
              setAgentVersionId(singleValue?.value);
            }}
          />
          <Button
            type="submit"
            className="bg-purple-800 hover:bg-purple-900 text-white hover:text-white"
            disabled={isPending}
          >
            {isPending && <IconLoader2 className="animate-spin" />}
            Save
          </Button>
        </div>
        <div className="flex gap-4 py-4">
          <div className="w-1/2 flex flex-col gap-4">
            <div className="grid grid-cols-1 gap-4">
              <FormField
                control={form.control}
                name="llm_model_ids"
                render={({ field }) => (
                  <FormItem className="">
                    <FormLabel htmlFor="llm_model_ids">LLM model *</FormLabel>
                    <Select
                      className="text-sm"
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      options={loadLLMModels()}
                      value={form.getValues("llm_model_ids")}
                      placeholder="Select models"
                      isDisabled
                    />
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <div className="grid grid-cols-1 gap-4">
              <FormField
                control={form.control}
                name="tool_ids"
                render={({ field }) => (
                  <FormItem className="">
                    <FormLabel htmlFor="tool_ids">Tool *</FormLabel>
                    <Select
                      className="text-sm"
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      options={loadTools()}
                      value={form.getValues("tool_ids")}
                      placeholder="Select tools"
                      isDisabled
                    />
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
          </div>
          <div className="grid gap-4 py-4 w-1/2">
            <div className="grid grid-cols-1 gap-4">
              <FormField
                control={form.control}
                name="tool_ids"
                render={({ field }) => (
                  <FormItem className="">
                    <FormLabel htmlFor="tool_ids">Setup Messages *</FormLabel>
                    <FormControl>
                      <div>
                        {fields.map((item, index) => {
                          const errorForField = form.formState.errors
                            ?.setup_message?.[index] as {
                            type: { message: string };
                          };
                          return (
                            <div key={item.id}>
                              <div
                                key={item.message}
                                className="flex gap-4 py-4 items-center"
                              >
                                <Select
                                  className="text-sm w-1/4"
                                  components={animatedComponents}
                                  placeholder="Type"
                                  defaultValue={loadMessageTypes().find(
                                    (v) => v.value === item.type
                                  )}
                                  options={loadMessageTypes()}
                                  onChange={(...e) => {
                                    const newValue = e[0] as SingleValue<{
                                      label: string;
                                      value: string;
                                    }>;
                                    if (newValue?.value)
                                      form.setValue(
                                        `setup_message.${index}.type`,
                                        newValue.value
                                      );
                                  }}
                                />
                                <Textarea
                                  placeholder="Type a message"
                                  className="w-full"
                                  defaultValue={item.message}
                                  {...form.register(
                                    `setup_message.${index}.message` as const
                                  )}
                                />

                                <div className="w-[50px]">
                                  <DashIcon
                                    onClick={() => handleRemove(index)}
                                    className="w-6 h-6 bg-yellow-500 hover:bg-yellow-600 transition-all text-white rounded-full cursor-pointer "
                                  />
                                </div>
                              </div>
                              <p className="text-destructive text-sm">
                                {errorForField?.type?.message}
                              </p>
                            </div>
                          );
                        })}
                      </div>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
            <Button
              variant={"secondary"}
              className="w-fit bg-purple-800 text-white h-8 flex items-center gap-1 hover:bg-purple-900 px-2 justify-center"
              type="button"
              onClick={handleAppend}
            >
              New Message <Pencil1Icon />
            </Button>
          </div>
        </div>
      </form>
    </Form>
  );
};
