"use client";

import { SkilledAgentVersionsActionDropdown } from "@/components/app/dropdown";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Skeleton,
  Tabs,
  TabsContent,
  TabsList,
  TabsTrigger,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useSkilledAgent } from "@/hooks";
import { IconRobot } from "@tabler/icons-react";
import Link from "next/link";
import { ReactElement } from "react";
import { AgentDetail } from "./agent-detail";
import { AgentVersionTab } from "./agent-version";

export default function SkilledAgentDetailManage({
  params,
}: {
  params: { agentId: string };
}): ReactElement {
  const skilledAgentQuery = useSkilledAgent(params.agentId);
  if (skilledAgentQuery.error)
    return (
      <div className="font-bold text-center">
        {skilledAgentQuery.error.message}
      </div>
    );

  return (
    <div className="mt-4 mr-4 mb-4">
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href={ROUTES.ADMIN_SKILLED_AGENT_MANAGE}
                className="flex items-center gap-1"
              >
                <IconRobot className="w-4" />
                Agent
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center gap-1" />
          <BreadcrumbItem>
            <BreadcrumbPage>
              {skilledAgentQuery.data?.agent_name}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {skilledAgentQuery.isLoading ? (
        <div className="my-4 flex flex-col gap-2">
          <Skeleton className=" h-[500px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
        </div>
      ) : (
        <div className="flex flex-wrap items-start gap-3 mt-4">
          {!skilledAgentQuery.data ? (
            <div className="text-center w-full font-bold">Not found Agent</div>
          ) : (
            <div className="w-full">
              <div className="">
                <SkilledAgentVersionsActionDropdown />
              </div>
              <div className="mt-4">
                <Tabs defaultValue="detail" className="w-full">
                  <TabsList className="grid w-full grid-cols-2">
                    <TabsTrigger value="detail">Agent Detail</TabsTrigger>
                    <TabsTrigger value="version">Agent Version</TabsTrigger>
                  </TabsList>
                  <TabsContent value="detail">
                    <AgentDetail />
                  </TabsContent>
                  <TabsContent value="version">
                    <AgentVersionTab agentId={params.agentId} />
                  </TabsContent>
                </Tabs>
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
}
