"use client"

import React from "react"

import { ColumnDef } from "@tanstack/react-table"
import { ISkilledAgentResponse } from "@/types"
import { SkilledAgentItemActionDropdown } from "@/components/app/dropdown"
import { useRouter } from "next/navigation"
import { IconCopy } from "@tabler/icons-react"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-center">Agent id</div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "agent_name",
    header: () => <div className="text-center">Agent name</div>,
    cell: ({ row }) => {
      return (
        <div className="font-bold hover:text-purple-800 cursor-pointer w-[150px]">
          {row.getValue("agent_name")}
        </div>
      )
    },
  },
  {
    accessorKey: "description",
    header: () => <div className="text-center">Description</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[150px] text-center">
          {row.getValue("description")}
        </div>
      )
    },
  },
  {
    accessorKey: "setup_message",
    header: () => <div className="text-center">Setup message</div>,
    cell: ({ row }) => {
      return <div className="w-[300px]">{row.getValue("setup_message")}</div>
    },
  },
  {
    accessorKey: "test_score",
    header: () => <div className="text-center">Test score</div>,
    cell: ({ row }) => {
      return <div className="">{row.getValue("test_score")}</div>
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const skilledAgent = row._valuesCache as unknown as ISkilledAgentResponse
      const id = row.getValue("id") as string
      return <SkilledAgentItemActionDropdown item={{ ...skilledAgent, id }} />
    },
  },
] as ColumnDef<ISkilledAgentResponse>[]
