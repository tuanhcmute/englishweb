"use client"

import React from "react"

import { ColumnDef } from "@tanstack/react-table"
import {
  INewsCategoryResponse,
  IStaticPageResponse,
  IUserCredentialResponse,
} from "@/types"
import { NewsCategoriesItemActionDropdown } from "@/components/app/dropdown"
import { useRouter } from "next/navigation"
import { IconCopy } from "@tabler/icons-react"
import { PageNameCell } from "./page-name-cell"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-center">Mã trang tĩnh</div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "pageName",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: PageNameCell,
  },
  {
    accessorKey: "createdBy",
    header: () => <div className="text-center">Được tạo bởi</div>,
    cell: ({ row }) => {
      const createdBy = row.getValue("createdBy") as IUserCredentialResponse
      return (
        <div className="flex justify-center">
          <div className="w-[200px flex items-center gap-2">
            <img
              src={createdBy.avatar}
              alt="avatar"
              className="w-8 h-8 rounded-full"
            />
            {createdBy.username}
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "createdDate",
    header: () => <div className="text-center">Thời gian tạo</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="w-[150px] text-center">
            {row.getValue("createdDate")}
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "lastModifiedDate",
    header: () => <div className="text-center">Thời gian chỉnh sửa</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="w-[200px] text-center">
            {row.getValue("lastModifiedDate")}
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const category = row._valuesCache as unknown as INewsCategoryResponse
      const id = row.getValue("id") as string
      return <NewsCategoriesItemActionDropdown category={{ ...category, id }} />
    },
  },
] as ColumnDef<IStaticPageResponse>[]
