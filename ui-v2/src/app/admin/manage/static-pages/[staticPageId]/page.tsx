"use client";

import React, { ReactElement } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Input,
  Label,
} from "@/components/ui";
import { useStaticPage } from "@/hooks/use-static-page";
import { IconBrowser } from "@tabler/icons-react";
import Link from "next/link";
import { UpdateStaticPageModal } from "@/components/app/modal";

interface IProps {
  params: {
    staticPageId: string;
  };
}

export default function StaticPageDetailManage({
  params,
}: IProps): ReactElement {
  const staticPageQuery = useStaticPage(params.staticPageId);
  if (staticPageQuery.error)
    return (
      <div className="font-bold text-center">
        {staticPageQuery.error.message}
      </div>
    );

  return (
    <div className="m-4">
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href="/admin/manage/static-pages"
                className="flex items-center gap-1"
              >
                <IconBrowser className="w-4" />
                Quản lý trang tĩnh
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center" />
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              {staticPageQuery.data?.data.pageName}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      <div className="mt-5">
        {staticPageQuery.data?.data && (
          <UpdateStaticPageModal staticPage={staticPageQuery.data.data} />
        )}
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-1 gap-4">
            <Label className="" htmlFor="pageCode">
              Mã trang tĩnh *
            </Label>
            <Input
              id="pageCode"
              placeholder="Nhập mã trang tĩnh"
              className="col-span-3"
              value={staticPageQuery.data?.data.pageCode}
              disabled
            />
          </div>
          <div className="grid grid-cols-1 gap-4">
            <Label className="pageName">Tên trang tĩnh *</Label>
            <Input
              id="pageName"
              placeholder="Nhập tên trang tĩnh"
              className="col-span-3"
              value={staticPageQuery.data?.data.pageName}
              disabled
            />
          </div>
          <div className="grid grid-cols-1 gap-4">
            <Label className="pageName">Nội dung *</Label>
            <div
              dangerouslySetInnerHTML={{
                __html: staticPageQuery.data?.data.pageContent || "",
              }}
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
}
