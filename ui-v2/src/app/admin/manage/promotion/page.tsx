"use client";

import { DiscountsActionDropdown } from "@/components/app/dropdown";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { useDiscounts } from "@/hooks";
import { IconTicket } from "@tabler/icons-react";
import { BookAudioIcon } from "lucide-react";
import Link from "next/link";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function DiscountManage(): ReactElement {
  const discountsQuery = useDiscounts();
  if (discountsQuery.error) return <div>{discountsQuery.error?.message}</div>;

  return (
    <div className='mt-4 mr-4 mb-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href={"/admin/manage/courses"}
                className='flex items-center gap-1'
              >
                <IconTicket className='w-4' />
                Quản lý khuyến mãi
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage className='flex items-center gap-1'>
              Chương trình khuyến mãi
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {discountsQuery.isLoading ? (
        <div className='my-4 flex flex-col gap-2'>
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className=''>
                <Skeleton className=' h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            );
          })}
        </div>
      ) : (
        <div className='w-full mt-4'>
          <DataTable
            columns={columns}
            actionButtons={<DiscountsActionDropdown />}
            data={discountsQuery.data?.data || []}
            filterByColumn='id'
          />
        </div>
      )}
    </div>
  );
}
