"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import { IDiscountResponse } from "@/types";
import { DiscountItemActionDropdown } from "@/components/app/dropdown";
import { DiscountStatus } from "@/enums";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      return <div className="text-right font-medium"></div>;
    },
  },
  {
    accessorKey: "discountName",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: ({ row }) => {
      return (
        <div className="font-bold hover:text-purple-800 w-[250px]">
          {row.getValue("discountName")}
        </div>
      );
    },
  },
  {
    accessorKey: "startDate",
    header: () => <div className="text-center">Thời gian bắt đầu</div>,
    cell: ({ row }) => {
      return (
        <div className="hover:text-purple-800 cursor-pointer text-center">
          {row.getValue("startDate")}
        </div>
      );
    },
  },
  {
    accessorKey: "endDate",
    header: () => <div className="text-center">Thời gian kết thúc</div>,
    cell: ({ row }) => {
      return (
        <div className="hover:text-purple-800 cursor-pointer">
          {row.getValue("endDate")}
        </div>
      );
    },
  },
  {
    accessorKey: "status",
    header: () => <div className="text-center">Trạng thái</div>,
    cell: ({ row }) => {
      const renderUI = (status: string) => {
        switch (status) {
          case DiscountStatus.SCHEDULED:
            return (
              <div className="bg-yellow-500 text-white py-1 px-2 rounded">
                Lên lịch
              </div>
            );
          case DiscountStatus.RUNNING:
            return (
              <div className="bg-green-600 text-white py-1 px-2 rounded ">
                Đang hoạt động
              </div>
            );
          case DiscountStatus.TERMINATED:
            return (
              <div className="bg-red-600 text-white py-1 px-2 rounded">
                Đã kết thúc
              </div>
            );
        }
      };
      return (
        <div className="flex justify-center min-w-[200px]">
          <div className="py-1 px-2">{renderUI(row.getValue("status"))}</div>
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const discount = row._valuesCache as unknown as IDiscountResponse;
      const id = row.getValue("id") as string;
      return <DiscountItemActionDropdown discount={{ ...discount, id }} />;
    },
  },
] as ColumnDef<IDiscountResponse>[];
