"use client";

import {
  CreateAutoFlashcardItemModal,
  CreateFlashcardItemModal,
  DeleteFlashcardItemModal,
  UpdateFlashcardItemImageModal,
  UpdateFlashcardItemModal,
} from "@/components/app/modal";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Skeleton,
} from "@/components/ui";
import { useFlashcard, useFlashcardItems } from "@/hooks";
import { IFlashcardItemResponse } from "@/types";
import { HomeIcon } from "@radix-ui/react-icons";
import { IconVolume } from "@tabler/icons-react";
import Link from "next/link";
import { ReactElement } from "react";

export default function FlashcardDetailManage({
  params,
}: {
  params: { flashcardId: string };
}): ReactElement {
  const flashcardQuery = useFlashcard(params.flashcardId);
  const flashcardItemsQuery = useFlashcardItems(params.flashcardId);

  if (flashcardQuery.error)
    return (
      <div className="font-bold text-center w-full">
        {flashcardQuery.error.message}
      </div>
    );

  return (
    <div className="my-4 mx-0">
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href="/admin/manage/flashcards"
                className="flex items-center gap-1"
              >
                <HomeIcon className="w-4" />
                Quản lý flashcard
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center" />
          <BreadcrumbItem>
            <BreadcrumbPage>
              {flashcardQuery.isLoading ? (
                <Skeleton className=" h-4 w-[100px] rounded-xl bg-gray-200 dark:bg-slate-900" />
              ) : (
                flashcardQuery.data?.data.flashcardTitle
              )}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      <div className="">
        <div className="mt-5">
          <div className="text-2xl font-bold">
            {flashcardQuery.data?.data.flashcardTitle}
          </div>
          <div className="text-sm mt-2">
            {flashcardQuery.data?.data.flashcardDescription}
          </div>
          <div className="my-2 text-sm italic">
            Danh sách có tất cả: {flashcardQuery.data?.data.totalFlashcardItem}{" "}
            từ
          </div>
          <div className="flex items-center gap-2">
            <CreateFlashcardItemModal flashcardId={params.flashcardId} />
            <CreateAutoFlashcardItemModal flashcardId={params.flashcardId} />
          </div>
          <div className="flex flex-col gap-4 mt-2">
            {flashcardItemsQuery.data?.data.map(
              (item: IFlashcardItemResponse) => {
                return (
                  <div className="border rounded-md px-3 py-1" key={item.id}>
                    <div className="flex items-start gap-2">
                      {/* Left content */}
                      <div className="">
                        <div className="flex items-center gap-2">
                          <p>{item.word}</p>
                          <p>{item.phonetic}</p>
                          <p
                            className="flex items-center gap-1 cursor-pointer"
                            onClick={() => {
                              const audio = new Audio(item.audio);
                              console.log({ audio });
                              audio.play();
                            }}
                          >
                            <IconVolume className="w-5" />{" "}
                            <p className="text-sm italic text-blue-500">
                              (Nhấn để nghe phát âm)
                            </p>
                          </p>
                        </div>
                        <div className="">
                          <p className="mt-2 italic w-fit">Loại từ:</p>
                          <p className="pl-3">{item.tags}</p>
                        </div>
                        <div className="">
                          <p className="mt-2 italic w-fit">Định nghĩa:</p>
                          <p
                            className="pl-3"
                            dangerouslySetInnerHTML={{
                              __html: item.meaning || "",
                            }}
                          ></p>
                        </div>
                        <div className="mt-2">
                          <p className="italic">Ví dụ:</p>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: item.examples || "",
                            }}
                          ></p>
                        </div>
                        <div className="flex items-center gap-3">
                          <UpdateFlashcardItemModal flashcardItem={item} />
                          <DeleteFlashcardItemModal flashcardItemId={item.id} />
                        </div>
                      </div>
                      {/* Right content */}
                      <div className="w-1/2 py-4">
                        <div
                          dangerouslySetInnerHTML={{ __html: item.image }}
                        ></div>
                        <UpdateFlashcardItemImageModal
                          flashcardItemId={item.id}
                        />
                      </div>
                    </div>
                  </div>
                );
              }
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
