"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import {
  ICourseResponse,
  IFlashcardResponse,
  IUserCredentialResponse,
} from "@/types";
import { CourseItemActionDropdown } from "@/components/app/dropdown";
import { IconCopy } from "@tabler/icons-react";
import { FlashcardItemActionDropdown } from "@/components/app/dropdown/flashcard-item-action-dropdown";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "flashcardTitle",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: ({ row }) => {
      return (
        <div className="font-medium w-[150px]">
          {row.getValue("flashcardTitle")}
        </div>
      );
    },
  },
  {
    accessorKey: "flashcardDescription",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[200px]">{row.getValue("flashcardDescription")}</div>
      );
    },
  },
  {
    accessorKey: "userCredential",
    header: () => <div className="text-center">Người tạo</div>,
    cell: ({ row }) => {
      const userCredential = row.getValue(
        "userCredential"
      ) as IUserCredentialResponse;
      return (
        <div className="w-[200px] flex items-center gap-1">
          <img
            src={userCredential?.avatar}
            alt="avatar"
            className="rounded-full w-8 h-8"
          />
          {userCredential?.username}
        </div>
      );
    },
  },
  {
    accessorKey: "totalFlashcardItem",
    header: () => <div className="text-center">Số lượng từ</div>,
    cell: ({ row }) => {
      return (
        <div className="border-green-600 bg-green-600  px-3 border py-1 w-fit rounded text-white">
          {row.getValue("totalFlashcardItem")} từ
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id");
      const flashcard = {
        ...row._valuesCache,
        id,
      } as unknown as IFlashcardResponse;
      return <FlashcardItemActionDropdown flashcard={flashcard} />;
    },
  },
] as ColumnDef<IFlashcardResponse>[];
