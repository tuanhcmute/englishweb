"use client";

import { FlashcardsActionDropdown } from "@/components/app/dropdown";
import { CreateSuggestionFlashcardModal } from "@/components/app/modal";
import { DataTable, Skeleton } from "@/components/ui";
import { useFlashcards } from "@/hooks";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function FlashcardManage(): ReactElement {
  const flashcardsQuery = useFlashcards();
  if (flashcardsQuery.error) return <div>{flashcardsQuery.error?.message}</div>;

  return (
    <div className="mt-4 mr-4 mb-4">
      {flashcardsQuery.isLoading ? (
        <div className="my-4 flex flex-col gap-2">
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className="">
                <Skeleton className=" h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
              </div>
            );
          })}
        </div>
      ) : (
        <div className="w-full mt-4">
          <DataTable
            columns={columns}
            actionButtons={<FlashcardsActionDropdown />}
            data={flashcardsQuery.data?.data || []}
            filterByColumn="id"
          />
        </div>
      )}
    </div>
  );
}
