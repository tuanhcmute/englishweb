"use client"

import React from "react"

import { ColumnDef } from "@tanstack/react-table"
import { ITestCategoryResponse } from "@/types"
import { TestCategoriesItemActionDropdown } from "@/components/app/dropdown"
import { useRouter } from "next/navigation"
import { TestCategoryNameCell } from "./test-category-name-cell"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      return <div className="text-right font-medium"></div>
    },
  },
  {
    accessorKey: "testCategoryName",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: TestCategoryNameCell,
  },
  {
    accessorKey: "numberOfPart",
    header: () => <div className="text-center">Số phần thi</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="text-center bg-green-600 text-white px-2 py-1 rounded w-fit">
            {row.getValue("numberOfPart")} phần
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "totalQuestion",
    header: () => <div className="text-center">Số lượng câu hỏi</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="text-center bg-yellow-500 text-white px-2 py-1 rounded w-fit">
            {row.getValue("totalQuestion")} câu hỏi
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "totalTime",
    header: () => <div className="text-center">Số lượng câu hỏi</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="text-center bg-orange-500 text-white px-2 py-1 rounded w-fit">
            {row.getValue("totalTime")} phút
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "createdDate",
    header: () => <div className="text-center">Thời gian tạo</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[200px] text-center">
          {row.getValue("createdDate")}
        </div>
      )
    },
  },
  {
    accessorKey: "lastModifiedDate",
    header: () => <div className="text-center">Thời gian chỉnh sửa</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[200px] text-center">
          {row.getValue("lastModifiedDate")}
        </div>
      )
    },
  },
  // {
  //   accessorKey: "status",
  //   header: () => <div className='text-right'>Trạng thái</div>,
  //   cell: ({ row }) => {
  //     return (
  //       <div
  //         className={cn(
  //           "text-center px-2 rounded text-white border w-fit py-1",
  //           row.getValue("status") === NewsCategoryType.VISIBLE
  //             ? "bg-green-600"
  //             : "bg-yellow-500"
  //         )}
  //       >
  //         {row.getValue("status") === NewsCategoryType.VISIBLE
  //           ? "Hiển thị"
  //           : "Ẩn"}
  //       </div>
  //     );
  //   },
  // },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const category = row._valuesCache as unknown as ITestCategoryResponse
      const id = row.getValue("id") as string
      return <TestCategoriesItemActionDropdown category={{ ...category, id }} />
    },
  },
] as ColumnDef<ITestCategoryResponse>[]
