import { ITestCategoryResponse } from "@/types"
import { Row } from "@tanstack/react-table"
import { useRouter } from "next/navigation"
import React from "react"

export const TestCategoryNameCell: React.FC<{
  row: Row<ITestCategoryResponse>
}> = ({ row }) => {
  const router = useRouter()
  return (
    <div
      onClick={() => router.push(`news/${row.getValue("id")}`)}
      className="font-bold hover:text-purple-800 cursor-pointer w-[200px]"
    >
      {row.getValue("testCategoryName")}
    </div>
  )
}
