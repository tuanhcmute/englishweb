"use client";

import { Loading } from "@/components/shared/loading";
import { usePartInformation } from "@/hooks";
import { ReactElement } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { IconNews } from "@tabler/icons-react";
import Link from "next/link";
import { columns } from "./columns";
import {
  PartInformationActionDropdown,
  TestCategoryActionDropdown,
} from "@/components/app/dropdown";

export default function PartInformationManage({
  params,
}: {
  params: { testCategoryId: string };
}): ReactElement {
  // Call API
  const partInformationQuery = usePartInformation(params.testCategoryId);
  if (partInformationQuery.isLoading) return <Loading />;
  if (partInformationQuery.error)
    return <div>{partInformationQuery.error.message}</div>;

  return (
    <div className='mt-4 mr-4 mb-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink className='flex items-center gap-1'>
              <Link
                href={ROUTES.ADMIN_TEST_CATEGORY_MANAGE}
                className='flex items-center gap-1'
              >
                <IconNews className='w-4' />
                Quản lý danh mục bài thi
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage>{params.testCategoryId}</BreadcrumbPage>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage>Thông tin phần thi</BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {partInformationQuery.isLoading ? (
        <div className='my-4 flex flex-col gap-2'>
          {Array.from({ length: 10 }).map((_, index) => {
            return (
              <div key={index} className=''>
                <Skeleton className=' h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            );
          })}
        </div>
      ) : (
        <div className='flex flex-wrap items-start gap-3 mt-4'>
          {partInformationQuery.data?.data.length === 0 ? (
            <div className='text-center w-full font-bold'>Không có dữ liệu</div>
          ) : (
            <div className='w-full'>
              <DataTable
                columns={columns}
                actionButtons={<PartInformationActionDropdown />}
                data={partInformationQuery.data?.data || []}
                filterByColumn='id'
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
}
