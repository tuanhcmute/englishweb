"use client"

import React from "react"

import { ColumnDef } from "@tanstack/react-table"
import { IPartInformationResponse } from "@/types"
import { PartInformationItemActionDropdown } from "@/components/app/dropdown"
import { useRouter } from "next/navigation"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      return <div className="text-right font-medium"></div>
    },
  },

  {
    accessorKey: "partSequenceNumber",
    header: () => <div className="text-center">Số thứ tự</div>,
    cell: ({ row }) => {
      return (
        <div className="text-center min-w-[100px]">
          {row.getValue("partSequenceNumber")}
        </div>
      )
    },
  },
  {
    accessorKey: "partInformationName",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: ({ row }) => {
      return (
        <div className="font-bold hover:text-purple-800 cursor-pointer w-[200px]">
          {row.getValue("partInformationName")}
        </div>
      )
    },
  },
  {
    accessorKey: "totalQuestion",
    header: () => <div className="text-center">Số lượng câu hỏi</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center min-w-[150px]">
          <div className="text-center bg-yellow-500 text-white px-2 py-1 rounded w-fit">
            {row.getValue("totalQuestion")} câu hỏi
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "partInformationDescription",
    header: () => <div className="text-center">Mô tả</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="">{row.getValue("partInformationDescription")}</div>
        </div>
      )
    },
  },
  {
    accessorKey: "partType",
    header: () => <div className="text-center">Loại</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center min-w-[100px]">
          <div className="text-center bg-green-600 text-white py-1 px-2 flex justify-center flex-col rounded capitalize">
            {row.getValue("partType")}
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const item = row._valuesCache as unknown as IPartInformationResponse
      const id = row.getValue("id") as string
      return <PartInformationItemActionDropdown item={{ ...item, id }} />
    },
  },
] as ColumnDef<IPartInformationResponse>[]
