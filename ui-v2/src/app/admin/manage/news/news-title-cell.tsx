import { INewsResponse } from "@/types"
import { Row } from "@tanstack/react-table"
import { useRouter } from "next/navigation"
import React from "react"

export const NewsTitleCell: React.FC<{ row: Row<INewsResponse> }> = ({
  row,
}) => {
  const router = useRouter()
  return (
    <div
      onClick={() => router.push(`news/${row.getValue("id")}`)}
      className="font-bold hover:text-purple-800 cursor-pointer"
    >
      {row.getValue("newsTitle")}
    </div>
  )
}
