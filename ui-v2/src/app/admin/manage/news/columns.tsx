"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import { INewsCategoryResponse, INewsResponse } from "@/types";
import { NewsItemActionDropdown } from "@/components/app/dropdown";
import { NewsTitleCell } from "./news-title-cell";
import { IconCopy } from "@tabler/icons-react";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "newsImage",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      return (
        <div
          className="text-right font-medium w-[200px]"
          dangerouslySetInnerHTML={{ __html: row.getValue("newsImage") }}
        ></div>
      );
    },
  },
  {
    accessorKey: "newsTitle",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: NewsTitleCell,
  },
  {
    accessorKey: "newsDescription",
    header: () => <div className="text-center">Mô tả</div>,
    cell: ({ row }) => {
      return (
        <div className="font-medium w-[300px]">
          {row.getValue("newsDescription")}
        </div>
      );
    },
  },
  {
    accessorKey: "newsCategory",
    header: () => <div className="text-center">Danh mục</div>,
    cell: ({ row }) => {
      const newsCategory = row.getValue(
        "newsCategory"
      ) as INewsCategoryResponse;
      return (
        <div className="w-[200px] flex justify-center">
          <div className="capitalize px-3 rounded bg-purple-800 text-white py-1 border w-fit">
            {newsCategory?.newsCategoryName}
          </div>
        </div>
      );
    },
  },
  // {
  //   accessorKey: "status",
  //   header: () => <div className="text-center">Trạng thái</div>,
  //   cell: ({ row }) => {
  //     return (
  //       <div className="w-[100px] flex justify-center">
  //         <div
  //           className={cn(
  //             "text-center px-2 rounded text-white border w-fit py-1",
  //             row.getValue("status") === NewsType.APPROVED
  //               ? "bg-green-600"
  //               : "bg-yellow-500"
  //           )}
  //         >
  //           {row.getValue("status") === NewsType.APPROVED ? "Hiển thị" : "Ẩn"}
  //         </div>
  //       </div>
  //     );
  //   },
  // },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const news = row._valuesCache as unknown as INewsResponse;
      return <NewsItemActionDropdown news={news} />;
    },
  },
] as ColumnDef<INewsResponse>[];
