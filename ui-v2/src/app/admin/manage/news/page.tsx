"use client";

import { NewsActionDropdown } from "@/components/app/dropdown";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbPage,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { useNews, useNewsCategories } from "@/hooks";
import { IconNews } from "@tabler/icons-react";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function NewsManage(): ReactElement {
  const newsCategoriesQuery = useNewsCategories();
  const newsQuery = useNews();

  if (newsCategoriesQuery.error)
    return <div>{newsCategoriesQuery.error?.message}</div>;
  if (newsQuery.error) return <div>{newsQuery.error?.message}</div>;

  return (
    <div className='mt-4 mr-4 mb-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbPage className='flex items-center gap-1'>
              <IconNews className='w-4' />
              Quản lý bài viết
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {newsQuery.isLoading ? (
        <div className='my-4 flex flex-col gap-2'>
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className=''>
                <Skeleton className=' h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            );
          })}
        </div>
      ) : (
        <div className='flex flex-wrap items-start gap-3 mt-4'>
          {newsQuery.data?.data.length === 0 ? (
            <div className='text-center w-full font-bold'>
              Không có bài viết nào
            </div>
          ) : (
            <div className='w-full'>
              <DataTable
                columns={columns}
                actionButtons={<NewsActionDropdown />}
                data={newsQuery.data?.data || []}
                filterByColumn='id'
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
}
