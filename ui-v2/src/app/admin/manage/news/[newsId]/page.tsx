"use client";

import { UpdateNewsContentModal } from "@/components/app/modal";
import { Loading } from "@/components/shared/loading";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Skeleton,
} from "@/components/ui";
import { useNewsById } from "@/hooks";
import { IconNews } from "@tabler/icons-react";
import Link from "next/link";
import { ReactElement } from "react";

export default function NewsDetailManage({
  params,
}: {
  params: { newsId: string };
}): ReactElement {
  const newsItemQuery = useNewsById(params.newsId);
  console.log({ newsItemQuery: newsItemQuery.data });

  if (newsItemQuery.error)
    return (
      <div className='font-bold text-center w-full'>
        {newsItemQuery.error.message}
      </div>
    );
  return (
    <div className='m-4 ml-0'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href='/admin/manage/news'
                className='flex items-center gap-1'
              >
                <IconNews className='w-4' />
                Quản lý bài viết
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage>
              {newsItemQuery.isLoading ? (
                <Skeleton className=' h-4 w-[100px] rounded-xl bg-gray-200 dark:bg-slate-900' />
              ) : (
                newsItemQuery.data?.data.newsTitle
              )}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>

      <div className=''>
        <div className='flex gap-3'>
          {!newsItemQuery.data?.data ? (
            <Skeleton className=' h-8 w-[200px] rounded-xl bg-gray-200 dark:bg-slate-900 mt-2' />
          ) : (
            <UpdateNewsContentModal newsItem={newsItemQuery.data.data} />
          )}
        </div>
        {newsItemQuery.isLoading ? (
          <Skeleton className=' h-[600px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 mt-2' />
        ) : (
          <div
            className='mt-2 text-sm leading-6'
            dangerouslySetInnerHTML={{
              __html: newsItemQuery.data?.data.newsContentHtml || "",
            }}
          ></div>
        )}
      </div>
    </div>
  );
}
