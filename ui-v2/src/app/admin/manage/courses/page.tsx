"use client";

import { CoursesActionDropdown } from "@/components/app/dropdown";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbPage,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { useCourses } from "@/hooks";
import { BookAudioIcon } from "lucide-react";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function CourseManage(): ReactElement {
  const coursesQuery = useCourses();
  if (coursesQuery.error) return <div>{coursesQuery.error?.message}</div>;

  return (
    <div className='mt-4 mr-4 mb-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbPage className='flex items-center gap-1'>
              <BookAudioIcon className='w-4' />
              Quản lý khóa học
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {coursesQuery.isLoading ? (
        <div className='my-4 flex flex-col gap-2'>
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className=''>
                <Skeleton className=' h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            );
          })}
        </div>
      ) : (
        <div className='w-full mt-4'>
          <DataTable
            columns={columns}
            actionButtons={<CoursesActionDropdown />}
            data={coursesQuery.data?.data || []}
            filterByColumn='id'
          />
        </div>
      )}
    </div>
  );
}
