"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import { ICourseResponse } from "@/types";
import { CourseItemActionDropdown } from "@/components/app/dropdown";
import { cn, convertNumberToMoney } from "@/lib";
import { CourseNameCell } from "./course-name-cell";
import { IconCopy } from "@tabler/icons-react";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "courseImg",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      return (
        <div
          className="text-right font-medium w-[200px]"
          dangerouslySetInnerHTML={{ __html: row.getValue("courseImg") }}
        ></div>
      );
    },
  },
  {
    accessorKey: "courseName",
    header: () => <div className="text-center">Tên khóa học</div>,
    cell: CourseNameCell,
  },
  {
    accessorKey: "oldPrice",
    header: () => <div className="text-center">Số tiền</div>,
    cell: ({ row }) => {
      const oldPrice = Number.parseInt(row.getValue("oldPrice"));
      return (
        <div className="hover:text-purple-800 cursor-pointer">
          {convertNumberToMoney(oldPrice)}
        </div>
      );
    },
  },
  {
    accessorKey: "isDiscount",
    header: () => <div className="text-center">Trạng thái giảm giá</div>,
    cell: ({ row }) => {
      const isDiscount = row.getValue("isDiscount") as boolean;
      // console.lo;
      return (
        <div
          className={cn(
            "cursor-pointer px-3 border py-1 w-fit rounded text-white",
            isDiscount
              ? "border-green-600 bg-green-600"
              : "bg-yellow-500 border-yellow-500"
          )}
        >
          {isDiscount ? "Đang giảm giá" : "Không giảm giá"}
        </div>
      );
    },
  },
  {
    accessorKey: "percentDiscount",
    header: () => <div className="text-center">Giảm giá</div>,
    cell: ({ row }) => {
      const percentDiscount = row.getValue("percentDiscount")
        ? Number.parseInt(row.getValue("percentDiscount"))
        : 0;
      return (
        <div className="text-purple-800 cursor-pointer border bg-blue-100 w-fit px-2 rounded-full">
          {percentDiscount}%
        </div>
      );
    },
  },
  {
    accessorKey: "newPrice",
    header: () => <div className="text-center">Số tiền sau giảm giá</div>,
    cell: ({ row }) => {
      const newPrice = row.getValue("newPrice")
        ? Number.parseInt(row.getValue("newPrice"))
        : Number.parseInt(row.getValue("oldPrice"));
      return (
        <div className="hover:text-purple-800 cursor-pointer">
          {convertNumberToMoney(newPrice)}
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id");
      const course = { ...row._valuesCache, id } as unknown as ICourseResponse;
      return <CourseItemActionDropdown course={course} />;
    },
  },
] as ColumnDef<ICourseResponse>[];
