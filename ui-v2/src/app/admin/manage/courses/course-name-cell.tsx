import { ICourseResponse } from "@/types";
import { Row } from "@tanstack/react-table";
import React from "react";

export const CourseNameCell: React.FC<{ row: Row<ICourseResponse> }> = ({
  row,
}) => {
  return (
    <div className="font-bold w-[250px]">{row.getValue("courseName")}</div>
  );
};
