"use client";

import { ReactElement } from "react";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui";
import { UnitsDropdown } from "@/components/app/dropdown";
import { CreateLessonModal, CreateUnitModal } from "@/components/app/modal";
import { UpdateLessonForm } from "@/components/app/forms";
import { useSearchParams } from "next/navigation";
import { useLesson } from "@/hooks";

export default function UnitManage({
  params,
}: {
  params: { courseId: string };
}): ReactElement {
  const searchParams = useSearchParams();
  const lessonId = searchParams.get("lessonId");
  const lessonQuery = useLesson(lessonId || "");

  return (
    <div id="unit" className="my-4 mr-4">
      <div className="flex items-center gap-4">
        <UnitsDropdown courseId={params.courseId} />
        <CreateUnitModal courseId={params.courseId} />
        <CreateLessonModal courseId={params.courseId} />
      </div>
      <div className="mt-4">
        <Tabs defaultValue="account" className="w-full">
          <TabsList className="">
            <TabsTrigger value="account" className="text-black dark:text-white">
              Thông tin bài học
            </TabsTrigger>
          </TabsList>
          <TabsContent value="account">
            {!lessonId && (
              <div className="text-sm text-center h-screen">
                Vui lòng chọn bài học
              </div>
            )}
            {lessonQuery.data && <UpdateLessonForm lesson={lessonQuery.data} />}
          </TabsContent>
        </Tabs>
      </div>
    </div>
  );
}
