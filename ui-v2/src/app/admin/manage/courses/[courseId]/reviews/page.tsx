"use client";

import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useCourse, useReviews } from "@/hooks";
import { BookAudioIcon } from "lucide-react";
import Link from "next/link";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function ReviewManage({
  params,
}: {
  params: { courseId: string };
}): ReactElement {
  const reviewsQuery = useReviews();
  const courseQuery = useCourse(params.courseId);
  if (reviewsQuery.error) return <div>{reviewsQuery.error?.message}</div>;

  return (
    <div className="mt-4 mr-4 mb-4">
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href={ROUTES.ADMIN_COURSE_MANAGE}
                className="flex items-center gap-1"
              >
                <BookAudioIcon className="w-4" />
                Quản lý khóa học
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center" />
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              {courseQuery.data?.courseName}
            </BreadcrumbPage>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center" />
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              Đánh giá
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {reviewsQuery.isLoading ? (
        <div className="my-4 flex flex-col gap-2">
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className="">
                <Skeleton className=" h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
              </div>
            );
          })}
        </div>
      ) : (
        <div className="w-full mt-4">
          <DataTable
            columns={columns}
            actionButtons={<></>}
            data={reviewsQuery.data || []}
            filterByColumn="id"
          />
        </div>
      )}
    </div>
  );
}
