"use client";

import { ReviewItemActionDropdown } from "@/components/app/dropdown";
import { cn } from "@/lib";
import { IReviewResponse, IUserCredentialResponse } from "@/types";
import { IconStarFilled } from "@tabler/icons-react";
import { ColumnDef } from "@tanstack/react-table";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className='text-right'></div>,
    cell: ({ row }) => {
      return <div className='text-right font-medium'></div>;
    },
  },
  {
    accessorKey: "content",
    header: () => <div className='text-center'>Nội dung</div>,
    cell: ({ row }) => {
      return (
        <div
          className='w-[200px]'
          dangerouslySetInnerHTML={{ __html: row.getValue("content") }}
        ></div>
      );
    },
  },
  {
    accessorKey: "userCredential",
    header: () => <div className='text-center'>Người đánh giá</div>,
    cell: ({ row }) => {
      const userCredential = row.getValue(
        "userCredential"
      ) as IUserCredentialResponse;
      return (
        <div className='font-medium w-[250px]'>{userCredential?.username}</div>
      );
    },
  },
  {
    accessorKey: "isPublic",
    header: () => <div className='text-center'>Trạng thái</div>,
    cell: ({ row }) => {
      const isPublic = row.getValue("isPublic") as boolean;
      return (
        <div
          className={cn(
            "px-3 border py-1 w-fit rounded text-white",
            isPublic
              ? "border-green-600 bg-green-600"
              : "bg-yellow-500 border-yellow-500"
          )}
        >
          {isPublic ? "Được duyệt" : "Chưa được duyệt"}
        </div>
      );
    },
  },
  {
    accessorKey: "rating",
    header: () => <div className='text-center'>Đánh giá</div>,
    cell: ({ row }) => {
      const rating = Number.parseInt(row.getValue("rating"));
      return (
        <div className='text-purple-800 justify-center flex items-center gap-1'>
          {rating} <IconStarFilled className='w-4 h-4 text-yellow-500' />
        </div>
      );
    },
  },
  {
    accessorKey: "createdDate",
    header: () => <div className='text-center'>Ngày tạo</div>,
    cell: ({ row }) => {
      return (
        <div className='hover:text-purple-800 cursor-pointer'>
          {row.getValue("createdDate")}
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className='text-center'></div>,
    cell: ({ row }) => {
      const id = row.getValue("id");
      const review = { ...row._valuesCache, id } as unknown as IReviewResponse;
      return <ReviewItemActionDropdown review={review} />;
    },
  },
] as ColumnDef<IReviewResponse>[];
