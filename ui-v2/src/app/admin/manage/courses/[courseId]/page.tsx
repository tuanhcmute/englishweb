"use client";

import { UpdateCourseDetailModal } from "@/components/app/modal";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Separator,
  Skeleton,
} from "@/components/ui";
import { useCourseDetail } from "@/hooks";
import { BookAudioIcon } from "lucide-react";
import Link from "next/link";
import { ReactElement } from "react";

export default function CourseDetailManage({
  params,
}: {
  params: { courseId: string };
}): ReactElement {
  const courseDetailQuery = useCourseDetail(params.courseId);

  if (courseDetailQuery.error)
    return (
      <div className='font-bold text-center w-full'>
        {courseDetailQuery.error.message}
      </div>
    );
  return (
    <div className='m-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink className='flex items-center gap-1'>
              <Link
                href='/admin/manage/courses'
                className='flex items-center gap-1'
              >
                <BookAudioIcon className='w-4' />
                Quản lý khóa học
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage className='flex items-center gap-1'>
              {courseDetailQuery.data?.course.courseName}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      <div className='flex gap-3'>
        {courseDetailQuery.data && (
          <UpdateCourseDetailModal courseDetail={courseDetailQuery.data} />
        )}
      </div>
      <div className='mt-4'>
        <div className='flex flex-col gap-4'>
          <div className='w-full'>
            <p className='font-bold '>Giới thiệu khóa học</p>
            {courseDetailQuery?.isLoading ? (
              <div className=''>
                <Skeleton className=' h-[100px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            ) : (
              <p
                className='pl-4 text-sm'
                dangerouslySetInnerHTML={{
                  __html: courseDetailQuery.data?.courseIntroduction || "",
                }}
              ></p>
            )}
          </div>
          <Separator className='bg-gray-300' />
          <div>
            <p className='font-bold'>Thông tin khóa học</p>
            {courseDetailQuery?.isLoading ? (
              <div className=''>
                <Skeleton className=' h-[500px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            ) : (
              <p
                className='pl-4 text-sm'
                dangerouslySetInnerHTML={{
                  __html: courseDetailQuery.data?.courseInformation || "",
                }}
              ></p>
            )}
          </div>
          <Separator className='bg-gray-300' />
          <div>
            <p className='font-bold'>Cách sử dụng khóa học</p>
            {courseDetailQuery?.isLoading ? (
              <div className=''>
                <Skeleton className=' h-[400px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            ) : (
              <p
                className='text-sm pl-4'
                dangerouslySetInnerHTML={{
                  __html: courseDetailQuery.data?.courseGuide || "",
                }}
              ></p>
            )}
          </div>
          <Separator className='bg-gray-300' />
          <div>
            <p className='font-bold'>Đầu ra khóa học</p>
            {courseDetailQuery?.isLoading ? (
              <div className=''>
                <Skeleton className=' h-[300px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            ) : (
              <p
                className='text-sm pl-4'
                dangerouslySetInnerHTML={{
                  __html: courseDetailQuery.data?.courseTarget || "",
                }}
              ></p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
