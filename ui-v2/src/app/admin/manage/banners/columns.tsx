"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import { IBannerResponse } from "@/types";
import { BannerItemActionDropdown } from "@/components/app/dropdown";
import { IconCopy } from "@tabler/icons-react";
import { PageNameCell } from "./page-name-cell";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-center">Mã banner</div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "image",
    header: () => <div className="text-center">Hình ảnh</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div
            className="w-[300px]"
            dangerouslySetInnerHTML={{ __html: row.getValue("image") }}
          ></div>
        </div>
      );
    },
  },
  {
    accessorKey: "createdDate",
    header: () => <div className="text-center">Thời gian tạo</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="w-[150px] text-center">
            {row.getValue("createdDate")}
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "lastModifiedDate",
    header: () => <div className="text-center">Thời gian chỉnh sửa</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center">
          <div className="w-[200px] text-center">
            {row.getValue("lastModifiedDate")}
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const banner = row._valuesCache as unknown as IBannerResponse;
      return <BannerItemActionDropdown banner={banner} />;
    },
  },
] as ColumnDef<IBannerResponse>[];
