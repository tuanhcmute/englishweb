import { IStaticPageResponse } from "@/types"
import { Row } from "@tanstack/react-table"
import { useRouter } from "next/navigation"
import React from "react"

export const PageNameCell: React.FC<{ row: Row<IStaticPageResponse> }> = ({
  row,
}) => {
  const router = useRouter()
  return (
    <div
      onClick={() => router.push(`static-pages/${row.getValue("id")}`)}
      className="font-bold hover:text-purple-800 cursor-pointer w-[200px]"
    >
      {row.getValue("pageName")}
    </div>
  )
}
