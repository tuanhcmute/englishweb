"use client"

import React from "react"

import { ColumnDef } from "@tanstack/react-table"
import { ICourseCategoryResponse, INewsCategoryResponse } from "@/types"
import { NewsCategoriesItemActionDropdown } from "@/components/app/dropdown"
import { useRouter } from "next/navigation"
import { CourseCategoryNameCell } from "./course-category-name-cell"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      return <div className="text-right font-medium"></div>
    },
  },
  {
    accessorKey: "courseCategoryName",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: CourseCategoryNameCell,
  },
  {
    accessorKey: "createdDate",
    header: () => <div className="text-center">Thời gian tạo</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[200px] text-center">
          {row.getValue("createdDate")}
        </div>
      )
    },
  },
  {
    accessorKey: "lastModifiedDate",
    header: () => <div className="text-center">Thời gian chỉnh sửa</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[200px] text-center">
          {row.getValue("lastModifiedDate")}
        </div>
      )
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const category = row._valuesCache as unknown as INewsCategoryResponse
      const id = row.getValue("id") as string
      return <NewsCategoriesItemActionDropdown category={{ ...category, id }} />
    },
  },
] as ColumnDef<ICourseCategoryResponse>[]
