"use client";

import { CourseCategoriesActionDropdown } from "@/components/app/dropdown";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbPage,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { useCourseCategories } from "@/hooks";
import { IconNews } from "@tabler/icons-react";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function NewsCatgoriesManage(): ReactElement {
  const courseCategoriesQuery = useCourseCategories();

  if (courseCategoriesQuery.error)
    return <div>{courseCategoriesQuery.error?.message}</div>;

  return (
    <div className='mt-4 mr-4 mb-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbPage className='flex items-center gap-1'>
              <IconNews className='w-4' />
              Quản lý danh mục Khóa học
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {courseCategoriesQuery.isLoading ? (
        <div className='my-4 flex flex-col gap-2'>
          {Array.from({ length: 10 }).map((_, index) => {
            return (
              <div key={index} className=''>
                <Skeleton className=' h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            );
          })}
        </div>
      ) : (
        <div className='flex flex-wrap items-start gap-3 mt-4'>
          {courseCategoriesQuery.data?.data.length === 0 ? (
            <div className='text-center w-full font-bold'>Không có dữ liệu</div>
          ) : (
            <div className='w-full'>
              <DataTable
                columns={columns}
                actionButtons={<CourseCategoriesActionDropdown />}
                data={courseCategoriesQuery.data?.data || []}
                filterByColumn='id'
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
}
