import { ICourseCategoryResponse } from "@/types"
import { Row } from "@tanstack/react-table"
import { useRouter } from "next/navigation"

export const CourseCategoryNameCell: React.FC<{
  row: Row<ICourseCategoryResponse>
}> = ({ row }) => {
  const router = useRouter()
  return (
    <div
      onClick={() => router.push(`news/${row.getValue("id")}`)}
      className="font-bold hover:text-purple-800 cursor-pointer w-[300px]"
    >
      {row.getValue("courseCategoryName")}
    </div>
  )
}
