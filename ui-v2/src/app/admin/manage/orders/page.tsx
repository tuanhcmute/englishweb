"use client";

import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { useOrders } from "@/hooks";
import { IconShoppingCart } from "@tabler/icons-react";
import Link from "next/link";
import { ReactElement } from "react";
import { columns } from "./columns";

export default function OrderManage(): ReactElement {
  const ordersQuery = useOrders();
  if (ordersQuery.error) return <div>{ordersQuery.error?.message}</div>;

  return (
    <div className='mt-4 mr-4 mb-4'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href={"/admin/manage/courses"}
                className='flex items-center gap-1'
              >
                <IconShoppingCart className='w-4' />
                Quản lý đơn hàng
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {ordersQuery.isLoading ? (
        <div className='my-4 flex flex-col gap-2'>
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className=''>
                <Skeleton className=' h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
              </div>
            );
          })}
        </div>
      ) : (
        <div className='w-full mt-4'>
          <DataTable
            columns={columns}
            actionButtons={<></>}
            data={ordersQuery.data || []}
            filterByColumn='id'
          />
        </div>
      )}
    </div>
  );
}
