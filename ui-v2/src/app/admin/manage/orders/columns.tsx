"use client"

import { OrderItemActionDropdown } from "@/components/app/dropdown"
import { convertNumberToMoney } from "@/lib"
import {
  IInvoiceResponse,
  IOrderLineResponse,
  IOrderResponse,
  IUserCredentialResponse,
} from "@/types"
import { IconCopy } from "@tabler/icons-react"
import { ColumnDef } from "@tanstack/react-table"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right">Mã đơn hàng</div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string
      return (
        <div className="text-right flex items-center gap-1 justify-center">
          #{id?.substring(0, 6)}{" "}
          <IconCopy
            className="w-4 cursor-pointer hover:text-purple-800 transition-all ease-in-out"
            onClick={() => {
              navigator.clipboard.writeText(id)
            }}
          />
        </div>
      )
    },
  },
  {
    accessorKey: "orderDate",
    header: () => <div className="text-center">Thời gian tạo</div>,
    cell: ({ row }) => {
      return (
        <div className="hover:text-purple-800 w-[250px] text-center">
          {row.getValue("orderDate")}
        </div>
      )
    },
  },
  {
    accessorKey: "userCredential",
    header: () => <div className="text-center">Người đặt hàng</div>,
    cell: ({ row }) => {
      const userCredential = row.getValue(
        "userCredential"
      ) as IUserCredentialResponse
      return (
        <div className="hover:text-purple-800 cursor-pointer text-center flex items-center gap-1">
          <img
            src={userCredential?.avatar}
            className="w-8 h-8 object-cover rounded-full"
            alt={userCredential.avatar}
          />
          {userCredential?.username}
        </div>
      )
    },
  },
  {
    accessorKey: "orderLines",
    header: () => <div className="text-center">Tên khóa học</div>,
    cell: ({ row }) => {
      const orderLines = row.getValue("orderLines") as IOrderLineResponse[]
      return (
        <div className="hover:text-purple-800 cursor-pointer w-[200px]">
          {orderLines?.at(0)?.course.courseName}
        </div>
      )
    },
  },
  {
    accessorKey: "invoice",
    header: () => <div className="text-center">Tổng số tiền thanh toán</div>,
    cell: ({ row }) => {
      const invoice = row.getValue("invoice") as IInvoiceResponse
      const totalPrice = invoice?.invoiceLines?.at(0)?.totalPrice || 0

      return (
        <div className="font-medium">{convertNumberToMoney(totalPrice)}</div>
      )
    },
  },
  {
    accessorKey: "paymentStatus",
    header: () => <div className="text-center">Trạng thái</div>,
    cell: ({ row }) => {
      const renderUI = (status: boolean) => {
        switch (status) {
          case false:
            return (
              <div className="bg-yellow-500 text-white py-1 px-2 rounded">
                Chưa thanh toán
              </div>
            )
          case true:
            return (
              <div className="bg-green-600 text-white py-1 px-2 rounded ">
                Đã thanh toán
              </div>
            )
        }
      }
      return (
        <div className="flex justify-center min-w-[200px]">
          <div className="py-1 px-2">
            {renderUI(row.getValue("paymentStatus") as boolean)}
          </div>
        </div>
      )
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const invoice = row.getValue("invoice") as IInvoiceResponse
      const id = row.getValue("id") as string
      return <OrderItemActionDropdown invoice={invoice} orderId={id} />
    },
  },
] as ColumnDef<IOrderResponse>[]
