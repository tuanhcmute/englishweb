import { INewsCategoryResponse } from "@/types"
import { Row } from "@tanstack/react-table"
import { useRouter } from "next/navigation"
import React from "react"

export const NewsCategoryNameCell: React.FC<{
  row: Row<INewsCategoryResponse>
}> = ({ row }) => {
  const router = useRouter()
  return (
    <div
      onClick={() => router.push(`news/${row.getValue("id")}`)}
      className="font-bold hover:text-purple-800 cursor-pointer w-[300px]"
    >
      {row.getValue("newsCategoryName")}
    </div>
  )
}
