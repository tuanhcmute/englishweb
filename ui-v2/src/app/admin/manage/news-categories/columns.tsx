"use client"

import React from "react"

import { ColumnDef } from "@tanstack/react-table"
import { INewsCategoryResponse } from "@/types"
import { NewsCategoriesItemActionDropdown } from "@/components/app/dropdown"
import { NewsCategoryType } from "@/enums"
import { cn } from "@/lib"
import { NewsCategoryNameCell } from "./news-category-name-cell"

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      return <div className="text-right font-medium"></div>
    },
  },
  {
    accessorKey: "newsCategoryName",
    header: () => <div className="text-center">Tiêu đề</div>,
    cell: NewsCategoryNameCell,
  },
  {
    accessorKey: "createdDate",
    header: () => <div className="text-center">Thời gian tạo</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[200px] text-center">
          {row.getValue("createdDate")}
        </div>
      )
    },
  },
  {
    accessorKey: "lastModifiedDate",
    header: () => <div className="text-center">Thời gian chỉnh sửa</div>,
    cell: ({ row }) => {
      return (
        <div className="w-[200px] text-center">
          {row.getValue("lastModifiedDate")}
        </div>
      )
    },
  },
  {
    accessorKey: "status",
    header: () => <div className="text-right">Trạng thái</div>,
    cell: ({ row }) => {
      return (
        <div
          className={cn(
            "text-center px-2 rounded text-white border w-fit py-1",
            row.getValue("status") === NewsCategoryType.VISIBLE
              ? "bg-green-600"
              : "bg-yellow-500"
          )}
        >
          {row.getValue("status") === NewsCategoryType.VISIBLE
            ? "Hiển thị"
            : "Ẩn"}
        </div>
      )
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const category = row._valuesCache as unknown as INewsCategoryResponse
      const id = row.getValue("id") as string
      return <NewsCategoriesItemActionDropdown category={{ ...category, id }} />
    },
  },
] as ColumnDef<INewsCategoryResponse>[]
