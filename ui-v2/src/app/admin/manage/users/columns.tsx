"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import { IUserAuthorityResponse, IUserCredentialResponse } from "@/types";
import { UserItemActionDropdown } from "@/components/app/dropdown";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui";
import { Role } from "@/enums";
import { UsernameCell } from "./username-cell";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      return <div className="text-right font-medium"></div>;
    },
  },
  {
    accessorKey: "avatar",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const imageUrl = row.getValue("avatar") as string;
      return (
        <div className="text-right font-medium w-[100px]">
          <Avatar>
            <AvatarImage src={imageUrl} alt="@shadcn" />
            <AvatarFallback>DT</AvatarFallback>
          </Avatar>
        </div>
      );
    },
  },
  {
    accessorKey: "username",
    header: () => <div className="text-left">Tên đăng nhập</div>,
    cell: UsernameCell,
  },
  {
    accessorKey: "fullName",
    header: () => <div className="text-left">Tên hiển thị</div>,
    cell: ({ row }) => {
      return (
        <div className="hover:text-purple-800 cursor-pointer min-w-[200px]">
          {row.getValue("fullName")}
        </div>
      );
    },
  },
  {
    accessorKey: "provider",
    header: () => <div className="text-center">Phương thức đăng nhập</div>,
    cell: ({ row }) => {
      return (
        <div className="hover:text-purple-800 cursor-pointer text-center">
          {row.getValue("provider")}
        </div>
      );
    },
  },
  {
    accessorKey: "verified",
    header: () => <div className="text-center">Trạng thái xác thực</div>,
    cell: ({ row }) => {
      const verified = row.getValue("verified") as boolean;
      // console.log({ verified });
      return (
        <div className="hover:text-purple-800 cursor-pointer flex items-center justify-center min-w-[200px]">
          {verified ? (
            <div className="px-2 py-1 text-white bg-green-600 rounded w-fit">
              Đã xác nhận
            </div>
          ) : (
            <div className="px-2 py-1 text-white bg-yellow-500 rounded w-fit">
              Chưa xác nhận
            </div>
          )}
        </div>
      );
    },
  },
  {
    accessorKey: "userAuthorities",
    header: () => <div className="text-center">Quyền hạn</div>,
    cell: ({ row }) => {
      const userAuthorities = row.getValue(
        "userAuthorities"
      ) as IUserAuthorityResponse[];

      const renderRoleName = (roleName: string) => {
        switch (roleName) {
          case Role.ROLE_ADMIN:
            return "Quản trị viên";
          case Role.ROLE_TEACHER:
            return "Giáo viên";
          default:
            return "Học viên";
        }
      };
      return (
        <div className="hover:text-purple-800 cursor-pointer text-center min-w-[200px]">
          {userAuthorities.length > 0
            ? renderRoleName(userAuthorities[0]?.role.roleName)
            : renderRoleName("")}
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      const userAuthorities = row.getValue(
        "userAuthorities"
      ) as IUserAuthorityResponse[];
      // console.log(row.getValue("userAuthorities"));
      const user = {
        ...row._valuesCache,
        id,
        ...userAuthorities,
      } as unknown as IUserCredentialResponse;
      return <UserItemActionDropdown user={user} />;
    },
  },
] as ColumnDef<IUserCredentialResponse>[];
