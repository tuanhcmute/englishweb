"use client";
import { CreateUserCredentialModal } from "@/components/app/modal";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbPage,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { useUserCredentials } from "@/hooks";
import { User2Icon } from "lucide-react";
import { ReactElement } from "react";
import { columns } from "./columns";

interface IProps {}

export default function UserManage({}: IProps): ReactElement {
  const userCredentialsQuery = useUserCredentials();
  if (userCredentialsQuery.error)
    return (
      <div className="text-center h-screen">
        {userCredentialsQuery.error.message}
      </div>
    );

  return (
    <div className="mt-4 mr-4 mb-4">
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              <User2Icon className="w-4" />
              Quản lý người dùng
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {userCredentialsQuery.isLoading ? (
        <div className="my-4 flex flex-col gap-2">
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className="">
                <Skeleton className=" h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
              </div>
            );
          })}
        </div>
      ) : (
        <div className=" mt-4">
          <DataTable
            columns={columns}
            actionButtons={<CreateUserCredentialModal />}
            data={userCredentialsQuery.data?.data || []}
            filterByColumn="id"
          />
        </div>
      )}
    </div>
  );
}
