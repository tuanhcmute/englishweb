import { IUserCredentialResponse } from "@/types";
import { Row } from "@tanstack/react-table";
import React from "react";

export const UsernameCell: React.FC<{ row: Row<IUserCredentialResponse> }> = ({
  row,
}) => {
  return (
    <div className="font-bold cursor-pointer min-w-[100px]">
      {row.getValue("username")}
    </div>
  );
};
