"use client";

import { useComments, useTest } from "@/hooks";
import { ReactElement } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { StackIcon } from "@radix-ui/react-icons";
import Link from "next/link";
import { columns } from "./columns";

export default function CommentsManage({
  params,
}: {
  params: { testId: string };
}): ReactElement {
  // Call API
  const testQuery = useTest(params.testId);
  const commentsQuery = useComments({ testId: params.testId });

  if (testQuery.error)
    return (
      <div className="text-center h-screen">{testQuery.error.message}</div>
    );

  return (
    <div className="mt-4 mr-4 mb-4">
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href="/admin/manage/tests"
                className="flex items-center gap-1"
              >
                <StackIcon className="w-4" />
                Quản lý bài thi
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center" />
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              {testQuery.data?.testName}
            </BreadcrumbPage>
          </BreadcrumbItem>
          <BreadcrumbSeparator className="flex items-center" />
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              Quản lý bình luận
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {commentsQuery.isLoading ? (
        <div className="my-4 flex flex-col gap-2">
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className="">
                <Skeleton className=" h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
              </div>
            );
          })}
        </div>
      ) : (
        <div className="w-full mt-4">
          <DataTable
            columns={columns}
            actionButtons={<div></div>}
            data={commentsQuery.data?.data || []}
            filterByColumn="id"
          />
        </div>
      )}
    </div>
  );
}
