"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import {
  ICommentResponse,
  IScoreConversionResponse,
  ITestCategoryResponse,
  ITestResponse,
  IUserCredentialResponse,
} from "@/types";
import {
  CommentItemActionDropdown,
  TestItemActionDropdown,
} from "@/components/app/dropdown";
import { TestNameCell } from "./test-name-cell";
import { IconCopy } from "@tabler/icons-react";
import { cn } from "@/lib";
import { Item } from "@radix-ui/react-dropdown-menu";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "content",
    header: () => <div className="text-left">Nội dung</div>,
    cell: ({ row }) => {
      return <div className="w-[250px]">{row.getValue("content")}</div>;
    },
  },
  {
    accessorKey: "isNegative",
    header: () => <div className="text-center">Loại bình luận</div>,
    cell: ({ row }) => {
      const isNegative = row.getValue("isNegative") as boolean;
      return (
        <div className="flex justify-center">
          <div
            className={cn(
              "text-center  text-white px-2 py-1 rounded w-fit",
              !isNegative ? "bg-green-600" : "bg-red-500"
            )}
          >
            {!isNegative ? "Tích cực" : "Tiêu cực"}
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "author",
    header: () => <div className="text-center">Người bình luận</div>,
    cell: ({ row }) => {
      const author = row.getValue("author") as IUserCredentialResponse;
      return (
        <div className="flex min-w-[120px] gap-1 items-center">
          <img
            src={author?.avatar}
            alt="avatar"
            className="w-8 h-8 rounded-full"
          />
          {author?.username}
        </div>
      );
    },
  },
  {
    accessorKey: "countReportComments",
    header: () => <div className="text-center">Số lượt báo cáo</div>,
    cell: ({ row }) => {
      return (
        <div className="text-center flex justify-center min-w-[100px]">
          <div className="bg-red-500 text-white rounded py-1 px-2 w-fit">
            {row.getValue("countReportComments")}
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "isReview",
    header: () => <div className="text-center">Kiểm duyệt</div>,
    cell: ({ row }) => {
      const isReview = row.getValue("isReview") as boolean;
      return (
        <div className="text-center flex justify-center min-w-[100px]">
          <div
            className={cn(
              "text-white rounded py-1 px-2 w-fit",
              isReview ? "bg-green-500" : "bg-yellow-500"
            )}
          >
            {isReview ? "Đã kiểm duyệt" : "Chưa kiểm duyệt"}
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const comment = row._valuesCache as unknown as ICommentResponse;
      const id = row.getValue("id") as string;
      return <CommentItemActionDropdown comment={{ ...comment, id }} />;
    },
  },
] as ColumnDef<ICommentResponse>[];
