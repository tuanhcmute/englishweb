"use client";

import React, { ReactElement } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Card,
  CardDescription,
  CardHeader,
  CardTitle,
  Skeleton,
} from "@/components/ui";
import { usePartOfTest, useTest } from "@/hooks";
import { Loading } from "@/components/shared/loading";
import { UpdatePartOfTestModal } from "@/components/app/modal";
import { useRouter } from "next/navigation";
import { StackIcon } from "@radix-ui/react-icons";
import Link from "next/link";

export default function CreateTest({
  params,
}: {
  params: { testId: string };
}): ReactElement {
  const partOfTestQuery = usePartOfTest(params.testId);
  const testQuery = useTest(params.testId);
  const router = useRouter();

  return (
    <div className='m-4 ml-0'>
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href='/admin/manage/tests'
                className='flex items-center gap-1'
              >
                <StackIcon className='w-4' />
                Quản lý bài thi
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage className='flex items-center gap-1'>
              {testQuery.data?.testName}
            </BreadcrumbPage>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage className='flex items-center gap-1'>
              Danh sách phần thi
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      <div className='mt-5'>
        <div className='flex justify-end gap-3 mt-4'>
          <UpdatePartOfTestModal testId={params.testId} />
        </div>
        {/* List of part infor */}
        {partOfTestQuery.isLoading ? (
          <div className='my-4 flex flex-col gap-2'>
            {Array.from({ length: 10 }).map((_, index) => {
              return (
                <div key={index} className=''>
                  <Skeleton className=' h-[150px] w-full rounded-xl bg-gray-200 dark:bg-slate-900' />
                </div>
              );
            })}
          </div>
        ) : (
          <div className='flex items-start flex-wrap gap-4 my-5'>
            {partOfTestQuery.data?.data
              .sort((a, b) => a.partSequenceNumber - b.partSequenceNumber)
              .map((item) => {
                return (
                  <Card className='w-full border-gray-300' key={item.id}>
                    <CardHeader className='p-3'>
                      <CardTitle
                        className='text-lg hover:text-purple-800 cursor-pointer'
                        onClick={() => router.push(`parts/${item.id}`)}
                      >
                        {item.partInformation.partInformationName}
                      </CardTitle>
                      <CardDescription>
                        <p className='text-sm'>
                          {item.partInformation.partInformationDescription}
                        </p>
                        <p className='mt-2'>
                          <span className='font-extrabold'>
                            Số lượng câu hỏi:
                          </span>{" "}
                          {item.partInformation.totalQuestion} câu hỏi
                        </p>
                      </CardDescription>
                    </CardHeader>
                  </Card>
                );
              })}
          </div>
        )}
      </div>
    </div>
  );
}
