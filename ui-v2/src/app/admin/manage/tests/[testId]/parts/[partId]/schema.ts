import { z } from "zod";

export const testAnswerSchema = z.object({
  answerContent: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
  answerTranslate: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
  answerChoice: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
});

export const testAnswersSchema = z.array(testAnswerSchema);

export const questionSchema = z.object({
  questionName: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
  questionDesc: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
  rightAnswer: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
  questionGuide: z.string().trim().min(1, {
    message: "Phần thi là bắt buộc",
  }),
  testAnswers: z.array(testAnswerSchema),
});
