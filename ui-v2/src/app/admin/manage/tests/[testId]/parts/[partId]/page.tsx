"use client";

import {
  CreateQuestionGroupModal,
  CreateQuestionModal,
  RemoveQuestionGroupModal,
  RemoveQuestionModal,
  UpdateQuestionGroupModal,
  UpdateQuestionModal,
  UploadQuestionGroupAudioModal,
} from "@/components/app/modal";
// import {
//   Breadcrumb,
//   BreadcrumbArrow,
//   BreadcrumbItem,
// } from "@/components/shared/breadcrumb";
import { Loading } from "@/components/shared/loading";
import {
  Breadcrumb,
  BreadcrumbEllipsis,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui";
import { usePartOfTest, useQuestionGroups, useTest } from "@/hooks";
import { HomeIcon, StackIcon } from "@radix-ui/react-icons";
import moment from "moment";
import Link from "next/link";
import { ReactElement, ReactNode, useRef } from "react";
import ReactPlayer from "react-player";

function ReactPlayerWrapper({ children }: { children: ReactNode }) {
  return <div className='w-full h-[40px] mt-4'>{children}</div>;
}

export default function Part({
  params,
}: {
  params: { partId: string; testId: string };
}): ReactElement {
  // // Call API
  const { data, isLoading, error } = useQuestionGroups(params.partId);
  const testQuery = useTest(params.testId);
  const partOfTestQuery = usePartOfTest(params.testId);
  // const testQuery = useTest(params.testId);
  const playerRef = useRef<ReactPlayer | null>(null);

  if (isLoading) return <Loading />;
  if (error) return <div className='text-center h-screen'>{error.message}</div>;

  return (
    <div className='mt-4 mr-4'>
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link
                href='/admin/manage/tests'
                className='flex items-center gap-1'
              >
                <StackIcon />
                Quản lý bài thi
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <DropdownMenu>
              <DropdownMenuTrigger className='flex items-center gap-1'>
                <BreadcrumbEllipsis className='h-4 w-4' />
                <span className='sr-only'>Toggle menu</span>
              </DropdownMenuTrigger>
              <DropdownMenuContent align='start'>
                <DropdownMenuItem>
                  {testQuery.data?.testName || ""}
                </DropdownMenuItem>
                <DropdownMenuItem>Danh sách phần thi</DropdownMenuItem>
              </DropdownMenuContent>
            </DropdownMenu>
          </BreadcrumbItem>
          <BreadcrumbSeparator className='flex items-center' />
          <BreadcrumbItem>
            <BreadcrumbPage>
              {partOfTestQuery.data?.data.find(
                (item) => item.id === params.partId
              )?.partInformation.partInformationName || ""}
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      <div className='mt-4'>
        <div className='flex items-end gap-2'>
          <CreateQuestionGroupModal
            testId={params.testId}
            partId={params.partId}
          />
        </div>
        <div className='mt-4 flex flex-col gap-4'>
          {data?.data
            .sort((a, b) => {
              return (
                moment(a.createdDate, "DD-MM-YYYY HH:mm:ss")
                  .toDate()
                  .getTime() -
                moment(b.createdDate, "DD-MM-YYYY HH:mm:ss").toDate().getTime()
              );
            })
            .map((questionGroup) => {
              return (
                <div
                  key={questionGroup.id}
                  className='border rounded-md p-2 hover:shadow-xl transition-shadow border-gray-300'
                >
                  <div className=''>
                    <p className='font-bold inline'>
                      {questionGroup.questionContentEn}
                    </p>
                    <p className='text-sm inline'>
                      {" "}
                      ({questionGroup.questionContentTranscript}).
                    </p>
                  </div>
                  {/* Image */}
                  {questionGroup.image &&
                    questionGroup.image !== "<p>&nbsp;</p>" && (
                      <div
                        className='mt-4'
                        dangerouslySetInnerHTML={{
                          __html: questionGroup.image,
                        }}
                      ></div>
                    )}
                  {questionGroup.audio && (
                    <ReactPlayer
                      url={questionGroup.audio}
                      controls
                      height={50}
                      // playing={true}
                      ref={playerRef}
                      wrapper={ReactPlayerWrapper}
                    />
                  )}
                  <div className='flex items-center gap-3'>
                    <UpdateQuestionGroupModal
                      questionGroup={questionGroup}
                      testId={params.testId}
                    />
                    <UploadQuestionGroupAudioModal
                      questionGroupId={questionGroup.id}
                    />
                    <CreateQuestionModal questionGroupId={questionGroup.id} />
                    <RemoveQuestionGroupModal questionGroup={questionGroup} />
                  </div>
                  <div className='mt-4'>
                    <div className='grid grid-cols-1 gap-4'>
                      {questionGroup.questions
                        .sort((a, b) => a.questionNumber - b.questionNumber)
                        .map((question) => {
                          return (
                            <div
                              key={question.id}
                              className='border rounded-md p-2 border-gray-300'
                            >
                              <div className='flex gap-2 items-center'>
                                {/* Update question modal */}
                                <UpdateQuestionModal question={question} />
                                {/* Remove question modal */}
                                <RemoveQuestionModal question={question} />
                              </div>
                              <div className='mt-3'>
                                {/* Question content */}
                                <p className='flex gap-2 items-start'>
                                  <p className='rounded-full w-8 h-8 bg-blue-100 text-purple-800 font-bold text-[14px] flex items-center flex-col justify-center'>
                                    {question.questionNumber}
                                  </p>
                                  <p
                                    dangerouslySetInnerHTML={{
                                      __html: question.questionContent,
                                    }}
                                  ></p>
                                </p>
                                <div className='flex items-start gap-5'>
                                  {/* Answer */}
                                  <div className='grid grid-cols-1 gap-3'>
                                    {question.testAnswers
                                      .sort((a, b) =>
                                        a.answerChoice.localeCompare(
                                          b.answerChoice
                                        )
                                      )
                                      .map((ans) => {
                                        return (
                                          <div
                                            key={ans.id}
                                            className='pl-10 text-sm mt-2'
                                          >
                                            <p
                                              className={`flex gap-2 items-center w-fit ${
                                                question.rightAnswer === ans.id
                                                  ? "bg-yellow-400"
                                                  : ""
                                              }`}
                                            >
                                              <p className=''>
                                                ({ans.answerChoice})
                                              </p>
                                              <p>
                                                {ans.answerContent} (
                                                {ans.answerTranslate}){" "}
                                                {ans.id ===
                                                  question.rightAnswer &&
                                                  "=> Đáp án đúng"}
                                              </p>
                                            </p>
                                          </div>
                                        );
                                      })}
                                  </div>
                                </div>
                                {/* Question guide */}
                                <div className='mt-3 text-sm'>
                                  <p className='flex gap-1 items-start'>
                                    <span className='font-bold text-green-600'>
                                      Giải thích:
                                    </span>{" "}
                                    <p
                                      dangerouslySetInnerHTML={{
                                        __html: question.questionGuide,
                                      }}
                                    ></p>
                                  </p>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
}
