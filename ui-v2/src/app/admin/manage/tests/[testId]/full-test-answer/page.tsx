"use client";

import { Loading } from "@/components/shared/loading";
import { useFullTestAnswer, useTests } from "@/hooks";
import { ReactElement } from "react";
import { SyncFullTestAnswerModal } from "@/components/app/modal";
import {} from "@/components/ui";
import { HomeIcon } from "@radix-ui/react-icons";
import {
  Breadcrumb,
  BreadcrumbArrow,
  BreadcrumbItem,
} from "@/components/shared/breadcrumb";
import { parseJsonToObject } from "@/lib";
import { IResultChoice } from "@/types";

export default function FullTestAnswer({
  params,
}: {
  params: { testId: string };
}): ReactElement {
  // Call API
  const fullTestAnswerQuery = useFullTestAnswer(params.testId);

  if (fullTestAnswerQuery.isLoading) return <Loading />;
  if (fullTestAnswerQuery.error)
    return (
      <div className='text-center h-screen'>
        {fullTestAnswerQuery.error.message}
      </div>
    );

  const renderFullTestAnswer = () => {
    if (!fullTestAnswerQuery.data?.fullTestAnswerJson)
      return <div>Chưa có đáp án cho bài test này</div>;

    const fullTestAnswer = parseJsonToObject<IResultChoice>(
      fullTestAnswerQuery.data.fullTestAnswerJson
    );
    console.log({ fullTestAnswer });
  };

  return (
    <div className='mt-4 mr-4'>
      <Breadcrumb>
        <BreadcrumbItem
          title='Quản lý bài thi'
          icon={HomeIcon}
          className='hover:underline transition-all'
          href={`/admin/manage/tests`}
        />
        <BreadcrumbArrow />
        <BreadcrumbItem
          className='hover:underline transition-all'
          title={"Chi tiết đáp án bài thi"}
          isCurrent
        />
      </Breadcrumb>
      <div className='mt-4'>
        <div className='flex justify-end gap-2'>
          <SyncFullTestAnswerModal testId={params.testId} />
        </div>
        <div className='mt-4'>{renderFullTestAnswer()}</div>
      </div>
    </div>
  );
}
