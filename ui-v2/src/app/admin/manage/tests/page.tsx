"use client";

import { useTests } from "@/hooks";
import { ReactElement } from "react";
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbList,
  BreadcrumbPage,
  DataTable,
  Skeleton,
} from "@/components/ui";
import { TestsActionDropdown } from "@/components/app/dropdown";
import { columns } from "./columns";
import { StackIcon } from "@radix-ui/react-icons";

export default function TestManage(): ReactElement {
  // Call API
  const { data, isLoading, error } = useTests();

  if (error) return <div className="text-center h-screen">{error.message}</div>;

  return (
    <div className="mt-4 mr-4 mb-4">
      {/* Path */}
      <Breadcrumb>
        <BreadcrumbList>
          <BreadcrumbItem>
            <BreadcrumbPage className="flex items-center gap-1">
              <StackIcon className="w-4" />
              Quản lý bài thi
            </BreadcrumbPage>
          </BreadcrumbItem>
        </BreadcrumbList>
      </Breadcrumb>
      {isLoading ? (
        <div className="my-4 flex flex-col gap-2">
          {Array.from({ length: 15 }).map((_, index) => {
            return (
              <div key={index} className="">
                <Skeleton className=" h-[50px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
              </div>
            );
          })}
        </div>
      ) : (
        <div className="w-full mt-4">
          <DataTable
            columns={columns}
            actionButtons={<TestsActionDropdown />}
            data={data?.data || []}
            filterByColumn="id"
          />
        </div>
      )}
    </div>
  );
}
