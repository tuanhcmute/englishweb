"use client";

import React from "react";

import { ColumnDef } from "@tanstack/react-table";
import {
  IScoreConversionResponse,
  ITestCategoryResponse,
  ITestResponse,
} from "@/types";
import { TestItemActionDropdown } from "@/components/app/dropdown";
import { TestNameCell } from "./test-name-cell";
import { IconCopy } from "@tabler/icons-react";

export const columns = [
  {
    accessorKey: "id",
    header: () => <div className="text-right"></div>,
    cell: ({ row }) => {
      const id = row.getValue("id") as string;
      return (
        <div className="flex justify-center">
          <div className="flex items-center gap-1 ">
            #{id?.substring(0, 6)}{" "}
            <IconCopy
              className="w-4 hover:text-purple-800 transition-all ease-linear cursor-pointer"
              onClick={(e) => navigator.clipboard.writeText(id)}
            />
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "thumbnail",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const imageUrl = row.getValue("thumbnail") as string;
      return (
        <div
          className="text-right font-medium w-[200px]"
          dangerouslySetInnerHTML={{ __html: imageUrl }}
        ></div>
      );
    },
  },
  {
    accessorKey: "testName",
    header: () => <div className="text-left">Tên bài thi</div>,
    cell: TestNameCell,
  },
  {
    accessorKey: "numberOfPart",
    header: () => <div className="text-center">Số lượng phần thi</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center min-w-[120px]">
          <div className="text-center bg-green-600 text-white px-2 py-1 rounded w-fit">
            {row.getValue("numberOfPart")} phần
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "numberOfQuestion",
    header: () => <div className="text-center">Số lượng câu hỏi</div>,
    cell: ({ row }) => {
      return (
        <div className="flex justify-center min-w-[120px]">
          <div className="text-center bg-yellow-500 text-white px-2 py-1 rounded w-fit">
            {row.getValue("numberOfQuestion")} câu hỏi
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "testCategory",
    header: () => <div className="text-center">Danh mục</div>,
    cell: ({ row }) => {
      const testCategory = row.getValue(
        "testCategory"
      ) as ITestCategoryResponse;
      return (
        <div className="hover:text-purple-800 cursor-pointer text-center flex justify-center min-w-[100px]">
          <div className="bg-purple-800 text-white rounded py-1 px-2 w-fit">
            {testCategory?.testCategoryName}
          </div>
        </div>
      );
    },
  },
  {
    accessorKey: "scoreConversion",
    header: () => <div className="text-center">Danh mục</div>,
    cell: ({ row }) => {
      return null;
    },
  },
  {
    accessorKey: "countUserAnswers",
    header: () => <div className="text-center">Lượt thực hiện</div>,
    cell: ({ row }) => {
      return (
        <div className="text-center">
          {row.getValue("countUserAnswers")} lượt
        </div>
      );
    },
  },
  {
    accessorKey: "actions",
    header: () => <div className="text-center"></div>,
    cell: ({ row }) => {
      const test = row._valuesCache as unknown as ITestResponse;
      const id = row.getValue("id") as string;
      const scoreConversion = row.getValue(
        "scoreConversion"
      ) as IScoreConversionResponse;
      return <TestItemActionDropdown test={{ ...test, id, scoreConversion }} />;
    },
  },
] as ColumnDef<ITestResponse>[];
