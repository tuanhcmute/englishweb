import { ROUTES } from "@/constants"
import { ITestResponse } from "@/types"
import { Row } from "@tanstack/react-table"
import { useRouter } from "next/navigation"
import React from "react"

export const TestNameCell: React.FC<{ row: Row<ITestResponse> }> = ({
  row,
}) => {
  const router = useRouter()
  return (
    <div
      onClick={() => router.push(`tests/${row.getValue("id")}/${ROUTES.PARTS}`)}
      className="font-bold hover:text-purple-800 cursor-pointer min-w-[150px]"
    >
      {row.getValue("testName")}
    </div>
  )
}
