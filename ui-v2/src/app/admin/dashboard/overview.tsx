"use client";

import { Tooltip, TooltipContent, TooltipTrigger } from "@/components/ui";
import { useDashboard } from "@/hooks";
import {
  IconNews,
  IconNote,
  IconShoppingCart,
  IconSwitchHorizontal,
  IconUser,
} from "@tabler/icons-react";
import { BookIcon, MoreVerticalIcon } from "lucide-react";
import _ from "lodash";
import Link from "next/link";
import { ROUTES } from "@/constants";

export const Overview = () => {
  const dashboardQuery = useDashboard();

  return (
    <div className="grid gap-4 py-2">
      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-5 gap-4">
        <div className="bg-white rounded-lg py-4 px-6 shadow-lg transition-all hover:shadow-xl">
          <div className="flex justify-end">
            <Tooltip>
              <TooltipTrigger asChild>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </TooltipTrigger>
              <TooltipContent>
                <Link href={ROUTES.ADMIN_COURSE_MANAGE}>Xem chi tiết</Link>
              </TooltipContent>
            </Tooltip>
          </div>
          <div className="w-12 h-12 rounded-md bg-blue-100 flex items-center flex-col justify-center text-purple-800 mt-2">
            <BookIcon className="w-6 h-6 " />
          </div>
          <div className="text-sm mt-2 text-gray-400">Khóa học</div>
          <div className="mt-1 font-bold text-xl text-gray-600">
            {dashboardQuery.data?.data.courseDashboard.totalCourses}{" "}
            <span className="text-sm font-normal">(khóa học)</span>
          </div>
        </div>

        <div className="bg-white rounded-lg py-4 px-6 shadow-lg transition-all hover:shadow-xl">
          <div className="flex justify-end">
            <Tooltip>
              <TooltipTrigger asChild>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </TooltipTrigger>
              <TooltipContent>
                <Link href={ROUTES.ADMIN_ORDER_MANAGE}>Xem chi tiết</Link>
              </TooltipContent>
            </Tooltip>
          </div>
          <div className="w-12 h-12 rounded-md bg-blue-100 flex items-center flex-col justify-center text-purple-800 mt-2">
            <IconShoppingCart className="w-6 h-6 " />
          </div>
          <div className="text-sm mt-2 text-gray-400">Đơn hàng</div>
          <div className="mt-1 font-bold text-xl text-gray-600">
            {dashboardQuery.data?.data.orderDashboard.totalOrders}{" "}
            <span className="text-sm font-normal">(đơn hàng)</span>
          </div>
        </div>

        <div className="bg-white rounded-lg py-4 px-6 shadow-lg transition-all hover:shadow-xl">
          <div className="flex justify-end">
            <Tooltip>
              <TooltipTrigger asChild>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </TooltipTrigger>
              <TooltipContent>
                <Link href={ROUTES.ADMIN_TEST_MANAGE}>Xem chi tiết</Link>
              </TooltipContent>
            </Tooltip>
          </div>
          <div className="w-12 h-12 rounded-md bg-blue-100 flex items-center flex-col justify-center text-purple-800 mt-2">
            <IconNote className="w-6 h-6 " />
          </div>
          <div className="text-sm mt-2 text-gray-400">Bài thi</div>
          <div className="mt-1 font-bold text-xl text-gray-600">
            {dashboardQuery.data?.data.testDashboard.totalTests}{" "}
            <span className="text-sm font-normal">(bài thi)</span>
          </div>
        </div>

        <div className="bg-white rounded-lg py-4 px-6 shadow-lg transition-all hover:shadow-xl">
          <div className="flex justify-end">
            <Tooltip>
              <TooltipTrigger asChild>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </TooltipTrigger>
              <TooltipContent>
                <Link href={ROUTES.ADMIN_FLASHCARD_MANAGE}>Xem chi tiết</Link>
              </TooltipContent>
            </Tooltip>
          </div>
          <div className="w-12 h-12 rounded-md bg-blue-100 flex items-center flex-col justify-center text-purple-800 mt-2">
            <IconSwitchHorizontal className="w-6 h-6 " />
          </div>
          <div className="text-sm mt-2 text-gray-400">Flashcard</div>
          <div className="mt-1 font-bold text-xl text-gray-600">
            {dashboardQuery.data?.data.flashcardDashboard.totalFlashcards}{" "}
            <span className="text-sm font-normal">(flashcards)</span>
          </div>
        </div>

        <div className="bg-white rounded-lg py-4 px-6 shadow-lg transition-all hover:shadow-xl">
          <div className="flex justify-end">
            <Tooltip>
              <TooltipTrigger asChild>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </TooltipTrigger>
              <TooltipContent>
                <Link href={ROUTES.ADMIN_NEWS_MANAGE}>Xem chi tiết</Link>
              </TooltipContent>
            </Tooltip>
          </div>
          <div className="w-12 h-12 rounded-md bg-blue-100 flex items-center flex-col justify-center text-purple-800 mt-2">
            <IconNews className="w-6 h-6 " />
          </div>
          <div className="text-sm mt-2 text-gray-400">Bài viết</div>
          <div className="mt-1 font-bold text-xl text-gray-600">
            {dashboardQuery.data?.data.newsDashboard.totalNews}{" "}
            <span className="text-sm font-normal">(bài viết)</span>
          </div>
        </div>

        <div className="bg-white rounded-lg py-4 px-6 shadow-lg transition-all hover:shadow-xl">
          <div className="flex justify-end">
            <Tooltip>
              <TooltipTrigger asChild>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </TooltipTrigger>
              <TooltipContent>
                <Link href={ROUTES.ADMIN_USER_MANAGE}>Xem chi tiết</Link>
              </TooltipContent>
            </Tooltip>
          </div>
          <div className="w-12 h-12 rounded-md bg-blue-100 flex items-center flex-col justify-center text-purple-800 mt-2">
            <IconUser className="w-6 h-6 " />
          </div>
          <div className="text-sm mt-2 text-gray-400">Người dùng</div>
          <div className="mt-1 font-bold text-xl text-gray-600">
            {dashboardQuery.data?.data.userDashboard.totalUser}{" "}
            <span className="text-sm font-normal">(Người dùng)</span>
          </div>
        </div>
      </div>
    </div>
  );
};
