"use client";

import { MyLineChart, MyPieChart } from "@/components/app/chart";
import {
  Separator,
  Tooltip,
  TooltipContent,
  TooltipTrigger,
  Popover,
  PopoverContent,
  PopoverTrigger,
  Calendar,
  Button,
} from "@/components/ui";
import { USER_CREDENTIAL_ID } from "@/constants";
import { useDashboard, useRevenues, useUserCredential } from "@/hooks";
import { IconArrowUp, IconShoppingCart, IconStack } from "@tabler/icons-react";
import { getCookie } from "cookies-next";
import { MoreVerticalIcon, SigmaIcon } from "lucide-react";
import React, { ReactElement } from "react";
import _ from "lodash";
import { Overview } from "./overview";
import { cn, convertNumberToMoney } from "@/lib";
import { DateRange } from "react-day-picker";
import { format, sub } from "date-fns";
import { CalendarIcon } from "@radix-ui/react-icons";
const moment = require("moment");

export default function DashboardPage(): ReactElement {
  const [date, setDate] = React.useState<DateRange | undefined>({
    from: sub(new Date(), { weeks: 1 }),
    to: new Date(),
  });
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const userCredentialQuery = useUserCredential(userCredentialId);
  const dashboardQuery = useDashboard();
  const revenueQuery = useRevenues({ page: 0, limit: 10, sort: "DESC" });
  const revenueAnalysistQuery = useRevenues({
    page: 0,
    limit: 100,
    from: moment(date?.from).format("YYYY-MM-DD"),
    to: moment(date?.to).format("YYYY-MM-DD"),
  });

  const getTotalRevenue = () => {
    if (!revenueQuery.data?.data) return 0;
    return revenueQuery.data.data.reduce((previousValue, currentValue) => {
      return previousValue + currentValue.totalAmount;
    }, 0);
  };

  return (
    <div id="dashboard">
      <div className="mx-4 lg:ml-0">
        {/* Hello */}
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-1 gap-4">
            <div className="flex items-start justify-between rounded-md p-4 bg-white shadow-lg hover:shadow-xl transition-all lg:basis-4/6">
              <div className="mt-10">
                <p className="text-2xl font-semibold text-purple-800 text-wrap">
                  Xin chào, {userCredentialQuery.data?.data.fullName}🎉
                </p>
                <p className="mt-8 text-sm dark:text-secondary">
                  Hôm nay đã bán được thêm {0} khóa học 🤩. Kiểm tra quản lý đơn
                  hàng.
                </p>
              </div>
              <div>
                {/* <img
                  className="w-56"
                  src="https://sneat-vuetify-admin-template.vercel.app/assets/illustration-john-light-0061869a.png"
                  alt="https://sneat-vuetify-admin-template.vercel.app/assets/illustration-john-light-0061869a.png"
                /> */}
              </div>
            </div>
          </div>
        </div>
        {/* Overview */}
        <Overview />
        {/* User analysis */}
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-1 gap-4 lg:grid-cols-3">
            <div className="p-4 bg-white rounded-md shadow-lg hover:shadow-xl transition-all lg:col-span-2">
              <div className="flex items-center justify-between">
                <p className="text-xl font-medium text-gray-500">
                  Bảng thống kê
                </p>
                <div className="rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <div className={cn("grid gap-2")}>
                    <Popover>
                      <PopoverTrigger asChild>
                        <Button
                          id="date"
                          variant={"outline"}
                          className={cn(
                            "w-fit justify-start text-left font-normal",
                            !date && "text-muted-foreground"
                          )}
                        >
                          <CalendarIcon className="mr-2 h-4 w-4" />
                          {date?.from ? (
                            date.to ? (
                              <>
                                {format(date.from, "LLL dd, y")} -{" "}
                                {format(date.to, "LLL dd, y")}
                              </>
                            ) : (
                              format(date.from, "LLL dd, y")
                            )
                          ) : (
                            <span>Pick a date</span>
                          )}
                        </Button>
                      </PopoverTrigger>
                      <PopoverContent className="w-auto p-0" align="start">
                        <Calendar
                          initialFocus
                          mode="range"
                          defaultMonth={date?.from}
                          selected={date}
                          onSelect={setDate}
                          numberOfMonths={2}
                        />
                      </PopoverContent>
                    </Popover>
                  </div>
                </div>
              </div>
              <div className="mt-2">
                <MyLineChart
                  revenues={revenueAnalysistQuery.data?.data || []}
                />
              </div>
            </div>
            <div className="">
              <div className="bg-white rounded-lg py-4 px-6 shadow-lg transition-all hover:shadow-xl">
                <div className="flex justify-between items-center">
                  <div className="text-xl mt-4 text-gray-500 font-medium">
                    Thông kê doanh thu
                  </div>
                  <Tooltip>
                    <TooltipTrigger asChild>
                      <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                        <MoreVerticalIcon className="w-4 h-4" />
                      </div>
                    </TooltipTrigger>
                    <TooltipContent>
                      <p>Xem chi tiết</p>
                    </TooltipContent>
                  </Tooltip>
                </div>
                <div className="flex items-center justify-center py-5 text-3xl font-bold text-blue-500">
                  {convertNumberToMoney(getTotalRevenue())}
                </div>
                <div className="mt-4">
                  {revenueQuery.data?.data.map((item) => {
                    return (
                      <div
                        className="flex items-center justify-between"
                        key={item.id}
                      >
                        <div className="flex items-start gap-2">
                          <div className="w-12 h-12 rounded-md bg-blue-100 flex items-center flex-col justify-center text-purple-800 mt-2">
                            <IconArrowUp className="w-6 h-6 " />
                          </div>
                          <div className="flex flex-col gap-1">
                            <div className="mt-1 text-sm text-gray-400">
                              {item.createdDate}
                            </div>
                            <p className="text-yellow-500 flex items-center gap-1 text-sm">
                              Tổng số
                            </p>
                          </div>
                        </div>
                        <div className="flex items-center gap-1 text-sm text-gray-500">
                          <SigmaIcon /> {convertNumberToMoney(item.totalAmount)}{" "}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Orders */}
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-1 lg:grid-cols-3 lg:gap-4">
            <div className="p-4 bg-white rounded-md shadow-lg hover:shadow-xl transition-all mb-4 lg:mb-0">
              <div className="flex items-center justify-between">
                <p className="text-xl font-medium text-gray-600">
                  Thống kê đơn hàng
                </p>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </div>
              <div className="mt-2">
                <div className="flex items-center justify-between">
                  <div className="w-24">
                    <MyPieChart />
                  </div>
                  <div className="flex flex-col items-center">
                    <p className="font-semibold text-2xl text-gray-600">
                      {dashboardQuery.data?.data.orderDashboard.totalOrders}
                    </p>
                    <p className="text-gray-400">Tổng đơn hàng</p>
                  </div>
                </div>
                <Separator className="mt-4" />
                <div className="flex items-center justify-between mt-1">
                  <div className="flex items-center gap-1">
                    <div className="w-12 h-12 rounded-md bg-green-100 flex items-center flex-col justify-center text-yellow-800 mt-2">
                      <IconShoppingCart className="w-6 h-6 " />
                    </div>
                    <p className="text-gray-500 text-sm w-[150px]">
                      Combo khoá học IELTS Intensive [Tặng khoá TED Talks]
                    </p>
                  </div>
                  <p className="font-semibold text-gray-600">500 đơn hàng</p>
                </div>
              </div>
            </div>
            <div className="basis-2/3 p-4 bg-white rounded-md shadow-lg hover:shadow-xl transition-all col-span-2">
              <div className="flex items-center justify-between">
                <p className="text-xl font-medium text-gray-500">
                  Biểu đồ đơn hàng
                </p>
                <div className="w-8 h-8 rounded-md bg-green-100 hover:bg-green-200 cursor-pointer flex items-center flex-col justify-center">
                  <MoreVerticalIcon className="w-4 h-4" />
                </div>
              </div>
              <div className="mt-2">
                <MyLineChart
                  revenues={revenueAnalysistQuery.data?.data || []}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
