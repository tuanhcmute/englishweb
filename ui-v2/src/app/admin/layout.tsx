import { AdminLayout } from "@/components/shared/layout";
import { ReactElement, ReactNode } from "react";

export default function Layout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return <AdminLayout>{children}</AdminLayout>;
}
