"use client";

import {
  Alert,
  AlertDescription,
  AlertTitle,
  Avatar,
  AvatarFallback,
  AvatarImage,
} from "@/components/ui";
import React from "react";
import { useCourse, useReviewsByCourse } from "@/hooks";
import { CreateReviewModal } from "@/components/app/modal";
import { IconStarFilled } from "@tabler/icons-react";

export const Review: React.FC<{ courseId: string }> = ({ courseId }) => {
  const courseQuery = useCourse(courseId);
  const reviewsQuery = useReviewsByCourse(courseId);

  if (courseQuery.error)
    return (
      <div className='text-center w-full font-bold text-red-500'>
        {courseQuery.error.message}
      </div>
    );

  if (reviewsQuery.error)
    return (
      <div className='text-center w-full font-bold text-red-500'>
        {reviewsQuery.error.message}
      </div>
    );

  return (
    <div className='bg-white p-4 dark:bg-transparent'>
      <div id='course-reviews' className='text-2xl mt-5 font-bold'>
        #Đánh giá
      </div>
      <div>
        {/* Input */}
        <CreateReviewModal courseId={courseId} />
        {/* Comments */}
        <div className='flex flex-col gap-2 mt-4'>
          {reviewsQuery.data?.map((item) => {
            return (
              <div key={item.id} className='text-sm'>
                <Alert className='w-full flex items-start gap-2 bg-transparent p-0 border-none'>
                  <Avatar>
                    <AvatarImage
                      src={item.userCredential.avatar}
                      alt='@shadcn'
                      className='border border-gray-300'
                    />
                    <AvatarFallback className='border border-gray-300'>
                      CN
                    </AvatarFallback>
                  </Avatar>
                  <div className='w-full'>
                    <AlertTitle className='flex items-end gap-1 text-purple-800'>
                      <p className='font-bold'>
                        {item.userCredential.fullName},
                      </p>
                      <p className='italic text-[12px]'>{item.createdDate}</p>
                    </AlertTitle>
                    <AlertDescription>
                      <div className='flex items-center gap-1'>
                        {Array.from({ length: item.rating }).map((_, index) => {
                          return (
                            <IconStarFilled
                              key={index}
                              className='text-yellow-500 cursor-pointer w-6 h-6'
                            />
                          );
                        })}
                      </div>
                      <p
                        dangerouslySetInnerHTML={{ __html: item.content }}
                        className='text-sm'
                      ></p>
                      <p
                        dangerouslySetInnerHTML={{ __html: item.image }}
                        className='text-sm'
                      ></p>
                    </AlertDescription>
                  </div>
                </Alert>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
