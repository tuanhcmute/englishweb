import { ReactElement, ReactNode } from "react";
import { Metadata, ResolvingMetadata } from "next";
import { cookies } from "next/headers";
import { ACCESS_TOKEN_KEY, REFRESH_TOKEN_KEY } from "@/constants";
import { IBaseResponse, ICourseResponse } from "@/types";
import { httpServer } from "@/lib";

export async function generateMetadata(
  { params }: { params: { courseId: string } },
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const cookieStore = cookies();
  const accessToken = cookieStore.get(ACCESS_TOKEN_KEY)?.value;
  const refreshToken = cookieStore.get(REFRESH_TOKEN_KEY)?.value;
  // fetch data
  const res: IBaseResponse<{ data: ICourseResponse }> = await httpServer(
    {
      accessToken: accessToken as string,
      refreshToken: refreshToken as string,
    },
    `/course/${params.courseId}`
  );

  return {
    title: `${res.data.data.courseName} - English Academy`,
    description: `${res.data.data.courseName} - English Academy`,
  };
}
export default function Layout({
  children,
}: {
  children: ReactNode;
}): ReactElement {
  return <>{children}</>;
}
