import { z } from "zod";

export const formReviewSchema = z.object({
  content: z.string().trim().min(1, {
    message: "Bình luận không được bỏ trống",
  }),
  courseId: z.string().trim().min(1, {
    message: "Mã khóa học là bắt buộc",
  }),
  authorId: z.string().trim().min(1, {
    message: "Mã người dùng là bắt buộc",
  }),
  rating: z.number().min(1).max(5),
});
