"use client";

import { SelectPaymentModal } from "@/components/app/modal";
import {
  Badge,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Button,
  Skeleton,
} from "@/components/ui";
import { ROUTES, USER_CREDENTIAL_ID } from "@/constants";
import { useCourseDetail, useOrders } from "@/hooks";
import { convertNumberToMoney } from "@/lib";
import { HomeIcon, LaptopIcon, TimerIcon } from "@radix-ui/react-icons";
import { getCookie } from "cookies-next";
import { BookIcon, User2Icon } from "lucide-react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { ReactElement } from "react";
import { Review } from "./review";

export default function CourseDetailPage({
  params,
}: {
  params: { courseId: string };
}): ReactElement {
  const router = useRouter();
  const courseDetailQuery = useCourseDetail(params.courseId);
  const userCredentialId = getCookie(USER_CREDENTIAL_ID) as string;
  const ordersByUserCredentialQuery = useOrders(userCredentialId);

  if (courseDetailQuery.error)
    return (
      <div className="text-center font-bold text-red-500 w-full">
        {courseDetailQuery.error?.message}
      </div>
    );

  if (ordersByUserCredentialQuery.error)
    return (
      <div className="text-center font-bold text-red-500 w-full">
        {ordersByUserCredentialQuery.error?.message}
      </div>
    );

  return (
    <section id="course-detail" className="py-10 bg-gray-100 dark:bg-slate-950">
      <div className="container flex items-start gap-5 flex-col md:flex-row">
        <div className="w-full border rounded-md bg-white dark:bg-slate-900">
          {/* Path */}
          <div className="pt-4 pl-2">
            <Breadcrumb>
              <BreadcrumbList>
                <BreadcrumbItem>
                  <BreadcrumbLink>
                    <Link
                      href={ROUTES.HOME}
                      className="flex items-center gap-1"
                    >
                      <HomeIcon className="w-4" />
                      Trang chủ
                    </Link>
                  </BreadcrumbLink>
                </BreadcrumbItem>
                <BreadcrumbSeparator className="flex items-center" />
                <BreadcrumbItem>
                  <BreadcrumbLink>
                    <Link
                      href={ROUTES.COURSES}
                      className="flex items-center gap-1"
                    >
                      Danh sách khóa học
                    </Link>
                  </BreadcrumbLink>
                  <BreadcrumbPage className="flex items-center gap-1"></BreadcrumbPage>
                </BreadcrumbItem>
                <BreadcrumbSeparator className="flex items-center" />
                <BreadcrumbItem>
                  <BreadcrumbPage className="flex items-center gap-1">
                    {courseDetailQuery.isLoading ? (
                      <Skeleton className="h-[30px] w-96 rounded-xl bg-gray-200 dark:bg-slate-900" />
                    ) : (
                      courseDetailQuery.data?.course.courseName
                    )}
                  </BreadcrumbPage>
                </BreadcrumbItem>
              </BreadcrumbList>
            </Breadcrumb>
          </div>
          <div className="p-2 mt-5">
            {courseDetailQuery.isLoading ? (
              <Skeleton className="h-[30px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
            ) : (
              <p className="font-bold text-xl">
                {courseDetailQuery.data?.course.courseName}
              </p>
            )}
            <div className="flex flex-col gap-1 mt-2">
              {courseDetailQuery.isLoading ? (
                <Skeleton className="h-[200px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
              ) : (
                <div
                  className="text-sm px-4 flex flex-col gap-2"
                  dangerouslySetInnerHTML={{
                    __html: courseDetailQuery.data?.courseIntroduction || "",
                  }}
                ></div>
              )}
            </div>
          </div>
          <div id="course-objectives" className="p-2">
            <p className="font-bold text-lg">
              Bạn sẽ đạt được gì sau khoá học?
            </p>
            {courseDetailQuery.isLoading ? (
              <Skeleton className="h-[300px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 mt-2" />
            ) : (
              <div
                className="mt-2 text-sm"
                dangerouslySetInnerHTML={{
                  __html: courseDetailQuery.data?.courseTarget || "",
                }}
              ></div>
            )}
          </div>
          <div id="course-information" className="p-2">
            <p className="font-bold text-lg">Thông tin khoá học</p>
            {courseDetailQuery.isLoading ? (
              <Skeleton className="h-[400px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 mt-2" />
            ) : (
              <div
                className="mt-2 text-sm"
                dangerouslySetInnerHTML={{
                  __html: courseDetailQuery.data?.courseInformation || "",
                }}
              ></div>
            )}
          </div>
          <div id="course-syllabus" className="p-2">
            <div className="font-bold text-lg">Hướng dẫn khóa học</div>
            {courseDetailQuery.isLoading ? (
              <Skeleton className="h-[300px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 mt-2" />
            ) : (
              <div
                className="mt-2 text-sm"
                dangerouslySetInnerHTML={{
                  __html: courseDetailQuery.data?.courseGuide || "",
                }}
              ></div>
            )}
          </div>
          {/* Course review */}
          <Review courseId={params.courseId} />
        </div>
        <div className="border top-20 md:sticky w-full md:w-3/5 lg:w-2/3 xl:w-1/3 rounded-md dark:bg-slate-900 bg-white">
          {courseDetailQuery.isLoading ? (
            <Skeleton className="h-[200px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
          ) : (
            <div
              dangerouslySetInnerHTML={{
                __html: courseDetailQuery.data?.course.courseImg || "",
              }}
            ></div>
          )}
          {courseDetailQuery.isLoading ? (
            <Skeleton className="h-[400px] w-full rounded-xl bg-gray-200 dark:bg-slate-900 p-3 mt-2" />
          ) : (
            <div className="p-3">
              {courseDetailQuery.data?.course.isDiscount && (
                <p className="font-bold">
                  {/* {courseDetailQuery.data.course.discount.discountName} */}
                </p>
              )}
              <div className="flex items-center gap-2 mt-2 flex-wrap">
                <p className="font-bold text-pink-500 text-2xl">
                  {convertNumberToMoney(
                    courseDetailQuery.data?.course.newPrice || 0
                  )}
                </p>
                {courseDetailQuery.data?.course.isDiscount && (
                  <div className="flex items-center gap-2">
                    <p className="text-sm line-through">
                      {convertNumberToMoney(
                        courseDetailQuery.data?.course.oldPrice || 0
                      )}
                    </p>
                    <Badge variant="destructive">
                      -{courseDetailQuery.data?.course.percentDiscount}%
                    </Badge>
                  </div>
                )}
              </div>
              {ordersByUserCredentialQuery.data?.find((item) =>
                item.orderLines.find(
                  (item2) =>
                    item2.course.id === courseDetailQuery.data?.course.id
                )
              ) ? (
                <Button
                  variant="default"
                  className="w-full mt-2 border border-purple-800 bg-transparent text-purple-800 hover:text-white hover:bg-purple-800"
                  onClick={() =>
                    router.push(
                      `/learning/${courseDetailQuery.data?.course.id}`
                    )
                  }
                >
                  Bắt đầu học
                </Button>
              ) : (
                <SelectPaymentModal
                  courseId={params.courseId}
                  totalAmount={courseDetailQuery.data?.course.newPrice || 0}
                />
              )}
              <div className="h-[1px] bg-gray-300 mt-5"></div>
              <div className="flex flex-col gap-2 mt-5">
                {/* <div className="flex items-center gap-2">
                  <User2Icon className="w-5 h-5 text-violet-500" />
                  <p className="text-sm">46,833 học viên đã đăng ký</p>
                </div> */}
                <div className="flex items-center gap-2">
                  <BookIcon className="w-5 h-5 text-violet-500" />
                  <p className="text-sm">
                    {courseDetailQuery.data?.course.countUnits || 0} chủ đề,{" "}
                    {courseDetailQuery.data?.course.countLessons || 0} bài học
                  </p>
                </div>
                <div className="flex items-center gap-2">
                  <TimerIcon className="w-5 h-5 text-violet-500" />
                  <p className="text-sm">Đăng ký 1 lần, học mãi mãi</p>
                </div>
                <div className="flex items-center gap-2">
                  <LaptopIcon className="w-5 h-5 text-violet-500" />
                  <p className="text-sm">
                    Có thể học trên điện thoại và máy tính
                  </p>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </section>
  );
}
