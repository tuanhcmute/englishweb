"use client";

import { PaginatedItems } from "@/components/shared/paginate";
import {
  Badge,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
  Card,
  CardContent,
  Skeleton,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { useCourseCategories, useCourses } from "@/hooks";
import { cn, convertNumberToMoney } from "@/lib";
import { ICourseCategoryResponse, ICourseResponse } from "@/types";
import { HomeIcon } from "@radix-ui/react-icons";
import { IconStar, IconStarFilled } from "@tabler/icons-react";
import Link from "next/link";
import { useRouter, useSearchParams } from "next/navigation";
import { ReactElement, useCallback, useState } from "react";

export default function CoursesPage(): ReactElement {
  const courseCategoriesQuery = useCourseCategories();
  const coursesQuery = useCourses({ isActive: "1" });
  const router = useRouter();
  const searchParams = useSearchParams();
  const [currentItems, setCurrentItems] = useState<ICourseResponse[]>([]);
  const [currentCourseCategory, setCurrentCourseCategory] =
    useState<string>("");
  const updateCurrentItems = useCallback((newItems: ICourseResponse[]) => {
    setCurrentItems(newItems);
  }, []);

  if (coursesQuery.error)
    return (
      <div className="text-center w-full font-bold text-red-500">
        {coursesQuery.error.message}
      </div>
    );

  if (courseCategoriesQuery.error)
    return (
      <div className="text-center w-full font-bold text-red-500">
        {courseCategoriesQuery.error.message}
      </div>
    );

  return (
    <div className="bg-gray-100 dark:bg-slate-950">
      <div className="container">
        {/* Path */}
        <div className="pt-4">
          {/* Path */}
          <Breadcrumb>
            <BreadcrumbList>
              <BreadcrumbItem>
                <BreadcrumbLink>
                  <Link href={ROUTES.HOME} className="flex items-center gap-1">
                    <HomeIcon className="w-4" />
                    Trang chủ
                  </Link>
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbSeparator className="flex items-center" />
              <BreadcrumbItem>
                <BreadcrumbPage className="flex items-center gap-1">
                  Danh sách khóa học
                </BreadcrumbPage>
              </BreadcrumbItem>
            </BreadcrumbList>
          </Breadcrumb>
        </div>

        {/* Course category */}
        <section id="test-category" className="mt-5">
          <div className="">
            <h3 className="text-2xl font-semibold">Khóa học online</h3>
            {courseCategoriesQuery.isLoading ? (
              <Skeleton className="h-[30px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
            ) : (
              <div className="flex items-center flex-wrap gap-3 mt-5">
                <div
                  className={cn(
                    "border border-gray-400 rounded-full py-1 px-3 text-sm cursor-pointer hover:bg-blue-100 hover:border-purple-800 hover:text-purple-800",
                    currentCourseCategory === ""
                      ? "bg-blue-100 border-purple-800 text-purple-800"
                      : ""
                  )}
                  onClick={() => {
                    setCurrentCourseCategory("");
                    router.push(ROUTES.COURSES);
                  }}
                >
                  Tất cả
                </div>
                {courseCategoriesQuery.isSuccess &&
                  courseCategoriesQuery.data.data.map(
                    (
                      item: ICourseCategoryResponse,
                      index: number
                    ): ReactElement => {
                      return (
                        <Link
                          href={`${ROUTES.COURSES}?c=${item.id}`}
                          key={index}
                          className={cn(
                            "rounded-full py-1 px-3 text-sm border-gray-400 border hover:bg-blue-100 hover:border-purple-800 hover:text-purple-800",
                            currentCourseCategory === item.id
                              ? "bg-blue-100 border-purple-800 text-purple-800"
                              : ""
                          )}
                          onClick={() => setCurrentCourseCategory(item.id)}
                        >
                          {item.courseCategoryName}
                        </Link>
                      );
                    }
                  )}
              </div>
            )}
          </div>
        </section>
        {coursesQuery.isLoading ? (
          <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 pb-4">
            {Array.from({ length: 8 }).map((_, index) => {
              return (
                <div key={index} className="">
                  <Skeleton className=" h-[300px] w-full rounded-xl bg-gray-200 dark:bg-slate-900" />
                </div>
              );
            })}
          </div>
        ) : (
          <section id="card-list">
            <div className="flex-1">
              {coursesQuery.isSuccess && coursesQuery.data.data.length > 0 ? (
                <div id="pagination" className="py-10">
                  <PaginatedItems<ICourseResponse>
                    itemsPerPage={16}
                    items={coursesQuery.data?.data || []}
                    setCurrentItems={updateCurrentItems}
                  >
                    <div className="grid grid-cols-1 gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 ">
                      {currentItems
                        .filter((item) => {
                          if (searchParams.get("c")) {
                            return (
                              item.courseCategory.id === searchParams.get("c")
                            );
                          }
                          return true;
                        })
                        .map((item) => {
                          return (
                            <Card className="" key={item.id}>
                              <div className="w-full flex justify-center">
                                <div
                                  dangerouslySetInnerHTML={{
                                    __html: item.courseImg,
                                  }}
                                  className="w-full rounded-t-sm"
                                ></div>
                              </div>
                              <CardContent>
                                <p
                                  className="font-semibold text-sm mt-2 hover:text-purple-800 cursor-pointer"
                                  onClick={() => {
                                    router.push(`/courses/${item.id}`);
                                  }}
                                >
                                  {item.courseName}
                                </p>
                                <div>
                                  <div className="flex items-end gap-1 mt-2">
                                    {Array.from({
                                      length: item.overallRating,
                                    }).map((_, index) => {
                                      return (
                                        <IconStarFilled
                                          key={index}
                                          className="text-yellow-500"
                                        />
                                      );
                                    })}
                                    {Array.from({
                                      length: 5 - item.overallRating,
                                    }).map((_, index) => {
                                      return (
                                        <IconStar
                                          key={index}
                                          className="text-yellow-500"
                                        />
                                      );
                                    })}
                                  </div>
                                </div>
                                <p className="text-[10px] pt-2">
                                  ({item.countReviews} lượt đánh giá)
                                </p>
                                <div className="flex items-center gap-2 mt-2">
                                  <p className="font-bold text-pink-500">
                                    {convertNumberToMoney(item.newPrice)}
                                  </p>
                                  {item.isDiscount && (
                                    <div className="flex items-center gap-2">
                                      <p className="line-through text-[12px]">
                                        {convertNumberToMoney(item.oldPrice)}
                                      </p>
                                      <Badge
                                        variant="destructive"
                                        className="text-[12px] px-2"
                                      >
                                        {item.percentDiscount}%
                                      </Badge>
                                    </div>
                                  )}
                                </div>
                                <p className="mt-2 text-[11px] py-1 px-2 rounded-full bg-blue-100 text-purple-800 w-fit">
                                  #{item.courseCategory.courseCategoryName}
                                </p>
                              </CardContent>
                            </Card>
                          );
                        })}
                    </div>
                  </PaginatedItems>
                </div>
              ) : (
                <div className="flex items-center justify-center h-full">
                  <span className="font-medium">Không có bài thi nào</span>
                </div>
              )}
            </div>
          </section>
        )}
      </div>
    </div>
  );
}
