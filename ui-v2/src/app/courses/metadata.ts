export const description = `
Chào mừng bạn đến với English Academy - trang web hàng đầu về luyện thi tiếng Anh! Chúng tôi tự hào là một nền tảng giáo dục trực tuyến nổi tiếng, cung cấp các khóa học chất lượng cao để giúp bạn nâng cao kỹ năng tiếng Anh và đạt được thành công trong các kỳ thi quan trọng.

Với chúng tôi, việc luyện thi tiếng Anh không còn là một nhiệm vụ đáng sợ. Chúng tôi hiểu rằng mỗi người học có nhu cầu và mục tiêu riêng, vì vậy chúng tôi đã tạo ra một loạt các khóa học đa dạng, phù hợp với mọi trình độ và mục tiêu của bạn.

Tại English Academy, chúng tôi phát triển các khóa học tiếng Anh dựa trên các phương pháp học tập hiệu quả và tài liệu chất lượng. Chúng tôi có đội ngũ giảng viên giàu kinh nghiệm, được chọn lọc kỹ lưỡng, cam kết mang đến cho bạn sự hướng dẫn tận tâm và chất lượng hàng đầu.

Khóa học của chúng tôi bao gồm các chủ đề quan trọng như ngữ pháp, từ vựng, phát âm, viết luận, và kỹ năng nghe nói. Bạn sẽ có cơ hội rèn luyện và thực hành kỹ năng tiếng Anh thông qua các bài tập, bài kiểm tra, và hoạt động tương tác trực tuyến.

Bên cạnh đó, chúng tôi cung cấp các khóa học luyện thi tiếng Anh chuyên sâu cho các kỳ thi quốc tế như IELTS, TOEFL, TOEIC và Cambridge exams. Bạn sẽ được làm quen với cấu trúc và yêu cầu của từng kỳ thi, học các chiến lược làm bài và nhận được phản hồi cá nhân từ giảng viên để nâng cao điểm số thi của mình.

Tất cả các khóa học của chúng tôi đều được cung cấp trên nền tảng trực tuyến tiện lợi, cho phép bạn học tại bất kỳ đâu và bất kỳ khi nào phù hợp với lịch trình của bạn. Bạn cũng có thể tương tác với cộng đồng học viên rộng lớn của chúng tôi, chia sẻ kinh nghiệm và học hỏi từ nhau.

Hãy đến với English Academy và khám phá các khóa học tiếng Anh chất lượng cao của chúng tôi. Chúng tôi cam kết sẽ giúp bạn đạt được mục tiêu luyện thi tiếng Anh và mang đến cho bạn những cơ hội mới trong tương lai. Hãy bắt đầu hôm nay và trang bị cho mình những kỹ năng cần thiết để thành công!
`;
