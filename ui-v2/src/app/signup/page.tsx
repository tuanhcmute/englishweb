"use client";

import {
  Button,
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
  Input,
  PasswordInput,
} from "@/components/ui";
import { ROUTES } from "@/constants";
import { Provider } from "@/enums";
import { useSignUp } from "@/hooks";
import { IUserCredentialRequest } from "@/types";
import { zodResolver } from "@hookform/resolvers/zod";
import { IconLoader2 } from "@tabler/icons-react";
import Link from "next/link";
import { ReactElement, useEffect } from "react";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { UserCredentialSchema } from "./schema";

export default function SignUp(): ReactElement {
  const { mutateAsync: handleSignUp, isPending } = useSignUp();
  const form = useForm<z.infer<typeof UserCredentialSchema>>({
    resolver: zodResolver(UserCredentialSchema),
    defaultValues: {
      username: "",
      password: "",
      email: "",
      phoneNumber: "",
      confirmPassword: "",
      fullName: "",
      provider: Provider.LOCAL,
    },
  });

  async function onSubmit(values: z.infer<typeof UserCredentialSchema>) {
    const requestData = {
      ...values,
    } as IUserCredentialRequest;
    console.log({ requestData });
    await handleSignUp(requestData);
  }

  return (
    <div className="flex flex-col items-center justify-center px-6 pt-8 mx-auto md:h-screen pt:mt-0 dark:bg-gray-900 mb-20">
      <Link
        href={ROUTES.HOME}
        className="flex items-center justify-center mb-8 text-2xl font-semibold lg:mb-10 dark:text-white hover:text-purple-800 transition-all"
      >
        <span>English Academy</span>
      </Link>
      <div className="w-full max-w-xl p-6 space-y-8 sm:p-8 bg-white rounded-lg dark:bg-gray-800 border border-gray-300">
        <h2 className="text-2xl font-bold text-gray-900 dark:text-white">
          Đăng ký tài khoản
        </h2>
        <div>
          <Form {...form}>
            <form
              onSubmit={form.handleSubmit(onSubmit)}
              className="flex flex-col gap-2"
            >
              <FormField
                control={form.control}
                name="username"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel htmlFor="username">Tên đăng nhập</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Tên đăng nhập"
                        {...field}
                        id="username"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="password"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Mật khẩu</FormLabel>
                    <FormControl>
                      <PasswordInput placeholder="Nhập mật khẩu" {...field} />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="confirmPassword"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel htmlFor="confirmPassword">
                      Xác nhận mật khẩu
                    </FormLabel>
                    <FormControl>
                      <PasswordInput
                        id="confirmPassword"
                        placeholder="Xác nhận mật khẩu"
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="phoneNumber"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel htmlFor="phoneNumber">Số điện thoại</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Số điện thoại"
                        {...field}
                        id="phoneNumber"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel htmlFor="email">Email</FormLabel>
                    <FormControl>
                      <Input
                        placeholder="Địa chỉ email"
                        {...field}
                        type="email"
                        id="email"
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="fullName"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel htmlFor="fullName">Họ tên</FormLabel>
                    <FormControl>
                      <Input placeholder="Họ và tên" {...field} id="fullName" />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
              <div className="flex justify-between align-middle">
                <Link
                  href={ROUTES.FORGOT_PASSWORD}
                  className="italic text-sm hover:underline hover:text-purple-800 transition-all"
                >
                  Quên mật khẩu?
                </Link>
                <Link
                  href={ROUTES.LOGIN}
                  className="text-sm hover:underline hover:text-purple-800 transition-all"
                >
                  Đã có tài khoản
                </Link>
              </div>
              <Button
                type="submit"
                variant="destructive"
                className="w-fit text-white bg-purple-700 hover:bg-purple-800"
                disabled={isPending}
              >
                {isPending ? (
                  <div className="flex items-center gap-1">
                    <IconLoader2 className="animate-spin" />
                    Đăng ký
                  </div>
                ) : (
                  "Đăng ký"
                )}
              </Button>
            </form>
          </Form>
        </div>
      </div>
    </div>
  );
}
