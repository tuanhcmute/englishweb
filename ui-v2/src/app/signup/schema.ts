import { z } from "zod";

export const UserCredentialSchema = z
  .object({
    username: z
      .string()
      .min(2, {
        message: "Tên đăng nhập gồm ít nhất 2 ký tự",
      })
      .max(100, {
        message: "Tên đăng nhập không được quá 100 ký tự",
      }),
    password: z
      .string()
      .min(6, { message: "Mật khẩu ít nhất 6 ký tự" })
      .regex(/[a-zA-Z]/, { message: "Mật khẩu phải chứa ít nhất 1 ký tự" })
      .regex(/[0-9]/, { message: "Mật khẩu phải chứa ít nhất một số" })
      .regex(/[^a-zA-Z0-9]/, {
        message: "Mật khẩu phải chứa ít nhất một ký tự đặc biệt",
      })
      .trim(),
    provider: z.string().trim(),
    confirmPassword: z.string().trim().min(1, {
      message: "Xác nhận mật khẩu là bắt buộc",
    }),
    phoneNumber: z.string().trim().min(1, {
      message: "Số điện thoại là bắt buộc",
    }),
    email: z.string().email({ message: "Email không hợp lệ" }).trim(),
    fullName: z.string().trim().min(1, {
      message: "Tên là bắt buộc",
    }),
  })
  .refine((data) => data.password === data.confirmPassword, {
    message: "Xác nhận mật khẩu không khớp",
    path: ["confirmPassword"],
  });
