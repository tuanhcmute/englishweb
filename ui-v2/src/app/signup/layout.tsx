import type { Metadata } from "next";
import { ReactNode } from "react";

export const metadata: Metadata = {
  title: "Đăng ký tài khoản - English Academy",
  description: "Generated by create next app",
};

export default function Layout({ children }: { children: ReactNode }) {
  return <>{children}</>;
}
