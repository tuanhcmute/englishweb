export const BASE_URL = process.env.NEXT_PUBLIC_BACKEND_BASE_URL;
export const CHATBOT_BASE_URL = process.env.NEXT_PUBLIC_CHATBOT_BASE_URL;
export const SESSION_SECRET = process.env.SESSION_SECRET;
export const ACCESS_TOKEN_KEY = "accessToken";
export const REFRESH_TOKEN_KEY = "refreshToken";
export const USER_CREDENTIAL = "userCredential";
export const USER_CREDENTIAL_ID = "userCredentialId";
export const MAX_FILE_SIZE = 5000000; // 5MB
export const MAX_VIDEO_FILE_SIZE = 2000000000; // 2GB
export const ACCEPTED_VIDEO_TYPES = [
  "video/mp4",
  "video/avi",
  "video/flv",
  "video/wmv",
  "video/mov",
  "video/webm",
  "video/avchd",
];

export const ACCEPTED_IMAGE_TYPES = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/webp",
];
export const ACCEPTED_AUDIO_TYPES = ["mp3"];

export const ROUTES = {
  UNITS: "/units",
  REVIEWS: "/reviews",
  ADMIN_CHAT_BOT_MANAGE: "/admin/manage/chatbot",
  ADMIN_SKILLED_AGENT_MANAGE: "/admin/manage/chatbot/skilled-agents",
  ADMIN_TEST_SUITE_MANAGE: "/admin/manage/chatbot/test-suites",
  ADMIN_STATIC_PAGE_MANAGE: "/admin/manage/static-pages",
  ADMIN_BANNER_MANAGE: "/admin/manage/banners",
  LESSONS: "/lessons",
  FULL_TEST_ANSWER: "/full-test-answer",
  PRACTICE_RESULTS: "/practice/results",
  HOME: "/",
  TESTS: "/tests",
  COURSES: "/courses",
  FLASHCARDS: "/flashcards",
  NEWS: "/news",
  ABOUT_US: "/about-us",
  MY_COURSES: "/my-account/courses",
  MY_TEST_RESULTS: "/my-account/test-results",
  PROFILE: "/my-account/profile",
  LOGIN: "/login",
  LOGOUT: "/logout",
  ADMIN_DASHBOARD: "/admin/dashboard",
  ADMIN_TEST_MANAGE: "/admin/manage/tests",
  ADMIN_TEST_CATEGORY_MANAGE: "/admin/manage/test-categories",
  ADMIN_COURSE_MANAGE: "/admin/manage/courses",
  ADMIN_COURSE_CATEGORY_MANAGE: "/admin/manage/course-categories",
  ADMIN_FLASHCARD_MANAGE: "/admin/manage/flashcards",
  ADMIN_USER_MANAGE: "/admin/manage/users",
  ADMIN_NEWS_MANAGE: "/admin/manage/news",
  ADMIN_NEWS_CATEGORY_MANAGE: "/admin/manage/news-categories",
  ADMIN_REVENUE_MANAGE: "/admin/manage/revenue",
  ADMIN_PROMOTION_MANAGE: "/admin/manage/promotion",
  ADMIN_COUPON_MANAGE: "/admin/manage/coupon",
  ADMIN_VOUCHER_MANAGE: "/admin/manage/voucher",
  SIGN_UP: "/signup",
  FORGOT_PASSWORD: "/forgot-password",
  PARTS: "/parts",
  PRACTICE: "/practice",
  ADMIN: "/admin",
  MY_ACCOUNT: "/my-account",
  ADMIN_ORDER_MANAGE: "/admin/manage/orders",
  ME_FLASHCARDS: "/flashcards/me",
};

export const PROTECTED_ROUTES = [
  ROUTES.ADMIN,
  ROUTES.PRACTICE,
  ROUTES.MY_ACCOUNT,
  ROUTES.FLASHCARDS,
];

export const KEY_QUERIES = {
  LESSONS_NOTES: "lessonNotes",
  LESSONS: "lessons",
  UNITS: "units",
  REVIEWS: "reviews",
  BANNERS: "banners",
  COMMENTS: "comments",
  REPORT_COMMENTS: "reportComments",
  STUDYING_FLASHCARDS: "studyingFlashcards",
  ORDERS: "orders",
  PAYMENTS: "payments",
  FLASHCARD_ITEMS: "flashcardItems",
  FLASHCARDS: "flashcards",
  SKILLED_AGENTS: "skilledAgent",
  SKILLED_AGENT_CHATS: "skilledAgentChats",
  SKILLED_AGEN_TESTS: "skilledAgentTests",
  SKILLED_AGENTS_VERSIONS: "skilledAgentVersions",
  TEST_SUITES: "testSuites",
  TEST_SUITE_RUNS: "testSuiteRuns",
  TEST_SUITE_RUN_RESULTS: "testSuiteRunResults",
  TEST_SUITE_QUESTIONS: "testSuiteQuestions",
  STATIC_PAGES: "staticPages",
  USER_CREDENTIALS: "userCredentials",
  // COURSE: "course",
  COURSE_DETAIL: "courseDetail",
  FULL_TEST_ANSWER: "fullTestAnswer",
  USER_ANSWER: "userAnswer",
  USER_ANSWERS: "userAnswers",
  QUESTIONS: "questions",
  QUESTION: "question",
  QUESTION_GROUPS: "questionGroups",
  TESTS: "tests",
  SCORE_CONVERSION: "scoreConversions",
  LLM_MODEL_MODULES: "llmModelModules",
  TOOL_MODULES: "toolModules",
  TOOLS: "tools",
  LLM_MODELS: "llmModels",
  TEST: "test",
  PART_INFORMATION: "partInformation",
  PART_OF_TEST: "partOfTest",
  TEST_CATEGORIES: "testCategories",
  LOGIN: "login",
  COURSE_CATEGORIES: "courseCategories",
  NEWS_CATEGORIES: "newsCategories",
  NEWS: "news",
  COURSES: "courses",
  DASHBOARD: "dashboard",
  DISCOUNTS: "discounts",
  DISCOUNT_CONDITIONS: "discountConditions",
  ROLES: "roles",
};

export const TEST_SUITE_RUN_STATUS = {
  NOT_RUN: "NOT RUN",
  RE_RUN: "RE-RUN",
  RUN: "RUN",
};
