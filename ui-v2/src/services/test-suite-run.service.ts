import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import {
  ICreateTestSuiteRunRequest,
  ITestSuiteQuestionResponse,
  ITestSuiteRunResponse,
  IUpdateTestSuiteRunRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class TestSuiteRunService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  async getTestSuiteRunById(id: string): Promise<ITestSuiteRunResponse> {
    const requestUrl = `${this.path}/test-suite-run/${id}`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async getAllTestSuiteRuns(): Promise<ITestSuiteRunResponse[]> {
    const requestUrl = `${this.path}/test-suite-runs`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createTestSuiteRun(
    requestData: ICreateTestSuiteRunRequest
  ): Promise<ITestSuiteQuestionResponse> {
    const requestUrl = `${this.path}/test-suite-run`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data;
  }

  async updateTestSuiteRun(
    requestData: IUpdateTestSuiteRunRequest
  ): Promise<ITestSuiteRunResponse> {
    const requestUrl = `${this.path}/test-suite-run/${requestData.id}`;
    const res: AxiosResponse = await httpChatbotServer.put(
      requestUrl,
      requestData
    );
    return res.data;
  }

  // async getStaticPageByCode(
  //   staticPageCode: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by page code");
  //   }
  // }
}

export const testSuiteRunService = new TestSuiteRunService();
