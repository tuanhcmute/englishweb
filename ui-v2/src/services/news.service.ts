import {
  IBaseResponse,
  ICreateNewsRequest,
  IFullNewsResponse,
  IGetNewsRequest,
  INewsResponse,
  IUpdateNewsRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";
import _ from "lodash";

class NewsService {
  private readonly path = `${BASE_URL}/news`;

  async getAllNewsByNewsCategory(
    newsCategoryId: string
  ): Promise<IBaseResponse<INewsResponse[]>> {
    try {
      const requestUrl = `${this.path}/byNewsCategory?value=${newsCategoryId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all news");
    }
  }

  async getAllNews(
    requestData?: IGetNewsRequest
  ): Promise<IBaseResponse<INewsResponse[]>> {
    let queryBuilder = "";
    const keys = _.keys(requestData) as Array<keyof IGetNewsRequest>;
    keys.forEach((key, index) => {
      if (requestData?.[key])
        queryBuilder = queryBuilder.concat(`${key}=${requestData[key]}`);
      if (index < keys.length - 1) queryBuilder = queryBuilder.concat("&");
    });
    const requestUrl = `${this.path}?${queryBuilder}`;
    const res: AxiosResponse = await http.get(requestUrl);
    return res.data?.data;
  }

  async getAllTopNews(
    count: number,
    start: number
  ): Promise<IBaseResponse<INewsResponse[]>> {
    try {
      const requestUrl = `${this.path}/top?count=${count}&start=${start}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all news");
    }
  }

  async getFullNewsById(id: string): Promise<IBaseResponse<IFullNewsResponse>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch news");
    }
  }

  async createNews(
    requestData: ICreateNewsRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create news categories");
    }
  }

  async updateNews(
    requestData: IUpdateNewsRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update news");
    }
  }
}

export const newsService = new NewsService();
