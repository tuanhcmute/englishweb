import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import {
  ICreateToolRequest,
  IToolModuleResponse,
  IToolResponse,
} from "@/types";
import { AxiosResponse } from "axios";

class ToolService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  // async getStaticPageById(
  //   id: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?id=${id}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by id");
  //   }
  // }

  async getAllToolModules(): Promise<IToolModuleResponse[]> {
    const requestUrl = `${this.path}/tools_module`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async getAllTools(): Promise<IToolResponse[]> {
    const requestUrl = `${this.path}/tools`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createTool(requestData: ICreateToolRequest): Promise<IToolResponse> {
    const requestUrl = `${this.path}/tool`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data;
  }

  // async updateStaticPage(
  //   requestData: IUpdateStaticPageRequest
  // ): Promise<IBaseResponse<void>> {
  //   try {
  //     const requestUrl = `${this.path}`;
  //     const res: AxiosResponse = await http.put(requestUrl, requestData);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to update static page");
  //   }
  // }

  // async getStaticPageByCode(
  //   staticPageCode: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by page code");
  //   }
  // }
}

export const toolService = new ToolService();
