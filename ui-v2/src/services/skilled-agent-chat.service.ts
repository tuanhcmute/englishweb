import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import {
  ICreateSkilledAgentChatRequest,
  ISkilledAgentChatResponse,
  ISkilledAgentResponse,
} from "@/types";
import { AxiosResponse } from "axios";

class SkilledAgentChatService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  async getAllSkilledAgentChat(): Promise<ISkilledAgentChatResponse[]> {
    const requestUrl = `${this.path}/skilled-agent-chats`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createSkilledAgentChat(
    requestData: ICreateSkilledAgentChatRequest
  ): Promise<ISkilledAgentChatResponse> {
    const requestUrl = `${this.path}/skilled-agent-chat`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data;
  }
}

export const skilledAgentChatService = new SkilledAgentChatService();
