import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBannerResponse,
  IBaseResponse,
  ICreateBannerRequest,
  IGetBannerRequest,
  IUpdateBannerRequest,
  IUpdateCommentRequest,
} from "@/types";
import { AxiosResponse } from "axios";
import _ from "lodash";

class BannerService {
  private readonly path = `${BASE_URL}/banner`;

  async getAllBanners(
    requestData?: IGetBannerRequest
  ): Promise<IBaseResponse<IBannerResponse[]>> {
    let queryBuilder = "";
    const keys = _.keys(requestData) as Array<keyof IGetBannerRequest>;
    keys.forEach((key, index) => {
      if (requestData?.[key])
        queryBuilder = queryBuilder.concat(`${key}=${requestData[key]}`);
      if (index < keys.length - 1) queryBuilder = queryBuilder.concat("&");
    });
    const requestUrl = `${this.path}?${queryBuilder}`;
    const res: AxiosResponse = await http.get(requestUrl);
    return res.data?.data;
  }

  async createBanner(
    requestData: ICreateBannerRequest
  ): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}`;
    const res: AxiosResponse = await http.post(requestUrl, requestData);
    return res.data?.data;
  }

  async updateBanner(
    requestData: IUpdateBannerRequest
  ): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}`;
    const res: AxiosResponse = await http.put(requestUrl, requestData);
    return res.data?.data;
  }

  async deleteBanner(id: string): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}/${id}`;
    const res: AxiosResponse = await http.delete(requestUrl);
    return res.data?.data;
  }
}

export const bannerService = new BannerService();
