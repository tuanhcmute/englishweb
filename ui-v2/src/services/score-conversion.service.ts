import { IBaseResponse, IUpdateScoreConversionRequest } from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class ScoreConversionService {
  private readonly path = `${BASE_URL}/score-conversion`;

  async updateScoreConversion(
    requestData: IUpdateScoreConversionRequest
  ): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}`;
    const res: AxiosResponse = await http.put(requestUrl, requestData);
    return res.data;
  }
}

export const scoreConversionService = new ScoreConversionService();
