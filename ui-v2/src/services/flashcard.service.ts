import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IFlashcardResponse,
  IBaseResponse,
  ICreateFlashcardRequest,
  IFullFlashcardResponse,
  IUpdateFlashcardRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class FlashcardService {
  private readonly path = `${BASE_URL}/flashcard`;

  async getAllFlashcards(): Promise<IBaseResponse<IFlashcardResponse[]>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.get(requestUrl);
      console.log({ res });
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all flashcards");
    }
  }

  async getAllFlashcardsByUserCredential(
    userCredentialId: string
  ): Promise<IBaseResponse<IFlashcardResponse[]>> {
    try {
      const requestUrl = `${this.path}/?userCredentialId=${userCredentialId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      console.log({ res });
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all flashcards by user credential");
    }
  }

  async getAllFlashcardsByType(
    type: string
  ): Promise<IBaseResponse<IFlashcardResponse[]>> {
    try {
      const requestUrl = `${this.path}?flashcardType=${type}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all flashcards by type");
    }
  }

  async getFlashcardById(
    id: string
  ): Promise<IBaseResponse<IFullFlashcardResponse>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch flashcard by id");
    }
  }

  async createFlashcard(
    requestData: ICreateFlashcardRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      console.log({ requestData });
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create flashcard");
    }
  }

  async updateFlashcard(
    requestData: IUpdateFlashcardRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      console.log({ requestData });
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update flashcard");
    }
  }

  async deleteFlashcard(flashcardId: string): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/${flashcardId}`;
      const res: AxiosResponse = await http.delete(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to delete flashcard");
    }
  }
}

export const flashcardService = new FlashcardService();
