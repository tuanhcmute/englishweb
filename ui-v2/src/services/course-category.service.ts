import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICourseCategoryResponse,
  ICreateCourseCategoryRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class CourseCategoryService {
  private readonly path = `${BASE_URL}/course-category`;

  async getAllCourseCategories(): Promise<
    IBaseResponse<ICourseCategoryResponse[]>
  > {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch course categories");
    }
  }

  async createCourseCategory(
    item: ICreateCourseCategoryRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, item);
      return res.data;
    } catch (error) {
      throw new Error("Failed to create course category");
    }
  }
}

export const courseCategoryService = new CourseCategoryService();
