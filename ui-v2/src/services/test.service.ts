import {
  ITest,
  IBaseResponse,
  ITestResponse,
  ITestRequest,
  IFullTestResponse,
  IUpdateTestRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class TestService {
  private readonly path = `${BASE_URL}/test`;
  async getAllTests(): Promise<IBaseResponse<ITestResponse[]>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch tests");
    }
  }

  async createTest(requestData: ITestRequest): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}`;
    const res: AxiosResponse = await http.post(requestUrl, requestData);
    return res.data;
  }

  async updateTest(
    requestData: IUpdateTestRequest
  ): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}`;
    const res: AxiosResponse = await http.put(requestUrl, requestData);
    return res.data;
  }

  async deleteTest(testId: string): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/${testId}`;
      const res: AxiosResponse = await http.delete(requestUrl);
      return res.data;
    } catch (error) {
      throw new Error(`Failed to delete test with testId = ${testId}`);
    }
  }
  async getTestById(
    testId: string
  ): Promise<IBaseResponse<{ data: ITestResponse }>> {
    try {
      const requestUrl = `${this.path}/${testId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data;
    } catch (error) {
      console.log(error);
      throw new Error(`Failed to get test with testId = ${testId}`);
    }
  }

  async getFullTestById(
    testId: string
  ): Promise<IBaseResponse<{ data: IFullTestResponse }>> {
    try {
      const requestUrl = `${this.path}/${testId}/practice`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data;
    } catch (error) {
      console.log(error);
      throw new Error(`Failed to get test with testId = ${testId}`);
    }
  }
}

export const testService = new TestService();
