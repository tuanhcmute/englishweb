import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICreateCourseCategoryRequest,
  IDashboardResponse,
} from "@/types";
import { AxiosResponse } from "axios";

class DashboardService {
  private readonly path = `${BASE_URL}/dashboard`;

  async getDashboard(): Promise<IBaseResponse<IDashboardResponse>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch dashboard");
    }
  }
}

export const dashboardService = new DashboardService();
