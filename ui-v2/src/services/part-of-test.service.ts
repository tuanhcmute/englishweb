import { IBaseResponse, IPartOfTest } from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class PartOfTestService {
  private readonly path = `${BASE_URL}/part-of-test`;
  async getAllPartOfTestByTest(
    testId: string
  ): Promise<IBaseResponse<IPartOfTest[]>> {
    try {
      // const requestUrl = this.path;
      const requestUrl = `${this.path}/byTest?value=${testId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch part of test");
    }
  }
  async updatePartOfTest(testId: string): Promise<IBaseResponse<void>> {
    try {
      // const requestUrl = this.path;
      const requestUrl = `${this.path}/sync/byTest?value=${testId}`;
      const res: AxiosResponse = await http.put(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update part of test");
    }
  }
}

export const partOfTestService = new PartOfTestService();
