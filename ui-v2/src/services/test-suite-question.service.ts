import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import {
  ICreateTestSuiteQuestionRequest,
  ITestSuiteQuestionResponse,
} from "@/types";
import { AxiosResponse } from "axios";

class TestSuiteQuestionService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  async getTestSuiteQuestionById(
    id: string
  ): Promise<ITestSuiteQuestionResponse> {
    const requestUrl = `${this.path}/question/${id}`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async getAllTestSuiteQuestions(): Promise<ITestSuiteQuestionResponse[]> {
    const requestUrl = `${this.path}/questions`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createTestSuiteQuestion(
    requestData: ICreateTestSuiteQuestionRequest
  ): Promise<ITestSuiteQuestionResponse> {
    const requestUrl = `${this.path}/question`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data;
  }

  // async updateStaticPage(
  //   requestData: IUpdateStaticPageRequest
  // ): Promise<IBaseResponse<void>> {
  //   try {
  //     const requestUrl = `${this.path}`;
  //     const res: AxiosResponse = await http.put(requestUrl, requestData);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to update static page");
  //   }
  // }

  // async getStaticPageByCode(
  //   staticPageCode: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by page code");
  //   }
  // }
}

export const testSuiteQuestionService = new TestSuiteQuestionService();
