import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  IUpdateRoleRequest,
  IUpdateUserCredentialRequest,
  IUserCredentialResponse,
} from "@/types";
import { AxiosResponse } from "axios";

class UserCerdentialService {
  private readonly path = `${BASE_URL}/user-credential`;

  async getUserCredentialByUsername(requestData: {
    username: string;
  }): Promise<IBaseResponse<IUserCredentialResponse>> {
    try {
      const requestUrl = `${this.path}/credential?username=${requestData.username}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to get user credential");
    }
  }

  async updateUserCredential(
    requestData: IUpdateUserCredentialRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update user credential");
    }
  }

  async getUserCredentialById(
    id: string
  ): Promise<IBaseResponse<IUserCredentialResponse>> {
    try {
      const requestUrl = `${this.path}/credential?id=${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to get user credential");
    }
  }

  async getAllUserCredentials(): Promise<
    IBaseResponse<IUserCredentialResponse[]>
  > {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to get all user credentials");
    }
  }

  async updateUserRole(
    requestData: IUpdateRoleRequest
  ): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}/${requestData.userCredentialId}/role`;
    const res: AxiosResponse = await http.put(requestUrl, requestData);
    return res.data?.data;
  }

  async deleteUserCredential(id: string): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}/${id}`;
    const res: AxiosResponse = await http.delete(requestUrl);
    return res.data?.data;
  }
}

export const userCerdentialService = new UserCerdentialService();
