import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import { IBaseResponse } from "@/types";
import {
  ICourseDetailResponse,
  IUpdateCourseDetailRequest,
} from "@/types/course-detail.type";
import { AxiosResponse } from "axios";

class CourseDetailService {
  private readonly path = `${BASE_URL}/course-detail`;
  async getCourseDetailByCourse(
    courseId: string
  ): Promise<IBaseResponse<ICourseDetailResponse>> {
    try {
      const requestUrl = `${this.path}/byCourse?value=${courseId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to fetch course detail");
    }
  }

  async updateCourseDetail(
    requestData: IUpdateCourseDetailRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to fetch course detail");
    }
  }
}

export const courseDetailService = new CourseDetailService();
