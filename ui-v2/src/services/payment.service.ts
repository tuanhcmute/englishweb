import { IBaseResponse, ICreatePaymentRequest } from "@/types";
import { http } from "@/lib";
import { AxiosError, AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class PaymentService {
  private readonly path = `${BASE_URL}/payment`;

  async createPayment(
    requestData: ICreatePaymentRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      if (error instanceof AxiosError) throw new Error(error.message);
      throw new Error("Fail to create payment");
    }
  }
}

export const paymentService = new PaymentService();
