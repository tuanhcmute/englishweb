import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import { IBaseResponse, IDiscountConditionResponse } from "@/types";
import { AxiosResponse } from "axios";

class DiscountConditionService {
  private readonly path = `${BASE_URL}/discount-condition`;

  async getAllDiscountConditions(
    discountId?: string
  ): Promise<IBaseResponse<IDiscountConditionResponse[]>> {
    try {
      let queryBuilder = "";
      if (discountId) {
        queryBuilder.concat(`discountId=${discountId}`);
      }
      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      console.log(res.data);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch discount conditions");
    }
  }
}

export const discountConditionService = new DiscountConditionService();
