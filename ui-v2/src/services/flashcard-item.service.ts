import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  IFlashcardItemResponse,
  ICreateFlashcardItemRequest,
  IUpdateFlashcardItemRequest,
  ICreateFlashcardItemByKeywordRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class FlashcardItemService {
  private readonly path = `${BASE_URL}/flashcard-item`;

  async getFlashcardItemById(
    id: string
  ): Promise<IBaseResponse<IFlashcardItemResponse>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch flashcard item by id");
    }
  }

  async getAllFlashcardItems(
    flashcardId?: string
  ): Promise<IBaseResponse<IFlashcardItemResponse[]>> {
    try {
      let queryBuilder = "";
      if (flashcardId) {
        queryBuilder = queryBuilder.concat(`flashcardId=${flashcardId}`);
      }
      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch flashcard items");
    }
  }

  async createFlashcardItem(
    requestData: ICreateFlashcardItemRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create flashcard item");
    }
  }

  async createFlashcardItemByKeyword(
    requestData: ICreateFlashcardItemByKeywordRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/keyword`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create flashcard item by keyword");
    }
  }

  async updateFlashcardItem(
    requestData: IUpdateFlashcardItemRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update flashcard item");
    }
  }

  async updateFlashcardItemImage(
    id: string,
    image: string
  ): Promise<IBaseResponse<void>> {
    try {
      const requestData: { image: string } = { image };
      const requestUrl = `${this.path}/${id}/uploadImage`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update flashcard item");
    }
  }

  async deleteFlashcardItem(
    flashcardItemId: string
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/${flashcardItemId}`;
      const res: AxiosResponse = await http.delete(requestUrl);
      return res.data;
    } catch (error) {
      throw new Error(
        `Failed to delete test with flashcard item = ${flashcardItemId}`
      );
    }
  }
}

export const flashcardItemService = new FlashcardItemService();
