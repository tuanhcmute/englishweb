import {
  IBaseResponse,
  IPartInformation,
  IQuestionGroupRequest,
  IQuestionGroupResponse,
  IUpdateQuestionGroupRequest,
  IUploadQuestionGroupFileRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class QuestionGroupService {
  private readonly path = `${BASE_URL}/question-group`;

  async createNewQuestionGroup(
    requestData: IQuestionGroupRequest
  ): Promise<IBaseResponse<IPartInformation[]>> {
    try {
      const formData = new FormData();
      formData.append("image", requestData.image);
      formData.append("partOfTestId", requestData.partOfTestId);
      formData.append("questionContentEn", requestData.questionContentEn);
      formData.append(
        "questionContentTranscript",
        requestData.questionContentTranscript
      );
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create question group");
    }
  }

  async updateQuestionGroup(
    requestData: IUpdateQuestionGroupRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update question group");
    }
  }

  async uploadQuestionGroupFile(
    requestData: IUploadQuestionGroupFileRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const formData = new FormData();
      formData.append("file", requestData.file);
      formData.append("type", requestData.type);
      const requestUrl = `${this.path}/${requestData.id}/upload`;
      const res: AxiosResponse = await http.post(requestUrl, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to upload question group file");
    }
  }

  async getAllQuestionGroupByPart(
    partId: string
  ): Promise<IBaseResponse<IQuestionGroupResponse[]>> {
    try {
      const requestUrl = `${this.path}/byPart?value=${partId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch question group");
    }
  }

  async deleteQuestionGroup(
    questionGroupId: string
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/${questionGroupId}`;
      const res: AxiosResponse = await http.delete(requestUrl);
      return res.data?.data;
    } catch (e) {
      throw new Error("Failed to delete question group");
    }
  }
}

export const questionGroupService = new QuestionGroupService();
