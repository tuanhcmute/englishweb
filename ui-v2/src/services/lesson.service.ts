import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICreateLessonRequest,
  ILessonResponse,
  IUpdateLessonRequest,
  IUploadVideoLessonRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class LessonService {
  private readonly path = `${BASE_URL}/lesson`;
  async getLessonById(id: string): Promise<IBaseResponse<ILessonResponse>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch lesson by id");
    }
  }

  async createLesson(
    requestData: ICreateLessonRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to create lesson");
    }
  }

  async updateLesson(
    requestData: IUpdateLessonRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to update lesson");
    }
  }

  async uploadLesson(
    requestData: IUploadVideoLessonRequest
  ): Promise<IBaseResponse<void>> {
    try {
      // const formData = new FormData();
      // formData.append("id", requestData.id);
      // formData.append("file", requestData.video);
      const requestUrl = `${this.path}/upload`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to upload video lesson");
    }
  }
}

export const lessonService = new LessonService();
