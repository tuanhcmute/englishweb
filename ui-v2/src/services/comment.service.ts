import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICommentResponse,
  ICreateCommentRequest,
  IGetCommentRequest,
  IUpdateCommentRequest,
} from "@/types";
import { AxiosResponse } from "axios";
import _ from "lodash";

class CommentService {
  private readonly path = `${BASE_URL}/comment`;

  async getAllComments(
    requestData?: IGetCommentRequest
  ): Promise<IBaseResponse<ICommentResponse[]>> {
    try {
      let queryBuilder = "";
      const keys = _.keys(requestData) as Array<keyof IGetCommentRequest>;
      keys.forEach((key, index) => {
        if (requestData?.[key])
          queryBuilder = queryBuilder.concat(`${key}=${requestData[key]}`);
        if (index < keys.length - 1) queryBuilder = queryBuilder.concat("&");
      });
      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all comments by test");
    }
  }

  async createComment(
    requestData: ICreateCommentRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create comment");
    }
  }

  async updateComment(
    requestData: IUpdateCommentRequest
  ): Promise<IBaseResponse<void>> {
    const requestUrl = `${this.path}`;
    const res: AxiosResponse = await http.put(requestUrl, requestData);
    return res.data?.data;
  }

  async getAllChildCommentsByParentComment(
    requestData: string
  ): Promise<IBaseResponse<ICommentResponse[]>> {
    try {
      const requestUrl = `${this.path}/${requestData}/sub-comments`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all comments by parent comment");
    }
  }
}

export const commentService = new CommentService();
