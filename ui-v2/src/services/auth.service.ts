import {
  IBaseResponse,
  IForgotPasswordRequest,
  ITokenResponse,
  IUserCredentialRequest,
} from "@/types";
import { http } from "@/lib";
import axios, { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class AuthService {
  private readonly path = `${BASE_URL}/auth`;

  async login(requestData: {
    username: string;
    password: string;
  }): Promise<IBaseResponse<{ accessToken: string; refreshToken: string }>> {
    // const requestUrl = this.path;
    const res: AxiosResponse = await axios.post("/api/login", requestData, {
      withCredentials: true,
    });
    return res.data?.data;
  }

  async signUp(
    requestData: IUserCredentialRequest
  ): Promise<IBaseResponse<void>> {
    // const requestUrl = this.path;
    const requestUrl = `${this.path}/signup`;
    const res: AxiosResponse = await http.post(requestUrl, requestData);
    return res.data?.data;
  }

  async loginSocial(requestData: {
    avatar: string;
    email: string;
    name: string;
    provider: string;
    username: string;
  }) {
    const res: AxiosResponse = await axios.post(
      "/api/login-social",
      requestData,
      {
        withCredentials: true,
      }
    );
    return res.data;
  }

  async refreshToken(
    requestData: string
  ): Promise<IBaseResponse<ITokenResponse>> {
    try {
      // const requestUrl = this.path;
      const requestUrl = `${this.path}/refresh-token`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to signup");
    }
  }

  async sendOTPForgotPassword(
    requestData: string
  ): Promise<IBaseResponse<ITokenResponse>> {
    try {
      const requestUrl = `${this.path}/forgot-password/otp?toEmail=${requestData}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to send OTP");
    }
  }

  async sendLinkVerification(
    requestData: string
  ): Promise<IBaseResponse<ITokenResponse>> {
    try {
      const requestUrl = `${this.path}/email/verification/otp?toEmail=${requestData}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to send OTP");
    }
  }

  async forgotPassword(
    requestData: IForgotPasswordRequest
  ): Promise<IBaseResponse<ITokenResponse>> {
    try {
      const requestUrl = `${this.path}/forgot-password/verification`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to forgot password");
    }
  }
}

export const authService = new AuthService();
