import {
  IBaseResponse,
  ICreateNewsCategoryRequest,
  INewsCategoryResponse,
  IUpdateNewsCategoryRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class NewsCategoryService {
  private readonly path = `${BASE_URL}/news-category`;

  async getAllNewsCategories(
    status?: string
  ): Promise<IBaseResponse<INewsCategoryResponse[]>> {
    try {
      let queryBuilder = "";
      if (status) {
        queryBuilder = queryBuilder.concat(`status=${status}`);
      }
      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all news categories");
    }
  }

  async createNewsCatgory(
    requestData: ICreateNewsCategoryRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create news category");
    }
  }

  async updateNewsCatgory(
    requestData: IUpdateNewsCategoryRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update news categories");
    }
  }
}

export const newsCategoryService = new NewsCategoryService();
