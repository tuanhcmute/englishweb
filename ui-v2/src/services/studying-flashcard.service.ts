import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICreateFlashcardRequest,
  ICreateStudyingFlashcardRequest,
  IStudyingFlashcardResponse,
  IUpdateStudyingFlashcardRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class StudyingFlashcardService {
  private readonly path = `${BASE_URL}/studying-flashcard`;

  async getAllStudyingFlashcardByUserCredential(
    userCredentialId: string
  ): Promise<IBaseResponse<IStudyingFlashcardResponse[]>> {
    try {
      const requestUrl = `${this.path}/?userCredentialId=${userCredentialId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      console.log({ res });
      return res.data?.data;
    } catch (error) {
      throw new Error(
        "Failed to fetch all studying flashcards by user credential"
      );
    }
  }

  async createStudyingFlashcard(
    requestData: ICreateStudyingFlashcardRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      console.log({ requestData });
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create studying flashcard");
    }
  }

  async deleteStudyingFlashcard(id: string): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.delete(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update studying flashcard");
    }
  }

  async updateStudyingFlashcard(
    requestData: IUpdateStudyingFlashcardRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update studying flashcard");
    }
  }
}

export const studyingFlashcardService = new StudyingFlashcardService();
