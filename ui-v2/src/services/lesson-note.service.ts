import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICreateLessonNoteRequest,
  ILessonNoteResponse,
  IUpdateLessonNoteRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class LessonNoteService {
  private readonly path = `${BASE_URL}/lesson-note`;
  async getAllLessonNotes(
    userCredentialId?: string,
    lessonId?: string
  ): Promise<IBaseResponse<ILessonNoteResponse[]>> {
    try {
      let queryBuilder = "";
      if (userCredentialId) {
        queryBuilder = queryBuilder.concat(
          `userCredentialId=${userCredentialId}&`
        );
      }
      if (lessonId) {
        queryBuilder = queryBuilder.concat(`lessonId=${lessonId}`);
      }
      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch lesson notes");
    }
  }

  async createLessonNote(
    requestData: ICreateLessonNoteRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to create lesson note");
    }
  }

  async updateLessonNote(
    requestData: IUpdateLessonNoteRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to update lesson note");
    }
  }

  async deleteLessonNote(id: string): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.delete(requestUrl);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to delete lesson note");
    }
  }
}

export const lessonNoteService = new LessonNoteService();
