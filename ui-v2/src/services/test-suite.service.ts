import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import { ICreateTestSuiteRequest, ITestSuiteResponse } from "@/types";
import { AxiosResponse } from "axios";

class TestSuiteService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  async getTestSuiteById(id: string): Promise<ITestSuiteResponse> {
    const requestUrl = `${this.path}/test-suite/${id}`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async getAllTestSuites(): Promise<ITestSuiteResponse[]> {
    const requestUrl = `${this.path}/test-suites`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createTestSuite(
    requestData: ICreateTestSuiteRequest
  ): Promise<ITestSuiteResponse> {
    const requestUrl = `${this.path}/test-suite`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data;
  }

  // async updateStaticPage(
  //   requestData: IUpdateStaticPageRequest
  // ): Promise<IBaseResponse<void>> {
  //   try {
  //     const requestUrl = `${this.path}`;
  //     const res: AxiosResponse = await http.put(requestUrl, requestData);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to update static page");
  //   }
  // }

  // async getStaticPageByCode(
  //   staticPageCode: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by page code");
  //   }
  // }
}

export const testSuiteService = new TestSuiteService();
