import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import { IBaseResponse, IGetRevenueRequest, IRevenueResponse } from "@/types";
import { AxiosResponse } from "axios";
import _ from "lodash";

class RevenueService {
  private readonly path = `${BASE_URL}/revenue`;

  async getAllRevenues(
    requestData?: IGetRevenueRequest
  ): Promise<IBaseResponse<IRevenueResponse[]>> {
    let queryBuilder = "";
    const keys = _.keys(requestData) as Array<keyof IGetRevenueRequest>;
    keys.forEach((key, index) => {
      if (requestData?.[key] === 0 || requestData?.[key])
        queryBuilder = queryBuilder.concat(`${key}=${requestData[key]}`);
      if (index < keys.length - 1) queryBuilder = queryBuilder.concat("&");
    });
    const requestUrl = `${this.path}?${queryBuilder}`;
    const res: AxiosResponse = await http.get(requestUrl);
    return res.data?.data;
  }
}

export const revenueService = new RevenueService();
