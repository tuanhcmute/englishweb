import {
  IBaseResponse,
  IGetReportCommentRequest,
  IReportCommentResponse,
  ICreateReportCommentRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";
import _ from "lodash";

class ReportCommentService {
  private readonly path = `${BASE_URL}/report-comment`;
  async getAllReportComments(
    requestData?: IGetReportCommentRequest
  ): Promise<IBaseResponse<IReportCommentResponse[]>> {
      let queryBuilder = "";
      const keys = _.keys(requestData) as Array<keyof IGetReportCommentRequest>;
      keys.forEach((key, index) => {
        if (requestData?.[key])
          queryBuilder = queryBuilder.concat(`${key}=${requestData[key]}`);
        if (index < keys.length - 1) queryBuilder = queryBuilder.concat("&");
      });
      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
  }

  async createReportComment(
    requestData: ICreateReportCommentRequest
  ): Promise<IBaseResponse<void>> {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
  }
}

export const reportCommentService = new ReportCommentService();
