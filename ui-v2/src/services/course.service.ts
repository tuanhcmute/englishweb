import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICourseResponse,
  ICreateCourseRequest,
  IGetCourseRequest,
  IUpdateCourseRequest,
} from "@/types";
import { AxiosResponse } from "axios";
import _ from "lodash";

class CourseService {
  private readonly path = `${BASE_URL}/course`;
  async getAllCourses(
    requestData?: IGetCourseRequest
  ): Promise<IBaseResponse<ICourseResponse[]>> {
    try {
      let queryBuilder = "";
      const keys = _.keys(requestData) as Array<keyof IGetCourseRequest>;
      keys.forEach((key, index) => {
        if (requestData?.[key])
          queryBuilder = queryBuilder.concat(`${key}=${requestData[key]}`);
        if (index < keys.length - 1) queryBuilder = queryBuilder.concat("&");
      });
      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch courses");
    }
  }

  async createCourse(
    requestData: ICreateCourseRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to create courses");
    }
  }

  async updateCourse(
    requestData: IUpdateCourseRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to create courses");
    }
  }

  async getCourseById(id: string): Promise<IBaseResponse<ICourseResponse>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch course by id");
    }
  }
}

export const courseService = new CourseService();
