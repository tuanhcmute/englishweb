import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IAssignDiscountRequest,
  IBaseResponse,
  ICreateDiscountRequest,
  IDiscountResponse,
  IUpdateDiscountRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class DiscountService {
  private readonly path = `${BASE_URL}/discount`;

  async getAllDiscounts(): Promise<IBaseResponse<IDiscountResponse[]>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch discounts");
    }
  }

  async createDiscount(
    requestData: ICreateDiscountRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      console.log({ res });
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to create discount");
    }
  }

  async updateDiscount(
    requestData: IUpdateDiscountRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to update discount");
    }
  }

  async assignDiscount(
    requestData: IAssignDiscountRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/assign`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to assign discount");
    }
  }
}

export const discountService = new DiscountService();
