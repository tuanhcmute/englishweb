import {
  IBaseResponse,
  ITestCategory,
  ITestCategoryRequest,
  ITestCategoryResponse,
  IUpdateTestCategoryRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class TestCategoryService {
  private readonly path = `${BASE_URL}/test-category`;
  async getAllTestCategories(): Promise<
    IBaseResponse<ITestCategoryResponse[]>
  > {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch test categories");
    }
  }

  async createTestCategory(
    requestData: ITestCategoryRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create test categories");
    }
  }

  async updateTestCategory(
    requestData: IUpdateTestCategoryRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update test categories");
    }
  }
}

export const testCategoryService = new TestCategoryService();
