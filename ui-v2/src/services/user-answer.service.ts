import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  IUserAnswerRequest,
  IUserAnswerResponse,
} from "@/types";
import { AxiosResponse } from "axios";

class UserAnswerService {
  private readonly path = `${BASE_URL}/user-answer`;

  async createNewUserAnswer(
    requestData: IUserAnswerRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const res: AxiosResponse = await http.post(this.path, requestData);
      return res.data?.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to create new user answer");
    }
  }

  async getAllUserAnswerByUserCredential(
    userCredentialId: string
  ): Promise<IBaseResponse<IUserAnswerResponse[]>> {
    try {
      const requestUrl = `${this.path}/?userCredentialId=${userCredentialId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to fetch user answer by credential");
    }
  }

  async getAllUserAnswerById(
    id: string
  ): Promise<IBaseResponse<IUserAnswerResponse>> {
    try {
      const requestUrl = `${this.path}/${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to fetch user answer by id");
    }
  }

  async countByTest(testId: string): Promise<IBaseResponse<number>> {
    try {
      const requestUrl = `${this.path}/count/byTest?value=${testId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to count user answer by test id");
    }
  }
}

export const userAnswerService = new UserAnswerService();
