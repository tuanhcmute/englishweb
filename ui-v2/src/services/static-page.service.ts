import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICreateStaticPageRequest,
  IStaticPageResponse,
  IUpdateStaticPageRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class StaticPageService {
  private readonly path = `${BASE_URL}/static-page`;

  async getStaticPageById(
    id: string
  ): Promise<IBaseResponse<IStaticPageResponse>> {
    try {
      const requestUrl = `${this.path}/staticPage?id=${id}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to get static page by id");
    }
  }

  async getAllStaticPages(): Promise<IBaseResponse<IStaticPageResponse[]>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to get all static pages");
    }
  }

  async createStaticPage(
    requestData: ICreateStaticPageRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create static page");
    }
  }

  async updateStaticPage(
    requestData: IUpdateStaticPageRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update static page");
    }
  }

  async getStaticPageByCode(
    staticPageCode: string
  ): Promise<IBaseResponse<IStaticPageResponse>> {
    try {
      const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to get static page by page code");
    }
  }
}

export const staticPageService = new StaticPageService();
