import {
  IBaseResponse,
  IPartInformationRequest,
  IPartInformationResponse,
  IUpdatePartInformationRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class PartInformationService {
  private readonly path = `${BASE_URL}/part-information`;

  async getAllPartInformationByTestCategory(
    testCategoryId: string
  ): Promise<IBaseResponse<IPartInformationResponse[]>> {
    try {
      // const requestUrl = this.path;
      const requestUrl = `${this.path}/byTestCategory?value=${testCategoryId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch part information");
    }
  }

  async createPartInformation(
    requestData: IPartInformationRequest
  ): Promise<IBaseResponse<void>> {
    try {
      // const requestUrl = this.path;
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create part information");
    }
  }

  async updatePartInformation(
    requestData: IUpdatePartInformationRequest
  ): Promise<IBaseResponse<void>> {
    try {
      // const requestUrl = this.path;
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update part information");
    }
  }
}

export const partInformationService = new PartInformationService();
