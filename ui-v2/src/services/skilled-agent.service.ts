import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import { ICreateSkilledAgentRequest, ISkilledAgentResponse } from "@/types";
import { AxiosResponse } from "axios";

class SkilledAgentService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  async getSkilledAgentById(id: string): Promise<ISkilledAgentResponse> {
    const requestUrl = `${this.path}/skilled-agent/${id}`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async getAllSkilledAgent(): Promise<ISkilledAgentResponse[]> {
    const requestUrl = `${this.path}/skilled-agents`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createSkilledAgent(
    requestData: ICreateSkilledAgentRequest
  ): Promise<ISkilledAgentResponse> {
    const requestUrl = `${this.path}/skilled-agent`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data;
  }

  // async updateStaticPage(
  //   requestData: IUpdateStaticPageRequest
  // ): Promise<IBaseResponse<void>> {
  //   try {
  //     const requestUrl = `${this.path}`;
  //     const res: AxiosResponse = await http.put(requestUrl, requestData);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to update static page");
  //   }
  // }

  // async getStaticPageByCode(
  //   staticPageCode: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by page code");
  //   }
  // }
}

export const skilledAgentService = new SkilledAgentService();
