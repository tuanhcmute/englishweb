import { BASE_URL, CHATBOT_BASE_URL } from "@/constants";
import { http, httpChatbotServer } from "@/lib";
import {
  ICreateSkilledAgentVersionRequest,
  ISkilledAgentVersionResponse,
  IUpdateSkilledAgentVersionRequest,
  IUpdateStatusSkilledAgentVersionRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class SkilledAgentVersionService {
  // Chatbot server
  private readonly path = `${CHATBOT_BASE_URL}`;
  private readonly serverPath = `${BASE_URL}`;

  async getSkilledAgentVersionById(
    id: string
  ): Promise<ISkilledAgentVersionResponse> {
    const requestUrl = `${this.path}/skilled-agent-version/${id}`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async getAllSkilledAgentVersions(
    agentId?: string
  ): Promise<ISkilledAgentVersionResponse[]> {
    let requestUrl = `${this.path}/skilled-agent-versions`;
    if (agentId) {
      requestUrl = requestUrl.concat(`/${agentId}`);
    }
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createSkilledAgentVersion(
    requestData: ICreateSkilledAgentVersionRequest
  ): Promise<ISkilledAgentVersionResponse> {
    const requestUrl = `${this.path}/skilled-agent-version`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data;
  }

  async updateSkilledAgentVersion(
    requestData: IUpdateSkilledAgentVersionRequest
  ): Promise<ISkilledAgentVersionResponse> {
    const requestUrl = `${this.path}/skilled-agent-version/${requestData.id}`;
    const res: AxiosResponse = await httpChatbotServer.put(
      requestUrl,
      requestData
    );
    return res.data;
  }

  async updateStatusSkilledAgentVersion(
    requestData: IUpdateStatusSkilledAgentVersionRequest
  ): Promise<void> {
    const requestUrl = `${this.serverPath}/skilled-agent-version/${requestData.skilled_agent_version_id}`;
    const res: AxiosResponse = await http.put(requestUrl, requestData);
    return res.data;
  }

  async getActivatedSkilledAgentVersion(): Promise<{
    data: ISkilledAgentVersionResponse;
  }> {
    const requestUrl = `${this.serverPath}/skilled-agent-version/activated`;
    const res: AxiosResponse = await http.get(requestUrl);
    return res.data?.data;
  }
}

export const skilledAgentVersionService = new SkilledAgentVersionService();
