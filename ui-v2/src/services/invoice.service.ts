import { IBaseResponse, ICreatePaymentRequest } from "@/types";
import { http } from "@/lib";
import { AxiosError, AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class InvoiceService {
  private readonly path = `${BASE_URL}/invoice`;
}

export const invoiceService = new InvoiceService();
