import { IBaseResponse, IOrderResponse } from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class OrderService {
  private readonly path = `${BASE_URL}/order`;
  async getAllOrders(
    userCredentialId?: string,
    paymentStatus?: string
  ): Promise<IBaseResponse<IOrderResponse[]>> {
    try {
      let queryBuilder = "";
      if (userCredentialId) {
        queryBuilder = queryBuilder.concat(
          `userCredentialId=${userCredentialId}`
        );
      }
      if (paymentStatus) {
        queryBuilder = queryBuilder.concat(`&paymentStatus=${paymentStatus}`);
      }

      const requestUrl = `${this.path}?${queryBuilder}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all orders by user credential");
    }
  }
}

export const orderService = new OrderService();
