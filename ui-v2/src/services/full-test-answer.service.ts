import {
  IBaseResponse,
  IFullTestAnswerResponse,
  IPartInformation,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class FullTestAnswerService {
  private readonly path = `${BASE_URL}/full-test-answer`;
  async getFullTestAnswerByTest(
    testId: string
  ): Promise<IBaseResponse<IFullTestAnswerResponse>> {
    try {
      // const requestUrl = this.path;
      const requestUrl = `${this.path}/byTest?value=${testId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch full test answer");
    }
  }
}

export const fullTestAnswerService = new FullTestAnswerService();
