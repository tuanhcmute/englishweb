import {
  IBaseResponse,
  IRoleResponse,
  ITestCategory,
  ITestCategoryRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class RoleService {
  private readonly path = `${BASE_URL}/role`;
  async getAllRoles(): Promise<IBaseResponse<IRoleResponse[]>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all roles");
    }
  }
}

export const roleService = new RoleService();
