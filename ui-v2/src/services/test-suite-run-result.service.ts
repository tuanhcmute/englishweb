import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import { ITestSuiteRunResultResponse } from "@/types";
import { AxiosResponse } from "axios";

class TestSuiteRunResultService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  async getTestSuiteRunResults(
    testSuiteId: string,
    agentVersionId: string
  ): Promise<ITestSuiteRunResultResponse[]> {
    const requestUrl = `${this.path}/test-run-results/${testSuiteId}/${agentVersionId}`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  // async updateStaticPage(
  //   requestData: IUpdateStaticPageRequest
  // ): Promise<IBaseResponse<void>> {
  //   try {
  //     const requestUrl = `${this.path}`;
  //     const res: AxiosResponse = await http.put(requestUrl, requestData);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to update static page");
  //   }
  // }

  // async getStaticPageByCode(
  //   staticPageCode: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by page code");
  //   }
  // }
}

export const testSuiteRunResultService = new TestSuiteRunResultService();
