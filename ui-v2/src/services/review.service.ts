import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICreateReviewRequest,
  IGetReviewRequest,
  IReviewResponse,
  IUpdateReviewRequest,
} from "@/types";
import { AxiosResponse } from "axios";
import _ from "lodash";

class ReviewService {
  private readonly path = `${BASE_URL}/review`;

  async getAllReviewsByCourse(
    courseId: string
  ): Promise<IBaseResponse<IReviewResponse[]>> {
    try {
      const requestUrl = `${this.path}/?courseId=${courseId}&isPublic=1`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch reviews by course");
    }
  }

  async getAllReviews(
    requestData?: IGetReviewRequest
  ): Promise<IBaseResponse<IReviewResponse[]>> {
    try {
      let queryBuilder = "";
      const keys = _.keys(requestData) as Array<keyof IGetReviewRequest>;
      keys.forEach((key, index) => {
        if (requestData?.[key])
          queryBuilder = queryBuilder.concat(`${key}=${requestData[key]}`);
        if (index < keys.length - 1) queryBuilder = queryBuilder.concat("&");
      });
      const requestUrl = `${this.path}?${queryBuilder}`;
      console.log({ requestUrl });
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch reviews by course");
    }
  }

  async createReview(
    requestData: ICreateReviewRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create review");
    }
  }

  async updateReview(
    requestData: IUpdateReviewRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update review");
    }
  }
}

export const reviewService = new ReviewService();
