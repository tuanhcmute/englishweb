import { CHATBOT_BASE_URL } from "@/constants";
import { httpChatbotServer } from "@/lib";
import {
  ICreateLLMModelRequest,
  ILLMModelModuleResponse,
  ILLMModelResponse,
  ISkilledAgentResponse,
  ITestSuiteResponse,
} from "@/types";
import { AxiosResponse } from "axios";

class LLMModelService {
  private readonly path = `${CHATBOT_BASE_URL}`;

  // async getStaticPageById(
  //   id: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?id=${id}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by id");
  //   }
  // }

  async getAllLLMModelModules(): Promise<ILLMModelModuleResponse[]> {
    const requestUrl = `${this.path}/llm_models_module`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async getAllLLMModels(): Promise<ILLMModelResponse[]> {
    const requestUrl = `${this.path}/llm_models`;
    const res: AxiosResponse = await httpChatbotServer.get(requestUrl);
    return res.data;
  }

  async createLLMModel(
    requestData: ICreateLLMModelRequest
  ): Promise<ILLMModelResponse> {
    const requestUrl = `${this.path}/llm_model`;
    const res: AxiosResponse = await httpChatbotServer.post(
      requestUrl,
      requestData
    );
    return res.data?.data;
  }

  // async updateStaticPage(
  //   requestData: IUpdateStaticPageRequest
  // ): Promise<IBaseResponse<void>> {
  //   try {
  //     const requestUrl = `${this.path}`;
  //     const res: AxiosResponse = await http.put(requestUrl, requestData);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to update static page");
  //   }
  // }

  // async getStaticPageByCode(
  //   staticPageCode: string
  // ): Promise<IBaseResponse<IStaticPageResponse>> {
  //   try {
  //     const requestUrl = `${this.path}/staticPage?pageCode=${staticPageCode}`;
  //     const res: AxiosResponse = await http.get(requestUrl);
  //     return res.data?.data;
  //   } catch (error) {
  //     throw new Error("Failed to get static page by page code");
  //   }
  // }
}

export const llmModelService = new LLMModelService();
