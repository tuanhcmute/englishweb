import {
  IBaseResponse,
  ICreateQuestionRequest,
  IUpdateQuestionRequest,
} from "@/types";
import { http } from "@/lib";
import { AxiosResponse } from "axios";
import { BASE_URL } from "@/constants";

class QuestionService {
  private readonly path = `${BASE_URL}/question`;

  async createNewQuestion(
    requestData: ICreateQuestionRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to create question");
    }
  }

  async updateQuestion(
    requestData: IUpdateQuestionRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}`;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to update question");
    }
  }

  async deleteQuestion(questionId: string): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = `${this.path}/${questionId}`;
      const res: AxiosResponse = await http.delete(requestUrl);
      return res.data?.data;
    } catch (e) {
      throw new Error("Failed to delete question");
    }
  }
}

export const questionService = new QuestionService();
