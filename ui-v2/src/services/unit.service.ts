import { BASE_URL } from "@/constants";
import { http } from "@/lib";
import {
  IBaseResponse,
  ICreateUnitRequest,
  IUnitResponse,
  IUpdateUnitRequest,
} from "@/types";
import { AxiosResponse } from "axios";

class UnitService {
  private readonly path = `${BASE_URL}/unit`;
  async getAllUnitsByCourse(
    courseId: string
  ): Promise<IBaseResponse<IUnitResponse[]>> {
    try {
      const requestUrl = `${this.path}?courseId${courseId}`;
      const res: AxiosResponse = await http.get(requestUrl);
      return res.data?.data;
    } catch (error) {
      throw new Error("Failed to fetch all units by course");
    }
  }

  async createUnit(
    requestData: ICreateUnitRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.post(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to create unit");
    }
  }

  async updateUnit(
    requestData: IUpdateUnitRequest
  ): Promise<IBaseResponse<void>> {
    try {
      const requestUrl = this.path;
      const res: AxiosResponse = await http.put(requestUrl, requestData);
      return res.data;
    } catch (error) {
      console.log({ error });
      throw new Error("Failed to update unit");
    }
  }
}

export const unitService = new UnitService();
