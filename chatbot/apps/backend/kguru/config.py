from pathlib import Path

DEFAULT_TIMEOUT = 3
TOOL_PATH = Path(__file__).parent.parent / 'tools'
LLM_MODEL_PATH = Path(__file__).parent.parent / 'llm_models'