from kguru.domains.models.http_model import SkilledAgentTestRequestModel
from kguru.domains.models.domain_model import DomainSkilledAgentTestModel


class SkilledAgentTestHttpMapper:
    @staticmethod
    def to_domain_model(
        model: SkilledAgentTestRequestModel,
    ) -> DomainSkilledAgentTestModel:
        return DomainSkilledAgentTestModel(**model.model_dump())
