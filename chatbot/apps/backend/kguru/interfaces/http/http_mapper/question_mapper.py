from kguru.domains.models.domain_model import DomainQuestionModel
from kguru.domains.models.http_model import QuestionRequestModel, QuestionResponseModel


class QuestionHttpMapper:
    @staticmethod
    def to_domain_model(model: QuestionRequestModel) -> DomainQuestionModel:
        return DomainQuestionModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainQuestionModel) -> QuestionResponseModel:
        return QuestionResponseModel(**model.model_dump())
