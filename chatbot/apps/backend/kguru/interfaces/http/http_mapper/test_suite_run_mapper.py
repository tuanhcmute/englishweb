from kguru.domains.models.domain_model import DomainTestSuiteRunModel
from kguru.domains.models.http_model import (
    TestSuiteRunRequestModel,
    TestSuiteRunReponseModel,
)


class TestSuiteRunHttpMapper:
    @staticmethod
    def to_domain_model(model: TestSuiteRunRequestModel) -> DomainTestSuiteRunModel:
        return DomainTestSuiteRunModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainTestSuiteRunModel) -> TestSuiteRunReponseModel:
        return TestSuiteRunReponseModel(**model.model_dump())
