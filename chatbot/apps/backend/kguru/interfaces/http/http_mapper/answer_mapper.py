from kguru.domains.models.domain_model import DomainAnswerModel
from kguru.domains.models.http_model import AnswerRequestModel, AnswerResponseModel


class AnswerHttpMapper:
    @staticmethod
    def to_domain_model(model: AnswerRequestModel) -> DomainAnswerModel:
        return DomainAnswerModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainAnswerModel) -> AnswerResponseModel:
        return AnswerResponseModel(**model.model_dump())
