from kguru.domains.models.domain_model import DomainLLMModel
from kguru.domains.models.http_model import LLMModelRequestModel, LLMModelResponseModel


class LLMModelHttpMapper:
    @staticmethod
    def to_domain_model(model: LLMModelRequestModel) -> DomainLLMModel:
        return DomainLLMModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainLLMModel) -> LLMModelResponseModel:
        return LLMModelResponseModel(**model.model_dump())
