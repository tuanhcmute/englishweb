from kguru.domains.models.domain_model import DomainTestSuiteModel
from kguru.domains.models.http_model import TestSuiteRequestModel, TestSuiteReponseModel


class TestSuiteHttpMapper:
    @staticmethod
    def to_domain_model(model: TestSuiteRequestModel) -> DomainTestSuiteModel:
        return DomainTestSuiteModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainTestSuiteModel) -> TestSuiteReponseModel:
        return TestSuiteReponseModel(**model.model_dump())
