from kguru.domains.models.domain_model import DomainToolModel
from kguru.domains.models.http_model import ToolRequestModel, ToolResponseModel


class ToolHttpMapper:
    @staticmethod
    def to_domain_model(model: ToolRequestModel) -> DomainToolModel:
        return DomainToolModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainToolModel) -> ToolResponseModel:
        return ToolResponseModel(**model.model_dump())
