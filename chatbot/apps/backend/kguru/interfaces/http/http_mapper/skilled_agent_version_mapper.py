from typing import List

from pydantic import TypeAdapter

from kguru.domains.models.domain_model import (
    DomainSkilledAgentVersionModel,
    DomainTestSuiteRunModel,
)
from kguru.domains.models.http_model import (
    SkilledAgentVersionRequestModel,
    SkilledAgentVersionReponseModel,
)


class SkilledAgentVersionHttpMapper:
    @staticmethod
    def to_domain_model(
        model: SkilledAgentVersionRequestModel,
    ) -> DomainSkilledAgentVersionModel:
        return DomainSkilledAgentVersionModel(**model.model_dump())

    @staticmethod
    def to_http_model(
        model: DomainSkilledAgentVersionModel,
    ) -> SkilledAgentVersionReponseModel:
        return SkilledAgentVersionReponseModel(**model.model_dump())
