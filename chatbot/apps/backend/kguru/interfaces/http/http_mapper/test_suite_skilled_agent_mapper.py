from kguru.domains.models.domain_model import DomainTestSuiteSkilledAgent
from kguru.domains.models.http_model import TestSuiteRequestModel, TestSuiteReponseModel


class TestSuiteSkilledAgentHttpMapper:
    @staticmethod
    def to_domain_model(model: TestSuiteRequestModel) -> DomainTestSuiteSkilledAgent:
        return DomainTestSuiteSkilledAgent(**model.model_dump())

    @staticmethod
    def to_http_model(
        model: DomainTestSuiteSkilledAgent,
    ) -> TestSuiteReponseModel:
        return TestSuiteReponseModel(**model.model_dump())
