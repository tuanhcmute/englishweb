from kguru.domains.models.domain_model import DomainAgentChatHistoryModel
from kguru.domains.models.http_model import (
    AgentChatHistoryRequestModel,
    AgentChatHistoryResponseModel,
)


class AgentChatHistoryHttpMapper:
    @staticmethod
    def to_domain_model(
        model: AgentChatHistoryRequestModel,
    ) -> DomainAgentChatHistoryModel:
        return DomainAgentChatHistoryModel(**model.model_dump())

    @staticmethod
    def to_http_model(
        model: DomainAgentChatHistoryModel,
    ) -> AgentChatHistoryResponseModel:
        return AgentChatHistoryResponseModel(**model.model_dump())
