from typing import List

from pydantic import TypeAdapter

from kguru.domains.models.domain_model import (
    DomainSkilledAgentModel,
    DomainSkilledAgentVersionModel,
    DomainTestSuiteModel,
)
from kguru.domains.models.http_model import (
    SkilledAgentRequestModel,
    SkilledAgentResponseModel,
    TestSuiteReponseModel,
    SkilledAgentVersionReponseModel,
)


class SkilledAgentHttpMapper:
    @staticmethod
    def to_domain_model(model: SkilledAgentRequestModel) -> DomainSkilledAgentModel:
        return DomainSkilledAgentModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainSkilledAgentModel) -> SkilledAgentResponseModel:
        return SkilledAgentResponseModel(**model.model_dump())
