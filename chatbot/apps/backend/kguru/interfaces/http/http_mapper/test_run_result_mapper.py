from kguru.domains.models.domain_model import DomainTestRunResultModel
from kguru.domains.models.http_model import (
    TestrunResultRequestModel,
    TestRunResultReponseModel,
)


class TestRunResultHttpMapper:
    @staticmethod
    def to_domain_model(model: TestrunResultRequestModel) -> DomainTestRunResultModel:
        return DomainTestRunResultModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainTestRunResultModel) -> TestRunResultReponseModel:
        return TestRunResultReponseModel(**model.model_dump())
