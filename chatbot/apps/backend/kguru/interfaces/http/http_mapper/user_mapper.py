from kguru.domains.models.domain_model import DomainUserModel
from kguru.domains.models.http_model import UserRequestModel, UserResponseModel


class UserHttpMapper:
    @staticmethod
    def to_domain_model(model: UserRequestModel) -> DomainUserModel:
        return DomainUserModel(**model.model_dump())

    @staticmethod
    def to_http_model(model: DomainUserModel) -> UserResponseModel:
        return UserResponseModel(**model.model_dump())
