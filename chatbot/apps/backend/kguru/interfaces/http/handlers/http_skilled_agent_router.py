from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    SkilledAgentRequestModel,
    SkilledAgentResponseModel,
)
from kguru.domains.models.domain_model import DomainSkilledAgentModel
from kguru.interfaces.http.http_mapper import SkilledAgentHttpMapper
from kguru.domains.exceptions import ValidationException


class HttpSkilledAgentRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/skilled-agent",
            self.create_new_skilled_agent,
            methods=["POST"],
            status_code=200,
            tags=["Skilled Agent"],
        )
        self.add_api_route(
            "/skilled-agent/{id}",
            self.read_skilled_agent,
            methods=["GET"],
            status_code=200,
            tags=["Skilled Agent"],
        )
        self.add_api_route(
            "/skilled-agent/{id}",
            self.update_skilled_agent,
            methods=["PUT"],
            status_code=200,
            tags=["Skilled Agent"],
        )
        self.add_api_route(
            "/skilled-agent/{id}",
            self.delete_skilled_agent,
            methods=["DELETE"],
            status_code=200,
            tags=["Skilled Agent"],
        )
        self.add_api_route(
            "/skilled-agents",
            self.get_all_skilled_agent,
            methods=["GET"],
            status_code=200,
            tags=["Skilled Agent"],
        )

    async def create_new_skilled_agent(
        self, model: SkilledAgentRequestModel
    ) -> SkilledAgentResponseModel:
        domain_model: DomainSkilledAgentModel = SkilledAgentHttpMapper.to_domain_model(
            model=model
        )
        data_create_result: DomainSkilledAgentModel = (
            await self.object_manage_controller.crud_object.crud_skilled_agent.create_new_object(
                base_object_model=domain_model
            )
        )
        if not data_create_result:
            raise ValidationException("Create new skilled agent model failed")

        return SkilledAgentHttpMapper.to_http_model(model=data_create_result)

    async def read_skilled_agent(self, id: str) -> SkilledAgentResponseModel:
        data_read_result: DomainSkilledAgentModel = (
            await self.object_manage_controller.crud_object.crud_skilled_agent.read_object(
                id=id
            )
        )
        return SkilledAgentHttpMapper.to_http_model(model=data_read_result)

    async def update_skilled_agent(
        self, id: str, model: SkilledAgentRequestModel
    ) -> SkilledAgentResponseModel:
        domain_model: DomainSkilledAgentModel = SkilledAgentHttpMapper.to_domain_model(
            model=model
        )
        data_update_result: DomainSkilledAgentModel = (
            await self.object_manage_controller.crud_object.crud_skilled_agent.update_object(
                id=id, base_object_model=domain_model
            )
        )
        if not data_update_result:
            raise ValidationException("Update skilled agent model failed")
        return SkilledAgentHttpMapper.to_http_model(model=data_update_result)

    async def delete_skilled_agent(self, id: str):
        await self.object_manage_controller.crud_object.crud_skilled_agent.delete_object(
            id=id
        )

    async def get_all_skilled_agent(self):
        skilled_agent_models: List[DomainSkilledAgentModel] = (
            await self.object_manage_controller.crud_object.crud_skilled_agent.get_all()
        )
        return TypeAdapter(List[SkilledAgentResponseModel]).dump_python(
            skilled_agent_models  # type: ignore
        )
