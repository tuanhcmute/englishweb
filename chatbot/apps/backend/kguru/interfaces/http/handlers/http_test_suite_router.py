from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    TestSuiteRequestModel,
    TestSuiteReponseModel,
)
from kguru.domains.models.domain_model import DomainTestSuiteModel
from kguru.interfaces.http.http_mapper import TestSuiteHttpMapper
from kguru.domains.exceptions import ValidationException


class HttpTestSuiteRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/test-suite",
            self.create_new_test_suite,
            methods=["POST"],
            status_code=200,
            tags=["Test suite"],
        )
        self.add_api_route(
            "/test-suite/{id}",
            self.read_test_suite,
            methods=["GET"],
            status_code=200,
            tags=["Test suite"],
        )
        self.add_api_route(
            "/test-suite/{id}",
            self.update_test_suite,
            methods=["PUT"],
            status_code=200,
            tags=["Test suite"],
        )
        self.add_api_route(
            "/test-suite/{id}",
            self.delete_test_suite,
            methods=["DELETE"],
            status_code=200,
            tags=["Test suite"],
        )
        self.add_api_route(
            "/test-suites",
            self.get_all_test_suite,
            methods=["GET"],
            status_code=200,
            tags=["Test suite"],
        )

    async def create_new_test_suite(
        self, model: TestSuiteRequestModel
    ) -> TestSuiteReponseModel:
        domain_model: DomainTestSuiteModel = TestSuiteHttpMapper.to_domain_model(
            model=model
        )
        data_create_result: DomainTestSuiteModel = (
            await self.object_manage_controller.crud_object.crud_test_suite.create_new_object(
                base_object_model=domain_model
            )
        )
        if not data_create_result:
            raise ValidationException("Create new test suite model failed")

        return TestSuiteHttpMapper.to_http_model(data_create_result)

    async def read_test_suite(self, id: str) -> TestSuiteReponseModel:
        data_read_result: DomainTestSuiteModel = (
            await self.object_manage_controller.crud_object.crud_test_suite.read_object(
                id=id
            )
        )
        return TestSuiteHttpMapper.to_http_model(data_read_result)

    async def update_test_suite(
        self, id: str, model: TestSuiteRequestModel
    ) -> TestSuiteReponseModel:
        domain_model: DomainTestSuiteModel = TestSuiteHttpMapper.to_domain_model(
            model=model
        )
        data_update_result: DomainTestSuiteModel = (
            await self.object_manage_controller.crud_object.crud_test_suite.update_object(
                id=id, base_object_model=domain_model
            )
        )
        if not data_update_result:
            raise ValidationException("Update test suite model failed")
        return TestSuiteHttpMapper.to_http_model(data_update_result)

    async def delete_test_suite(self, id: str):
        await self.object_manage_controller.crud_object.crud_test_suite.delete_object(
            id=id
        )

    async def get_all_test_suite(self) -> List[TestSuiteReponseModel]:
        test_suite_models: List[DomainTestSuiteModel] = (
            await self.object_manage_controller.crud_object.crud_test_suite.get_all()
        )

        return TypeAdapter(List[TestSuiteReponseModel]).dump_python(test_suite_models)  # type: ignore
