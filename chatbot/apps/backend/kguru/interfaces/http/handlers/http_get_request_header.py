from typing import Any, Dict, List

from fastapi import Request
from fastapi import APIRouter


class HttpGetRequestHeaderRouter(APIRouter):
    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.add_api_route(
            "/request-header",
            self.read_request_header,
            methods=["GET"],
            status_code=200,
            tags=["Request header"],
        )

    async def read_request_header(self, request: Request) -> Dict[str, List[str]]:
        return {"keys": request.headers.keys(), "values": request.headers.values()}
