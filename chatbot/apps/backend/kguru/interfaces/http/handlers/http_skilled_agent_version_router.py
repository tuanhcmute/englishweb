from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    SkilledAgentVersionRequestModel,
    SkilledAgentVersionReponseModel,
)
from kguru.domains.models.domain_model import DomainSkilledAgentVersionModel
from kguru.interfaces.http.http_mapper import SkilledAgentVersionHttpMapper
from kguru.domains.exceptions import ValidationException


class HttpSkilledAgentVersionRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/skilled-agent-version",
            self.create_new_skilled_agent_version,
            methods=["POST"],
            status_code=200,
            tags=["Skilled Agent Version"],
        )
        self.add_api_route(
            "/skilled-agent-version/{id}",
            self.read_skilled_agent_version,
            methods=["GET"],
            status_code=200,
            tags=["Skilled Agent Version"],
        )
        self.add_api_route(
            "/skilled-agent-version/{id}",
            self.update_skilled_agent_version,
            methods=["PUT"],
            status_code=200,
            tags=["Skilled Agent Version"],
        )
        self.add_api_route(
            "/skilled-agent-version/{id}",
            self.delete_skilled_agent_version,
            methods=["DELETE"],
            status_code=200,
            tags=["Skilled Agent Version"],
        )
        self.add_api_route(
            "/skilled-agent-versions",
            self.get_all_skilled_agent_version,
            methods=["GET"],
            status_code=200,
            tags=["Skilled Agent Version"],
        )
        self.add_api_route(
            "/skilled-agent-versions/{skilled_agent_id}",
            self.get_skilled_agent_version_by_skilled_agent_id_and_skilled_agent_version_id,
            methods=["GET"],
            status_code=200,
            tags=["Skilled Agent Version"],
        )

    async def create_new_skilled_agent_version(
        self, model: SkilledAgentVersionRequestModel
    ) -> SkilledAgentVersionReponseModel:
        domain_model: DomainSkilledAgentVersionModel = (
            SkilledAgentVersionHttpMapper.to_domain_model(model=model)
        )

        data_create_result: DomainSkilledAgentVersionModel = (
            await self.object_manage_controller.crud_object.crud_skilled_agent_version.create_new_object(
                base_object_model=domain_model
            )
        )
        if not data_create_result:
            raise ValidationException("Create new skilled agent version failed")

        return SkilledAgentVersionHttpMapper.to_http_model(model=data_create_result)

    async def read_skilled_agent_version(
        self, id: str
    ) -> SkilledAgentVersionReponseModel:
        data_read_result: DomainSkilledAgentVersionModel = (
            await self.object_manage_controller.crud_object.crud_skilled_agent_version.read_object(
                id=id
            )
        )
        return SkilledAgentVersionHttpMapper.to_http_model(model=data_read_result)

    async def update_skilled_agent_version(
        self, id: str, model: SkilledAgentVersionRequestModel
    ) -> SkilledAgentVersionReponseModel:
        domain_model: DomainSkilledAgentVersionModel = (
            SkilledAgentVersionHttpMapper.to_domain_model(model=model)
        )
        data_update_result: DomainSkilledAgentVersionModel = (
            await self.object_manage_controller.crud_object.crud_skilled_agent_version.update_object(
                id=id, base_object_model=domain_model
            )
        )
        if not data_update_result:
            raise ValidationException("Update skilled agent version failed")
        return SkilledAgentVersionHttpMapper.to_http_model(model=data_update_result)

    async def delete_skilled_agent_version(self, id: str):
        await self.object_manage_controller.crud_object.crud_skilled_agent_version.delete_object(
            id=id
        )

    async def get_all_skilled_agent_version(
        self,
    ) -> List[SkilledAgentVersionReponseModel]:
        skilled_agent_version_models: List[DomainSkilledAgentVersionModel] = (
            await self.object_manage_controller.crud_object.crud_skilled_agent_version.get_all()
        )
        return TypeAdapter(List[SkilledAgentVersionReponseModel]).dump_python(
            skilled_agent_version_models  # type: ignore
        )

    async def get_skilled_agent_version_by_skilled_agent_id_and_skilled_agent_version_id(
        self, skilled_agent_id: str
    ) -> List[SkilledAgentVersionReponseModel]:
        skilled_agent_version_models: List[DomainSkilledAgentVersionModel] = (
            await self.object_manage_controller.crud_object.crud_skilled_agent_version.get_objects_by_specific_foreign_key(
                skilled_agent_id=skilled_agent_id
            )
        )
        return TypeAdapter(List[SkilledAgentVersionReponseModel]).dump_python(
            skilled_agent_version_models  # type: ignore
        )
