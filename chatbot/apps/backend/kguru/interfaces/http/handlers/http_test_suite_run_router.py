from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    TestSuiteRunRequestModel,
    TestSuiteRunReponseModel,
)
from kguru.domains.models.domain_model import DomainTestSuiteRunModel
from kguru.interfaces.http.http_mapper import TestSuiteRunHttpMapper
from kguru.domains.exceptions import ValidationException


class HttpTestSuiteRunRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/test-suite-run",
            self.create_new_test_suite_run,
            methods=["POST"],
            status_code=200,
            tags=["Test suite run"],
        )
        self.add_api_route(
            "/test-suite-runs",
            self.create_new_test_suite_runs,
            methods=["POST"],
            status_code=200,
            tags=["Test suite run"],
        )
        self.add_api_route(
            "/test-suite-run/{id}",
            self.read_test_suite_run,
            methods=["GET"],
            status_code=200,
            tags=["Test suite run"],
        )
        self.add_api_route(
            "/test-suite-run/{id}",
            self.update_test_suite_run,
            methods=["PUT"],
            status_code=200,
            tags=["Test suite run"],
        )
        self.add_api_route(
            "/test-suite-run/{id}",
            self.delete_test_suite_run,
            methods=["DELETE"],
            status_code=200,
            tags=["Test suite run"],
        )
        self.add_api_route(
            "/test-suite-runs",
            self.get_all_test_suite_run,
            methods=["GET"],
            status_code=200,
            tags=["Test suite run"],
        )

    async def create_new_test_suite_run(
        self, model: TestSuiteRunRequestModel
    ) -> TestSuiteRunReponseModel:
        domain_model: DomainTestSuiteRunModel = TestSuiteRunHttpMapper.to_domain_model(
            model=model
        )
        data_create_result: DomainTestSuiteRunModel = (
            await self.object_manage_controller.crud_object.crud_test_suite_run.create_new_object(
                base_object_model=domain_model
            )
        )
        if not data_create_result:
            raise ValidationException("Create new test suite run model failed")

        return TestSuiteRunHttpMapper.to_http_model(model=data_create_result)

    async def create_new_test_suite_runs(self, models: List[TestSuiteRunRequestModel]):
        domain_models: List[DomainTestSuiteRunModel] = TypeAdapter(
            List[DomainTestSuiteRunModel]
        ).dump_python(
            models  # type: ignore
        )

        await self.object_manage_controller.crud_object.crud_test_suite_run.create_new_objects(
            base_object_models=domain_models
        )

    async def read_test_suite_run(self, id: str) -> TestSuiteRunReponseModel:
        data_read_result: DomainTestSuiteRunModel = (
            await self.object_manage_controller.crud_object.crud_test_suite_run.read_object(
                id=id
            )
        )
        return TestSuiteRunHttpMapper.to_http_model(model=data_read_result)

    async def update_test_suite_run(
        self, id: str, model: TestSuiteRunRequestModel
    ) -> TestSuiteRunReponseModel:
        domain_model: DomainTestSuiteRunModel = TestSuiteRunHttpMapper.to_domain_model(
            model=model
        )
        data_update_result: DomainTestSuiteRunModel = (
            await self.object_manage_controller.crud_object.crud_test_suite_run.update_object(
                id=id, base_object_model=domain_model
            )
        )
        if not data_update_result:
            raise ValidationException("Update test suite run model failed")
        return TestSuiteRunHttpMapper.to_http_model(model=data_update_result)

    async def delete_test_suite_run(self, id: str):
        await self.object_manage_controller.crud_object.crud_test_suite_run.delete_object(
            id=id
        )

    async def get_all_test_suite_run(self) -> List[TestSuiteRunReponseModel]:
        test_suite_run_models: List[DomainTestSuiteRunModel] = (
            await self.object_manage_controller.crud_object.crud_test_suite_run.get_all()
        )
        return [
            TestSuiteRunHttpMapper.to_http_model(result)
            for result in test_suite_run_models
        ]
