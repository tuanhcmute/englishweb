from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    QuestionRequestModel,
    QuestionResponseModel,
)
from kguru.domains.models.domain_model import DomainQuestionModel
from kguru.interfaces.http.http_mapper import QuestionHttpMapper
from kguru.domains.exceptions import ValidationException


class HttpQuestionRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/question",
            self.create_new_question,
            methods=["POST"],
            status_code=200,
            tags=["Question"],
        )
        self.add_api_route(
            "/question/{id}",
            self.read_question,
            methods=["GET"],
            status_code=200,
            tags=["Question"],
        )
        self.add_api_route(
            "/question/{id}",
            self.update_question,
            methods=["PUT"],
            status_code=200,
            tags=["Question"],
        )
        self.add_api_route(
            "/question/{id}",
            self.delete_question,
            methods=["DELETE"],
            status_code=200,
            tags=["Question"],
        )
        self.add_api_route(
            "/questions",
            self.get_all_question,
            methods=["GET"],
            status_code=200,
            tags=["Question"],
        )

    async def create_new_question(
        self, model: QuestionRequestModel
    ) -> QuestionResponseModel:
        domain_model: DomainQuestionModel = QuestionHttpMapper.to_domain_model(
            model=model
        )
        data_create_result: DomainQuestionModel = (
            await self.object_manage_controller.crud_object.crud_question.create_new_object(
                base_object_model=domain_model
            )
        )
        if not data_create_result:
            raise ValidationException("Create new question model failed")

        return QuestionHttpMapper.to_http_model(model=data_create_result)

    async def read_question(self, id: str) -> QuestionResponseModel:
        data_read_result: DomainQuestionModel = (
            await self.object_manage_controller.crud_object.crud_question.read_object(
                id=id
            )
        )
        return QuestionHttpMapper.to_http_model(model=data_read_result)

    async def update_question(
        self, id: str, model: QuestionRequestModel
    ) -> QuestionResponseModel:
        domain_model: DomainQuestionModel = QuestionHttpMapper.to_domain_model(
            model=model
        )
        data_update_result: DomainQuestionModel = (
            await self.object_manage_controller.crud_object.crud_question.update_object(
                id=id, base_object_model=domain_model
            )
        )
        if not data_update_result:
            raise ValidationException("Update question model failed")

        return QuestionHttpMapper.to_http_model(model=data_update_result)

    async def delete_question(self, id: str):
        await self.object_manage_controller.crud_object.crud_question.delete_object(
            id=id
        )

    async def get_all_question(self) -> List[QuestionResponseModel]:
        question_models: List[DomainQuestionModel] = (
            await self.object_manage_controller.crud_object.crud_question.get_all()
        )
        return TypeAdapter(List[QuestionResponseModel]).dump_python(question_models)  # type: ignore
