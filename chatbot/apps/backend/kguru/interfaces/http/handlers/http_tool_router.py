from typing import Any, List, Dict
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.services.tools import ToolLoader
from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    ToolRequestModel,
    ToolResponseModel,
)
from kguru.domains.models.domain_model import DomainToolModel
from kguru.interfaces.http.http_mapper import ToolHttpMapper
from kguru.domains.exceptions import ValidationException

from kguru import config


class HttpToolRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.tool_loader = ToolLoader(config.TOOL_PATH)

        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/tool",
            self.create_new_tool,
            methods=["POST"],
            status_code=200,
            tags=["Tool"],
        )
        self.add_api_route(
            "/tool/{id}",
            self.read_tool,
            methods=["GET"],
            status_code=200,
            tags=["Tool"],
        )
        self.add_api_route(
            "/tool/{id}",
            self.update_tool,
            methods=["PUT"],
            status_code=200,
            tags=["Tool"],
        )
        self.add_api_route(
            "/tool/{id}",
            self.delete_tool,
            methods=["DELETE"],
            status_code=200,
            tags=["Tool"],
        )
        self.add_api_route(
            "/tools",
            self.get_all_tool,
            methods=["GET"],
            status_code=200,
            tags=["Tool"],
        )
        self.add_api_route(
            "/tools_module",
            self.get_tool_from_module,
            methods=["GET"],
            status_code=200,
            tags=["Tool"],
        )

    async def create_new_tool(self, model: ToolRequestModel) -> ToolResponseModel:
        domain_tool: DomainToolModel = ToolHttpMapper.to_domain_model(model=model)
        data_create_result: DomainToolModel = (
            await self.object_manage_controller.crud_object.crud_tool.create_new_object(
                base_object_model=domain_tool
            )
        )
        if not data_create_result:
            raise ValidationException("Create new tool failed")

        return ToolHttpMapper.to_http_model(model=data_create_result)

    async def read_tool(self, id: str) -> ToolResponseModel:
        data_read_result: DomainToolModel = (
            await self.object_manage_controller.crud_object.crud_tool.read_object(id=id)
        )
        return ToolHttpMapper.to_http_model(model=data_read_result)

    async def update_tool(self, id: str, model: ToolRequestModel) -> ToolResponseModel:
        domain_tool: DomainToolModel = ToolHttpMapper.to_domain_model(model=model)
        data_update_result: DomainToolModel = (
            await self.object_manage_controller.crud_object.crud_tool.update_object(
                id=id, base_object_model=domain_tool
            )
        )
        if not data_update_result:
            raise ValidationException("Update tool failed")
        return ToolHttpMapper.to_http_model(model=data_update_result)

    async def delete_tool(self, id: str):
        await self.object_manage_controller.crud_object.crud_tool.delete_object(id=id)

    async def get_all_tool(self) -> List[ToolResponseModel]:
        tool_models: List[DomainToolModel] = (
            await self.object_manage_controller.crud_object.crud_tool.get_all()
        )
        return TypeAdapter(List[ToolResponseModel]).dump_python(tool_models)  # type: ignore

    async def get_tool_from_module(self) -> List[ToolResponseModel]:
        self.tool_loader.load_classes()
        tools: List[dict] = self.tool_loader.get_class_names()
        tool_models: List[DomainToolModel] = [DomainToolModel(**tool) for tool in tools]
        return TypeAdapter(List[ToolResponseModel]).dump_python(tool_models)  # type: ignore
