from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    TestrunResultRequestModel,
    TestRunResultReponseModel,
)
from kguru.domains.models.domain_model import DomainTestRunResultModel
from kguru.interfaces.http.http_mapper import TestRunResultHttpMapper
from kguru.domains.exceptions import ValidationException


class HttpTestRunResultRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/test-run-result",
            self.create_new_test_run_result,
            methods=["POST"],
            status_code=200,
            tags=["Test run result"],
        )
        self.add_api_route(
            "/test-run-result/{id}",
            self.read_test_run_result,
            methods=["GET"],
            status_code=200,
            tags=["Test run result"],
        )
        self.add_api_route(
            "/test-run-result/{id}",
            self.update_test_run_result,
            methods=["PUT"],
            status_code=200,
            tags=["Test run result"],
        )
        self.add_api_route(
            "/test-run-result/{id}",
            self.delete_test_run_result,
            methods=["DELETE"],
            status_code=200,
            tags=["Test run result"],
        )
        self.add_api_route(
            "/test-run-results",
            self.get_all_test_run_result,
            methods=["GET"],
            status_code=200,
            tags=["Test run result"],
        )
        self.add_api_route(
            "/test-run-results/{test_suite_run_id}/{skilled_agent_version_id}",
            self.get_test_run_results_by_test_suite_run_id_and_skilled_agent_version_id,
            methods=["GET"],
            status_code=200,
            tags=["Test run result"],
        )

    async def create_new_test_run_result(
        self, model: TestrunResultRequestModel
    ) -> TestRunResultReponseModel:
        domain_model: DomainTestRunResultModel = (
            TestRunResultHttpMapper.to_domain_model(model=model)
        )
        data_create_result: DomainTestRunResultModel = (
            await self.object_manage_controller.crud_object.crud_test_run_result.create_new_object(
                base_object_model=domain_model
            )
        )
        if not data_create_result:
            raise ValidationException("Create new test run result model failed")

        return TestRunResultHttpMapper.to_http_model(model=data_create_result)

    async def read_test_run_result(self, id: str) -> TestRunResultReponseModel:
        data_read_result: DomainTestRunResultModel = (
            await self.object_manage_controller.crud_object.crud_test_run_result.read_object(
                id=id
            )
        )
        return TestRunResultHttpMapper.to_http_model(model=data_read_result)

    async def update_test_run_result(
        self, id: str, model: TestrunResultRequestModel
    ) -> TestRunResultReponseModel:
        domain_model: DomainTestRunResultModel = (
            TestRunResultHttpMapper.to_domain_model(model=model)
        )
        data_update_result: DomainTestRunResultModel = (
            await self.object_manage_controller.crud_object.crud_test_run_result.update_object(
                id=id, base_object_model=domain_model
            )
        )
        if not data_update_result:
            raise ValidationException("Update test run result model failed")
        return TestRunResultHttpMapper.to_http_model(model=data_update_result)

    async def delete_test_run_result(self, id: str):
        await self.object_manage_controller.crud_object.crud_test_run_result.delete_object(
            id=id
        )

    async def get_all_test_run_result(self) -> List[TestRunResultReponseModel]:
        test_run_result_models: List[DomainTestRunResultModel] = (
            await self.object_manage_controller.crud_object.crud_test_run_result.get_all()
        )
        return TypeAdapter(List[TestRunResultReponseModel]).dump_python(
            test_run_result_models  # type: ignore
        )

    async def get_test_run_results_by_test_suite_run_id_and_skilled_agent_version_id(
        self, test_suite_run_id: str, skilled_agent_version_id: str
    ) -> List[TestRunResultReponseModel]:
        test_run_result_models: List[DomainTestRunResultModel] = (
            await self.object_manage_controller.crud_object.crud_test_run_result.get_objects_by_specific_foreign_key(
                test_suite_run_id=test_suite_run_id,
                skilled_agent_version_id=skilled_agent_version_id,
            )
        )
        return TypeAdapter(List[TestRunResultReponseModel]).dump_python(
            test_run_result_models  # type: ignore
        )
