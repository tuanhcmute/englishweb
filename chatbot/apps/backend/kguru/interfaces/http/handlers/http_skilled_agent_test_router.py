from typing import Any, List

from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import SkilledAgentTestController
from kguru.domains.models.domain_model import (
    DomainSkilledAgentTestModel,
    DomainTestRunResultModel,
)
from kguru.domains.models.http_model import (
    SkilledAgentTestRequestModel,
    TestRunResultReponseModel,
)
from kguru.interfaces.http.http_mapper import SkilledAgentTestHttpMapper


class HttpSkilledAgentTestRouter(APIRouter):
    skilled_agent_test_controller: SkilledAgentTestController

    def __init__(
        self,
        skilled_agent_test_controller: SkilledAgentTestController,
        *args: Any,
        **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.skilled_agent_test_controller = skilled_agent_test_controller
        self.add_api_route(
            "/skilled-agent-test",
            self.run_skilled_agent_test,
            methods=["POST"],
            status_code=200,
            tags=["Skilled Agent Test"],
        )
        self.add_api_route(
            "/re-run-skilled-agent-test",
            self.re_run_skilled_agent_test,
            methods=["POST"],
            status_code=200,
            tags=["Skilled Agent Test"],
        )

    async def run_skilled_agent_test(
        self, model: SkilledAgentTestRequestModel
    ) -> List[TestRunResultReponseModel]:
        domain_model: DomainSkilledAgentTestModel = (
            SkilledAgentTestHttpMapper.to_domain_model(model=model)
        )

        result: List[DomainTestRunResultModel] = (
            await self.skilled_agent_test_controller.run_skilled_agent_test(
                model=domain_model
            )
        )

        return TypeAdapter(List[TestRunResultReponseModel]).validate_python(
            TypeAdapter(List[DomainTestRunResultModel]).dump_python(result)
        )

    async def re_run_skilled_agent_test(
        self, model: SkilledAgentTestRequestModel
    ) -> List[TestRunResultReponseModel]:
        print("===========================", model)
        domain_model: DomainSkilledAgentTestModel = (
            SkilledAgentTestHttpMapper.to_domain_model(model=model)
        )

        result: List[DomainTestRunResultModel] = (
            await self.skilled_agent_test_controller.re_run_skilled_agent_test(
                model=domain_model
            )
        )

        return TypeAdapter(List[TestRunResultReponseModel]).validate_python(
            TypeAdapter(List[DomainTestRunResultModel]).dump_python(result)
        )
