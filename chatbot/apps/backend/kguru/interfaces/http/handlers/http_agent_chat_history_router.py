from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.domains.controllers import AgentChatController
from kguru.domains.models.domain_model import DomainAgentChatHistoryModel
from kguru.domains.models.http_model import (
    AgentChatHistoryRequestModel,
    AgentChatHistoryResponseModel,
)
from kguru.interfaces.http.http_mapper import AgentChatHistoryHttpMapper
import logging

logger = logging.getLogger(__name__)

class HttpAgentChatHistoryRouter(APIRouter):
    agent_chat_controller: AgentChatController

    def __init__(
        self, agent_chat_controller: AgentChatController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.agent_chat_controller = agent_chat_controller
        self.add_api_route(
            "/skilled-agent-chat",
            self.create_new_agent_chat,
            methods=["POST"],
            status_code=200,
            tags=["Skilled Agent Chat"],
        )

        self.add_api_route(
            "/skilled-agent-chats",
            self.get_agent_chat_history_by_user_and_skilled_agent_version,
            methods=["GET"],
            status_code=200,
            tags=["Skilled Agent Chat"],
        )

    async def create_new_agent_chat(
        self, model: AgentChatHistoryRequestModel
    ) -> AgentChatHistoryResponseModel:
        logger.info("============================= create_new_agent_chat =============================") 
        logger.info(str(model)) 
        logger.info("============================= create_new_agent_chat =============================") 
        domain_model: DomainAgentChatHistoryModel = (
            AgentChatHistoryHttpMapper.to_domain_model(model=model)
        )
        result: DomainAgentChatHistoryModel = (
            await self.agent_chat_controller.create_new_chat_history(model=domain_model)
        )
        return AgentChatHistoryHttpMapper.to_http_model(model=result)

    async def get_agent_chat_history_by_user_and_skilled_agent_version(
        self,
        skilled_agent_version_id: str,
        user_id: str,
        offset: int = 0,
        limit: int = 30,
    ) -> List[AgentChatHistoryResponseModel]:
        results: List[DomainAgentChatHistoryModel] = (
            await self.agent_chat_controller.get_chat_history_by_user_and_skilled_version(
                skilled_agent_version_id=skilled_agent_version_id,
                user_id=user_id,
                offset=offset,
                limit=limit,
            )
        )

        return TypeAdapter(List[AgentChatHistoryResponseModel]).dump_python(
            TypeAdapter(List[DomainAgentChatHistoryModel]).dump_python(results)
        )
