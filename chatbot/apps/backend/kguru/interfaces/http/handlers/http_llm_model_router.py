from typing import Any, List
from pydantic import TypeAdapter

from fastapi import APIRouter

from kguru.services.llm_model import LLMModelLoader
from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import (
    LLMModelRequestModel,
    LLMModelResponseModel,
)
from kguru.domains.models.domain_model import DomainLLMModel
from kguru.interfaces.http.http_mapper import LLMModelHttpMapper
from kguru.domains.exceptions import ValidationException
from kguru import config


class HttpLLMModelRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.llm_model_loader = LLMModelLoader(config.LLM_MODEL_PATH)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/llm_model",
            self.create_new_llm_model,
            methods=["POST"],
            status_code=200,
            tags=["LLM Model"],
        )
        self.add_api_route(
            "/llm_model/{id}",
            self.read_llm_model,
            methods=["GET"],
            status_code=200,
            tags=["LLM Model"],
        )
        self.add_api_route(
            "/llm_model/{id}",
            self.update_llm_model,
            methods=["PUT"],
            status_code=200,
            tags=["LLM Model"],
        )
        self.add_api_route(
            "/llm_model/{id}",
            self.delete_llm_model,
            methods=["DELETE"],
            status_code=200,
            tags=["LLM Model"],
        )
        self.add_api_route(
            "/llm_models",
            self.get_all_llm_model,
            methods=["GET"],
            status_code=200,
            tags=["LLM Model"],
        )
        self.add_api_route(
            "/llm_models_module",
            self.get_all_models,
            methods=["GET"],
            status_code=200,
            tags=["LLM Model"],
        )

    async def create_new_llm_model(
        self, model: LLMModelRequestModel
    ) -> LLMModelResponseModel:
        domain_model: DomainLLMModel = LLMModelHttpMapper.to_domain_model(model=model)
        data_create_result: DomainLLMModel = (
            await self.object_manage_controller.crud_object.crud_llm_model.create_new_object(
                base_object_model=domain_model
            )
        )
        if not data_create_result:
            raise ValidationException("Create new llm model failed")

        return LLMModelHttpMapper.to_http_model(model=data_create_result)

    async def read_llm_model(self, id: str) -> LLMModelResponseModel:
        data_read_result: DomainLLMModel = (
            await self.object_manage_controller.crud_object.crud_llm_model.read_object(
                id=id
            )
        )
        return LLMModelHttpMapper.to_http_model(model=data_read_result)

    async def update_llm_model(
        self, id: str, model: LLMModelRequestModel
    ) -> LLMModelResponseModel:
        domain_model: DomainLLMModel = LLMModelHttpMapper.to_domain_model(model=model)
        data_update_result: DomainLLMModel = (
            await self.object_manage_controller.crud_object.crud_llm_model.update_object(
                id=id, base_object_model=domain_model
            )
        )
        if not data_update_result:
            raise ValidationException("Update llm model failed")
        return LLMModelHttpMapper.to_http_model(model=data_update_result)

    async def delete_llm_model(self, id: str):
        await self.object_manage_controller.crud_object.crud_llm_model.delete_object(
            id=id
        )

    async def get_all_llm_model(self) -> List[LLMModelResponseModel]:
        llm_models: List[DomainLLMModel] = (
            await self.object_manage_controller.crud_object.crud_llm_model.get_all()
        )
        print('object_manage_controller', self.object_manage_controller)
        # if llm_models is None: return []
        return TypeAdapter(List[LLMModelResponseModel]).dump_python(llm_models)  # type: ignore

    async def get_all_models(self) -> List[LLMModelResponseModel]:
        self.llm_model_loader.load_classes()
        models = self.llm_model_loader.get_class_names()
        llm_models: List[DomainLLMModel] = [DomainLLMModel(**model) for model in models]
        return TypeAdapter(List[LLMModelResponseModel]).dump_python(llm_models)  # type: ignore
