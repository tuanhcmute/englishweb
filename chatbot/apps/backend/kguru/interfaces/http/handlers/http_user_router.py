from typing import Any

from fastapi import APIRouter

from kguru.domains.controllers import CRUDObjectController
from kguru.domains.models.http_model import UserRequestModel, UserResponseModel
from kguru.domains.models.domain_model import DomainUserModel
from kguru.interfaces.http.http_mapper import UserHttpMapper
from kguru.domains.exceptions import ValidationException


class HttpUserRouter(APIRouter):
    object_manage_controller: CRUDObjectController

    def __init__(
        self, object_manage_controller: CRUDObjectController, *args: Any, **kwargs: Any
    ):
        super().__init__(*args, **kwargs)
        self.object_manage_controller = object_manage_controller
        self.add_api_route(
            "/user",
            self.create_new_user,
            methods=["POST"],
            status_code=200,
            tags=["User"],
        )

        self.add_api_route(
            "/user/{email}",
            self.read_user,
            methods=["GET"],
            status_code=200,
            tags=["User"],
        )

    async def create_new_user(self, model: UserRequestModel) -> UserResponseModel:
        domain_tool: DomainUserModel = UserHttpMapper.to_domain_model(model=model)
        data_create_result: DomainUserModel = (
            await self.object_manage_controller.crud_object.crud_user.create_new_object(
                base_object_model=domain_tool
            )
        )
        if not data_create_result:
            raise ValidationException("Create new tool failed")

        return UserHttpMapper.to_http_model(model=data_create_result)

    async def read_user(self, email: str) -> UserResponseModel:
        data_read_result: DomainUserModel = (
            await self.object_manage_controller.crud_object.crud_user.read_object_by_specific_attribute(
                email=email
            )
        )
        return UserHttpMapper.to_http_model(model=data_read_result)
