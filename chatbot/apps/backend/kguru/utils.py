import functools
import importlib.util
import inspect
import logging
import os
from typing import Any, Tuple
from abc import ABC, abstractmethod

logger = logging.getLogger(__name__)


def alru_cache(maxsize: int = 128, typed: bool = False):
    """
    Create an async LRU cache decorator with the given maxsize.
    """

    def decorator(func):
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            key = args + tuple(kwargs.items())
            if typed:
                key += tuple(type(v) for v in args) + tuple(
                    type(v) for v in kwargs.values()
                )
            if key not in wrapper.cache:
                if len(wrapper.cache) >= maxsize:
                    wrapper.cache.popitem()
                wrapper.cache[key] = await func(*args, **kwargs)
            return wrapper.cache[key]

        wrapper.cache = {}
        return wrapper

    return decorator


class ClassLoader(ABC):
    """
    ToolLoader class is responsible for loading tools from a folder. Same the way Airflow loads operators from a folder.

    Args:
        class_folder (str): Path to the folder containing the tools, encouraged to be an absolute path.
    """

    def __init__(
        self, class_folder: str, base_class_definition: tuple, class_key_name: str
    ) -> None:
        self.class_folder = os.path.abspath(class_folder)
        self.base_class_definition = base_class_definition
        self.class_key_name = class_key_name

        self.classes = {}

        logger.info(f"Tool folder: {self.class_folder}")

    def load_classes(self):
        """
        Load all classes from classes folder
        """
        for file_name in os.listdir(self.class_folder):
            if file_name.endswith(".py") and not file_name.startswith("__init__"):
                file_path = os.path.join(self.class_folder, file_name)
                self._load_class(file_path)

    def _load_class(self, file_path: str):
        """
        Load a tool from a file. Tool is a class with is_tool attribute.

        Args:
            filepath (str): Path to the file containing the tool class
        """
        module_name, ext = os.path.splitext(os.path.basename(file_path))
        spec = importlib.util.spec_from_file_location(module_name, file_path)
        module = importlib.util.module_from_spec(spec)
        try:
            spec.loader.exec_module(module)
            classes = filter(self._filter, inspect.getmembers(module, inspect.isclass))

            for cls_name, obj in classes:
                if name := self._class_name_definition(obj):
                    cls_name = name

                self.register_class(cls_name, obj)
                logger.info(f"{self.__class__.__name__} loaded class: {cls_name}")

        except Exception as e:
            logger.error(f"Failed to load tool from file {file_path}: {e}")
            return

    def _filter(self, obj: Tuple[str, Any]) -> bool:
        _, class_obj = obj
        return (
            issubclass(class_obj, self.base_class_definition)
            and class_obj not in self.base_class_definition
            and self._filter_condition(obj)
        )

    def get_classes(self) -> dict:
        return self.classes

    def get_class_names(self) -> list:
        return [{"id": name, self.class_key_name: name} for name in self.classes.keys()]

    def get_class(self, name: str) -> Any:
        return self.classes.get(name)

    def register_class(self, name: str, obj: Any):
        self.classes[name] = obj

    def _filter_condition(self, obj: Tuple[str, Any]) -> bool:
        """
        Return True if the object is a tool, False otherwise.
        By default, all concrete classes are considered tools.

        Args:
            obj (Tuple): The object (class name, obj class) to check if it is a tool
        """
        return True

    def _class_name_definition(self, obj: Any) -> bool:
        """
        Return the name of the tool.
        If the tool does not have a name, return None.
        By default, the name is the class name.

        Args:
            obj (Any): The object (class) to check the tool name
        """
