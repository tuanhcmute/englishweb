import os
from dotenv import load_dotenv
from fastapi.encoders import jsonable_encoder
import uvicorn

load_dotenv(override=True)
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware

from kguru.domains.exceptions.base_exception import BaseApplicationException
from kguru.services.storages.storages_config import PostgresConfig
from kguru.domains.controllers import (
    CRUDObjectController,
    SkilledAgentTestController,
    AgentChatController,
)
from kguru.services.storages.storage_service import StorageService
from kguru.services.questions_and_answers_pharse import SkilledAgentTest
from kguru.interfaces.http.handlers import (
    HttpQuestionRouter,
    HttpSkilledAgentRouter,
    HttpTestRunResultRouter,
    HttpTestSuiteRouter,
    HttpLLMModelRouter,
    HttpToolRouter,
    HttpSkilledAgentVersionRouter,
    HttpTestSuiteRunRouter,
    HttpSkilledAgentTestRouter,
    HttpAgentChatHistoryRouter,
    HttpUserRouter,
    HttpGetRequestHeaderRouter,
)
from kguru.services.agent_chat import AgentChatHistoryService
from fastapi.staticfiles import StaticFiles

from starlette.requests import Request
import logging
import pathlib

root_path = str(pathlib.Path(__file__).parent.parent.parent.resolve())

LOG = logging.getLogger()

storage_driver = (
    PostgresConfig()
    .set_username(username=os.environ["POSTGRES_USERNAME"])
    .set_password(password=os.environ["POSTGRES_PASSWORD"])
    .set_host(host=os.environ["POSTGRES_HOST"])
    .set_port(port=int(os.environ["POSTGRES_PORT"]))
    .set_database(database=os.environ["POSTGRES_DATABASE"])
)
storage_driver.connect()


skilled_agent_test = SkilledAgentTest(storage_driver=storage_driver)
skilled_agent_chat_history_service = AgentChatHistoryService(
    storage_driver=storage_driver
)
storage_service = StorageService(storage_driver=storage_driver)


crud_object = CRUDObjectController(crud_object=storage_service)
skilled_agent_test_controller = SkilledAgentTestController(
    skilled_agent_test=skilled_agent_test
)
skilled_agent_chat_controller = AgentChatController(
    agent_chat_service=skilled_agent_chat_history_service
)


http_question_router = HttpQuestionRouter(object_manage_controller=crud_object)
http_skilled_agent_router = HttpSkilledAgentRouter(object_manage_controller=crud_object)
http_test_run_result_router = HttpTestRunResultRouter(
    object_manage_controller=crud_object
)
http_test_suite_router = HttpTestSuiteRouter(object_manage_controller=crud_object)
http_llm_model_router = HttpLLMModelRouter(object_manage_controller=crud_object)
http_tool_router = HttpToolRouter(object_manage_controller=crud_object)
http_skilled_agent_version = HttpSkilledAgentVersionRouter(
    object_manage_controller=crud_object
)
http_test_suite_run_router = HttpTestSuiteRunRouter(
    object_manage_controller=crud_object
)
http_skilled_agent_test_router = HttpSkilledAgentTestRouter(
    skilled_agent_test_controller=skilled_agent_test_controller
)
http_skilled_agent_chat_history_router = HttpAgentChatHistoryRouter(
    agent_chat_controller=skilled_agent_chat_controller
)
http_user_router = HttpUserRouter(object_manage_controller=crud_object)
http_get_request_header = HttpGetRequestHeaderRouter()


app = FastAPI(root_path="/chatbot")

origins = [
    "http://localhost:80",
    "http://localhost",
    "http://localhost:5173",
    os.environ["FE_URL"],
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    # allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(BaseApplicationException)
async def validation_exception_handler(
    request: Request, exception: BaseApplicationException
):
    LOG.error(f"Bad request: {exception.message}", exc_info=True)
    return JSONResponse(
        status_code=400, content=jsonable_encoder({"message": exception.message})
    )


@app.exception_handler(Exception)
async def validation_general_exception(request: Request, exception: Exception):
    LOG.error(f"Internal server error", exc_info=True)
    return JSONResponse(
        status_code=500, content=jsonable_encoder({"message": "Internal server error"})
    )


@app.exception_handler(RequestValidationError)
async def request_validation_exception_handler(
    request, exception: RequestValidationError
):
    return JSONResponse(
        status_code=400,
        content=jsonable_encoder(
            {"message": "Invalid request", "detail": exception.errors()}
        ),
    )


app.include_router(http_question_router)
app.include_router(http_skilled_agent_router)
app.include_router(http_test_run_result_router)
app.include_router(http_test_suite_router)
app.include_router(http_llm_model_router)
app.include_router(http_tool_router)
app.include_router(http_skilled_agent_version)
app.include_router(http_test_suite_run_router)
app.include_router(http_user_router)
app.include_router(http_skilled_agent_test_router)
app.include_router(http_skilled_agent_chat_history_router)
app.include_router(http_get_request_header)

logging.addLevelName(
    logging.INFO, "\033[1;36m%s\033[1;0m" % logging.getLevelName(logging.INFO)
)

logging.addLevelName(
    logging.WARNING, "\033[1;31m%s\033[1;0m" % logging.getLevelName(logging.WARNING)
)

logging.addLevelName(
    logging.ERROR, "\033[1;41m%s\033[1;0m" % logging.getLevelName(logging.ERROR)
)


async def startup_event():
    root_logger = logging.getLogger()
    for handler in root_logger.handlers:
        root_logger.removeHandler(handler)
    handler = logging.StreamHandler()
    handler.setFormatter(
        logging.Formatter(
            "[%(asctime)s] %(module)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s"
        )
    )
    root_logger.setLevel("INFO")
    root_logger.addHandler(handler)


app.add_event_handler(event_type="startup", func=startup_event)
app.mount(
    "/static", StaticFiles(directory=os.environ["STATIC_RESOURCE_PATH"]), name="static"
)


if __name__ == "__main__":
    uvicorn.run(
        app,
        host="0.0.0.0",
        port=8000,
        access_log=False,
    )
