from abc import abstractmethod
from typing import Any, Dict

from kguru.domains.models.task import Task


class BaseCondition:
    def __init__(self, *args: Any) -> None:
        self.args = args

    @abstractmethod
    def get_tasks(self) -> list[str]:
        pass

    def compare(self, tasks: Dict[str, Task]) -> bool:
        return False
