import logging

from .task import Task


logger = logging.getLogger()


class PromptProvider:
    async def summary_prompt(self, tasks: list[Task]) -> str:
        # [logger.info(f"Values: {task.task_result}") for task in tasks]
        return ""
