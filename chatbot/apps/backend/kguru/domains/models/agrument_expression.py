import operator
from typing import Any, Dict

from kguru.domains.exceptions.bussiness_exception import BussinessException
from ..exceptions import ValidationException
from .task import Task
from .task_property import TaskProperty
from .expression import Expression

OPS = {
    Expression.EQUAL: operator.eq,
    Expression.GREATER_THAN: operator.gt,
    Expression.GREATER_THAN_OR_EQUAL: operator.ge,
    Expression.LESS_THAN: operator.lt,
    Expression.LESS_THAN_OR_EQUAL: operator.le,
    Expression.CONTAIN: operator.contains,
}


class ArgumentExpression:
    def __init__(
        self,
        task_id: str,
        property: TaskProperty,
        compare_value: Any,
        expression: Expression,
        take_input_from_expression: bool,
    ) -> None:
        self.task_id = task_id
        self.property = property
        self.compare_value = compare_value
        self.expression = expression
        self.take_input_from_expression = take_input_from_expression

    def get_tasks(self) -> list[str]:
        return [self.task_id]

    def compare(self, tasks: Dict[str, Task]) -> bool:
        if not self.task_id.strip():
            raise ValidationException.empty("task_id")

        if tasks.__len__() == 0:
            raise ValidationException.require("tasks")

        if self.task_id not in tasks:
            raise BussinessException("Task id is not in tasks dict")

        return OPS[self.expression](
            getattr(tasks[self.task_id], self.property.value), self.compare_value
        )

    def prompt_builder(self):
        pass
