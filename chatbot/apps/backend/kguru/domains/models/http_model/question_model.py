from pydantic import BaseModel
from .base_http_model import BaseHttpModel


class QuestionRequestModel(BaseModel):
    question_name: str
    expected_answer: str
    test_suite_id: str


class QuestionResponseModel(BaseHttpModel, QuestionRequestModel):
    pass
