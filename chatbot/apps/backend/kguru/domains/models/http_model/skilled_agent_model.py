from pydantic import BaseModel
from typing import List, TYPE_CHECKING

from .base_http_model import BaseHttpModel
from .skilled_agent_version import SkilledAgentVersionReponseModel

if TYPE_CHECKING:
    from .test_suite_model import TestSuiteReponseModel


class SkilledAgentRequestModel(BaseModel):
    agent_name: str
    setup_message: str
    test_score: float
    description: str
    test_suite_ids: List[str]


class SkilledAgentResponseModel(BaseHttpModel, BaseModel):
    agent_name: str
    setup_message: str
    test_score: float
    description: str
    test_suites: List["TestSuiteReponseModel"]
