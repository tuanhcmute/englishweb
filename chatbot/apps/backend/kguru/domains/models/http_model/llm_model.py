from typing import Optional

from pydantic import BaseModel

from .base_http_model import BaseHttpModel


class LLMModelRequestModel(BaseModel):
    llm_model_name: str
    param: Optional[str] = None


class LLMModelResponseModel(BaseHttpModel, LLMModelRequestModel):
    pass
