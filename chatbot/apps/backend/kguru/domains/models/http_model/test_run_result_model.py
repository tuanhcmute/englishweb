from pydantic import BaseModel
from datetime import datetime

from .base_http_model import BaseHttpModel
from .run_question import RunQuestionResponseModel
from .answer_model import AnswerResponseModel


class TestrunResultRequestModel(BaseModel):
    test_suite_run_id: str
    skilled_agent_version_id: str
    is_pass: bool
    speed: float
    run_question_id: str
    answer_id: str
    created_at: datetime


class TestRunResultReponseModel(BaseHttpModel, TestrunResultRequestModel):
    run_question_result: RunQuestionResponseModel
    answer_result: AnswerResponseModel
