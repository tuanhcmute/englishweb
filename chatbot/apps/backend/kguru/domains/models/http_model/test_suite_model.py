from typing import List
from pydantic import BaseModel

from .base_http_model import BaseHttpModel

from .skilled_agent_model import SkilledAgentResponseModel
from .question_model import QuestionResponseModel
from .test_suite_run_model import TestSuiteRunReponseModel


class TestSuiteRequestModel(BaseModel):
    test_suite_name: str
    description: str
    skilled_agent_ids: List[str]


class TestSuiteReponseModel(BaseHttpModel, BaseModel):
    test_suite_name: str
    description: str
    skilled_agents: List[SkilledAgentResponseModel]
    questions: List[QuestionResponseModel]
    test_suite_runs: List[TestSuiteRunReponseModel]
