from pydantic import BaseModel
from .base_http_model import BaseHttpModel


class RunQuestionRequestModel(BaseModel):
    question_name: str
    expected_answer: str


class RunQuestionResponseModel(BaseHttpModel, RunQuestionRequestModel):
    pass
