from typing import Optional

from pydantic import BaseModel

from .base_http_model import BaseHttpModel


class ToolRequestModel(BaseModel):
    tool_name: str
    param: Optional[str] = None


class ToolResponseModel(BaseHttpModel, ToolRequestModel):
    pass
