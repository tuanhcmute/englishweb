from pydantic import BaseModel

from .base_http_model import BaseHttpModel


class UserRequestModel(BaseModel):
    user_name: str
    email: str
    platform: str


class UserResponseModel(BaseHttpModel, UserRequestModel):
    pass
