from pydantic import BaseModel

from .base_http_model import BaseHttpModel


class TestSuiteRunRequestModel(BaseModel):
    version: str
    score: float
    run_status: str
    test_suite_id: str
    skilled_agent_version_id: str
    the_number_of_question: int


class TestSuiteRunReponseModel(
    BaseHttpModel,
):
    version: str
    score: float
    run_status: str
    test_suite_id: str
    skilled_agent_version_id: str
    the_number_of_question: int
