from pydantic import BaseModel
from datetime import datetime

from .base_http_model import BaseHttpModel


class AgentChatHistoryRequestModel(BaseModel):
    message: str
    is_kguru_msg: bool
    skilled_agent_version_id: str
    user_id: str


class AgentChatHistoryResponseModel(BaseHttpModel, AgentChatHistoryRequestModel):
    message: str
    is_kguru_msg: bool
    created_at: datetime
    skilled_agent_version_id: str
    user_id: str
