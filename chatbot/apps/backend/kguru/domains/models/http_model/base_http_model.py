from pydantic import BaseModel


class BaseHttpModel(BaseModel):
    id: str
