from pydantic import BaseModel

from .base_http_model import BaseHttpModel


class SkilledAgentTestRequestModel(BaseModel):
    test_suite_run_id: str
    skilled_agent_version_id: str


class SkilledAgentTestResponseModel(BaseHttpModel):
    pass
