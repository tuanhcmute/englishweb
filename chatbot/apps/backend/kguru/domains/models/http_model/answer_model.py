from pydantic import BaseModel

from .base_http_model import BaseHttpModel


class AnswerRequestModel(BaseModel):
    answer_content: str


class AnswerResponseModel(BaseHttpModel, AnswerRequestModel):
    pass
