from pydantic import BaseModel

from .base_http_model import BaseHttpModel


class TestSuiteSkilledAgentRequestModel(BaseModel):
    test_suite_id: str
    skilled_agent_id: str


class TestSuiteSkilledAgentReponseModel(
    BaseHttpModel, TestSuiteSkilledAgentRequestModel
):
    pass
