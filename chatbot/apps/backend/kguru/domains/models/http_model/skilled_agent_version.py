from pydantic import BaseModel
from typing import List, Optional

from .base_http_model import BaseHttpModel
from .test_suite_run_model import TestSuiteRunReponseModel
from .tool_model import ToolResponseModel
from .llm_model import LLMModelResponseModel


class SkilledAgentVersionRequestModel(BaseModel):
    skilled_agent_id: str
    version: str
    setup_message: str
    llm_model_ids: List[str]
    tool_ids: List[str]


class SkilledAgentVersionReponseModel(BaseHttpModel):
    skilled_agent_id: str
    version: str
    setup_message: str
    test_suite_runs: List[TestSuiteRunReponseModel]
    tools: List[ToolResponseModel]
    llm_model_result: List[LLMModelResponseModel]
    skilled_agent_name: str
