from kguru.domains.models.task import Task
from ..models import BaseCondition
from typing import Any, Dict


class And(BaseCondition):
    def __init__(self, *args: Any) -> None:
        super().__init__(*args)

    def get_tasks(self) -> list[str]:
        return [arg.task_id for arg in self.args]

    def compare(self, tasks: Dict[str, Task]) -> bool:
        return all([arg.compare(tasks) == True for arg in self.args])
