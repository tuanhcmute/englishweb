from kguru.domains.models.agrument_expression import ArgumentExpression
from kguru.domains.models.base_condition import BaseCondition


class TaskDependency:
    dependency: BaseCondition | ArgumentExpression

    def __init__(self, dependency: BaseCondition | ArgumentExpression) -> None:
        self.dependency = dependency
