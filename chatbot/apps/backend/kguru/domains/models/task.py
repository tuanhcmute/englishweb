from datetime import datetime
import logging

from .skilled_agent import SkilledAgent

logger = logging.getLogger()


class Task:
    task_id: str
    task_result: str
    skilled_agent: SkilledAgent
    start_time: datetime
    command: str  # cua chinh no

    def __init__(self, task_id: str, skilled_agent: SkilledAgent, command: str) -> None:
        self.task_id = task_id
        self.command = command
        self.skilled_agent = skilled_agent

    # Receive prompt from task dependencies by PromptProvider class
    async def run(self, prompt: str):
        self.start_time = datetime.now()

        new_prompt: str = f"{prompt} {self.command}"
        self.task_result = await self.skilled_agent.run(prompt=new_prompt)
