from typing import List, Optional
from pydantic import BaseModel

from .domain_test_suite_run_model import DomainTestSuiteRunModel
from .domain_tool_model import DomainToolModel
from .domain_llm_model import DomainLLMModel


class DomainSkilledAgentVersionModel(BaseModel):
    id: Optional[str] = None
    skilled_agent_id: str
    version: str
    setup_message: str
    llm_model_ids: List[str] = []
    tool_ids: List[str] = []
    test_suite_runs: List[DomainTestSuiteRunModel] = []
    tools: List[DomainToolModel] = []
    llm_model_result: List[DomainLLMModel] = []
    skilled_agent_name: Optional[str] = ""
