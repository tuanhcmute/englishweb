from typing import Optional
from pydantic import BaseModel


class DomainRunQuestionModel(BaseModel):
    id: Optional[str] = None
    question_name: str
    expected_answer: str
