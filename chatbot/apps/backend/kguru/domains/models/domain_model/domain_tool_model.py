from typing import Optional
from pydantic import BaseModel


class DomainToolModel(BaseModel):
    id: Optional[str] = None
    tool_name: str
    param: Optional[str] = None