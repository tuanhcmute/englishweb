from typing import Optional
from datetime import datetime
from pydantic import BaseModel

from .domain_run_question_model import DomainRunQuestionModel
from .domain_answer_model import DomainAnswerModel


class DomainTestRunResultModel(BaseModel):
    id: Optional[str] = None
    test_suite_run_id: str
    skilled_agent_version_id: str
    is_pass: bool
    speed: float
    run_question_id: str
    answer_id: str
    created_at: Optional[datetime] = None
    run_question_result: Optional[DomainRunQuestionModel] = None
    answer_result: Optional[DomainAnswerModel] = None
