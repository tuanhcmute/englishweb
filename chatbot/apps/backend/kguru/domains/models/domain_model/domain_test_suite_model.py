from typing import List, Optional, TYPE_CHECKING
from pydantic import BaseModel

from .domain_question_model import DomainQuestionModel
from .domain_test_suite_run_model import DomainTestSuiteRunModel

if TYPE_CHECKING:
    from .domain_skilled_agent_model import DomainSkilledAgentModel


class DomainTestSuiteModel(BaseModel):
    id: Optional[str] = None
    test_suite_name: str
    description: str
    skilled_agent_ids: List[str] = []
    question_ids: List[str] = []
    skilled_agents: Optional[List["DomainSkilledAgentModel"]] = []
    questions: Optional[List[DomainQuestionModel]] = []
    test_suite_runs: Optional[List[DomainTestSuiteRunModel]] = []
