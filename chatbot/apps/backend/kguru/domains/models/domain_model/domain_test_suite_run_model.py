from typing import Optional, List
from pydantic import BaseModel


class DomainTestSuiteRunModel(BaseModel):
    id: Optional[str] = None
    version: str
    score: float
    run_status: str
    test_suite_id: str
    skilled_agent_version_id: str
    the_number_of_question: int
