from typing import Optional
from datetime import datetime
from pydantic import BaseModel


class DomainAgentChatHistoryModel(BaseModel):
    id: Optional[str] = None
    message: str
    is_kguru_msg: bool
    created_at: Optional[datetime] = None
    skilled_agent_version_id: str
    user_id: str
