from typing import Optional
from pydantic import BaseModel


class DomainLLMModel(BaseModel):
    id: Optional[str] = None
    llm_model_name: str
    param: Optional[str] = None
