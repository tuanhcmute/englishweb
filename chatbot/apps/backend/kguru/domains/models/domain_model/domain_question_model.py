from typing import Optional
from pydantic import BaseModel


class DomainQuestionModel(BaseModel):
    id: Optional[str] = None
    question_name: str
    expected_answer: str
    test_suite_id: str
