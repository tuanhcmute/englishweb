from typing import Optional
from pydantic import BaseModel


class DomainUserModel(BaseModel):
    id: Optional[str] = None
    user_name: str
    email: str
    platform: str
