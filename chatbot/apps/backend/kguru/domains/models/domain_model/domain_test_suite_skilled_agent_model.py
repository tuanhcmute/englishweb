from typing import Optional
from pydantic import BaseModel


class DomainTestSuiteSkilledAgent(BaseModel):
    id: Optional[str] = None
    test_suite_id: str
    skilled_agent_id: str
