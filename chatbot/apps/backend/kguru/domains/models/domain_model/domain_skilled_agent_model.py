from typing import List, Optional
from pydantic import BaseModel

from .domain_test_suite_model import DomainTestSuiteModel


class DomainSkilledAgentModel(BaseModel):

    id: Optional[str] = None
    agent_name: str
    setup_message: str
    test_score: float
    description: str
    test_suite_ids: List[str] = []
    test_suites: List[DomainTestSuiteModel] = []
