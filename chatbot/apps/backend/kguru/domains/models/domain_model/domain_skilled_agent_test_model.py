from typing import Optional
from pydantic import BaseModel


class DomainSkilledAgentTestModel(BaseModel):
    test_suite_run_id: str
    skilled_agent_version_id: str
