from typing import Optional
from pydantic import BaseModel


class DomainAnswerModel(BaseModel):
    id: Optional[str] = None
    answer_content: str
