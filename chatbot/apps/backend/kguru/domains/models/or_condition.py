from kguru.domains.models.task import Task
from ..models import BaseCondition
from typing import Any, Dict


class Or(BaseCondition):
    def __init__(self, *args: Any) -> None:
        super().__init__(*args)

    def compare(self, tasks: Dict[str, Task]) -> bool:
        return any([arg.compare(tasks) == True for arg in self.args])

    def get_tasks(self) -> list[str]:
        return [arg.task_id for arg in self.args]
