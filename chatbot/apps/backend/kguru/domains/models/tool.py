from abc import abstractmethod, ABC
from typing import Any, Union, Dict, Tuple, List, Union, Type, Optional, TypeVar
from langchain.tools import Tool
from pydantic.v1 import BaseModel, Field, create_model
import uuid
from kguru.domains.exceptions.bussiness_exception import BussinessException
import logging
from langchain_core.tools import ToolException
from langchain_core.callbacks.manager import Callbacks  # type: ignore
from langchain_core.runnables import (
    RunnableConfig,
)
from inspect import signature
from langchain_core.callbacks import (
    AsyncCallbackManager,
)
from contextvars import copy_context
from langchain_core.runnables.config import (
    patch_config,
    var_child_runnable_config,
)
from langchain_core.runnables.utils import accepts_context
import asyncio
from langchain_core.pydantic_v1 import (
    BaseModel,
    Field,
    ValidationError,
    create_model,
)
from langchain_core.pydantic_v1.dataclasses import dataclass

logger = logging.getLogger(__name__)

T = TypeVar("T")


class ToolInputSchemaRegistry(BaseModel):
    input_name: str
    input_description: str
    input_data_type: str


class TypeAllowedBaseModel(BaseModel):
    class Config:
        arbitrary_types_allowed = True


class ToolExecutionContextResult(TypeAllowedBaseModel):
    """
    Cause result may contains some Object like pandas dataframe which pydantic don't cover the validation,
    we have to put arbitrary_types_allowed = True config for ToolExecutionContextResult
    """

    pass


class BaseTool(ABC):
    name: str
    description: str

    kguru_is_tool: bool = True
    kguru_tool_name: str

    async def run(self) -> str:
        return ""

    @abstractmethod
    def build(self) -> "LCTool":
        pass

    def _build_tool_input_pydantic_schema(
        self, tool_inputs_schema_registry: List[ToolInputSchemaRegistry]
    ) -> Type[BaseModel]:
        """
        Construct pydantic schema for tool input.
        This helps dynamic tool input schema from construct param.
        """
        model_attribute = {}
        for tool_input_schema_registry in tool_inputs_schema_registry:
            model_attribute[tool_input_schema_registry.input_name] = (
                self._get_mapping_data_type(tool_input_schema_registry.input_data_type),
                Field(description=tool_input_schema_registry.input_description),
            )

        tool_input_schema = create_model(
            f"ToolInput-{uuid.uuid4()}",
            **model_attribute,
            __config__=BaseModel.Config,
        )
        return tool_input_schema

    def _get_mapping_data_type(
        self, type: str
    ) -> Union[Type[str], Type[int], Type[List[str]]]:
        match type:
            case "str":
                return str
            case "int":
                return int
            case "List[str]":
                return List[str]
            case _:
                raise BussinessException(
                    f"Tool input data type: {type} is not supported"
                )

    def set_execution_result_to_context(
        self,
        execution_result: ToolExecutionContextResult,
        context: Optional[Dict[str, Any]],
    ):
        """
        Check if context already have any tool_execution_context_result and set the result into context.
        """
        if context != None:
            if context.get("tool_execution_context_result") != None:
                context["tool_execution_context_result"].append(execution_result)
            else:
                context["tool_execution_context_result"] = [execution_result]

    def get_tool_execution_result_context(
        self, context: Optional[Dict[str, Any]]
    ) -> List[ToolExecutionContextResult]:
        if context == None or context["tool_execution_context_result"] == None:
            return []
        return context["tool_execution_context_result"]

    def get_latest_execution_result(
        self,
        context: Optional[Dict[str, Any]],
        result_class: Type[T],
    ) -> Optional[T]:
        """
        If tool executed many time, this function help to return latest execute result.
        """
        tool_execution_result_context = self.get_tool_execution_result_context(context)
        result = None
        for tool_execution_result in tool_execution_result_context:
            if tool_execution_result.__class__.__name__ == result_class.__name__:
                result = tool_execution_result
        return result  # type: ignore


class LCTool(Tool):
    """
    LangChain tool don't support multiple tool input.
    This implementation is trying to support:
        1. multiple tool input by changing _to_args_and_kwargs, convert_args, _parse_input
        2. invoke tool with context by changing arun.
            context.run(
                self._arun, *tool_args, run_manager=run_manager, **tool_kwargs
            )
            -> context.run(
                self._arun, *tool_args, run_manager=run_manager, **tool_kwargs, context=kwargs.get("context")
            )
    Notes:
        Cause this is reimplement some langchain private method so must be awared about langchain version upgrading
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _to_args_and_kwargs(self, tool_input: Union[str, Dict]) -> Tuple[Tuple, Dict]:
        """Convert tool input to pydantic model."""
        args, kwargs = self.convert_args(tool_input)
        return args, kwargs

    def convert_args(self, tool_input: Union[str, Dict]):
        if isinstance(tool_input, str):
            return (tool_input,), {}
        else:
            return (), tool_input

    def _parse_input(
        self,
        tool_input: Union[str, Dict],
    ) -> Union[str, Dict[str, Any]]:
        """Convert tool input to pydantic model."""
        input_args = self.args_schema
        if isinstance(tool_input, str):
            if input_args is not None:
                key_ = next(iter(input_args.__fields__.keys()))
                input_args.validate({key_: tool_input})
            return tool_input
        else:

            if input_args is not None:
                try:
                    result = input_args.parse_obj(tool_input)
                except Exception as e:
                    logger.error(e, exc_info=True)
                    raise e
                result_dict = {k: getattr(result, k) for k, v in result.dict().items()}
                return result_dict
        return tool_input

    async def arun(
        self,
        tool_input: Union[str, Dict],
        verbose: Optional[bool] = None,
        start_color: Optional[str] = "green",
        color: Optional[str] = "green",
        callbacks: Callbacks = None,
        *,
        tags: Optional[List[str]] = None,
        metadata: Optional[Dict[str, Any]] = None,
        run_name: Optional[str] = None,
        run_id: Optional[uuid.UUID] = None,
        config: Optional[RunnableConfig] = None,
        **kwargs: Any,
    ) -> Any:
        """Run the tool asynchronously."""
        if not self.verbose and verbose is not None:
            verbose_ = verbose
        else:
            verbose_ = self.verbose
        callback_manager = AsyncCallbackManager.configure(
            callbacks,
            self.callbacks,
            verbose_,
            tags,
            self.tags,
            metadata,
            self.metadata,
        )
        new_arg_supported = signature(self._arun).parameters.get("run_manager")
        run_manager = await callback_manager.on_tool_start(
            {"name": self.name, "description": self.description},
            tool_input if isinstance(tool_input, str) else str(tool_input),
            color=start_color,
            name=run_name,
            inputs=tool_input,
            run_id=run_id,
            **kwargs,
        )
        try:
            parsed_input = self._parse_input(tool_input)
            # We then call the tool on the tool input to get an observation
            tool_args, tool_kwargs = self._to_args_and_kwargs(parsed_input)
            child_config = patch_config(
                config,
                callbacks=run_manager.get_child(),
            )
            context = copy_context()
            context.run(var_child_runnable_config.set, child_config)
            coro = (
                context.run(
                    self._arun,
                    *tool_args,
                    run_manager=run_manager,
                    **tool_kwargs,
                    context=kwargs.get("context"),
                )
                if new_arg_supported
                else context.run(self._arun, *tool_args, **tool_kwargs)
            )
            if accepts_context(asyncio.create_task):
                observation = await asyncio.create_task(coro, context=context)  # type: ignore
            else:
                observation = await coro

        except ValidationError as e:
            if not self.handle_validation_error:
                raise e
            elif isinstance(self.handle_validation_error, bool):
                observation = "Tool input validation error"
            elif isinstance(self.handle_validation_error, str):
                observation = self.handle_validation_error
            elif callable(self.handle_validation_error):
                observation = self.handle_validation_error(e)
            else:
                raise ValueError(
                    f"Got unexpected type of `handle_validation_error`. Expected bool, "
                    f"str or callable. Received: {self.handle_validation_error}"
                )
            return observation
        except ToolException as e:
            if not self.handle_tool_error:
                await run_manager.on_tool_error(e)
                raise e
            elif isinstance(self.handle_tool_error, bool):
                if e.args:
                    observation = e.args[0]
                else:
                    observation = "Tool execution error"
            elif isinstance(self.handle_tool_error, str):
                observation = self.handle_tool_error
            elif callable(self.handle_tool_error):
                observation = self.handle_tool_error(e)
            else:
                raise ValueError(
                    f"Got unexpected type of `handle_tool_error`. Expected bool, str "
                    f"or callable. Received: {self.handle_tool_error}"
                )
            await run_manager.on_tool_end(
                observation, color="red", name=self.name, **kwargs
            )
            return observation
        except (Exception, KeyboardInterrupt) as e:
            await run_manager.on_tool_error(e)
            raise e
        else:
            await run_manager.on_tool_end(
                observation, color=color, name=self.name, **kwargs
            )
            return observation
