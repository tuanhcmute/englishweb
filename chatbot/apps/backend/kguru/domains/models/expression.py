from enum import Enum


class Expression(Enum):
    EQUAL = ("equal",)
    GREATER_THAN = ("greater_than",)
    GREATER_THAN_OR_EQUAL = ("greater_than_or_equal",)
    LESS_THAN = ("less_than",)
    LESS_THAN_OR_EQUAL = ("less_than_or_equal",)
    CONTAIN = ("contain",)
