from enum import Enum


class TaskProperty(Enum):
    TASK_RESULT = "task_result"
    TASK_ID = "task_id"
    CONTEXT = "context"
    START_TIME = "start_time"
