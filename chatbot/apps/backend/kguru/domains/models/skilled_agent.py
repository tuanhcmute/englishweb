from abc import ABC
import json
from typing import Dict, List, Optional, AsyncIterator, Union, Tuple, Any

from langchain_core.runnables import Runnable
from langchain.agents import (
    AgentExecutor,
    create_tool_calling_agent,
    create_openai_tools_agent,
)
from langchain_core.prompts.chat import (
    ChatPromptTemplate,
    MessagesPlaceholder,
    HumanMessagePromptTemplate,
)
from langchain_core.messages import SystemMessage, HumanMessage, AIMessage
from langchain_core.agents import AgentAction, AgentFinish, AgentStep
from langchain_core.tools import BaseTool
from langchain_core.callbacks import (
    AsyncCallbackManagerForChainRun,
)
from langchain.agents.tools import InvalidTool
from langchain.agents.agent import ExceptionTool
import logging
import asyncio
from langchain_core.exceptions import OutputParserException

logger = logging.getLogger(__name__)


class AgentExecutorWithToolContext(AgentExecutor):
    """
    Langchain AgentExecutor don't have state to pass in every tool
    This implementation is trying to help passing invoke context to tool execution

    By default _aperform_agent_action is invoked without passing inputs (argument of agent invoke function). We try to pass inputs argument from
    _aiter_next_step to _aperform_agent_action. Then pass inputs to tool run in "context=inputs"

    Note:
        Cause this implementation is overwriting Langchain AgentExecutor private method, you must aware about Langchain version compatible
    """

    async def _aperform_agent_action(
        self,
        name_to_tool_map: Dict[str, BaseTool],
        color_mapping: Dict[str, str],
        agent_action: AgentAction,
        inputs: Dict[str, Any],
        run_manager: Optional[AsyncCallbackManagerForChainRun] = None,
    ) -> AgentStep:
        if run_manager:
            await run_manager.on_agent_action(
                agent_action, verbose=self.verbose, color="green"
            )
        # Otherwise we lookup the tool
        if agent_action.tool in name_to_tool_map:
            tool = name_to_tool_map[agent_action.tool]
            return_direct = tool.return_direct
            color = color_mapping[agent_action.tool]
            tool_run_kwargs = self.agent.tool_run_logging_kwargs()
            if return_direct:
                tool_run_kwargs["llm_prefix"] = ""
            # We then call the tool on the tool input to get an observation
            observation = await tool.arun(
                agent_action.tool_input,
                verbose=self.verbose,
                color=color,
                callbacks=run_manager.get_child() if run_manager else None,
                **tool_run_kwargs,
                context=inputs,
            )
        else:
            tool_run_kwargs = self.agent.tool_run_logging_kwargs()
            observation = await InvalidTool().arun(
                {
                    "requested_tool_name": agent_action.tool,
                    "available_tool_names": list(name_to_tool_map.keys()),
                },
                verbose=self.verbose,
                color=None,
                callbacks=run_manager.get_child() if run_manager else None,
                **tool_run_kwargs,
                context=inputs,
            )
        return AgentStep(action=agent_action, observation=observation)

    async def _aiter_next_step(
        self,
        name_to_tool_map: Dict[str, BaseTool],
        color_mapping: Dict[str, str],
        inputs: Dict[str, Any],
        intermediate_steps: List[Tuple[AgentAction, str]],
        run_manager: Optional[AsyncCallbackManagerForChainRun] = None,
    ) -> AsyncIterator[Union[AgentFinish, AgentAction, AgentStep]]:
        """Take a single step in the thought-action-observation loop.

        Override this to take control of how the agent makes and acts on choices.
        """
        try:
            intermediate_steps = self._prepare_intermediate_steps(intermediate_steps)

            # Call the LLM to see what to do.
            output = await self.agent.aplan(
                intermediate_steps,
                callbacks=run_manager.get_child() if run_manager else None,
                **inputs,
            )
        except OutputParserException as e:
            if isinstance(self.handle_parsing_errors, bool):
                raise_error = not self.handle_parsing_errors
            else:
                raise_error = False
            if raise_error:
                raise ValueError(
                    "An output parsing error occurred. "
                    "In order to pass this error back to the agent and have it try "
                    "again, pass `handle_parsing_errors=True` to the AgentExecutor. "
                    f"This is the error: {str(e)}"
                )
            text = str(e)
            if isinstance(self.handle_parsing_errors, bool):
                if e.send_to_llm:
                    observation = str(e.observation)
                    text = str(e.llm_output)
                else:
                    observation = "Invalid or incomplete response"
            elif isinstance(self.handle_parsing_errors, str):
                observation = self.handle_parsing_errors
            elif callable(self.handle_parsing_errors):
                observation = self.handle_parsing_errors(e)
            else:
                raise ValueError("Got unexpected type of `handle_parsing_errors`")
            output = AgentAction("_Exception", observation, text)
            tool_run_kwargs = self.agent.tool_run_logging_kwargs()
            observation = await ExceptionTool().arun(
                output.tool_input,
                verbose=self.verbose,
                color=None,
                callbacks=run_manager.get_child() if run_manager else None,
                **tool_run_kwargs,
            )
            yield AgentStep(action=output, observation=observation)
            return

        # If the tool chosen is the finishing tool, then we end and return.
        if isinstance(output, AgentFinish):
            yield output
            return

        actions: List[AgentAction]
        if isinstance(output, AgentAction):
            actions = [output]
        else:
            actions = output
        for agent_action in actions:
            yield agent_action

        # Use asyncio.gather to run multiple tool.arun() calls concurrently
        result = await asyncio.gather(
            *[
                self._aperform_agent_action(
                    name_to_tool_map, color_mapping, agent_action, inputs, run_manager
                )
                for agent_action in actions
            ],
        )

        # TODO This could yield each result as it becomes available
        for chunk in result:
            yield chunk


class SkilledAgent:
    """
    Skilled Agent Interface, that defines the basic structure for all of the skilled agents.
    Such as SQLAgent, DatabaseAgent, etc.

    @Attributes:
        setup_message: str
        model: LLMModel
        tools: List

    """

    __message_type = {"SystemMessage": SystemMessage, "HumanMessage": HumanMessage, "AIMessage": AIMessage}

    def __init__(self, setup_message: str, model, tools: List, *args, **kwargs) -> None:
        self.setup_message = setup_message
        self.model = model
        self.tools = tools

        self.args = args
        self.kwargs = kwargs

        messages: List = [
            *[
                self.__message_type[message["type"]](message["message"])
                for message in json.loads(self.setup_message)
            ],
            MessagesPlaceholder(variable_name="agent_scratchpad"),
            HumanMessagePromptTemplate.from_template("{input}"),
        ]
        self.prompt = ChatPromptTemplate.from_messages(messages)

        self.agent = self.create_agent_executor()

    def create_agent_executor(self) -> AgentExecutor:
        agent: Runnable = create_openai_tools_agent(
            llm=self.model, tools=self.tools, prompt=self.prompt
        )
        # return AgentExecutor(agent=agent, tools=self.tools)
        return AgentExecutorWithToolContext(agent=agent, tools=self.tools)

    async def run(self, prompt: str) -> str:
        return await self._run(prompt)

    async def _run(self, prompt: str) -> str:
        result = await self.agent.ainvoke({"input": prompt})
        return result["output"]
