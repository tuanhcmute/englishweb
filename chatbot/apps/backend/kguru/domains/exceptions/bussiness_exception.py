from typing import Any

from .base_exception import BaseApplicationException


class BussinessException(BaseApplicationException):
    def __init__(self, message: str, data: Any = None) -> None:
        super().__init__(message, data)

    @staticmethod
    def exist(info: str) -> Exception:
        return BussinessException(f"exist: {info}")

    @staticmethod
    def not_exist(info: str) -> Exception:
        return BussinessException(f"not exist: {info}")

    @staticmethod
    def timeout(info: str) -> Exception:
        return BussinessException(f"timeout: {info}")
