from typing import Any


class BaseApplicationException(Exception):
    message: str
    data: Any

    def __init__(self, message: str, data: Any) -> None:
        self.message = message
        self.data = data
