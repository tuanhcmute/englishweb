from typing import Any

from .base_exception import BaseApplicationException


class ValidationException(BaseApplicationException):
    def __init__(self, message: str, data: Any = None) -> None:
        super().__init__(message, data)

    @staticmethod
    def require(prop: str) -> Exception:
        return ValidationException(f"{prop} is required")

    @staticmethod
    def empty(prop: str) -> Exception:
        return ValidationException(f"{prop} is empty")
