from typing import List, Any

from kguru.services.agent_chat import AgentChatHistoryService
from kguru.domains.models.domain_model import DomainAgentChatHistoryModel


class AgentChatController:
    agent_chat_service: AgentChatHistoryService

    def __init__(self, agent_chat_service: AgentChatHistoryService) -> None:
        self.agent_chat_service = agent_chat_service

    async def create_new_chat_history(
        self, model: DomainAgentChatHistoryModel
    ) -> DomainAgentChatHistoryModel:
        return await self.agent_chat_service.create_new_chat_history(model=model)

    async def get_chat_history_by_user_and_skilled_version(
        self, offset: int, limit: int, **kwargs: Any
    ) -> List[DomainAgentChatHistoryModel]:
        return (
            await self.agent_chat_service.get_chat_histories_by_user_and_skilled_agent(
                offset=offset, limit=limit, **kwargs
            )
        )
