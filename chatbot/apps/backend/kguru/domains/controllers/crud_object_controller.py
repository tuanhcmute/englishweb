from typing import Generic, List, TypeVar, Any

from pydantic import BaseModel

from kguru.services.storages.storage_service import CRUDObject, StorageService
from kguru.domains.exceptions import ValidationException

T = TypeVar("T", bound=BaseModel)


class CRUDObjectController(Generic[T]):
    crud_object: StorageService

    def __init__(self, crud_object: StorageService) -> None:
        self.crud_object = crud_object

    async def create_new_object(self, model: T) -> T:
        if not model:
            raise ValidationException.require("model")

        if not bool(model):
            raise ValidationException.empty("model")

        return await self.crud_object.create_new_object(base_object_model=model)

    async def create_new_objects(self, models: List[T]):
        if not models:
            raise ValidationException.require("models")

        if not bool(models):
            raise ValidationException.empty("models")

        await self.crud_object.create_new_objects(base_object_models=models)

    async def read_object(self, id: str) -> T:
        if not id.strip():
            raise ValidationException.empty("id")

        return await self.crud_object.read_object(id=id)

    async def read_object_by_specific_attribute(self, **kwargs: Any) -> T:
        return await self.crud_object.read_object_by_specific_attribute(**kwargs)

    async def modify_object(self, id: str, model: T) -> T:
        if not model:
            raise ValidationException.require("model")

        return await self.crud_object.update_object(id, base_object_model=model)

    async def delete_object(self, id: str):
        if not id.strip():
            raise ValidationException.empty("id")
        await self.crud_object.delete_object(id=id)

    async def get_all_object(self) -> List[T]:
        print('get_all==========', await self.crud_object.get_all())
        return await self.crud_object.get_all()

    async def get_objects_by_specific_foreign_key(self, **kwargs: Any) -> List[T]:
        return await self.crud_object.get_objects_by_specific_foreign_key(**kwargs)
