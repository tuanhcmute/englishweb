from typing import List

from kguru.domains.models.domain_model import (
    DomainSkilledAgentTestModel,
    DomainTestRunResultModel,
)
from kguru.services.questions_and_answers_pharse import SkilledAgentTest


class SkilledAgentTestController:
    skilled_agent_test: SkilledAgentTest

    def __init__(self, skilled_agent_test: SkilledAgentTest) -> None:
        self.skilled_agent_test = skilled_agent_test

    async def run_skilled_agent_test(
        self, model: DomainSkilledAgentTestModel
    ) -> List[DomainTestRunResultModel]:
        return await self.skilled_agent_test.run_skilled_agent_test(model=model)

    async def re_run_skilled_agent_test(
        self, model: DomainSkilledAgentTestModel
    ) -> List[DomainTestRunResultModel]:
        return await self.skilled_agent_test.re_run_skilled_agent_test(model=model)

    async def show_result_skilled_agent_test(self):
        pass

    async def results_evaluation(self):
        pass
