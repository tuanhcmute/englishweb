import asyncio
from typing import Dict, List, Set, Any
from datetime import datetime
import logging

from kguru.domains.exceptions import ValidationException, BussinessException
from kguru.domains.models import Task, TaskState, TaskDependency, PromptProvider

logger = logging.getLogger()


class TaskExecutor:
    max_active_task: int
    max_retries: int
    task_retry_times: Dict[str, int]
    task_timeout: Dict[str, int]
    tasks: Dict[str, Task]
    task_dependencies: Dict[str, TaskDependency]
    task_states: Dict[str, TaskState]
    prompt_provider: PromptProvider

    def __init__(self, max_active_task: int, max_retries: int) -> None:
        self.max_active_task = max_active_task
        self.max_retries = max_retries
        self.tasks = {}
        self.task_retry_times = {}
        self.task_dependencies = {}
        self.task_timeout = {}
        self.task_states = {}
        self.loop = asyncio.AbstractEventLoop()
        self.prompt_provider = PromptProvider()

    async def run(self) -> Dict[str, str]:
        """
        Return dict format: (key, value) = (task_id, task_result)
        """

        self._validate_argument()
        self.loop = asyncio.get_running_loop()

        logger.info(f"start time: {datetime.now().strftime('%X')}")
        await self.await_all_task_complete(task_dependencies=self.task_dependencies)
        logger.info(f"end time: {datetime.now().strftime('%X')}")

        return self._get_task_results()

    async def await_all_task_complete(
        self, task_dependencies: Dict[str, TaskDependency]
    ):
        # store a list of tasks to be executed
        pending_tasks: Set[Any] = set()
        done_tasks: set[Any] = set()

        # init retry time and state for all tasks
        for task in self.tasks.values():
            self.task_retry_times[task.task_id] = 0
            self.task_states[task.task_id] = TaskState.QUEUED

        # loop all task in self.tasks to check if COMPLETE
        while True:
            if self._get_max_retries_running_tasks() >= self.max_retries:
                raise BussinessException("One task timeout or reach to max retries")

            queued_tasks = self._get_task_by_state(TaskState.QUEUED)
            running_tasks = self._get_task_by_state(TaskState.RUNNING)

            if running_tasks.__len__() >= self.max_active_task:
                continue
            if queued_tasks.__len__() == 0:
                break

            for task in queued_tasks:
                if pending_tasks.__len__() >= self.max_active_task:
                    break
                if not self._is_ready_to_run_task(task, task_dependencies):
                    continue

                logger.info(
                    f"Add task: {task.task_id} {f', executing at retries time {self.task_retry_times[task.task_id]}' if self.task_retry_times[task.task_id] > 0 else ''}"
                )

                pending_tasks.add(self.loop.create_task(self.run_task(task=task)))

            logger.info(
                f"Running tasks: {running_tasks.__len__()}. Pending tasks: {pending_tasks.__len__()}"
            )

            if pending_tasks.__len__() >= self.max_active_task:
                done_tasks, pending_tasks = await asyncio.wait(
                    pending_tasks, return_when="FIRST_COMPLETED"
                )
            else:
                if pending_tasks.__len__() > 0:
                    done_tasks, pending_tasks = await asyncio.wait(pending_tasks)

            logger.info(f"{done_tasks.__len__()} task has just executed")

    async def run_task(self, task: Task) -> bool:
        if not bool(task):
            raise ValidationException.empty("task")

        try:
            self.task_states[task.task_id] = TaskState.RUNNING
            prompt = await self._get_task_input(task=task)
            await asyncio.wait_for(
                task.run(prompt=prompt),
                timeout=self.task_timeout[task.task_id],
            )

            task_state = self._get_task_state_from_result(task=task)
            if task_state == TaskState.SUCCESS:
                self.task_states[task.task_id] = TaskState.SUCCESS
                return True
            else:
                self._handle_task_fail(task=task)
                return False

        except asyncio.TimeoutError:
            logger.warning(f"Time out occur at task: {task.task_id}")
            self._handle_task_fail(task=task)
            return False

    def set_tasks(self, tasks: Dict[str, Task]):
        self.tasks = tasks
        return self

    def set_timeouts(self, task_timeout: Dict[str, int]):
        self.task_timeout = task_timeout
        return self

    def set_task_dependencies(self, task_dependencies: Dict[str, TaskDependency]):
        self.task_dependencies = task_dependencies
        return self

    async def _get_task_input(self, task: Task):
        # Use task id to get dependency(List task id in task dependencies) after build it by PromptProvider to the new prompt for run task
        tasks: list[Task] = []
        if task.task_id in self.task_dependencies:
            tasks = [
                self.tasks[task_id]
                for task_id in self.task_dependencies[
                    task.task_id
                ].dependency.get_tasks()
            ]
        return await self.prompt_provider.summary_prompt(tasks=tasks)

    def _get_task_state_from_result(self, task: Task) -> TaskState:
        return TaskState.FAILED if not task.task_result.strip() else TaskState.SUCCESS

    def _validate_argument(self) -> bool:
        if self.task_timeout.__len__() == 0:
            raise ValidationException.empty("Timeout")

        if self.tasks.__len__() == 0:
            raise ValidationException.empty("Tasks")

        if self.task_dependencies.__len__() == 0:
            raise ValidationException.empty("task_dependencies")

        return True

    def _get_task_by_state(self, task_state: TaskState) -> List[Task]:
        return [
            task
            for task in self.tasks.values()
            if self.task_states[task.task_id] == task_state
        ]

    def _is_ready_to_run_task(
        self,
        task: Task,
        task_dependencies: Dict[str, TaskDependency],
    ):
        """
        Check task has already ready to run
        """

        if self.task_states[task.task_id] != TaskState.QUEUED:
            return False
        if task.task_id not in task_dependencies:
            return True

        is_executed_success = all(
            [
                self.task_states[dep_task_id] == TaskState.SUCCESS
                for dep_task_id in task_dependencies[
                    task.task_id
                ].dependency.get_tasks()
            ]
        )
        if is_executed_success:
            is_dependecy_ok = task_dependencies[task.task_id].dependency.compare(
                tasks=self.tasks
            )

            if not is_dependecy_ok:
                raise BussinessException(
                    f"Expression not success at task: {task.task_id}"
                )

        return is_executed_success

    def _handle_task_fail(self, task: Task):
        self.task_retry_times[task.task_id] += 1
        self.task_states[task.task_id] = TaskState.QUEUED

    def _get_max_retries_running_tasks(self) -> int:
        return (
            max(self.task_retry_times.values())
            if self.task_retry_times.__len__() > 0
            else 0
        )

    def _get_task_results(self) -> Dict[str, str]:
        if not all([state == TaskState.SUCCESS for state in self.task_states.values()]):
            raise BussinessException("Not all task success")

        return {task.task_id: task.task_result for task in self.tasks.values()}
