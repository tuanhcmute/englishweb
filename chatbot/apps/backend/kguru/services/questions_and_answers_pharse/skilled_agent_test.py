from typing import List
import asyncio
import json
import traceback
from datetime import datetime

from kguru.services.storages.storages_config import BaseConfig

from kguru.services.tools import (
    ToolLoader,
)
from kguru.services.llm_model import (
    LLMModelLoader,
)
from kguru.domains.models import SkilledAgent
from kguru.services.storages.storage_service import (
    CRUDTestSuiteObject,
    CRUDQuestionObject,
    CRUDSkilledAgentVersionObject,
    CRUDSkilledAgentObject,
    CRUDAnswerObject,
    CRUDTestRunResultObject,
    CRUDTestSuiteRunObject,
    CRUDLLMModelObject,
    CRUDRunQuestionObject,
)
from kguru.domains.models.domain_model import (
    DomainSkilledAgentTestModel,
    DomainTestSuiteRunModel,
    DomainTestSuiteModel,
    DomainQuestionModel,
    DomainSkilledAgentVersionModel,
    DomainToolModel,
    DomainLLMModel,
    DomainAnswerModel,
    DomainTestRunResultModel,
    DomainRunQuestionModel,
)
from kguru import config


class SkilledAgentTest:
    storage_driver: BaseConfig
    crud_test_suite: CRUDTestSuiteObject
    crud_answer: CRUDAnswerObject
    crud_test_run_result: CRUDTestRunResultObject
    crud_question: CRUDQuestionObject
    crud_skilled_agent: CRUDSkilledAgentObject
    crud_skilled_agent_version: CRUDSkilledAgentVersionObject
    crud_test_suite_run: CRUDTestSuiteRunObject
    crud_llm_model: CRUDLLMModelObject
    crud_run_question: CRUDRunQuestionObject

    def __init__(self, storage_driver: BaseConfig) -> None:
        self.storage_driver = storage_driver
        self.crud_test_suite = CRUDTestSuiteObject(storage_driver=storage_driver)
        self.crud_answer = CRUDAnswerObject(storage_driver=storage_driver)
        self.crud_test_run_result = CRUDTestRunResultObject(
            storage_driver=storage_driver
        )
        self.crud_question = CRUDQuestionObject(storage_driver=storage_driver)
        self.crud_skilled_agent = CRUDSkilledAgentObject(storage_driver=storage_driver)
        self.crud_skilled_agent_version = CRUDSkilledAgentVersionObject(
            storage_driver=storage_driver
        )
        self.crud_test_suite_run = CRUDTestSuiteRunObject(storage_driver=storage_driver)
        self.crud_llm_model = CRUDLLMModelObject(storage_driver=storage_driver)
        self.crud_run_question = CRUDRunQuestionObject(storage_driver=storage_driver)

        self.llm_model_loader = LLMModelLoader(config.LLM_MODEL_PATH)
        self.llm_model_loader.load_classes()

        self.tool_loader = ToolLoader(config.TOOL_PATH)
        self.tool_loader.load_classes()

    async def run_skilled_agent_test(
        self, model: DomainSkilledAgentTestModel
    ) -> List[DomainTestRunResultModel]:
        test_suite_run_id: str = model.test_suite_run_id
        skilled_agent_version_id: str = model.skilled_agent_version_id

        # Get test suite id and domain questions
        test_suite_run: DomainTestSuiteRunModel = (
            await self.crud_test_suite_run.read_object(id=test_suite_run_id)
        )
        test_suite_id: str = test_suite_run.test_suite_id
        test_suite: DomainTestSuiteModel = await self.crud_test_suite.read_object(
            id=test_suite_id
        )
        questions: List[DomainQuestionModel] = test_suite.questions  # type: ignore

        # Get skilled agent version
        skilled_agent_version: DomainSkilledAgentVersionModel = (
            await self.crud_skilled_agent_version.read_object(
                id=skilled_agent_version_id
            )
        )

        llm_models: List[DomainLLMModel] = skilled_agent_version.llm_model_result

        # Get tools from Skilled agent version
        tools: List[DomainToolModel] = skilled_agent_version.tools
        domain_llm_model: DomainLLMModel = llm_models[0]

        setup_message: str = skilled_agent_version.setup_message

        llm_model_definition = self.llm_model_loader.get_class(
            domain_llm_model.llm_model_name
        )
        llm_model = llm_model_definition()

        tools_definition: List[dict] = [
            (
                tool.param,
                self.tool_loader.get_class(tool.tool_name),
            )
            for tool in skilled_agent_version.tools
        ]
        tools = [
            tool(json.loads(param)).build()
            for param, tool in tools_definition
            if tool is not None
        ]

        if not tools:
            raise Exception("No tools found for skilled agent")

        test_run_results: List[DomainTestRunResultModel] = []

        skilled_agent_running: SkilledAgent = SkilledAgent(
            model=llm_model, setup_message=setup_message, tools=tools
        )

        tasks = []
        created_at = datetime.now()
        for question in questions:
            task = asyncio.ensure_future(
                self._run_test_suite(
                    question=question,
                    skilled_agent=skilled_agent_running,
                    model=model,
                    created_at=created_at,
                )
            )
            tasks.append(task)

        tasks_to_run = asyncio.gather(*tasks)
        test_run_results = await tasks_to_run

        return test_run_results

    async def re_run_skilled_agent_test(
        self, model: DomainSkilledAgentTestModel
    ) -> List[DomainTestRunResultModel]:
        skilled_agent_version_id: str = model.skilled_agent_version_id

        # Get skilled agent version
        skilled_agent_version: DomainSkilledAgentVersionModel = (
            await self.crud_skilled_agent_version.read_object(
                id=skilled_agent_version_id
            )
        )

        llm_models: List[DomainLLMModel] = skilled_agent_version.llm_model_result

        # Get tools from Skilled agent version
        tools: List[DomainToolModel] = skilled_agent_version.tools
        domain_llm_model: DomainLLMModel = llm_models[0]

        setup_message: str = skilled_agent_version.setup_message

        llm_model_definition = self.llm_model_loader.get_class(
            domain_llm_model.llm_model_name
        )
        llm_model = llm_model_definition()

        tools_definition: List[dict] = [
            (
                tool.param,
                self.tool_loader.get_class(tool.tool_name),
            )
            for tool in skilled_agent_version.tools
        ]
        tools = [
            tool(json.loads(param)).build()
            for param, tool in tools_definition
            if tool is not None
        ]

        if not tools:
            raise Exception("No tools found for skilled agent")

        skilled_agent_running: SkilledAgent = SkilledAgent(
            model=llm_model, setup_message=setup_message, tools=tools
        )

        old_test_run_results: List[DomainTestRunResultModel] = (
            await self.crud_test_run_result.get_objects_by_specific_foreign_key(
                test_suite_run_id=model.test_suite_run_id,
                skilled_agent_version_id=model.skilled_agent_version_id,
            )
        )

        tasks = []
        created_at = datetime.now()
        for test_run_result in old_test_run_results:
            run_question: DomainRunQuestionModel = test_run_result.run_question_result
            del run_question.__dict__["id"]
            task = asyncio.ensure_future(
                self._rerun_test_suite(
                    run_question=run_question,
                    model=model,
                    skilled_agent=skilled_agent_running,
                    created_at=created_at,
                )
            )
            tasks.append(task)
        tasks_to_run = asyncio.gather(*tasks)
        re_run_results = await tasks_to_run

        return re_run_results

    async def _rerun_test_suite(
        self,
        run_question: DomainRunQuestionModel,
        model: DomainSkilledAgentTestModel,
        skilled_agent: SkilledAgent,
        created_at: datetime,
    ) -> DomainTestRunResultModel:

        run_question_model: DomainRunQuestionModel = (
            await self.crud_run_question.create_new_object(
                base_object_model=run_question
            )
        )
        start_time: datetime = datetime.now()
        try:
            skilled_agent_run_output: str = await skilled_agent.run(
                prompt=run_question.question_name
            )
        except Exception as e:
            skilled_agent_run_output = str(e)
            traceback.print_exc()

        end_time: datetime = datetime.now()
        execution_time: float = (end_time - start_time).total_seconds()

        answer: DomainAnswerModel = DomainAnswerModel(
            answer_content=skilled_agent_run_output
        )
        answer_model: DomainAnswerModel = await self.crud_answer.create_new_object(
            base_object_model=answer
        )

        test_run_result: DomainTestRunResultModel = DomainTestRunResultModel(
            test_suite_run_id=model.test_suite_run_id,
            skilled_agent_version_id=model.skilled_agent_version_id,
            is_pass=False,
            speed=execution_time,
            run_question_id=run_question_model.id,
            answer_id=answer_model.id,
            created_at=created_at,
        )

        result: DomainTestRunResultModel = (
            await self.crud_test_run_result.create_new_object(
                base_object_model=test_run_result
            )
        )
        return result

    async def _run_test_suite(
        self,
        question: DomainQuestionModel,
        skilled_agent: SkilledAgent,
        model: DomainSkilledAgentTestModel,
        created_at: datetime,
    ) -> DomainTestRunResultModel:
        run_question: DomainRunQuestionModel = DomainRunQuestionModel(
            question_name=question.question_name,
            expected_answer=question.expected_answer,
        )
        run_question_model: DomainRunQuestionModel = (
            await self.crud_run_question.create_new_object(
                base_object_model=run_question
            )
        )
        start_time: datetime = datetime.now()
        try:
            skilled_agent_run_output: str = await skilled_agent.run(
                prompt=question.question_name
            )
        except Exception as e:
            skilled_agent_run_output = str(e)
            traceback.print_exc()

        end_time: datetime = datetime.now()
        execution_time: float = (end_time - start_time).total_seconds()

        answer: DomainAnswerModel = DomainAnswerModel(
            answer_content=skilled_agent_run_output
        )
        answer_model: DomainAnswerModel = await self.crud_answer.create_new_object(
            base_object_model=answer
        )

        test_run_result: DomainTestRunResultModel = DomainTestRunResultModel(
            test_suite_run_id=model.test_suite_run_id,
            skilled_agent_version_id=model.skilled_agent_version_id,
            is_pass=False,
            speed=execution_time,
            run_question_id=run_question_model.id,
            answer_id=answer_model.id,
            created_at=created_at,
        )

        result: DomainTestRunResultModel = (
            await self.crud_test_run_result.create_new_object(
                base_object_model=test_run_result
            )
        )
        return result
