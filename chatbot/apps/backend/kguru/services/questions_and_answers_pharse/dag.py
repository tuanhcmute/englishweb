import logging
from typing import Dict

from kguru.domains.models.task_dependency import TaskDependency

from kguru.domains.exceptions import BussinessException, ValidationException
from kguru.domains.models import Task
from .task_executor import TaskExecutor

logger = logging.getLogger(__name__)


class DAG:
    dag_id: str
    tasks: Dict[str, Task]
    task_executor: TaskExecutor
    task_dependencies: Dict[str, TaskDependency]
    task_timeout: Dict[str, int]

    def __init__(
        self,
        dag_id: str,
        max_retries: int = 4,
        max_active_task: int = 16,
    ) -> None:
        self.dag_id = dag_id
        self.tasks = {}
        self.task_dependencies = {}
        self.task_executor = TaskExecutor(
            max_retries=max_retries, max_active_task=max_active_task
        )

    def add_task(self, task: Task):
        if not task.task_id.strip():
            raise ValidationException.empty("task id")

        if task.task_id in self.tasks:
            raise BussinessException.exist("task")

        self.tasks[task.task_id] = task
        return self

    def set_task_timeout(self, task_timeout: Dict[str, int]):
        if task_timeout.__len__() == 0:
            raise BussinessException.not_exist("task_timeout")

        self.task_timeout = task_timeout
        return self

    def set_dependency(self, target_id: str, task_dependency: TaskDependency):
        if self.get_task(task_id=target_id) is None:
            raise BussinessException.not_exist("task")

        if any(
            task_id not in self.tasks
            for task_id in task_dependency.dependency.get_tasks()
        ):
            raise BussinessException.not_exist("task in dependency")

        self.task_dependencies[target_id] = task_dependency

        return self

    def get_task(self, task_id: str) -> Task | None:
        return self.tasks[task_id] if task_id in self.tasks else None

    def remove_task(self, task_id: str) -> Task | None:
        if self.get_task(task_id=task_id) is None:
            raise BussinessException.not_exist("task")

        self.tasks.pop(task_id, None)

    async def run(self) -> Dict[str, Dict[str, str]]:
        if len(self.tasks) == 0:
            raise ValidationException.empty("tasks")

        self._check_dependency_met()
        self.task_executor.set_tasks(tasks=self.tasks).set_timeouts(
            task_timeout=self.task_timeout
        ).set_task_dependencies(task_dependencies=self.task_dependencies)

        logger.info(f"Running {self.tasks.__len__()} tasks")
        return {"result": await self.task_executor.run()}

    def _check_dependency_met(self):
        pass
