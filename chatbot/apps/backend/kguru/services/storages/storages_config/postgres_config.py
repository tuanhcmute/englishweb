# type: ignore
from kguru.services.storages.schemas import (
    Answer,
    Question,
    RunQuestion,
    RunTool,
    SkilledAgent,
    SkilledAgentVersion,
    TestRunResult,
    TestSuiteRun,
    TestSuiteSkilledAgent,
    TestSuite,
    Tool,
    LLMModel,
)

from sqlmodel import Session, create_engine, SQLModel
from sqlalchemy.engine.base import Engine
from sqlalchemy.exc import InterfaceError

from .base_config import BaseConfig

from kguru.domains.exceptions import ValidationException, BussinessException


class PostgresConfig(BaseConfig):
    engine: Engine | None
    username: str
    password: str
    host: str
    port: int
    database: str

    def __init__(self) -> None:
        self.engine = None
        self.username = ""
        self.password = ""
        self.host = ""
        self.port = 0
        self.database = ""

    def set_username(self, username: str):

        if not username.strip():
            raise ValidationException.empty("username")

        self.username = username
        return self

    def set_password(self, password: str):
        if not password.strip():
            raise ValidationException.empty("password")

        self.password = password
        return self

    def set_host(self, host: str):
        if not host.strip():
            raise ValidationException.empty("host")

        self.host = host
        return self

    def set_port(self, port: int):
        if port < 1 or port > 65535:
            raise ValidationException("Port value is invalid!")
        self.port = port
        return self

    def set_database(self, database: str):
        if not database.strip():
            raise ValidationException.empty("database")

        self.database = database
        return self

    def connect(self):
        try:
            connection_str: str = (
                # f"postgresql+pg8000://{self.username}:{self.password}@{self.host}:{self.port}/{self.database}"
                f"mysql+pymysql://{self.username}:{self.password}@{self.host}:{self.port}/{self.database}"
            )
            self.engine = create_engine(connection_str)

            SQLModel.metadata.create_all(
                self.engine,
            )
        except InterfaceError as err:
            raise BussinessException(err.orig.args[0])  # type: ignore

    def get_engine(self) -> Engine:
        return self.engine

    def session(self) -> Session:
        session: Session = Session(self.engine, expire_on_commit=False)
        return session
