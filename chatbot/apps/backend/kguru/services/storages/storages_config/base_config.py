from sqlmodel import Session
from sqlalchemy.engine.base import Engine


class BaseConfig:
    def get_engine(self) -> Engine:  # type: ignore
        pass

    def session(self) -> Session:  # type: ignore
        pass
