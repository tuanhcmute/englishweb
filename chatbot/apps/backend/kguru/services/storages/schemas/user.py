import uuid
from typing import List

from sqlmodel import Field, SQLModel, Relationship


class User(SQLModel, table=True):
    __tablename__: str = "user"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    user_name: str
    email: str
    platform: str

    agent_chat_histories: List["AgentChatHistory"] = Relationship(back_populates="user")  # type: ignore
