import uuid

from typing import List

from sqlmodel import Field, SQLModel, Relationship

from .test_suite_skilled_agent import TestSuiteSkilledAgent


class TestSuite(SQLModel, table=True):
    __tablename__: str = "test_suite"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    test_suite_name: str
    description: str

    test_suite_runs: List["TestSuiteRun"] = Relationship(back_populates="test_suite")  # type: ignore
    skilled_agents: List["SkilledAgent"] = Relationship(back_populates="test_suites", link_model=TestSuiteSkilledAgent)  # type: ignore
    questions: List["Question"] = Relationship(back_populates="test_suite")  # type: ignore
