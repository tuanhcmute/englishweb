import uuid

from typing import List

from sqlmodel import Field, SQLModel, Relationship

from .test_suite_skilled_agent import TestSuiteSkilledAgent


class SkilledAgent(SQLModel, table=True):
    __tablename__: str = "skilled_agent"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    agent_name: str
    setup_message: str
    test_score: float
    description: str
    skilled_agent_versions: List["SkilledAgentVersion"] = Relationship(back_populates="skilled_agent")  # type: ignore
    test_suites: List["TestSuite"] = Relationship(back_populates="skilled_agents", link_model=TestSuiteSkilledAgent)  # type: ignore
