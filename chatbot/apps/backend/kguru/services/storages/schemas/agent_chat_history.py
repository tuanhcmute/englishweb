import uuid
from typing import Optional
from datetime import datetime

from sqlmodel import Field, SQLModel, Relationship
from sqlalchemy.sql.sqltypes import String
from sqlalchemy import ForeignKey, Column

from .skilled_agent_version import SkilledAgentVersion
from .user import User


class AgentChatHistory(SQLModel, table=True):
    __tablename__: str = "agent_chat_history"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    message: str
    is_kguru_msg: bool
    created_at: datetime
    skilled_agent_version_id: Optional[str] = Field(
        sa_column=Column(
            String(255), ForeignKey("skilled_agent_version.id", ondelete="CASCADE")
        )
    )
    user_id: Optional[str] = Field(
        sa_column=Column(String(255), ForeignKey("user.id", ondelete="CASCADE"))
    )

    skilled_agent_version: Optional[SkilledAgentVersion] = Relationship(
        back_populates="agent_chat_histories"
    )
    user: Optional[User] = Relationship(back_populates="agent_chat_histories")
