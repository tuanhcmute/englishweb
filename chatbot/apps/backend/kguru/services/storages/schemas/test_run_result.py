import uuid
from datetime import datetime

from typing import List, Optional

from sqlmodel import Field, SQLModel, Relationship
from sqlalchemy.sql.sqltypes import String
from sqlalchemy import ForeignKey, Column

from .skilled_agent_version import SkilledAgentVersion
from .test_suite_run import TestSuiteRun
from .run_question import RunQuestion
from .answer import Answer


class TestRunResult(SQLModel, table=True):
    __tablename__: str = "test_run_result"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    test_suite_run_id: Optional[str] = Field(
        sa_column=Column(String(255), ForeignKey("test_suite_run.id", ondelete="CASCADE"))
    )
    skilled_agent_version_id: Optional[str] = Field(
        sa_column=Column(
            String(255), ForeignKey("skilled_agent_version.id", ondelete="CASCADE")
        )
    )
    is_pass: bool
    speed: float
    run_question_id: Optional[str] = Field(
        sa_column=Column(String(255), ForeignKey("run_question.id", ondelete="CASCADE"))
    )
    answer_id: Optional[str] = Field(
        sa_column=Column(String(255), ForeignKey("answer.id", ondelete="CASCADE"))
    )
    created_at: datetime

    test_suite_run: Optional[TestSuiteRun] = Relationship(
        back_populates="test_run_results"
    )
    skilled_agent_version: Optional[SkilledAgentVersion] = Relationship(
        back_populates="test_run_results"
    )
    run_question: Optional[RunQuestion] = Relationship(
        back_populates="test_run_results"
    )
    answer: Optional[Answer] = Relationship(back_populates="test_run_results")
