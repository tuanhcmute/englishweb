import uuid

from typing import List, Optional

from sqlmodel import Field, SQLModel, Relationship
from sqlalchemy.sql.sqltypes import String
from sqlalchemy import ForeignKey, Column

from .test_suite import TestSuite
from .skilled_agent_version import SkilledAgentVersion


class TestSuiteRun(SQLModel, table=True):
    __tablename__: str = "test_suite_run"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    version: str
    score: float
    run_status: str
    the_number_of_question: int
    test_suite_id: Optional[str] = Field(
        sa_column=Column(String(255), ForeignKey("test_suite.id", ondelete="CASCADE"))
    )
    skilled_agent_version_id: Optional[str] = Field(
        sa_column=Column(
            String(255), ForeignKey("skilled_agent_version.id", ondelete="CASCADE")
        )
    )
    test_run_results: List["TestRunResult"] = Relationship(back_populates="test_suite_run")  # type: ignore

    test_suite: TestSuite = Relationship(back_populates="test_suite_runs")
    skilled_agent_version: SkilledAgentVersion = Relationship(
        back_populates="test_suite_runs"
    )
