from sqlmodel import Field, SQLModel


class RunLLMModel(SQLModel, table=True):
    __tablename__ = "run_llm_model"  # type: ignore

    llm_model_id: str | None = Field(default=None, foreign_key="llm_model.id", primary_key=True)
    skilled_agent_version_id: str | None = Field(
        default=None, foreign_key="skilled_agent_version.id", primary_key=True
    )
