import uuid
from typing import List, Optional

from sqlmodel import Field, SQLModel, Relationship
from sqlalchemy.sql.sqltypes import String
from sqlalchemy import ForeignKey, Column

from .test_suite import TestSuite


class Question(SQLModel, table=True):
    __tablename__: str = "question"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    question_name: str
    expected_answer: str
    test_suite_id: Optional[str] = Field(
        sa_column=Column(String(255), ForeignKey("test_suite.id", ondelete="CASCADE"))
    )

    test_suite: Optional[TestSuite] = Relationship(back_populates="questions")  # type: ignore
