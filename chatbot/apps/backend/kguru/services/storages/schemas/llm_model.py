import uuid
from typing import Optional, List

from sqlmodel import Field, SQLModel, Relationship

from .run_llm_model import RunLLMModel


class LLMModel(SQLModel, table=True):
    __tablename__: str = "llm_model"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    llm_model_name: str
    param: str
    skilled_agent_versions: List["SkilledAgentVersion"] = Relationship(back_populates="llm_models", link_model=RunLLMModel)  # type: ignore
