import uuid
from typing import List

from sqlmodel import Field, SQLModel, Relationship


class RunQuestion(SQLModel, table=True):
    __tablename__ = "run_question"  # type: ignore

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    question_name: str
    expected_answer: str
    test_run_results: List["TestRunResult"] = Relationship(back_populates="run_question")  # type: ignore
