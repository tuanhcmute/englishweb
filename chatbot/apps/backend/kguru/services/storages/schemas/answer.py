import uuid
from typing import List

from sqlmodel import Field, SQLModel, Relationship


class Answer(SQLModel, table=True):
    __tablename__: str = "answer"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    answer_content: str
    test_run_results: List["TestRunResult"] = Relationship(back_populates="answer")  # type: ignore
