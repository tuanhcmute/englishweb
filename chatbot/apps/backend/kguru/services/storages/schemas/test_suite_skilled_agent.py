from sqlmodel import Field, SQLModel


class TestSuiteSkilledAgent(SQLModel, table=True):
    __tablename__ = "test_suite_skilled_agent"  # type: ignore

    test_suite_id: str | None = Field(
        default=None, foreign_key="test_suite.id", primary_key=True
    )
    skilled_agent_id: str | None = Field(
        default=None, foreign_key="skilled_agent.id", primary_key=True
    )
