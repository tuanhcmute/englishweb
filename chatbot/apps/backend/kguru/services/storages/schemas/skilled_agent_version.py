import uuid

from typing import List, Optional

from sqlmodel import Field, SQLModel, Relationship
from sqlalchemy.sql.sqltypes import String
from sqlalchemy import ForeignKey, Column, Text
from sqlalchemy import UniqueConstraint

from .skilled_agent import SkilledAgent
from .run_tool import RunTool
from .run_llm_model import RunLLMModel


class SkilledAgentVersion(SQLModel, table=True):
    __tablename__: str = "skilled_agent_version"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    skilled_agent_id: Optional[str] = Field(
        sa_column=Column(String(255), ForeignKey("skilled_agent.id", ondelete="CASCADE"))
    )
    version: str
    setup_message: str = Field(sa_column=Column(Text))
    tools: List["Tool"] = Relationship(back_populates="skilled_agent_versions", link_model=RunTool)  # type: ignore
    llm_models: List["LLMModel"] = Relationship(back_populates="skilled_agent_versions", link_model=RunLLMModel)  # type: ignore
    test_run_results: List["TestRunResult"] = Relationship(back_populates="skilled_agent_version")  # type: ignore
    test_suite_runs: List["TestSuiteRun"] = Relationship(back_populates="skilled_agent_version")  # type: ignore
    agent_chat_histories: List["AgentChatHistory"] = Relationship(back_populates="skilled_agent_version")  # type: ignore

    skilled_agent: Optional[SkilledAgent] = Relationship(
        back_populates="skilled_agent_versions"
    )

    # __table_args__ = (
    #     UniqueConstraint(
    #         "skilled_agent_id",
    #         "version",
    #         name="_skilled_agent_version_uc",
    #     ),
    # )
