from sqlmodel import Field, SQLModel


class RunTool(SQLModel, table=True):
    __tablename__ = "run_tool"  # type: ignore

    tool_id: str | None = Field(default=None, foreign_key="tool.id", primary_key=True)
    skilled_agent_version_id: str | None = Field(
        default=None, foreign_key="skilled_agent_version.id", primary_key=True
    )
