import uuid

from typing import List

from sqlmodel import Field, SQLModel, Relationship

from .run_tool import RunTool


class Tool(SQLModel, table=True):
    __tablename__: str = "tool"

    id: str = Field(primary_key=True, default_factory=lambda: str(uuid.uuid4()))
    tool_name: str
    param: str
    skilled_agent_versions: List["SkilledAgentVersion"] = Relationship(back_populates="tools", link_model=RunTool)  # type: ignore
