from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import TestSuite, SkilledAgent, Question
from kguru.domains.models.domain_model import DomainTestSuiteModel
from kguru.services.storages.data_mapper import TestSuiteDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDTestSuiteObject(CRUDObject[DomainTestSuiteModel]):
    crud: CRUD
    crud_skilled_agent: CRUD
    crud_question: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=TestSuite)
        self.crud_skilled_agent = CRUD(
            storage_driver=storage_driver, schema_type=SkilledAgent
        )
        self.crud_question = CRUD(storage_driver=storage_driver, schema_type=Question)

    async def create_new_object(
        self, base_object_model: DomainTestSuiteModel
    ) -> DomainTestSuiteModel:
        test_suite: TestSuite = TestSuiteDataMapper.to_sqlmodel(model=base_object_model)

        skilled_agents: List[SkilledAgent] = []
        for skilled_agent_id in base_object_model.skilled_agent_ids:
            skilled_agents.append(
                await self.crud_skilled_agent.read_object(id=skilled_agent_id)
            )

        test_suite.skilled_agents = skilled_agents

        result: TestSuite = await self.crud.create_new_object(
            base_object_model=test_suite
        )
        return TestSuiteDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainTestSuiteModel:
        result: TestSuite = await self.crud.read_object(id=id)
        return TestSuiteDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainTestSuiteModel
    ) -> DomainTestSuiteModel:
        test_suite: TestSuite = TestSuiteDataMapper.to_sqlmodel(model=base_object_model)

        skilled_agents: List[SkilledAgent] = []
        for skilled_agent_id in base_object_model.skilled_agent_ids:
            skilled_agents.append(
                await self.crud_skilled_agent.read_object(id=skilled_agent_id)
            )

        questions: List[Question] = []
        for question_id in base_object_model.question_ids:
            questions.append(await self.crud_question.read_object(id=question_id))

        test_suite.skilled_agents = skilled_agents
        test_suite.questions = questions

        result: TestSuite = await self.crud.update_object(
            id=id, base_object_model=test_suite
        )
        return TestSuiteDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainTestSuiteModel]:
        results: List[TestSuite] = await self.crud.get_all()

        return [TestSuiteDataMapper.to_domain_model(result) for result in results]
