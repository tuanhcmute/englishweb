from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import RunQuestion

from kguru.domains.models.domain_model import DomainRunQuestionModel
from kguru.services.storages.data_mapper import RunQuestionDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDRunQuestionObject(CRUDObject[RunQuestion]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=RunQuestion)

    async def create_new_object(
        self, base_object_model: DomainRunQuestionModel
    ) -> DomainRunQuestionModel:
        run_question: RunQuestion = RunQuestionDataMapper.to_sqlmodel(
            model=base_object_model
        )
        result: RunQuestion = await self.crud.create_new_object(
            base_object_model=run_question
        )
        return RunQuestionDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainRunQuestionModel:
        result: RunQuestion = await self.crud.read_object(id=id)
        return RunQuestionDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainRunQuestionModel
    ) -> DomainRunQuestionModel:
        run_question: RunQuestion = RunQuestionDataMapper.to_sqlmodel(
            model=base_object_model
        )
        result: RunQuestion = await self.crud.update_object(
            id=id, base_object_model=run_question
        )
        return RunQuestionDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainRunQuestionModel]:
        results: List[RunQuestion] = await self.crud.get_all()

        return TypeAdapter(List[DomainRunQuestionModel]).dump_python(results)  # type: ignore
