from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import SkilledAgent, TestSuite
from kguru.domains.models.domain_model import DomainSkilledAgentModel
from kguru.services.storages.data_mapper import SkilledAgentDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDSkilledAgentObject(CRUDObject[DomainSkilledAgentModel]):
    crud: CRUD
    crud_test_suite: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=SkilledAgent)
        self.crud_test_suite = CRUD(
            storage_driver=storage_driver, schema_type=TestSuite
        )

    async def create_new_object(
        self, base_object_model: DomainSkilledAgentModel
    ) -> DomainSkilledAgentModel:
        skilled_agent: SkilledAgent = SkilledAgentDataMapper.to_sqlmodel(
            model=base_object_model
        )

        test_suites: List[TestSuite] = []
        for test_suite_id in base_object_model.test_suite_ids:
            test_suites.append(await self.crud_test_suite.read_object(id=test_suite_id))
        skilled_agent.test_suites = test_suites

        result: SkilledAgent = await self.crud.create_new_object(
            base_object_model=skilled_agent
        )

        return SkilledAgentDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainSkilledAgentModel:
        result: SkilledAgent = await self.crud.read_object(id=id)
        return SkilledAgentDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainSkilledAgentModel
    ) -> DomainSkilledAgentModel:
        skilled_agent: SkilledAgent = SkilledAgentDataMapper.to_sqlmodel(
            model=base_object_model
        )
        result: SkilledAgent = await self.crud.update_object(
            id=id, base_object_model=skilled_agent
        )
        return SkilledAgentDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainSkilledAgentModel]:
        results: List[SkilledAgent] = await self.crud.get_all()
        return TypeAdapter(List[DomainSkilledAgentModel]).dump_python(results)  # type: ignore
