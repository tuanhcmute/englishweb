from typing import List, Any
from datetime import datetime

from pydantic import BaseModel

from kguru.services.storages.storages_config import BaseConfig

from typing import Generic, TypeVar
from abc import ABC

T = TypeVar("T", bound=BaseModel)


class CRUDObject(ABC, Generic[T]):
    storage_driver: BaseConfig

    def __init__(self, storage_driver: BaseConfig) -> None:
        self.storage_driver = storage_driver

    async def create_new_object(self, base_object_model: T) -> T:  # type: ignore
        pass

    async def create_new_objects(self, base_object_models: List[T]):
        pass

    async def read_object(self, id: str) -> T:  # type: ignore
        pass

    async def read_object_by_specific_attribute(self, **kwargs: Any) -> T:  # type: ignore
        pass

    async def update_object(self, id: str, base_object_model: T) -> T:  # type: ignore
        pass

    async def delete_object(self, id: str):
        pass

    async def get_all(self) -> List[T]:  # type: ignore
        pass

    async def get_objects_by_specific_foreign_key(
        self, **kwargs: Any
    ) -> List[T]:  # type: ignore
        pass

    async def get_latest_created_date(self) -> datetime:  # type: ignore
        pass
