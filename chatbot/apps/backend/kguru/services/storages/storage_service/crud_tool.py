from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import Tool
from kguru.domains.models.domain_model import DomainToolModel
from kguru.services.storages.data_mapper import ToolDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDToolObject(CRUDObject[DomainToolModel]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=Tool)

    async def create_new_object(
        self, base_object_model: DomainToolModel
    ) -> DomainToolModel:
        tool: Tool = ToolDataMapper.to_sqlmodel(model=base_object_model)
        result: Tool = await self.crud.create_new_object(base_object_model=tool)
        return ToolDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainToolModel:
        result: Tool = await self.crud.read_object(id=id)
        return ToolDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainToolModel
    ) -> DomainToolModel:
        tool: Tool = ToolDataMapper.to_sqlmodel(model=base_object_model)
        result: Tool = await self.crud.update_object(id=id, base_object_model=tool)
        return ToolDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainToolModel]:
        results: List[Tool] = await self.crud.get_all()

        return TypeAdapter(List[DomainToolModel]).dump_python(results)  # type: ignore
