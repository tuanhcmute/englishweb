from typing import List, Any, Optional
from datetime import datetime

from sqlalchemy.exc import ProgrammingError, IntegrityError
from sqlmodel import SQLModel, Session, select, update, delete, and_, func

from kguru.services.storages.storages_config import BaseConfig
from kguru.domains.exceptions import ValidationException, BussinessException

from sqlalchemy.orm import subqueryload, Query

from typing import TypeVar, Type

T = TypeVar("T", bound=SQLModel)


class CRUD:
    storage_driver: BaseConfig
    schema_type: Type[T]  # type: ignore

    def __init__(self, storage_driver: BaseConfig, schema_type: T) -> None:
        self.storage_driver = storage_driver
        self.schema_type = schema_type

    async def create_new_object(self, base_object_model: T) -> T:
        try:
            if not base_object_model:
                raise ValidationException.require("base_object_model")

            if not bool(base_object_model):
                raise ValidationException.empty("base_object_model")

            session: Session = self.storage_driver.session()
            with session as ss:
                ss.add(base_object_model)
                ss.commit()
                return await self.read_object(id=base_object_model.id)  # type: ignore
        except (ProgrammingError, IntegrityError) as err:
            raise BussinessException(err.orig.args[0]["M"])  # type: ignore

    async def create_new_objects(self, base_object_models: List[T]):
        try:
            session: Session = self.storage_driver.session()
            with session as ss:
                for base_object_model in base_object_models:
                    ss.add(base_object_model)
                ss.commit()
        except (ProgrammingError, IntegrityError) as err:
            raise BussinessException(err.orig.args[0]["M"])  # type: ignore

    async def read_object(self, id: str) -> T:
        if not id.strip():
            raise ValidationException.empty("id")

        session: Session = self.storage_driver.session()
        with session as ss:
            statement = (
                select(self.schema_type)
                .where(self.schema_type.id == id)
                .options(subqueryload("*"))
            )
            query = ss.exec(statement)
            result = query.first()
            if not result:
                raise BussinessException(
                    f"Can not find {self.schema_type.__name__} with id: {id}"
                )
            return result  # type: ignore

    async def update_object(self, id: str, base_object_model: T) -> T:
        if not base_object_model:
            raise ValidationException.require("base_object_model")

        session: Session = self.storage_driver.session()
        if not await self.read_object(id=id):
            raise BussinessException(f"Can not find {self.schema_type.__name__}")
        try:
            with session as ss:
                statement = update(self.schema_type).where(self.schema_type.id == id)
                del base_object_model.__dict__["id"]
                ss.exec(statement=statement, params=base_object_model.__dict__)  # type: ignore
                ss.commit()
                return await self.read_object(id=id)  # type: ignore
        except ProgrammingError as err:
            raise BussinessException(
                f"Error when update {self.schema_type.__name__}: {err.orig.args[0]['M']}"  # type: ignore
            )

    async def delete_object(self, id: str):
        if not id.strip():
            raise ValidationException.empty("id")

        session: Session = self.storage_driver.session()
        with session as ss:
            statement = delete(self.schema_type).where(self.schema_type.id == id)  # type: ignore
            ss.exec(statement)  # type: ignore
            ss.commit()

    async def get_all(self) -> List[T]:
        sql_models: List[T] = []
        session: Session = self.storage_driver.session()
        with session as ss:
            statement = select(self.schema_type).options(subqueryload("*"))  # type: ignore
            results = ss.exec(statement)  # type: ignore
            for result in results:  # type: ignore
                sql_models.append(result)  # type: ignore

            if not bool(sql_models):
                raise BussinessException(f"Can not find {type(self.schema_type).__name__}")  # type: ignore

            return sql_models

    async def get_list_object_by_specific_attributes_offset_limit(
        self,
        offset: int,
        limit: int,
        order_by: Optional[List[str]] = None,
        **kwargs: Any,
    ) -> List[T]:
        return (
            self._get_list_object_by_specific_attributes(**kwargs)
            .order_by(*order_by)  # type: ignore
            .offset(offset)
            .limit(limit)
            .all()
        )

    async def get_list_object_by_specific_attributes(self, **kwargs: Any) -> List[T]:
        return self._get_list_object_by_specific_attributes(**kwargs).all()

    async def get_object_by_specific_attributes(self, **kwargs: Any) -> T:
        return self._get_list_object_by_specific_attributes(**kwargs).one()

    def _get_list_object_by_specific_attributes(self, **kwargs: Any) -> Query[Any]:
        session: Session = self.storage_driver.session()
        with session as ss:
            return ss.query(self.schema_type).filter_by(**kwargs)

    async def get_latest_created_date(self, **kwargs: Any) -> datetime:
        session: Session = self.storage_driver.session()
        with session as ss:
            statement = select(func.max(self.schema_type.created_at)).filter_by(
                **kwargs
            )
            query = ss.exec(statement)
            result = query.first()
            return result
