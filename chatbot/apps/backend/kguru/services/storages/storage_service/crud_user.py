from typing import Any

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import User

from kguru.domains.models.domain_model import DomainUserModel
from kguru.services.storages.data_mapper import UserDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDUserObject(CRUDObject[User]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=User)

    async def create_new_object(
        self, base_object_model: DomainUserModel
    ) -> DomainUserModel:
        user: User = UserDataMapper.to_sqlmodel(model=base_object_model)
        result: User = await self.crud.create_new_object(base_object_model=user)
        return UserDataMapper.to_domain_model(model=result)

    async def read_object_by_specific_attribute(self, **kwargs: Any) -> DomainUserModel:
        result: User = await self.crud.get_object_by_specific_attributes(**kwargs)
        return UserDataMapper.to_domain_model(model=result)
