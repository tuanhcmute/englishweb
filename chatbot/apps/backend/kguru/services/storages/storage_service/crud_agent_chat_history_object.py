from typing import Any, List, Optional
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import AgentChatHistory
from kguru.domains.models.domain_model import DomainAgentChatHistoryModel
from kguru.services.storages.data_mapper import AgentChatHistoryDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDAgentChatHistoryObject(CRUDObject[DomainAgentChatHistoryModel]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=AgentChatHistory)

    async def create_new_object(
        self, base_object_model: DomainAgentChatHistoryModel
    ) -> DomainAgentChatHistoryModel:
        agent_chat_history_model: AgentChatHistory = (
            AgentChatHistoryDataMapper.to_sqlmodel(model=base_object_model)
        )
        result: AgentChatHistory = await self.crud.create_new_object(
            base_object_model=agent_chat_history_model
        )
        return AgentChatHistoryDataMapper.to_domain_model(model=result)

    async def get_objects_by_specific_foreign_key(
        self, **kwargs: Any
    ) -> List[DomainAgentChatHistoryModel]:
        results: List[AgentChatHistory] = (
            await self.crud.get_list_object_by_specific_attributes(**kwargs)
        )
        return TypeAdapter(List[DomainAgentChatHistoryModel]).dump_python(
            TypeAdapter(List[AgentChatHistory]).dump_python(results)
        )

    async def get_objects_by_specific_foreign_key_offset_limit(
        self, offset: int, limit: int, order_by: Optional[List] = None, **kwargs: Any
    ) -> List[DomainAgentChatHistoryModel]:
        results: List[AgentChatHistory] = (
            await self.crud.get_list_object_by_specific_attributes_offset_limit(
                offset=offset, limit=limit, order_by=order_by, **kwargs
            )
        )
        return TypeAdapter(List[DomainAgentChatHistoryModel]).dump_python(
            TypeAdapter(List[AgentChatHistory]).dump_python(results)
        )
