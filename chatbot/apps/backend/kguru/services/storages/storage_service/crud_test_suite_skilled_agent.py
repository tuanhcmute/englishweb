from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import TestSuiteSkilledAgent
from kguru.domains.models.domain_model import DomainTestSuiteSkilledAgent
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDTestSuiteAndSkilledAgentObject(CRUDObject[DomainTestSuiteSkilledAgent]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(
            storage_driver=storage_driver, schema_type=TestSuiteSkilledAgent
        )

    async def create_new_objects(
        self, base_object_models: List[DomainTestSuiteSkilledAgent]
    ):
        test_suite_skilled_agents: List[TestSuiteSkilledAgent] = TypeAdapter(List[TestSuiteSkilledAgent]).dump_python(base_object_models)  # type: ignore
        await self.crud.create_new_objects(base_object_models=test_suite_skilled_agents)
