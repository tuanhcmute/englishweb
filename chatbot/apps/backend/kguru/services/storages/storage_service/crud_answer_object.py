from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import Answer
from kguru.domains.models.domain_model import DomainAnswerModel
from kguru.services.storages.data_mapper import AnswerDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDAnswerObject(CRUDObject[DomainAnswerModel]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=Answer)

    async def create_new_object(
        self, base_object_model: DomainAnswerModel
    ) -> DomainAnswerModel:
        answer_model: Answer = AnswerDataMapper.to_sqlmodel(model=base_object_model)
        result: Answer = await self.crud.create_new_object(
            base_object_model=answer_model
        )
        return AnswerDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainAnswerModel:
        result: Answer = await self.crud.read_object(id=id)
        return AnswerDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainAnswerModel
    ) -> DomainAnswerModel:
        answer_model: Answer = AnswerDataMapper.to_sqlmodel(model=base_object_model)
        result: Answer = await self.crud.update_object(
            id=id, base_object_model=answer_model
        )
        return AnswerDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainAnswerModel]:
        results: List[Answer] = await self.crud.get_all()

        return TypeAdapter(List[DomainAnswerModel]).validate_python(
            TypeAdapter(List[Answer]).dump_python(results)
        )
