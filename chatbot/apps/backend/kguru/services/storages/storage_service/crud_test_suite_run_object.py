from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import TestSuiteRun
from kguru.domains.models.domain_model import DomainTestSuiteRunModel
from kguru.services.storages.data_mapper import TestSuiteRunDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDTestSuiteRunObject(CRUDObject[DomainTestSuiteRunModel]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=TestSuiteRun)

    async def create_new_object(
        self, base_object_model: DomainTestSuiteRunModel
    ) -> DomainTestSuiteRunModel:
        test_suite_run: TestSuiteRun = TestSuiteRunDataMapper.to_sqlmodel(
            model=base_object_model
        )
        result: TestSuiteRun = await self.crud.create_new_object(
            base_object_model=test_suite_run
        )
        return TestSuiteRunDataMapper.to_domain_model(model=result)

    async def create_new_objects(
        self, base_object_models: List[DomainTestSuiteRunModel]
    ):
        test_suite_runs: List[TestSuiteRun] = TypeAdapter(
            List[TestSuiteRun]
        ).validate_python(
            TypeAdapter(List[DomainTestSuiteRunModel]).dump_python(base_object_models)
        )
        await self.crud.create_new_objects(base_object_models=test_suite_runs)

    async def read_object(self, id: str) -> DomainTestSuiteRunModel:
        result: TestSuiteRun = await self.crud.read_object(id=id)
        return TestSuiteRunDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainTestSuiteRunModel
    ) -> DomainTestSuiteRunModel:
        test_suite_run: TestSuiteRun = TestSuiteRunDataMapper.to_sqlmodel(
            model=base_object_model
        )
        result: TestSuiteRun = await self.crud.update_object(
            id=id, base_object_model=test_suite_run
        )
        return TestSuiteRunDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainTestSuiteRunModel]:
        results: List[TestSuiteRun] = await self.crud.get_all()

        return [
            TestSuiteRunDataMapper.to_domain_model(model=result) for result in results
        ]
