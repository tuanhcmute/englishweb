from typing import List
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import LLMModel
from kguru.domains.models.domain_model import DomainLLMModel
from kguru.services.storages.data_mapper import LLMModelDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDLLMModelObject(CRUDObject[DomainLLMModel]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=LLMModel)

    async def create_new_object(
        self, base_object_model: DomainLLMModel
    ) -> DomainLLMModel:
        llm_model: LLMModel = LLMModelDataMapper.to_sqlmodel(model=base_object_model)
        result: LLMModel = await self.crud.create_new_object(
            base_object_model=llm_model
        )
        return LLMModelDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainLLMModel:
        result: LLMModel = await self.crud.read_object(id=id)
        return LLMModelDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainLLMModel
    ) -> DomainLLMModel:
        llm_model: LLMModel = LLMModelDataMapper.to_sqlmodel(model=base_object_model)
        result: LLMModel = await self.crud.update_object(
            id=id, base_object_model=llm_model
        )
        return LLMModelDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainLLMModel]:
        results: List[LLMModel] = await self.crud.get_all()
        return TypeAdapter(List[DomainLLMModel]).validate_python(
            TypeAdapter(List[LLMModel]).dump_python(results)
        )
