from typing import List, Any
from pydantic import TypeAdapter
from datetime import datetime

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import TestRunResult
from kguru.domains.models.domain_model import DomainTestRunResultModel
from kguru.services.storages.data_mapper import TestRunResultDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDTestRunResultObject(CRUDObject[DomainTestRunResultModel]):
    crud: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=TestRunResult)

    async def create_new_object(
        self, base_object_model: DomainTestRunResultModel
    ) -> DomainTestRunResultModel:
        test_run_result: TestRunResult = TestRunResultDataMapper.to_sqlmodel(
            model=base_object_model
        )
        result: TestRunResult = await self.crud.create_new_object(
            base_object_model=test_run_result
        )
        return TestRunResultDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainTestRunResultModel:
        result: TestRunResult = await self.crud.read_object(id=id)
        return TestRunResultDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainTestRunResultModel
    ) -> DomainTestRunResultModel:
        test_run_result: TestRunResult = TestRunResultDataMapper.to_sqlmodel(
            model=base_object_model
        )
        result: TestRunResult = await self.crud.update_object(
            id=id, base_object_model=test_run_result
        )
        return TestRunResultDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainTestRunResultModel]:
        results: List[TestRunResult] = await self.crud.get_all()

        return [TestRunResultDataMapper.to_domain_model(result) for result in results]

    async def get_objects_by_specific_foreign_key(
        self, **kwargs: Any
    ) -> List[DomainTestRunResultModel]:
        created_at: datetime = await self.crud.get_latest_created_date(**kwargs)
        results: List[TestRunResult] = (
            await self.crud.get_list_object_by_specific_attributes(
                **kwargs, created_at=created_at
            )
        )

        return [TestRunResultDataMapper.to_domain_model(result) for result in results]
