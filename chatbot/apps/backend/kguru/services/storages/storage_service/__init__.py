from .crud_object import CRUDObject
from .crud_question_object import CRUDQuestionObject
from .crud_skilled_agent import CRUDSkilledAgentObject
from .crud_test_run_result_object import CRUDTestRunResultObject
from .crud_test_suite_object import CRUDTestSuiteObject
from .crud_llm_model import CRUDLLMModelObject
from .crud_tool import CRUDToolObject
from .crud_skilled_agent_version import CRUDSkilledAgentVersionObject
from .crud_test_suite_run_object import CRUDTestSuiteRunObject
from .crud_answer_object import CRUDAnswerObject
from .crud_agent_chat_history_object import CRUDAgentChatHistoryObject
from .crud_user import CRUDUserObject
from .crud_run_question import CRUDRunQuestionObject

from kguru.services.storages.storages_config import BaseConfig


class StorageService(CRUDObject):
    crud_question: CRUDQuestionObject
    crud_skilled_agent: CRUDSkilledAgentObject
    crud_test_run_result: CRUDTestRunResultObject
    crud_test_suite: CRUDTestSuiteObject
    crud_llm_model: CRUDLLMModelObject
    crud_tool: CRUDToolObject
    crud_skilled_agent_version: CRUDSkilledAgentVersionObject
    crud_test_suite_run: CRUDTestSuiteRunObject
    crud_user: CRUDUserObject

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud_question = CRUDQuestionObject(storage_driver=storage_driver)
        self.crud_skilled_agent = CRUDSkilledAgentObject(storage_driver=storage_driver)
        self.crud_test_run_result = CRUDTestRunResultObject(
            storage_driver=storage_driver
        )
        self.crud_test_suite = CRUDTestSuiteObject(storage_driver=storage_driver)
        self.crud_llm_model = CRUDLLMModelObject(storage_driver=storage_driver)
        self.crud_tool = CRUDToolObject(storage_driver=storage_driver)
        self.crud_skilled_agent_version = CRUDSkilledAgentVersionObject(
            storage_driver=storage_driver
        )
        self.crud_test_suite_run = CRUDTestSuiteRunObject(storage_driver=storage_driver)
        self.crud_user = CRUDUserObject(storage_driver=storage_driver)
