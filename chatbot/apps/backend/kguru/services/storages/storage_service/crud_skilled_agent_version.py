from typing import List, Any
from pydantic import TypeAdapter

from kguru.services.storages.storages_config.base_config import BaseConfig
from kguru.services.storages.schemas import SkilledAgentVersion, Tool, LLMModel
from kguru.domains.models.domain_model import DomainSkilledAgentVersionModel
from kguru.services.storages.data_mapper import SkilledAgentVersionDataMapper
from .crud_object import CRUDObject
from .crud import CRUD


class CRUDSkilledAgentVersionObject(CRUDObject[DomainSkilledAgentVersionModel]):
    crud: CRUD
    crud_tool: CRUD
    crud_llm_model: CRUD

    def __init__(self, storage_driver: BaseConfig) -> None:
        super().__init__(storage_driver)
        self.crud = CRUD(storage_driver=storage_driver, schema_type=SkilledAgentVersion)
        self.crud_tool = CRUD(storage_driver=storage_driver, schema_type=Tool)
        self.crud_llm_model = CRUD(storage_driver=storage_driver, schema_type=LLMModel)

    async def create_new_object(
        self, base_object_model: DomainSkilledAgentVersionModel
    ) -> DomainSkilledAgentVersionModel:
        skilled_agent_version: SkilledAgentVersion = (
            SkilledAgentVersionDataMapper.to_sqlmodel(model=base_object_model)
        )

        llm_models: List[LLMModel] = []
        for llm_id in base_object_model.llm_model_ids:
            llm_models.append(await self.crud_llm_model.read_object(id=llm_id))

        tools: List[Tool] = []
        for too_id in base_object_model.tool_ids:
            tools.append(await self.crud_tool.read_object(id=too_id))

        skilled_agent_version.tools = tools
        skilled_agent_version.llm_models = llm_models

        result: SkilledAgentVersion = await self.crud.create_new_object(
            base_object_model=skilled_agent_version
        )
        return SkilledAgentVersionDataMapper.to_domain_model(model=result)

    async def read_object(self, id: str) -> DomainSkilledAgentVersionModel:
        result: SkilledAgentVersion = await self.crud.read_object(id=id)
        return SkilledAgentVersionDataMapper.to_domain_model(model=result)

    async def update_object(
        self, id: str, base_object_model: DomainSkilledAgentVersionModel
    ) -> DomainSkilledAgentVersionModel:
        skilled_agent_version: SkilledAgentVersion = (
            SkilledAgentVersionDataMapper.to_sqlmodel(model=base_object_model)
        )
        result: SkilledAgentVersion = await self.crud.update_object(
            id=id, base_object_model=skilled_agent_version
        )
        return SkilledAgentVersionDataMapper.to_domain_model(model=result)

    async def delete_object(self, id: str):
        return await self.crud.delete_object(id=id)

    async def get_all(self) -> List[DomainSkilledAgentVersionModel]:
        results: List[SkilledAgentVersion] = await self.crud.get_all()

        return [
            SkilledAgentVersionDataMapper.to_domain_model(result) for result in results
        ]

    async def get_objects_by_specific_foreign_key(
        self, **kwargs: Any
    ) -> List[DomainSkilledAgentVersionModel]:
        results: List[SkilledAgentVersion] = (
            await self.crud.get_list_object_by_specific_attributes(**kwargs)
        )

        return [
            SkilledAgentVersionDataMapper.to_domain_model(result) for result in results
        ]
