from kguru.domains.models.domain_model import DomainTestSuiteSkilledAgent
from kguru.services.storages.schemas import TestSuiteSkilledAgent


class TestSuiteSkilledAgentDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainTestSuiteSkilledAgent) -> TestSuiteSkilledAgent:
        return TestSuiteSkilledAgent(**model.model_dump())

    @staticmethod
    def to_domain_model(model: TestSuiteSkilledAgent) -> DomainTestSuiteSkilledAgent:
        return DomainTestSuiteSkilledAgent(**model.model_dump())
