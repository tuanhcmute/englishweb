from kguru.domains.models.domain_model import DomainToolModel
from kguru.services.storages.schemas import Tool


class ToolDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainToolModel) -> Tool:
        return Tool(**model.model_dump())

    @staticmethod
    def to_domain_model(model: Tool) -> DomainToolModel:
        return DomainToolModel(**model.model_dump())
