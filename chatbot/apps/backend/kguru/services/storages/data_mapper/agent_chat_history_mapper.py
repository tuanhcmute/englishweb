from kguru.domains.models.domain_model import DomainAgentChatHistoryModel
from kguru.services.storages.schemas import AgentChatHistory


class AgentChatHistoryDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainAgentChatHistoryModel) -> AgentChatHistory:
        return AgentChatHistory(**model.model_dump())

    @staticmethod
    def to_domain_model(model: AgentChatHistory) -> DomainAgentChatHistoryModel:
        return DomainAgentChatHistoryModel(**model.model_dump())
