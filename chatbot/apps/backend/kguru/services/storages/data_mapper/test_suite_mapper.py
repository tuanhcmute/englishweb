from typing import List
from pydantic import TypeAdapter

from kguru.domains.models.domain_model import (
    DomainTestSuiteModel,
    DomainSkilledAgentModel,
    DomainQuestionModel,
    DomainTestSuiteRunModel,
)
from kguru.services.storages.schemas import TestSuite


class TestSuiteDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainTestSuiteModel) -> TestSuite:
        return TestSuite(**model.model_dump())

    @staticmethod
    def to_domain_model(model: TestSuite) -> DomainTestSuiteModel:
        return DomainTestSuiteModel(
            **model.model_dump(),
            skilled_agents=TypeAdapter(List[DomainSkilledAgentModel]).dump_python(
                model.skilled_agents
            ),
            questions=TypeAdapter(List[DomainQuestionModel]).dump_python(
                model.questions
            ),
            test_suite_runs=TypeAdapter(List[DomainTestSuiteRunModel]).dump_python(
                model.test_suite_runs
            )
        )
