from kguru.domains.models.domain_model import DomainLLMModel
from kguru.services.storages.schemas import LLMModel


class LLMModelDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainLLMModel) -> LLMModel:
        return LLMModel(**model.model_dump())

    @staticmethod
    def to_domain_model(model: LLMModel) -> DomainLLMModel:
        return DomainLLMModel(**model.model_dump())
