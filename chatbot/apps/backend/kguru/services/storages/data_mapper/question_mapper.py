from kguru.domains.models.domain_model import DomainQuestionModel
from kguru.services.storages.schemas import Question


class QuestionDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainQuestionModel) -> Question:
        return Question(**model.model_dump())

    @staticmethod
    def to_domain_model(model: Question) -> DomainQuestionModel:
        return DomainQuestionModel(**model.model_dump())
