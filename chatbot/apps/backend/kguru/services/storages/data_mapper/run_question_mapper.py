from kguru.domains.models.domain_model import DomainRunQuestionModel
from kguru.services.storages.schemas import RunQuestion


class RunQuestionDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainRunQuestionModel) -> RunQuestion:
        return RunQuestion(**model.model_dump())

    @staticmethod
    def to_domain_model(model: RunQuestion) -> DomainRunQuestionModel:
        return DomainRunQuestionModel(**model.model_dump())
