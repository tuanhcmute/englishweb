from kguru.domains.models.domain_model import DomainUserModel
from kguru.services.storages.schemas import User


class UserDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainUserModel) -> User:
        return User(**model.model_dump())

    @staticmethod
    def to_domain_model(model: User) -> DomainUserModel:
        return DomainUserModel(**model.model_dump())
