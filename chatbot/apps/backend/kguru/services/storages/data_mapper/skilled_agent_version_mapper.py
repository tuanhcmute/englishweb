from typing import List

from pydantic import TypeAdapter

from kguru.domains.models.domain_model import (
    DomainSkilledAgentVersionModel,
    DomainTestSuiteRunModel,
    DomainToolModel,
    DomainLLMModel,
)
from kguru.services.storages.schemas import SkilledAgentVersion


class SkilledAgentVersionDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainSkilledAgentVersionModel) -> SkilledAgentVersion:
        return SkilledAgentVersion(**model.model_dump())

    @staticmethod
    def to_domain_model(model: SkilledAgentVersion) -> DomainSkilledAgentVersionModel:
        return DomainSkilledAgentVersionModel(
            **model.model_dump(),
            skilled_agent_name=model.skilled_agent.agent_name,  # type: ignore
            test_suite_runs=TypeAdapter(List[DomainTestSuiteRunModel]).dump_python(
                model.test_suite_runs
            ),
            tools=TypeAdapter(List[DomainToolModel]).dump_python(model.tools),
            llm_model_result=TypeAdapter(List[DomainLLMModel]).dump_python(model.llm_models)  # type: ignore
        )
