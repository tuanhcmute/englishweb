from typing import List

from pydantic import TypeAdapter

from kguru.domains.models.domain_model import (
    DomainSkilledAgentModel,
    DomainTestSuiteModel,
)
from kguru.services.storages.schemas import SkilledAgent


class SkilledAgentDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainSkilledAgentModel) -> SkilledAgent:
        return SkilledAgent(**model.model_dump())

    @staticmethod
    def to_domain_model(model: SkilledAgent) -> DomainSkilledAgentModel:
        return DomainSkilledAgentModel(
            **model.model_dump(),
            test_suites=TypeAdapter(List[DomainTestSuiteModel]).dump_python(
                model.test_suites
            ),
        )
