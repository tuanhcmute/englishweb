from kguru.domains.models.domain_model import DomainAnswerModel
from kguru.services.storages.schemas import Answer


class AnswerDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainAnswerModel) -> Answer:
        return Answer(**model.model_dump())

    @staticmethod
    def to_domain_model(model: Answer) -> DomainAnswerModel:
        return DomainAnswerModel(**model.model_dump())
