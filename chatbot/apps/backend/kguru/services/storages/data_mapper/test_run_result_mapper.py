from typing import List
from pydantic import TypeAdapter

from kguru.domains.models.domain_model import (
    DomainTestRunResultModel,
    DomainAnswerModel,
    DomainRunQuestionModel,
)
from kguru.services.storages.schemas import TestRunResult


class TestRunResultDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainTestRunResultModel) -> TestRunResult:
        return TestRunResult(**model.model_dump())

    @staticmethod
    def to_domain_model(model: TestRunResult) -> DomainTestRunResultModel:
        return DomainTestRunResultModel(
            **model.model_dump(),
            run_question_result=TypeAdapter(List[DomainRunQuestionModel]).dump_python(
                model.run_question
            ),
            answer_result=TypeAdapter(List[DomainAnswerModel]).dump_python(
                model.answer
            ),
        )
