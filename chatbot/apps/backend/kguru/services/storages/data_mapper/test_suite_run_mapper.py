from pydantic import TypeAdapter
from typing import List

from kguru.domains.models.domain_model import (
    DomainTestSuiteRunModel,
)
from kguru.services.storages.schemas import TestSuiteRun


class TestSuiteRunDataMapper:
    @staticmethod
    def to_sqlmodel(model: DomainTestSuiteRunModel) -> TestSuiteRun:
        return TestSuiteRun(**model.model_dump())

    @staticmethod
    def to_domain_model(model: TestSuiteRun) -> DomainTestSuiteRunModel:
        return DomainTestSuiteRunModel(**model.model_dump())
