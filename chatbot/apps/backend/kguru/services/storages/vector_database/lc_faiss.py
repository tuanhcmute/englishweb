from langchain.agents.agent import AgentExecutor
from langchain.agents import AgentExecutor, create_openai_tools_agent
from langchain_community.docstore.base import Docstore
from langchain_community.vectorstores.utils import DistanceStrategy
from langchain_core.embeddings import Embeddings
from langchain_openai import ChatOpenAI
from langchain import hub
from langchain.agents import AgentExecutor
from langchain_core.messages import SystemMessage
from typing import Dict, cast, List, Callable, Any
import os
from langchain_core.prompts.chat import (
    ChatPromptTemplate,
    MessagesPlaceholder,
    HumanMessagePromptTemplate,
)
from langchain_core.documents import Document
from langchain_community.vectorstores.faiss import FAISS as LC_FAISS
from langchain_openai import OpenAIEmbeddings
from langchain.tools.retriever import create_retriever_tool
from langchain_core.vectorstores import VectorStoreRetriever as LCVectorStoreRetriever
from langchain.tools import Tool
from langchain_core.embeddings import Embeddings
import logging

LOG = logging.getLogger(__name__)


class FAISS(LC_FAISS):
    def __init__(
        self,
        embedding_function: Callable[[str], List[float]] | Embeddings,
        index: hub.Any,  # type: ignore
        docstore: Docstore,
        index_to_docstore_id: Dict[int, str],
        relevance_score_fn: Callable[[float], float] | None = None,
        normalize_L2: bool = False,
        distance_strategy: DistanceStrategy = DistanceStrategy.EUCLIDEAN_DISTANCE,
    ):
        super().__init__(
            embedding_function,
            index,
            docstore,
            index_to_docstore_id,
            relevance_score_fn,
            normalize_L2,
            distance_strategy,
        )

    @classmethod
    def build_local_database(
        cls, documents: List[Document], data_path: str, embeddings: Embeddings
    ):
        db = cls.from_documents(documents, embeddings)
        db.save_local(data_path)

    @classmethod
    def from_built_local(
        cls, data_path: str, embeddings: Embeddings, **kwargs
    ) -> "LC_FAISS":
        return cls.load_local(data_path, embeddings, **kwargs)
