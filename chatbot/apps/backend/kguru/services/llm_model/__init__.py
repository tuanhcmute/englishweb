import importlib
import importlib.util
import inspect
import logging
import os
from typing import Any

from kguru.utils import ClassLoader

from .base import BaseChatModel, BaseChatModelWrapper, ChatOpenAIWrapper, ChatGoogleGenerativeAIWrapper


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


BASE_CLASS_DEFINITION = (BaseChatModel, BaseChatModelWrapper, ChatOpenAIWrapper, ChatGoogleGenerativeAIWrapper)

class LLMModelLoader(ClassLoader):
    """
    llm_modelLoader class is responsible for loading llm_models from a folder. Same the way Airflow loads operators from a folder.
    
    Args:
        llm_model_folder (str): Path to the folder containing the llm_models, encouraged to be an absolute path.
    """

    def __init__(self, class_folder: str) -> None:
        super().__init__(class_folder, BASE_CLASS_DEFINITION, class_key_name="llm_model_name")

    def _class_name_definition(self, obj: Any) -> str:
        return obj.__fields__.get('name').default
