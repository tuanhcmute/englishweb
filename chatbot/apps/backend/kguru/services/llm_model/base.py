from langchain.chat_models.base import BaseChatModel
from langchain_core.language_models.base import Field
from langchain_openai.chat_models import ChatOpenAI
from langchain_openai import ChatOpenAI

from langchain_google_genai import ChatGoogleGenerativeAI


class ChatOpenAIWrapper(ChatOpenAI):
    """
    Wrapper for ChatOpenAI class, to be used as a base class for LLM models.
    Provide some additional attributes.
    """

    kguru_is_llm_model: bool = True
    name: str = Field(
        default=None,
        title="Name",
        description="Name of the model",
        example="GPT 3.5 Turbo",
    )


class ChatGoogleGenerativeAIWrapper(ChatGoogleGenerativeAI):
    """
    Wrapper for ChatGoogleGenerativeAI class, to be used as a base class for LLM models.
    Provide some additional attributes.
    """

    kguru_is_llm_model: bool = True
    name: str = Field(
        default=None,
        title="Name",
        description="Name of the model",
        example="Gemini Pro",
    )


class BaseChatModelWrapper(BaseChatModel):
    """
    Wrapper for BaseChatModel class, to be used as a base class for LLM models.
    Provide some additional attributes.
    """

    kguru_is_llm_model: bool = True
    name: str = Field(
        default=None, title="Name", description="Name of the model", example="Mistral"
    )
