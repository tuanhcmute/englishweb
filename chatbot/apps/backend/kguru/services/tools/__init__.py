from typing import Any

from kguru.utils import ClassLoader

from kguru.domains.models.tool import BaseTool


BASE_CLASS_DEFINITION = (BaseTool,)


class ToolLoader(ClassLoader):
    """
    ToolLoader class is responsible for loading tools from a folder. Same the way Airflow loads operators from a folder.

    Args:
        tool_folder (str): Path to the folder containing the tools, encouraged to be an absolute path.
    """

    def __init__(self, class_folder: str) -> None:
        super().__init__(
            class_folder, BASE_CLASS_DEFINITION, class_key_name="tool_name"
        )

    def _class_name_definition(self, obj: Any) -> str:
        if name := getattr(obj, "name", None):
            return name
