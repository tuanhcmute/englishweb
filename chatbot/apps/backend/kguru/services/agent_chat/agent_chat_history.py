import json
import logging
from datetime import datetime
from typing import Any, List

from sqlalchemy import desc, asc

from kguru.domains.models.domain_model import DomainAgentChatHistoryModel
from kguru.services.storages.storage_service import (
    CRUDAgentChatHistoryObject,
    CRUDLLMModelObject,
    CRUDSkilledAgentObject,
    CRUDSkilledAgentVersionObject,
    CRUDToolObject,
)
from kguru.services.storages.storages_config import BaseConfig
from kguru.domains.models import SkilledAgent
from kguru.services.tools import ToolLoader
from kguru.services.llm_model import LLMModelLoader
from kguru import config, utils


logger = logging.getLogger(__name__)


class AgentChatHistoryService:
    crud: CRUDAgentChatHistoryObject

    def __init__(self, storage_driver: BaseConfig) -> None:
        self.storage_driver = storage_driver
        self.crud_agent_chat = CRUDAgentChatHistoryObject(storage_driver=storage_driver)
        self.crud_llm_model = CRUDLLMModelObject(storage_driver=storage_driver)
        self.crud_skilled_agent = CRUDSkilledAgentObject(storage_driver=storage_driver)
        self.crud_skilled_agent_version = CRUDSkilledAgentVersionObject(
            storage_driver=storage_driver
        )
        self.crud_tool = CRUDToolObject(storage_driver=storage_driver)

        self.llm_model_loader = LLMModelLoader(config.LLM_MODEL_PATH)
        self.tool_loader = ToolLoader(config.TOOL_PATH)

        self.llm_model_loader.load_classes()
        self.tool_loader.load_classes()

    async def create_new_chat_history(
        self, model: DomainAgentChatHistoryModel
    ) -> DomainAgentChatHistoryModel:
        """
        Create a new chat history record of a user, then put user's message for agent to respond.
        Finally, save the agent's response to the chat history and return the chat history.
        """
        model.created_at = datetime.now()
        user_msg = await self.crud_agent_chat.create_new_object(base_object_model=model)
        skilled_agent = await self.construct_skilled_agent(
            user_msg.skilled_agent_version_id, user_msg.user_id
        )
        try:
            logger.info('===========================here=================', skilled_agent.run)
            agent_response = await skilled_agent.run(user_msg.message)
        except Exception as e:
            logger.error(e, exc_info=True)
            # agent_response = str(e)
            agent_response = "Encounter some error. Please contact for support."

        agent_msg = DomainAgentChatHistoryModel(
            user_id=user_msg.user_id,
            is_kguru_msg=True,
            created_at=datetime.now(),
            skilled_agent_version_id=user_msg.skilled_agent_version_id,
            message=agent_response,
        )
        return await self.crud_agent_chat.create_new_object(base_object_model=agent_msg)

    async def get_chat_histories_by_user_and_skilled_agent(
        self, offset: int = 0, limit: int = 20, **kwargs: Any
    ) -> List[DomainAgentChatHistoryModel]:
        results: List[DomainAgentChatHistoryModel] = (
            await self.crud_agent_chat.get_objects_by_specific_foreign_key_offset_limit(
                offset=offset, limit=limit, order_by=[desc("created_at")], **kwargs
            )
        )
        results.reverse()
        return results

    @utils.alru_cache(maxsize=128, typed=True)
    async def construct_skilled_agent(
        self, skilled_agent_version_id: str, user_id: str
    ) -> SkilledAgent:
        """
        Construct a skilled agent from a skilled agent version id and a user id.
        """
        logger.info(
            f"Constructing skilled agent for user {user_id} with skilled agent version {skilled_agent_version_id}"
        )
        agent_version = await self.crud_skilled_agent_version.read_object(
            id=skilled_agent_version_id
        )

        domain_llm_model = next(iter(agent_version.llm_model_result))

        LLMClass = self.llm_model_loader.get_class(domain_llm_model.llm_model_name)

        tools_definition: List[dict] = [
            (self.tool_loader.get_class(tool.tool_name), tool.param)
            for tool in agent_version.tools
        ]

        llm_model = LLMClass()

        tools = []
        for ToolClass, param in tools_definition:
            logger.info(f"Constructing tool {ToolClass} with param: {param}")
            tools.append(ToolClass(json.loads(param)).build())

        return SkilledAgent(
            setup_message=agent_version.setup_message,
            model=llm_model,
            tools=tools,
        )
