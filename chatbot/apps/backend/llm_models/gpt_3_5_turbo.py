

from kguru.services.llm_model.base import ChatOpenAIWrapper


class GPT35Turbo(ChatOpenAIWrapper):
    """
    Chat model using GPT-3.5-turbo model.
    """
    name: str = "GPT 3.5 Turbo"

    def __init__(self, **kwargs) -> None:
        super().__init__(model_name="gpt-3.5-turbo", **kwargs)

