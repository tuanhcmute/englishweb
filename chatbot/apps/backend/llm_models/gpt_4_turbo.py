

from kguru.services.llm_model.base import ChatOpenAIWrapper


class GPT4Turbo(ChatOpenAIWrapper):
    """
    Chat model using GPT-4-turbo model.
    """
    name: str = "GPT 4 Turbo"

    def __init__(self, **kwargs) -> None:
        super().__init__(model_name="gpt-4-turbo", **kwargs)

