
from kguru.services.llm_model.base import ChatGoogleGenerativeAIWrapper


class Gemini15Pro(ChatGoogleGenerativeAIWrapper):
    """
    Wrapper for ChatGoogleGenerativeAI class, to be used as a base class for LLM models.
    Provide some additional attributes.
    """
    name: str = "Gemini 1.5 Pro"

    def __init__(self, *args, **kwargs):
        super().__init__(model="gemini-pro", *args, **kwargs)