import sys

import anyio

import dagger


async def main():
    config = dagger.Config(log_output=sys.stdout)

    async with dagger.Connection(config) as client:
        context_dir = client.host().directory(".")
        source = (
            client.container()
            .from_("python:3.10")
            .with_directory(
                "/src",
                context_dir,
            )
            .with_workdir("/src")
            .with_mounted_cache("/root/.cache/pip", client.cache_volume("python-310"))
        )
        runner = source.with_exec(
            ["pip", "install", "--upgrade", "pip", "-r", "requirements.txt"]
        )

        # Check code type
        await runner.with_exec(["mypy", "."]).stderr()

        # Test step
        await runner.with_exec(["python3", "-m" "pytest"]).stderr()


anyio.run(main)
