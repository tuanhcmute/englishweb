from kguru.domains.models.tool import BaseTool, LCTool, ToolExecutionContextResult
from typing import Dict, Type, Union, Any, Optional, List, Tuple
from langchain_core.pydantic_v1 import BaseModel, Field
import logging
import json
import requests
from sqlalchemy.engine import Engine
from sqlalchemy import create_engine
import pandas as pd
from sqlalchemy import text

logger = logging.getLogger(__name__)


class CubeQueryMetricsToolConstructParam(BaseModel):
    cube_api_url: str = Field(description="URL of cube API")
    cube_api_secret: str = Field(description="Secret of cube API")
    database_connection_string: str = Field(description="SQLalchemy connection string")


class QueryFilter(BaseModel):
    member: str
    operator: str = Field(
        description="operator include only: equals | notEquals | inDateRange | notInDateRange | afterDate | afterOrOnDate | beforeDate | beforeOrOnDate"
    )
    values: List[str]


class TimeDimensionFilter(BaseModel):
    dimension: str
    from_date: str
    to_date: str


class QueryToolInput(BaseModel):
    measure_names: Optional[List[str]] = Field(default_factory=lambda: [])
    dimension_names: Optional[List[str]] = Field(
        description="Don't include Dim Date in dimension when it already have in time_dimensions",
        default_factory=lambda: [],
    )
    query_filters: Optional[List[QueryFilter]] = Field(default=None)
    time_dimensions: Optional[List[TimeDimensionFilter]] = Field(default=None)


class CubeQueryMetricsToolExecutionContextResult(ToolExecutionContextResult):
    sql: str
    params: Dict[str, Any]
    df: pd.DataFrame


class CubeQueryMetricsTool(BaseTool):
    name: str = "CubeQueryMetricsTool"
    description: str = """
Input to this tool is metrics and dimensions related to user question in JSON format. Example {"measure_names":["fact_pmo.amm"], "dimension_names":["fact_pmo.dim_date"],"time_dimensions":[{"dimension":"dim_date.date","from_date":"2024-03-01","to_date":"2024-04-30"}],"query_filters":[{"member":"fact_pmo.ee","operator":"gte","values":["0.9"]}] }
Output is the result of query """

    def __init__(self, construct_param: Dict) -> None:
        assert construct_param != None
        self.construct_param = CubeQueryMetricsToolConstructParam(**construct_param)
        self.engine: Engine = create_engine(
            self.construct_param.database_connection_string
        )

    async def run(
        self,
        measure_names: Optional[List[str]],
        dimension_names: Optional[List[str]],
        query_filters: Optional[List[QueryFilter]] = None,
        time_dimensions: Optional[List[TimeDimensionFilter]] = None,
        context: Optional[Dict[str, Any]] = None,
    ) -> str:

        logger.info(
            f"""execute CubeQueryMetricsTool with param:
        measure_names: {measure_names}
        dimension_names: {dimension_names}
        query_filters: {query_filters}
        time_dimensions: {time_dimensions}
        """
        )
        try:
            return await self._run(
                measure_names, dimension_names, query_filters, time_dimensions, context
            )
        except requests.ConnectionError as e:
            logger.error(e, exc_info=True)
            return "ConnectionError cannot retrieve data"
        except Exception as e:
            logger.error(e, exc_info=True)
            return f"Encounter error {e.__class__} {str(e)}. You can retry with different parameter."

    async def _run(
        self,
        measure_names: Optional[List[str]],
        dimension_names: Optional[List[str]],
        query_filters: Optional[List[QueryFilter]] = None,
        time_dimensions: Optional[List[TimeDimensionFilter]] = None,
        context: Optional[Dict[str, Any]] = None,
    ) -> str:
        query_object: Dict[str, Any] = {
            "measures": measure_names,
            "dimensions": dimension_names,
        }
        query_object["filters"] = self._format_query_filter(query_filters)
        query_object["timeDimensions"] = self._format_time_dimensions(time_dimensions)
        sql, params = self._get_sql_and_param(query_object)
        with self.engine.connect() as conn:
            df = pd.read_sql(sql=text(sql), con=conn, params=params)
            print(df)
        self.set_execution_result_to_context(
            CubeQueryMetricsToolExecutionContextResult(sql=sql, params=params, df=df),
            context,
        )
        return df.to_markdown(index=False, floatfmt=f".2f")

    def _get_sql_and_param(
        self, query_object: Dict[str, Any]
    ) -> Tuple[str, Dict[str, Any]]:
        """
        Call cube api to get sql and parameter.
        The parameters would be binded with ? to support sqlalchemy generic usecases
        :raises:
            requests.exceptions.RequestException: if encorunted error when sending request to cubejs
        """
        logger.info(f"query_object: {query_object}")
        params = {"query": json.dumps(query_object)}

        url = f"{self.construct_param.cube_api_url}/sql"
        payload = {}
        headers = {
            "Accept": "*/*",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Pragma": "no-cache",
            "authorization": self.construct_param.cube_api_secret,
        }
        response = requests.request(
            "GET", url, headers=headers, data=payload, timeout=10, params=params
        )
        response_object = json.loads(response.text)
        sql: str = response_object["sql"]["sql"][0]
        parameters = response_object["sql"]["sql"][1]
        converted_parameter = {}
        i = 1
        while True:
            try:
                if sql.index("?"):
                    sql = sql.replace("?", f":param_{i}", 1)
                    converted_parameter[f"param_{i}"] = parameters[i - 1]
                    i += 1
            except ValueError as e:
                break

        return (sql, converted_parameter)

    def _format_query_filter(
        self,
        query_filters: Optional[List[QueryFilter]],
    ) -> list[dict[str, Any]]:
        if query_filters != None:
            return [
                {"member": a.member, "operator": a.operator, "values": a.values}
                for a in list(filter(lambda x: (x.member != None), query_filters))
            ]

        return []

    def _format_time_dimensions(
        self, time_dimensions: Optional[List[TimeDimensionFilter]]
    ) -> list[dict[str, Any]]:
        if time_dimensions != None:
            return [
                {
                    "dimension": a.dimension,
                    "granularity": "day",
                    "dateRange": [a.from_date, a.to_date],
                }
                for a in list(filter(lambda x: (x.dimension != None), time_dimensions))
            ]

        return []

    def build(self) -> LCTool:
        """
        Build AsyncTool object for the database executor tool.
        """
        return LCTool(
            name=self.__class__.__name__,
            description=self.description,
            func=None,
            coroutine=self.run,
            args_schema=QueryToolInput,
            handle_tool_error=True,
            handle_validation_error=True,
        )

    @classmethod
    def get_tool_param(cls) -> Type[BaseModel]:
        return CubeQueryMetricsToolConstructParam
