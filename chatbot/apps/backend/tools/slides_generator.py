import logging
from typing import Any, Dict, Type, List, Optional

from odin_slides.presentation import create_presentation
from langchain_core.pydantic_v1 import BaseModel, Field

from kguru.domains.models.tool import BaseTool, LCTool

LOG = logging.getLogger(__name__)


class SlidesGeneratorToolConstructParam(BaseModel):
    template_file: str = Field(
        description="Path to the template file for the slides generator tool."
    )
    output_file: str = Field(
        description="Path to the output file for the slides generator tool."
    )
    config: Dict[str, Any] = Field(
        default_factory=lambda x: {},
        description="Configuration for the slides generator tool.",
    )


class SlidesContent(BaseModel):
    """Content of a slide."""

    title: str = Field(description="Title of the slide.")
    content: str = Field(description="Content of the slide.")
    narration: str = Field(description="Narration of the slide.")
    page: Optional[int] = Field(default=None, description="Page number of the slide.")


class SlidesGeneratorInput(BaseModel):
    """Input to the slides generator tool."""

    slides: List[SlidesContent] = Field(description="List of slides to generate.")


class SlidesGeneratorTool(BaseTool):
    name = "Slides Generator"
    description = "Slides generator tool to generate slides from the given content."

    def __init__(self, construct_param: Dict = {}) -> None:
        self.construct_param = SlidesGeneratorToolConstructParam(**construct_param)

    async def run(self, slides: List[SlidesContent]) -> None:
        """
        Generate slides from the given content.
        """
        try:
            create_presentation(
                template_file=self.construct_param.template_file,
                output_file=self.construct_param.output_file,
                slide_content=slides,
                logger=LOG,
            )
        except Exception as e:
            LOG.error(f"An unexpected error occurred: {e}")
            raise e

    def build(self) -> LCTool:
        """
        Build Slide Generator tool.
        """
        return LCTool(
            name=self.__class__.__name__,
            description=self.description,
            func=None,
            coroutine=self.run,
            args_schema=SlidesGeneratorInput,
        )

    @classmethod
    def get_tool_param(cls) -> Type[BaseModel]:
        return SlidesGeneratorToolConstructParam
