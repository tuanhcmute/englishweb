import logging
from pathlib import Path
from typing import Type, Dict

from kguru.services.storages.vector_database.lc_faiss import FAISS
from langchain_core.prompts import PromptTemplate, format_document
from langchain_core.pydantic_v1 import BaseModel, Field
from langchain_openai import OpenAIEmbeddings

from kguru.domains.models.tool import BaseTool, LCTool

LOG = logging.getLogger(__name__)


class RetrieverToolConstructParam(BaseModel):
    k: int
    name: str
    description: str


class RetrieverInput(BaseModel):
    """Input to the retriever."""

    query: str = Field(description="user question to look up in retriever")


class RetrieverTool(BaseTool):
    name = "Retriever"
    description = """Given a comprehensive question from a user, analyze and extract the relevant metrics and aspects pertinent to the inquiry."""

    def __init__(self, construct_param: Dict = {}) -> None:
        self.cwd = Path().cwd()
        self.construct_param = RetrieverToolConstructParam(**construct_param)
        embeddings = OpenAIEmbeddings(model="text-embedding-3-small", timeout=10)
        self.retriever = FAISS.from_built_local(
            self.cwd.joinpath("vector_db/faiss"),
            embeddings,
            allow_dangerous_deserialization=True,
        )
        self.document_prompt_template = PromptTemplate.from_template(
            "Document: {page_content}, file_name: {file_name}"
        )
        self.document_separator = "\n\n"

    async def run(self, query: str) -> str:
        LOG.info(f"query {query}")
        docs = await self.retriever.asimilarity_search(query, k=self.construct_param.k)
        return self.document_separator.join(
            format_document(doc, self.document_prompt_template) for doc in docs
        )

    def build(self) -> LCTool:
        return LCTool(
            name=self.name,
            description=self.description,
            func=None,
            coroutine=self.run,
            args_schema=RetrieverInput,
        )

    @classmethod
    def get_tool_param(cls) -> Type[BaseModel]:
        return RetrieverToolConstructParam
