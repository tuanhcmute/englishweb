from kguru.domains.models.tool import BaseTool, LCTool, ToolInputSchemaRegistry
from langchain_core.pydantic_v1 import BaseModel, Field
from typing import Dict, Type, List, Any, Optional
import weaviate
from weaviate.auth import AuthApiKey
from langchain_weaviate.vectorstores import WeaviateVectorStore
import logging
import asyncio
from langchain_community.embeddings.openai import OpenAIEmbeddings
from langchain_core.prompts import format_document, PromptTemplate
from langchain_core.documents import Document
from weaviate.classes.query import Filter
import copy

logger = logging.getLogger()


def remove_duplicated_and_format_documents(docs: List[Document]) -> List[Document]:
    seen = set()
    result = []
    for doc in docs:
        t = doc.page_content
        if t not in seen:
            seen.add(t)
            result.append(doc)
    return result


class WeaviateDimensionRetrieverToolInput(BaseModel):
    measures: List[str] = Field(description="List of measure related to user question")


class WeaviateDimensionRetrieverToolConstructParam(BaseModel):
    k: int
    http_host: str
    http_port: int
    http_secure: bool
    grpc_host: str
    grpc_port: int
    grpc_secure: bool
    auth_credentials: str
    index_name: str
    tool_description: str
    document_prompt: str
    filter: Optional[Dict[str, Any]]
    embeding_model: str = Field(default="text-embedding-ada-002")
    name: str = Field(default="WeaviateRetrieverTool")


class WeaviateDimensionRetrieverTool(BaseTool):
    name = "WeaviateDimensionRetrieverTool"
    description = ""

    def __init__(self, construct_param: Dict) -> None:
        self.construct_param = WeaviateDimensionRetrieverToolConstructParam(
            **construct_param
        )
        self.weaviate_client = weaviate.connect_to_custom(
            http_host=self.construct_param.http_host,
            http_port=self.construct_param.http_port,
            http_secure=self.construct_param.http_secure,
            grpc_host=self.construct_param.grpc_host,
            grpc_port=self.construct_param.grpc_port,
            grpc_secure=self.construct_param.grpc_secure,
            auth_credentials=AuthApiKey(self.construct_param.auth_credentials),
        )
        self.vector_store = WeaviateVectorStore(
            client=self.weaviate_client,
            index_name=self.construct_param.index_name,
            text_key="text",
            embedding=OpenAIEmbeddings(model=self.construct_param.embeding_model),
        )
        self.description = self.construct_param.tool_description
        self.name = self.construct_param.name
        self.document_prompt = PromptTemplate.from_template(
            template=self.construct_param.document_prompt
        )
        self.construct_filter()

    def construct_filter(self):
        """
        Construct weaviate filter object using filter property in construct param
        """
        if self.construct_param.filter == None:
            return None

        filters = None
        for filter_key in self.construct_param.filter:
            if filters == None:
                filters = Filter.by_property(filter_key).equal(
                    self.construct_param.filter[filter_key]
                )
            else:
                filters = filters & Filter.by_property(filter_key).equal(
                    self.construct_param.filter[filter_key]
                )
        self.filters = filters

    async def run(self, context: Optional[Dict[str, Any]], measures: List[str]) -> str:
        logger.info(f"""WeaviateDimensionRetrieverTool execute with: {measures}""")
        queries_task = []

        ### Add measure filter in constructed filter ###
        for measure in measures:
            if self.filters != None:
                filters = copy.deepcopy(self.filters) & Filter.by_property(
                    "measure"
                ).equal(measure)
            else:
                filters = Filter.by_property("measure").equal(measure)
            queries_task.append(
                self.vector_store.asimilarity_search(
                    measure,
                    k=self.construct_param.k,
                    filters=filters,
                )
            )
        result = await asyncio.gather(*queries_task)

        ### BEST FLATMAP IMPLEMENTATION ###
        docs = []
        for x in result:
            docs.extend(x)

        for doc in docs:
            logger.info(doc)

        return "\n\n".join(
            format_document(doc, self.document_prompt)
            for doc in remove_duplicated_and_format_documents(docs)
        )

    def build(self) -> LCTool:
        return LCTool(
            name=self.name,
            description=self.description,
            func=None,
            coroutine=self.run,
            args_schema=WeaviateDimensionRetrieverToolInput,
            handle_tool_error=True,
            handle_validation_error=True,
        )

    @classmethod
    def get_tool_param(cls) -> Type[BaseModel]:
        return WeaviateDimensionRetrieverToolConstructParam
