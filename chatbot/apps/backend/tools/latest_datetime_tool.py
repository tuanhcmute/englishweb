from kguru.domains.models.tool import BaseTool, LCTool, ToolInputSchemaRegistry
import logging
from typing import Optional, Dict, Type
from datetime import datetime
from langchain_core.pydantic_v1 import BaseModel, Field

logger = logging.getLogger()


class LatestDateTimeToolConstructParam(BaseModel):
    pass


class LatestDateTimeToolInput(BaseModel):
    pass


class LatestDateTimeTool(BaseTool):
    name: str = "LatestDateTimeTool"
    description: str = """
Don't need to input anything
Output is the latest date time
"""

    def __init__(self, construct_param: Optional[Dict] = None) -> None:
        pass

    async def run(self, **kwargs) -> str:
        logger.info(f"""LatestDateTimeTool execute with: {kwargs}""")
        year = datetime.today().strftime("%Y")
        month = datetime.today().strftime("%m")
        date = datetime.today().strftime("%d")
        return f"Current Date: year: {year} | month: {month} | date: {date}"

    def build(self) -> LCTool:
        return LCTool(
            name=self.name,
            description=self.description,
            func=None,
            coroutine=self.run,
            args_schema=LatestDateTimeToolConstructParam,
            handle_tool_error=True,
            handle_validation_error=True,
        )

    @classmethod
    def get_tool_param(cls) -> Type[BaseModel]:
        return LatestDateTimeToolConstructParam
