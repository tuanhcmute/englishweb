from kguru.domains.models.tool import (
    BaseTool,
    LCTool,
    ToolExecutionContextResult,
)
import logging
from typing import List, Optional, Dict, Type, Union, Any, Literal
from langchain_core.pydantic_v1 import BaseModel, Field
from tools.cube_query_metrics_tool import CubeQueryMetricsToolExecutionContextResult
import os
import uuid

logger = logging.getLogger()


class VisualizeCubeQueryResultToolConstructParam(BaseModel):
    pass


class VisualizeCubeQueryResultToolInput(BaseModel):
    kind: Literal[
        "line",
        "bar",
        "barh",
        "hist",
        "box",
        "kde",
        "density",
        "area",
        "pie",
        "scatter",
        "hexbin",
    ] = Field(description="kind of chart including: bar, line, pie, area")
    column: Optional[Union[str, List[str]]] = Field(
        description="column or list of columns to visualize"
    )


class VisualizeCubeQueryResultToolExecutionContextResult(ToolExecutionContextResult):
    chart_url: str


class VisualizeCubeQueryResultTool(BaseTool):
    name: str = "VisualizeCubeQueryResultTool"
    description: str = """
Input is config of function pandas plot.
Output is location of file chart
"""

    def __init__(self, construct_param: Optional[Dict] = None) -> None:
        pass

    async def run(
        self,
        kind: Literal[
            "line",
            "bar",
            "barh",
            "hist",
            "box",
            "kde",
            "density",
            "area",
            "pie",
            "scatter",
            "hexbin",
        ],
        column: Optional[Union[str, List[str]]],
        context: Dict[str, Any],
    ) -> str:
        logger.info(
            f"""VisualizeCubeQueryResultTool execute with: 
                    kind: {kind}
                    column: {column}"""
        )
        latest_cube_query_result = self.get_latest_execution_result(
            context=context, result_class=CubeQueryMetricsToolExecutionContextResult
        )
        if latest_cube_query_result == None:
            return "There is no result of CubeQueryMetricsTool. CubeQueryMetricsTool must be executed first."
        df = latest_cube_query_result.df  # type: ignore
        if df.empty:
            raise Exception(f"There is no data to visualize in {self.name}")
        figure = df.plot(kind=kind, column=column).get_figure()
        file_name = "chart_visualize_" + uuid.uuid4().hex
        figure.savefig(f"{os.environ['STATIC_RESOURCE_PATH']}/{file_name}.png")  # type: ignore
        chart_url = f"{os.environ['STATIC_RESOURCE_URL']}/{file_name}.png"
        self.set_execution_result_to_context(
            VisualizeCubeQueryResultToolExecutionContextResult(chart_url=chart_url),
            context,
        )
        return chart_url

    def build(self) -> LCTool:
        return LCTool(
            name=self.name,
            description=self.description,
            func=None,
            coroutine=self.run,
            args_schema=VisualizeCubeQueryResultToolInput,
            handle_tool_error=True,
            handle_validation_error=True,
        )

    @classmethod
    def get_tool_param(cls) -> Type[BaseModel]:
        return VisualizeCubeQueryResultToolConstructParam
