import logging
from typing import Any, Dict, Type, Optional

import pandas as pd
from langchain_core.pydantic_v1 import BaseModel, Field
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.sql import text


from kguru.domains.models.tool import BaseTool, LCTool

LOG = logging.getLogger(__name__)


class DBExecutorToolConstructParam(BaseModel):
    connection: str = Field(
        description="Connection string to the database. Follows the SQLAlchemy format."
    )
    config: Dict[str, Any] = Field(
        default_factory=lambda x: {},
        description="Configuration for the database engine.",
    )


class DBExecutorInput(BaseModel):
    """Input to the database engine executor."""

    query: str = Field(description="SQL query to execute")


class DBExecutorTool(BaseTool):
    name = "Database Executor"
    description = "Database executor tool to execute SQL queries on a database."

    def __init__(self, construct_param: Dict = {}) -> None:
        self.construct_param = DBExecutorToolConstructParam(**construct_param)
        self.engine = create_async_engine(
            self.construct_param.connection, **self.construct_param.config
        )

    async def run(self, query: str, context: Optional[Dict[str, Any]] = None) -> str:
        """
        Execute the SQL query on the database and return the result in markdown format.
        The markdown format is included column names as headers and the data as rows.

        Args:
            query (str): SQL query to execute

        Returns:
            str: Markdown formatted string of the query result
        """

        async with self.engine.connect() as conn:
            result = await conn.execute(text(query))
            data = pd.DataFrame(
                data=result.fetchall(), columns=result.keys()
            ).to_markdown()
            LOG.debug(f"Query executed: \n{query} \nResult: \n{data}")
            return data

    def build(self) -> LCTool:
        """
        Build AsyncTool object for the database executor tool.
        """
        return LCTool(
            name=self.__class__.__name__,
            description=self.description,
            func=None,
            coroutine=self.run,
            args_schema=DBExecutorInput,
        )

    @classmethod
    def get_tool_param(cls) -> Type[BaseModel]:
        return DBExecutorToolConstructParam
