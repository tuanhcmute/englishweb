package com.englishweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableRedisRepositories
public class EnglishWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnglishWebApplication.class, args);
    }

}
