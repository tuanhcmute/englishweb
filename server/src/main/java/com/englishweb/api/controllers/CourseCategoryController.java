package com.englishweb.api.controllers;

import com.englishweb.api.dto.CourseCategoryDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateCourseCategoryRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CourseCategoryService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course-category")
@Tag(name = "Course category APIs")
@RequiredArgsConstructor
public class CourseCategoryController {
    private final CourseCategoryService courseCategoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<CourseCategoryDTO>> getAllCourseCategory() {
        return courseCategoryService.getAllCourseCategory();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<CourseCategoryDTO> getCourseCategoryById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return courseCategoryService.getCourseCategoryById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteCourseCategory(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return courseCategoryService.deleteCourseCategory(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createCourseCategory(@RequestBody CreateCourseCategoryRequest request) {
        return courseCategoryService.createCourseCategory(request);
    }
}