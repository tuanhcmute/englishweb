package com.englishweb.api.controllers;

import com.englishweb.api.dto.StaticPageDTO;
import com.englishweb.api.request.CreatePageStaticRequest;
import com.englishweb.api.request.UpdatePageStaticRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.StaticPageService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/static-page")
@Tag(name = "Static APIs")
public class StaticPageController {

    private final StaticPageService staticPageService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<StaticPageDTO>> getAllStaticPages() {
        return staticPageService.getAllStaticPages();
    }

    @GetMapping("/staticPage")
    @ResponseStatus(HttpStatus.OK)
    public Response<StaticPageDTO> getStaticPage(@RequestParam(value = "id", required = false) String id,
                                                 @RequestParam(value = "pageCode", required = false) String pageCode) {
        return staticPageService.getStaticPage(id, pageCode);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public Response<Void> createStaticPage(@RequestBody CreatePageStaticRequest request) {
        return staticPageService.createStaticPage(request);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateStaticPage(@RequestBody UpdatePageStaticRequest request) {
        return staticPageService.updateStaticPage(request);
    }

}
