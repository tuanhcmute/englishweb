package com.englishweb.api.controllers;

import com.englishweb.api.dto.UserCredentialDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.UpdateUserInfoRequest;
import com.englishweb.api.request.UpdateUserRoleRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.UserCredentialService;
import com.englishweb.api.utils.UserInfoUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user-credential")
@Tag(name = "User credential APIs")
@RequiredArgsConstructor
public class UserCredentialController {
    private final UserCredentialService userCredentialService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<UserCredentialDTO>> getAllUserCredentials(@RequestParam Map<String, String> params) {
        return userCredentialService.getAllUserCredential(params);
    }

    @Validated
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public Response<Void> deleteUserCredential(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return userCredentialService.deleteUserCredential(id);
    }

    @GetMapping("/credential")
    @ResponseStatus(HttpStatus.OK)
    public Response<UserCredentialDTO> getUserCredential(
            @RequestParam(value = "id", required = false) String id,
            @RequestParam(value = "username", required = false) String username) {
        return userCredentialService.getUserCredential(id, username);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateUserCredential(@RequestBody UpdateUserInfoRequest userInfoRequest) {
        UserInfoUtils.userInfoValdate(userInfoRequest);
        return userCredentialService.updateUserInfo(userInfoRequest);
    }

    @PutMapping("/{id}/role")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public Response<Void> updateUserRole(@PathVariable String id, @RequestBody UpdateUserRoleRequest requestData) {
        return userCredentialService.updateUserRole(id, requestData);
    }

}
