package com.englishweb.api.controllers;

import com.englishweb.api.dto.FullTestAnswerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.FullTestAnswer;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.FullTestAnswerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/full-test-answer")
@Tag(name = "Full test answer APIs")
@RequiredArgsConstructor
public class FullTestAnswerController {
    private final FullTestAnswerService fullTestAnswerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<FullTestAnswer>> getAllTestAnswer() {
        return fullTestAnswerService.getAllTestAnswer();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<FullTestAnswer> getFullTestAnswerById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return fullTestAnswerService.getFullTestAnswerById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public Response<Void> deleteFullTestAnswer(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return fullTestAnswerService.deleteFullTestAnswer(id);
    }

    @GetMapping("/byTest")
    @ResponseStatus(HttpStatus.OK)
    public Response<FullTestAnswerDTO> getFullTestAnswerByTest(@RequestParam("value") String value) {
        if (value.isEmpty() || value.isBlank())
            throw new BadRequestException("Test id is null");
        return fullTestAnswerService.getFullTestAnswerByTest(value);
    }
}
