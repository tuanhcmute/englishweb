package com.englishweb.api.controllers;

import com.englishweb.api.dto.PaymentDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreatePayment;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.PaymentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/payment")
@Tag(name = "Payment APIs")
@RequiredArgsConstructor
@Slf4j
public class PaymentController {
    private final PaymentService paymentService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<PaymentDTO>> getAllPayment() {
        return paymentService.getAllPayment();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<PaymentDTO> getPaymentById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return paymentService.getPaymentById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deletePaymentById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return paymentService.deletePaymentById(id);
    }

    @GetMapping("/redirect")
    public void createPayment(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam("courseId") String courseId,
            @RequestParam("userCredentialId") String userCredentialId,
            @RequestParam("totalAmount") String totalAmount,
            @RequestParam("paymentMethod") String paymentMethod,
            @RequestParam("successUrl") String successUrl,
            @RequestParam("failureUrl") String failureUrl,
            @RequestParam("baseUrl") String baseUrl
    ) {
        log.info("{ courseId: {} }", courseId);
        log.info("{ userCredentialId: {} }", userCredentialId);
        log.info("{ totalAmount: {} }", totalAmount);
        log.info("{ paymentMethod: {} }", paymentMethod);
        log.info("{ Base url: {} }", baseUrl);
        log.info("{ Success url: {} }", successUrl);
        log.info("{ Failure url: {} }", failureUrl);
        log.info("{ RequestUrl: {} }", request.getRequestURL().toString());
        try {
            CreatePayment createPayment = CreatePayment.builder()
                    .courseId(courseId)
                    .userCredentialId(userCredentialId)
                    .paymentMethod(paymentMethod)
                    .totalAmount(Integer.parseInt(totalAmount))
                    .successUrl(successUrl)
                    .failureUrl(failureUrl)
                    .baseUrl(baseUrl)
                    .build();
            log.info("{ Remote IP address: {} }", request.getRemoteAddr());
            log.info("{ Real IP address: {} }", request.getHeader("X-Real-IP"));
            log.info("{ X-FORWARDED-FOR: {} }", request.getHeader("X-FORWARDED-FOR"));
            paymentService.createPayment(createPayment, request, response);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

}
