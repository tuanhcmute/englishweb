package com.englishweb.api.controllers;

import com.englishweb.api.dto.TestAnswerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.TestAnswerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test-answer")
@Tag(name = "Test answer APIs")
@RequiredArgsConstructor
public class TestAnswerController {
    private final TestAnswerService testAnswerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<TestAnswerDTO>> getAllTestAnswer() {
        return testAnswerService.getAllTestAnswer();
    }

    @GetMapping("/{id}")
    @Validated
    @ResponseStatus(HttpStatus.OK)
    public Response<TestAnswerDTO> getTestAnswerById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return testAnswerService.getTestAnswerById(id);
    }

    @Validated
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> deleteTestAnswer(@PathVariable String id) {
        return testAnswerService.deleteTestAnswer(id);
    }
}

