package com.englishweb.api.controllers;

import com.englishweb.api.dto.DiscountConditionDTO;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.DiscountConditionService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/discount-condition")
@Tag(name = "Discount condition APIs")
@RequiredArgsConstructor
@Slf4j
public class DiscountConditionController {
    private final DiscountConditionService discountConditionService;
//    private final MessageSource messageSource;


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<DiscountConditionDTO>> getAllDiscountConditions(@RequestParam Map<String, String> params) {
        return this.discountConditionService.getAllDiscountConditions(params);
    }
}
