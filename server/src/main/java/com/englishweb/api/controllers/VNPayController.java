package com.englishweb.api.controllers;

import com.englishweb.api.services.interfaces.VNPayService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/vnpay")
@Tag(name = "VNPay APIs")
public class VNPayController {
    private final VNPayService vnPayService;

    @GetMapping
    public void returnVNPay(HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam("courseId") String courseId,
                            @RequestParam("userCredentialId") String userCredentialId
//                            @RequestParam("successUrl") String successUrl,
//                            @RequestParam("failureUrl") String failureUrl
    ) {
        log.info("{ courseId: {} }", courseId);
        log.info("{ userCredentialId: {} }", userCredentialId);
//        log.info("{ Success url: {} }", successUrl);
//        log.info("{ Failure url: {} }", failureUrl);
        vnPayService.returnVNPay(request, response, courseId, userCredentialId);
    }
}
