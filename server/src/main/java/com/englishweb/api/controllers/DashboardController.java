package com.englishweb.api.controllers;

import com.englishweb.api.dto.DashboardDTO;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.DashboardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/dashboard")
public class DashboardController {

    private final DashboardService dashboardService;

    @GetMapping
    public Response<DashboardDTO> getDashboard() {
        return dashboardService.getDashboard();
    }
}
