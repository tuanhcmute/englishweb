package com.englishweb.api.controllers;

import com.englishweb.api.dto.FullPartInformationDTO;
import com.englishweb.api.dto.PartInformationDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreatePartInformation;
import com.englishweb.api.request.UpdatePartInformation;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.PartInformationService;
import com.englishweb.api.utils.PartInformationUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/part-information")
@Tag(name = "Part information APIs")
@RequiredArgsConstructor
@Slf4j
public class PartInformationController {
    private final PartInformationService partInformationService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<FullPartInformationDTO>> getAllPartInformation() {
        return partInformationService.getAllPartInformation();
    }

    @GetMapping("/byTestCategory")
    @ResponseStatus(HttpStatus.OK)
    public Response<List<FullPartInformationDTO>> getAllPartInformationByTestCategory(@RequestParam("value") String value) {
        log.info("{value: {}}", value);
        return partInformationService.getAllPartInformationByTestCategory(value);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<PartInformationDTO> getPartInformationById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return partInformationService.getPartInformationById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deletePartInformation(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return partInformationService.deletePartInformation(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createNewPartInformation(@RequestBody CreatePartInformation partInformation) {
        PartInformationUtils.partInformationValidate(partInformation);
        return partInformationService.createNewPartInformation(partInformation);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updatePartInformation(@RequestBody UpdatePartInformation partInformation) {
        return partInformationService.updatePartInformation(partInformation);
    }
}

