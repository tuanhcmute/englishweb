package com.englishweb.api.controllers;

import com.englishweb.api.dto.UserAnswerDTO;
import com.englishweb.api.request.CreateNewUserAnswer;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.UserAnswerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/user-answer")
@Tag(name = "User answer APIs")
public class UserAnswerController {

    private final UserAnswerService userAnswerService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createUserAnswer(@RequestBody CreateNewUserAnswer request) {
        return userAnswerService.createNewUserAnswer(request);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<UserAnswerDTO> getUserAnswerById(@PathVariable String id) {
        return userAnswerService.getUserAnswerById(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<UserAnswerDTO>> getAllUserAnswers(@RequestParam Map<String, String> params) {
        return userAnswerService.getAllUserAnswers(params);
    }

    @GetMapping("/count/byTest")
    @ResponseStatus(HttpStatus.OK)
    public Response<Long> countByTest(@RequestParam("value") String value) {
        return userAnswerService.countByTest(value);
    }
}
