package com.englishweb.api.controllers;

import com.englishweb.api.dto.InvoiceLineDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewInvoiceLineRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.InvoiceLineService;
import com.englishweb.api.utils.InvoiceLineUtils;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/invoice-line")
@Tag(name = "Invoice line APIs")
@RequiredArgsConstructor
public class InvoiceLineController {
    private final InvoiceLineService invoiceLineService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<InvoiceLineDTO>> getAllInvoiceLine() {
        return invoiceLineService.getAllInvoiceLine();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<InvoiceLineDTO> getInvoiceLineById(@PathVariable String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null");
        return invoiceLineService.getInvoiceLineById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteInvoiceLine(@PathVariable String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null");
        return invoiceLineService.deleteInvoiceLine(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createNewInvoice(@RequestBody CreateNewInvoiceLineRequest createNewInvoiceLineRequest) {
        InvoiceLineUtils.validateInvoiceLine(createNewInvoiceLineRequest);
        return invoiceLineService.createNewInvoiceLine(createNewInvoiceLineRequest);
    }
}
