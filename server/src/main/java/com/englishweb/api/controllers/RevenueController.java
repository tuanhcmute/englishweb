package com.englishweb.api.controllers;

import com.englishweb.api.dto.RevenueDTO;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.RevenueService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/revenue")
@Tag(name = "Auth APIs")
@RequiredArgsConstructor
public class RevenueController {

    private final RevenueService revenueService;

    @GetMapping
    public Response<List<RevenueDTO>> getAllRevenues(@RequestParam Map<String, String> params) {
        return revenueService.getAllRevenues(params);
    }

}
