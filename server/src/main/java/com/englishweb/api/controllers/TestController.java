package com.englishweb.api.controllers;

import com.englishweb.api.dto.FullTestDTO;
import com.englishweb.api.dto.TestDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateTestRequest;
import com.englishweb.api.request.UpdateTestRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.TestService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/test")
@Tag(name = "Test APIs")
@RequiredArgsConstructor
public class TestController {
    private final TestService testService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<TestDTO>> getAllTest(@RequestParam Map<String, String> params) {
        return testService.getAllTest(params);
    }

    @Validated
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<TestDTO> getTestById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return testService.getTestById(id);
    }

    @Validated
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> deleteTest(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return testService.deleteTest(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createTest(@Valid @RequestBody CreateTestRequest requestData) {
        return testService.createNewTest(requestData);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateTest(@Valid @RequestBody UpdateTestRequest requestData) {
        return testService.updateTest(requestData);
    }

    @Validated
    @GetMapping("/{id}/practice")
    @ResponseStatus(HttpStatus.OK)
    public Response<FullTestDTO> getFullTestById(@PathVariable String id) {
        return testService.getFullTestById(id);
    }
}
