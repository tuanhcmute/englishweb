package com.englishweb.api.controllers;

import com.englishweb.api.dto.PartOfTestDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.PartOfTest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.PartOfTestService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/part-of-test")
@Tag(name = "Part of test APIs")
@RequiredArgsConstructor
public class PartOfTestController {
    private final PartOfTestService partOfTestService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<PartOfTest>> getAllPartOfTest() {
        return partOfTestService.getAllPartOfTest();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<PartOfTest> getPartOfTestById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return partOfTestService.getPartOfTestById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public Response<Void> deletePartOfTest(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return partOfTestService.deletePartOfTest(id);
    }

    @GetMapping("/byTest")
    @ResponseStatus(HttpStatus.OK)
    public Response<List<PartOfTestDTO>> getAllPartOfTestByTest(@RequestParam("value") String value) {
        return partOfTestService.getAllPartOfTestByTest(value);
    }

    @PutMapping("/sync/byTest")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updatePartOfTestByTest(@RequestParam("value") String value) {
        return partOfTestService.updatePartOfTestByTest(value);
    }
}
