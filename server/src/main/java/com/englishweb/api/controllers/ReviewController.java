package com.englishweb.api.controllers;

import com.englishweb.api.dto.ReviewDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateReviewRequest;
import com.englishweb.api.request.UpdateReviewRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.ReviewService;
import com.englishweb.api.utils.ReviewUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/review")
@Tag(name = "Review APIs")
@RequiredArgsConstructor
public class ReviewController {
    private final ReviewService reviewService;

    /**
     *
     * @param params courseId?:str, isPublic?:str, page?:str, limit?:str
     * @return List of reviews filter by isPublic and courseId.
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<ReviewDTO>> getAllReviews(@RequestParam Map<String, String> params) {
        return reviewService.getAllReviews(params);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteReviewById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return reviewService.deleteReviewById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createReview(@RequestBody CreateReviewRequest createReviewRequest) {
        ReviewUtils.reviewValidate(createReviewRequest);
        return reviewService.createReview(createReviewRequest);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateReview(@RequestBody UpdateReviewRequest requestData) {
        return reviewService.updateReview(requestData);
    }
}
