package com.englishweb.api.controllers;

import com.englishweb.api.dto.FullNewsDTO;
import com.englishweb.api.dto.NewsDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewsRequest;
import com.englishweb.api.request.UpdateNewsRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.NewsService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/news")
@Tag(name = "News APIs")
@RequiredArgsConstructor
public class NewsController {
    private final NewsService newsService;

    /**
     *
     * @param params:
     *              - page: str
     *              - limit: str
     *              - categoryId: str
     * @return list of news. Sort by last modified date. Filter categoryId.
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<NewsDTO>> getAllNews(@RequestParam Map<String, String> params) {
        return newsService.getAllNews(params);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createNews(@RequestBody CreateNewsRequest requestData) {
        return newsService.createNews(requestData);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateNews(@RequestBody UpdateNewsRequest requestData) {
        return newsService.updateNews(requestData);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<FullNewsDTO> getNewsById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return newsService.getNewsById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteNews(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return newsService.deleteNews(id);
    }
}
