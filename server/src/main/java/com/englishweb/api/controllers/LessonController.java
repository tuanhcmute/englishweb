package com.englishweb.api.controllers;


import com.englishweb.api.dto.LessonDTO;
import com.englishweb.api.request.CreateLessonRequest;
import com.englishweb.api.request.UpdateLessonRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.LessonService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lesson")
@Tag(name = "Lesson APIs")
public class LessonController {
    private final LessonService lessonService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createLesson(@RequestBody CreateLessonRequest requestData) {
        return lessonService.createNewLesson(requestData);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<LessonDTO> getLessonById(@PathVariable String id) {
        return lessonService.getLessonById(id);
    }

    @PostMapping("/upload")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> uploadVideoLesson(
            @RequestParam("id") String id,
            @RequestParam("file") MultipartFile file) {
        return lessonService.uploadVideoLesson(id, file);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateLesson(@RequestBody UpdateLessonRequest requestData) {
        return lessonService.updateLesson(requestData);
    }
}
