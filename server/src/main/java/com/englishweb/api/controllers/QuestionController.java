package com.englishweb.api.controllers;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.Question;
import com.englishweb.api.request.QuestionRequest;
import com.englishweb.api.request.UpdateQuestionRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuestionService;
import com.englishweb.api.utils.QuestionUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/question")
@Tag(name = "Question APIs")
@RequiredArgsConstructor
public class QuestionController {
    private final QuestionService questionService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<Question>> getAllQuestion() {
        return questionService.getAllQuestion();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Question> getQuestionById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return questionService.getQuestionById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> deleteQuestion(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return questionService.deleteQuestion(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createNewQuestion(@RequestBody QuestionRequest questionRequest) {
        QuestionUtils.questionValidate(questionRequest);
        return questionService.createNewQuestion(questionRequest);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateQuestion(@RequestBody UpdateQuestionRequest updateQuestionRequest) {
        return questionService.updateQuestion(updateQuestionRequest);
    }
}
