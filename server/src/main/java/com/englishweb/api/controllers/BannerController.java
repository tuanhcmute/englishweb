package com.englishweb.api.controllers;

import com.englishweb.api.dto.BannerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateBannerItemRequest;
import com.englishweb.api.request.UpdateBannerRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.BannerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/banner")
@Tag(name = "Banner item APIs")
@RequiredArgsConstructor
public class BannerController {
    private final BannerService bannerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<BannerDTO>> getAllBanners(@RequestParam Map<String, String> params) {
        return bannerService.getAllBanners(params);
    }

    @Validated
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<BannerDTO> getBannerById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return bannerService.getBannerById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteBannerById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return bannerService.deleteBannerById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Response<Void> createBanner(@Valid @RequestBody CreateBannerItemRequest requestData) {
        return bannerService.createBanner(requestData);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Response<Void> updateBanner(@Valid @RequestBody UpdateBannerRequest requestData) {
        return bannerService.updateBanner(requestData);
    }
}
