package com.englishweb.api.controllers;

import com.englishweb.api.dto.FlashcardDTO;
import com.englishweb.api.dto.FullFlashcardDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateFlashcardRequest;
import com.englishweb.api.request.UpdateFlashcardRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.FlashcardService;
import com.englishweb.api.utils.FlashcardUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/flashcard")
@Tag(name = "Flashcard APIs")
@RequiredArgsConstructor
public class FlashcardController {
    private final FlashcardService flashcardService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<FlashcardDTO>> getAllFlashcard(@RequestParam Map<String, String> params) {
        return flashcardService.getAllFlashcard(params);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<FlashcardDTO> getFlashcardById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return flashcardService.getFlashcardById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteFlashcard(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return flashcardService.deleteFlashcard(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createNewFlashcard(@RequestBody CreateFlashcardRequest createNewFlashcard) {
        FlashcardUtils.flashcardValidate(createNewFlashcard);
        return flashcardService.createNewFlashcard(createNewFlashcard);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateFlashcard(@RequestBody UpdateFlashcardRequest requestData) {
        return flashcardService.updateFlashcard(requestData);
    }
}
