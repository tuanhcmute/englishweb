package com.englishweb.api.controllers;

import com.englishweb.api.dto.CommentDTO;
import com.englishweb.api.request.UpdateCommentRequest;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateCommentRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CommentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/comment")
@Tag(name = "Comment APIs")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService commentService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<CommentDTO>> getAllComments(@RequestParam Map<String, String> params) {
        return commentService.getAllComments(params);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<CommentDTO> getCommentById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return commentService.getCommentById(id);
    }

    @GetMapping("/{id}/sub-comments")
    @ResponseStatus(HttpStatus.OK)
    public Response<List<CommentDTO>> getAllSubComments(@PathVariable("id") String id) {
        return commentService.getAllChildCommentsByParentComment(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteComment(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return commentService.deleteComment(id);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createComment(@RequestBody CreateCommentRequest requestData) {
        return commentService.createComment(requestData);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateComment(@RequestBody UpdateCommentRequest requestData) {
        return commentService.updateComment(requestData);
    }
}
