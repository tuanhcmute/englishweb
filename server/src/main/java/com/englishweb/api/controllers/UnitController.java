package com.englishweb.api.controllers;


import com.englishweb.api.dto.UnitDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateUnitRequest;
import com.englishweb.api.request.UpdateUnitRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.UnitService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/unit")
@Tag(name = "Unit APIs")
public class UnitController {
    private final UnitService unitService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<UnitDTO>> getAllUnits(@RequestParam Map<String, String> params) {
        return unitService.getAllUnits(params);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<UnitDTO> getUnitById(@PathVariable String id) {
        if (id.isEmpty())
            throw new BadRequestException("Id value is empty!");

        return unitService.getUnitById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> deleteUnitById(@PathVariable String id) {
        if (id.isEmpty())
            throw new BadRequestException("Id value is empty!");

        return unitService.deleteUnitById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createNewUnit(@Valid @RequestBody CreateUnitRequest createUnitRequest) {
//        UnitUtils.validateCreatNewUnitRequestModel(createUnitRequest);
        return unitService.createNewUnit(createUnitRequest);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateUnit(@RequestBody UpdateUnitRequest requestData) {
        return unitService.updateUnit(requestData);
    }
}
