package com.englishweb.api.controllers;

import com.englishweb.api.dto.QuizAnswerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuizAnswerService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/quiz-answer")
@Tag(name = "Quiz answer APIs")
@RequiredArgsConstructor
public class QuizAnswerController {
    private final QuizAnswerService quizAnswerService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<QuizAnswerDTO>> getAllQuizAnswer() {
        return quizAnswerService.getAllQuizAnswer();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<QuizAnswerDTO> getQuizAnswerById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return quizAnswerService.getQuizAnswerById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteQuizCategoryById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return quizAnswerService.deleteQuizAnswerById(id);
    }
}
