package com.englishweb.api.controllers;

import com.englishweb.api.dto.ReportCommentDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewReportCommentRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.ReportCommentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/report-comment")
@Tag(name = "Report APIs")
@RequiredArgsConstructor
public class ReportCommentController {
    private final ReportCommentService reportCommentService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<ReportCommentDTO>> getAllReportComments(@RequestParam Map<String, String> params) {
        return reportCommentService.getAllReportComments(params);
    }

    @Validated
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<ReportCommentDTO> getReportCommentById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return reportCommentService.getReportCommentById(id);
    }

    @Validated
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteReportCommentById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        
        return reportCommentService.deleteReportCommentById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createReportComment(@Valid @RequestBody CreateNewReportCommentRequest requestData) {
        return reportCommentService.createReportComment(requestData);
    }
}
