package com.englishweb.api.controllers;

import com.englishweb.api.dto.TestCategoryDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateTestCategory;
import com.englishweb.api.request.UpdateTestCategory;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.TestCategoryService;
import com.englishweb.api.utils.TestCategoryUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test-category")
@Tag(name = "Test category APIs")
@RequiredArgsConstructor
public class TestCategoryController {
    private final TestCategoryService testCategoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<TestCategoryDTO>> getAllTestCategory() {
        return testCategoryService.getAllTestCategory();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<TestCategoryDTO> getTestCategoryById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return testCategoryService.getTestCategoryById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> deleteTestCategory(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return testCategoryService.deleteTestCategory(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createNewTestCategory(@RequestBody CreateTestCategory request) {
        TestCategoryUtils.testCategoryValidate(request);
        return testCategoryService.createNewTestCategory(request);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateTestCategory(@RequestBody UpdateTestCategory request) {
        return testCategoryService.updateTestCategory(request);
    }
}

