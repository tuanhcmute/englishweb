package com.englishweb.api.controllers;

import com.englishweb.api.dto.NewsCategoryDTO;
import com.englishweb.api.request.CreateNewsCategory;
import com.englishweb.api.request.UpdateNewsCategory;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.NewsCategoryService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/news-category")
@Tag(name = "News category APIs")
@RequiredArgsConstructor
public class NewsCategoryController {
    private final NewsCategoryService newsCategoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<NewsCategoryDTO>> getAllNewsCategories(@RequestParam Map<String, String> params) {
        return newsCategoryService.getAllNewsCategories(params);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createNewsCategory(@RequestBody CreateNewsCategory requestData) {
        return newsCategoryService.createNewsCategory(requestData);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateNewsCategory(@RequestBody UpdateNewsCategory requestData) {
        return newsCategoryService.updateNewsCategory(requestData);
    }

    @Validated
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteNewsCategory(@PathVariable String id) {
        return newsCategoryService.deleteNewsCategory(id);
    }
}
