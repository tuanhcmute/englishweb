package com.englishweb.api.controllers;

import com.englishweb.api.dto.CourseDetailDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.UpdateCourseDetailRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CourseDetailService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course-detail")
@Tag(name = "Course detail APIs")
@RequiredArgsConstructor
public class CourseDetailController {
    private final CourseDetailService courseDetailService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<CourseDetailDTO>> getAllCourseDetail() {
        return courseDetailService.getAllCourseDetail();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<CourseDetailDTO> getCourseDetailById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return courseDetailService.getCourseDetailById(id);
    }

    @GetMapping("/byCourse")
    @ResponseStatus(HttpStatus.OK)
    public Response<CourseDetailDTO> getCourseDetailByCourse(@RequestParam("value") String courseId) {
        if (courseId.isEmpty() || courseId.isBlank())
            throw new BadRequestException("Course id is null");
        return courseDetailService.getCourseDetailByCourse(courseId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public Response<Void> deleteCourseDetail(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return courseDetailService.deleteCourseDetailById(id);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateCourseDetail(@RequestBody UpdateCourseDetailRequest request) {
        return courseDetailService.updateCourseDetail(request);
    }
}
