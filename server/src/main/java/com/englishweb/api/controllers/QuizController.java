package com.englishweb.api.controllers;

import com.englishweb.api.dto.QuizDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuizService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/quiz")
@Tag(name = "Quiz APIs")
@RequiredArgsConstructor
public class QuizController {
    private final QuizService quizService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<QuizDTO>> getAllQiz() {
        return quizService.getAllQuiz();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<QuizDTO> getQuizById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return quizService.getQuizById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteQuizById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return quizService.deleteQuizById(id);
    }
}
