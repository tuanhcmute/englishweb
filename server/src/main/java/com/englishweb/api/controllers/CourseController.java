package com.englishweb.api.controllers;

import com.englishweb.api.dto.CourseDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateCourseRequest;
import com.englishweb.api.request.UpdateCourseRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CourseService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/course")
@Tag(name = "Course APIs")
@RequiredArgsConstructor
@Slf4j
public class CourseController {
    private final CourseService courseService;
//    private final MessageSource messageSource;


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<CourseDTO>> getAllCourse(@RequestParam Map<String, String> params) {
        return courseService.getAllCourse(params);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<CourseDTO> getCourseById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return courseService.getCourseById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteCourse(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return courseService.deleteCourse(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createCourse(@RequestBody CreateCourseRequest course) {
        return courseService.createNewCourse(course);
    }

    @GetMapping("/byDiscount")
    @ResponseStatus(HttpStatus.OK)
    public Response<List<CourseDTO>> getAllCoursesByDiscount(@RequestParam("value") String value) {
        return courseService.getAllCoursesByDiscount(value);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateCourse(@RequestBody UpdateCourseRequest course) {
        return courseService.updateCourse(course);
    }

}
