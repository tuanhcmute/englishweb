package com.englishweb.api.controllers;

import com.englishweb.api.dto.FlashcardItemDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateFlashcardItem;
import com.englishweb.api.request.CreateFlashcardItemByKeyword;
import com.englishweb.api.request.UpdateFlashcardItem;
import com.englishweb.api.request.UploadFlashcardItemImageRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.FlashcardItemService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/flashcard-item")
@Tag(name = "Flashcard item APIs")
@RequiredArgsConstructor
public class FlashcardItemController {
    private final FlashcardItemService flashcardItemService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<FlashcardItemDTO>> getAllFlashcardItem(@RequestParam Map<String, String> params) {
        return flashcardItemService.getAllFlashcardItem(params);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<FlashcardItemDTO> getFlashcardItemById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return flashcardItemService.getAllFlashcardItemById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createFlashcardItem(@RequestBody CreateFlashcardItem request) {
        return flashcardItemService.createFlashcardItem(request);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateFlashcardItem(@RequestBody UpdateFlashcardItem request) {
        return flashcardItemService.updateFlashcardItem(request);
    }

    @PutMapping("/{id}/uploadImage")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateFlashcardItemImage(@PathVariable String id, @Valid @RequestBody UploadFlashcardItemImageRequest requestData) {
        return flashcardItemService.updateFlashcardItemImage(id, requestData);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteFlashcardItem(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return flashcardItemService.deleteFlashcardItem(id);
    }

    @PostMapping("/keyword")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createFlashcardItemByKeyword(@RequestBody CreateFlashcardItemByKeyword request) {
        return flashcardItemService.createFlashcardItemByKeyword(request);
    }


}
