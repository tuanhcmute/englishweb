package com.englishweb.api.controllers;

import com.englishweb.api.dto.QuestionGroupDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.QuestionGroup;
import com.englishweb.api.request.QuestionGroupRequest;
import com.englishweb.api.request.UpdateQuestionGroupRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuestionGroupService;
import com.englishweb.api.utils.QuestionGroupUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/question-group")
@Tag(name = "Question group APIs")
@RequiredArgsConstructor
@Slf4j
public class QuestionGroupController {
    private final QuestionGroupService questionGroupService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<QuestionGroupDTO>> getAllQuestionGroup() {
        return questionGroupService.getAllQuestionGroup();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<QuestionGroup> getQuestionGroupById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return questionGroupService.getQuestionGroupById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> deleteQuestionGroupById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return questionGroupService.deleteQuestionGroupById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createNewQuestionGroup(
            @RequestParam("questionContentEn") String questionContentEn,
            @RequestParam("image") String image,
            @RequestParam("questionContentTranscript") String questionContentTranscript,
            @RequestParam("partOfTestId") String partOfTestId) {
        QuestionGroupRequest request = QuestionGroupRequest.builder()
                .questionContentEn(questionContentEn)
                .partOfTestId(partOfTestId)
                .questionContentTranscript(questionContentTranscript)
                .image(image)
                .build();
        QuestionGroupUtils.validate(request);
        return questionGroupService.createNewQuestionGroup(request);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> updateQuestionGroup(@RequestBody UpdateQuestionGroupRequest request) {
        return questionGroupService.updateQuestionGroup(request);
    }

    @PostMapping("/{id}/upload")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> uploadQuestionGroupFile(@PathVariable String id,
                                                  @RequestParam("type") String type,
                                                  @RequestParam("file") MultipartFile file) {
        return questionGroupService.uploadQuestionGroupFile(id, type, file);
    }

    @GetMapping("/byPart")
    @ResponseStatus(HttpStatus.OK)
    public Response<List<QuestionGroupDTO>> getAllQuestionGroupByPart(@RequestParam("value") String value) {
        if (value.isEmpty() || value.isBlank())
            throw new BadRequestException("Part id is null");

        log.info("{ PartId: {} }", value);
        return questionGroupService.getAllQuestionGroupByPart(value);
    }
}
