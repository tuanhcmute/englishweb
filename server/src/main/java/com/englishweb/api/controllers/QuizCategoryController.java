package com.englishweb.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.englishweb.api.dto.QuizCategoryDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuizCategoryService;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/quiz-category")
@Tag(name = "Quiz category APIs")
@RequiredArgsConstructor
public class QuizCategoryController {
    private final QuizCategoryService quizCategoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<QuizCategoryDTO>> getAllQuizCategory() {
        return quizCategoryService.getAllQuizCategory();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<QuizCategoryDTO> getQuizCategoryById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return quizCategoryService.getQuizCategoryById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deletQuizCategoryById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return quizCategoryService.deletQuizCategoryById(id);
    }
}
