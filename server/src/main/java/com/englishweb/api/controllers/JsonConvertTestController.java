package com.englishweb.api.controllers;

import com.englishweb.api.request.UserTestResultRequest;
import com.englishweb.api.response.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/json")
@Tag(name = "Json APIs")
@RequiredArgsConstructor
public class JsonConvertTestController {
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<UserTestResultRequest>> testConvert() throws JsonProcessingException {
        String json = "[{\"partId\":\"34ca1db2-5991-4487-9913-3d157d5dcd7e\",\"questionGroups\":[{\"questionGroupId\":\"6e591a08-afe1-4517-ad8e-aee6de68c50d\",\"questions\":[{\"questionId\":\"1d738a84-8f06-4e01-bb7f-e48159b1e1b3\",\"answerId\":\"2cc946c6-e74d-4967-b754-6a801e20b393\"},{\"questionId\":\"32149ad4-c8eb-45d4-956d-94c2474952d6\",\"answerId\":\"685ae080-7eb5-4643-9379-62ad7124dca7\"},{\"questionId\":\"5d8d55b0-ea86-4f26-b5b7-3ece5dc921ce\",\"answerId\":\"e1652c03-2938-47ae-9331-244884a970bc\"},{\"questionId\":\"b0b1cb6a-811a-4823-ac0d-56cb1cd42d84\",\"answerId\":\"855383ee-4f9c-4726-90a3-1144764fbc00\"},{\"questionId\":\"95edb146-9c46-42c4-a78c-470cf4570db0\",\"answerId\":\"f962f481-e20b-498f-92c9-31651664c27a\"},{\"questionId\":\"82baeb00-1927-4c8f-9acc-dda6caa6d01b\",\"answerId\":\"c7f8b033-ee04-4d1e-aa04-f9fc7908af50\"}]}]},{\"partId\":\"90ceb74a-e506-4c08-a152-8cea92037149\",\"questionGroups\":[{\"questionGroupId\":\"0ba7014f-b280-4233-a2b4-e97ce528608d\",\"questions\":[{\"questionId\":\"e84121f2-1813-45b5-949b-0cc6f7dc0c90\",\"answerId\":\"33b5a954-b73b-4dd7-bb1e-43e30b2dabd5\"}]}]}]";
        ObjectMapper objectMapper = new ObjectMapper();
        List<UserTestResultRequest> questionUserTestRequests = objectMapper.readValue(json, new TypeReference<>() {
        });
        Map<String, List<UserTestResultRequest>> data = new HashMap<>();
        data.put("data", questionUserTestRequests);

        String json2 = "{\"0\":5,\"1\":5,\"2\":5,\"3\":5,\"4\":5,\"5\":5,\"6\":5,\"7\":10,\"8\":15,\"9\":20,\"10\":25,\"11\":30,\"12\":35,\"13\":40,\"14\":45,\"15\":50,\"16\":55,\"17\":60,\"18\":65,\"19\":70,\"20\":75,\"21\":80,\"22\":85,\"23\":90,\"24\":95,\"25\":100,\"26\":110,\"27\":115,\"28\":120,\"29\":125,\"30\":130,\"31\":135,\"32\":140,\"33\":145,\"34\":150,\"35\":160,\"36\":165,\"37\":170,\"38\":175,\"39\":180,\"40\":185,\"41\":190,\"42\":195,\"43\":200,\"44\":210,\"45\":215,\"46\":220,\"47\":230,\"48\":240,\"49\":245,\"50\":250,\"51\":255,\"52\":260,\"53\":270,\"54\":275,\"55\":280,\"56\":290,\"67\":295,\"58\":300,\"59\":310,\"60\":315,\"61\":320,\"62\":325,\"63\":330,\"64\":340,\"65\":345,\"66\":350,\"67\":360,\"68\":365,\"69\":370,\"70\":380,\"71\":385,\"72\":390,\"73\":395,\"74\":400,\"75\":405,\"76\":410,\"77\":420,\"78\":425,\"79\":430,\"80\":440,\"81\":445,\"82\":450,\"83\":460,\"84\":465,\"85\":470,\"86\":475,\"87\":480,\"88\":485,\"89\":490,\"90\":495,\"91\":495,\"92\":495,\"93\":495,\"94\":495,\"95\":495,\"96\":495,\"97\":495,\"98\":495,\"99\":495,\"100\":495}";
        Map<Integer, Integer> data2 = objectMapper.readValue(json2, new TypeReference<>() {
        });
        System.out.println(data2.get(10));

        String json3 = "{\"listening\":{\"0\":5,\"1\":5,\"2\":5,\"3\":5,\"4\":5,\"5\":5,\"6\":5,\"7\":10,\"8\":15,\"9\":20,\"10\":25,\"11\":30,\"12\":35,\"13\":40,\"14\":45,\"15\":50,\"16\":55,\"17\":60,\"18\":65,\"19\":70,\"20\":75,\"21\":80,\"22\":85,\"23\":90,\"24\":95,\"25\":100,\"26\":110,\"27\":115,\"28\":120,\"29\":125,\"30\":130,\"31\":135,\"32\":140,\"33\":145,\"34\":150,\"35\":160,\"36\":165,\"37\":170,\"38\":175,\"39\":180,\"40\":185,\"41\":190,\"42\":195,\"43\":200,\"44\":210,\"45\":215,\"46\":220,\"47\":230,\"48\":240,\"49\":245,\"50\":250,\"51\":255,\"52\":260,\"53\":270,\"54\":275,\"55\":280,\"56\":290,\"67\":295,\"58\":300,\"59\":310,\"60\":315,\"61\":320,\"62\":325,\"63\":330,\"64\":340,\"65\":345,\"66\":350,\"67\":360,\"68\":365,\"69\":370,\"70\":380,\"71\":385,\"72\":390,\"73\":395,\"74\":400,\"75\":405,\"76\":410,\"77\":420,\"78\":425,\"79\":430,\"80\":440,\"81\":445,\"82\":450,\"83\":460,\"84\":465,\"85\":470,\"86\":475,\"87\":480,\"88\":485,\"89\":490,\"90\":495,\"91\":495,\"92\":495,\"93\":495,\"94\":495,\"95\":495,\"96\":495,\"97\":495,\"98\":495,\"99\":495,\"100\":495},\"reading\":{\"0\":5,\"1\":5,\"2\":5,\"3\":5,\"4\":5,\"5\":5,\"6\":5,\"7\":10,\"8\":15,\"9\":20,\"10\":25,\"11\":30,\"12\":35,\"13\":40,\"14\":45,\"15\":50,\"16\":55,\"17\":60,\"18\":65,\"19\":70,\"20\":75,\"21\":80,\"22\":85,\"23\":90,\"24\":95,\"25\":100,\"26\":110,\"27\":115,\"28\":120,\"29\":125,\"30\":130,\"31\":135,\"32\":140,\"33\":145,\"34\":150,\"35\":160,\"36\":165,\"37\":170,\"38\":175,\"39\":180,\"40\":185,\"41\":190,\"42\":195,\"43\":200,\"44\":210,\"45\":215,\"46\":220,\"47\":230,\"48\":240,\"49\":245,\"50\":250,\"51\":220,\"52\":225,\"53\":230,\"54\":235,\"55\":240,\"56\":250,\"57\":255,\"58\":260,\"59\":265,\"60\":270,\"61\":280,\"62\":285,\"63\":290,\"64\":300,\"65\":305,\"66\":310,\"67\":320,\"68\":325,\"69\":330,\"70\":335,\"71\":340,\"72\":350,\"73\":355,\"74\":360,\"75\":365,\"76\":370,\"77\":380,\"78\":385,\"79\":390,\"80\":395,\"81\":400,\"82\":405,\"83\":410,\"84\":415,\"85\":420,\"86\":425,\"87\":430,\"88\":435,\"89\":445,\"90\":450,\"91\":455,\"92\":465,\"93\":470,\"94\":480,\"95\":485,\"96\":490,\"97\":495,\"98\":495,\"99\":495,\"100\":495}}";
        Map<String, Map<Integer, Integer>> data3 = objectMapper.readValue(json3, new TypeReference<>() {
        });
        System.out.println(data3.get("listening").get(0));

        return Response.<List<UserTestResultRequest>>builder()
                .message("Convert string to json object")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

}
