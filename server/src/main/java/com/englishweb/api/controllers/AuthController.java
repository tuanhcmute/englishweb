package com.englishweb.api.controllers;

import com.englishweb.api.dto.AuthDTO;
import com.englishweb.api.request.ForgotPasswordRequest;
import com.englishweb.api.request.SignUpRequest;
import com.englishweb.api.request.UserAuthRequest;
import com.englishweb.api.request.UserSocialAuthRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.AuthService;
import com.englishweb.api.utils.ForgotPasswordUtils;
import com.englishweb.api.utils.SignUpUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@Tag(name = "Auth APIs")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public Response<AuthDTO> login(@RequestBody UserAuthRequest request) {
        return authService.login(request);
    }

    @PostMapping("/signup")
//    @ResponseStatus(HttpStatus.OK)
    public Response<Void> signUp(@RequestBody SignUpRequest signUpRequest) {
        SignUpUtils.signUpValidate(signUpRequest);
        return authService.signUp(signUpRequest);
    }

    @PostMapping("/loginWithSocial")
    @ResponseStatus(HttpStatus.OK)
    public Response<AuthDTO> loginWithSocial(@RequestBody UserSocialAuthRequest request) {
        return authService.loginWithSocial(request);
    }

    @GetMapping("/forgot-password/otp")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> sendOTPForgotPassword(@RequestParam("toEmail") String toEmail) {
        return authService.sendOTPForgotPassword(toEmail);
    }

    @GetMapping("/email/verification/otp")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> sendOTPEmailVerification(@RequestParam("toEmail") String toEmail) {
        return authService.sendOTPEmailVerification(toEmail);
    }

    @PutMapping("/forgot-password/verification")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> forgotPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest) {
        ForgotPasswordUtils.forgotPasswordValidate(forgotPasswordRequest);
        return authService.forgotPassword(forgotPasswordRequest);
    }

    @GetMapping("/email/verification/{hash}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> emailVerification(@PathVariable String hash, @RequestParam("email") String email) {
        return authService.emailVerification(hash, email);
    }
}