package com.englishweb.api.controllers;

import com.englishweb.api.dto.InvoiceDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewInvoiceRequest;
import com.englishweb.api.request.ExportInvoiceRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.InvoiceService;
import com.englishweb.api.utils.InvoiceUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/invoice")
@Tag(name = "Invoice APIs")
@RequiredArgsConstructor
public class InvoiceController {
    private final InvoiceService invoiceService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<InvoiceDTO>> getAllInvoices() {
        return invoiceService.getAllInvoices();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<InvoiceDTO> getInvoiceById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return invoiceService.getInvoiceById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteInvoiceId(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return invoiceService.deleteInvoiceId(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> createNewInvoice(@RequestBody CreateNewInvoiceRequest createNewInvoiceRequest) {
        InvoiceUtils.validateInvoice(createNewInvoiceRequest);
        return invoiceService.createNewInvoice(createNewInvoiceRequest);
    }

    @Validated
    @GetMapping("/exportPDF/{orderId}")
    @ResponseStatus(HttpStatus.OK)
    public byte[] exportInvoice(@PathVariable @NotBlank String orderId, HttpServletResponse response) {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=generated.pdf");
        ExportInvoiceRequest requestData = ExportInvoiceRequest.builder()
                .orderId(orderId)
                .build();
        return invoiceService.exportInvoice(requestData);
    }
}
