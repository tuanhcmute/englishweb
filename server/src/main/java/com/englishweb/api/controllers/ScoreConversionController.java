package com.englishweb.api.controllers;

import com.englishweb.api.request.UpdateScoreConversionRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.ScoreConversionService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/score-conversion")
@RequiredArgsConstructor
@Tag(name = "Score conversion APIs")
public class ScoreConversionController {
    private final ScoreConversionService scoreConversionService;

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateScoreConversion(@Valid @RequestBody UpdateScoreConversionRequest requestData) {
        return  this.scoreConversionService.updateScoreConversion(requestData);
    }
}
