package com.englishweb.api.controllers;

import com.englishweb.api.dto.StudyingFlashcardDTO;
import com.englishweb.api.request.CreateStudyingFlashcardRequest;
import com.englishweb.api.request.UpdateStudyingFlashcardRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.StudyingFlashcardService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("studying-flashcard")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Studying flashcard APIs")
public class StudyingFlashcardController {
    private final StudyingFlashcardService studyingFlashcardService;

    @GetMapping
    public Response<List<StudyingFlashcardDTO>> getAllStudyingFlashcard(@RequestParam Map<String, String> params) {
        return studyingFlashcardService.getAllStudyingFlashcard(params);
    }

    @PostMapping
    public Response<Void> createStudyingFlashcard(@RequestBody CreateStudyingFlashcardRequest requestData) {
        return studyingFlashcardService.createStudyingFlashcard(requestData);
    }

    @PutMapping
    public Response<Void> updateStudyingFlashcard(@RequestBody UpdateStudyingFlashcardRequest requestData) {
        return studyingFlashcardService.updateStudyingFlashcard(requestData);
    }

    @DeleteMapping("/{id}")
    public Response<Void> deleteStudyingFlashcard(@PathVariable String id) {
        return studyingFlashcardService.deleteStudyingFlashcard(id);
    }

}
