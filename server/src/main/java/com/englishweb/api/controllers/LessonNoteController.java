package com.englishweb.api.controllers;

import com.englishweb.api.dto.LessonNoteDTO;
import com.englishweb.api.request.CreateLessonNoteRequest;
import com.englishweb.api.request.UpdateLessonNoteRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.LessonNoteService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lesson-note")
@Tag(name = "Lesson note APIs")
public class LessonNoteController {
    private final LessonNoteService lessonNoteService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> createLessonNote(@Valid @RequestBody CreateLessonNoteRequest requestData) {
        return lessonNoteService.createLessonNote(requestData);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateLessonNote(@Valid @RequestBody UpdateLessonNoteRequest requestData) {
        return lessonNoteService.updateLessonNote(requestData);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<LessonNoteDTO>> getAllLessonNotes(@RequestParam Map<String, String> params) {
        return lessonNoteService.getAllLessonNotes(params);
    }

    @Validated
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> deleteLessonNote(@NotNull @PathVariable String id) {
        return lessonNoteService.deleteLessonNote(id);
    }
}
