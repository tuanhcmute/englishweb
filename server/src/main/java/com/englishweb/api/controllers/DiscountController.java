package com.englishweb.api.controllers;

import com.englishweb.api.dto.DiscountDTO;
import com.englishweb.api.request.CreateDiscountRequest;
import com.englishweb.api.request.DiscountRequest;
import com.englishweb.api.request.UpdateDiscountRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.DiscountService;
import com.englishweb.api.utils.DiscountUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/discount")
@Tag(name = "Discount APIs")
@RequiredArgsConstructor
public class DiscountController {
    private final DiscountService discountService;

    @PostMapping("/assign")
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    public Response<Void> assignCourseDiscount(@RequestBody DiscountRequest discountRequest) {
        DiscountUtils.discountValidate(discountRequest);
        return discountService.assignDiscount(discountRequest);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    private Response<Void> createDiscount(@RequestBody CreateDiscountRequest requestData) {
        return discountService.createDiscount(requestData);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
//    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER')")
    private Response<Void> updateDiscount(@RequestBody UpdateDiscountRequest requestData) {
        return discountService.updateDiscount(requestData);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    private Response<List<DiscountDTO>> getAllDiscounts() {
        return this.discountService.getAllDiscounts();
    }

}
