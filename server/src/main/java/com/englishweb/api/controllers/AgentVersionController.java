package com.englishweb.api.controllers;

import com.englishweb.api.dto.AgentVersionDTO;
import com.englishweb.api.request.UpdateAgentVersionRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.AgentVersionService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/skilled-agent-version")
@Tag(name = "Agent version APIs")
public class AgentVersionController {

    private final AgentVersionService agentVersionService;

    @GetMapping("/activated")
    public Response<AgentVersionDTO> getAgentVersion() {
        return agentVersionService.getAgentVersion();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> updateAgentVersion(@PathVariable String id, @RequestBody UpdateAgentVersionRequest requestData) {
        log.info("Hello");
        return agentVersionService.updateAgentVersion(id, requestData);
    }
}
