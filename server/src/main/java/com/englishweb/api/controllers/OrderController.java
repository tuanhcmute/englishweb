package com.englishweb.api.controllers;

import com.englishweb.api.dto.FullOrderDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
@Tag(name = "Order APIs")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    /**
     * @param params includes userCredentialId and paymentStatus
     * @return Response that includes list of orders
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Response<List<FullOrderDTO>> getAllOrder(@RequestParam Map<String, String> params) {
        return orderService.getAllOrders(params);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<FullOrderDTO> getOrderById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        return orderService.getOrderById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Response<Void> deleteOrderById(@PathVariable String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        return orderService.deleteOrderById(id);
    }

}
