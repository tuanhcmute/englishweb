package com.englishweb.api.controllers;

import com.englishweb.api.models.Word;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.WordService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/word")
@Tag(name = "Word APIs")
public class WordController {
    private final WordService wordService;

    @GetMapping
    public Response<Word[]> getAllWordByTopic(@RequestParam("topic") String topic, @RequestParam("maxResults") String maxResults) {
        String url = String.format("https://api.datamuse.com/words?ml=%s&max=%s", topic, maxResults);
        RestTemplate restTemplate = new RestTemplate();

        // Make the HTTP GET request
        Word[] words = restTemplate.getForObject(url, Word[].class);

        Map<String, Word[]> data = new HashMap<>();
        data.put("data", words);

        return Response.<Word[]>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Get all words by %s successfully", topic))
                .data(data)
                .build();

        // Parse the JSON response and extract words
        // Note: You may want to use a JSON parsing library like Jackson for more complex JSON parsing

    }
}
