package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "user_answer_tbl")
@EntityListeners(AuditingEntityListener.class)
public class UserAnswer implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "answer_json_column")
    private String answerJson;

    @CreatedDate
    @Column(name = "test_date_column")
    private LocalDateTime testDate;

    @Column(name = "total_score_column")
    private Integer totalScore;

    @Column(name = "listening_correct_column")
    private Integer listeningCorrectAnsCount;

    @Column(name = "reading_correct_column")
    private Integer readingCorrectAnsCount;

    @Column(name = "listening_wrong_column")
    private Integer listeningWrongAnsCount;

    @Column(name = "reading_wrong_column")
    private Integer readingWrongAnsCount;

    @Column(name = "ignore_ans_column")
    private Integer ignoreAnsCount;

    @Column(name = "total_sentences_column")
    private Integer totalSentences;

    @Column(name = "completion_time_column")
    private Integer completionTime;

    @Column(name = "practice_type_column")
    private String practiceType;

    @ManyToOne
    @JoinColumn
    private Test test;

    @ManyToOne
    @JoinColumn
    private UserCredential userCredential;
}
