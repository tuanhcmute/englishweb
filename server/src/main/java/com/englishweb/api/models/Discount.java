package com.englishweb.api.models;

import com.englishweb.api.enums.DiscountStatus;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "discount_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Discount {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "discount_name_column")
    private String discountName;

    @Column(name = "start_date_column")
    private LocalDateTime startDate;

    @Column(name = "end_date_column")
    private LocalDateTime endDate;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;

    @Column(name = "status_column")
    @Builder.Default
    private String status = DiscountStatus.SCHEDULED.getValue();

    @OneToMany(mappedBy = "discount")
    @Builder.Default
    private List<DiscountCondition> discountConditions = new ArrayList<>();

}
