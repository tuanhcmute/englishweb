package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "discount_condition_tbl")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class DiscountCondition {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "percent_discount_column")
    private Integer percentDiscount;

    @Column(name = "type_column")
    private String type;

    @ManyToOne
    @JoinColumn
    private Discount discount;

    @OneToMany(mappedBy = "discountCondition", cascade = CascadeType.ALL)
    @Builder.Default
    private List<ConditionTarget> conditionTargets = new ArrayList<>();

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;

}
