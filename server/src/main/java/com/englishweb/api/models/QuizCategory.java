package com.englishweb.api.models;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "quiz_category_tbl")
@EntityListeners(AuditingEntityListener.class)
public class QuizCategory {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "quiz_category_name_column")
    private String quizCategoryName;

    @OneToMany(mappedBy = "quizCategory")
    @Builder.Default
    private Set<Quiz> quizzes = new HashSet<>();
}
