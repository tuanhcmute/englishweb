package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "unit_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Unit {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "title_column")
    private String title;

    @Column(name = "total_lesson_column")
    private Integer totalLesson;

    @Column(name = "sequence_column")
    private Integer sequence;

    @Column(name = "total_hours_column")
    private Integer totalHours;

    @OneToMany(mappedBy = "unit")
    @Builder.Default
    private Set<Lesson> lessons = new HashSet<>();

    @ManyToOne
    @JoinColumn
    private Course course;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}
