package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "flashcard_item_tbl")
@EntityListeners(AuditingEntityListener.class)
public class FlashcardItem implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn
    private Flashcard flashcard;

    @Lob
    @Column(name = "word_column")
    private String word;

    @Lob
    @Column(name = "phonetic_column")
    private String phonetic;

    @Lob
    @Column(name = "meaning_column")
    private String meaning;

    @Lob
    @Column(name = "tags_column")
    private String tags;

    @Lob
    @Column(name = "audio_column")
    private String audio;

    @Lob
    @Column(name = "image_column")
    private String image;

    @Lob
    @Column(name = "examples_column")
    private String examples;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;
}
