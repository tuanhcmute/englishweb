package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "course_detail_tbl")
public class CourseDetail implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "course_introduction_column")
    private String courseIntroduction;

    @Lob
    @Column(name = "course_target_column")
    private String courseTarget;

    @Lob
    @Column(name = "course_information_column")
    private String courseInformation;

    @Lob
    @Column(name = "course_guide_column")
    private String courseGuide;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "course_id_tbl", referencedColumnName = "id_column")
    private Course course;
}
