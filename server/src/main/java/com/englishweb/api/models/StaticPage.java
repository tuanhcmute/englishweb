package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "static_page_tbl")
@EntityListeners(AuditingEntityListener.class)
public class StaticPage implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "page_code_column", unique = true)
    private String pageCode;

    @Lob
    @Column(name = "page_name_column")
    private String pageName;

    @Lob
    @Column(name = "page_content_column")
    private String pageContent;

    @Lob
    @Column(name = "page_preview_column")
    private String pagePreview;

    @Lob
    @Column(name = "page_url_video_column")
    private String pageUrlVideo;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;

    @ManyToOne
    @JoinColumn
    private UserCredential createdBy;
}
