package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "user_credential_tbl")
@EntityListeners(AuditingEntityListener.class)
public class UserCredential implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "username_column", unique = true, updatable = false)
    private String username;

    @Column(name = "full_name_column")
    private String fullName;

    @Column(name = "password_column")
    private String password;

    @Column(name = "description_column")
    private String description;

    @Column(name = "role_name_column")
    private String roleName;

    @Column(name = "phone_number_column")
    private String phoneNumber;

    @Column(name = "avatar_column")
    private String avatar;

    @Column(name = "email_column", nullable = false, unique = true)
    private String email;

    @Column(name = "verified_column", nullable = false)
    private Boolean verified;

    @Column(name = "provider_column", nullable = false)
    private String provider;

    @Column(name = "dob_column")
    private LocalDate dob;

    @Column(name = "address_column")
    private String address;

    @Column(name = "gender_column")
    private String gender;

    @Column(name = "major_column")
    private String major;

    @OneToMany(mappedBy = "userCredential")
    @Builder.Default
    private Set<UserAnswer> userAnswers = new HashSet<>();

    @OneToMany(mappedBy = "createdBy")
    @Builder.Default
    private Set<StaticPage> staticPages = new HashSet<>();

    @OneToMany(mappedBy = "userCredential")
    @Builder.Default
    private Set<Flashcard> flashcards = new HashSet<>();

    @OneToMany(mappedBy = "userCredential", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Builder.Default
    Set<UserAuthority> userAuthorities = new HashSet<>();

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;

}
