package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "course_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Course implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "course_img_column")
    private String courseImg;

    @Lob
    @Column(name = "course_name_column")
    private String courseName;

    @Column(name = "new_price_column")
    private Integer newPrice;

    @Column(name = "old_price_column")
    private Integer oldPrice;

    @Column(name = "percent_discount_column")
    private Integer percentDiscount;

    @Column(name = "is_active_column", nullable = false)
    private Boolean isActive;

    @OneToOne(mappedBy = "course")
    private CourseDetail courseDetail;

    @ManyToOne
    @JoinColumn
    private CourseCategory courseCategory;

    @Column(name = "is_discount_column", nullable = false)
    @Builder.Default
    private Boolean isDiscount = false;

    @OneToMany(mappedBy = "course")
    @Builder.Default
    private Set<Unit> units = new HashSet<>();

    @OneToMany(mappedBy = "course")
    @Builder.Default
    private Set<Review> reviews = new HashSet<>();

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;
}
