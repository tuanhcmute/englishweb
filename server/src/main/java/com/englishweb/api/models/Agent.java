package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "skilled_agent")
@EntityListeners(AuditingEntityListener.class)
public class Agent implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false)
    private String id;

    @Column(name = "agent_name")
    private String agentName;

    @Column(name = "setup_message")
    private String setupMessage;

    @Column(name = "test_score")
    private String testScore;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "agent")
    @Builder.Default
    private List<AgentVersion> agentVersions = new ArrayList<>();
}
