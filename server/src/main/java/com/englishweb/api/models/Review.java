package com.englishweb.api.models;

import com.englishweb.api.enums.ReviewType;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "review_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Review {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @ManyToOne
    @JoinColumn
    private Course course;

    @ManyToOne
    @JoinColumn
    private UserCredential userCredential;

    @Lob
    @Column(name = "content_column")
    private String content;

    @Lob
    @Column(name = "image_column")
    private String image;

    @Column(name = "rating_column")
    private Integer rating;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;

    @Column(name = "status_column")
    @Builder.Default
    private String status = ReviewType.PENDING.getValue();

    @Column(name = "is_public_column")
    @Builder.Default
    private Boolean isPublic = false;
}
