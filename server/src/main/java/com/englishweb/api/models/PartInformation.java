package com.englishweb.api.models;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "part_information_tbl")
@EntityListeners(AuditingEntityListener.class)
public class PartInformation implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "part_information_name_column")
    private String partInformationName;

    @Column(name = "part_type_column")
    private String partType; // Reading or Listening

    @Column(name = "total_question_column")
    private Integer totalQuestion;

    @Column(name = "part_sequence_number_column")
    private Integer partSequenceNumber;

    @Lob
    @Column(name = "part_information_description_column")
    private String partInformationDescription;

    @ManyToOne
    @JoinColumn
    private TestCategory testCategory;

    @OneToMany(mappedBy = "partInformation", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<PartOfTest> partOfTests = new HashSet<>();
}
