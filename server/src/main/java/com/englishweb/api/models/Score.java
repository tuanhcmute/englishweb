package com.englishweb.api.models;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Score {
    private Map<Integer, Integer> reading;
    private Map<Integer, Integer> listening;
}
