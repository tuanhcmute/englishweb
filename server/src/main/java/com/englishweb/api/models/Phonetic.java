package com.englishweb.api.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Phonetic {
    private String text;
    private String audio;
}
