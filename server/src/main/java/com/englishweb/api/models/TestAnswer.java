package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "test_answer_tbl")
@EntityListeners(AuditingEntityListener.class)
public class TestAnswer implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "answer_content_column")
    private String answerContent;

    @Column(name = "answer_choice_column")
    private String answerChoice;

    @Column(name = "answer_translate_column")
    private String answerTranslate;

    @Column(name = "is_correct_column")
    private Boolean isCorrect;

    @ManyToOne
    @JoinColumn
    private Question question;
}
