package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "test_tbl")
//@RedisHash("redis_test")
@EntityListeners(AuditingEntityListener.class)
public class Test implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Nationalized
    @Column(name = "test_name_column")
    private String testName;

    @Lob
    @Nationalized
    @Column(name = "test_description_column")
    private String testDescription;

    @Column(name = "number_of_part_column")
    private Integer numberOfPart;

    @Lob
    @Nationalized
    @Column(name = "thumbnail_column")
    private String thumbnail;

    @Column(name = "number_of_question_column")
    private Integer numberOfQuestion;

    @Column(name = "year_column")
    private Integer year;

    @ManyToOne
    @JoinColumn
    private TestCategory testCategory;

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<PartOfTest> listOfPart = new HashSet<>();

    @OneToMany(mappedBy = "test")
    @Builder.Default
    private Set<UserAnswer> userAnswers = new HashSet<>();

    @OneToMany(mappedBy = "test")
    @Builder.Default
    private Set<Comment> testReview = new HashSet<>();

    @OneToOne(mappedBy = "test", cascade = CascadeType.ALL)
    private FullTestAnswer fullTestAnswer;

    @OneToOne(mappedBy = "test", cascade = CascadeType.ALL)
    private ScoreConversion scoreConversion;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}