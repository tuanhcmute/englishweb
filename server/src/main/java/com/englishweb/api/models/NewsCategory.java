package com.englishweb.api.models;

import com.englishweb.api.enums.NewsCategoryType;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "news_category_tbl")
@EntityListeners(AuditingEntityListener.class)
public class NewsCategory implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "course_category_name_column")
    private String newsCategoryName;

    @Column(name = "status_column")
    @Builder.Default
    private String status = NewsCategoryType.VISIBLE.getValue();

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;

    @OneToMany(mappedBy = "newsCategory", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<News> news = new HashSet<>();
}