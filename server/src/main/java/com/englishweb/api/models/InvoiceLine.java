package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "invoice_line_tbl")
@EntityListeners(AuditingEntityListener.class)
public class InvoiceLine {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @ManyToOne
    @JoinColumn
    private Invoice invoice;

    @Column(name = "quantity_column")
    private Integer quantity;

    @Column(name = "unit_price_column")
    private Integer unitPrice;

    @Column(name = "discount_column")
    private Integer discount;

    @Column(name = "total_price_column")
    private Integer totalPrice;

    @ManyToOne
    @JoinColumn
    private Course course;
}
