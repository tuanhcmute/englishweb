package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "lesson_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Lesson implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "title_column")
    private String title;

    @Column(name = "time_column")
    private Integer time;

    @Lob
    @Column(name = "video_column")
    private String video;

    @Column(name = "video_type_column")
    private String videoType;

    @Column(name = "sequence_column")
    private Integer sequence;

    @Lob
    @Column(name = "content_column")
    private String content;

    @ManyToOne
    @JoinColumn
    private Unit unit;

    @OneToMany(mappedBy = "lesson")
    @Builder.Default
    private Set<Quiz> quizzes = new HashSet<>();

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}
