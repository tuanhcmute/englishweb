package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "role_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Role implements Serializable {
    @OneToMany(mappedBy = "role")
    Set<UserAuthority> userAuthorities;
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;
    @Column(name = "role_name_column")
    @Enumerated(value = EnumType.STRING)
    private com.englishweb.api.enums.Role roleName;
}
