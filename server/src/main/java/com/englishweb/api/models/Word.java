package com.englishweb.api.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Word {
    private String word;
    private String score;
    private String[] tags;
}
