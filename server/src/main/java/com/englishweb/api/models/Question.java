package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "question_tbl")
public class Question implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Nationalized
    @Column(name = "question_name_column")
    private String questionContent;

    @Column(name = "question_image_column")
    private String questionImage;

    @Column(name = "question_number_column")
    private Integer questionNumber;

    @Lob
    @Nationalized
    @Column(name = "right_answer_column")
    private String rightAnswer;

    @Lob
    @Nationalized
    @Column(name = "question_guide_column")
    private String questionGuide;

    @ManyToOne
    @JoinColumn
    private QuestionGroup questionGroup;


    @OneToMany(mappedBy = "question", cascade = {CascadeType.ALL})
    @Builder.Default
    private Set<TestAnswer> testAnswers = new HashSet<>();
}
