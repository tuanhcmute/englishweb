package com.englishweb.api.models;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Definition {
    private String definition;
    private String example;
}
