package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "flashcard_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Flashcard implements Serializable {

    @OneToMany(mappedBy = "flashcard", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, orphanRemoval = true)
    @Builder.Default
    Set<FlashcardItem> flashcardItems = new HashSet<>();
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")

    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "flashcard_title_column")
    private String flashcardTitle;

    @Lob
    @Column(name = "flashcard_description_column")
    private String flashcardDescription;

    @Column(name = "total_flashcard_item_column")
    private Integer totalFlashcardItem;

    @ManyToOne
    @JoinColumn
    private UserCredential userCredential;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;

    @Column(name = "flashcard_type_column")
    private String flashcardType;
}
