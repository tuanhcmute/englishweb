package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "full_test_answer_tbl")
public class FullTestAnswer implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "full_test_answer_json_column")
    @Lob
    private String fullTestAnswerJson;

    @OneToOne
    @JoinColumn(name = "test_id_column", referencedColumnName = "id_column")
    private Test test;
}
