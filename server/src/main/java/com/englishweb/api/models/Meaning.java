package com.englishweb.api.models;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Meaning {
    private String partOfSpeech;
    private List<Definition> definitions;
}


