package com.englishweb.api.models;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChunkWithMetadata {
    private FileMetadata fileMetadata;
    private byte[] chunk;
}
