package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "skilled_agent_version")
@EntityListeners(AuditingEntityListener.class)
public class AgentVersion implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false)
    private String id;

    @Column(name = "version")
    private String version;

    @Column(name = "setup_message")
    private String setupMessage;

    @Column(name = "is_active")
    @Builder.Default
    private Boolean isActive = false;

    @ManyToOne
    @JoinColumn(name = "skilled_agent_id")
    private Agent agent;
}
