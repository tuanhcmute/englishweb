package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "comment_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Comment implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "content_column")
    private String content;

    @ManyToOne
    @JoinColumn
    private Test test;

    @ManyToOne
    @JoinColumn
    private UserCredential author;

    @Column(name = "is_root_column")
    @Builder.Default
    private Boolean isRoot = false;

    @Column(name = "is_negative_column")
    @Builder.Default
    private Boolean isNegative = false;

    @Builder.Default
    @Column(name = "is_filtered_column")
    private Boolean isFiltered = false;

    @Builder.Default
    @Column(name = "is_review_column")
    private Boolean isReview = false;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date_column")
    private LocalDateTime lastModifiedDate;

    @OneToMany(mappedBy = "comment")
    @Builder.Default
    private Set<ReportComment> reportComments = new HashSet<>();

    @OneToMany(mappedBy = "parentComment", cascade = {CascadeType.ALL})
    @Builder.Default
    Set<SubComment> subComments = new HashSet<>();
}
