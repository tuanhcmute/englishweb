package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "order_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Order implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @CreatedDate
    @Column(name = "order_date_column")
    private LocalDateTime orderDate;

    @Column(name = "payment_status_column")
    private Boolean paymentStatus;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<OrderLine> orderLines = new HashSet<>();

    @ManyToOne
    @JoinColumn
    private UserCredential userCredential;

    @OneToOne(mappedBy = "order")
    private Invoice invoice;
}
