package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "test_category_tbl")
@EntityListeners(AuditingEntityListener.class)
public class TestCategory implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "number_of_part_column")
    private Integer numberOfPart;

    @Column(name = "total_question_column")
    private Integer totalQuestion;

    @Lob
    @Column(name = "test_category_name_column")
    private String testCategoryName;

    @OneToMany(mappedBy = "testCategory")
    @Builder.Default
    private Set<PartInformation> partInformation = new HashSet<>();

    @OneToMany(mappedBy = "testCategory")
    @Builder.Default
    private Set<Test> tests = new HashSet<>();

    @Column(name = "total_time_column")
    private Integer totalTime;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}
