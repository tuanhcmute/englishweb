package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "question_group_tbl")
@EntityListeners(AuditingEntityListener.class)
public class QuestionGroup implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "audio_column")
    private String audio;

    @Lob
    @Column(name = "image_column")
    private String image;

    @Lob
    @Nationalized
    @Column(name = "question_content_en_column")
    private String questionContentEn;

    @Lob
    @Nationalized
    @Column(name = "question_content_transcript_column")
    private String questionContentTranscript;

    @ManyToOne
    @JoinColumn
    private PartOfTest partOfTest;

    @OneToMany(mappedBy = "questionGroup", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<Question> questions = new HashSet<>();

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}
