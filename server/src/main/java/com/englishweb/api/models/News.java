package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "news_tbl")
@EntityListeners(AuditingEntityListener.class)
public class News implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "news_title_column")
    private String newsTitle;

    @Lob
    @Column(name = "news_description_column")
    private String newsDescription;

    @Lob
    @Column(name = "news_image_column")
    private String newsImage;

    @Lob
    @Column(name = "news_content_html_column")
    private String newsContentHtml;

    @Column(name = "status_column")
    private String status;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;

    @ManyToOne
    @JoinColumn
    private NewsCategory newsCategory;

    @ManyToOne
    @JoinColumn
    private UserCredential author;
}
