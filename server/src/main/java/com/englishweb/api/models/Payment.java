package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "payment_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Payment implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Lob
    @Column(name = "order_info_column")
    private String orderInfo;

    @Column(name = "payment_time_column")
    private String paymentTime;

    @Column(name = "transaction_id_column")
    private String transactionId;

    @Column(name = "total_amount_column")
    private String totalAmount;

    @CreatedDate
    @Column(name = "created_date_column")
    private LocalDateTime createdDate;
}
