package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "invoice_tbl")
@EntityListeners(AuditingEntityListener.class)
public class Invoice implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @ManyToOne
    @JoinColumn
    private Payment payment;

    @Column(name = "amount_price_column")
    private Integer amountPrice;

    @Column(name = "total_price_column")
    private Integer totalPrice;

    @Column(name = "tax_column")
    private Integer tax;

    @Lob
    @Column(name = "invoice_title_column")
    private String invoiceTitle;

    @Lob
    @Column(name = "invoice_description_column")
    private String invoiceDescription;

    @Column(name = "created_date_column")
    @CreatedDate
    private LocalDateTime createdDate;

    @OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<InvoiceLine> invoiceLines = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id_tbl", referencedColumnName = "id_column")
    private Order order;
}
