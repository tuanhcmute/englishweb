package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "part_of_test_tbl")
@EntityListeners(AuditingEntityListener.class)
public class PartOfTest implements Serializable {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @ManyToOne
    @JoinColumn
    private PartInformation partInformation;

    @Column(name = "part_sequence_number_column")
    private Integer partSequenceNumber;


    @ManyToOne
    @JoinColumn
    private Test test;

    @OneToMany(mappedBy = "partOfTest")
    @Builder.Default
    private Set<QuestionGroup> listOfQuestionGroups = new HashSet<>();
}
