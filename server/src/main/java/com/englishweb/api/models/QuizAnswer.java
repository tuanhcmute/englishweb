package com.englishweb.api.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "quiz_answer_tbl")
@EntityListeners(AuditingEntityListener.class)
public class QuizAnswer {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id_column", updatable = false)
    private String id;

    @Column(name = "quiz_answer_content_column")
    private String quizAnswerContent;

    @Column(name = "quiz_answer_choice_column")
    private String quizAnswerChoice;

    @Column(name = "quiz_answer_translate_column")
    private String quizAnswerTranslate;

    @Column(name = "is_correct_column")
    private Boolean isCorrect;

    @ManyToOne
    @JoinColumn
    private Quiz quiz;
}
