package com.englishweb.api.events;

import com.englishweb.api.models.Discount;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class DiscountEvent extends ApplicationEvent {

    private Discount discount;

    public DiscountEvent(Object source, Discount discount) {
        super(source);
        this.discount = discount;
    }
}
