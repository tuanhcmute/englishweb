package com.englishweb.api.events;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class FlashcardItemEvent extends ApplicationEvent {
    private String flashcardId;

    public FlashcardItemEvent(Object source, String flashcardId) {
        super(source);
        this.flashcardId = flashcardId;
    }
}
