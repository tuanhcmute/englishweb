package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateFlashcardRequest;

public class FlashcardUtils {
    public static void flashcardValidate(CreateFlashcardRequest createFlashcardRequest) {
        if (createFlashcardRequest.getFlashcardDescription().isBlank())
            throw new BadRequestException("Flash card description value is empty");

        if (createFlashcardRequest.getFlashcardTitle().isBlank())
            throw new BadRequestException("Flash card title value is empty");

        if (createFlashcardRequest.getTotalFlashcardItem() == null)
            throw new BadRequestException("Flash card total value is empty");

        if (createFlashcardRequest.getTotalFlashcardItem() < 0)
            throw new BadRequestException("Flash card description value is invalid");
    }
}
