package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateReviewRequest;

public class ReviewUtils {
    public static void reviewValidate(CreateReviewRequest createReviewRequest) {
        if (createReviewRequest.getContent().trim().isEmpty())
            throw new BadRequestException("Review content value is empty!");
    }
}
