package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateCourseRequest;

public class CourseUtils {
    public static void courseValidate(CreateCourseRequest course) {
        if (course.getCourseName().isEmpty())
            throw new BadRequestException("Course name value is empty!");

        if (course.getOldPrice() == null)
            throw new BadRequestException("Old price value is empty!");

        if (course.getIsActive() == null)
            throw new BadRequestException("Active status is value empty!");
    }
}
