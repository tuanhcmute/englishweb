package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.QuestionRequest;
import com.englishweb.api.request.UpdateQuestionRequest;

public class QuestionUtils {
    public static void questionValidate(QuestionRequest questionRequest) {
        if (questionRequest.getQuestionContent().isEmpty())
            throw new BadRequestException("Question name value is empty!");

        if (questionRequest.getQuestionGroupId().isEmpty())
            throw new BadRequestException("Question group id value is empty!");

        if (questionRequest.getQuestionGuide().isEmpty())
            throw new BadRequestException("Question guide value is empty!");

//        if (questionRequest.getRightAnswer().isEmpty())
//            throw new BadRequestException("Right answer value is empty!");
    }

    public static void updateQuestionValidate(UpdateQuestionRequest updateQuestionRequest) {
        if (updateQuestionRequest.getQuestionContent().isEmpty())
            throw new BadRequestException("Question name value is empty!");

        if (updateQuestionRequest.getQuestionGuide().isEmpty())
            throw new BadRequestException("Question guide value is empty!");

        if (updateQuestionRequest.getQuestionNumber() == null)
            throw new BadRequestException("Question number value is invalid!");

        if (updateQuestionRequest.getTestAnswers().size() == 0)
            throw new BadRequestException("List answer of question is empty!");
    }
}
