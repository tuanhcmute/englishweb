package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreatePaymentRequest;

public class PaymentUtils {
    public static void paymentValidate(CreatePaymentRequest createPaymentRequest) {
        if (createPaymentRequest.getPaymentImage().strip().isEmpty())
            throw new BadRequestException("Payment image value is empty!");

        if (createPaymentRequest.getVotes() == null)
            throw new BadRequestException("Vote values is null!");

        if (createPaymentRequest.getVotes() < 0)
            throw new BadRequestException("Vote values is invalid!");
    }
}
