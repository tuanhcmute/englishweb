package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.SendOTPRequest;

public class SendOTPRequestUtils {
    public static void sendOTPValidate(SendOTPRequest sendOTPRequest) {
        if (sendOTPRequest.getEmailTo().isEmpty())
            throw new BadRequestException("Email value is empty!");
    }
}
