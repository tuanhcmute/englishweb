package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.SignUpRequest;

public class SignUpUtils {
    public static void signUpValidate(SignUpRequest signUpRequest) {
        if (signUpRequest.getUsername().trim().isEmpty())
            throw new BadRequestException("Username value is empty!");

        if (signUpRequest.getFullName().trim().isEmpty())
            throw new BadRequestException("Full name value is empty!");

        if (signUpRequest.getPassword().trim().isEmpty())
            throw new BadRequestException("Password value is empty!");

        if (signUpRequest.getConfirmPassword().trim().isEmpty())
            throw new BadRequestException("Confirm password value is empty!");

        if (!signUpRequest.getPassword().trim().equals(signUpRequest.getConfirmPassword().trim()))
            throw new BadRequestException("Confirm password and password value is different!");
    }
}
