package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateFlashcardItemRequest;

public class FlashcardItemUtils {
    public static void flashcardItemValidate(CreateFlashcardItemRequest CreateFlashcardItemRequest) {
        if (CreateFlashcardItemRequest.getWord().strip().isEmpty())
            throw new BadRequestException("Flash card item word value is empty!");

        if (CreateFlashcardItemRequest.getPhonetic().strip().isEmpty())
            throw new BadRequestException("Flash card item phonetic value is empty!");

        if (CreateFlashcardItemRequest.getMeaning().strip().isEmpty())
            throw new BadRequestException("Flash card item meaning value is empty!");

        if (CreateFlashcardItemRequest.getTags().strip().isEmpty())
            throw new BadRequestException("Flash card item audio tags value is empty!");

        if (CreateFlashcardItemRequest.getTags().strip().isEmpty())
            throw new BadRequestException("Flash card item audio tags value is empty!");
    }
}
