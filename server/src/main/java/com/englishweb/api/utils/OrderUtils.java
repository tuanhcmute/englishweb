package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewOrderRequest;

public class OrderUtils {
    public static void validateOrder(CreateNewOrderRequest createNewOrderRequest) {
        if (createNewOrderRequest.getOrderDate() == null)
            throw new BadRequestException("Order date is null!");

        if (createNewOrderRequest.getPaymentStatus() == null)
            throw new BadRequestException("Payment status is null!");
    }
}
