package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateLessonRequest;

public class LessonUtils {
    public static void validateCreateNewLessonRequestModel(CreateLessonRequest createLessonRequest) {
        if (createLessonRequest.getTitle().trim().isEmpty())
            throw new BadRequestException("Title value is empty!");

        if (createLessonRequest.getUnitId() == null)
            throw new BadRequestException("Unit id value is null!");
    }
}
