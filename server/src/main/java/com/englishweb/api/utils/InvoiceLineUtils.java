package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewInvoiceLineRequest;

public class InvoiceLineUtils {
    public static void validateInvoiceLine(CreateNewInvoiceLineRequest createNewInvoiceLineRequest) {
        if (createNewInvoiceLineRequest.getQuantity() == null)
            throw new BadRequestException("Quantity value is nulll!");

        if (createNewInvoiceLineRequest.getQuantity() < 0)
            throw new BadRequestException("Quantity value is invalid!");

        if (createNewInvoiceLineRequest.getUnitPrice() == null)
            throw new BadRequestException("Unit price value is nulll!");

        if (createNewInvoiceLineRequest.getUnitPrice() < 0)
            throw new BadRequestException("Unit price value is invalid!");

        if (createNewInvoiceLineRequest.getDiscount() == null)
            throw new BadRequestException("Discount value is nulll!");

        if (createNewInvoiceLineRequest.getDiscount() < 0)
            throw new BadRequestException("Discount value is invalid!");

        if (createNewInvoiceLineRequest.getTotalPrice() == null)
            throw new BadRequestException("Total price0 value is nulll!");

        if (createNewInvoiceLineRequest.getTotalPrice() < 0)
            throw new BadRequestException("Total price0 value is invalid!");
    }
}
