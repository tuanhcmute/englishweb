package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreatePartInformation;

public class PartInformationUtils {
    public static void partInformationValidate(CreatePartInformation partInformation) {
        if (partInformation.getPartInformationName().isEmpty())
            throw new BadRequestException("Part information name value is empty!");

        if (partInformation.getTotalQuestion() == null)
            throw new BadRequestException("Total question value is empty!");

        if (partInformation.getPartInformationDescription().isEmpty())
            throw new BadRequestException("Part information description value is empty!");
    }
}
