package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.ForgotPasswordRequest;

public class ForgotPasswordUtils {
    public static void forgotPasswordValidate(ForgotPasswordRequest forgotPasswordRequest) {
        if (forgotPasswordRequest.getEmail().isEmpty())
            throw new BadRequestException("Email value is empty!");

        if (forgotPasswordRequest.getConfirmPassword().isEmpty())
            throw new BadRequestException("Confirm password value is empty!");

        if (forgotPasswordRequest.getNewPassword().isEmpty())
            throw new BadRequestException("New password value is empty!");

        if (forgotPasswordRequest.getOtp().isEmpty())
            throw new BadRequestException("OTP value is empty!");
    }
}
