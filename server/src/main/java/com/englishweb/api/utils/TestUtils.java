package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateTestRequest;

public class TestUtils {
    public static void testValidate(CreateTestRequest createTestRequest) {
        if (createTestRequest.getTestName().isEmpty() || createTestRequest.getTestName().isBlank())
            throw new BadRequestException("Test name value is empty!");

        if (createTestRequest.getTestDescription().isEmpty() || createTestRequest.getTestDescription().isBlank())
            throw new BadRequestException("Test description value is empty!");

        if (createTestRequest.getNumberOfPart() == null)
            throw new BadRequestException("Number of part value is empty!");

        if (createTestRequest.getNumberOfQuestion() == null)
            throw new BadRequestException("Number of question value is empty!");

        if (createTestRequest.getTestCategoryId().isEmpty() || createTestRequest.getTestCategoryId().isBlank())
            throw new BadRequestException("Test category id value is empty!");
    }
}
