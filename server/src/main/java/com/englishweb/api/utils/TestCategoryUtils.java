package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateTestCategory;

public class TestCategoryUtils {
    public static void testCategoryValidate(CreateTestCategory testCategory) {
        if (testCategory.getTestCategoryName().isEmpty())
            throw new BadRequestException("Test category name value is empty!");

        if (testCategory.getNumberOfPart() == null)
            throw new BadRequestException("Number of part value is empty!");
    }
}
