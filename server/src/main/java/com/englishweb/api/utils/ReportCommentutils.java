package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewReportCommentRequest;

public class ReportCommentutils {
    public static void reportCommentValidate(CreateNewReportCommentRequest createNewReportCommentRequest) {
        if (createNewReportCommentRequest.getCommentId().isEmpty())
            throw new BadRequestException("Comment id value is empty!");

        if (createNewReportCommentRequest.getReportContent().isEmpty())
            throw new BadRequestException("Report content id value is empty!");
    }
}
