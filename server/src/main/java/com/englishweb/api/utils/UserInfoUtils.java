package com.englishweb.api.utils;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.UpdateUserInfoRequest;

public class UserInfoUtils {
    public static void userInfoValdate(UpdateUserInfoRequest updateUserInfoRequest) {
        if (updateUserInfoRequest.getId().strip().isEmpty())
            throw new BadRequestException("Id value is empty!");

        if (updateUserInfoRequest.getFullName().strip().isEmpty())
            throw new BadRequestException("FullName value is empty!");

        if (updateUserInfoRequest.getPhoneNumber().strip().isEmpty())
            throw new BadRequestException("Phone number value is empty!");

        if (updateUserInfoRequest.getDob() == null)
            throw new BadRequestException("Dob value is invalid!");

        if (updateUserInfoRequest.getAddress().strip().isEmpty())
            throw new BadRequestException("Address value is empty!");

        if (updateUserInfoRequest.getGender().strip().isEmpty())
            throw new BadRequestException("Gender value is empty!");

        if (updateUserInfoRequest.getMajor().strip().isEmpty())
            throw new BadRequestException("Major value is empty!");
    }
}
