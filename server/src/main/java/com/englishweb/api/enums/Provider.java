package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum Provider {
    GOOGLE("google.com"),
    LOCAL("local");
    private String value;
}
