package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum QuestionGroupFileType {
    AUDIO("audio"),
    IMAGE("image");
    private String value;
}
