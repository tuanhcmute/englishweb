package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum PaypalPaymentMethod {
    CREDIT_CARD("credit_card"), PAYPAL("paypal");
    private String value;
}
