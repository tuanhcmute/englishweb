package com.englishweb.api.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ErrorStatus {
    REDIS_UNCONNECTED(1999),
    EMAIL_EXIST(2000),
    USERNAME_EXIST(2001),
    PASSWORD_NOT_MATCH(2002),
    PROVIDER_NOT_FOUND(2003),
    ROLE_NOT_FOUND(2004),
    USER_EXIST(2005),
    METHOD_NOT_ALLOWED(2006),
    USERNAME_NOT_EXIST(2007),
    FIELD_NOT_VALID(2008);
    private Integer errorCode;
}
