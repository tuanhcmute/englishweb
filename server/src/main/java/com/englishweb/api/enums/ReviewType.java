package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum ReviewType {
    PENDING("pending"),
    REJECTED("rejected"),
    APPROVED("approved");
    private String value;
}
