package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum PaypalPaymentIntent {
    SALE("sale"), AUTHORIZE("authorize"), ORDER("order");
    private String value;
}
