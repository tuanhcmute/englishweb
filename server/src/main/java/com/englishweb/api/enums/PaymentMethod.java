package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum PaymentMethod {
    VNPAY("vnpay"),
    PAYPAL("paypal");
    private String value;
}
