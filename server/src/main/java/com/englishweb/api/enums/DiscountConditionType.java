package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum DiscountConditionType {
    COURSE("course"),
    COURSE_CATEGORY("course_category");
    private String value;
}
