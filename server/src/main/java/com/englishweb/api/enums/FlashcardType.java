package com.englishweb.api.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum FlashcardType {
    PERSONAL("personal"),
    SUGGESTION("suggestion");
    private String value;
}
