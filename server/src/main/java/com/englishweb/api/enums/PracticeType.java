package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum PracticeType {
    FULL_TEST("full_test"),
    PARTIALS("partials");
    private String value;
}
