package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum NewsCategoryType {
    VISIBLE("visible"),
    INVISIBLE("invisible");
    private String value;
}
