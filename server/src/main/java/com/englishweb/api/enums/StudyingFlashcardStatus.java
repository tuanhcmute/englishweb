package com.englishweb.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum StudyingFlashcardStatus {
    STUDYING("studying"),
    DONE("done");
    private String value;

}
