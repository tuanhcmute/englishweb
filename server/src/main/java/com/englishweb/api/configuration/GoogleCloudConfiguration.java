package com.englishweb.api.configuration;


import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.language.v1.LanguageServiceClient;
import com.google.cloud.language.v1.LanguageServiceSettings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

@Configuration
public class GoogleCloudConfiguration {
    @Value("${app.google-cloud.path}")
    private String resource;

    @Bean
    public LanguageServiceClient languageServiceClient() throws IOException {
        ClassPathResource classPathResource = new ClassPathResource(resource);
        GoogleCredentials credentials = GoogleCredentials.fromStream(classPathResource.getInputStream());
        LanguageServiceSettings settings = LanguageServiceSettings.newBuilder().setCredentialsProvider(() -> credentials).build();
        return LanguageServiceClient.create(settings);
    }
}
