package com.englishweb.api.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectMapperConfiguration {

    @Bean
    ObjectMapper objectMapper() {
        ObjectMapper _objectMapper = new ObjectMapper();
        _objectMapper.findAndRegisterModules();
        return _objectMapper;
    }
}
