package com.englishweb.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@Configuration
public class ITextRendererConfiguration {
    @Bean
    public ITextRenderer iTextRenderer() {
        ITextRenderer iTextRenderer = new ITextRenderer();
//        ITextFontResolver fontResolver = iTextRenderer.getFontResolver();
//
//        // Register all TTF fonts in the fonts directory
//        Resource fontsDirResource = new ClassPathResource("fonts/roboto");
//        try {
//            URI uri = fontsDirResource.getURI();
//            Path fontsDirPath;
//
//            if ("jar".equals(uri.getScheme())) {
//                try (FileSystem fs = FileSystems.newFileSystem(uri, Collections.emptyMap())) {
//                    fontsDirPath = fs.getPath("fonts/roboto");
//                    registerFonts(fontsDirPath, fontResolver);
//                }
//            } else {
//                fontsDirPath = Paths.get(uri);
//                registerFonts(fontsDirPath, fontResolver);
//            }
//        } catch (IOException e) {
//            throw new RuntimeException("Error resolving font directory path: " + e.getMessage(), e);
//        }
        return iTextRenderer;
    }

    private void registerFonts(Path fontsDirPath, ITextFontResolver fontResolver) throws IOException {
        try (Stream<Path> paths = Files.walk(fontsDirPath)) {
            paths.filter(Files::isRegularFile)
                    .filter(path -> path.toString().toLowerCase().endsWith(".ttf"))
                    .forEach(path -> {
                        try {
                            fontResolver.addFont(path.toString(), true);
                            System.out.println("Registered font: " + path);
                        } catch (Exception e) {
                            System.err.println("Error adding font: " + path + " - " + e.getMessage());
                            e.printStackTrace();
                        }
                    });
        }
    }
}
