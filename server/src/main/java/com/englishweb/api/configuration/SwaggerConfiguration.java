package com.englishweb.api.configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.SpecVersion;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;

@Configuration
public class SwaggerConfiguration {
    @Bean
    public OpenAPI openAPI() {
        SecurityRequirement securityRequirement = new SecurityRequirement().addList("Bearer")
                .addList(String.valueOf(new Scopes().addString("global", "access all APIs")));
        return new OpenAPI(SpecVersion.V30).info(info()).externalDocs(new ExternalDocumentation()).servers(servers())
                .security(Collections.singletonList(securityRequirement))
                .components(new Components().addSecuritySchemes("Bearer",
                        new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")));
    }

    private Info info() {
        Info info = new Info();
        info.title("Swagger Documentation");
        info.description("This project is developed to explore about spring doc configurations");
        return info;
    }

    private List<Server> servers() {

        Server localServer = new Server();
        Server developServer = new Server();
        Server productionServer = new Server();

        // Local
        localServer.url("http://localhost:8080/v1.0.0");
        localServer.description("API URL localhost");

        // Develop
        developServer.url("https://englishweb.insidedev.me/server/v1.0.0");
        developServer.description("API URL develop");

        // Production
        productionServer.url("https://english.workon.space/server/v1.0.0");
        productionServer.description("API URL production");
        return List.of(localServer, developServer, productionServer);
    }
}
