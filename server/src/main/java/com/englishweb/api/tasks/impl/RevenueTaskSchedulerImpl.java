package com.englishweb.api.tasks.impl;

import com.englishweb.api.services.interfaces.RevenueService;
import com.englishweb.api.tasks.RevenueTaskScheduler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class RevenueTaskSchedulerImpl implements RevenueTaskScheduler {
    private final RevenueService revenueService;

    @Scheduled(cron = "0 27 11 28 6 *") // Runs every minute
    private void createRevenue() {
        revenueService.createRevenue();
    }
}
