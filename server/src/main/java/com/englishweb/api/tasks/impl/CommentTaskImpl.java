package com.englishweb.api.tasks.impl;

import com.englishweb.api.models.Comment;
import com.englishweb.api.services.interfaces.CommentService;
import com.englishweb.api.services.repository.CommentRepository;
import com.englishweb.api.tasks.CommentTask;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class CommentTaskImpl implements CommentTask {

    private final CommentService commentService;

    private final CommentRepository commentRepository;

    @Override
    @Scheduled(cron = "0 0 0 * * ?") // Runs every day at midnight
//    @Scheduled(cron = "0 * * * * *") // Runs every minute
    public void filterComments() {
        List<Comment> comments = commentRepository
                .findAllByIsFiltered(false)
                .stream().peek(comment -> comment.setIsFiltered(true))
                .toList();

        List<Comment> negativeComments = comments.stream()
                .filter(comment -> commentService.isNegativeComment(comment.getContent()))
                .peek(comment -> comment.setIsNegative(true))
                .collect(Collectors.toList());
        log.info("{ Negative comments: {} }", negativeComments.size());

        commentRepository.saveAll(negativeComments);
        commentRepository.saveAll(comments);
        log.info("{ Negative comments updated successfully: {} }", negativeComments.size());
    }
}
