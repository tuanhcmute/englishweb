package com.englishweb.api.tasks.impl;

import com.englishweb.api.enums.DiscountStatus;
import com.englishweb.api.models.Course;
import com.englishweb.api.models.Discount;
import com.englishweb.api.services.repository.CourseRepository;
import com.englishweb.api.services.repository.DiscountRepository;
import com.englishweb.api.tasks.DiscountTaskScheduler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
@RequiredArgsConstructor
@Slf4j
public class DiscountTaskSchedulerImpl implements DiscountTaskScheduler {
    private final DiscountRepository discountRepository;
    private final CourseRepository courseRepository;
    private final ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Override
    public void deactivateDiscountAfterEndDate(Discount discount) {
        if (Objects.isNull(discount)) {
            throw new IllegalArgumentException("Discount cannot be null");
        }
        Date endDate = Date.from(discount.getEndDate().atZone(ZoneId.systemDefault()).toInstant());
        threadPoolTaskScheduler.schedule(() -> {
//            Update discount
            discount.setStatus(DiscountStatus.TERMINATED.getValue());
            discountRepository.save(discount);

            // Get all discount courses
//            List<Course> courses = new ArrayList<>();
//            Set<String> processedCourseIds = new HashSet<>();
//
//            discount.getDiscountConditions().forEach(discountCondition ->
//                    discountCondition.getConditionTargets().forEach(conditionTarget -> {
//                        if (DiscountConditionType.COURSE.getValue().equals(discountCondition.getType())) {
//                            handleCourseCondition(courses, processedCourseIds, conditionTarget.getCourse());
//                        } else if (conditionTarget.getCourseCategory() != null) {
//                            conditionTarget.getCourseCategory().getCourses().forEach(course -> {
//                                if (!processedCourseIds.contains(course.getId())) {
//                                    handleCourseCondition(courses, processedCourseIds, course);
//                                }
//                            });
//                        }
//                    }));
////            Update all
//            courseRepository.saveAll(courses);
//            log.info("Updated " + courses.size() + " courses with discount reset.");
        }, endDate);
    }

    private void handleCourseCondition(List<Course> courses, Set<String> processedCourseIds, Course course) {
        if (Objects.nonNull(course) && processedCourseIds.add(course.getId())) {
            Integer oldPrice = course.getOldPrice();
            course.setIsDiscount(false);
            course.setPercentDiscount(0);
            course.setNewPrice(oldPrice);
            courses.add(course);
        }
    }

    @Override
    @Async
    public void updateDiscountStatus() {
        List<Discount> discounts = discountRepository.findAll(Sort.by(
                Sort.Direction.DESC, "lastModifiedDate"
        ));

        discounts.forEach(discount -> {
//            Check between

//            Check scheduled

//            Check terminated

//            threadPoolTaskScheduler.schedule(() -> {
//
//            })
        });
    }
}
