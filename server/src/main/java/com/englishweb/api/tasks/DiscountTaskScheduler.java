package com.englishweb.api.tasks;

import com.englishweb.api.models.Discount;

public interface DiscountTaskScheduler {
    void deactivateDiscountAfterEndDate(Discount discount);

    void updateDiscountStatus();
}
