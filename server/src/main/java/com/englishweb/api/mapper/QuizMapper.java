package com.englishweb.api.mapper;

import com.englishweb.api.dto.QuizDTO;
import com.englishweb.api.models.Quiz;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuizMapper {
    QuizDTO toDTO(Quiz quiz);

    List<QuizDTO> toListDTO(List<Quiz> quizzes);
}
