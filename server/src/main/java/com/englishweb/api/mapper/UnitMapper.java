package com.englishweb.api.mapper;

import com.englishweb.api.dto.UnitDTO;
import com.englishweb.api.models.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {LessonMapper.class})
public interface UnitMapper {

    @Mappings({
            @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    UnitDTO toDTO(Unit unit);

    List<UnitDTO> toListDTO(List<Unit> units);
}
