package com.englishweb.api.mapper;

import com.englishweb.api.dto.CourseCategoryDTO;
import com.englishweb.api.models.CourseCategory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourseCategoryMapper {

    @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    CourseCategoryDTO toDTO(CourseCategory courseCategory);

    List<CourseCategoryDTO> toListDTO(List<CourseCategory> courseCategories);
}
