package com.englishweb.api.mapper;

import com.englishweb.api.dto.AgentVersionDTO;
import com.englishweb.api.models.AgentVersion;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AgentVersionMapper {

    AgentVersionDTO toDTO(AgentVersion agentVersion);
}
