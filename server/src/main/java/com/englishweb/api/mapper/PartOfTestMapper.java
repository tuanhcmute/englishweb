package com.englishweb.api.mapper;

import com.englishweb.api.dto.FullPartOfTestDTO;
import com.englishweb.api.dto.PartOfTestDTO;
import com.englishweb.api.models.PartOfTest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {PartInformationMapper.class})
public interface PartOfTestMapper {
    PartOfTestDTO toDTO(PartOfTest partOfTest);

    @Mappings(@Mapping(source = "listOfQuestionGroups", target = "questionGroups"))
    FullPartOfTestDTO toFullPartOfTestDTO(PartOfTest partOfTest);

    List<PartOfTestDTO> toListDTO(List<PartOfTest> partOfTests);
}
