package com.englishweb.api.mapper;

import com.englishweb.api.dto.QuizAnswerDTO;
import com.englishweb.api.models.QuizAnswer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public
interface QuizAnswerMapper {
    QuizAnswerDTO toDTO(QuizAnswer quizAnswer);

    List<QuizAnswerDTO> toListDTO(List<QuizAnswer> quizAnswers);
}
