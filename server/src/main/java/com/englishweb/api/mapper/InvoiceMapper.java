package com.englishweb.api.mapper;

import com.englishweb.api.dto.InvoiceDTO;
import com.englishweb.api.models.Invoice;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface InvoiceMapper {
    @Mappings({
            @Mapping(target = "createdDate", source = "invoice.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
    })
    InvoiceDTO toDTO(Invoice invoice);

    List<InvoiceDTO> toListDTO(List<Invoice> invoices);
}
