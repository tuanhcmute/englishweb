package com.englishweb.api.mapper;

import com.englishweb.api.dto.QuizCategoryDTO;
import com.englishweb.api.models.QuizCategory;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuizCategoryMapper {
    QuizCategoryDTO toDTO(QuizCategory quizCategory);

    List<QuizCategoryDTO> toListDTO(List<QuizCategory> quizCategories);
}
