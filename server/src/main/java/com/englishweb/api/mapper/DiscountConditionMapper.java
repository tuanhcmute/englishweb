package com.englishweb.api.mapper;

import com.englishweb.api.dto.DiscountConditionDTO;
import com.englishweb.api.models.DiscountCondition;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ConditionTargetMapper.class})
public interface DiscountConditionMapper {
    DiscountConditionDTO toDTO(DiscountCondition discountCondition);

    List<DiscountConditionDTO> toListDTO(List<DiscountCondition> discountConditions);
}
