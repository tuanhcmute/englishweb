package com.englishweb.api.mapper;

import com.englishweb.api.dto.FullOrderDTO;
import com.englishweb.api.dto.OrderDTO;
import com.englishweb.api.models.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {InvoiceMapper.class, OrderLineMapper.class, UserCredentialMapper.class})
public interface OrderMapper {

    @Mappings({
            @Mapping(target = "orderDate", source = "order.orderDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
    })
    FullOrderDTO toFullDTO(Order order);

    @Mappings({
            @Mapping(target = "orderDate", source = "order.orderDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
    })
    OrderDTO toDTO(Order order);

    List<FullOrderDTO> toFullListDTO(List<Order> orders);

    List<OrderDTO> toListDTO(List<Order> orders);
}
