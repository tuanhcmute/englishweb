package com.englishweb.api.mapper;

import com.englishweb.api.dto.FlashcardDTO;
import com.englishweb.api.dto.FullFlashcardDTO;
import com.englishweb.api.models.Flashcard;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserCredentialMapper.class})
public interface FlashcardMapper {
    FlashcardDTO toDTO(Flashcard flashcard);

    FullFlashcardDTO toFullDTO(Flashcard flashcard);

    List<FlashcardDTO> toListDTO(List<Flashcard> flashcards);
}
