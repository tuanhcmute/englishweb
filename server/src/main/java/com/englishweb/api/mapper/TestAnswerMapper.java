package com.englishweb.api.mapper;

import com.englishweb.api.dto.TestAnswerDTO;
import com.englishweb.api.models.TestAnswer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TestAnswerMapper {
    @Mapping(target = "isCorrect", source = "isCorrect", qualifiedByName = "getIsCorrect")
    TestAnswerDTO toDTO(TestAnswer testAnswer);

    @Named("getIsCorrect")
    default Boolean getIsCorrect(Boolean x) {
        return x;
    }

    List<TestAnswerDTO> toListAnswerDTO(List<TestAnswer> testAnswers);
}
