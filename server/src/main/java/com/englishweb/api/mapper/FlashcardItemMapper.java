package com.englishweb.api.mapper;

import com.englishweb.api.dto.FlashcardItemDTO;
import com.englishweb.api.models.FlashcardItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FlashcardItemMapper {
    @Mappings({
            @Mapping(target = "createdDate", source = "flashcardItem.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "flashcardItem.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    FlashcardItemDTO toDTO(FlashcardItem flashcardItem);

    List<FlashcardItemDTO> toListDTO(List<FlashcardItem> flashcardItems);
}
