package com.englishweb.api.mapper;

import com.englishweb.api.dto.CourseDetailDTO;
import com.englishweb.api.models.CourseDetail;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CourseMapper.class})
public interface CourseDetailMapper {
    CourseDetailDTO toDTO(CourseDetail courseDetail);

    List<CourseDetailDTO> toListDTO(List<CourseDetail> courseDetails);
}
