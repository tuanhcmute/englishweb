package com.englishweb.api.mapper;

import com.englishweb.api.dto.ConditionTargetDTO;
import com.englishweb.api.models.ConditionTarget;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CourseCategoryMapper.class})
public interface ConditionTargetMapper {
    ConditionTargetDTO toDTO(ConditionTarget conditionTarget);

    List<ConditionTargetDTO> toListDTO(List<ConditionTargetDTO> conditionTargets);
}
