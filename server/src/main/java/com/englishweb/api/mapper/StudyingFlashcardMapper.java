package com.englishweb.api.mapper;

import com.englishweb.api.dto.StudyingFlashcardDTO;
import com.englishweb.api.models.StudyingFlashcard;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserCredentialMapper.class, FlashcardMapper.class})
public interface StudyingFlashcardMapper {
    StudyingFlashcardDTO toDTO(StudyingFlashcard studyingFlashcard);

    List<StudyingFlashcardDTO> toListDTO(List<StudyingFlashcard> studyingFlashcards);
}
