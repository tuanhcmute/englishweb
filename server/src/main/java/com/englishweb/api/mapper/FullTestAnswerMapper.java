package com.englishweb.api.mapper;

import com.englishweb.api.dto.FullTestAnswerDTO;
import com.englishweb.api.models.FullTestAnswer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FullTestAnswerMapper {
    FullTestAnswerDTO toDTO(FullTestAnswer fullTestAnswer);

    List<FullTestAnswerDTO> toListDTO(List<FullTestAnswer> fullTestAnswers);
}
