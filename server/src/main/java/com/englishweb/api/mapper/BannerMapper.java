package com.englishweb.api.mapper;

import com.englishweb.api.dto.BannerDTO;
import com.englishweb.api.models.Banner;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BannerMapper {
    @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    BannerDTO toDTO(Banner banner);

    List<BannerDTO> toListDTO(List<Banner> banners);
}
