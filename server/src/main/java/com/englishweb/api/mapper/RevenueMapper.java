package com.englishweb.api.mapper;

import com.englishweb.api.dto.RevenueDTO;
import com.englishweb.api.models.Revenue;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RevenueMapper {
    @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy")
    @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy")
    RevenueDTO toDTO(Revenue revenue);

    List<RevenueDTO> toListDTO(List<Revenue> revenues);
}
