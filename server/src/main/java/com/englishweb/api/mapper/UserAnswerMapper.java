package com.englishweb.api.mapper;

import com.englishweb.api.dto.UserAnswerDTO;
import com.englishweb.api.models.UserAnswer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TestMapper.class})
public interface UserAnswerMapper {

    @Mappings({
            @Mapping(target = "testDate", source = "userAnswer.testDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
    })
    UserAnswerDTO toDTO(UserAnswer userAnswer);

    List<UserAnswerDTO> toListDTO(List<UserAnswer> userAnswers);
}
