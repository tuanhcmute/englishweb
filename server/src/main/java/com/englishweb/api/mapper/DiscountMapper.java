package com.englishweb.api.mapper;

import com.englishweb.api.dto.DiscountDTO;
import com.englishweb.api.models.Discount;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DiscountMapper {
    @Mappings({
            @Mapping(target = "startDate", source = "discount.startDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "endDate", source = "discount.endDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "createdDate", source = "discount.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "discount.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    DiscountDTO toDTO(Discount discount);

    List<DiscountDTO> toListDTO(List<Discount> discounts);
}
