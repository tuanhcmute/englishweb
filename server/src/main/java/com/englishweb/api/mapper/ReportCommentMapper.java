package com.englishweb.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.englishweb.api.dto.ReportCommentDTO;
import com.englishweb.api.models.ReportComment;

@Mapper(componentModel = "spring")
public interface ReportCommentMapper {
    ReportCommentDTO toDTO(ReportComment reportComment);

    List<ReportCommentDTO> toListDTO(List<ReportComment> reportComments); 
}
