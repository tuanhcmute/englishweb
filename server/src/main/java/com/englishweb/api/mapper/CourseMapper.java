package com.englishweb.api.mapper;


import com.englishweb.api.dto.CourseDTO;
import com.englishweb.api.models.Course;
import com.englishweb.api.models.Review;
import com.englishweb.api.models.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {ReviewMapper.class, CourseCategoryMapper.class, DiscountMapper.class})
public interface CourseMapper {

    @Mappings({
            @Mapping(source = "reviews", target = "countReviews", qualifiedByName = "mapCountReviews"),
            @Mapping(source = "units", target = "countUnits", qualifiedByName = "mapCountUnits"),
            @Mapping(target = "countLessons", expression = "java(mapCountLessons(course))"),
            @Mapping(target = "overallRating", expression = "java(mapOverallRating(course))")
    })
    CourseDTO toDTO(Course course);

    List<CourseDTO> toListDTO(List<Course> courses);

    default Integer mapCountUnits(Set<Unit> units) {
        if(Objects.isNull(units))  return 0;
        return units.size();
    }

    default Integer mapCountReviews(Set<Review> reviews) {
        if(Objects.isNull(reviews))  return 0;
        return reviews.stream().filter(Review::getIsPublic).toList().size();
    }

    default Integer mapCountLessons(Course course) {
        return course.getUnits().stream()
                .mapToInt(unit -> unit.getLessons().size())
                .sum();
    }

    default double mapOverallRating(Course course) {
        if(Objects.isNull(course.getReviews())) return 0;
        if(course.getReviews().isEmpty()) return 0;
        double overallRating = 0;
        int maxStar = 5;
        for(int i = 0; i < maxStar; i++) {
            int finalI = i + 1;
            long count = course.getReviews().stream().filter(review -> review.getRating() == (finalI) && review.getIsPublic()).count();
           overallRating += (count * finalI);
        }
        int totalRating = course.getReviews().stream().filter(Review::getIsPublic).toList().size();
        return overallRating / totalRating;
    }
}
