package com.englishweb.api.mapper;

import com.englishweb.api.dto.UserCredentialDTO;
import com.englishweb.api.models.UserCredential;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface UserCredentialMapper {
    @Mapping(target = "dob", source = "dob", dateFormat = "yyyy-MM-dd")
    UserCredentialDTO toDTO(UserCredential userCredential);

    List<UserCredentialDTO> toListDTO(List<UserCredential> userCredentials);
}
