package com.englishweb.api.mapper;

import com.englishweb.api.dto.RoleDTO;
import com.englishweb.api.models.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    @Mapping(target = "roleName", source = "roleName")
    RoleDTO toDTO(Role role);

    List<RoleDTO> toListDTO(List<Role> roles);
}
