package com.englishweb.api.mapper;

import com.englishweb.api.dto.FullNewsDTO;
import com.englishweb.api.dto.NewsDTO;
import com.englishweb.api.models.News;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {NewsCategoryMapper.class, UserCredentialMapper.class})
public interface NewsMapper {
    @Mappings({
            @Mapping(target = "createdDate", source = "news.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "news.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    NewsDTO toDTO(News news);

    @Mappings({
            @Mapping(target = "createdDate", source = "news.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "news.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    FullNewsDTO toFullDTO(News news);

    List<NewsDTO> toListDTO(List<News> newsList);
}
