package com.englishweb.api.mapper;

import com.englishweb.api.dto.QuestionDTO;
import com.englishweb.api.models.Question;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionMapper {
    QuestionDTO toDTO(Question question);

    List<QuestionDTO> toListDTO(List<Question> questions);
}
