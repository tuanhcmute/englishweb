package com.englishweb.api.mapper;


import com.englishweb.api.dto.CommentDTO;
import com.englishweb.api.models.Comment;
import com.englishweb.api.models.ReportComment;
import com.englishweb.api.models.SubComment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    @Mapping(source = "subComments", target = "hasSubComment", qualifiedByName = "mapHasSubComment")
    @Mapping(source = "subComments", target = "countSubComment", qualifiedByName = "mapCountSubComments")
    @Mapping(source = "reportComments", target = "countReportComments", qualifiedByName = "mapCountReportComments")
    @Mapping(target = "createdDate", source = "comment.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    @Mapping(target = "lastModifiedDate", source = "comment.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    CommentDTO toDTO(Comment comment);

    List<CommentDTO> toListDTO(List<Comment> comments);

    default Boolean mapHasSubComment(Set<SubComment> subComments) {
        return subComments.size() > 0;
    }

    default Integer mapCountSubComments(Set<SubComment> subComments) {
        return subComments.size();
    }

    default Integer mapCountReportComments(Set<ReportComment> reportComments) {
        return reportComments.size();
    }

}
