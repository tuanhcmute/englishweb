package com.englishweb.api.mapper;

import com.englishweb.api.dto.LessonNoteDTO;
import com.englishweb.api.models.LessonNote;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {LessonMapper.class})
public interface LessonNoteMapper {
    @Mappings({
            @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    LessonNoteDTO toDTO(LessonNote lessonNote);

    List<LessonNoteDTO> toListDTO(List<LessonNote> lessonNotes);
}
