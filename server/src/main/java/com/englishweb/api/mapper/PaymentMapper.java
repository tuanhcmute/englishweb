package com.englishweb.api.mapper;

import com.englishweb.api.dto.PaymentDTO;
import com.englishweb.api.models.Payment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {OrderMapper.class})
public interface PaymentMapper {

    @Mappings({
            @Mapping(target = "createdDate", source = "payment.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
    })
    PaymentDTO toDTO(Payment payment);

    List<PaymentDTO> toListDTO(List<Payment> payments);
}
