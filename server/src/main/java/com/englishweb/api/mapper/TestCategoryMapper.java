package com.englishweb.api.mapper;

import com.englishweb.api.dto.TestCategoryDTO;
import com.englishweb.api.models.TestCategory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TestCategoryMapper {

    @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    TestCategoryDTO toDTO(TestCategory testCategory);

    List<TestCategoryDTO> toListDTO(List<TestCategory> testCategories);
}
