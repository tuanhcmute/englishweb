package com.englishweb.api.mapper;

import com.englishweb.api.dto.InvoiceLineDTO;
import com.englishweb.api.models.InvoiceLine;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface InvoiceLineMapper {
    InvoiceLineDTO toDTO(InvoiceLine invoiceLine);

    List<InvoiceLineDTO> toListDTO(List<InvoiceLine> invoiceLines);
}
