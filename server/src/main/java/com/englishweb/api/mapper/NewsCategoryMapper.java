package com.englishweb.api.mapper;

import com.englishweb.api.dto.NewsCategoryDTO;
import com.englishweb.api.models.NewsCategory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NewsCategoryMapper {
    @Mappings({
            @Mapping(target = "createdDate", source = "newsCategory.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "newsCategory.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    NewsCategoryDTO toDTO(NewsCategory newsCategory);

    List<NewsCategoryDTO> toListDTO(List<NewsCategory> newsCategories);
}
