package com.englishweb.api.mapper;

import com.englishweb.api.dto.FullPartInformationDTO;
import com.englishweb.api.dto.PartInformationDTO;
import com.englishweb.api.models.PartInformation;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TestCategoryMapper.class})
public interface PartInformationMapper {
    FullPartInformationDTO toFullDTO(PartInformation partInformation);

    PartInformationDTO toDTO(PartInformation partInformation);

    List<FullPartInformationDTO> toListDTO(List<PartInformation> listPartInformation);
}
