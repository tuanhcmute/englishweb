package com.englishweb.api.mapper;

import com.englishweb.api.dto.QuestionGroupDTO;
import com.englishweb.api.models.QuestionGroup;
import com.englishweb.api.request.QuestionGroupRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionGroupMapper {
    QuestionGroup requestToModel(QuestionGroupRequest request);

    List<QuestionGroupDTO> toListDTO(List<QuestionGroup> questionGroups);

    @Mappings({
            @Mapping(target = "createdDate", source = "questionGroup.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "questionGroup.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    QuestionGroupDTO toDTO(QuestionGroup questionGroup);

    default String mapFileToString(MultipartFile file) {
        return file.getName();
    }

}
