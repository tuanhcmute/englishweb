package com.englishweb.api.mapper;

import com.englishweb.api.dto.LessonDTO;
import com.englishweb.api.models.Lesson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    @Mappings({
            @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    LessonDTO toDTO(Lesson lesson);

    List<LessonDTO> toListDTO(List<Lesson> lessons);
}
