package com.englishweb.api.mapper;

import com.englishweb.api.dto.OrderLineDTO;
import com.englishweb.api.models.OrderLine;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderLineMapper {

    OrderLineDTO toDTO(OrderLine orderLine);

    List<OrderLineDTO> toListDTO(List<OrderLine> orderLines);
}
