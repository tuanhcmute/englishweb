package com.englishweb.api.mapper;

import com.englishweb.api.dto.FullTestDTO;
import com.englishweb.api.dto.TestDTO;
import com.englishweb.api.models.Comment;
import com.englishweb.api.models.Test;
import com.englishweb.api.models.UserAnswer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {PartOfTestMapper.class, TestCategoryMapper.class})
public interface TestMapper {
    @Mappings({
            @Mapping(target = "createdDate", source = "test.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "test.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(source = "userAnswers", target = "countUserAnswers", qualifiedByName = "mapCountUserAnswers"),
            @Mapping(source = "testReview", target = "countTestComments", qualifiedByName = "mapCountTestComments")
    })
    TestDTO toDTO(Test test);

    FullTestDTO toFullTestDTO(Test test);

    List<TestDTO> toListDTO(List<Test> tests);

    default Integer mapCountUserAnswers(Set<UserAnswer> userAnswers) {
        if(userAnswers == null) return 0;
        return userAnswers.size();
    }
    default Integer mapCountTestComments(Set<Comment> comments) {
        if(Objects.isNull(comments)) return 0;
        return comments.size();
    }
}
