package com.englishweb.api.mapper;

import com.englishweb.api.dto.ReviewDTO;
import com.englishweb.api.models.Review;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReviewMapper {

    @Mappings({
            @Mapping(target = "createdDate", source = "createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "courseId", expression = "java(mapCourseId(review))"),
            @Mapping(target = "courseName", expression = "java(mapCourseName(review))")
    })
    ReviewDTO toDTO(Review review);

    List<ReviewDTO> toListDTO(List<Review> reviews);

    default String mapCourseId(Review review) {
        return review.getCourse().getId();
    }

    default String mapCourseName(Review review) {
        return review.getCourse().getCourseName();
    }
}
