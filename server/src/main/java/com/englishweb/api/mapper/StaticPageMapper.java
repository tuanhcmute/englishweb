package com.englishweb.api.mapper;

import com.englishweb.api.dto.StaticPageDTO;
import com.englishweb.api.models.StaticPage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StaticPageMapper {
    @Mappings({
            @Mapping(target = "createdDate", source = "staticPage.createdDate", dateFormat = "dd-MM-yyyy HH:mm:ss"),
            @Mapping(target = "lastModifiedDate", source = "staticPage.lastModifiedDate", dateFormat = "dd-MM-yyyy HH:mm:ss")
    })
    StaticPageDTO toDTO(StaticPage staticPage);

    List<StaticPageDTO> toListDTO(List<StaticPage> staticPages);
}
