package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateCourseRequest {

    @NotBlank(message = "CourseName could not be found")
    private String courseName;

    @NotBlank(message = "CourseCategoryId could not be found")
    private String courseCategoryId;

    @NotNull(message = "OldPrice could not be found")
    private Integer oldPrice;

    @NotBlank(message = "CourseImg could not be found")
    private String courseImg;

    @NotNull(message = "IsActive could not be found")
    private Boolean isActive;
}
