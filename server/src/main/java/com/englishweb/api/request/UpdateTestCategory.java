package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateTestCategory {

    @NotBlank(message = "Id can not null")
    private String id;

    @NotNull(message = "NumberOfPart can not null")
    private Integer numberOfPart;

    @NotNull(message = "TotalQuestion can not null")
    private Integer totalQuestion;

    @NotNull(message = "TotalTime can not null")
    private Integer totalTime;

    @NotBlank(message = "testCategoryName can not null")
    private String testCategoryName;
}
