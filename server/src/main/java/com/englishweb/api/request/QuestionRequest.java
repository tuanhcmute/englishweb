package com.englishweb.api.request;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class QuestionRequest {
    private String questionContent;
    private Integer questionNumber;
    //    private String rightAnswer;
    private String questionGuide;
    private String questionGroupId;
    //    private MultipartFile questionImage;
    private List<CreateTestAnswerRequest> testAnswers;
}
