package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UpdateTestRequest {

    @NotBlank(message = "id could not be blank")
    private String id;

    @NotBlank(message = "testName could not be blank")
    private String testName;

    @NotBlank(message = "testDescription could not be blank")
    private String testDescription;

    @NotBlank(message = "thumbnail could not be blank")
    private String thumbnail;
}
