package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateReviewRequest {
    @NotBlank
    private String id;
    private String content;
    private Integer rating;
    private Boolean isPublic;
    private String image;
}
