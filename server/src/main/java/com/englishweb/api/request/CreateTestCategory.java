package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateTestCategory {
    private Integer numberOfPart;

    private Integer totalQuestion;

    private Integer totalTime;

    private String testCategoryName;
}
