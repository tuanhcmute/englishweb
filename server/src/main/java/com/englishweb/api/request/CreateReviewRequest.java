package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateReviewRequest {
    private String userCredentialId;
    private String content;
    private String image;
    private Integer rating;
    private String courseId;
}
