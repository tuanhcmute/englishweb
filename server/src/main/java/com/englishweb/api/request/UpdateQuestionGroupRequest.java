package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateQuestionGroupRequest {
    private String id;
    private String image;
    private String questionContentEn;
    private String questionContentTranscript;
}
