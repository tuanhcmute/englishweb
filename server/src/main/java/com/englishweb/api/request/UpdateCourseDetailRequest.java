package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateCourseDetailRequest {
    private String courseIntroduction;

    private String courseTarget;

    private String courseInformation;

    private String courseGuide;

    private String courseId;
    private String courseDetailId;
}
