package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUnitRequest {
    private String id;
    private String title;
}
