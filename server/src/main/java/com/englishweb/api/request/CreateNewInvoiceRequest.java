package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateNewInvoiceRequest {
    private String paymentId;
    private String orderId;
}
