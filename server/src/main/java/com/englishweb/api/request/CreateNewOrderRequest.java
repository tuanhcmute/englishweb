package com.englishweb.api.request;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateNewOrderRequest {
    private LocalDateTime orderDate;
    private Boolean paymentStatus;
}
