package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateLessonNoteRequest {

    @NotBlank(message = "LessonId can not null")
    private String lessonId;

    @NotBlank(message = "UserCredentialId can not null")
    private String userCredentialId;

    @NotBlank(message = "Content can not null")
    private String content;

    @NotNull(message = "NoteTime can not null")
    private Double noteTime;
}
