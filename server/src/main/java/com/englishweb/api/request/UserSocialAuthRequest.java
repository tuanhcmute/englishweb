package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserSocialAuthRequest {
    private String avatar;
    private String email;
    private String name;
    private String provider;
    private String username;
}
