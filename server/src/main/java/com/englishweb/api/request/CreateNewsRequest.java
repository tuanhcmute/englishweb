package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateNewsRequest {
    private String newsTitle;
    private String newsDescription;
    private String newsImage;
    private String newsCategoryId;
    private String status;
    private String userCredentialId;
}
