package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateAgentVersionRequest {
    private Boolean is_active;
    private String skilled_agent_version_id;
}
