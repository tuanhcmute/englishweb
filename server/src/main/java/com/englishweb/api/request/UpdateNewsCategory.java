package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateNewsCategory {

    @NotBlank(message = "Id can not blank")
    private String id;

    @NotBlank(message = "NewsCategoryName can not blank")
    private String newsCategoryName;

    @NotBlank(message = "Status can not blank")
    private String status;
}
