package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateUserRoleRequest {

    //    @NotBlank(message = "Role id must not be blank")
    private String roleId;
}
