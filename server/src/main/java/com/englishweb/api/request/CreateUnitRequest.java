package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateUnitRequest {

    @NotBlank(message = "Title is could not be blank")
    private String title;

    @NotBlank(message = "CourseId is could not be blank")
    private String courseId;
}
