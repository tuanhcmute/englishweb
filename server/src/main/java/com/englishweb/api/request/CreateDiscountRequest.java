package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateDiscountRequest {

    @NotBlank(message = "DiscountName could not be found")
    private String discountName;

    @NotBlank(message = "StartDate could not be found")
    private String startDate;

    @NotBlank(message = "EndDate could not be found")
    private String endDate;

    @NotBlank(message = "Status could not be found")
    private String status;
}
