package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateNewInvoiceLineRequest {
    private Integer quantity;
    private Integer unitPrice;
    private Integer discount;
    private Integer totalPrice;
}
