package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateNewUserAnswer {
    private String testId;
    private String userCredentialId;
    private String results;
    private String practiceType;
    private Integer completionTime;
}
