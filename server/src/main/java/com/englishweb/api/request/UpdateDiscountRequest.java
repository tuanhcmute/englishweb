package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateDiscountRequest {
    private String id;

    private String discountName;

    private String startDate;

    private String endDate;

    private String status;

}

