package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CreateTestRequest {
    @NotBlank(message = "testCategoryId could not be blank")
    private String testCategoryId;

    @NotBlank(message = "testName could not be blank")
    private String testName;

    @NotBlank(message = "testDescription could not be blank")
    private String testDescription;

    @NotNull(message = "numberOfPart could not be blank")
    private Integer numberOfPart;

    @NotNull(message = "numberOfQuestion could not be blank")
    private Integer numberOfQuestion;

    @NotBlank(message = "thumbnail could not be blank")
    private String thumbnail;
}
