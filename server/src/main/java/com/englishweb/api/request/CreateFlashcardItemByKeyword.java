package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateFlashcardItemByKeyword {

    @NotBlank(message = "FlashcardId could not be found")
    private String flashcardId;

    @NotBlank(message = "Keyword could not be found")
    private String keyword;

    @NotBlank(message = "Limit could not be found")
    private Integer limit;
}
