package com.englishweb.api.request;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateLessonRequest {
    private String id;
    private String title;
    private String content;
    private String video;
}
