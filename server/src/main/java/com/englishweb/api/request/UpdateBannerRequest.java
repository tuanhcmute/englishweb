package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateBannerRequest {

    @NotBlank(message = "id could not be blank")
    private String id;

    @NotBlank(message = "image could not be blank")
    private String image;
}
