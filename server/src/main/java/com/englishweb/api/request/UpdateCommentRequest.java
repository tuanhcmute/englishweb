package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCommentRequest {

    @NotBlank(message = "id could not be blank")
    private String id;

    @NotBlank(message = "content could not be blank")
    private String content;

    @NotNull(message = "isNegative could not be null")
    private Boolean isNegative;

    @NotNull(message = "isReview could not be null")
    private Boolean isReview;
}
