package com.englishweb.api.request;


import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateCommentRequest {

    @NotBlank(message = "Content could not be found")
    private String content;

    @NotBlank(message = "TestId could not be found")
    private String testId;

    @NotBlank(message = "AuthorId could not be found")
    private String authorId;

    @NotBlank(message = "ParentCommentId could not be found")
    private String parentCommentId;
}
