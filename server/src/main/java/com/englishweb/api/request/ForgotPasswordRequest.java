package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ForgotPasswordRequest {
    private String email;
    private String newPassword;
    private String confirmPassword;
    private String otp;
}
