package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UploadFlashcardItemImageRequest {
    @NotBlank
    private String image;
}
