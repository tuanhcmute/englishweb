package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserInfoRequest {
    private String id;
    private String fullName;
    private String phoneNumber;
    private String dob;
    private String address;
    private String gender;
    private String major;
}
