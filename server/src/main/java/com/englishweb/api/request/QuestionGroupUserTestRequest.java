package com.englishweb.api.request;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionGroupUserTestRequest {
    private String questionGroupId;
    private List<QuestionUserTestRequest> questions;
}
