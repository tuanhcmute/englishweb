package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateNewReportCommentRequest {

    @NotBlank(message = "reportContent could not be blank")
    private String reportContent;

    @NotBlank(message = "commentId could not be blank")
    private String commentId;

    @NotBlank(message = "userCredentialId could not be blank")
    private String userCredentialId;
}
