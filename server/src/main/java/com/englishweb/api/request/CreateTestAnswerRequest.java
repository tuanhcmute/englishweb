package com.englishweb.api.request;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CreateTestAnswerRequest {
    private String answerContent;
    private String answerChoice;
    private String answerTranslate;
    private Boolean isCorrect;
}
