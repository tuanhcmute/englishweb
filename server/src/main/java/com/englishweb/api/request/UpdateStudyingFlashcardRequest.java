package com.englishweb.api.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateStudyingFlashcardRequest {
    private String id;
    private String userCredentialId;
    private String flashcardId;
    private String status;
}

