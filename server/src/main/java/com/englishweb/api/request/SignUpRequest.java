package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignUpRequest {
    private String username;

    private String password;

    private String confirmPassword;

    @Email
    private String email;

    @Pattern(regexp = "(^$|[0-9]{10})")
    private String phoneNumber;

    private String fullName;

    private String provider;

    private String avatar;
}
