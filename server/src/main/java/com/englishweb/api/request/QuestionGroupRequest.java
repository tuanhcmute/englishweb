package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QuestionGroupRequest {
    private String image;
    private String questionContentEn;
    private String questionContentTranscript;
    private String partOfTestId;
}
