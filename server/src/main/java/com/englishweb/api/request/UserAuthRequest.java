package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserAuthRequest {
    @NotNull
    @Email
    private String username;

    @NotNull
    private String password;
}
