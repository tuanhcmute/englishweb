package com.englishweb.api.request;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserTestResultRequest {
    private String partId;
    private List<QuestionGroupUserTestRequest> questionGroups;
}
