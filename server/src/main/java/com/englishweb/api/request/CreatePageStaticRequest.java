package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePageStaticRequest {
    private String pageCode;

    private String pageName;

    private String pageContent;

    private String userCredentialId;
}
