package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePartInformation {
    private String partInformationName;

    private Integer totalQuestion;

    private Integer partSequenceNumber;

    private String partInformationDescription;

    private String testCategoryId;

    private String partType;
}
