package com.englishweb.api.request;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CreatePaymentRequest {
    private String paymentImage;
    private LocalDateTime time;
    private Integer votes;
}
