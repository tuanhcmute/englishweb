package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateLessonNoteRequest {
    @NotBlank(message = "Id can not null")
    private String id;
    @NotBlank(message = "Content can not null")
    private String content;
}
