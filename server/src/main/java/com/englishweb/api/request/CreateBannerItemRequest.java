package com.englishweb.api.request;

import com.englishweb.api.models.Banner;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateBannerItemRequest {

    @NotBlank(message = "image could not be blank")
    private String image;

    public Banner buildBanner() {
        return Banner.builder()
                .image(this.image)
                .build();
    }
}
