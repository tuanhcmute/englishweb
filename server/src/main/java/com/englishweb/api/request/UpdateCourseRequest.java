package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateCourseRequest {
    private String id;

    private String courseName;

    private Integer oldPrice;

    private Boolean isActive;

    private String courseImg;
}
