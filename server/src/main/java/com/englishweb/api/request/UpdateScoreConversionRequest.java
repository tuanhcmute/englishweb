package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateScoreConversionRequest {
    @NotBlank(message = "id could not be blank")
    private String id;

    @NotBlank(message = "jsonScore could not be blank")
    private String jsonScore;
}
