package com.englishweb.api.request;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreatePayment {
    private String courseId;
    private String userCredentialId;
    private Integer totalAmount;
    private String paymentMethod;
    private String baseUrl;
    private String failureUrl;
    private String successUrl;
}
