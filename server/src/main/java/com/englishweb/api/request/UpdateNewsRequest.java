package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateNewsRequest {
    private String id;
    private String newsDescription;
    private String newsImage;
    private String status;
    private String newsTitle;
    private String newsContentHtml;
}
