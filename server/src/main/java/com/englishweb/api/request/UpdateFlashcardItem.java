package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateFlashcardItem {
    private String word;

    private String phonetic;

    private String meaning;

    private String tags;

    private String examples;

    private String flashcardItemId;
}
