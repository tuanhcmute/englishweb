package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateFlashcardItemRequest {

    @NotBlank(message = "word could not be found")
    private String word;

    @NotBlank(message = "Phonetic could not be found")
    private String phonetic;

    @NotBlank(message = "Meaning could not be found")
    private String meaning;

    @NotBlank(message = "Tags could not be found")
    private String tags;

    @NotBlank(message = "Audio could not be found")
    private String audio;
}
