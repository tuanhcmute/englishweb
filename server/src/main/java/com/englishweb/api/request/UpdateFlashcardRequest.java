package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateFlashcardRequest {
    private String id;
    private String flashcardTitle;
    private String flashcardDescription;
}
