package com.englishweb.api.request;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SendOTPRequest {
    private String emailTo;
}
