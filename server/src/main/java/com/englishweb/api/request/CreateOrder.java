package com.englishweb.api.request;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateOrder {
    private String userCredentialId;
    private String courseId;
}
