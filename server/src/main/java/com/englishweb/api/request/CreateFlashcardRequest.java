package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateFlashcardRequest {

    private String flashcardTitle;
    private String flashcardDescription;
    private Integer totalFlashcardItem;
    private String userCredentialId;
    private String flashcardType;
}
