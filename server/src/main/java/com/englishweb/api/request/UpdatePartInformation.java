package com.englishweb.api.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePartInformation {
    private String id;

    private String partInformationName;

    private Integer totalQuestion;

    private Integer partSequenceNumber;

    private String partInformationDescription;

    private String partType;
}
