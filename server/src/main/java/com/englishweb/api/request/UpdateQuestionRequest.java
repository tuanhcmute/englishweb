package com.englishweb.api.request;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateQuestionRequest {
    private String id;
    private String questionContent;
    private Integer questionNumber;
    private String questionGuide;
    private String rightAnswer;
    private List<UpdateTestAnswerRequest> testAnswers;
}
