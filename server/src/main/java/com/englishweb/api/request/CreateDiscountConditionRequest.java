package com.englishweb.api.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateDiscountConditionRequest {
    @NotBlank(message = "Type could not be blanked")
    private String type;

    @NotNull(message = "Percent discount could not be null")
    private Integer percentDiscount;

    @Builder.Default
    private List<String> courseIds = new ArrayList<>();

    @Builder.Default
    private List<String> categoryIds = new ArrayList<>();
}
