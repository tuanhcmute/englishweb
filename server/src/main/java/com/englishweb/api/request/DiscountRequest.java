package com.englishweb.api.request;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DiscountRequest {
    private String discountId;
    @Builder.Default
    private List<CreateDiscountConditionRequest> conditions = new ArrayList<>();
}
