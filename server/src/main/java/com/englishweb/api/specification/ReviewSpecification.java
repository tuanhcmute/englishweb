package com.englishweb.api.specification;

import com.englishweb.api.models.Review;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface ReviewSpecification {
    static Specification<Review> hasCourse(String courseId) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(courseId)) {
                Join<Object, Object> userCredentialJoin = root.join("course");
                return criteriaBuilder.equal(userCredentialJoin.get("id"), courseId);
            }
            return criteriaBuilder.conjunction();
        };
    }

    static Specification<Review> hasPublic(String hasPublic) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(hasPublic)) {
                boolean isPublic = hasPublic.equals("1");
                return criteriaBuilder.equal(root.get("isPublic"), isPublic);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
