package com.englishweb.api.specification;

import com.englishweb.api.models.Order;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;

public interface OrderSpecification {
    static Specification<Order> hasPaymentStatus(String paymentStatus) {

        return (root, query, criteriaBuilder) -> {
            if (paymentStatus != null) {
                Boolean isPaid = paymentStatus.equals("1");
                return criteriaBuilder.equal(root.get("paymentStatus"), isPaid);
            }
            return criteriaBuilder.conjunction();
        };
    }

    static Specification<Order> hasUserCredential(String userCredentialId) {
        return (root, query, criteriaBuilder) -> {
            if (userCredentialId != null) {
                Join<Object, Object> userCredentialJoin = root.join("userCredential");
                return criteriaBuilder.equal(userCredentialJoin.get("id"), userCredentialId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
