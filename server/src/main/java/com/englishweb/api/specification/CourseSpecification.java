package com.englishweb.api.specification;

import com.englishweb.api.models.Course;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

public interface CourseSpecification {
    static Specification<Course> hasActivated(String isActive) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(isActive)) {
                Boolean isActiveBool = isActive.equals("1");
                return criteriaBuilder.equal(root.get("isActive"), isActiveBool);
            }
            return criteriaBuilder.conjunction();
        };
    }

}
