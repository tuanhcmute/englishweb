package com.englishweb.api.specification;

import com.englishweb.api.models.Comment;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface CommentSpecification {
    static Specification<Comment> hasTest(String testId) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(testId)) {
                Join<Object, Object> testJoin = root.join("test");
                return criteriaBuilder.equal(testJoin.get("id"), testId);
            }
            return criteriaBuilder.conjunction();
        };
    }

    static Specification<Comment> hasRoot(String isRoot) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(isRoot)) {
                Boolean isRootBool = isRoot.equals("1");
                return criteriaBuilder.equal(root.get("isRoot"), isRootBool);
            }
            return criteriaBuilder.conjunction();
        };
    }

    static Specification<Comment> hasFiltered(String isFiltered) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(isFiltered)) {
                Boolean isFilteredBool = isFiltered.equals("1");
                return criteriaBuilder.equal(root.get("isFiltered"), isFilteredBool);
            }
            return criteriaBuilder.conjunction();
        };
    }

}
