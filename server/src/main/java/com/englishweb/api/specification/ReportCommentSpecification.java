package com.englishweb.api.specification;

import com.englishweb.api.models.ReportComment;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface ReportCommentSpecification {
    static Specification<ReportComment> hasComment(String commentId) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(commentId)) {
                Join<Object, Object> commentJoin = root.join("comment");
                return criteriaBuilder.equal(commentJoin.get("id"), commentId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
