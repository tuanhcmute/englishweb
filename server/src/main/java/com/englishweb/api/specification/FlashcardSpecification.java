package com.englishweb.api.specification;

import com.englishweb.api.models.Flashcard;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface FlashcardSpecification {
    static Specification<Flashcard> hasFlashcardType(String flashcardType) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(flashcardType)) {
                return criteriaBuilder.equal(root.get("flashcardType"), flashcardType);
            }
            return criteriaBuilder.conjunction();
        };
    }

    static Specification<Flashcard> hasUserCredential(String userCredentialId) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(userCredentialId)) {
                Join<Object, Object> userCredentialJoin = root.join("userCredential");
                return criteriaBuilder.equal(userCredentialJoin.get("id"), userCredentialId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
