package com.englishweb.api.specification;

import com.englishweb.api.models.Unit;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface UnitSpecification {

    static Specification<Unit> hasCourse(String courseId) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(courseId)) {
                Join<Object, Object> course = root.join("course");
                return criteriaBuilder.equal(course.get("id"), courseId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
