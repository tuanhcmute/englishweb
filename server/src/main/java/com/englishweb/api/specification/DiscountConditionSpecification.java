package com.englishweb.api.specification;

import com.englishweb.api.models.DiscountCondition;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface DiscountConditionSpecification {
    static Specification<DiscountCondition> hasDiscount(String discountId) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(discountId)) {
                Join<Object, Object> discountJoin = root.join("discount");
                return criteriaBuilder.equal(discountJoin.get("id"), discountId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
