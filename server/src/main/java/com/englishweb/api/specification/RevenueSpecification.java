package com.englishweb.api.specification;

import com.englishweb.api.models.Revenue;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public interface RevenueSpecification {
    static Specification<Revenue> hasBetweenDate(String fromDate, String toDate) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(fromDate) && Objects.nonNull(toDate)) {
                LocalDate parsedFromDate = LocalDate.parse(fromDate);
                LocalDate parsedToDate = LocalDate.parse(toDate);
                return criteriaBuilder.between(
                        root.get("createdDate"),
                        parsedFromDate.atStartOfDay(),
                        parsedToDate.atTime(LocalTime.MAX));
            }
            return criteriaBuilder.conjunction();
        };
    }
}
