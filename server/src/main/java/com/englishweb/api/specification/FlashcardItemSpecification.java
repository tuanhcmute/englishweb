package com.englishweb.api.specification;

import com.englishweb.api.models.FlashcardItem;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface FlashcardItemSpecification {
    static Specification<FlashcardItem> hasFlashcard(String flashcardId) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(flashcardId)) {
                Join<Object, Object> flashcardJoin = root.join("flashcard");
                return criteriaBuilder.equal(flashcardJoin.get("id"), flashcardId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
