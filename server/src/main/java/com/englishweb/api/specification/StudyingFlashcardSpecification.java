package com.englishweb.api.specification;

import com.englishweb.api.models.StudyingFlashcard;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface StudyingFlashcardSpecification {

    static Specification<StudyingFlashcard> hasUserCredential(String userCredentialId) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(userCredentialId)) {
                Join<Object, Object> userCredentialJoin = root.join("userCredential");
                return criteriaBuilder.equal(userCredentialJoin.get("id"), userCredentialId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
