package com.englishweb.api.specification;

import com.englishweb.api.models.NewsCategory;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

public interface NewsCategorySpecification {
    static Specification<NewsCategory> hasStatus(String status) {

        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(status)) {
                return criteriaBuilder.equal(root.get("status"), status);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
