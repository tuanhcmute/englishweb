package com.englishweb.api.specification;

import com.englishweb.api.models.News;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import java.util.Objects;

public interface NewsSpecification {
    static Specification<News> hasCategory(String categoryId) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(categoryId)) {
                Join<Object, Object> categoryJoin = root.join("newsCategory");
                return criteriaBuilder.equal(categoryJoin.get("id"), categoryId);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
