package com.englishweb.api.specification;

import com.englishweb.api.models.Invoice;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;

public interface InvoiceSpecification {
    static Specification<Invoice> hasCreatedDate(LocalDate currentDate) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(currentDate)) {
                LocalDateTime startOfDay = currentDate.atStartOfDay();
                LocalDateTime endOfDay = currentDate.atTime(LocalTime.MAX);
                return criteriaBuilder.between(root.get("createdDate"), startOfDay, endOfDay);
            }
            return criteriaBuilder.conjunction();
        };
    }
}
