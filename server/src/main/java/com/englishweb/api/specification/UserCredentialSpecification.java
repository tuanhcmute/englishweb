package com.englishweb.api.specification;

import com.englishweb.api.models.UserCredential;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

public interface UserCredentialSpecification {
    static Specification<UserCredential> hasId(String id) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(id)) {
                return criteriaBuilder.equal(root.get("id"), id);
            }
            return criteriaBuilder.disjunction();
        };
    }
    static Specification<UserCredential> hasUsername(String username) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(username)) {
                return criteriaBuilder.equal(root.get("username"), username);
            }
            return criteriaBuilder.disjunction();
        };
    }
}
