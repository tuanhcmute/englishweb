package com.englishweb.api.specification;

import com.englishweb.api.models.StaticPage;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

public interface StaticPageSpecification {
    static Specification<StaticPage> hasId(String id) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(id)) {
                return criteriaBuilder.equal(root.get("id"), id);
            }
            return criteriaBuilder.disjunction();
        };
    }
    static Specification<StaticPage> hasPageCode(String pageCode) {
        return (root, query, criteriaBuilder) -> {
            if (Objects.nonNull(pageCode)) {
                return criteriaBuilder.equal(root.get("pageCode"), pageCode);
            }
            return criteriaBuilder.disjunction();
        };
    }
}
