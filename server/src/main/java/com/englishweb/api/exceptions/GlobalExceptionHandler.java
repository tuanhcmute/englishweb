package com.englishweb.api.exceptions;

import com.englishweb.api.enums.ErrorStatus;
import com.englishweb.api.response.ErrorResponse;
import com.englishweb.api.response.Response;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({AuthenticationException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Response<Void> handleAuthenticationException(Exception ex, HttpServletRequest request) {
        log.error("{ {} }", ex.getMessage());
        return Response.<Void>builder()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .message(ex.getMessage())
                .error(true)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .build();
    }

    @ExceptionHandler({BadRequestException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Void> handleBadRequestException(Exception ex, HttpServletRequest request) {
        log.error("{ {} }", ex.getMessage());

        Response<Void> response = Response.<Void>builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .error(true)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .build();
        if (ex instanceof BadRequestException) {
            ErrorResponse errorResponse = ((BadRequestException) ex).getErrorResponse();
            if (Objects.nonNull(errorResponse)) {
                response.setErrors(errorResponse);
            }
        }
        return response;
    }

    @ExceptionHandler({AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Response<Void> handleAccessDeniedException(Exception ex, HttpServletRequest request) {
        log.error("{ {} }", ex.getMessage());
        return Response.<Void>builder()
                .statusCode(HttpStatus.FORBIDDEN.value())
                .message(ex.getMessage())
                .error(true)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .build();
    }

    @ExceptionHandler({NotFoundRequestException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response<Void> handleNotFoundException(Exception ex, HttpServletRequest request) {
        log.error("{ {} }", ex.getMessage());

        Response<Void> response = Response.<Void>builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .message(ex.getMessage())
                .error(true)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .build();
        if (ex instanceof NotFoundRequestException) {
            ErrorResponse errorResponse = ((NotFoundRequestException) ex).getErrorResponse();
            if (Objects.nonNull(errorResponse)) {
                response.setErrors(errorResponse);
            }
        }
        return response;
    }

    @Override
    protected @NonNull ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            @NonNull HttpRequestMethodNotSupportedException ex,
            @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(
                " method is not supported for this request. Supported methods are ");
        Objects.requireNonNull(ex.getSupportedHttpMethods()).forEach(t -> builder.append(t).append(" "));
        ErrorResponse errorResponse = ErrorResponse.builder()
                .errorCode(ErrorStatus.METHOD_NOT_ALLOWED.getErrorCode())
                .errorMessage(builder.toString())
                .build();
        Response<Void> response = Response.<Void>builder()
                .statusCode(HttpStatus.METHOD_NOT_ALLOWED.value())
                .message(ex.getMessage())
                .errors(errorResponse)
                .error(true)
                .method(ex.getMethod())
                .build();
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(response);
    }

    @ExceptionHandler({ConnectException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Void> handleConnectRefusedException(Exception ex, HttpServletRequest request) {
        log.error("{ {} }", ex.getMessage());
        return Response.<Void>builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .error(true)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .build();
    }

    @Override
    protected @NonNull ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .errorCode(ErrorStatus.FIELD_NOT_VALID.getErrorCode())
                .build();
        if (ex.getBindingResult().getAllErrors().size() > 0) {
            errorResponse.setErrorMessage(ex.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        }
        Response<Void> response = Response.<Void>builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .errors(errorResponse)
                .error(true)
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<Void> handleConstraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        log.error("{ {} }", ex.getMessage());
        List<String> errors = new ArrayList<>();
        ex.getConstraintViolations().forEach(cv -> errors.add(cv.getMessage()));

        ErrorResponse errorResponse = ErrorResponse.builder()
                .errorCode(ErrorStatus.FIELD_NOT_VALID.getErrorCode())
                .errorMessage(errors.get(0))
                .build();

        return Response.<Void>builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage())
                .error(true)
                .path(request.getRequestURI())
                .method(request.getMethod())
                .errors(errorResponse)
                .build();
    }
}
