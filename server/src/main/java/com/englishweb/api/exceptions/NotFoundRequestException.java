package com.englishweb.api.exceptions;

import com.englishweb.api.response.ErrorResponse;

public class NotFoundRequestException extends RuntimeException {
    private ErrorResponse errorResponse;

    public NotFoundRequestException(String message) {
        super(message);
    }

    public NotFoundRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundRequestException(ErrorResponse errorResponse, String message) {
        super(message);
        this.errorResponse = errorResponse;
    }

    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(ErrorResponse errorResponse) {
        this.errorResponse = errorResponse;
    }
}
