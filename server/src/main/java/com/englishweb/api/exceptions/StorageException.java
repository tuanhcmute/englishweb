package com.englishweb.api.exceptions;

public class StorageException extends RuntimeException {
    public StorageException(Exception ex) {
        super(ex);
    }
}
