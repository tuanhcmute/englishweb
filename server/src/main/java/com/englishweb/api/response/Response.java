package com.englishweb.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response<TData> {

    private String message;

    private Integer statusCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, TData> data;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String path;

    @Builder.Default
    private Boolean error = false;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ErrorResponse errors;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String method;
}
