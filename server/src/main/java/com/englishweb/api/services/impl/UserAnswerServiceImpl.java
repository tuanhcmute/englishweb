package com.englishweb.api.services.impl;

import com.englishweb.api.dto.UserAnswerDTO;
import com.englishweb.api.enums.PartType;
import com.englishweb.api.enums.PracticeType;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.UserAnswerMapper;
import com.englishweb.api.models.*;
import com.englishweb.api.request.CreateNewUserAnswer;
import com.englishweb.api.request.QuestionGroupUserTestRequest;
import com.englishweb.api.request.QuestionUserTestRequest;
import com.englishweb.api.request.UserTestResultRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.UserAnswerService;
import com.englishweb.api.services.repository.*;
import com.englishweb.api.specification.UserAnswerSpecification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserAnswerServiceImpl implements UserAnswerService {
    private final UserAnswerRepository userAnswerRepository;
    private final UserCredentialRepository userCredentialRepository;
    private final QuestionRepository questionRepository;
    private final TestAnswerRepository testAnswerRepository;
    private final TestRepository testRepository;
    private final ScoreConversionRepository scoreConversionRepository;
    private final UserAnswerMapper userAnswerMapper;
    private final PartOfTestRepository partOfTestRepository;
    private final ObjectMapper objectMapper;

    public List<Integer> calcScoreByPracticeType(List<QuestionGroupUserTestRequest> questionGroupUserTestRequests, int totalRightAnswer, int totalWrongAnswer) {
        List<Integer> result = new ArrayList<>();

        for (QuestionGroupUserTestRequest questionGroupUserTestRequest : questionGroupUserTestRequests) {
            List<QuestionUserTestRequest> questionUserTestRequests = questionGroupUserTestRequest.getQuestions();

            for (QuestionUserTestRequest userTestRequest : questionUserTestRequests) {
                Question question = questionRepository.findById(userTestRequest.getQuestionId())
                        .orElseThrow(() -> new BadRequestException("Question could not be found!"));
                TestAnswer testAnswer = testAnswerRepository.findById(userTestRequest.getAnswerId())
                        .orElseThrow(() -> new BadRequestException("Answer could not be found"));

                String rightAnswer = question.getRightAnswer().strip();
                String userTestAnswer = testAnswer.getId().strip();

                if (rightAnswer.equals(userTestAnswer))
                    totalRightAnswer++;
                else
                    totalWrongAnswer++;
            }

        }

        result.add(totalRightAnswer);
        result.add(totalWrongAnswer);
        return result;
    }

    public List<Integer> calcUserAnswer(List<UserTestResultRequest> questionUserTestRequest) {
        List<Integer> result = new ArrayList<>();
        int totalReadingRightAnswer = 0;
        int totalReadingWrongAnswer = 0;
        int totalListeningRightAnswer = 0;
        int totalListeningWrongAnswer = 0;
        int totalSentences = 0;

        for (UserTestResultRequest userTestResultRequest : questionUserTestRequest) {

            String partId = userTestResultRequest.getPartId();
            PartOfTest partOfTest = partOfTestRepository.findById(partId)
                    .orElseThrow(() -> new BadRequestException("Part of test could not be found!"));

            String partType = partOfTest.getPartInformation().getPartType(); // Reading or Listening
            totalSentences += partOfTest.getPartInformation().getTotalQuestion();
            List<QuestionGroupUserTestRequest> questionGroupUserTestRequests = userTestResultRequest.getQuestionGroups();

            if (partType.equals(PartType.READING.getValue())) {
                result = this.calcScoreByPracticeType(questionGroupUserTestRequests, totalReadingRightAnswer, totalReadingWrongAnswer);
                totalReadingRightAnswer = result.get(0);
                totalReadingWrongAnswer = result.get(1);
            } else if (partType.equals(PartType.LISTENING.getValue())) {
                result = this.calcScoreByPracticeType(questionGroupUserTestRequests, totalListeningRightAnswer, totalListeningWrongAnswer);
                totalListeningRightAnswer = result.get(0);
                totalListeningWrongAnswer = result.get(1);
            }
            result.clear();
        }
        result.add(totalReadingRightAnswer);
        result.add(totalReadingWrongAnswer);
        result.add(totalListeningRightAnswer);
        result.add(totalListeningWrongAnswer);
        result.add(totalSentences);
        return result;
    }

    @Override
    public Response<Void> createNewUserAnswer(CreateNewUserAnswer request) {
        // Check user
        UserCredential userCredential = userCredentialRepository.findById(request.getUserCredentialId())
                .orElseThrow(() -> new BadRequestException("User credential could not found"));

        // Check test
        Test test = testRepository.findById(request.getTestId())
                .orElseThrow(() -> new BadRequestException("Test could not found"));

        ScoreConversion scoreConversion = scoreConversionRepository.findByTest(test)
                .orElseThrow(() -> new BadRequestException("Score conversion could not found"));
        String jsonScore = scoreConversion.getJsonScore();
        log.info("{ Json score: {} }", jsonScore);

        // Get and convert json user answer
        List<UserTestResultRequest> questionUserTestRequests;
        Score scoreBand;

        try {
            questionUserTestRequests = objectMapper.readValue(request.getResults(), new TypeReference<>() {});
            log.info("{ Size of test results: {} }", questionUserTestRequests.size());
        } catch (JsonProcessingException e) {
            log.error("{ Json processing exception: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        try {
            scoreBand = objectMapper.readValue(jsonScore, Score.class);
        } catch (JsonProcessingException e) {
            log.error("{ Json processing exception: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        // Get practice type
        String practiceType = request.getPracticeType();

        List<Integer> results = this.calcUserAnswer(questionUserTestRequests);
        int totalReadingRightAnswer = results.get(0);
        int totalReadingWrongAnswer = results.get(1);
        int totalListeningRightAnswer = results.get(2);
        int totalListeningWrongAnswer = results.get(3);
        int totalSentences = results.get(4);
        if (practiceType.equals(PracticeType.FULL_TEST.getValue())) {
            totalSentences = test.getTestCategory().getTotalQuestion();
        }
        int ignoreAns = totalSentences - totalReadingWrongAnswer - totalReadingRightAnswer - totalListeningWrongAnswer - totalListeningRightAnswer;
        int score = 0;

        log.info("{ Total reading right answer: {} }", totalReadingRightAnswer);
        log.info("{ Total reading wrong answer: {} }", totalReadingWrongAnswer);
        log.info("{ Total listening right answer: {} }", totalListeningRightAnswer);
        log.info("{ Total listening right answer: {} }", totalListeningWrongAnswer);
        log.info("{ Total ignore answer: {} }", ignoreAns);
        log.info("{ Total sentences: {} }", totalSentences);

        if (practiceType.equals(PracticeType.FULL_TEST.getValue())) {
            Map<Integer, Integer> readingScore = scoreBand.getReading();
            Map<Integer, Integer> listeningScore = scoreBand.getListening();
            score = readingScore.get(totalReadingRightAnswer) + listeningScore.get(totalListeningRightAnswer);
        } else if (practiceType.equals(PracticeType.PARTIALS.getValue())) {
            score = ((totalListeningRightAnswer + totalReadingRightAnswer) / totalSentences) * 100;
        }

        UserAnswer userAnswer = UserAnswer.builder()
                .answerJson(request.getResults())
                .userCredential(userCredential)
                .test(test)
                .listeningCorrectAnsCount(totalListeningRightAnswer)
                .listeningWrongAnsCount(totalListeningWrongAnswer)
                .readingCorrectAnsCount(totalReadingRightAnswer)
                .readingWrongAnsCount(totalReadingWrongAnswer)
                .totalScore(score)
                .ignoreAnsCount(ignoreAns)
                .totalSentences(totalSentences)
                .practiceType(practiceType)
                .completionTime(request.getCompletionTime())
                .build();
        try {
            userAnswerRepository.save(userAnswer);
            log.info("{ New user answer: {} }", userAnswer.getId());
        } catch (Exception e) {
            log.error("{ Error when creating user answer: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Create new user answer successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<UserAnswerDTO> getUserAnswerById(String id) {

        Optional<UserAnswer> userAnswerOptional = userAnswerRepository.findById(id);
        if (userAnswerOptional.isEmpty()) throw new BadRequestException("User answer could not found");

        Map<String, UserAnswerDTO> data = new HashMap<>();
        data.put("data", userAnswerMapper.toDTO(userAnswerOptional.get()));
        return Response.<UserAnswerDTO>builder()
                .message("Get user answer by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Long> countByTest(String value) {
        Optional<Test> testOptional = testRepository.findById(value);
        if (testOptional.isEmpty()) throw new BadRequestException("Test could not found");
        Long result = userAnswerRepository.countByTest(testOptional.get());
        Map<String, Long> data = new HashMap<>();
        data.put("data", result);
        return Response.<Long>builder()
                .message("Count user answer by test successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<List<UserAnswerDTO>> getAllUserAnswers(Map<String, String> params) {
        List<UserAnswer> userAnswers = userAnswerRepository.findAll(
                Specification.where(UserAnswerSpecification.hasUserCredential(params.get("userCredentialId"))),
                Sort.by(Sort.Direction.DESC, "testDate")
        );
        Map<String, List<UserAnswerDTO>> data = new HashMap<>();
        data.put("data", userAnswerMapper.toListDTO(userAnswers));
        return Response.<List<UserAnswerDTO>>builder()
                .message("Get user answers by user credential successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }
}
