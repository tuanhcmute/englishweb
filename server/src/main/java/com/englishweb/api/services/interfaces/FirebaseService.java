package com.englishweb.api.services.interfaces;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;


public interface FirebaseService {
    String getImageUrl(String name);

    String save(MultipartFile file) throws IOException;

    Boolean isFileExist(String filename);

    String save(BufferedImage bufferedImage, String originalFileName) throws IOException;

    void delete(String name) throws IOException;

    default String getExtension(String originalFileName) {
        return StringUtils.getFilenameExtension(originalFileName);
    }

    default String generateFileName(String originalFileName) {
        String extension = getExtension(originalFileName);
        return String.format("%s_%s.%s",
                originalFileName.replace(String.format(".%s", extension), ""),
                UUID.randomUUID().toString().replace("-", ""),
                getExtension(originalFileName));
    }

    default byte[] getByteArrays(BufferedImage bufferedImage, String format) throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(bufferedImage, format, baos);
            baos.flush();
            return baos.toByteArray();
        }
    }

    default String getFileName(String url) {
        if (url == null || url.trim().isEmpty()) return "";
        // Split the URL by '/' to get the segments
        String[] segments = url.split("/");
        // Get the last segment which contains the file name
        String fileName = segments[segments.length - 1];
        // If the file name contains query parameters, remove them
        if (fileName.contains("?")) {
            fileName = fileName.split("\\?")[0];
        }
        // Return the file name
        return fileName;
    }

}
