package com.englishweb.api.services.impl;

import com.englishweb.api.dto.RevenueDTO;
import com.englishweb.api.mapper.RevenueMapper;
import com.englishweb.api.models.Invoice;
import com.englishweb.api.models.Revenue;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.RevenueService;
import com.englishweb.api.services.repository.InvoiceRepository;
import com.englishweb.api.services.repository.RevenueRepository;
import com.englishweb.api.specification.InvoiceSpecification;
import com.englishweb.api.specification.RevenueSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class RevenueServiceImpl implements RevenueService {

    private final RevenueRepository revenueRepository;
    private final RevenueMapper revenueMapper;
    private final InvoiceRepository invoiceRepository;

    @Override
    public Response<List<RevenueDTO>> getAllRevenues(Map<String, String> params) {
        String pageStr = params.get("page");
        String limitStr = params.get("limit");
        String fromDate = params.get("from");
        String toDate = params.get("to");
        String sort = params.get("sort");
        if(StringUtils.isBlank(pageStr)) pageStr = "0";
        if(StringUtils.isBlank(limitStr)) limitStr = "10";
        if(StringUtils.isBlank(sort)) sort = "ASC";

        Sort.Direction direction = sort.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC;

        Page<Revenue> page = revenueRepository.findAll(
               Specification.where(RevenueSpecification.hasBetweenDate(fromDate, toDate)),
                PageRequest.of(
                Integer.parseInt(pageStr),
                Integer.parseInt(limitStr),
                Sort.by(direction, "lastModifiedDate")));

        Map<String, List<RevenueDTO>> data = new HashMap<>();
        data.put("data", revenueMapper.toListDTO(page.getContent()));

        return Response.<List<RevenueDTO>>builder()
                .message("Get all banner item successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public void createRevenue() {
        List<Invoice> invoices = invoiceRepository.findAll(
                Specification.where(InvoiceSpecification.hasCreatedDate(LocalDate.now()))
        );
        Integer totalAmount = invoices.stream().mapToInt(Invoice::getTotalPrice).sum();
        Revenue revenue = Revenue.builder()
                .totalAmount(totalAmount)
                .build();

        revenueRepository.save(revenue);
        log.info("Revenue {} created at: {}", revenue.getId(), LocalDateTime.now());
    }
}
