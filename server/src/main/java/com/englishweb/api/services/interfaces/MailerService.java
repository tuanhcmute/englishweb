package com.englishweb.api.services.interfaces;

public interface MailerService {
    void sendOTPForgotPassword(String toEmail);

    void sendWelcome(String toEmail);

    void sendEmailVerification(String toEmail);
}
