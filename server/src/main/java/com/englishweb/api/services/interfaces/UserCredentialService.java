package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.UserCredentialDTO;
import com.englishweb.api.request.SignUpRequest;
import com.englishweb.api.request.UpdateUserInfoRequest;
import com.englishweb.api.request.UpdateUserRoleRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface UserCredentialService {
    Response<List<UserCredentialDTO>> getAllUserCredential(Map<String, String> params);

    Response<Void> deleteUserCredential(String id);

    Response<Void> createNewUser(SignUpRequest signUpRequest);

    Response<Long> countAll();

    Response<Void> updateUserInfo(UpdateUserInfoRequest userInfoRequest);

    Response<Void> updateUserRole(String id, UpdateUserRoleRequest requestData);

    Response<UserCredentialDTO> getUserCredential(String id, String username);
}
