package com.englishweb.api.services.impl;

import com.englishweb.api.dto.QuestionGroupDTO;
import com.englishweb.api.enums.QuestionGroupFileType;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.QuestionGroupMapper;
import com.englishweb.api.models.PartOfTest;
import com.englishweb.api.models.QuestionGroup;
import com.englishweb.api.request.QuestionGroupRequest;
import com.englishweb.api.request.UpdateQuestionGroupRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.FirebaseService;
import com.englishweb.api.services.interfaces.QuestionGroupService;
import com.englishweb.api.services.repository.PartOfTestRepository;
import com.englishweb.api.services.repository.QuestionGroupRepository;
import com.englishweb.api.utils.QuestionGroupUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuestionGroupImpl implements QuestionGroupService {
    private final QuestionGroupRepository questionGroupRepository;
    private final PartOfTestRepository partOfTestRepository;
    private final QuestionGroupMapper questionGroupMapper;
    private final FirebaseService firebaseService;

    @Override
    public Response<List<QuestionGroupDTO>> getAllQuestionGroup() {
        List<QuestionGroup> questionGroups = questionGroupRepository.findAll();

        return getListResponse(questionGroups);
    }

    @Override
    public Response<QuestionGroup> getQuestionGroupById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<QuestionGroup> optionalQuestionGroup = questionGroupRepository.findById(id);

        if (optionalQuestionGroup.isEmpty())
            throw new BadRequestException("Can not find question group!");

        Map<String, QuestionGroup> data = new HashMap<>();
        data.put("data", optionalQuestionGroup.get());

        return Response.<QuestionGroup>builder()
                .message("Get question groups by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteQuestionGroupById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<QuestionGroup> questionGroupOptional = questionGroupRepository.findById(id);

        try {
            if (questionGroupOptional.isPresent()) {
                if (questionGroupOptional.get().getAudio() != null &&
                        !questionGroupOptional.get().getAudio().isEmpty())
                    firebaseService.delete(firebaseService.getFileName(
                            questionGroupOptional.get().getAudio()
                    ));
            }
            questionGroupRepository.deleteById(id);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Delete question groups by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewQuestionGroup(QuestionGroupRequest request) {
//        Validate
        QuestionGroupUtils.validate(request);
        Optional<PartOfTest> partOfTestOptional = partOfTestRepository.findById(request.getPartOfTestId());
        if (partOfTestOptional.isEmpty()) throw new BadRequestException("Part of test could not be found");
        QuestionGroup questionGroup = questionGroupMapper.requestToModel(request);

        questionGroup.setPartOfTest(partOfTestOptional.get());
        //        Save
        questionGroupRepository.save(questionGroup);
        log.info("{ New question group: {} }", questionGroup.getId());

        return Response.<Void>builder()
                .message("Create question groups by id successfully!")
                .statusCode(HttpStatus.CREATED.value())
                .build();
    }

    @Override
    public Response<List<QuestionGroupDTO>> getAllQuestionGroupByPart(String id) {
        Optional<PartOfTest> partOfTestOptional = partOfTestRepository.findById(id);
        if (partOfTestOptional.isEmpty()) throw new BadRequestException("Part of test could not be found");
        List<QuestionGroup> questionGroups = questionGroupRepository.findAllByPartOfTest(partOfTestOptional.get());
        log.info("{ Question groups: {} }", questionGroups.size());
        return getListResponse(questionGroups);
    }

    @Override
    public Response<Void> uploadQuestionGroupFile(String id, String type, MultipartFile file) {
        Optional<QuestionGroup> questionGroupOptional = questionGroupRepository.findById(id);
        if (questionGroupOptional.isEmpty()) throw new BadRequestException("Question group could not be found");
        try {
            String fileUrl = firebaseService.save(file);
            if (type.equals(QuestionGroupFileType.AUDIO.getValue())) {
                log.info("{ Upload audio }");
                if (!questionGroupOptional.get().getAudio().isEmpty()) {
                    firebaseService.delete(firebaseService.getFileName(
                            questionGroupOptional.get().getAudio()
                    ));
                }
                questionGroupOptional.get().setAudio(fileUrl);
            }
//            } else {
//                log.info("{ Upload image}");
//                if(!questionGroupOptional.get().getImage().isEmpty()) {
//                    firebaseService.delete(firebaseService.getFileName(
//                            questionGroupOptional.get().getImage()
//                    ));
//                }
//                questionGroupOptional.get().setImage(fileUrl);
//            }
            questionGroupRepository.save(questionGroupOptional.get());
        } catch (IOException e) {
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message(String.format("Upload question group %s successfully!", type))
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateQuestionGroup(UpdateQuestionGroupRequest request) {
        Optional<QuestionGroup> questionGroupOptional = questionGroupRepository.findById(request.getId());
        if (questionGroupOptional.isEmpty()) throw new BadRequestException("Question group could not be found");
        questionGroupOptional.get().setQuestionContentEn(request.getQuestionContentEn());
        questionGroupOptional.get().setImage(request.getImage());
        questionGroupOptional.get().setQuestionContentTranscript(request.getQuestionContentTranscript());
        questionGroupRepository.save(questionGroupOptional.get());
        return Response.<Void>builder()
                .message("Update question group successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    private Response<List<QuestionGroupDTO>> getListResponse(List<QuestionGroup> questionGroups) {
        Map<String, List<QuestionGroupDTO>> data = new HashMap<>();
        List<QuestionGroupDTO> questionGroupDTOs = questionGroupMapper.toListDTO(questionGroups);
        data.put("data", questionGroupDTOs);

        return Response.<List<QuestionGroupDTO>>builder()
                .message("Get all question groups successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

}
