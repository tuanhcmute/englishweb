package com.englishweb.api.services.impl;

import com.englishweb.api.dto.PaymentDTO;
import com.englishweb.api.enums.PaymentMethod;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.PaymentMapper;
import com.englishweb.api.models.Payment;
import com.englishweb.api.request.CreatePayment;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.PaymentService;
import com.englishweb.api.services.interfaces.VNPayService;
import com.englishweb.api.services.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentImpl implements PaymentService {
    private final PaymentRepository paymentRepository;
    private final PaymentMapper paymentMapper;
    private final VNPayService vnPayService;

    @Override
    public Response<List<PaymentDTO>> getAllPayment() {
        List<Payment> payments = paymentRepository.findAll();
        List<PaymentDTO> paymentDTOs = paymentMapper.toListDTO(payments);
        Map<String, List<PaymentDTO>> data = new HashMap<>();
        data.put("data", paymentDTOs);

        return Response.<List<PaymentDTO>>builder()
                .message("Get all payment successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<PaymentDTO> getPaymentById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<Payment> pOptional = paymentRepository.findById(id);
        if (pOptional.isEmpty())
            throw new BadRequestException("Can not found payment by id!");

        Map<String, PaymentDTO> data = new HashMap<>();
        data.put("data", paymentMapper.toDTO(pOptional.get()));

        return Response.<PaymentDTO>builder()
                .message("Get payment by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deletePaymentById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        paymentRepository.deleteById(id);
        return Response.<Void>builder()
                .message("Delete payment by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public void createPayment(CreatePayment createPayment,
                              HttpServletRequest request,
                              HttpServletResponse response) {
        if (createPayment.getPaymentMethod().equals(PaymentMethod.VNPAY.getValue())) {
            // Call vnpay service
            String returnUrl = String.format("%s/vnpay?courseId=%s&userCredentialId=%s",
                    createPayment.getBaseUrl(),
                    createPayment.getCourseId(),
                    createPayment.getUserCredentialId()
            );
            String vnPayUrl = vnPayService.createPayment(createPayment.getTotalAmount(), returnUrl, request);
            try {
                log.info("{ VNPay Url: {} }", vnPayUrl);
                response.sendRedirect(vnPayUrl);
            } catch (IOException e) {
                log.error("{ Error when create vnpay payment: {} }", e.getMessage());
                throw new BadRequestException(e.getMessage());
            }
        }
    }

}
