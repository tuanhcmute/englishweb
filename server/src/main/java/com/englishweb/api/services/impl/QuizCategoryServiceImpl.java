package com.englishweb.api.services.impl;

import com.englishweb.api.dto.QuizCategoryDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.QuizCategoryMapper;
import com.englishweb.api.models.QuizCategory;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuizCategoryService;
import com.englishweb.api.services.repository.QuizCategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QuizCategoryServiceImpl implements QuizCategoryService{
    private final QuizCategoryRepository quizCategoryRepository;
    private final QuizCategoryMapper quizCategoryMapper;

    @Override
    public Response<List<QuizCategoryDTO>> getAllQuizCategory() {
        List<QuizCategory> quizCategories = quizCategoryRepository.findAll();

        Map< String, List<QuizCategoryDTO>> data = new HashMap<>();
        data.put("data", quizCategoryMapper.toListDTO(quizCategories));

        return Response.<List<QuizCategoryDTO>>builder()
            .statusCode(HttpStatus.OK.value())
            .message("Get all quiz category successfully!")
            .data(data)
            .build();
    }

    @Override
    public Response<QuizCategoryDTO> getQuizCategoryById(String id) {
        if(id.trim().isEmpty())
            throw new BadRequestException("Id value is empty!");

        Optional<QuizCategory> quizOptional = quizCategoryRepository.findById(id);

        if(quizOptional.isEmpty())
            throw new BadRequestException("Can not find quiz category by id!");

        Map< String, QuizCategoryDTO> data = new HashMap<>();
        data.put("data", quizCategoryMapper.toDTO(quizOptional.get()));

        return Response.<QuizCategoryDTO>builder()
            .statusCode(HttpStatus.OK.value())
            .message("Get quiz category by id successfully!")
            .data(data)
            .build();
    }

    @Override
    public Response<Void> deletQuizCategoryById(String id) {
        if(id.trim().isEmpty())
            throw new BadRequestException("Id value is empty!");

        quizCategoryRepository.deleteById(id);

        return Response.<Void>builder()
        .statusCode(HttpStatus.OK.value())
        .message("Delete quiz category by id successfully!")
        .build();
    }
    
}
