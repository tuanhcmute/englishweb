package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.AgentVersionDTO;
import com.englishweb.api.request.UpdateAgentVersionRequest;
import com.englishweb.api.response.Response;

public interface AgentVersionService {
    Response<Void> updateAgentVersion(String id, UpdateAgentVersionRequest requestData);

    Response<AgentVersionDTO> getAgentVersion();
}
