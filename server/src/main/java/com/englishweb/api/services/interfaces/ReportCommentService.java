package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.ReportCommentDTO;
import com.englishweb.api.request.CreateNewReportCommentRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface ReportCommentService {
    Response<List<ReportCommentDTO>> getAllReportComments(Map<String, String> params);

    Response<ReportCommentDTO> getReportCommentById(String id);

    Response<Void> deleteReportCommentById(String id);

    Response<Void> createReportComment(CreateNewReportCommentRequest createNewReportCommentRequest);
}
