package com.englishweb.api.services.impl;

import com.englishweb.api.dto.FlashcardDTO;
import com.englishweb.api.enums.FlashcardType;
import com.englishweb.api.events.FlashcardItemEvent;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.FlashcardMapper;
import com.englishweb.api.models.Flashcard;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateFlashcardRequest;
import com.englishweb.api.request.UpdateFlashcardRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.FlashcardService;
import com.englishweb.api.services.repository.FlashcardItemRepository;
import com.englishweb.api.services.repository.FlashcardRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.FlashcardSpecification;
import com.englishweb.api.utils.FlashcardUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class FlashcardServiceImpl implements FlashcardService {
    private final FlashcardRepository flashcardRepository;
    private final FlashcardItemRepository flashcardItemRepository;
    private final FlashcardMapper flashcardMapper;
    private final UserCredentialRepository userCredentialRepository;

    @Override
    public Response<List<FlashcardDTO>> getAllFlashcard(Map<String, String> params) {
        List<Flashcard> flashcards = flashcardRepository.findAll(
                Specification.where(FlashcardSpecification.hasUserCredential(params.get("userCredentialId")))
                        .and(FlashcardSpecification.hasFlashcardType(params.get("flashcardType"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );

        List<FlashcardDTO> flashcardDTOs = flashcardMapper.toListDTO(flashcards);
        Map<String, List<FlashcardDTO>> data = new HashMap<>();
        data.put("data", flashcardDTOs);

        return Response.<List<FlashcardDTO>>builder()
                .message("Get all flash card successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<FlashcardDTO> getFlashcardById(String id) {
        Flashcard flashcard = flashcardRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Flashcard %s could not be found", id)));

        Map<String, FlashcardDTO> data = new HashMap<>();
        data.put("data", flashcardMapper.toDTO(flashcard));

        return Response.<FlashcardDTO>builder()
                .message("Get flash card by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteFlashcard(String id) {
        if (id.isBlank())
            throw new BadRequestException("Id value is empty!");

        flashcardRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete flash card by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewFlashcard(CreateFlashcardRequest createFlashcardRequest) {
        FlashcardUtils.flashcardValidate(createFlashcardRequest);
        UserCredential userCredential = userCredentialRepository.findById(createFlashcardRequest.getUserCredentialId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", createFlashcardRequest.getUserCredentialId())));

        boolean anyMatch = Arrays.stream(FlashcardType.values()).anyMatch(type -> type.getValue().equals(createFlashcardRequest.getFlashcardType()));
        if (!anyMatch) throw new BadRequestException("Flashcard type could not be found");

        Flashcard flashcard = Flashcard.builder()
                .flashcardDescription(createFlashcardRequest.getFlashcardDescription())
                .flashcardTitle(createFlashcardRequest.getFlashcardTitle())
                .totalFlashcardItem(0)
                .userCredential(userCredential)
                .flashcardType(createFlashcardRequest.getFlashcardType())
                .build();
        try {
            flashcardRepository.save(flashcard);
            log.info("{ New flashcard: {} }", flashcard.getId());
        } catch (Exception e) {
            log.error("{ Error when creating flashcard: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message("Create new flash card successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateFlashcard(UpdateFlashcardRequest requestData) {
        Flashcard flashcard = flashcardRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Flashcard %s could not be found", requestData.getId())));

        flashcard.setFlashcardTitle(requestData.getFlashcardTitle());
        flashcard.setFlashcardDescription(requestData.getFlashcardDescription());

        try {
            flashcardRepository.save(flashcard);
            log.info("{ Update flashcard: {} }", flashcard.getId());
        } catch (Exception e) {
            log.error("{ Error when updating flashcard: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message("Update flashcard successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Async
    @EventListener
    @Override
    public void updateTotalFlashcardListener(FlashcardItemEvent flashcardItemEvent) {
        flashcardRepository.findById(flashcardItemEvent.getFlashcardId())
                .ifPresent(flashcard -> {
                    long count = flashcardItemRepository.countByFlashcard(flashcard);
                    log.info("{ count: {} }", count);
                    int totalFlashcardItem = flashcard.getFlashcardItems().size();
                    log.info("{ Size of flashcard item: {} }", totalFlashcardItem);

                    flashcard.setTotalFlashcardItem(totalFlashcardItem);
                    flashcardRepository.save(flashcard);
                    log.info("{ Update flashcard: {} }", flashcard.getId());
                });
    }

}
