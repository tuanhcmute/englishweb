package com.englishweb.api.services.interfaces;

public interface OTPService {
    String generateOTP(String email);

    Boolean verifyOTP(String email, String otpCode);

    Boolean isRateLimited(String rateLimitKey);

    Boolean isRedisConnected();
}
