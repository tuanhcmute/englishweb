package com.englishweb.api.services.repository;

import com.englishweb.api.models.TestCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TestCategoryRepository extends JpaRepository<TestCategory, String>, JpaSpecificationExecutor<TestCategory> {

}

