package com.englishweb.api.services.repository;

import com.englishweb.api.models.UserAuthority;
import com.englishweb.api.models.UserCredential;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserAuthorityRepository extends JpaRepository<UserAuthority, String> {
    Optional<UserAuthority> findByUserCredential(UserCredential userCredential);
}
