package com.englishweb.api.services.repository;

import com.englishweb.api.models.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizRepository extends JpaRepository<Quiz, String>{
    
}
