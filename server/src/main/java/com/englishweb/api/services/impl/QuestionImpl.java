package com.englishweb.api.services.impl;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.Question;
import com.englishweb.api.models.QuestionGroup;
import com.englishweb.api.models.TestAnswer;
import com.englishweb.api.request.CreateTestAnswerRequest;
import com.englishweb.api.request.QuestionRequest;
import com.englishweb.api.request.UpdateQuestionRequest;
import com.englishweb.api.request.UpdateTestAnswerRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuestionService;
import com.englishweb.api.services.repository.QuestionGroupRepository;
import com.englishweb.api.services.repository.QuestionRepository;
import com.englishweb.api.services.repository.TestAnswerRepository;
import com.englishweb.api.utils.QuestionUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuestionImpl implements QuestionService {
    private final QuestionRepository questionRepository;
    private final QuestionGroupRepository questionGroupRepository;
    private final TestAnswerRepository testAnswerRepository;

    @Override
    public Response<List<Question>> getAllQuestion() {
        List<Question> questions = questionRepository.findAll();

        Map<String, List<Question>> data = new HashMap<>();
        data.put("data", questions);

        return Response.<List<Question>>builder()
                .message("Get all questions successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Question> getQuestionById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<Question> quesOptional = questionRepository.findById(id);

        if (quesOptional.isEmpty())
            throw new BadRequestException("Can not found question!");


        Map<String, Question> data = new HashMap<>();
        data.put("data", quesOptional.get());

        return Response.<Question>builder()
                .message("Get all questions successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteQuestion(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        questionRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete question by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewQuestion(QuestionRequest questionRequest) {
        QuestionUtils.questionValidate(questionRequest);

        Optional<QuestionGroup> questionGroupOptional = questionGroupRepository.findById(questionRequest.getQuestionGroupId());

        if (questionGroupOptional.isEmpty())
            throw new BadRequestException("Can not find question group by question group id!");

        Set<TestAnswer> testAnswers = new HashSet<>();
        List<CreateTestAnswerRequest> createTestAnswerRequests = questionRequest.getTestAnswers();

        Question question = Question.builder()
                .questionContent(questionRequest.getQuestionContent())
                .questionNumber(questionRequest.getQuestionNumber())
//                .rightAnswer(questionRequest.getRightAnswer())
                .questionGuide(questionRequest.getQuestionGuide())
                .questionGroup(questionGroupOptional.get())
                .build();

        try {
            questionRepository.save(question);
            log.info("{ Updated question: {} }", question.getId());
        } catch (Exception e) {
            log.error("{ Error when updating question: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        for (CreateTestAnswerRequest createTestAnswerRequest : createTestAnswerRequests) {
            TestAnswer testAnswer = TestAnswer.builder()
                    .answerChoice(createTestAnswerRequest.getAnswerChoice())
                    .answerContent(createTestAnswerRequest.getAnswerContent())
                    .answerTranslate(createTestAnswerRequest.getAnswerTranslate())
                    .question(question)
                    .isCorrect(createTestAnswerRequest.getIsCorrect())
                    .build();
            testAnswers.add(testAnswer);
        }
        testAnswerRepository.saveAll(testAnswers);

        testAnswers.forEach(ta -> {
            if (ta.getIsCorrect()) {
                question.setRightAnswer(ta.getId());
            }
        });
        questionRepository.save(question);

        return Response.<Void>builder()
                .message("Create new question successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateQuestion(UpdateQuestionRequest updateQuestionRequest) {
        QuestionUtils.updateQuestionValidate(updateQuestionRequest);

        Optional<Question> quesOptional = questionRepository.findById(updateQuestionRequest.getId());

        if (quesOptional.isEmpty())
            throw new BadRequestException("Can not find question need to update!");

        Question question = quesOptional.get();

        question.setQuestionContent(updateQuestionRequest.getQuestionContent());
        question.setQuestionGuide(updateQuestionRequest.getQuestionGuide());
        question.setQuestionNumber(updateQuestionRequest.getQuestionNumber());

        try {
            questionRepository.save(question);
            log.info("{ Updated question: {} }", question.getId());
        } catch (Exception e) {
            log.error("{ Error when updating question: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        for (UpdateTestAnswerRequest testAnswerRequest : updateQuestionRequest.getTestAnswers()) {
            Optional<TestAnswer> testAnswerOptional = testAnswerRepository.findById(testAnswerRequest.getId());
            if (testAnswerOptional.isPresent()) {
                testAnswerOptional.get().setAnswerTranslate(testAnswerRequest.getAnswerTranslate());
                testAnswerOptional.get().setAnswerChoice(testAnswerRequest.getAnswerChoice());
                testAnswerOptional.get().setAnswerContent(testAnswerRequest.getAnswerContent());
                testAnswerOptional.get().setIsCorrect(testAnswerRequest.getIsCorrect());
                try {
                    testAnswerRepository.save(testAnswerOptional.get());
                    log.info("{ Updated answer: {} }", testAnswerOptional.get().getId());
                } catch (Exception e) {
                    log.error("{ Error when updating answer: {} }", e.getMessage());
                    throw new BadRequestException(e.getMessage());
                }

                if (testAnswerOptional.get().getIsCorrect()) {
                    question.setRightAnswer(testAnswerOptional.get().getId());
                    questionRepository.save(question);
                    log.info("{ Update right answer of question: {} }", question.getRightAnswer());
                }
            }
        }

        return Response.<Void>builder()
                .message("Update question successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }
}
