package com.englishweb.api.services.impl;

import com.englishweb.api.dto.FlashcardItemDTO;
import com.englishweb.api.events.FlashcardItemEvent;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.FlashcardItemMapper;
import com.englishweb.api.models.*;
import com.englishweb.api.request.CreateFlashcardItem;
import com.englishweb.api.request.CreateFlashcardItemByKeyword;
import com.englishweb.api.request.UpdateFlashcardItem;
import com.englishweb.api.request.UploadFlashcardItemImageRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.FlashcardItemService;
import com.englishweb.api.services.interfaces.WordService;
import com.englishweb.api.services.repository.FlashcardItemRepository;
import com.englishweb.api.services.repository.FlashcardRepository;
import com.englishweb.api.specification.FlashcardItemSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class FlashcardItemImpl implements FlashcardItemService {
    private final FlashcardItemRepository flashcardItemRepository;
    private final FlashcardRepository flashcardRepository;
    private final FlashcardItemMapper flashcardItemMapper;
    private final WordService wordService;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public Response<List<FlashcardItemDTO>> getAllFlashcardItem(Map<String, String> params) {
        List<FlashcardItem> flashcardItems = flashcardItemRepository.findAll(
                Specification.where(FlashcardItemSpecification.hasFlashcard(params.get("flashcardId"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        List<FlashcardItemDTO> flashcardItemDTOS = flashcardItemMapper.toListDTO(flashcardItems);

        Map<String, List<FlashcardItemDTO>> data = new HashMap<>();
        data.put("data", flashcardItemDTOS);

        return Response.<List<FlashcardItemDTO>>builder()
                .message("Get all flash card item successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<FlashcardItemDTO> getAllFlashcardItemById(String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null!");

        Optional<FlashcardItem> fOptional = flashcardItemRepository.findById(id);
        if (fOptional.isEmpty())
            throw new BadRequestException("Can not find flash card item!");

        Map<String, FlashcardItemDTO> data = new HashMap<>();
        data.put("data", flashcardItemMapper.toDTO(fOptional.get()));

        return Response.<FlashcardItemDTO>builder()
                .message("Get flash card item by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteFlashcardItem(String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null!");

        Optional<FlashcardItem> flashcardItemOptional = flashcardItemRepository.findById(id);
        if (flashcardItemOptional.isEmpty())
            throw new BadRequestException("Flashcard item is null!");

        Flashcard flashcard = flashcardItemOptional.get().getFlashcard();
        flashcard.getFlashcardItems().remove(flashcardItemOptional.get());

        try {
            flashcardItemRepository.delete(flashcardItemOptional.get());
            flashcardRepository.save(flashcard);
            applicationEventPublisher.publishEvent(new FlashcardItemEvent(this, flashcard.getId()));
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }


        return Response.<Void>builder()
                .message("Delete flash card item by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createFlashcardItem(CreateFlashcardItem request) {
        Optional<Flashcard> flashcardOptional = flashcardRepository.findById(request.getFlashcardId());
        if (flashcardOptional.isEmpty()) throw new BadRequestException("Flashcard could not be found");

        FlashcardItem flashcardItem = FlashcardItem.builder()
                .flashcard(flashcardOptional.get())
                .examples(request.getExamples())
                .tags(request.getTags())
                .word(request.getWord())
                .meaning(request.getMeaning())
                .phonetic(request.getPhonetic())
                .audio(String.format("https://dict.youdao.com/dictvoice?audio=%s&amp;type=1", request.getWord()))
                .build();
        try {
            flashcardItemRepository.save(flashcardItem);
            log.info("{ New flashcard item: {} }", flashcardItem.getId());
            applicationEventPublisher.publishEvent(new FlashcardItemEvent(this, flashcardOptional.get().getId()));
        } catch (Exception e) {
            log.error("{ Error when creating flashcard item: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message("Create flashcard item successfully!")
                .statusCode(HttpStatus.CREATED.value())
                .build();
    }

    @Override
    public Response<Void> updateFlashcardItem(UpdateFlashcardItem request) {
        Optional<FlashcardItem> flashcardItemOptional = flashcardItemRepository.findById(request.getFlashcardItemId());
        if (flashcardItemOptional.isEmpty()) throw new BadRequestException("Flashcard item could not be found");

        flashcardItemOptional.get().setAudio(String.format("https://dict.youdao.com/dictvoice?audio=%s&amp;type=1", request.getWord()));
        flashcardItemOptional.get().setWord(request.getWord());
        flashcardItemOptional.get().setExamples(request.getExamples());
        flashcardItemOptional.get().setMeaning(request.getMeaning());
        flashcardItemOptional.get().setPhonetic(request.getPhonetic());
        flashcardItemOptional.get().setTags(request.getTags());
        try {
            flashcardItemRepository.save(flashcardItemOptional.get());
            log.info("{ Update flashcard item: {} }", flashcardItemOptional.get().getId());
        } catch (Exception e) {
            log.error("{ Error when updating flashcard item: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message("Update flashcard item successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateFlashcardItemImage(String id, UploadFlashcardItemImageRequest requestData) {
        FlashcardItem flashcardItem = flashcardItemRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Flashcard item could not be found"));

        flashcardItem.setImage(requestData.getImage());
        flashcardItemRepository.save(flashcardItem);
        log.info("{ Update flashcard item: {} }", flashcardItem.getId());
        return Response.<Void>builder()
                .message("Update flashcard item successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createFlashcardItemByKeyword(CreateFlashcardItemByKeyword request) {
        Optional<Flashcard> flashcardOptional = flashcardRepository.findById(request.getFlashcardId());
        if (flashcardOptional.isEmpty()) throw new BadRequestException("Flashcard could not be found");
        String[] keywords = request.getKeyword().split(", ");
        List<FlashcardItem> flashcardItems = new ArrayList<>();
        Arrays.stream(keywords).forEach(item -> {
            List<Word> words = wordService.getAllWordsByKeyword(item, request.getLimit());
            words.forEach(item2 -> {
                List<WordDefinition> wordDefinitions = wordService.getAllWordDefinitionByWord(item2.getWord());
                if (wordDefinitions.size() > 0) {
                    String word = item2.getWord();
                    String phonetic = "";
                    String audio = String.format("https://dict.youdao.com/dictvoice?audio=%s&amp;type=1", word);
                    String meaning = "";
                    String examples = "";
                    String tags = Arrays.toString(item2.getTags());
                    if (StringUtils.hasText(wordDefinitions.get(0).getPhonetic()))
                        phonetic = wordDefinitions.get(0).getPhonetic();
                    if (!wordDefinitions.get(0).getMeanings().isEmpty()) {
                        Meaning meaningObj = wordDefinitions.get(0).getMeanings().get(0);
                        if (!meaningObj.getDefinitions().isEmpty()) {
                            Definition definitionObj = meaningObj.getDefinitions().get(0);
                            if (StringUtils.hasText(definitionObj.getDefinition()))
                                meaning = definitionObj.getDefinition();
                            if (StringUtils.hasText(definitionObj.getExample()))
                                examples = definitionObj.getExample();
                        }
                    }
                    FlashcardItem flashcardItem = FlashcardItem.builder()
                            .audio(audio)
                            .flashcard(flashcardOptional.get())
                            .word(word)
                            .phonetic(phonetic)
                            .meaning(meaning)
                            .tags(tags)
                            .examples(examples)
                            .build();
                    flashcardItems.add(flashcardItem);
                }
            });
        });
        flashcardItemRepository.saveAll(flashcardItems);
        applicationEventPublisher.publishEvent(new FlashcardItemEvent(this, flashcardOptional.get().getId()));
        return Response.<Void>builder()
                .message("Create flashcard item successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
