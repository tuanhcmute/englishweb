package com.englishweb.api.services.repository;

import com.englishweb.api.models.PartInformation;
import com.englishweb.api.models.TestCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PartInformationRepository extends JpaRepository<PartInformation, String> {
    List<PartInformation> findAllByTestCategoryOrderByPartSequenceNumberAsc(TestCategory testCategory);
}

