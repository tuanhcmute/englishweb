package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.FullOrderDTO;
import com.englishweb.api.models.Order;
import com.englishweb.api.request.CreateOrder;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface OrderService {
    Response<List<FullOrderDTO>> getAllOrders(Map<String, String> params);

    Response<FullOrderDTO> getOrderById(String id);

    Response<Void> deleteOrderById(String id);

    Order createOrder(CreateOrder createOrder);

}
