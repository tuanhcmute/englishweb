package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.FlashcardDTO;
import com.englishweb.api.events.FlashcardItemEvent;
import com.englishweb.api.request.CreateFlashcardRequest;
import com.englishweb.api.request.UpdateFlashcardRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface FlashcardService {
    Response<List<FlashcardDTO>> getAllFlashcard(Map<String, String> params);

    Response<FlashcardDTO> getFlashcardById(String id);

    Response<Void> deleteFlashcard(String id);

    Response<Void> createNewFlashcard(CreateFlashcardRequest createFlashcardRequest);

    Response<Void> updateFlashcard(UpdateFlashcardRequest requestData);

    void updateTotalFlashcardListener(FlashcardItemEvent flashcardItemEvent);
}
