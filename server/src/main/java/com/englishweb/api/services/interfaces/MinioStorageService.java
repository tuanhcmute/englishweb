package com.englishweb.api.services.interfaces;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.UUID;

public interface MinioStorageService {
    void save(MultipartFile file, String uuid);

    InputStream getInputStream(UUID uuid, long offset, long length);
}
