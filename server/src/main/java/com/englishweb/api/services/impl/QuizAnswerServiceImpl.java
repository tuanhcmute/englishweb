package com.englishweb.api.services.impl;

import com.englishweb.api.dto.QuizAnswerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.QuizAnswerMapper;
import com.englishweb.api.models.QuizAnswer;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuizAnswerService;
import com.englishweb.api.services.repository.QuizAnswerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QuizAnswerServiceImpl implements QuizAnswerService{
    private final QuizAnswerRepository quizAnswerRepository;
    private final QuizAnswerMapper quizAnswerMapper;

    @Override
    public Response<List<QuizAnswerDTO>> getAllQuizAnswer() {
        List<QuizAnswer> quizAnswers = quizAnswerRepository.findAll();

        Map<String, List<QuizAnswerDTO>> data = new HashMap<>();
        data.put("data", quizAnswerMapper.toListDTO(quizAnswers));

        return Response.<List<QuizAnswerDTO>>builder()
            .statusCode(HttpStatus.OK.value())
            .message("Get all quiz answer successful!")
            .data(data)
            .build();
    }

    @Override
    public Response<QuizAnswerDTO> getQuizAnswerById(String id) {
        if(id.trim().isEmpty())
            throw new BadRequestException("Id value is empty!");

        Optional<QuizAnswer> quizOptional = quizAnswerRepository.findById(id);
        if(quizOptional.isEmpty())
            throw new BadRequestException("Id value is empty!");

        Map<String, QuizAnswerDTO> data = new HashMap<>();
        data.put("data", quizAnswerMapper.toDTO(quizOptional.get()));

        return Response.<QuizAnswerDTO>builder()
        .statusCode(HttpStatus.OK.value())
        .message("Get quiz answer by id successful!")
        .data(data)
        .build();
    }

    @Override
    public Response<Void> deleteQuizAnswerById(String id) {
        if(id.trim().isEmpty())
            throw new BadRequestException("Id value is empty!");

        quizAnswerRepository.deleteById(id);
        return Response.<Void>builder()
        .statusCode(HttpStatus.OK.value())
        .message("Delete quiz answer by id successful!")
        .build();
    }
    
}
