package com.englishweb.api.services.impl;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.interfaces.MailerService;
import com.englishweb.api.services.interfaces.OTPService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Base64;

@Service
@Slf4j
@RequiredArgsConstructor
public class MailerServiceImpl implements MailerService {
    private final OTPService otpService;
    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    @Value("${spring.mail.username}")
    private String fromMail;

    @Override
    public void sendOTPForgotPassword(String toEmail) {
        String otpCode = otpService.generateOTP(toEmail);
        log.info("{ OTP code: {} }", otpCode);
        String templateName = "otp-forgot-password";

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        try {
            helper.setFrom(fromMail);
            helper.setTo(toEmail);
            helper.setSubject("Yêu Cầu Đặt Lại Mật Khẩu");
            Context thymeleafContext = new Context();
            thymeleafContext.setVariable("otp", otpCode);
            thymeleafContext.setVariable("toEmail", toEmail);
            String htmlBody = templateEngine.process(templateName, thymeleafContext);
            helper.setText(htmlBody, true);
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public void sendWelcome(String toEmail) {
        String templateName = "welcome";

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        try {
            helper.setFrom(fromMail);
            helper.setTo(toEmail);
            helper.setSubject(String.format("Chào Mừng Bạn Đến Với English Academy %s", toEmail));
            Context thymeleafContext = new Context();
            thymeleafContext.setVariable("toEmail", toEmail);
            String htmlBody = templateEngine.process(templateName, thymeleafContext);
            helper.setText(htmlBody, true);
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public void sendEmailVerification(String toEmail) {
//        Get verify url
        String otpCode = otpService.generateOTP(String.format("email_verification_%s", toEmail));
        String otpCodeEncoded = Base64.getEncoder().encodeToString(otpCode.getBytes());
        String verifyUrl = String.format("https://english.workon.space/server/v1.0.0/auth/email/verification/%s?email=%s", otpCodeEncoded, toEmail);
        String templateName = "email-verification";

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        try {
            helper.setFrom(fromMail);
            helper.setTo(toEmail);
            helper.setSubject("Xác Nhận Địa Chỉ Email Của Bạn");
            Context thymeleafContext = new Context();
            thymeleafContext.setVariable("verifyUrl", verifyUrl);
            String htmlBody = templateEngine.process(templateName, thymeleafContext);
            helper.setText(htmlBody, true);
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            throw new BadRequestException(e.getMessage());
        }
    }
}
