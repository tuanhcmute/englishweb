package com.englishweb.api.services.repository;

import com.englishweb.api.models.Flashcard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FlashcardRepository extends JpaRepository<Flashcard, String>, JpaSpecificationExecutor<Flashcard> {
}
