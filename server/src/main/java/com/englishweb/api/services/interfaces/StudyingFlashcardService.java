package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.StudyingFlashcardDTO;
import com.englishweb.api.request.CreateStudyingFlashcardRequest;
import com.englishweb.api.request.UpdateStudyingFlashcardRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface StudyingFlashcardService {

    Response<Void> createStudyingFlashcard(CreateStudyingFlashcardRequest requestData);

    Response<Void> updateStudyingFlashcard(UpdateStudyingFlashcardRequest requestData);

    Response<Void> deleteStudyingFlashcard(String id);

    Response<List<StudyingFlashcardDTO>> getAllStudyingFlashcard(Map<String, String> params);
}
