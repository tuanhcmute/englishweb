package com.englishweb.api.services.impl;

import com.englishweb.api.dto.StudyingFlashcardDTO;
import com.englishweb.api.enums.StudyingFlashcardStatus;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.StudyingFlashcardMapper;
import com.englishweb.api.models.Flashcard;
import com.englishweb.api.models.StudyingFlashcard;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateStudyingFlashcardRequest;
import com.englishweb.api.request.UpdateStudyingFlashcardRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.StudyingFlashcardService;
import com.englishweb.api.services.repository.FlashcardRepository;
import com.englishweb.api.services.repository.StudyingFlashcardRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.StudyingFlashcardSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class StudyingFlashcardServiceImpl implements StudyingFlashcardService {
    private final StudyingFlashcardRepository studyingFlashcardRepository;
    private final StudyingFlashcardMapper studyingFlashcardMapper;
    private final UserCredentialRepository userCredentialRepository;
    private final FlashcardRepository flashcardRepository;

    @Override
    public Response<Void> createStudyingFlashcard(CreateStudyingFlashcardRequest requestData) {
        Optional<Flashcard> flashcardOptional = flashcardRepository.findById(requestData.getFlashcardId());
        if (flashcardOptional.isEmpty())
            throw new BadRequestException("Can not found studying flashcard!");

        Optional<UserCredential> userCredentialOptional = userCredentialRepository.findById(requestData.getUserCredentialId());
        if (userCredentialOptional.isEmpty())
            throw new BadRequestException("Can not found user credential!");

        StudyingFlashcard studyingFlashcard = StudyingFlashcard.builder()
                .flashcard(flashcardOptional.get())
                .userCredential(userCredentialOptional.get())
                .status(StudyingFlashcardStatus.STUDYING.getValue())
                .build();

        try {
            studyingFlashcardRepository.save(studyingFlashcard);
            log.info("{ Create studying flashcard: {} }", studyingFlashcard.getId());
        } catch (Exception e) {
            log.error("{Error when updating studying flashcard: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Create studying flashcard by id successful!")
                .statusCode(HttpStatus.CREATED.value())
                .build();
    }

    @Override
    public Response<Void> updateStudyingFlashcard(UpdateStudyingFlashcardRequest requestData) {
        Optional<StudyingFlashcard> studyingFlashcardOptional = studyingFlashcardRepository.findById(requestData.getId());
        if (studyingFlashcardOptional.isEmpty())
            throw new BadRequestException("Can not found studying flashcard!");
        studyingFlashcardOptional.get().setStatus(requestData.getStatus());

        try {
            studyingFlashcardRepository.save(studyingFlashcardOptional.get());
            log.info("{ Create studying flashcard: {} }", studyingFlashcardOptional.get().getId());
        } catch (Exception e) {
            log.error("{Error when updating studying flashcard: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Update studying flashcard by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> deleteStudyingFlashcard(String id) {
        try {
            studyingFlashcardRepository.deleteById(id);
            log.info("{ Delete studying flashcard: {} }", id);
        } catch (Exception e) {
            log.error("{Error when deleting studying flashcard: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Delete studying flashcard by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<List<StudyingFlashcardDTO>> getAllStudyingFlashcard(Map<String, String> params) {
        List<StudyingFlashcard> studyingFlashcards = studyingFlashcardRepository.findAll(
                Specification.where(StudyingFlashcardSpecification
                        .hasUserCredential(params.get("userCredentialId"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );

        return getListResponse(studyingFlashcards);
    }

    private Response<List<StudyingFlashcardDTO>> getListResponse(List<StudyingFlashcard> studyingFlashcards) {
        Map<String, List<StudyingFlashcardDTO>> data = new HashMap<>();
        data.put("data", studyingFlashcardMapper.toListDTO(studyingFlashcards));

        return Response.<List<StudyingFlashcardDTO>>builder()
                .message("Get all course category successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }
}
