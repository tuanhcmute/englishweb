package com.englishweb.api.services.repository;

import com.englishweb.api.models.Test;
import com.englishweb.api.models.UserAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserAnswerRepository extends JpaRepository<UserAnswer, String>, JpaSpecificationExecutor<UserAnswer> {

    long countByTest(Test test);
}
