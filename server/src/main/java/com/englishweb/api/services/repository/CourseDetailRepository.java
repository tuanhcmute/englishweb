package com.englishweb.api.services.repository;

import com.englishweb.api.models.Course;
import com.englishweb.api.models.CourseDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CourseDetailRepository extends JpaRepository<CourseDetail, String> {

    Optional<CourseDetail> findByCourse(Course course);
}
