package com.englishweb.api.services.repository;

import com.englishweb.api.models.TestAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestAnswerRepository extends JpaRepository<TestAnswer, String> {

}
