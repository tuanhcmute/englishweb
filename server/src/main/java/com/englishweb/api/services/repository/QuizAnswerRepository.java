package com.englishweb.api.services.repository;

import com.englishweb.api.models.QuizAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizAnswerRepository extends JpaRepository<QuizAnswer, String>{
    
}
