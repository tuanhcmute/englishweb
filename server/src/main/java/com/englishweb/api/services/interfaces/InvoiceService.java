package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.InvoiceDTO;
import com.englishweb.api.request.CreateNewInvoiceRequest;
import com.englishweb.api.request.ExportInvoiceRequest;
import com.englishweb.api.response.Response;

import java.util.List;

public interface InvoiceService {
    Response<List<InvoiceDTO>> getAllInvoices();

    Response<InvoiceDTO> getInvoiceById(String id);

    Response<Void> deleteInvoiceId(String id);

    Response<Void> createNewInvoice(CreateNewInvoiceRequest createNewInvoiceRequest);

    byte[] exportInvoice(ExportInvoiceRequest requestData);

}
