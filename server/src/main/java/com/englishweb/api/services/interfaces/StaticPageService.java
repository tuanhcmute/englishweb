package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.StaticPageDTO;
import com.englishweb.api.request.CreatePageStaticRequest;
import com.englishweb.api.request.UpdatePageStaticRequest;
import com.englishweb.api.response.Response;

import java.util.List;

public interface StaticPageService {
    Response<List<StaticPageDTO>> getAllStaticPages();

    Response<Void> createStaticPage(CreatePageStaticRequest request);

    Response<Void> updateStaticPage(UpdatePageStaticRequest request);

    Response<StaticPageDTO> getStaticPage(String id, String pageCode);
}
