package com.englishweb.api.services.impl;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.ScoreConversion;
import com.englishweb.api.models.Test;
import com.englishweb.api.request.UpdateScoreConversionRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.ScoreConversionService;
import com.englishweb.api.services.repository.ScoreConversionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ScoreConversionServiceImpl implements ScoreConversionService {
    private final ScoreConversionRepository scoreConversionRepository;

    @Override
    public void createScoreConversion(Test test) {
        ScoreConversion scoreConversion =ScoreConversion.builder()
                .test(test)
                .jsonScore("{}")
                .build();
        scoreConversionRepository.save(scoreConversion);
        log.info("{ Score conversion: {} created }", scoreConversion.getId());
    }

    @Override
    @Transactional
    public Response<Void> updateScoreConversion(UpdateScoreConversionRequest requestData) {
        ScoreConversion scoreConversion = scoreConversionRepository.findById(requestData.getId())
                .orElseThrow(() -> new BadRequestException(String.format("Score conversion %s could not be found", requestData.getId())));
        scoreConversion.setJsonScore(requestData.getJsonScore());

        scoreConversionRepository.save(scoreConversion);
        log.info("{ Score conversion {} updated }", scoreConversion.getId());
        return Response.<Void>builder()
                .message("Update score conversion successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }
}
