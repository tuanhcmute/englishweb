package com.englishweb.api.services.repository;

import com.englishweb.api.models.LessonNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LessonNoteRepository extends JpaRepository<LessonNote, String>, JpaSpecificationExecutor<LessonNote> {
}
