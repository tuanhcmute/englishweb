package com.englishweb.api.services.impl;

import com.englishweb.api.services.interfaces.MinioStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MinioStorageServiceImpl implements MinioStorageService {

//    private final MinioClient minioClient;

    @Value("${app.minio.put-object-part-size}")
    private Long putObjectPartSize;

    @Override
    public void save(MultipartFile file, String uuid) {
//        try {
//            minioClient.putObject(
//                    PutObjectArgs
//                            .builder()
//                            .bucket(MinioConfiguration.COMMON_BUCKET_NAME)
//                            .object(uuid)
//                            .stream(file.getInputStream(), file.getSize(), putObjectPartSize)
//                            .build()
//            );
//        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException |
//                 InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException |
//                 XmlParserException e) {
//            throw new BadRequestException(e.getMessage());
//        }
    }

    @Override
    public InputStream getInputStream(UUID uuid, long offset, long length) {
//        try {
//            return minioClient.getObject(
//                    GetObjectArgs
//                            .builder()
//                            .bucket(MinioConfiguration.COMMON_BUCKET_NAME)
//                            .offset(offset)
//                            .length(length)
//                            .object(uuid.toString())
//                            .build());
//        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException |
//                 InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException |
//                 XmlParserException e) {
//            throw new BadRequestException(e.getMessage());
//        }
        return null;
    }
}
