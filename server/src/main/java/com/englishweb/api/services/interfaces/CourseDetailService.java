package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.CourseDetailDTO;
import com.englishweb.api.request.UpdateCourseDetailRequest;
import com.englishweb.api.response.Response;

import java.util.List;

public interface CourseDetailService {
    Response<List<CourseDetailDTO>> getAllCourseDetail();

    Response<CourseDetailDTO> getCourseDetailById(String id);

    Response<Void> deleteCourseDetailById(String id);

    Response<CourseDetailDTO> getCourseDetailByCourse(String courseId);

    Response<Void> updateCourseDetail(UpdateCourseDetailRequest request);
}
