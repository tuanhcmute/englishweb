package com.englishweb.api.services.repository;

import com.englishweb.api.models.Flashcard;
import com.englishweb.api.models.FlashcardItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FlashcardItemRepository extends JpaRepository<FlashcardItem, String>, JpaSpecificationExecutor<FlashcardItem> {
    long countByFlashcard(Flashcard flashcard);
}
