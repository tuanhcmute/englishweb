package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.LessonDTO;
import com.englishweb.api.request.CreateLessonRequest;
import com.englishweb.api.request.UpdateLessonRequest;
import com.englishweb.api.response.Response;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface LessonService {
    Response<List<LessonDTO>> getAllLessons();

    Response<LessonDTO> getLessonById(String id);

    Response<Void> deleteLessonById(String id);

    Response<Void> createNewLesson(CreateLessonRequest createLessonRequest);

    Response<Void> updateLesson(UpdateLessonRequest requestData);

    Response<Void> uploadVideoLesson(String id, MultipartFile file);
}
