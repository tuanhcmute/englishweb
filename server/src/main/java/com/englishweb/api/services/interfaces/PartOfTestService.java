package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.PartOfTestDTO;
import com.englishweb.api.models.PartOfTest;
import com.englishweb.api.models.Test;
import com.englishweb.api.models.TestCategory;
import com.englishweb.api.response.Response;

import java.util.List;

public interface PartOfTestService {
    Response<List<PartOfTest>> getAllPartOfTest();

    Response<PartOfTest> getPartOfTestById(String id);

    Response<Void> deletePartOfTest(String id);

    Response<List<PartOfTestDTO>> getAllPartOfTestByTest(String value);

    Response<Void> updatePartOfTestByTest(String value);

    void buckCreatePartOfTest(TestCategory testCategory, Test test);
}
