package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.DiscountDTO;
import com.englishweb.api.models.Discount;
import com.englishweb.api.request.CreateDiscountRequest;
import com.englishweb.api.request.DiscountRequest;
import com.englishweb.api.request.UpdateDiscountRequest;
import com.englishweb.api.response.Response;

import java.util.List;

public interface DiscountService {
    Response<Void> assignDiscount(DiscountRequest discountRequest);

    Response<List<DiscountDTO>> getAllDiscounts();

    Response<Void> createDiscount(CreateDiscountRequest requestData);

    Response<Void> updateDiscount(UpdateDiscountRequest requestData);

    void updateDiscountStatus(Discount discount);
}
