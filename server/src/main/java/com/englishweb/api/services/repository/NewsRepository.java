package com.englishweb.api.services.repository;

import com.englishweb.api.models.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NewsRepository extends JpaRepository<News, String>, JpaSpecificationExecutor<News> {
}
