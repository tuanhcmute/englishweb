package com.englishweb.api.services.repository;

import com.englishweb.api.models.PartOfTest;
import com.englishweb.api.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PartOfTestRepository extends JpaRepository<PartOfTest, String> {
    List<PartOfTest> findAllByTestOrderByPartSequenceNumberAsc(Test test);
}
