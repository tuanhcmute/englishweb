package com.englishweb.api.services.repository;

import com.englishweb.api.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, String> {
    Optional<Role> findByRoleName(com.englishweb.api.enums.Role roleName);
}
