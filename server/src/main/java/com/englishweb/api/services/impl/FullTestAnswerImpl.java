package com.englishweb.api.services.impl;

import com.englishweb.api.dto.FullTestAnswerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.FullTestAnswerMapper;
import com.englishweb.api.models.FullTestAnswer;
import com.englishweb.api.models.Test;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.FullTestAnswerService;
import com.englishweb.api.services.repository.FullTestAnswerRepository;
import com.englishweb.api.services.repository.TestRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class FullTestAnswerImpl implements FullTestAnswerService {
    private final FullTestAnswerRepository fullTestAnswerRepository;
    private final TestRepository testRepository;
    private final FullTestAnswerMapper fullTestAnswerMapper;

    @Override
    public Response<List<FullTestAnswer>> getAllTestAnswer() {
        List<FullTestAnswer> fullTestAnswers = fullTestAnswerRepository.findAll();
        Map<String, List<FullTestAnswer>> data = new HashMap<>();
        data.put("data", fullTestAnswers);

        return Response.<List<FullTestAnswer>>builder()
                .message("Get full test answer successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<FullTestAnswer> getFullTestAnswerById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<FullTestAnswer> fullTestAnswerOptional = fullTestAnswerRepository.findById(id);
        if (fullTestAnswerOptional.isEmpty())
            throw new BadRequestException("Can not full test answer course!");

        Map<String, FullTestAnswer> data = new HashMap<>();
        data.put("data", fullTestAnswerOptional.get());

        return Response.<FullTestAnswer>builder()
                .message("Get full test answer by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteFullTestAnswer(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        fullTestAnswerRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete full test answer by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<FullTestAnswerDTO> getFullTestAnswerByTest(String value) {
        if (value.isEmpty() || value.isBlank())
            throw new BadRequestException("Id is null");

        Optional<Test> testOptional = testRepository.findById(value);
        if (testOptional.isEmpty()) throw new BadRequestException("Test could not be found");

        Optional<FullTestAnswer> fullTestAnswerOptional = fullTestAnswerRepository.findByTest(testOptional.get());
        if (fullTestAnswerOptional.isEmpty())
            throw new BadRequestException("Can not full test answer!");

        Map<String, FullTestAnswerDTO> data = new HashMap<>();
        data.put("data", fullTestAnswerMapper.toDTO(fullTestAnswerOptional.get()));

        return Response.<FullTestAnswerDTO>builder()
                .message("Get full test answer by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();

    }

    @Override
    @Transactional
    public void createFullTestAnswer(Test test) {
        FullTestAnswer fullTestAnswer = FullTestAnswer.builder()
                .test(test)
                .build();
        fullTestAnswerRepository.save(fullTestAnswer);
        log.info("{ Full test answer: {} created }", fullTestAnswer.getId());
    }

}
