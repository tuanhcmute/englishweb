package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.FullPartInformationDTO;
import com.englishweb.api.dto.PartInformationDTO;
import com.englishweb.api.request.CreatePartInformation;
import com.englishweb.api.request.UpdatePartInformation;
import com.englishweb.api.response.Response;

import java.util.List;

public interface PartInformationService {
    Response<List<FullPartInformationDTO>> getAllPartInformation();

    Response<List<FullPartInformationDTO>> getAllPartInformationByTestCategory(String testCategoryId);

    Response<PartInformationDTO> getPartInformationById(String id);

    Response<Void> deletePartInformation(String id);

    Response<Void> createNewPartInformation(CreatePartInformation partInformation);

    Response<Void> updatePartInformation(UpdatePartInformation partInformation);
}

