package com.englishweb.api.services.impl;

import com.englishweb.api.dto.ReportCommentDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.ReportCommentMapper;
import com.englishweb.api.models.Comment;
import com.englishweb.api.models.ReportComment;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateNewReportCommentRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.ReportCommentService;
import com.englishweb.api.services.repository.CommentRepository;
import com.englishweb.api.services.repository.ReportCommentRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.ReportCommentSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ReportCommentServiceImpl implements ReportCommentService{
    private final ReportCommentRepository reportCommentRepository;
    private final CommentRepository commentRepository;
    private final ReportCommentMapper reportCommentMapper;
    private final UserCredentialRepository userCredentialRepository;
    
    @Override
    public Response<List<ReportCommentDTO>> getAllReportComments(Map<String, String> params) {
        List<ReportComment> reportComments = reportCommentRepository.findAll(
                Specification.where(ReportCommentSpecification.hasComment(params.get("commentId"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        Map<String, List<ReportCommentDTO>> data = new HashMap<>();
        data.put("data", reportCommentMapper.toListDTO(reportComments));

        return Response.<List<ReportCommentDTO>>builder()
            .statusCode(HttpStatus.OK.value())
            .message("Get all report comment successfully!")
            .data(data)
            .build();
    }

    @Override
    public Response<ReportCommentDTO> getReportCommentById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        ReportComment reportComment = reportCommentRepository.findById(id)
            .orElseThrow(() -> new NotFoundRequestException(String.format("Report comment %s could not be found", id)));

        Map<String, ReportCommentDTO> data = new HashMap<>();
        data.put("data", reportCommentMapper.toDTO(reportComment));
        return Response.<ReportCommentDTO>builder()
            .statusCode(HttpStatus.OK.value())
            .message("Get report comment by id successfully!")
            .data(data)
            .build();
    }

    @Override
    public Response<Void> deleteReportCommentById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        reportCommentRepository.deleteById(id);
        
        return Response.<Void>builder()
        .statusCode(HttpStatus.OK.value())
        .message("Delete report comment by id successfully!")
        .build();
    }

    @Override
    public Response<Void> createReportComment(CreateNewReportCommentRequest requestData) {
        Comment comment = commentRepository.findById(requestData.getCommentId())
            .orElseThrow(() -> new NotFoundRequestException(String.format("Comment %s could not be found", requestData.getCommentId())));
        UserCredential userCredential = userCredentialRepository.findById(requestData.getUserCredentialId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", requestData.getUserCredentialId())));

        ReportComment reportComment = ReportComment
                .builder()
                .comment(comment)
                .reportContent(requestData.getReportContent())
                .userCredential(userCredential)
                .build();
        reportCommentRepository.save(reportComment);

        return Response.<Void>builder()
        .statusCode(HttpStatus.OK.value())
        .message("Create new report comment successfully!")
        .build();
    }
}
