package com.englishweb.api.services.impl;

import com.englishweb.api.dto.FullOrderDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.OrderMapper;
import com.englishweb.api.models.Course;
import com.englishweb.api.models.Order;
import com.englishweb.api.models.OrderLine;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateOrder;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.OrderService;
import com.englishweb.api.services.repository.CourseRepository;
import com.englishweb.api.services.repository.OrderRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.OrderSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final UserCredentialRepository userCredentialRepository;
    private final CourseRepository courseRepository;

    @Override
    public Response<List<FullOrderDTO>> getAllOrders(Map<String, String> params) {
//        Get all params
        final String userCredentialId = params.get("userCredentialId");
        final String paymentStatus = params.get("paymentStatus");

        List<Order> orders = orderRepository
                .findAll(
                        Specification.where(OrderSpecification.hasUserCredential(userCredentialId))
                                .and(OrderSpecification.hasPaymentStatus(paymentStatus)),
                        Sort.by(Sort.Direction.DESC, "orderDate"));

        List<FullOrderDTO> fullOrderDTOS = orderMapper.toFullListDTO(orders);
        Map<String, List<FullOrderDTO>> data = new HashMap<>();
        data.put("data", fullOrderDTOS);

        return Response.<List<FullOrderDTO>>builder()
                .message("Get all order successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<FullOrderDTO> getOrderById(String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null!");

        Optional<Order> orOptional = orderRepository.findById(id);

        if (orOptional.isEmpty())
            throw new BadRequestException("Can not find order!");

        Map<String, FullOrderDTO> data = new HashMap<>();
        data.put("data", orderMapper.toFullDTO(orOptional.get()));

        return Response.<FullOrderDTO>builder()
                .message("Get order by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteOrderById(String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null!");

        orderRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete order by id successfully!")
                .build();
    }

    @Override
    public Order createOrder(CreateOrder createOrder) {
        Optional<UserCredential> userCredentialOptional = userCredentialRepository.findById(createOrder.getUserCredentialId());
        if (userCredentialOptional.isEmpty()) throw new BadRequestException("User credential could not be found");

        Optional<Course> courseOptional = courseRepository.findById(createOrder.getCourseId());
        if (courseOptional.isEmpty()) throw new BadRequestException("Course could not be found");

        Order order = Order.builder()
                .userCredential(userCredentialOptional.get())
                .paymentStatus(true)
                .build();
        OrderLine orderLine = OrderLine.builder()
                .order(order)
                .course(courseOptional.get())
                .build();
        order.getOrderLines().add(orderLine);
        try {
            orderRepository.save(order);
            log.info("{ Order: {} }", order.getId());
            return order;
        } catch (Exception e) {
            log.error("{ Error when creating order: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }
}
