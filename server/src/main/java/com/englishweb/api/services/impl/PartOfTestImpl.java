package com.englishweb.api.services.impl;

import com.englishweb.api.dto.PartOfTestDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.PartOfTestMapper;
import com.englishweb.api.models.PartInformation;
import com.englishweb.api.models.PartOfTest;
import com.englishweb.api.models.Test;
import com.englishweb.api.models.TestCategory;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.PartOfTestService;
import com.englishweb.api.services.repository.PartInformationRepository;
import com.englishweb.api.services.repository.PartOfTestRepository;
import com.englishweb.api.services.repository.TestRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class PartOfTestImpl implements PartOfTestService {
    private final PartOfTestRepository partOfTestRepository;
    private final TestRepository testRepository;
    private final PartOfTestMapper partOfTestMapper;
    private final PartInformationRepository partInformationRepository;

    @Override
    public Response<List<PartOfTest>> getAllPartOfTest() {
        List<PartOfTest> partOfTests = partOfTestRepository.findAll();
        Map<String, List<PartOfTest>> data = new HashMap<>();
        data.put("data", partOfTests);

        return Response.<List<PartOfTest>>builder()
                .message("Get all part of test successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<PartOfTest> getPartOfTestById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<PartOfTest> partOfTesOptional = partOfTestRepository.findById(id);

        if (partOfTesOptional.isEmpty())
            throw new BadRequestException("Can not found part of test!");

        Map<String, PartOfTest> data = new HashMap<>();
        data.put("data", partOfTesOptional.get());

        return Response.<PartOfTest>builder()
                .message("Get all part of test by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deletePartOfTest(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        partOfTestRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete part of test successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<List<PartOfTestDTO>> getAllPartOfTestByTest(String value) {
        Optional<Test> testOptional = testRepository.findById(value);
        if (testOptional.isEmpty()) throw new BadRequestException("Test could not be found");
        List<PartOfTest> partOfTests = partOfTestRepository.findAllByTestOrderByPartSequenceNumberAsc(testOptional.get());
        Map<String, List<PartOfTestDTO>> data = new HashMap<>();
        data.put("data", partOfTestMapper.toListDTO(partOfTests));
        return Response.<List<PartOfTestDTO>>builder()
                .message("Get all part of test successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> updatePartOfTestByTest(String value) {
        Optional<Test> testOptional = testRepository.findById(value);
        if (testOptional.isEmpty()) throw new BadRequestException("Test could not be found");
        List<PartInformation> partInformation = partInformationRepository.findAllByTestCategoryOrderByPartSequenceNumberAsc(testOptional.get().getTestCategory());
        List<PartOfTest> partOfTests = partOfTestRepository.findAllByTestOrderByPartSequenceNumberAsc(testOptional.get());

        partInformation.forEach(pi -> {
            boolean anyMatch = partOfTests.stream().anyMatch(pot -> pot.getPartInformation().getId().equals(pi.getId()));
            if (!anyMatch) {
                PartOfTest p = PartOfTest.builder()
                        .test(testOptional.get())
                        .partSequenceNumber(pi.getPartSequenceNumber())
                        .partInformation(pi)
                        .build();
                partOfTestRepository.save(p);
            }
        });
        return Response.<Void>builder()
                .message("Update part of test successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    @Transactional
    public void buckCreatePartOfTest(TestCategory testCategory, Test test) {
        List<PartOfTest> partOfTests = new ArrayList<>();
        //        Create part
        testCategory.getPartInformation().forEach(
                item -> {
                    PartOfTest partOfTest = PartOfTest.builder()
                            .test(test)
                            .partInformation(item)
                            .partSequenceNumber(item.getPartSequenceNumber())
                            .build();
                    partOfTests.add(partOfTest);
                }
        );
        partOfTestRepository.saveAll(partOfTests);
        log.info("{ {} Part of tests created }", partOfTests.size());
    }

}
