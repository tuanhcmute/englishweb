package com.englishweb.api.services.impl;

import com.englishweb.api.dto.StaticPageDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.StaticPageMapper;
import com.englishweb.api.models.StaticPage;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreatePageStaticRequest;
import com.englishweb.api.request.UpdatePageStaticRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.StaticPageService;
import com.englishweb.api.services.repository.StaticPageRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.StaticPageSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class StaticPageServiceImpl implements StaticPageService {
    private final StaticPageRepository staticPageRepository;
    private final StaticPageMapper staticPageMapper;
    private final UserCredentialRepository userCredentialRepository;

    @Override
    public Response<List<StaticPageDTO>> getAllStaticPages() {
        List<StaticPage> staticPages = staticPageRepository.findAll();
        Map<String, List<StaticPageDTO>> data = new HashMap<>();
        data.put("data", staticPageMapper.toListDTO(staticPages));

        return Response.<List<StaticPageDTO>>builder()
                .message("Get all test successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    private Response<StaticPageDTO> getStaticPageDTOResponse(Optional<StaticPage> staticPageOptional) {
        if (staticPageOptional.isEmpty())
            throw new BadRequestException("Can not found static page!");

        Map<String, StaticPageDTO> data = new HashMap<>();
        data.put("data", staticPageMapper.toDTO(staticPageOptional.get()));

        return Response.<StaticPageDTO>builder()
                .message("Get static page successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }


    @Override
    public Response<Void> createStaticPage(CreatePageStaticRequest request) {
        Optional<UserCredential> userCredentialOptional = userCredentialRepository.findById(request.getUserCredentialId());
        if (userCredentialOptional.isEmpty()) throw new BadRequestException("User credential could not be found");

        StaticPage staticPage = StaticPage.builder()
                .pageContent(request.getPageContent())
                .pageName(request.getPageName())
                .pageCode(request.getPageCode())
                .createdBy(userCredentialOptional.get())
                .build();
        try {
            staticPageRepository.save(staticPage);
            log.info("{ New static page: {} }", staticPage.getId());
            return Response.<Void>builder()
                    .message("Create static page successful!")
                    .statusCode(HttpStatus.OK.value())
                    .build();
        } catch (Exception e) {
            log.error("{ Error when creating static page: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public Response<Void> updateStaticPage(UpdatePageStaticRequest request) {
        StaticPage staticPage = staticPageRepository
                .findById(request.getId())
                .orElseThrow(() -> new BadRequestException("Can not found static page!"));
        staticPage.setPageContent(request.getPageContent());
        staticPage.setPageCode(request.getPageCode());
        staticPage.setPageName(request.getPageName());
        staticPage.setPagePreview(request.getPagePreview());
        staticPage.setPageUrlVideo(request.getPageUrlVideo());
        try {
            staticPageRepository.save(staticPage);
            log.info("{ New static page: {} }", staticPage.getId());
            return Response.<Void>builder()
                    .message("Update static page successful!")
                    .statusCode(HttpStatus.OK.value())
                    .build();
        } catch (Exception e) {
            log.error("{ Error when updating static page: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public Response<StaticPageDTO> getStaticPage(String id, String pageCode) {
        Optional<StaticPage> staticPageOptional = staticPageRepository.findOne(
                Specification.where(StaticPageSpecification.hasId(id))
                        .or(StaticPageSpecification.hasPageCode(pageCode))
        );
        return getStaticPageDTOResponse(staticPageOptional);
    }
}
