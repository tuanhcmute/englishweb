package com.englishweb.api.services.repository;

import com.englishweb.api.models.InvoiceLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceLineRepository extends JpaRepository<InvoiceLine, String> {

}
