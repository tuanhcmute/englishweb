package com.englishweb.api.services.interfaces;

import com.englishweb.api.models.Question;
import com.englishweb.api.request.QuestionRequest;
import com.englishweb.api.request.UpdateQuestionRequest;
import com.englishweb.api.response.Response;

import java.util.List;

public interface QuestionService {
    Response<List<Question>> getAllQuestion();

    Response<Question> getQuestionById(String id);

    Response<Void> deleteQuestion(String id);

    Response<Void> createNewQuestion(QuestionRequest questionRequest);

    Response<Void> updateQuestion(UpdateQuestionRequest updateQuestionRequest);
}
