package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.FlashcardItemDTO;
import com.englishweb.api.request.CreateFlashcardItem;
import com.englishweb.api.request.CreateFlashcardItemByKeyword;
import com.englishweb.api.request.UpdateFlashcardItem;
import com.englishweb.api.request.UploadFlashcardItemImageRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface FlashcardItemService {
    Response<List<FlashcardItemDTO>> getAllFlashcardItem(Map<String, String> params);

    Response<FlashcardItemDTO> getAllFlashcardItemById(String id);

    Response<Void> deleteFlashcardItem(String id);

    Response<Void> createFlashcardItem(CreateFlashcardItem request);

    Response<Void> updateFlashcardItem(UpdateFlashcardItem request);

    Response<Void> updateFlashcardItemImage(String id, UploadFlashcardItemImageRequest requestData);

    Response<Void> createFlashcardItemByKeyword(CreateFlashcardItemByKeyword request);
}
