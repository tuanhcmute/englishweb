package com.englishweb.api.services.impl;

import com.englishweb.api.dto.AuthDTO;
import com.englishweb.api.enums.ErrorStatus;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.ForgotPasswordRequest;
import com.englishweb.api.request.SignUpRequest;
import com.englishweb.api.request.UserAuthRequest;
import com.englishweb.api.request.UserSocialAuthRequest;
import com.englishweb.api.response.ErrorResponse;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.*;
import com.englishweb.api.services.repository.UserCredentialRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final JwtService jwtService;
    private final UserCredentialService userCredentialService;
    private final UserCredentialRepository userCredentialRepository;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImpl userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final OTPService otpService;
    private final MailerService mailerService;


    @Override
    public Response<AuthDTO> login(UserAuthRequest request) {
//        Get user
        Optional<UserCredential> userCredentialOptional = userCredentialRepository.findByUsername(request.getUsername());
        if (userCredentialOptional.isEmpty()) {
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .errorCode(ErrorStatus.USERNAME_NOT_EXIST.getErrorCode())
                    .errorMessage(ErrorStatus.USERNAME_NOT_EXIST.toString())
                    .build();
            return Response.<AuthDTO>builder()
                    .message(String.format("User %s could not be found", request.getUsername()))
                    .statusCode(HttpStatus.BAD_REQUEST.value())
                    .errors(errorResponse)
                    .build();
        }

        UserCredential userCredential = userCredentialOptional.get();

//        Validate password
        if (!passwordEncoder.matches(request.getPassword(), userCredential.getPassword())) {
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .errorCode(ErrorStatus.PASSWORD_NOT_MATCH.getErrorCode())
                    .errorMessage(ErrorStatus.PASSWORD_NOT_MATCH.toString())
                    .build();
            return Response.<AuthDTO>builder()
                    .message("Username or password is not valid")
                    .statusCode(HttpStatus.BAD_REQUEST.value())
                    .errors(errorResponse)
                    .build();
        }

        // Authenticate the user
        return getAuthDTOResponse(userCredential);
    }

    @Override
    public Response<Void> signUp(SignUpRequest signUpRequest) {
        if(!otpService.isRedisConnected()) {
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .errorCode(ErrorStatus.REDIS_UNCONNECTED.getErrorCode())
                    .errorMessage(ErrorStatus.REDIS_UNCONNECTED.toString())
                    .build();
            throw new BadRequestException(errorResponse, "Redis unconnected");
        }
        Response<Void> response = userCredentialService.createNewUser(signUpRequest);
        if (response.getStatusCode() == HttpStatus.CREATED.value()) {
            mailerService.sendWelcome(signUpRequest.getEmail());
            mailerService.sendEmailVerification(signUpRequest.getEmail());
        }
        return response;
    }

    @Override
    public Response<AuthDTO> loginWithSocial(UserSocialAuthRequest request) {
        if(!otpService.isRedisConnected()) {
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .errorCode(ErrorStatus.REDIS_UNCONNECTED.getErrorCode())
                    .errorMessage(ErrorStatus.REDIS_UNCONNECTED.toString())
                    .build();
            throw new BadRequestException(errorResponse, "Redis unconnected");
        }

//        Find by email
        Optional<UserCredential> userExist = userCredentialRepository
                .findByEmailOrUsername(
                        request.getEmail(),
                        request.getUsername());

        if (userExist.isEmpty()) {
            // create user
            String password = UUID.randomUUID().toString();
            SignUpRequest signUpRequest = SignUpRequest.builder()
                    .password(password)
                    .confirmPassword(password)
                    .email(request.getEmail())
                    .fullName(request.getName())
                    .provider(request.getProvider())
                    .username(request.getUsername())
                    .avatar(request.getAvatar())
                    .build();
            Response<Void> response = userCredentialService.createNewUser(signUpRequest);
            if (response.getStatusCode() == HttpStatus.CREATED.value()) {
                mailerService.sendWelcome(signUpRequest.getEmail());
                mailerService.sendEmailVerification(signUpRequest.getEmail());
            }
        } else {
            if (!userExist.get().getProvider().equals(request.getProvider())) {
                ErrorResponse errorResponse = ErrorResponse.builder()
                        .errorCode(ErrorStatus.USER_EXIST.getErrorCode())
                        .errorMessage(ErrorStatus.USER_EXIST.toString())
                        .build();
                return Response.<AuthDTO>builder()
                        .message("User credential already exists!")
                        .statusCode(HttpStatus.BAD_REQUEST.value())
                        .errors(errorResponse)
                        .build();
            }
        }

        Optional<UserCredential> userCredentialOptional = userCredentialRepository
                .findByEmailAndUsername(request.getEmail(), request.getUsername());
        if ((userCredentialOptional.isEmpty())) throw new BadRequestException("User credential could not be found");
        // Authenticate the user
        return getAuthDTOResponse(userCredentialOptional.get());
    }

    @Override
    public Response<Void> forgotPassword(ForgotPasswordRequest forgotPasswordRequest) {
        Optional<UserCredential> userCredentialOptional = userCredentialRepository.findByEmail(forgotPasswordRequest.getEmail());

        if (userCredentialOptional.isEmpty())
            throw new BadRequestException(String.format("Could not found user with email %s", forgotPasswordRequest.getEmail()));

        // Verify OTP
        boolean isVerified = otpService.verifyOTP(forgotPasswordRequest.getEmail(), forgotPasswordRequest.getOtp());
        if (!isVerified) throw new BadRequestException("OTP is not matched");

//        Save new pass
        userCredentialOptional.get().setPassword(passwordEncoder.encode(forgotPasswordRequest.getNewPassword()));
        userCredentialRepository.save(userCredentialOptional.get());

        return Response.<Void>builder()
                .message("Forgot password successfully")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> sendOTPForgotPassword(String toEmail) {
        mailerService.sendOTPForgotPassword(toEmail);
        return Response.<Void>builder()
                .message(String.format("Send email to %s successfully", toEmail))
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> emailVerification(String hash, String email) {
        String otpCodeDecoded = Arrays.toString(Base64.getDecoder().decode(hash));
        boolean isVerified = otpService.verifyOTP(String.format("email_verification_%s", email), otpCodeDecoded);
        if (!isVerified) throw new BadRequestException("Could not verified this email. Please, try later");

        UserCredential userCredential = userCredentialRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", email)));

        userCredential.setVerified(true);

        return Response.<Void>builder()
                .message(String.format("Email %s verified", email))
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> sendOTPEmailVerification(String toEmail) {
        mailerService.sendEmailVerification(String.format("email_verification_%s", toEmail));
        return Response.<Void>builder()
                .message(String.format("Send email to %s successfully", toEmail))
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    private Response<AuthDTO> getAuthDTOResponse(UserCredential userCredential) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(userCredential.getUsername());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        authenticationManager.authenticate(authentication);

//        Generate token
        String accessToken = jwtService.createToken(authentication);

        AuthDTO authDTO = AuthDTO.builder()
                .accessToken(accessToken)
                .refreshToken(accessToken)
                .build();
        Map<String, AuthDTO> data = new HashMap<>();
        data.put("data", authDTO);

        return Response.<AuthDTO>builder()
                .message("Login successfully")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }
}
