package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.QuizCategoryDTO;
import com.englishweb.api.response.Response;

import java.util.List;

public interface QuizCategoryService {
    Response<List<QuizCategoryDTO>> getAllQuizCategory();

    Response<QuizCategoryDTO> getQuizCategoryById(String id);

    Response<Void> deletQuizCategoryById(String id);
}
