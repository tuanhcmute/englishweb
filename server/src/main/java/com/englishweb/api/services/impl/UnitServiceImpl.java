package com.englishweb.api.services.impl;

import com.englishweb.api.dto.UnitDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.UnitMapper;
import com.englishweb.api.models.Course;
import com.englishweb.api.models.Unit;
import com.englishweb.api.request.CreateUnitRequest;
import com.englishweb.api.request.UpdateUnitRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.UnitService;
import com.englishweb.api.services.repository.CourseRepository;
import com.englishweb.api.services.repository.UnitRepository;
import com.englishweb.api.specification.UnitSpecification;
import com.englishweb.api.utils.UnitUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UnitServiceImpl implements UnitService {
    private final UnitRepository unitRepository;
    private final UnitMapper unitMapper;
    private final CourseRepository courseRepository;

    @Override
    public Response<List<UnitDTO>> getAllUnits(Map<String, String> params) {
        List<Unit> units = unitRepository.findAll(
                Specification.where(UnitSpecification.hasCourse(params.get("courseId"))),
                Sort.by(Sort.Direction.ASC, "sequence")
        );
        return getUnitDTOResponse(units);
    }

    @Override
    public Response<UnitDTO> getUnitById(String id) {
        if (id.isEmpty())
            throw new BadRequestException("Id value is empty");

        Optional<Unit> uOptional = unitRepository.findById(id);

        if (uOptional.isEmpty())
            throw new BadRequestException("Can nit find unit!");

        Map<String, UnitDTO> data = new HashMap<>();
        data.put("data", unitMapper.toDTO(uOptional.get()));

        return Response.<UnitDTO>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get unit by id successfully!")
                .data(data)
                .build();
    }

    private Response<List<UnitDTO>> getUnitDTOResponse(List<Unit> units) {
        Map<String, List<UnitDTO>> data = new HashMap<>();
        data.put("data", unitMapper.toListDTO(units));

        return Response.<List<UnitDTO>>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get all units successfully!")
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteUnitById(String id) {
        if (id.isEmpty())
            throw new BadRequestException("Id value is empty");

        unitRepository.deleteById(id);


        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Delete unit by id successfully!")
                .build();
    }

    @Override
    public Response<Void> createNewUnit(CreateUnitRequest createUnitRequest) {
        UnitUtils.validateCreatNewUnitRequestModel(createUnitRequest);

        Course course = courseRepository.findById(createUnitRequest.getCourseId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Course %s could not be found", createUnitRequest.getCourseId())));

        int sizeUnits = unitRepository.countAllByCourse(course);

        Unit unit = Unit.builder()
                .title(createUnitRequest.getTitle())
                .course(course)
                .sequence(sizeUnits)
                .build();

        unitRepository.save(unit);

        return Response.<Void>builder()
                .statusCode(HttpStatus.CREATED.value())
                .message(String.format("Unit %s created successfully", unit.getId()))
                .build();
    }

    @Override
    public Response<Void> updateUnit(UpdateUnitRequest requestData) {
        Unit unit = unitRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Unit %s could not be found", requestData.getId())));
        unit.setTitle(requestData.getTitle());
        unitRepository.save(unit);

        return Response.<Void>builder()
                .statusCode(HttpStatus.CREATED.value())
                .message(String.format("Unit %s updated successfully", unit.getId()))
                .build();
    }

}
