package com.englishweb.api.services.impl;

import com.englishweb.api.dto.ReviewDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.ReviewMapper;
import com.englishweb.api.models.Course;
import com.englishweb.api.models.Review;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateReviewRequest;
import com.englishweb.api.request.UpdateReviewRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.ReviewService;
import com.englishweb.api.services.repository.CourseRepository;
import com.englishweb.api.services.repository.ReviewRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.ReviewSpecification;
import com.englishweb.api.utils.ReviewUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ReviewServiceImpl implements ReviewService {
    private final ReviewRepository reviewRepository;
    private final ReviewMapper reviewMapper;
    private final UserCredentialRepository userCredentialRepository;
    private final CourseRepository courseRepository;

    @Override
    public Response<List<ReviewDTO>> getAllReviews(Map<String, String> params) {
        String pageStr = params.get("page");
        String limitStr = params.get("limit");
        if(StringUtils.isBlank(pageStr)) pageStr = "0";
        if(StringUtils.isBlank(limitStr)) limitStr = "10";
        Page<Review> page = reviewRepository.findAll(
                Specification.where(ReviewSpecification.hasCourse(params.get("courseId")))
                        .and(ReviewSpecification.hasPublic(params.get("isPublic"))),
                PageRequest.of(
                        Integer.parseInt(pageStr),
                        Integer.parseInt(limitStr),
                        Sort.by(Sort.Direction.DESC, "lastModifiedDate"))
        );
        return getListResponse(page.getContent());
    }

    private Response<List<ReviewDTO>> getListResponse(List<Review> reviews) {
        Map<String, List<ReviewDTO>> data = new HashMap<>();

        data.put("data", reviewMapper.toListDTO(reviews));

        return Response.<List<ReviewDTO>>builder()
                .message("Get all reviews successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteReviewById(String id) {
        if (id.isEmpty())
            throw new BadRequestException("Id is empty");

        reviewRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete review successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createReview(CreateReviewRequest createReviewRequest) {
        ReviewUtils.reviewValidate(createReviewRequest);

        UserCredential userCredential = userCredentialRepository.findById(createReviewRequest.getUserCredentialId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", createReviewRequest.getUserCredentialId())));

        Course course = courseRepository.findById(createReviewRequest.getCourseId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Course %s could not be found", createReviewRequest.getCourseId())));

        Review review = Review.builder()
                .content(createReviewRequest.getContent())
                .rating(createReviewRequest.getRating())
                .course(course)
                .image(createReviewRequest.getImage())
                .userCredential(userCredential)
                .build();
        try {
            reviewRepository.save(review);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message(String.format("Review %s created successfully", review.getId()))
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateReview(UpdateReviewRequest requestData) {
        Review review = reviewRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Review %s could not be found", requestData.getId())));

        if(Objects.nonNull(requestData.getContent())) review.setContent(requestData.getContent());
        if(Objects.nonNull(requestData.getRating())) review.setRating(requestData.getRating());
        if(Objects.nonNull(requestData.getIsPublic())) review.setIsPublic(requestData.getIsPublic());
        if(Objects.nonNull(requestData.getImage())) review.setImage(requestData.getImage());

        try {
            reviewRepository.save(review);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message(String.format("Review %s updated successfully", review.getId()))
                .statusCode(HttpStatus.OK.value())
                .build();
    }
}
