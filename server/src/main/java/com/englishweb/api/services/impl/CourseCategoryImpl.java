package com.englishweb.api.services.impl;

import com.englishweb.api.dto.CourseCategoryDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.CourseCategoryMapper;
import com.englishweb.api.models.CourseCategory;
import com.englishweb.api.request.CreateCourseCategoryRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CourseCategoryService;
import com.englishweb.api.services.repository.CourseCategoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class CourseCategoryImpl implements CourseCategoryService {
    private final CourseCategoryRepository courseCategoryRepository;
    private final CourseCategoryMapper courseCategoryMapper;

    @Override
    public Response<List<CourseCategoryDTO>> getAllCourseCategory() {
        List<CourseCategory> courseCategories = courseCategoryRepository.findAll();
        Map<String, List<CourseCategoryDTO>> data = new HashMap<>();
        data.put("data", courseCategoryMapper.toListDTO(courseCategories));

        return Response.<List<CourseCategoryDTO>>builder()
                .message("Get all course category successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<CourseCategoryDTO> getCourseCategoryById(String id) {
        CourseCategory courseCategory = courseCategoryRepository.findById(id)
                .orElseThrow(()
                        -> new NotFoundRequestException(String.format("Course category %s could not be found", id)));

        Map<String, CourseCategoryDTO> data = new HashMap<>();
        data.put("data", courseCategoryMapper.toDTO(courseCategory));

        return Response.<CourseCategoryDTO>builder()
                .message("Get course category by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteCourseCategory(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        courseCategoryRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete course category by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createCourseCategory(CreateCourseCategoryRequest request) {
        CourseCategory courseCategory = CourseCategory.builder()
                .courseCategoryName(request.getCourseCategoryName())
                .build();
        try {
            log.info("{ New course category: {} }", courseCategoryRepository.save(courseCategory));
        } catch (Exception e) {
            log.error("{ Error when creating course category: {} }", e.getMessage());
        }
        return Response.<Void>builder()
                .message("Create course category successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
