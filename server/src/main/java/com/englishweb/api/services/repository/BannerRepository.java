package com.englishweb.api.services.repository;

import com.englishweb.api.models.Banner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BannerRepository extends JpaRepository<Banner, String> {

}
