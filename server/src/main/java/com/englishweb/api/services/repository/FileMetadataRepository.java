package com.englishweb.api.services.repository;

import com.englishweb.api.models.FileMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileMetadataRepository extends JpaRepository<FileMetadata, String> {
}
