package com.englishweb.api.services.repository;

import com.englishweb.api.models.StaticPage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface StaticPageRepository extends JpaRepository<StaticPage, String>, JpaSpecificationExecutor<StaticPage> {
}
