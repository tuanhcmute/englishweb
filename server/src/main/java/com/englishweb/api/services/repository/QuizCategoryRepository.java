package com.englishweb.api.services.repository;

import com.englishweb.api.models.QuizCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuizCategoryRepository extends JpaRepository<QuizCategory, String>{
    
}
