package com.englishweb.api.services.repository;

import com.englishweb.api.models.Discount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiscountRepository extends JpaRepository<Discount, String> {

}
