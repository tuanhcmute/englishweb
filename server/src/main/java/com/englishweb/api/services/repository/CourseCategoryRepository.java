package com.englishweb.api.services.repository;

import com.englishweb.api.models.CourseCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseCategoryRepository extends JpaRepository<CourseCategory, String> {

}
