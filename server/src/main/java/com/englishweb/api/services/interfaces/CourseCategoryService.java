package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.CourseCategoryDTO;
import com.englishweb.api.request.CreateCourseCategoryRequest;
import com.englishweb.api.response.Response;

import java.util.List;

public interface CourseCategoryService {
    Response<List<CourseCategoryDTO>> getAllCourseCategory();

    Response<CourseCategoryDTO> getCourseCategoryById(String id);

    Response<Void> deleteCourseCategory(String id);

    Response<Void> createCourseCategory(CreateCourseCategoryRequest request);
}