package com.englishweb.api.services.repository;

import com.englishweb.api.models.AgentVersion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AgentVersionRepository extends JpaRepository<AgentVersion, String> {
    boolean existsByIsActive(Boolean isActive);

    Optional<AgentVersion> findByIsActive(Boolean isActive);
}
