package com.englishweb.api.services.repository;

import com.englishweb.api.models.Revenue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RevenueRepository extends JpaRepository<Revenue, String>, JpaSpecificationExecutor<Revenue> {
}
