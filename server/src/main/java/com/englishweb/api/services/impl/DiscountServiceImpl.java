package com.englishweb.api.services.impl;

import com.englishweb.api.dto.DiscountDTO;
import com.englishweb.api.enums.DiscountConditionType;
import com.englishweb.api.enums.DiscountStatus;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.DiscountMapper;
import com.englishweb.api.models.*;
import com.englishweb.api.request.CreateDiscountRequest;
import com.englishweb.api.request.DiscountRequest;
import com.englishweb.api.request.UpdateDiscountRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.DiscountService;
import com.englishweb.api.services.repository.CourseCategoryRepository;
import com.englishweb.api.services.repository.CourseRepository;
import com.englishweb.api.services.repository.DiscountConditionRepository;
import com.englishweb.api.services.repository.DiscountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class DiscountServiceImpl implements DiscountService {
    private final DiscountRepository discountRepository;
    private final CourseRepository courseRepository;
    private final DiscountMapper discountMapper;
    private final CourseCategoryRepository courseCategoryRepository;
    private final DiscountConditionRepository discountConditionRepository;

    @Override
//    @Transactional
    public Response<Void> assignDiscount(DiscountRequest requestData) {
        Discount discount = discountRepository.findById(requestData.getDiscountId())
                .orElseThrow(() -> new NotFoundRequestException("Discount %s could not be found"));

//        Save all discount condition
        List<DiscountCondition> discountConditions = new ArrayList<>();
        requestData.getConditions().forEach(item -> {
            DiscountCondition discountCondition = DiscountCondition.builder()
                    .percentDiscount(item.getPercentDiscount())
                    .type(item.getType())
                    .discount(discount)
                    .build();
            if(item.getType().equals("course")) {
                List<Course> courses = courseRepository.findAllById(item.getCourseIds());
                courses.forEach(course -> {
                    ConditionTarget conditionTarget = ConditionTarget.builder()
                            .course(course)
                            .discountCondition(discountCondition)
                            .build();
                    discountCondition.getConditionTargets().add(conditionTarget);
                });
            } else {
                List<CourseCategory> courseCategories = courseCategoryRepository.findAllById(item.getCategoryIds());
                courseCategories.forEach(courseCategory -> {
                    ConditionTarget conditionTarget = ConditionTarget.builder()
                            .courseCategory(courseCategory)
                            .discountCondition(discountCondition)
                            .build();
                    discountCondition.getConditionTargets().add(conditionTarget);
                });
            }
            discountConditions.add(discountCondition);
            discount.getDiscountConditions().add(discountCondition);
        });
        discountConditionRepository.saveAll(discountConditions);

//        Update discount
        discountRepository.save(discount);

        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Assign discount successfully")
                .build();
    }

    @Override
    public Response<List<DiscountDTO>> getAllDiscounts() {
        List<Discount> discounts = discountRepository.findAll(
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        Map<String, List<DiscountDTO>> data = new HashMap<>();
        data.put("data", discountMapper.toListDTO(discounts));

        return Response.<List<DiscountDTO>>builder()
                .message("Get all discounts successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> createDiscount(CreateDiscountRequest requestData) {
        LocalDateTime startDate = LocalDateTime.parse(requestData.getStartDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        LocalDateTime endDate = LocalDateTime.parse(requestData.getEndDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        Discount discount = Discount.builder()
                .discountName(requestData.getDiscountName())
                .startDate(startDate)
                .endDate(endDate)
                .status(requestData.getStatus())
                .build();
        try {
            discountRepository.save(discount);
            log.info("{ Save discount {} successfully }", discount.getId());

        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Create discount successfully")
                .build();
    }

    @Override
    public Response<Void> updateDiscount(UpdateDiscountRequest requestData) {
        Discount discount = discountRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException("Discount %s could not be found"));

        LocalDateTime startDate = LocalDateTime.parse(requestData.getStartDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        LocalDateTime endDate = LocalDateTime.parse(requestData.getEndDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);

        discount.setDiscountName(requestData.getDiscountName());
        discount.setStartDate(startDate);
        discount.setEndDate(endDate);
        discount.setStatus(requestData.getStatus());

//        Update discount
        discountRepository.save(discount);

//        Dispatch update discount
        updateDiscountStatus(discount);

        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Update discount successfully")
                .build();
    }

//    @Async
//    @EventListener
    @Override
    public void updateDiscountStatus(Discount discount) {
        log.info(discount.getStatus());
        _updateDiscountStatus(discount);
    }

    private void _updateDiscountStatus(Discount discount) {
        // Get all discount courses
        List<Course> courses = new ArrayList<>();
        Set<String> processedCourseIds = new HashSet<>();
        discount.getDiscountConditions().forEach(discountCondition ->
                discountCondition.getConditionTargets().forEach(conditionTarget -> {
                    if (DiscountConditionType.COURSE.getValue().equals(discountCondition.getType())) {
                        if(discount.getStatus().equals(DiscountStatus.RUNNING.getValue())) {
                            handleCourseCondition(courses, processedCourseIds, conditionTarget.getCourse(), true, discountCondition.getPercentDiscount());
                        } else {
                            handleCourseCondition(courses, processedCourseIds, conditionTarget.getCourse(), false, 0);
                        }
                    } else if (conditionTarget.getCourseCategory() != null) {
                        conditionTarget.getCourseCategory().getCourses().forEach(course -> {
                            if (!processedCourseIds.contains(course.getId())) {
//                                handleCourseCondition(courses, processedCourseIds, course);
                                if(discount.getStatus().equals(DiscountStatus.RUNNING.getValue())) {
                                    handleCourseCondition(courses, processedCourseIds, course, true, discountCondition.getPercentDiscount());
                                } else {
                                    handleCourseCondition(courses, processedCourseIds, course, false, 0);
                                }
                            }
                        });
                    }
                }));
//            Update all
        courseRepository.saveAll(courses);
        log.info("Updated " + courses.size() + " courses with discount reset.");
    }

    private void handleCourseCondition(List<Course> courses, Set<String> processedCourseIds, Course course, boolean isDiscount, Integer percentDiscount) {
        if (Objects.nonNull(course) && processedCourseIds.add(course.getId())) {
            Integer newPrice = course.getOldPrice() - (course.getOldPrice() * percentDiscount) / 100;
            course.setIsDiscount(isDiscount);
            course.setPercentDiscount(percentDiscount);
            course.setNewPrice(newPrice);
            courses.add(course);
        }
    }
}
