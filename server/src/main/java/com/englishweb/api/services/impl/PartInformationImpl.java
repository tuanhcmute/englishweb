package com.englishweb.api.services.impl;

import com.englishweb.api.dto.FullPartInformationDTO;
import com.englishweb.api.dto.PartInformationDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.PartInformationMapper;
import com.englishweb.api.models.PartInformation;
import com.englishweb.api.models.TestCategory;
import com.englishweb.api.request.CreatePartInformation;
import com.englishweb.api.request.UpdatePartInformation;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.PartInformationService;
import com.englishweb.api.services.repository.PartInformationRepository;
import com.englishweb.api.services.repository.TestCategoryRepository;
import com.englishweb.api.utils.PartInformationUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PartInformationImpl implements PartInformationService {
    private final PartInformationRepository partInformationRepository;
    private final TestCategoryRepository testCategoryRepository;
    private final PartInformationMapper partInformationMapper;

    @Override
    public Response<List<FullPartInformationDTO>> getAllPartInformation() {
        List<PartInformation> partInformation = partInformationRepository.findAll();
        return getListResponse(partInformation);
    }

    private Response<List<FullPartInformationDTO>> getListResponse(List<PartInformation> partInformation) {
        Map<String, List<FullPartInformationDTO>> data = new HashMap<>();
        data.put("data", partInformationMapper.toListDTO(partInformation));

        return Response.<List<FullPartInformationDTO>>builder()
                .message("Get all part information successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<List<FullPartInformationDTO>> getAllPartInformationByTestCategory(String testCategoryId) {
        Optional<TestCategory> testCategoryOptional = testCategoryRepository.findById(testCategoryId);
        if (testCategoryOptional.isEmpty()) {
            throw new BadRequestException("Test category could not be found");
        }
        List<PartInformation> listOfPartInformation = partInformationRepository
                .findAllByTestCategoryOrderByPartSequenceNumberAsc(testCategoryOptional.get());
        log.info("{Size: {}}", listOfPartInformation.size());
        return getListResponse(listOfPartInformation);
    }

    @Override
    public Response<PartInformationDTO> getPartInformationById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<PartInformation> optionalPartInformation = partInformationRepository.findById(id);

        if (optionalPartInformation.isEmpty())
            throw new BadRequestException("Can not found part information!");

        Map<String, PartInformationDTO> data = new HashMap<>();
        data.put("data", partInformationMapper.toDTO(optionalPartInformation.get()));

        return Response.<PartInformationDTO>builder()
                .message("Get part information by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deletePartInformation(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        partInformationRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete part information by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewPartInformation(CreatePartInformation request) {
        PartInformationUtils.partInformationValidate(request);

        Optional<TestCategory> testCategoryOptional = testCategoryRepository.findById(request.getTestCategoryId());
        if (testCategoryOptional.isEmpty()) throw new BadRequestException("Test category is not defined");

        PartInformation partInformation = PartInformation.builder()
                .testCategory(testCategoryOptional.get())
                .partInformationDescription(request.getPartInformationDescription())
                .partInformationName(request.getPartInformationName())
                .partSequenceNumber(request.getPartSequenceNumber())
                .totalQuestion(request.getTotalQuestion())
                .partType(request.getPartType())
                .build();

        try {
            partInformationRepository.save(partInformation);
            log.info("{ Create part information successfully: {} }", partInformation.getId());
        } catch (Exception e) {
            log.error("{ Error when updating part information: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Create new part information successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updatePartInformation(UpdatePartInformation partInformation) {
        Optional<PartInformation> optionalPartInformation = partInformationRepository.findById(partInformation.getId());
        if (optionalPartInformation.isEmpty())
            throw new BadRequestException("Can not found part information!");
        optionalPartInformation.get().setPartInformationDescription(partInformation.getPartInformationDescription());
        optionalPartInformation.get().setPartType(partInformation.getPartType());
        optionalPartInformation.get().setPartInformationName(partInformation.getPartInformationName());
        optionalPartInformation.get().setPartSequenceNumber(partInformation.getPartSequenceNumber());
        optionalPartInformation.get().setTotalQuestion(partInformation.getTotalQuestion());

        try {
            partInformationRepository.save(optionalPartInformation.get());
            log.info("{ Update part information successfully }");
        } catch (Exception e) {
            log.error("{ Error when updating part information: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message("Update part information successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
