package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.PaymentDTO;
import com.englishweb.api.request.CreatePayment;
import com.englishweb.api.response.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface PaymentService {
    Response<List<PaymentDTO>> getAllPayment();

    Response<PaymentDTO> getPaymentById(String id);

    Response<Void> deletePaymentById(String id);

    void createPayment(CreatePayment createPayment, HttpServletRequest request, HttpServletResponse response);
}
