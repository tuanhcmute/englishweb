package com.englishweb.api.services.repository;

import com.englishweb.api.models.UserCredential;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface UserCredentialRepository extends JpaRepository<UserCredential, String>, JpaSpecificationExecutor<UserCredential> {
    Optional<UserCredential> findByUsername(String username);

    Optional<UserCredential> findByEmail(String email);

    Optional<UserCredential> findByEmailOrUsername(String email, String username);

    Optional<UserCredential> findByEmailAndUsername(String email, String username);
}
