package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.ReviewDTO;
import com.englishweb.api.request.CreateReviewRequest;
import com.englishweb.api.request.UpdateReviewRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface ReviewService {
    Response<List<ReviewDTO>> getAllReviews(Map<String, String> params);

    Response<Void> deleteReviewById(String id);

    Response<Void> createReview(CreateReviewRequest createReviewRequest);

    Response<Void> updateReview(UpdateReviewRequest requestData);
}
