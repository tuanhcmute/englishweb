package com.englishweb.api.services.repository;

import com.englishweb.api.models.Comment;
import com.englishweb.api.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, String>, JpaSpecificationExecutor<Comment> {
    List<Comment> findAllByTestAndIsRoot(Test test, Boolean isRoot);

    List<Comment> findAllByIsFiltered(Boolean isFiltered);
}
