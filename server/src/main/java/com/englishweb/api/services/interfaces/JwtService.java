package com.englishweb.api.services.interfaces;

import org.springframework.security.core.Authentication;

public interface JwtService {
    String createToken(Authentication authentication);

    String getUsernameFromToken(String token);

    boolean validateToken(String token);
}


