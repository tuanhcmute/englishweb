package com.englishweb.api.services.impl;

import com.englishweb.api.dto.RoleDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.RoleMapper;
import com.englishweb.api.models.Role;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.RoleService;
import com.englishweb.api.services.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class RoleImpl implements RoleService {
    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    @Override
    public Response<List<RoleDTO>> getAllRoles() {
        List<Role> roles = roleRepository.findAll();

        Map<String, List<RoleDTO>> data = new HashMap<>();
        data.put("data", roleMapper.toListDTO(roles));

        return Response.<List<RoleDTO>>builder()
                .message("Get all roles successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<RoleDTO> getRoleById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Role %s could not be found", id)));

        Map<String, RoleDTO> data = new HashMap<>();
        data.put("data", roleMapper.toDTO(role));

        return Response.<RoleDTO>builder()
                .message("Get all roles successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteRole(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        roleRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete role successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
