package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.AuthDTO;
import com.englishweb.api.request.ForgotPasswordRequest;
import com.englishweb.api.request.SignUpRequest;
import com.englishweb.api.request.UserAuthRequest;
import com.englishweb.api.request.UserSocialAuthRequest;
import com.englishweb.api.response.Response;

public interface AuthService {

    Response<AuthDTO> login(UserAuthRequest request);

    Response<Void> signUp(SignUpRequest signUpRequest);

    Response<AuthDTO> loginWithSocial(UserSocialAuthRequest request);

    Response<Void> forgotPassword(ForgotPasswordRequest forgotPasswordRequest);

    Response<Void> sendOTPForgotPassword(String toEmail);


    Response<Void> emailVerification(String hash, String email);

    Response<Void> sendOTPEmailVerification(String toEmail);
}
