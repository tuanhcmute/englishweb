package com.englishweb.api.services.impl;

import com.englishweb.api.dto.CourseDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.CourseMapper;
import com.englishweb.api.models.Course;
import com.englishweb.api.models.CourseCategory;
import com.englishweb.api.models.CourseDetail;
import com.englishweb.api.models.Discount;
import com.englishweb.api.request.CreateCourseRequest;
import com.englishweb.api.request.UpdateCourseRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CourseService;
import com.englishweb.api.services.repository.CourseCategoryRepository;
import com.englishweb.api.services.repository.CourseDetailRepository;
import com.englishweb.api.services.repository.CourseRepository;
import com.englishweb.api.services.repository.DiscountRepository;
import com.englishweb.api.specification.CourseSpecification;
import com.englishweb.api.utils.CourseUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class CourseImpl implements CourseService {
    private final CourseRepository courseRepository;
    private final CourseCategoryRepository courseCategoryRepository;
    private final CourseDetailRepository courseDetailRepository;
    private final CourseMapper courseMapper;
    private final DiscountRepository discountRepository;

    @Override
    public Response<List<CourseDTO>> getAllCourse(Map<String, String> params) {
        List<Course> courses = courseRepository.findAll(
                Specification.where(CourseSpecification.hasActivated(params.get("isActive"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate"));
        return getCourseDTOResponse(courses);
    }

    @Override
    public Response<CourseDTO> getCourseById(String id) {
        Course course = courseRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Course %s could not be found", id)));

        Map<String, CourseDTO> data = new HashMap<>();
        data.put("data", courseMapper.toDTO(course));

        return Response.<CourseDTO>builder()
                .message("Get course by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteCourse(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        courseRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete course by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewCourse(CreateCourseRequest request) {
        CourseUtils.courseValidate(request);

        CourseCategory courseCategory = courseCategoryRepository.findById(request.getCourseCategoryId())
                .orElseThrow(() -> new NotFoundRequestException(
                        String.format("Course category %s could not be found", request.getCourseCategoryId())));

        Course course = Course.builder()
                .courseName(request.getCourseName())
                .newPrice(request.getOldPrice())
                .oldPrice(request.getOldPrice())
                .percentDiscount(0)
                .courseCategory(courseCategory)
                .isActive(request.getIsActive())
                .courseImg(request.getCourseImg())
                .build();

        CourseDetail courseDetail = CourseDetail.builder()
                .course(course)
                .build();

        try {
            courseDetailRepository.save(courseDetail);
            log.info("{ New course: {} }", course.getId());
            log.info("{ Course detail: {} }", courseDetail.getId());
        } catch (Exception e) {
            log.error("{ Error when creating course: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Create new course successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateCourse(UpdateCourseRequest request) {

        Course course = courseRepository.findById(request.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Course %s could not be found", request.getId())));

        course.setCourseName(request.getCourseName());
        course.setIsActive(request.getIsActive());
        course.setOldPrice(request.getOldPrice());
        course.setCourseImg(request.getCourseImg());

        try {
            courseRepository.save(course);
            log.info("{ Updated Course: {} }", course.getId());
        } catch (Exception e) {
            log.error("{Error when updating course: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message("Update course successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<List<CourseDTO>> getAllCoursesByDiscount(String value) {
        Discount discount = discountRepository.findById(value)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Discount %s could not be found", value)));
//        List<Course> courses = courseRepository.findAllByDiscount(discount);
        return getCourseDTOResponse(new ArrayList<>());
    }

    private Response<List<CourseDTO>> getCourseDTOResponse(List<Course> courses) {
        Map<String, List<CourseDTO>> data = new HashMap<>();
        data.put("data", courseMapper.toListDTO(courses));

        return Response.<List<CourseDTO>>builder()
                .message("Get all course successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }
}
