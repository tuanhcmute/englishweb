package com.englishweb.api.services.repository;

import com.englishweb.api.models.StudyingFlashcard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface StudyingFlashcardRepository extends JpaRepository<StudyingFlashcard, String>, JpaSpecificationExecutor<StudyingFlashcard> {
}
