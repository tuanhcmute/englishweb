package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.RevenueDTO;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface RevenueService {
    Response<List<RevenueDTO>> getAllRevenues(Map<String, String> params);

    void createRevenue();
}
