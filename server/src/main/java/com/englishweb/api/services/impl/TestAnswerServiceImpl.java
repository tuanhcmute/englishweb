package com.englishweb.api.services.impl;

import com.englishweb.api.dto.TestAnswerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.TestAnswerMapper;
import com.englishweb.api.models.TestAnswer;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.TestAnswerService;
import com.englishweb.api.services.repository.TestAnswerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class TestAnswerServiceImpl implements TestAnswerService {
    private final TestAnswerRepository testAnswerRepository;
    private final TestAnswerMapper testAnswerMapper;

    @Override
    public Response<List<TestAnswerDTO>> getAllTestAnswer() {
        List<TestAnswer> testAnswers = testAnswerRepository.findAll(
                Sort.by(Sort.Direction.DESC, "createdDate"));
        Map<String, List<TestAnswerDTO>> data = new HashMap<>();
        data.put("data", testAnswerMapper.toListAnswerDTO(testAnswers));

        return Response.<List<TestAnswerDTO>>builder()
                .message("Get all test answer successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<TestAnswerDTO> getTestAnswerById(String id) {
        TestAnswer testAnswer = testAnswerRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Can not find test answer"));

        Map<String, TestAnswerDTO> data = new HashMap<>();
        data.put("data", testAnswerMapper.toDTO(testAnswer));
        return Response.<TestAnswerDTO>builder()
                .message("Get test answer by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    @Transactional
    public Response<Void> deleteTestAnswer(String id) {
        testAnswerRepository.deleteById(id);
        return Response.<Void>builder()
                .message("Delete test answer by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
