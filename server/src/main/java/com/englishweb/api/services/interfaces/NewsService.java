package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.FullNewsDTO;
import com.englishweb.api.dto.NewsDTO;
import com.englishweb.api.request.CreateNewsRequest;
import com.englishweb.api.request.UpdateNewsRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface NewsService {
    Response<List<NewsDTO>> getAllNews(Map<String, String> params);

    Response<FullNewsDTO> getNewsById(String id);

    Response<Void> deleteNews(String id);

    Response<Void> createNews(CreateNewsRequest requestData);

    Response<Void> updateNews(UpdateNewsRequest requestData);

    Response<List<NewsDTO>> getTopNews(String count, String start);
}
