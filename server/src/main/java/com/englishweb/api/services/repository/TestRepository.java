package com.englishweb.api.services.repository;

import com.englishweb.api.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<Test, String> {

}
