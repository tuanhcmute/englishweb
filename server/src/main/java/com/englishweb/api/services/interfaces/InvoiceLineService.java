package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.InvoiceLineDTO;
import com.englishweb.api.request.CreateNewInvoiceLineRequest;
import com.englishweb.api.response.Response;

import java.util.List;

public interface InvoiceLineService {
    Response<List<InvoiceLineDTO>> getAllInvoiceLine();

    Response<InvoiceLineDTO> getInvoiceLineById(String id);

    Response<Void> deleteInvoiceLine(String id);

    Response<Void> createNewInvoiceLine(CreateNewInvoiceLineRequest createNewInvoiceLineRequest);
}
