package com.englishweb.api.services.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.englishweb.api.models.ReportComment;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ReportCommentRepository extends JpaRepository<ReportComment, String>, JpaSpecificationExecutor<ReportComment> {
    
}
