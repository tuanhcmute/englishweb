package com.englishweb.api.services.impl;

import com.englishweb.api.dto.QuizDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.QuizMapper;
import com.englishweb.api.models.Quiz;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.QuizService;
import com.englishweb.api.services.repository.QuizRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QuizServiceImpl implements QuizService{
    private final QuizRepository quizRepository;
    private final QuizMapper quizMapper;

    @Override
    public Response<List<QuizDTO>> getAllQuiz() {
        List<Quiz> quizes = quizRepository.findAll();
        Map<String, List<QuizDTO>> data = new HashMap<>();

        data.put("data", quizMapper.toListDTO(quizes));
        
        return Response.<List<QuizDTO>>builder()
            .statusCode(HttpStatus.OK.value())
            .message("Get all quizzes successfully!")
            .data(data)
            .build();
    }

    @Override
    public Response<QuizDTO> getQuizById(String id) {
        if (id.trim().isEmpty())
            throw new BadRequestException("Id value is empty!");

        Optional<Quiz> quizOptional = quizRepository.findById(id);

        if (quizOptional.isEmpty())
            throw new BadRequestException("Can not find quiz!");

        Map<String, QuizDTO> data = new HashMap<>();
        data.put("data", quizMapper.toDTO(quizOptional.get()));
        
        return Response.<QuizDTO>builder()
        .statusCode(HttpStatus.OK.value())
        .message("Get quiz by id successfully!")
        .data(data)
        .build();
    }

    @Override
    public Response<Void> deleteQuizById(String id) {
        if (id.trim().isEmpty())
            throw new BadRequestException("Id value is empty!");
        
        quizRepository.deleteById(id);

        return Response.<Void>builder()
        .statusCode(HttpStatus.OK.value())
        .message("Delete quiz by id successfully!")
        .build();
    }
    
}
