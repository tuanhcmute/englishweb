package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.LessonNoteDTO;
import com.englishweb.api.request.CreateLessonNoteRequest;
import com.englishweb.api.request.UpdateLessonNoteRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface LessonNoteService {
    Response<Void> createLessonNote(CreateLessonNoteRequest requestData);

    Response<List<LessonNoteDTO>> getAllLessonNotes(Map<String, String> params);

    Response<Void> updateLessonNote(UpdateLessonNoteRequest requestData);

    Response<Void> deleteLessonNote(String id);
}
