package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.DiscountConditionDTO;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface DiscountConditionService {
    Response<List<DiscountConditionDTO>> getAllDiscountConditions(Map<String, String> params);
}
