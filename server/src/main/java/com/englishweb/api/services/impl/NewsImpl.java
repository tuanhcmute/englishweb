package com.englishweb.api.services.impl;

import com.englishweb.api.dto.FullNewsDTO;
import com.englishweb.api.dto.NewsDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.NewsMapper;
import com.englishweb.api.models.News;
import com.englishweb.api.models.NewsCategory;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateNewsRequest;
import com.englishweb.api.request.UpdateNewsRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.NewsService;
import com.englishweb.api.services.repository.NewsCategoryRepository;
import com.englishweb.api.services.repository.NewsRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.NewsSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class NewsImpl implements NewsService {
    private final NewsRepository newsRepository;
    private final NewsCategoryRepository newsCategoryRepository;
    private final UserCredentialRepository userCredentialRepository;
    private final NewsMapper newsMapper;

    @Override
    public Response<List<NewsDTO>> getAllNews(Map<String, String> params) {
        String pageStr = params.get("page");
        String limitStr = params.get("limit");
        if(StringUtils.isBlank(pageStr)) pageStr = "0";
        if(StringUtils.isBlank(limitStr)) limitStr = "10";

        try {
            Page<News> page = newsRepository.findAll(
                    Specification.where(NewsSpecification.hasCategory(params.get("categoryId"))),
                    PageRequest.of(
                            Integer.parseInt(pageStr),
                            Integer.parseInt(limitStr),
                            Sort.by(Sort.Direction.DESC, "lastModifiedDate"))
            );
            return getListResponse(page.getContent());
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public Response<FullNewsDTO> getNewsById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<News> newsOptional = newsRepository.findById(id);
        if (newsOptional.isEmpty())
            throw new BadRequestException("News is null");

        Map<String, FullNewsDTO> data = new HashMap<>();
        data.put("data", newsMapper.toFullDTO(newsOptional.get()));

        return Response.<FullNewsDTO>builder()
                .message("Get news by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteNews(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");
        newsRepository.deleteById(id);
        return Response.<Void>builder()
                .message("Delete news by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNews(CreateNewsRequest requestData) {
        Optional<NewsCategory> newsCategoryOptional = newsCategoryRepository.findById(requestData.getNewsCategoryId());
        if (newsCategoryOptional.isEmpty()) throw new BadRequestException("News category could not be found");

        Optional<UserCredential> userCredentialOptional = userCredentialRepository.findById(requestData.getUserCredentialId());
        if (userCredentialOptional.isEmpty()) throw new BadRequestException("User credential could not be found");

        News news = News.builder()
                .status(requestData.getStatus())
                .newsDescription(requestData.getNewsDescription())
                .newsTitle(requestData.getNewsTitle())
                .newsImage(requestData.getNewsImage())
                .newsCategory(newsCategoryOptional.get())
                .author(userCredentialOptional.get())
                .build();

        try {
            newsRepository.save(news);
            log.info(news.getId());
        } catch (Exception e) {
            log.error("{ Error when creating news: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Create news successful!")
                .statusCode(HttpStatus.OK.value())
                .build();

    }

    @Override
    public Response<Void> updateNews(UpdateNewsRequest requestData) {
        Optional<News> newsOptional = newsRepository.findById(requestData.getId());
        if (newsOptional.isEmpty())
            throw new BadRequestException("News is null");

        newsOptional.get().setNewsTitle(requestData.getNewsTitle());
        newsOptional.get().setNewsDescription(requestData.getNewsDescription());
        newsOptional.get().setStatus(requestData.getStatus());
        newsOptional.get().setNewsImage(requestData.getNewsImage());
        newsOptional.get().setNewsContentHtml(requestData.getNewsContentHtml());

        try {
            newsRepository.save(newsOptional.get());
            log.info("{ News updated successfully }");
        } catch (Exception e) {
            log.error("{ Error when updating news: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Update news successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<List<NewsDTO>> getTopNews(String count, String start) {
        List<News> newsRes = new ArrayList<>();
        try {
            Page<News> newsPage = newsRepository.findAll(PageRequest.of(0, Integer.parseInt(count)));
            List<News> newsList = newsPage.getContent();

            if (newsList.size() >= Integer.parseInt(start)) {
                newsRes = newsList.subList(Integer.parseInt(start), newsList.size());
            }
        } catch (Exception e) {
            log.error("{ Error when getting top news: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return getListResponse(newsRes);

    }

    private Response<List<NewsDTO>> getListResponse(List<News> newsRes) {
        Map<String, List<NewsDTO>> data = new HashMap<>();
        data.put("data", newsMapper.toListDTO(newsRes));

        return Response.<List<NewsDTO>>builder()
                .message("Get all news successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

}
