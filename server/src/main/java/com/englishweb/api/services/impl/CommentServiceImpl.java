package com.englishweb.api.services.impl;

import com.englishweb.api.dto.CommentDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.CommentMapper;
import com.englishweb.api.models.Comment;
import com.englishweb.api.models.SubComment;
import com.englishweb.api.models.Test;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateCommentRequest;
import com.englishweb.api.request.UpdateCommentRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CommentService;
import com.englishweb.api.services.repository.CommentRepository;
import com.englishweb.api.services.repository.TestRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.CommentSpecification;
import com.google.cloud.language.v1.Document;
import com.google.cloud.language.v1.LanguageServiceClient;
import com.google.cloud.language.v1.Sentiment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final TestRepository testRepository;
    private final UserCredentialRepository userCredentialRepository;
    private final CommentMapper commentMapper;
    private final LanguageServiceClient languageServiceClient;

    @Override
    public Response<List<CommentDTO>> getAllComments(Map<String, String> params) {
        List<Comment> comments = commentRepository.findAll(
                Specification.where(CommentSpecification.hasTest(params.get("testId")))
                        .and(CommentSpecification.hasRoot(params.get("isRoot")))
                        .and(CommentSpecification.hasFiltered(params.get("isFiltered"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        Map<String, List<CommentDTO>> data = new HashMap<>();
        data.put("data", commentMapper.toListDTO(comments));

        return Response.<List<CommentDTO>>builder()
                .message("Get all comment successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<CommentDTO> getCommentById(String id) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Comment %s could not be found", id)));

        Map<String, CommentDTO> data = new HashMap<>();
        data.put("data", commentMapper.toDTO(comment));

        return Response.<CommentDTO>builder()
                .message("Get comment by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteComment(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        commentRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete comment by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<List<CommentDTO>> getAllChildCommentsByParentComment(String commentId) {
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Comment %s could not be found", commentId)));

        Set<SubComment> subComments = comment.getSubComments();
        List<Comment> childComments = subComments.stream().map(SubComment::getComment).toList();

        Map<String, List<CommentDTO>> data = new HashMap<>();
        data.put("data", commentMapper.toListDTO(childComments));

        return Response.<List<CommentDTO>>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Get all child comments by comment id %s successfully!", commentId))
                .data(data)
                .build();
    }

    @Override
    @Transactional
    public Response<Void> createComment(CreateCommentRequest requestData) {
        UserCredential userCredential = userCredentialRepository.findById(requestData.getAuthorId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", requestData.getAuthorId())));

        Test test = testRepository.findById(requestData.getTestId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Test %s could not be found", requestData.getTestId())));

        Comment comment = Comment.builder()
                .content(requestData.getContent())
                .author(userCredential)
                .test(test)
                .build();
        commentRepository.save(comment);

        commentRepository.findById(requestData.getParentCommentId())
                .ifPresentOrElse(item -> {
                    // true
                    SubComment subComment = SubComment.builder()
                            .comment(comment)
                            .parentComment(item)
                            .build();
                    item.getSubComments().add(subComment);
                    commentRepository.save(item);
                }, () -> {
                    //                    False
                    comment.setIsRoot(true);
                    commentRepository.save(comment);
                });
        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Comment %s created successfully!", comment.getId()))
                .build();
    }

    @Override
    public boolean isNegativeComment(String comment) {
        Document doc = Document.newBuilder().setContent(comment).setType(Document.Type.PLAIN_TEXT).build();
        Sentiment sentiment = languageServiceClient.analyzeSentiment(doc).getDocumentSentiment();

        // Define a threshold for sentiment score
        float threshold = (float) -0.5; // Comments with sentiment scores below this threshold are considered toxic

        // Consider comments with a negative sentiment score less than -0.5 as negative
        return sentiment.getScore() < threshold;
    }

    @Transactional
    @Override
    public Response<Void> updateComment(UpdateCommentRequest requestData) {
        Comment comment = commentRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Comment %s could not be found", requestData.getId())));

        comment.setContent(requestData.getContent());
        comment.setIsNegative(requestData.getIsNegative());
        comment.setIsFiltered(true);
        comment.setIsReview(requestData.getIsReview());

        commentRepository.save(comment);
        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Comment %s updated successfully!", comment.getId()))
                .build();
    }

}
