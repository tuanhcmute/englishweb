package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.UserAnswerDTO;
import com.englishweb.api.request.CreateNewUserAnswer;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface UserAnswerService {
    Response<Void> createNewUserAnswer(CreateNewUserAnswer request);

    Response<UserAnswerDTO> getUserAnswerById(String id);

    Response<Long> countByTest(String value);

    Response<List<UserAnswerDTO>> getAllUserAnswers(Map<String, String> params);
}
