package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.FullTestAnswerDTO;
import com.englishweb.api.models.FullTestAnswer;
import com.englishweb.api.models.Test;
import com.englishweb.api.response.Response;

import java.util.List;

public interface FullTestAnswerService {
    Response<List<FullTestAnswer>> getAllTestAnswer();

    Response<FullTestAnswer> getFullTestAnswerById(String id);

    Response<Void> deleteFullTestAnswer(String id);

    Response<FullTestAnswerDTO> getFullTestAnswerByTest(String value);

    void createFullTestAnswer(Test test);
}
