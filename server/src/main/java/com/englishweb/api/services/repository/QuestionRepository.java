package com.englishweb.api.services.repository;

import com.englishweb.api.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, String> {

}
