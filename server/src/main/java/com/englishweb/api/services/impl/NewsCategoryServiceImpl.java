package com.englishweb.api.services.impl;

import com.englishweb.api.dto.NewsCategoryDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.NewsCategoryMapper;
import com.englishweb.api.models.NewsCategory;
import com.englishweb.api.request.CreateNewsCategory;
import com.englishweb.api.request.UpdateNewsCategory;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.NewsCategoryService;
import com.englishweb.api.services.repository.NewsCategoryRepository;
import com.englishweb.api.specification.NewsCategorySpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class NewsCategoryServiceImpl implements NewsCategoryService {
    private final NewsCategoryRepository newsCategoryRepository;
    private final NewsCategoryMapper newsCategoryMapper;

    @Override
    public Response<List<NewsCategoryDTO>> getAllNewsCategories(Map<String, String> params) {
        List<NewsCategory> news = newsCategoryRepository.findAll(
                Specification.where(NewsCategorySpecification.hasStatus(params.get("status"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate"));

        Map<String, List<NewsCategoryDTO>> data = new HashMap<>();
        data.put("data", newsCategoryMapper.toListDTO(news));

        return Response.<List<NewsCategoryDTO>>builder()
                .message("Get all news category successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> createNewsCategory(CreateNewsCategory requestData) {
        NewsCategory newsCategory = NewsCategory.builder()
                .newsCategoryName(requestData.getNewsCategoryName())
                .build();

        try {
            newsCategoryRepository.save(newsCategory);
            log.info(newsCategory.getId());
        } catch (Exception e) {
            log.error("{ Error when creating news category: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Create news category successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateNewsCategory(UpdateNewsCategory requestData) {
        NewsCategory newsCategory = newsCategoryRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("News category %s could not be found", requestData.getId())));

        newsCategory.setStatus(requestData.getStatus());
        newsCategory.setNewsCategoryName(requestData.getNewsCategoryName());

        try {
            newsCategoryRepository.save(newsCategory);
            log.info(newsCategory.getId());
        } catch (Exception e) {
            log.error("{ Error when updating news category: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("News category updated successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> deleteNewsCategory(String id) {
        try {
            newsCategoryRepository.deleteById(id);
            log.error("{News category {} deleted successfully }", id);
        } catch (Exception e) {
            log.error("{ Error when deleting news category: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("News category deleted successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }
}
