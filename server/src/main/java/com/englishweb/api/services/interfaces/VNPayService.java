package com.englishweb.api.services.interfaces;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface VNPayService {
    String createPayment(int totalAmount, String urlReturn, HttpServletRequest request);

    void returnVNPay(HttpServletRequest request,
                     HttpServletResponse response,
                     String courseId,
                     String userCredentialId);
}
