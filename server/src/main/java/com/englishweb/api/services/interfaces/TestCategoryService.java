package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.TestCategoryDTO;
import com.englishweb.api.request.CreateTestCategory;
import com.englishweb.api.request.UpdateTestCategory;
import com.englishweb.api.response.Response;

import java.util.List;

public interface TestCategoryService {
    Response<List<TestCategoryDTO>> getAllTestCategory();

    Response<TestCategoryDTO> getTestCategoryById(String id);

    Response<Void> deleteTestCategory(String id);

    Response<Void> createNewTestCategory(CreateTestCategory testCategory);

    Response<Void> updateTestCategory(UpdateTestCategory request);
}

