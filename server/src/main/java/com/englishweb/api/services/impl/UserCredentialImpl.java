package com.englishweb.api.services.impl;

import com.englishweb.api.dto.UserCredentialDTO;
import com.englishweb.api.enums.ErrorStatus;
import com.englishweb.api.enums.Provider;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.UserCredentialMapper;
import com.englishweb.api.models.Role;
import com.englishweb.api.models.UserAuthority;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.SignUpRequest;
import com.englishweb.api.request.UpdateUserInfoRequest;
import com.englishweb.api.request.UpdateUserRoleRequest;
import com.englishweb.api.response.ErrorResponse;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.UserCredentialService;
import com.englishweb.api.services.repository.RoleRepository;
import com.englishweb.api.services.repository.UserAuthorityRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.UserCredentialSpecification;
import com.englishweb.api.utils.UserInfoUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserCredentialImpl implements UserCredentialService {

    private final UserCredentialRepository userCredentialRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserCredentialMapper userCredentialMapper;
    private final UserAuthorityRepository userAuthorityRepository;

    @Override
    public Response<List<UserCredentialDTO>> getAllUserCredential(Map<String, String> params) {
        List<UserCredential> userCredentials = userCredentialRepository.findAll(
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );

        Map<String, List<UserCredentialDTO>> data = new HashMap<>();
        data.put("data", userCredentialMapper.toListDTO(userCredentials));

        return Response.<List<UserCredentialDTO>>builder()
                .message("Get all user credential successfully")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    private Response<UserCredentialDTO> getUserCredentialDTOResponse(UserCredential userCredential) {
        Map<String, UserCredentialDTO> data = new HashMap<>();
        data.put("data", userCredentialMapper.toDTO(userCredential));

        return Response.<UserCredentialDTO>builder()
                .message("Get all user credential successfully")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteUserCredential(String id) {
        if (id.isEmpty())
            throw new BadRequestException("Id is null");

        userCredentialRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete user credential by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewUser(SignUpRequest signUpRequest) {
        // Validate email
        userCredentialRepository.findByEmail(signUpRequest.getEmail()).ifPresent(item -> {
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .errorCode(ErrorStatus.EMAIL_EXIST.getErrorCode())
                    .errorMessage(ErrorStatus.EMAIL_EXIST.toString())
                    .build();
            throw new BadRequestException(errorResponse, "Email already exists!");
        });

        // Validate username
        userCredentialRepository.findByUsername(signUpRequest.getUsername()).ifPresent(
                item -> {
                    ErrorResponse errorResponse = ErrorResponse.builder()
                            .errorCode(ErrorStatus.USERNAME_EXIST.getErrorCode())
                            .errorMessage(ErrorStatus.USERNAME_EXIST.toString())
                            .build();
                    throw new BadRequestException(errorResponse, "Username already exists!");
                }
        );

        // Validate role
        Role role = roleRepository.findByRoleName(com.englishweb.api.enums.Role.ROLE_STUDENT)
                .orElseThrow(() -> {
                    ErrorResponse errorResponse = ErrorResponse.builder()
                            .errorCode(ErrorStatus.ROLE_NOT_FOUND.getErrorCode())
                            .errorMessage(ErrorStatus.ROLE_NOT_FOUND.toString())
                            .build();
                    return new BadRequestException(errorResponse, String.format("Role %s could not be found!", signUpRequest.getProvider()));
                });

//        Set<Role> roles = new HashSet<>();
//        roles.add(role);

        // Validate provider
        boolean providerMatch = Arrays.stream(Provider.values())
                .anyMatch(value -> value.getValue().equals(signUpRequest.getProvider()));
        if (!providerMatch) {
            ErrorResponse errorResponse = ErrorResponse.builder()
                    .errorCode(ErrorStatus.PROVIDER_NOT_FOUND.getErrorCode())
                    .errorMessage(ErrorStatus.PROVIDER_NOT_FOUND.toString())
                    .build();
            throw new BadRequestException(errorResponse, String.format("%s provider is not supported!", signUpRequest.getProvider()));
        }

        UserCredential userCredential = UserCredential.builder()
                .description(String.format("New user: %s", signUpRequest.getUsername()))
                .email(signUpRequest.getEmail())
                .password(passwordEncoder.encode(signUpRequest.getPassword()))
                .roleName(com.englishweb.api.enums.Role.ROLE_STUDENT.name())
//                .roles(roles)
                .username(signUpRequest.getUsername())
                .verified(false)
                .provider(signUpRequest.getProvider())
                .fullName(signUpRequest.getFullName())
                .build();

        if (signUpRequest.getPhoneNumber() != null)
            userCredential.setPhoneNumber(signUpRequest.getPhoneNumber());

        if (signUpRequest.getAvatar() != null)
            userCredential.setAvatar(signUpRequest.getAvatar());

        UserAuthority userAuthority = UserAuthority.builder()
                .userCredential(userCredential)
                .role(role)
                .build();

        try {
            userCredentialRepository.save(userCredential);
            log.info("{ New user: {} }", userCredential.getId());

            userAuthorityRepository.save(userAuthority);
            log.info("{ New user authority: {} }", userAuthority.getId());
        } catch (Exception e) {
            log.error("{ Error when creating new user: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Create new account successfully!")
                .statusCode(HttpStatus.CREATED.value())
                .build();
    }

    @Override
    public Response<Long> countAll() {
        Long result = userCredentialRepository.count();
        Map<String, Long> data = new HashMap<>();
        data.put("data", result);

        return Response.<Long>builder()
                .message("Count all user credential successfully")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> updateUserInfo(UpdateUserInfoRequest userInfoRequest) {
        UserInfoUtils.userInfoValdate(userInfoRequest);

        UserCredential userCredential = userCredentialRepository.findById(userInfoRequest.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", userInfoRequest.getId())));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(userInfoRequest.getDob(), formatter);

        userCredential.setFullName(userInfoRequest.getFullName());
        userCredential.setPhoneNumber(userInfoRequest.getPhoneNumber());
        userCredential.setDob(date);
        userCredential.setAddress(userInfoRequest.getAddress());
        userCredential.setGender(userInfoRequest.getGender());
        userCredential.setMajor(userInfoRequest.getMajor());

        userCredentialRepository.save(userCredential);

        return Response.<Void>builder()
                .message("Update user info successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateUserRole(String id, UpdateUserRoleRequest requestData) {
        UserCredential userCredential = userCredentialRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", id)));
        Role role = roleRepository.findById(requestData.getRoleId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Role %s could not be found", requestData.getRoleId())));

        UserAuthority userAuthority = userAuthorityRepository.findByUserCredential(userCredential)
                .orElseThrow(() -> new NotFoundRequestException("User authority could not be found"));

        userAuthority.setRole(role);
        userAuthorityRepository.save(userAuthority);

        userCredential.setRoleName(role.getRoleName().getValue());
        userCredentialRepository.save(userCredential);

        return Response.<Void>builder()
                .message(String.format("User authority updated successfully with user %s and role: %s",
                        userCredential.getUsername(), role.getRoleName().name()))
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<UserCredentialDTO> getUserCredential(String id, String username) {
        UserCredential userCredential = userCredentialRepository.findOne(
                Specification.where(UserCredentialSpecification.hasId(id))
                        .or(UserCredentialSpecification.hasUsername(username))
        ).orElseThrow(() -> new NotFoundRequestException("User credential could not be found"));

        return getUserCredentialDTOResponse(userCredential);
    }
}
