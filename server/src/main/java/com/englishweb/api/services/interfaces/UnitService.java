package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.UnitDTO;
import com.englishweb.api.request.CreateUnitRequest;
import com.englishweb.api.request.UpdateUnitRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface UnitService {
    Response<List<UnitDTO>> getAllUnits(Map<String, String> params);

    Response<UnitDTO> getUnitById(String id);

    Response<Void> deleteUnitById(String id);

    Response<Void> createNewUnit(CreateUnitRequest createUnitRequest);

    Response<Void> updateUnit(UpdateUnitRequest requestData);
}
