package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.CommentDTO;
import com.englishweb.api.request.CreateCommentRequest;
import com.englishweb.api.request.UpdateCommentRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface CommentService {
    Response<List<CommentDTO>> getAllComments(Map<String, String> params);

    Response<CommentDTO> getCommentById(String id);

    Response<Void> deleteComment(String id);

    Response<List<CommentDTO>> getAllChildCommentsByParentComment(String id);

    Response<Void> createComment(CreateCommentRequest requestData);

    boolean isNegativeComment(String comment);

    Response<Void> updateComment(UpdateCommentRequest requestData);
}
