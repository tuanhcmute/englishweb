package com.englishweb.api.services.impl;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.Order;
import com.englishweb.api.models.Payment;
import com.englishweb.api.request.CreateNewInvoiceRequest;
import com.englishweb.api.request.CreateOrder;
import com.englishweb.api.services.interfaces.InvoiceService;
import com.englishweb.api.services.interfaces.OrderService;
import com.englishweb.api.services.interfaces.VNPayService;
import com.englishweb.api.services.repository.PaymentRepository;
import com.englishweb.api.utils.VNPayUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class VNPayServiceImpl implements VNPayService {
    private final PaymentRepository paymentRepository;
    private final OrderService orderService;
    private final InvoiceService invoiceService;

    @Override
    public String createPayment(int total, String urlReturn, HttpServletRequest request) {
        log.info("{ Return url: {} }", urlReturn);
        String vnp_Version = "2.1.0";
        String vnp_Command = "pay";
        String vnp_TxnRef = VNPayUtils.getRandomNumber(8);
        String vnp_IpAddr = VNPayUtils.getIpAddress(request);
        log.info("{ vnp_IpAddr: {} }", vnp_IpAddr);
        String vnp_TmnCode = VNPayUtils.vnp_TmnCode;
        String orderType = "billpayment";

        Map<String, String> vnp_Params = new HashMap<>();
        vnp_Params.put("vnp_Version", vnp_Version);
        vnp_Params.put("vnp_Command", vnp_Command);
        vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
        vnp_Params.put("vnp_Amount", String.valueOf(total * 100));
        vnp_Params.put("vnp_CurrCode", "VND");

        vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
        vnp_Params.put("vnp_OrderInfo", String.format("Thanh toan cho ma GD: %s", vnp_TxnRef));
        vnp_Params.put("vnp_OrderType", orderType);

        String locate = "vn";
        vnp_Params.put("vnp_Locale", locate);

        urlReturn += VNPayUtils.vnp_ReturnUrl;
        vnp_Params.put("vnp_ReturnUrl", urlReturn);
        vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

        ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of("GMT+7"));

        // Format the current time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String vnp_CreateDate = currentTime.format(formatter);
        vnp_Params.put("vnp_CreateDate", vnp_CreateDate);

        ZonedDateTime fifteenMinutesAfter = currentTime.plusMinutes(15);
        String vnp_ExpireDate = fifteenMinutesAfter.format(formatter);
        vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);

        List<String> fieldNames = new ArrayList<>(vnp_Params.keySet());
        Collections.sort(fieldNames);
        StringBuilder hashData = new StringBuilder();
        StringBuilder query = new StringBuilder();
        Iterator<String> itr = fieldNames.iterator();
        while (itr.hasNext()) {
            String fieldName = itr.next();
            String fieldValue = vnp_Params.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                //Build hash data
                hashData.append(fieldName);
                hashData.append('=');
                hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII));
                //Build query
                query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII));
                query.append('=');
                query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII));
                if (itr.hasNext()) {
                    query.append('&');
                    hashData.append('&');
                }
            }
        }
        String queryUrl = query.toString();
        String vnp_SecureHash = VNPayUtils.hmacSHA512(VNPayUtils.secretKey, hashData.toString());
        queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
        return VNPayUtils.vnp_PayUrl + "?" + queryUrl;
    }

    private int getVNPayStatus(HttpServletRequest request) {

        if ("00".equals(request.getParameter("vnp_TransactionStatus"))) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void returnVNPay(HttpServletRequest request,
                            HttpServletResponse response,
                            String courseId,
                            String userCredentialId) {
        int paymentStatus = this.getVNPayStatus(request);
        String orderInfo = request.getParameter("vnp_OrderInfo");
        String paymentTime = request.getParameter("vnp_PayDate");
        String transactionId = request.getParameter("vnp_TransactionNo");
        int totalAmountParam = Integer.parseInt(request.getParameter("vnp_Amount"));
        String requestUrl = request.getRequestURL().toString().replace("/v1.0.0/vnpay", "");
        String totalAmount = String.valueOf(totalAmountParam / 100);
        log.info("{ Request url: {} }", requestUrl);

//        Send success
        if (paymentStatus == 1) {
//        Create payment
            Payment payment = Payment.builder()
                    .paymentTime(paymentTime)
                    .transactionId(transactionId)
                    .orderInfo(orderInfo)
                    .totalAmount(totalAmount)
                    .build();
            try {
                paymentRepository.save(payment);
                // Create order
                Order order = orderService.createOrder(CreateOrder.builder()
                        .courseId(courseId)
                        .userCredentialId(userCredentialId)
                        .build());
                // Create invoice
                invoiceService.createNewInvoice(CreateNewInvoiceRequest.builder()
                        .paymentId(payment.getId())
                        .orderId(order.getId())
                        .build());
                log.info("{ New payment: {} }", payment.getId());
            } catch (Exception e) {
                log.error("{ Error when creating payment: {} }", e.getMessage());
                throw new BadRequestException(e.getMessage());
            }

            try {
                response.sendRedirect(String.format("%s/checkout/success", requestUrl));
            } catch (IOException e) {
                log.error("{ Error when redirect url: {} }", e.getMessage());
                throw new BadRequestException(e.getMessage());
            }
        } else {
            // send fail
            try {
                response.sendRedirect(String.format("%s/checkout/failure", requestUrl));
            } catch (IOException e) {
                log.error("{ Error when redirect payment fail url: {} }", e.getMessage());
                throw new BadRequestException(e.getMessage());
            }
        }
    }
}
