package com.englishweb.api.services.impl;

import com.englishweb.api.dto.LessonDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.LessonMapper;
import com.englishweb.api.models.Lesson;
import com.englishweb.api.models.Unit;
import com.englishweb.api.request.CreateLessonRequest;
import com.englishweb.api.request.UpdateLessonRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.LessonService;
import com.englishweb.api.services.interfaces.VideoService;
import com.englishweb.api.services.repository.LessonRepository;
import com.englishweb.api.services.repository.UnitRepository;
import com.englishweb.api.utils.LessonUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {
    private final LessonRepository lessonRepository;
    private final LessonMapper lessonMapper;
    private final UnitRepository unitRepository;
    private final VideoService videoService;

    @Value("${app.minio.fileUrl}")
    private String fileUrl;

    @Override
    public Response<List<LessonDTO>> getAllLessons() {
        List<Lesson> lessons = lessonRepository.findAll();
        Map<String, List<LessonDTO>> data = new HashMap<>();
        data.put("data", lessonMapper.toListDTO(lessons));

        return Response.<List<LessonDTO>>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get all lesson successfully!")
                .data(data)
                .build();
    }

    @Override
    public Response<LessonDTO> getLessonById(String id) {
        Lesson lesson = lessonRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Lesson %s could not be found", id)));

        Map<String, LessonDTO> data = new HashMap<>();
        data.put("data", lessonMapper.toDTO(lesson));

        return Response.<LessonDTO>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get lesson by id successfully!")
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteLessonById(String id) {
        if (id.isEmpty())
            throw new BadRequestException("Id value is empty");

        lessonRepository.deleteById(id);

        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Delete lesson by id successfully!")
                .build();
    }

    @Override
    public Response<Void> createNewLesson(CreateLessonRequest createLessonRequest) {
        LessonUtils.validateCreateNewLessonRequestModel(createLessonRequest);
        Unit unit = unitRepository.findById(createLessonRequest.getUnitId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Unit %s could not be found", createLessonRequest.getUnitId())));

        int currentSize = lessonRepository.countAllByUnit(unit);
        Lesson lesson = Lesson.builder()
                .title(createLessonRequest.getTitle())
                .unit(unit)
                .sequence(currentSize + 1)
                .build();
        lessonRepository.save(lesson);

        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Lesson %s created successfully", lesson.getId()))
                .build();
    }

    @Override
    public Response<Void> updateLesson(UpdateLessonRequest requestData) {
        Lesson lesson = lessonRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException(
                        String.format("Lesson %s could not be found", requestData.getId())));

        lesson.setTitle(requestData.getTitle());
        lesson.setContent(requestData.getContent());
        lesson.setVideo(requestData.getVideo());

        lessonRepository.save(lesson);

        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Lesson %s updated successfully", lesson.getId()))
                .build();

    }

    @Override
    public Response<Void> uploadVideoLesson(String id, MultipartFile file) {
        Lesson lesson = lessonRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Lesson %s could not be found", id)));
        String fileMetaDataId = videoService.save(file);
        lesson.setVideo(String.format(fileUrl, fileMetaDataId));
        lessonRepository.save(lesson);

        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Video Lesson %s created successfully", lesson.getVideo()))
                .build();
    }

}
