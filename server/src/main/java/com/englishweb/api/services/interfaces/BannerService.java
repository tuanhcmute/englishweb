package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.BannerDTO;
import com.englishweb.api.request.CreateBannerItemRequest;
import com.englishweb.api.request.UpdateBannerRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface BannerService {
    Response<List<BannerDTO>> getAllBanners(Map<String, String> params);

    Response<BannerDTO> getBannerById(String id);

    Response<Void> deleteBannerById(String id);

    Response<Void> createBanner(CreateBannerItemRequest requestData);

    Response<Void> updateBanner(UpdateBannerRequest requestData);
}
