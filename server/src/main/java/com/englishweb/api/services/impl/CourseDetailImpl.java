package com.englishweb.api.services.impl;

import com.englishweb.api.dto.CourseDetailDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.CourseDetailMapper;
import com.englishweb.api.models.Course;
import com.englishweb.api.models.CourseDetail;
import com.englishweb.api.request.UpdateCourseDetailRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.CourseDetailService;
import com.englishweb.api.services.repository.CourseDetailRepository;
import com.englishweb.api.services.repository.CourseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CourseDetailImpl implements CourseDetailService {
    private final CourseDetailRepository courseDetailRepository;
    private final CourseDetailMapper courseDetailMapper;
    private final CourseRepository courseRepository;

    @Override
    public Response<List<CourseDetailDTO>> getAllCourseDetail() {
        List<CourseDetail> courseDetails = courseDetailRepository.findAll();
        Map<String, List<CourseDetailDTO>> data = new HashMap<>();
        data.put("data", courseDetailMapper.toListDTO(courseDetails));

        return Response.<List<CourseDetailDTO>>builder()
                .message("Get all course detail successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<CourseDetailDTO> getCourseDetailById(String id) {
        CourseDetail courseDetail = courseDetailRepository.findById(id)
                .orElseThrow(() -> new NotFoundRequestException(String.format("Course detail %s could not be found", id)));

        Map<String, CourseDetailDTO> data = new HashMap<>();
        data.put("data", courseDetailMapper.toDTO(courseDetail));

        return Response.<CourseDetailDTO>builder()
                .message("Get course detail by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteCourseDetailById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        courseDetailRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete course detail by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<CourseDetailDTO> getCourseDetailByCourse(String courseId) {
        try {

            Course course = courseRepository.findById(courseId)
                    .orElseThrow(() -> new NotFoundRequestException(String.format("Course %s could not be found", courseId)));

            Optional<CourseDetail> courseDetailOptional = courseDetailRepository.findByCourse(course);
            CourseDetail courseDetail;
            if (courseDetailOptional.isEmpty()) {
                courseDetail = CourseDetail.builder()
                        .course(course)
                        .build();
                courseDetailRepository.save(courseDetail);
            } else courseDetail = courseDetailOptional.get();

            Map<String, CourseDetailDTO> data = new HashMap<>();
            data.put("data", courseDetailMapper.toDTO(courseDetail));

            return Response.<CourseDetailDTO>builder()
                    .message("Get course detail by id successful!")
                    .statusCode(HttpStatus.OK.value())
                    .data(data)
                    .build();
        } catch (Exception e) {
            log.error("{ Error when getting course detail: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public Response<Void> updateCourseDetail(UpdateCourseDetailRequest request) {
        try {
            CourseDetail courseDetail = courseDetailRepository.findById(request.getCourseDetailId())
                    .orElseThrow(() -> new NotFoundRequestException(String.format("Course detail %s could not be found", request.getCourseDetailId())));
            courseDetail.setCourseGuide(request.getCourseGuide());
            courseDetail.setCourseTarget(request.getCourseTarget());
            courseDetail.setCourseIntroduction(request.getCourseIntroduction());
            courseDetail.setCourseInformation(request.getCourseInformation());
            courseDetailRepository.save(courseDetail);

            return Response.<Void>builder()
                    .message("Update course detail successful!")
                    .statusCode(HttpStatus.OK.value())
                    .build();
        } catch (Exception e) {
            log.error("{ Error when updating course detail: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

}
