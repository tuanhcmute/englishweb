package com.englishweb.api.services.interfaces;

import com.englishweb.api.models.Word;
import com.englishweb.api.models.WordDefinition;

import java.util.List;

public interface WordService {
    List<Word> getAllWordsByKeyword(String keyword, int limit);

    List<WordDefinition> getAllWordDefinitionByWord(String word);
}
