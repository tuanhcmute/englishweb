package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.RoleDTO;
import com.englishweb.api.response.Response;

import java.util.List;

public interface RoleService {
    Response<List<RoleDTO>> getAllRoles();

    Response<RoleDTO> getRoleById(String id);

    Response<Void> deleteRole(String id);
}
