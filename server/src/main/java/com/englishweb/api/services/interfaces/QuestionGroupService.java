package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.QuestionGroupDTO;
import com.englishweb.api.models.QuestionGroup;
import com.englishweb.api.request.QuestionGroupRequest;
import com.englishweb.api.request.UpdateQuestionGroupRequest;
import com.englishweb.api.response.Response;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface QuestionGroupService {
    Response<List<QuestionGroupDTO>> getAllQuestionGroup();

    Response<QuestionGroup> getQuestionGroupById(String id);

    Response<Void> deleteQuestionGroupById(String id);

    Response<Void> createNewQuestionGroup(QuestionGroupRequest request);

    Response<List<QuestionGroupDTO>> getAllQuestionGroupByPart(String id);

    Response<Void> uploadQuestionGroupFile(String id, String type, MultipartFile file);

    Response<Void> updateQuestionGroup(UpdateQuestionGroupRequest request);
}
