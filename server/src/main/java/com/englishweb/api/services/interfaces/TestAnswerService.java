package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.TestAnswerDTO;
import com.englishweb.api.response.Response;

import java.util.List;

public interface TestAnswerService {
    Response<List<TestAnswerDTO>> getAllTestAnswer();

    Response<TestAnswerDTO> getTestAnswerById(String id);

    Response<Void> deleteTestAnswer(String id);
}
