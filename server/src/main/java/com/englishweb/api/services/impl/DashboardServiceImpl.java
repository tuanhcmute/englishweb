package com.englishweb.api.services.impl;

import com.englishweb.api.dto.*;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.DashboardService;
import com.englishweb.api.services.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class DashboardServiceImpl implements DashboardService {

    private final UserCredentialRepository userCredentialRepository;
    private final CourseRepository courseRepository;
    private final TestRepository testRepository;
    private final FlashcardRepository flashcardRepository;
    private final OrderRepository orderRepository;
    private final NewsRepository newsRepository;

    @Override
    public Response<DashboardDTO> getDashboard() {
        DashboardDTO dashboard = DashboardDTO.builder()
                .userDashboard(buildUserDashboard())
                .newsDashboard(buildNewsDashboard())
                .courseDashboard(buildCourseDashboard())
                .testDashboard(buildTestDashboard())
                .flashcardDashboard(buildFlashcardDashboard())
                .orderDashboard(buildOrderDashboard())
                .build();

        Map<String, DashboardDTO> data = new HashMap<>();
        data.put("data", dashboard);

        return Response.<DashboardDTO>builder()
                .message("Get dashboard successfully")
                .data(data)
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    private UserDashboardDTO buildUserDashboard() {
        Integer totalUser = Math.toIntExact(userCredentialRepository.count());
//        userCredentialRepository.count(Specification.where(UserCredentialSpecification.hasId()))
        return UserDashboardDTO.builder()
                .totalUser(totalUser)
                .totalAdmin(0)
                .totalStudent(0)
                .totalTeacher(0)
                .build();
    }

    private NewsDashboardDTO buildNewsDashboard() {
        Integer totalNews = Math.toIntExact(newsRepository.count());
        return NewsDashboardDTO.builder()
                .totalNews(totalNews)
                .build();
    }

    private CourseDashboardDTO buildCourseDashboard() {
        Integer totalCourses = Math.toIntExact(courseRepository.count());
        return CourseDashboardDTO.builder()
                .totalCourses(totalCourses)
                .build();
    }

    private TestDashboardDTO buildTestDashboard() {
        Integer totalTests = Math.toIntExact(testRepository.count());
        return TestDashboardDTO.builder()
                .totalTests(totalTests)
                .build();
    }

    private FlashcardDashboardDTO buildFlashcardDashboard() {
        Integer totalFlashcard = Math.toIntExact(flashcardRepository.count());
        return FlashcardDashboardDTO.builder()
                .totalFlashcards(totalFlashcard)
                .build();
    }

    private OrderDashboardDTO buildOrderDashboard() {
        Integer totalOrders = Math.toIntExact(orderRepository.count());
        return OrderDashboardDTO.builder()
                .totalOrders(totalOrders)
                .build();
    }
}
