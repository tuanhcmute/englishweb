package com.englishweb.api.services.repository;


import com.englishweb.api.models.DiscountCondition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DiscountConditionRepository extends JpaRepository<DiscountCondition, String>, JpaSpecificationExecutor<DiscountCondition> {
}
