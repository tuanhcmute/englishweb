package com.englishweb.api.services.impl;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.interfaces.OTPService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
@Slf4j
public class OTPServiceImpl implements OTPService {

    private static final String OTP_PREFIX = "otp:";
    private static final String RATE_LIMIT_PREFIX = "rate_limit:";
    private static final int RATE_LIMIT = 5;
    private static final long RATE_LIMIT_PERIOD_SECONDS = 60;

    private final StringRedisTemplate redisTemplate;

    @Override
    public String generateOTP(String email) {
        // Check rate limiting
        String rateLimitKey = RATE_LIMIT_PREFIX + email;
        if (!isRateLimited(rateLimitKey)) {
            throw new BadRequestException("Rate limit exceeded. Please try again later.");
        }

        // Generate OTP
        String otpKey = OTP_PREFIX + email;
        String existingOTP = redisTemplate.opsForValue().get(otpKey);
        if (existingOTP != null) {
            throw new BadRequestException("An OTP has already been generated for this email. Please check your email.");
        }

        String otpCode = String.format("%06d", new Random().nextInt(1000000));
        redisTemplate.opsForValue().set(otpKey, otpCode, Duration.ofMinutes(5));
        return otpCode;
    }

    @Override
    public Boolean verifyOTP(String email, String otpCode) {
        String otpKey = OTP_PREFIX + email;
        String storedOTP = redisTemplate.opsForValue().get(otpKey);

        if (storedOTP != null && storedOTP.equals(otpCode)) {
            redisTemplate.delete(otpKey);
            return true;
        }
        return false;
    }

    @Override
    public Boolean isRateLimited(String rateLimitKey) {
        Long currentCount = redisTemplate.opsForValue().increment(rateLimitKey, 1);
        if (currentCount == null) {
            return true;
        }
        if (currentCount == 1) {
            redisTemplate.expire(rateLimitKey, RATE_LIMIT_PERIOD_SECONDS, TimeUnit.SECONDS);
        }
        return currentCount <= RATE_LIMIT;
    }

    @Override
    public Boolean isRedisConnected() {
        try {
            Objects.requireNonNull(redisTemplate.getConnectionFactory()).getConnection().ping();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
