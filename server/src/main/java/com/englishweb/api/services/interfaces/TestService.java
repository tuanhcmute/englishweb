package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.FullTestDTO;
import com.englishweb.api.dto.TestDTO;
import com.englishweb.api.request.CreateTestRequest;
import com.englishweb.api.request.UpdateTestRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface TestService {
    Response<List<TestDTO>> getAllTest(Map<String, String> params);

    Response<TestDTO> getTestById(String id);

    Response<Void> deleteTest(String id);

    Response<Void> createNewTest(CreateTestRequest createTestRequest);

    Response<FullTestDTO> getFullTestById(String id);

    Response<Long> countAll();

    Response<Void> updateTest(UpdateTestRequest requestData);
}
