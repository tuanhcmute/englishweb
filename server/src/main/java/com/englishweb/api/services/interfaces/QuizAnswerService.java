package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.QuizAnswerDTO;
import com.englishweb.api.response.Response;

import java.util.List;

public interface QuizAnswerService {
    Response<List<QuizAnswerDTO>> getAllQuizAnswer();

    Response<QuizAnswerDTO> getQuizAnswerById(String id);

    Response<Void> deleteQuizAnswerById(String id);
}
