package com.englishweb.api.services.impl;

import com.englishweb.api.dto.InvoiceDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.InvoiceMapper;
import com.englishweb.api.models.Invoice;
import com.englishweb.api.models.InvoiceLine;
import com.englishweb.api.models.Order;
import com.englishweb.api.models.Payment;
import com.englishweb.api.request.CreateNewInvoiceRequest;
import com.englishweb.api.request.ExportInvoiceRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.InvoiceService;
import com.englishweb.api.services.repository.InvoiceRepository;
import com.englishweb.api.services.repository.OrderRepository;
import com.englishweb.api.services.repository.PaymentRepository;
import com.englishweb.api.utils.InvoiceUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class InvoiceImpl implements InvoiceService {
    private final InvoiceRepository invoiceRepository;
    private final InvoiceMapper invoiceMapper;
    private final OrderRepository orderRepository;
    private final PaymentRepository paymentRepository;
    private final TemplateEngine templateEngine;
    private final ITextRenderer renderer;

    @Override
    public Response<List<InvoiceDTO>> getAllInvoices() {
        List<Invoice> invoices = invoiceRepository.findAll();
        List<InvoiceDTO> invoiceDTOs = invoiceMapper.toListDTO(invoices);

        Map<String, List<InvoiceDTO>> data = new HashMap<>();
        data.put("data", invoiceDTOs);

        return Response.<List<InvoiceDTO>>builder()
                .message("Get all invoice successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<InvoiceDTO> getInvoiceById(String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null!");

        Optional<Invoice> iOptional = invoiceRepository.findById(id);
        if (iOptional.isEmpty())
            throw new BadRequestException("Id is null!");

        Map<String, InvoiceDTO> data = new HashMap<>();
        data.put("data", invoiceMapper.toDTO(iOptional.get()));
        return Response.<InvoiceDTO>builder()
                .message("Get invoice by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteInvoiceId(String id) {
        if (id.isBlank())
            throw new BadRequestException("Id is null!");

        invoiceRepository.deleteById(id);
        return Response.<Void>builder()
                .message("Get invoice by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewInvoice(CreateNewInvoiceRequest createNewInvoiceRequest) {
        InvoiceUtils.validateInvoice(createNewInvoiceRequest);
        Optional<Payment> paymentOptional = paymentRepository.findById(createNewInvoiceRequest.getPaymentId());
        if (paymentOptional.isEmpty()) throw new BadRequestException("Payment could not be found");

        Optional<Order> orderOptional = orderRepository.findById(createNewInvoiceRequest.getOrderId());
        if (orderOptional.isEmpty()) throw new BadRequestException("Order could not be found");

        Invoice invoice = Invoice.builder()
                .amountPrice(Integer.parseInt(paymentOptional.get().getTotalAmount()))
                .totalPrice(Integer.parseInt(paymentOptional.get().getTotalAmount()))
                .tax(0)
                .invoiceTitle("Hóa đơn thanh toán")
                .invoiceDescription("")
                .order(orderOptional.get())
                .payment(paymentOptional.get())
                .build();

        InvoiceLine invoiceLine = InvoiceLine.builder()
                .quantity(1)
                .invoice(invoice)
                .discount(orderOptional.get().getOrderLines().stream().toList().get(0).getCourse().getPercentDiscount())
                .unitPrice(orderOptional.get().getOrderLines().stream().toList().get(0).getCourse().getOldPrice())
                .totalPrice(orderOptional.get().getOrderLines().stream().toList().get(0).getCourse().getNewPrice())
                .course(orderOptional.get().getOrderLines().stream().toList().get(0).getCourse())
                .build();
        invoice.getInvoiceLines().add(invoiceLine);

        try {
            invoiceRepository.save(invoice);
            log.info("{ Invoice: {} }", invoice.getId());
        } catch (Exception e) {
            log.error("{ Error when creating invoice: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .message("Create new invoice by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public byte[] exportInvoice(ExportInvoiceRequest requestData) {
        Order order = orderRepository.findById(requestData.getOrderId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Order %s could not be found", requestData.getOrderId())));

        Context thymeleafContext = new Context();
        thymeleafContext.setVariable("today", new Date());
        thymeleafContext.setVariable("invoiceId", order.getInvoice().getId().substring(0, 6));
        thymeleafContext.setVariable("fullName", order.getUserCredential().getFullName());
        thymeleafContext.setVariable("address", order.getUserCredential().getAddress());
        thymeleafContext.setVariable("company", order.getUserCredential().getMajor());
        thymeleafContext.setVariable("invoiceLines", order.getInvoice().getInvoiceLines());
        thymeleafContext.setVariable("tax", order.getInvoice().getTax());
        thymeleafContext.setVariable("totalPrice", order.getInvoice().getTotalPrice());
        String htmlBody = templateEngine.process("invoice", thymeleafContext);

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            renderer.setDocumentFromString(htmlBody);
            renderer.layout();
            renderer.createPDF(outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

}
