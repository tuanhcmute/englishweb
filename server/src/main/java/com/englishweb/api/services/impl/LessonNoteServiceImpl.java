package com.englishweb.api.services.impl;

import com.englishweb.api.dto.LessonNoteDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.LessonNoteMapper;
import com.englishweb.api.models.Lesson;
import com.englishweb.api.models.LessonNote;
import com.englishweb.api.models.UserCredential;
import com.englishweb.api.request.CreateLessonNoteRequest;
import com.englishweb.api.request.UpdateLessonNoteRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.LessonNoteService;
import com.englishweb.api.services.repository.LessonNoteRepository;
import com.englishweb.api.services.repository.LessonRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import com.englishweb.api.specification.LessonNoteSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class LessonNoteServiceImpl implements LessonNoteService {
    private final LessonNoteRepository lessonNoteRepository;
    private final LessonRepository lessonRepository;
    private final UserCredentialRepository userCredentialRepository;
    private final LessonNoteMapper lessonNoteMapper;

    @Override
    @Transactional
    public Response<Void> createLessonNote(CreateLessonNoteRequest requestData) {
        Lesson lesson = lessonRepository.findById(requestData.getLessonId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Lesson %s could not be found", requestData.getLessonId())));
        UserCredential userCredential = userCredentialRepository.findById(requestData.getUserCredentialId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("User %s could not be found", requestData.getUserCredentialId())));

        LessonNote lessonNote = LessonNote.builder()
                .noteTime(requestData.getNoteTime())
                .content(requestData.getContent())
                .userCredential(userCredential)
                .lesson(lesson)
                .build();

       try {
           lessonNoteRepository.save(lessonNote);
           log.info(String.format("Lesson note %s created successfully", lessonNote.getId()));
       } catch (Exception e) {
           log.error(e.getMessage());
           throw new BadRequestException(e.getMessage());
       }
        return Response.<Void>builder()
                .statusCode(HttpStatus.CREATED.value())
                .message(String.format("Lesson note %s created successfully", lessonNote.getId()))
                .build();
    }

    @Override
    public Response<List<LessonNoteDTO>> getAllLessonNotes(Map<String, String> params) {
        List<LessonNote> lessonNotes = lessonNoteRepository.findAll(
                Specification.where(LessonNoteSpecification.hasUserCredential(params.get("userCredentialId")))
                        .and(LessonNoteSpecification.hasLesson(params.get("lessonId"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        Map<String, List<LessonNoteDTO>> data = new HashMap<>();
        data.put("data", lessonNoteMapper.toListDTO(lessonNotes));

        return Response.<List<LessonNoteDTO>>builder()
                .statusCode(HttpStatus.OK.value())
                .message("Get lesson notes successfully!")
                .data(data)
                .build();
    }

    @Override
    public Response<Void> updateLessonNote(UpdateLessonNoteRequest requestData) {
        LessonNote lessonNote = lessonNoteRepository.findById(requestData.getId())
                .orElseThrow(() -> new NotFoundRequestException(String.format("Lesson not %s could not be found", requestData.getId())));

        lessonNote.setContent(requestData.getContent());
        try {
            lessonNoteRepository.save(lessonNote);
            log.info(String.format("Lesson note %s updated successfully", lessonNote.getId()));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Lesson note %s updated successfully", lessonNote.getId()))
                .build();
    }

    @Override
    public Response<Void> deleteLessonNote(String id) {
        try {
            lessonNoteRepository.deleteById(id);
            log.info(String.format("Lesson note %s deleted successfully", id));
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return Response.<Void>builder()
                .statusCode(HttpStatus.OK.value())
                .message(String.format("Lesson note %s deleted successfully", id))
                .build();
    }
}
