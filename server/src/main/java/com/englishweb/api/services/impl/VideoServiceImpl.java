package com.englishweb.api.services.impl;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.models.ChunkWithMetadata;
import com.englishweb.api.models.FileMetadata;
import com.englishweb.api.models.Range;
import com.englishweb.api.services.interfaces.MinioStorageService;
import com.englishweb.api.services.interfaces.VideoService;
import com.englishweb.api.services.repository.FileMetadataRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class VideoServiceImpl implements VideoService {

    private final MinioStorageService storageService;

    private final FileMetadataRepository fileMetadataRepository;

    @Override
    @Transactional
    public String save(MultipartFile video) {
        try {
            FileMetadata metadata = FileMetadata.builder()
                    .size(video.getSize())
                    .httpContentType(video.getContentType())
                    .build();
            fileMetadataRepository.save(metadata);
            storageService.save(video, metadata.getId());
            return metadata.getId();
        } catch (Exception ex) {
            log.error("Exception occurred when trying to save the file", ex);
//            throw new StorageException((IOException) ex);
            throw new BadRequestException(ex.getMessage());
        }
    }

    @Override
    public ChunkWithMetadata fetchChunk(UUID uuid, Range range) {
        FileMetadata fileMetadata = fileMetadataRepository.findById(uuid.toString())
                .orElseThrow(() -> new NotFoundRequestException(String.format("File metadata %s could not be found", uuid)));
        return new ChunkWithMetadata(fileMetadata, readChunk(uuid, range, fileMetadata.getSize()));
    }

    private byte[] readChunk(UUID uuid, Range range, long fileSize) {
        long startPosition = range.getRangeStart();
        long endPosition = range.getRangeEnd(fileSize);
        int chunkSize = (int) (endPosition - startPosition + 1);
        try (InputStream inputStream = storageService.getInputStream(uuid, startPosition, chunkSize)) {
            return inputStream.readAllBytes();
        } catch (Exception exception) {
            log.error("Exception occurred when trying to read file with ID = {}", uuid);
//            throw new StorageException(exception);
            throw new BadRequestException(exception.getMessage());
        }
    }
}
