package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.NewsCategoryDTO;
import com.englishweb.api.request.CreateNewsCategory;
import com.englishweb.api.request.UpdateNewsCategory;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface NewsCategoryService {
    Response<List<NewsCategoryDTO>> getAllNewsCategories(Map<String, String> params);

    Response<Void> createNewsCategory(CreateNewsCategory requestData);

    Response<Void> updateNewsCategory(UpdateNewsCategory requestData);

    Response<Void> deleteNewsCategory(String id);
}
