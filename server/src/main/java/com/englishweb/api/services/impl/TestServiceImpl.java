package com.englishweb.api.services.impl;

import com.englishweb.api.dto.FullTestDTO;
import com.englishweb.api.dto.TestDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.TestMapper;
import com.englishweb.api.models.Test;
import com.englishweb.api.models.TestCategory;
import com.englishweb.api.request.CreateTestRequest;
import com.englishweb.api.request.UpdateTestRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.ScoreConversionService;
import com.englishweb.api.services.interfaces.FullTestAnswerService;
import com.englishweb.api.services.interfaces.PartOfTestService;
import com.englishweb.api.services.interfaces.TestService;
import com.englishweb.api.services.repository.TestCategoryRepository;
import com.englishweb.api.services.repository.TestRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestServiceImpl implements TestService {
    private final TestRepository testRepository;
    private final TestCategoryRepository testCategoryRepository;
    private final TestMapper testMapper;
    private final PartOfTestService partOfTestService;
    private final FullTestAnswerService fullTestAnswerService;
    private final ScoreConversionService scoreConversionService;

    @Override
    public Response<List<TestDTO>> getAllTest(Map<String, String> params) {
        List<Test> tests = testRepository.findAll(
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        Map<String, List<TestDTO>> data = new HashMap<>();
        data.put("data", testMapper.toListDTO(tests));

        return Response.<List<TestDTO>>builder()
                .message("Get all test successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<TestDTO> getTestById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<Test> optionalTest = testRepository.findById(id);

        if (optionalTest.isEmpty())
            throw new BadRequestException("Can not found test!");

        Map<String, TestDTO> data = new HashMap<>();
        data.put("data", testMapper.toDTO(optionalTest.get()));

        return Response.<TestDTO>builder()
                .message("Get test by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    @Transactional
    public Response<Void> deleteTest(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        testRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete test by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    @Transactional
    public Response<Void> createNewTest(CreateTestRequest createTestRequest) {
        TestCategory testCategory = testCategoryRepository.findById(createTestRequest.getTestCategoryId())
                .orElseThrow(() -> new BadRequestException("Can not find test category by test category id!"));

        Test test = Test.builder()
                .testName(createTestRequest.getTestName())
                .testDescription(createTestRequest.getTestDescription())
                .numberOfPart(createTestRequest.getNumberOfPart())
                .numberOfQuestion(createTestRequest.getNumberOfQuestion())
                .testCategory(testCategory)
                .thumbnail(createTestRequest.getThumbnail())
                .build();
            testRepository.save(test);
            log.info("{ Created test: {} }", test.getId());

//            Create multiple part of test
            partOfTestService.buckCreatePartOfTest(testCategory, test);

//            Create empty full test answer
        fullTestAnswerService.createFullTestAnswer(test);

//        Create empty score convention
        scoreConversionService.createScoreConversion(test);

        return Response.<Void>builder()
                .message("Create new test successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<FullTestDTO> getFullTestById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<Test> optionalTest = testRepository.findById(id);

        if (optionalTest.isEmpty())
            throw new BadRequestException("Can not found test!");

        Map<String, FullTestDTO> data = new HashMap<>();
        data.put("data", testMapper.toFullTestDTO(optionalTest.get()));
        return Response.<FullTestDTO>builder()
                .message("Get test by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Long> countAll() {
        Long result = testRepository.count();
        Map<String, Long> data = new HashMap<>();
        data.put("data", result);
        return Response.<Long>builder()
                .message("Get test by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> updateTest(UpdateTestRequest requestData) {
        Test test = testRepository.findById(requestData.getId())
                .orElseThrow(() -> new BadRequestException(String.format("%s could not be found", requestData.getId())));

        test.setTestName(test.getTestName());
        test.setTestDescription(test.getTestDescription());
        test.setThumbnail(requestData.getThumbnail());

//        Update
        testRepository.save(test);
        log.info("{ Created test: {} }", test.getId());

        return Response.<Void>builder()
                .message("Create new test successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
