package com.englishweb.api.services.repository;

import com.englishweb.api.models.PartOfTest;
import com.englishweb.api.models.QuestionGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionGroupRepository extends JpaRepository<QuestionGroup, String> {
    List<QuestionGroup> findAllByPartOfTest(PartOfTest partOfTest);
}
