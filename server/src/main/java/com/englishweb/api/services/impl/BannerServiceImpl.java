package com.englishweb.api.services.impl;

import com.englishweb.api.dto.BannerDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.BannerMapper;
import com.englishweb.api.models.Banner;
import com.englishweb.api.request.CreateBannerItemRequest;
import com.englishweb.api.request.UpdateBannerRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.BannerService;
import com.englishweb.api.services.repository.BannerRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class BannerServiceImpl implements BannerService {
    private final BannerRepository bannerRepository;
    private final BannerMapper bannerMapper;

    @Override
    public Response<List<BannerDTO>> getAllBanners(Map<String, String> params) {
        String pageStr = params.get("page");
        String limitStr = params.get("limit");
        if(StringUtils.isBlank(pageStr)) pageStr = "0";
        if(StringUtils.isBlank(limitStr)) limitStr = "10";

        Page<Banner> page = bannerRepository.findAll( PageRequest.of(
                Integer.parseInt(pageStr),
                Integer.parseInt(limitStr),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")));

        Map<String, List<BannerDTO>> data = new HashMap<>();
        data.put("data", bannerMapper.toListDTO(page.getContent()));
        return Response.<List<BannerDTO>>builder()
                .message("Get all banner item successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<BannerDTO> getBannerById(String id) {
        if (StringUtils.isBlank(id)) throw new BadRequestException("Id is null");

        Banner banner = bannerRepository.findById(id)
                .orElseThrow(
                        () -> new BadRequestException("Can not found banner!"));

        Map<String, BannerDTO> data = new HashMap<>();
        data.put("data", bannerMapper.toDTO(banner));

        return Response.<BannerDTO>builder()
                .message("Get banner by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteBannerById(String id) {
        if (StringUtils.isBlank(id)) throw new BadRequestException("Id is null");
        bannerRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete banner by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createBanner(CreateBannerItemRequest requestData) {
        Banner banner = requestData.buildBanner();
        bannerRepository.save(banner);
        return Response.<Void>builder()
                .message("Create banner successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateBanner(UpdateBannerRequest requestData) {
        Banner banner = bannerRepository.findById(requestData.getId())
                .orElseThrow(
                        () -> new BadRequestException("Can not found banner!"));

        banner.setImage(requestData.getImage());
        bannerRepository.save(banner);

        return Response.<Void>builder()
                .message("Update banner successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
