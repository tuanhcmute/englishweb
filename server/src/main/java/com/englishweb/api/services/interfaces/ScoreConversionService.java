package com.englishweb.api.services.interfaces;

import com.englishweb.api.models.Test;
import com.englishweb.api.request.UpdateScoreConversionRequest;
import com.englishweb.api.response.Response;

public interface ScoreConversionService {
    void createScoreConversion(Test test);

    Response<Void> updateScoreConversion(UpdateScoreConversionRequest requestData);
}
