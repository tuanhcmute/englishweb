package com.englishweb.api.services.impl;

import com.englishweb.api.dto.InvoiceLineDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.InvoiceLineMapper;
import com.englishweb.api.models.InvoiceLine;
import com.englishweb.api.request.CreateNewInvoiceLineRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.InvoiceLineService;
import com.englishweb.api.services.repository.InvoiceLineRepository;
import com.englishweb.api.utils.InvoiceLineUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InvoiceLineImpl implements InvoiceLineService {
    private final InvoiceLineRepository invoiceLineRepository;
    private final InvoiceLineMapper invoiceLineMapper;

    @Override
    public Response<List<InvoiceLineDTO>> getAllInvoiceLine() {
        List<InvoiceLine> invoiceLines = invoiceLineRepository.findAll();
        List<InvoiceLineDTO> invoiceLineDTOs = invoiceLineMapper.toListDTO(invoiceLines);

        Map<String, List<InvoiceLineDTO>> data = new HashMap<>();
        data.put("data", invoiceLineDTOs);

        return Response.<List<InvoiceLineDTO>>builder()
                .message("Get all invoice line successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<InvoiceLineDTO> getInvoiceLineById(String id) {
        if (id.strip().isEmpty())
            throw new BadRequestException("Id is null");

        Optional<InvoiceLine> invoiceOptional = invoiceLineRepository.findById(id);

        if (invoiceOptional.isEmpty())
            throw new BadRequestException("Can not find invoice line!");

        Map<String, InvoiceLineDTO> data = new HashMap<>();
        data.put("data", invoiceLineMapper.toDTO(invoiceOptional.get()));

        return Response.<InvoiceLineDTO>builder()
                .message("Get invoice line by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteInvoiceLine(String id) {
        if (id.strip().isEmpty())
            throw new BadRequestException("Id is null");

        invoiceLineRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete invoice line by id successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewInvoiceLine(CreateNewInvoiceLineRequest createNewInvoiceLineRequest) {
        InvoiceLineUtils.validateInvoiceLine(createNewInvoiceLineRequest);

        InvoiceLine invoiceLine = InvoiceLine.builder()
                .quantity(createNewInvoiceLineRequest.getQuantity())
                .unitPrice(createNewInvoiceLineRequest.getUnitPrice())
                .discount(createNewInvoiceLineRequest.getDiscount())
                .totalPrice(createNewInvoiceLineRequest.getTotalPrice())
                .build();

        invoiceLineRepository.save(invoiceLine);

        return Response.<Void>builder()
                .message("Create invoice line successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}
