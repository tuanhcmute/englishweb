package com.englishweb.api.services.interfaces;


import com.englishweb.api.models.ChunkWithMetadata;
import com.englishweb.api.models.Range;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface VideoService {
    String save(MultipartFile video);

    ChunkWithMetadata fetchChunk(UUID uuid, Range range);
}
