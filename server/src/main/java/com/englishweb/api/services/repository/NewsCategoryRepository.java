package com.englishweb.api.services.repository;

import com.englishweb.api.models.NewsCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NewsCategoryRepository extends JpaRepository<NewsCategory, String>, JpaSpecificationExecutor<NewsCategory> {
}
