package com.englishweb.api.services.repository;

import com.englishweb.api.models.Course;
import com.englishweb.api.models.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UnitRepository extends JpaRepository<Unit, String>, JpaSpecificationExecutor<Unit> {
    int countAllByCourse(Course course);
}
