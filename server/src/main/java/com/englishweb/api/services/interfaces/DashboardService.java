package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.DashboardDTO;
import com.englishweb.api.response.Response;

public interface DashboardService {
    Response<DashboardDTO> getDashboard();
}
