package com.englishweb.api.services.repository;

import com.englishweb.api.models.Lesson;
import com.englishweb.api.models.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonRepository extends JpaRepository<Lesson, String> {
    int countAllByUnit(Unit unit);
}
