package com.englishweb.api.services.interfaces;

import com.englishweb.api.models.Word;
import com.englishweb.api.models.WordDefinition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class WordServiceImpl implements WordService {

    @Override
    public List<Word> getAllWordsByKeyword(String keyword, int limit) {
        String url = String.format("https://api.datamuse.com/words?ml=%s&max=%d", keyword, limit);
        RestTemplate restTemplate = new RestTemplate();

        // Make the HTTP GET request
        try {
            Word[] words = restTemplate.getForObject(url, Word[].class);
            if (words == null) return new ArrayList<>();
            return Arrays.stream(words).toList();
        } catch (Exception e) {
            log.error("{ Error when getting words: {} }", e.getMessage());
            return new ArrayList<>();
        }
    }

    @Override
    public List<WordDefinition> getAllWordDefinitionByWord(String word) {
        String url = String.format("https://api.dictionaryapi.dev/api/v2/entries/en/%s", word);
        RestTemplate restTemplate = new RestTemplate();

        // Make the HTTP GET request
        try {

            WordDefinition[] words = restTemplate.getForObject(url, WordDefinition[].class);
            if (words == null) return new ArrayList<>();
            return Arrays.stream(words).toList();
        } catch (Exception e) {
            log.error("{ Error when getting word definition: {} }", e.getMessage());
            return new ArrayList<>();
        }
    }
}
