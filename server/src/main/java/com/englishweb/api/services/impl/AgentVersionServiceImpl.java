package com.englishweb.api.services.impl;

import com.englishweb.api.dto.AgentVersionDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.mapper.AgentVersionMapper;
import com.englishweb.api.models.AgentVersion;
import com.englishweb.api.request.UpdateAgentVersionRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.AgentVersionService;
import com.englishweb.api.services.repository.AgentVersionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class AgentVersionServiceImpl implements AgentVersionService {

    private final AgentVersionRepository agentVersionRepository;
    private final AgentVersionMapper agentVersionMapper;

    @Override
    public Response<Void> updateAgentVersion(String id, UpdateAgentVersionRequest requestData) {
        boolean isExists = agentVersionRepository.existsByIsActive(true);
        AgentVersion agentVersion = agentVersionRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(String.format("Agent version %s could not be found", id)));
//        if(isExists && requestData.getIs_active()) throw new BadRequestException("The other agent version is activated");

        agentVersion.setIsActive(requestData.getIs_active());

        agentVersionRepository.save(agentVersion);
        return Response.<Void>builder()
                .message(String.format("Agent version %s updated successfully", agentVersion.getId()))
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<AgentVersionDTO> getAgentVersion() {
        AgentVersion agentVersion = agentVersionRepository.findByIsActive(true)
                .orElseThrow(() -> new BadRequestException("Activated agent version could not be found"));

        Map<String, AgentVersionDTO> data = new HashMap<>();
        data.put("data", agentVersionMapper.toDTO(agentVersion));

        return Response.<AgentVersionDTO>builder()
                .message("Agent version found")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }
}
