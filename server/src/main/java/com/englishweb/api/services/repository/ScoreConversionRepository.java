package com.englishweb.api.services.repository;

import com.englishweb.api.models.ScoreConversion;
import com.englishweb.api.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ScoreConversionRepository extends JpaRepository<ScoreConversion, String> {
    Optional<ScoreConversion> findByTest(Test test);
}
