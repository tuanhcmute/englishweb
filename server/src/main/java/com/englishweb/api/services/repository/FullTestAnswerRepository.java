package com.englishweb.api.services.repository;

import com.englishweb.api.models.FullTestAnswer;
import com.englishweb.api.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FullTestAnswerRepository extends JpaRepository<FullTestAnswer, String> {
    Optional<FullTestAnswer> findByTest(Test test);

}
