package com.englishweb.api.services.impl;

import com.englishweb.api.dto.DiscountConditionDTO;
import com.englishweb.api.mapper.DiscountConditionMapper;
import com.englishweb.api.models.DiscountCondition;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.DiscountConditionService;
import com.englishweb.api.services.repository.DiscountConditionRepository;
import com.englishweb.api.specification.DiscountConditionSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class DiscountConditionServiceImpl implements DiscountConditionService {
    private final DiscountConditionRepository discountConditionRepository;
    private final DiscountConditionMapper discountConditionMapper;

    @Override
    public Response<List<DiscountConditionDTO>> getAllDiscountConditions(Map<String, String> params) {
        List<DiscountCondition> discountConditions = discountConditionRepository.findAll(Specification
                        .where(DiscountConditionSpecification.hasDiscount(params.get("discountId"))),
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        Map<String, List<DiscountConditionDTO>> data = new HashMap<>();
        data.put("data", discountConditionMapper.toListDTO(discountConditions));

        return Response.<List<DiscountConditionDTO>>builder()
                .message("Get all discount conditions successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }
}
