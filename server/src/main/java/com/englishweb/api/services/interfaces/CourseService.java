package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.CourseDTO;
import com.englishweb.api.request.CreateCourseRequest;
import com.englishweb.api.request.UpdateCourseRequest;
import com.englishweb.api.response.Response;

import java.util.List;
import java.util.Map;

public interface CourseService {
    Response<List<CourseDTO>> getAllCourse(Map<String, String> params);

    Response<CourseDTO> getCourseById(String id);

    Response<Void> deleteCourse(String id);

    Response<Void> createNewCourse(CreateCourseRequest course);

    Response<Void> updateCourse(UpdateCourseRequest course);

    Response<List<CourseDTO>> getAllCoursesByDiscount(String value);
}
