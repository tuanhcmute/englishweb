package com.englishweb.api.services.impl;

import com.englishweb.api.dto.TestCategoryDTO;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.mapper.TestCategoryMapper;
import com.englishweb.api.models.TestCategory;
import com.englishweb.api.request.CreateTestCategory;
import com.englishweb.api.request.UpdateTestCategory;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.interfaces.TestCategoryService;
import com.englishweb.api.services.repository.TestCategoryRepository;
import com.englishweb.api.utils.TestCategoryUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestCategoryImpl implements TestCategoryService {
    private final TestCategoryRepository testCategoryRepository;
    private final TestCategoryMapper testCategoryMapper;

    @Override
    public Response<List<TestCategoryDTO>> getAllTestCategory() {
        List<TestCategory> testCategories = testCategoryRepository.findAll(
                Sort.by(Sort.Direction.DESC, "lastModifiedDate")
        );
        Map<String, List<TestCategoryDTO>> data = new HashMap<>();
        data.put("data", testCategoryMapper.toListDTO(testCategories));

        return Response.<List<TestCategoryDTO>>builder()
                .message("Get all test category successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<TestCategoryDTO> getTestCategoryById(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        Optional<TestCategory> optionalTestCategory = testCategoryRepository.findById(id);

        if (optionalTestCategory.isEmpty())
            throw new BadRequestException("Can not found test category!");

        Map<String, TestCategoryDTO> data = new HashMap<>();
        data.put("data", testCategoryMapper.toDTO(optionalTestCategory.get()));

        return Response.<TestCategoryDTO>builder()
                .message("Get test category by id successful!")
                .statusCode(HttpStatus.OK.value())
                .data(data)
                .build();
    }

    @Override
    public Response<Void> deleteTestCategory(String id) {
        if (id.isEmpty() || id.isBlank())
            throw new BadRequestException("Id is null");

        testCategoryRepository.deleteById(id);

        return Response.<Void>builder()
                .message("Delete test category by id successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> createNewTestCategory(CreateTestCategory request) {
        TestCategoryUtils.testCategoryValidate(request);

        TestCategory testCategory = TestCategory.builder()
                .totalQuestion(request.getTotalQuestion())
                .testCategoryName(request.getTestCategoryName())
                .numberOfPart(request.getNumberOfPart())
                .totalTime(request.getTotalTime())
                .build();

        testCategoryRepository.save(testCategory);

        return Response.<Void>builder()
                .message("Create new test category successful!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    @Override
    public Response<Void> updateTestCategory(UpdateTestCategory request) {
        TestCategory testCategory = testCategoryRepository.findById((request.getId()))
                .orElseThrow(() -> new NotFoundRequestException(String.format("Test category %s could not be found", request.getId())));

        testCategory.setTestCategoryName(request.getTestCategoryName());
        testCategory.setNumberOfPart(request.getNumberOfPart());
        testCategory.setTotalQuestion(request.getTotalQuestion());
        testCategory.setTotalTime(request.getTotalTime());

        try {
            testCategoryRepository.save(testCategory);
            log.info(testCategory.getId());
        } catch (Exception e) {
            log.error("{ Error when updating test category: {} }", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }

        return Response.<Void>builder()
                .message("Test category updated successfully!")
                .statusCode(HttpStatus.OK.value())
                .build();
    }

}

