package com.englishweb.api.services.interfaces;

import com.englishweb.api.dto.QuizDTO;
import com.englishweb.api.response.Response;

import java.util.List;

public interface QuizService {
    Response<List<QuizDTO>> getAllQuiz();

    Response<QuizDTO> getQuizById(String id);

    Response<Void> deleteQuizById(String id);
}
