package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDTO {
    private String id;
    private UserCredentialDTO userCredential;
    private String content;
    private Integer rating;
    private String createdDate;
    private String lastModifiedDate;
    private Boolean isPublic;
    private String courseId;
    private String courseName;
    private String image;
}
