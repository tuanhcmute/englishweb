package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DiscountCourseDTO {
    private String id;

    private String courseName;

    private String createdDate;

    private String lastModifiedDate;
}
