package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CourseDTO {
    private String id;

    private String courseName;

    private Integer newPrice;

    private Integer oldPrice;

    private Integer percentDiscount;

    private Boolean isActive;

    private CourseCategoryDTO courseCategory;

    private String courseImg;

    private DiscountDTO discount;

    private Boolean isDiscount;

    private List<ReviewDTO> reviews;

    private Integer countReviews;

    private Integer countUnits;

    private Integer countLessons;

    private Double overallRating;

    private String createdDate;

    private String lastModifiedDate;
}
