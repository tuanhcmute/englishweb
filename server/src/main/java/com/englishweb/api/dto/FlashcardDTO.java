package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlashcardDTO {
    private String id;
    private String flashcardTitle;
    private String flashcardDescription;
    private Integer totalFlashcardItem;
    private UserCredentialDTO userCredential;
    private String flashcardType;
}
