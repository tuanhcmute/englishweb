package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentDTO {
    private String id;
    private String orderInfo;
    private String paymentTime;
    private String transactionId;
    private String totalAmount;
    private String createdDate;
}
