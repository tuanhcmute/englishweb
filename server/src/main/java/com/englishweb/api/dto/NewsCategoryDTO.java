package com.englishweb.api.dto;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewsCategoryDTO {
    private String id;

    private String newsCategoryName;

    private String createdDate;

    private String lastModifiedDate;

    private String status;
}