package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestAnswerDTO {
    private String id;
    private String answerContent;
    private String answerChoice;
    private String answerTranslate;
    private Boolean isCorrect;
}
