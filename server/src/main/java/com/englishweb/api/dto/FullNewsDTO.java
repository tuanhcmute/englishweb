package com.englishweb.api.dto;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FullNewsDTO implements Serializable {
    private String id;

    private String newsTitle;

    private String newsDescription;

    private String newsImage;

    private String newsContentHtml;

    private String status;

    private LocalDateTime createdDate;

    private LocalDateTime lastModifiedDate;

    private NewsCategoryDTO newsCategory;

    private UserCredentialDTO author;
}
