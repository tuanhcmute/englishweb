package com.englishweb.api.dto;


import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HashTagDTO implements Serializable {
    private String id;

    private String tagName;

    private LocalDateTime createdDate;

    private LocalDateTime lastModifiedDate;
}
