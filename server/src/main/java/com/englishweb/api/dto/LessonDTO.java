package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LessonDTO {
    private String id;

    private String title;

    private Integer time;

    private String video;

    private String content;

    private String createdDate;

    private String lastModifiedDate;

    private Integer sequence;
}
