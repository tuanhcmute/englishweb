package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuizDTO {
    private String quizName;
    private String quizDescription;
    private String rightAnswer;
    private LocalDateTime createdDate;
    private LocalDateTime lastModifiedDate;
}
