package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FullFlashcardDTO {
    private String id;
    private String flashcardTitle;
    private String flashcardDescription;
    private Integer totalFlashcardItem;
    private List<FlashcardItemDTO> flashcardItems;
    private UserCredentialDTO userCredential;
}
