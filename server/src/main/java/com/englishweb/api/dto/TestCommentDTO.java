package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TestCommentDTO {
    private String id;
    private String testName;
    private String testDescription;
}
