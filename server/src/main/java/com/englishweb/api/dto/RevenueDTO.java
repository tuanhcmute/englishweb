package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RevenueDTO {
    private String id;

    private Integer totalAmount;

    private String createdDate;

    private String lastModifiedDate;
}
