package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReportCommentDTO {
    private String id;
    private String reportContent;
    private UserCredentialCommentDTO userCredential;
}
