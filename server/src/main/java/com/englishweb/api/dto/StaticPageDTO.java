package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StaticPageDTO {
    private String id;

    private String pageCode;

    private String pageName;

    private String pageContent;

    private String createdDate;

    private String lastModifiedDate;

    private UserCredentialDTO createdBy;

    private String pagePreview;

    private String pageUrlVideo;
}
