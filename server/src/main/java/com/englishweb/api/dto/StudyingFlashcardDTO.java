package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudyingFlashcardDTO {
    private String id;

    private UserCredentialDTO userCredential;

    private FlashcardDTO flashcard;

    private String status;

    private String createdDate;

    private String lastModifiedDate;
}
