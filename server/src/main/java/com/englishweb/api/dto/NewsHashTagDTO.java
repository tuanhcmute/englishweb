package com.englishweb.api.dto;


import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewsHashTagDTO implements Serializable {
    private String id;

    private NewsDTO news;

    private HashTagDTO hashTag;

    private LocalDateTime createdDate;

    private LocalDateTime lastModifiedDate;
}
