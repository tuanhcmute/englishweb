package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceLineDTO {
    private String id;
    private Integer quantity;
    private Long unitPrice;
    private Long discount;
    private Long totalPrice;
    private Long voucher;
}
