package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO {
    List<TestAnswerDTO> testAnswers;
    private String id;
    private String questionContent;
    private Integer questionNumber;
    private String questionGuide;
    private String rightAnswer;
}
