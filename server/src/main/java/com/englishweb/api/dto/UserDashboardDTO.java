package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDashboardDTO {
    private Integer totalUser;
    private Integer totalStudent;
    private Integer totalTeacher;
    private Integer totalAdmin;
}
