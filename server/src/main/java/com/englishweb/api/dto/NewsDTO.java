package com.englishweb.api.dto;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewsDTO implements Serializable {
    private String id;

    private String newsTitle;

    private String newsDescription;

    private String newsImage;

    private String status;

    private String createdDate;

    private String lastModifiedDate;

    private NewsCategoryDTO newsCategory;

    private UserCredentialDTO author;
}
