package com.englishweb.api.dto;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TestDTO {
    private String id;
    private String testName;
    private String testDescription;
    private Integer numberOfPart;
    private Integer numberOfQuestion;
    private TestCategoryDTO testCategory;
    private String thumbnail;
    private String createdDate;
    private String lastModifiedDate;
    private Integer countUserAnswers;
    private Integer countTestComments;
    private ScoreConversionDTO scoreConversion;
}
