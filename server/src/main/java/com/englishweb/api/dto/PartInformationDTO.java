package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PartInformationDTO {
    private String id;
    private String partInformationName;
    private Integer totalQuestion;
    private String partInformationDescription;
    private String partType;
    private Integer partSequenceNumber;
}
