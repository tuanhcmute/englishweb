package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseDetailDTO {

    private String id;

    private String courseIntroduction;

    private String courseTarget;

    private String courseInformation;

    private String courseGuide;

    private CourseDTO course;
}
