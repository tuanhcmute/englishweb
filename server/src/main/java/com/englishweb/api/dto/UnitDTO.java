package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UnitDTO {
    private String id;

    private String title;

    private Integer totalLesson;

    private Integer totalHours;

    private Integer sequence;

    private List<LessonDTO> lessons;

    private String createdDate;

    private String lastModifiedDate;
}
