package com.englishweb.api.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DiscountConditionDTO {
    private String id;

    private Integer percentDiscount;

    private String type;

    @Builder.Default
    private List<ConditionTargetDTO> conditionTargets = new ArrayList<>();

    private String createdDate;

    private String lastModifiedDate;

}
