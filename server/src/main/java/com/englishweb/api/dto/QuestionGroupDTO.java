package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuestionGroupDTO {
    List<QuestionDTO> questions;
    private String id;
    private String audio;
    private String image;
    private String questionContentEn;
    private String questionContentTranscript;
    private String createdDate;
    private String lastModifiedDate;
}
