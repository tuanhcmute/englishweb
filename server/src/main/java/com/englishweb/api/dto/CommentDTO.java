package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommentDTO {
    private String id;
    private String content;
    private Boolean hasSubComment;
    private Boolean isNegative;
    private Boolean isReview;
    private Integer countReportComments;
    private Integer countSubComment;
    private UserCredentialCommentDTO author;
    private TestCommentDTO test;
    private String createdDate;
    private String lastModifiedDate;
}
