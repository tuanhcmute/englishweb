package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullOrderLineDTO {
    private String id;
    private CourseDTO course;
    private OrderDTO order;
}
