package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseCategoryDTO {
    private String id;

    private String courseCategoryName;

    private String createdDate;

    private String lastModifiedDate;
}
