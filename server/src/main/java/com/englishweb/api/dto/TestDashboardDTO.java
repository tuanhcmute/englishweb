package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestDashboardDTO {
    private Integer totalTests;
}
