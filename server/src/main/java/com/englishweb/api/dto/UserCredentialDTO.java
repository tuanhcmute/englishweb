package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserCredentialDTO {
    private String id;

    private String username;

    private String description;

    private String roleName;

    private String phoneNumber;

    private String email;

    private String avatar;

    private String fullName;

    private Boolean verified;

    private String provider;

    private List<UserAuthorityDTO> userAuthorities;

    private String dob;

    private String address;

    private String gender;

    private String major;
}
