package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullTestDTO {
    List<FullPartOfTestDTO> listOfPart;
    private String id;
    private String testName;
    private String testDescription;
    private Integer numberOfPart;
    private Integer numberOfQuestion;
    private String thumbnail;
}
