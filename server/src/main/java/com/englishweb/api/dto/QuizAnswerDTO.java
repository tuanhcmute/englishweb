package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuizAnswerDTO {
    private String quizAnswerContent;
    private String quizAnswerChoice;
    private String quizAnswerTranslate;
    private Boolean isCorrect;
}
