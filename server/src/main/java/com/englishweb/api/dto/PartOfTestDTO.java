package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PartOfTestDTO {
    private String id;
    private FullPartInformationDTO partInformation;
    private Integer partSequenceNumber;
}
