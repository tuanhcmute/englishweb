package com.englishweb.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TestCategoryDTO {
    private String id;
    private String testCategoryName;
    private Integer numberOfPart;
    private Integer totalQuestion;
    private Integer totalTime;
    private String createdDate;
    private String lastModifiedDate;
}
