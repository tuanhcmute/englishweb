package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonNoteDTO {

    private String id;

    private String content;

    private LessonDTO lesson;

    private String createdDate;

    private String lastModifiedDate;

    private Double noteTime;
}
