package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDTO {
    private String id;
    private String orderDate;
    private Boolean paymentStatus;
    private UserCredentialDTO userCredential;
    private InvoiceDTO invoice;
    List<OrderLineDTO> orderLines;
}
