package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserAnswerDTO {
    private String id;

    private String answerJson;

    private String testDate;

    private Integer listeningCorrectAnsCount;

    private Integer readingCorrectAnsCount;

    private Integer listeningWrongAnsCount;

    private Integer readingWrongAnsCount;

    private Integer ignoreAnsCount;

    private Integer completionTime;

    private Integer totalScore;

    private Integer totalSentences;

    private String practiceType;

    private TestDTO test;
}
