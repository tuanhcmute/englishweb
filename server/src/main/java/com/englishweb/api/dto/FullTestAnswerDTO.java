package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullTestAnswerDTO {
    private String id;
    private String fullTestAnswerJson;
    private TestDTO test;
}
