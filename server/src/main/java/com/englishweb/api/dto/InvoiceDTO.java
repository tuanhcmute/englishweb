package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceDTO {
    private String id;
    private Long amountPrice;
    private Long totalPrice;
    private Long tax;
    private String invoiceTitle;
    private String invoiceDescription;
    private String createdDate;
    private List<InvoiceLineDTO> invoiceLines;
    private PaymentDTO payment;
}
