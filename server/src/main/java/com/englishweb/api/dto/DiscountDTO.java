package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DiscountDTO {
    private String id;

    private String discountName;

    private String startDate;

    private String endDate;

    private String createdDate;

    private String lastModifiedDate;

    private String status;

}
