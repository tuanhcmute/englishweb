package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullPartOfTestDTO {
    private String id;
    private PartInformationDTO partInformation;
    private List<QuestionGroupDTO> questionGroups;
    private Integer partSequenceNumber;
}
