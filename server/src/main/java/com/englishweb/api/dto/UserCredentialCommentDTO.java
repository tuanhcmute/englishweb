package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserCredentialCommentDTO {
    private String id;

    private String username;

    private String description;

    private String avatar;

    private String fullName;
}
