package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAuthorityDTO {
    private RoleDTO role;
}
