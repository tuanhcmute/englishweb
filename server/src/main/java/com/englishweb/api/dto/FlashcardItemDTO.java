package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FlashcardItemDTO {
    private String id;
    private String word;
    private String phonetic;
    private String meaning;
    private String tags;
    private String examples;
    private String audio;
    private String image;
    private String createdDate;
    private String lastModifiedDate;
}
