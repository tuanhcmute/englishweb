package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConditionTargetDTO {

    private String id;

    private DiscountCourseDTO course;

    private CourseCategoryDTO courseCategory;

}
