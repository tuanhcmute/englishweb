package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BannerDTO {
    private String id;
    private String image;
    private String createdDate;
    private String lastModifiedDate;
}
