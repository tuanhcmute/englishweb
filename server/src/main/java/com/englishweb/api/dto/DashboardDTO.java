package com.englishweb.api.dto;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DashboardDTO {
    private UserDashboardDTO userDashboard;
    private NewsDashboardDTO newsDashboard;
    private OrderDashboardDTO orderDashboard;
    private CourseDashboardDTO courseDashboard;
    private TestDashboardDTO testDashboard;
    private FlashcardDashboardDTO flashcardDashboard;
}
