package com.englishweb.api.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullOrderDTO {
    private String id;
    private String orderDate;
    private Boolean paymentStatus;
    private UserCredentialDTO userCredential;
    private InvoiceDTO invoice;
    private List<OrderLineDTO> orderLines;
}
