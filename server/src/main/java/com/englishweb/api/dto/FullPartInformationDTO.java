package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullPartInformationDTO {
    private String id;
    private String partInformationName;
    private Integer totalQuestion;
    private String partInformationDescription;
    private TestCategoryDTO testCategory;
    private Integer partSequenceNumber;
    private String partType;
}
