package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDashboardDTO {
    private Integer totalOrders;
}
