package com.englishweb.api.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CourseDashboardDTO {
    private Integer totalCourses;
}
