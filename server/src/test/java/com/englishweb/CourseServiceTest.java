package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.CreateCourseRequest;
import com.englishweb.api.request.UpdateCourseRequest;
import com.englishweb.api.services.impl.CourseImpl;
import com.englishweb.api.services.repository.CourseCategoryRepository;
import com.englishweb.api.services.repository.CourseRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CourseServiceTest {
    @Mock
    CourseRepository courseRepository;

    @Mock
    CourseCategoryRepository courseCategoryRepository;

    @InjectMocks
    CourseImpl courseImpl;
  
    // ====================================== FAILED ======================================
    @Test
    void testGetCourseByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> courseImpl.getCourseById(""));

        // ----------------------------------
        String expectedMessage = String.format("Course %s could not be found", "");
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteCourseByIdWithIdIsEmpty() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> courseImpl.deleteCourse(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllCourseWithEmptyData() {
        assertThrows(NullPointerException.class, () -> courseImpl.getAllCourse(null));
    }

    @Test
    void testCreateNewCourseWithCourseCategoryIsNotExist() {
        CreateCourseRequest createCourseRequest = new CreateCourseRequest("Test create new course", "Test category id value is not exist", 1000, "http://localhost:8000", true);
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> courseImpl.createNewCourse(createCourseRequest));

        // ----------------------------------
        String expectedMessage = String.format("Course category %s could not be found", createCourseRequest.getCourseCategoryId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }



    // ====================================== SUCCESS ======================================
}
