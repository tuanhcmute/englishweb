package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewsRequest;
import com.englishweb.api.services.impl.NewsImpl;
import com.englishweb.api.services.repository.NewsCategoryRepository;
import com.englishweb.api.services.repository.NewsRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class NewsServiceTest {
    @Mock
    NewsRepository newsRepository;

    @Mock
    NewsCategoryRepository newsCategoryRepository;

    @Mock
    UserCredentialRepository userCredentialRepository;


    @InjectMocks
    NewsImpl newsImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetNewsByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> newsImpl.getNewsById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }



    @Test
    void testDeleteNewsByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> newsImpl.deleteNews(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllNewsWithEmptyData() {
        assertThrows(BadRequestException.class, () -> newsImpl.getAllNews(new HashMap<>()));
    }

    @Test
    void testCreateNewNewsWithNotExistRef() {
        CreateNewsRequest createNewsRequest = new CreateNewsRequest("Title", "Description", "New images", "Category id value is not exist", "Status", "User credential id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> newsImpl.createNews(createNewsRequest));

        // ----------------------------------
        String expectedMessage = "News category could not be found";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
