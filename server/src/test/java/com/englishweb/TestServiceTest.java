package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateTestRequest;
import com.englishweb.api.services.impl.TestServiceImpl;
import com.englishweb.api.services.repository.TestCategoryRepository;
import com.englishweb.api.services.repository.TestRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class TestServiceTest {
    @Mock
    TestRepository testRepository;

    @Mock
    TestCategoryRepository testCategoryRepository;

    @InjectMocks
    TestServiceImpl testServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetTestByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> testServiceImpl.getTestById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteTestByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> testServiceImpl.deleteTest(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllTestWithEmptyData() {
        assertThrows(NullPointerException.class, () -> testServiceImpl.getAllTest(new HashMap<>()));
    }

    @Test
    void testCreateNewTestWithNotExistRefId() {
        CreateTestRequest testRequest = new CreateTestRequest("Test category id value is not exist", "testName", "testDescription", 4, 100, null);
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> testServiceImpl.createNewTest(testRequest));

        // ----------------------------------
        String expectedMessage = "Can not find test category by test category id!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ====================================== 
}
