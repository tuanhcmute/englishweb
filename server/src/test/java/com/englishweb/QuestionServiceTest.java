package com.englishweb;

import java.util.List;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.Question;
import com.englishweb.api.request.QuestionRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.impl.QuestionImpl;
import com.englishweb.api.services.repository.QuestionGroupRepository;
import com.englishweb.api.services.repository.QuestionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceTest {
    @Mock
    QuestionRepository questionRepository;

    @Mock 
    QuestionGroupRepository questionGroupRepository;

    @InjectMocks
    QuestionImpl questionImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetQuestionByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> questionImpl.getQuestionById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteQuestionByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> questionImpl.deleteQuestion(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

   @Test
    void testGetAllQuestionWithEmptyData() {
        Response<List<Question>> response =  questionImpl.getAllQuestion();
        assertEquals(200, response.getStatusCode());
        assertEquals("Get all questions successful!", response.getMessage());
    }

    @Test
    void testCreateNewQuestionWithNotExistRefId() {
        QuestionRequest questionRequest = new QuestionRequest("questionContent", 10, "questionGuide", "Question group id value is not exist", null);
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> questionImpl.createNewQuestion(questionRequest));

        // ----------------------------------
        String expectedMessage = "Can not find question group by question group id!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
