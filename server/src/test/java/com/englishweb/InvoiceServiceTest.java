package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewInvoiceRequest;
import com.englishweb.api.services.impl.InvoiceImpl;
import com.englishweb.api.services.repository.InvoiceRepository;
import com.englishweb.api.services.repository.OrderRepository;
import com.englishweb.api.services.repository.PaymentRepository;

@ExtendWith(MockitoExtension.class)
public class InvoiceServiceTest {
    @Mock
    InvoiceRepository invoiceRepository;

    @Mock
    PaymentRepository paymentRepository;

    @Mock
    OrderRepository orderRepository;

    @InjectMocks
    InvoiceImpl invoiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetInvoceByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> invoiceImpl.getInvoiceById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteInvoceByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> invoiceImpl.deleteInvoiceId(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllInvoicesWithEmptyData() {
        assertThrows(NullPointerException.class, () -> invoiceImpl.getAllInvoices());
    }

    @Test
    void testCreateNewInvoideWithNotExistRefId() {
        CreateNewInvoiceRequest createNewInvoiceRequest = new CreateNewInvoiceRequest("Payment id value is not exist", "Order is value is not exist");
        
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> invoiceImpl.createNewInvoice(createNewInvoiceRequest));

        // ----------------------------------
        String expectedMessage = "Payment could not be found";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
