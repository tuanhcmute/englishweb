package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateNewInvoiceLineRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.impl.InvoiceLineImpl;
import com.englishweb.api.services.repository.InvoiceLineRepository;

@ExtendWith(MockitoExtension.class)
public class InvoiceLineServiceImplTest {
    @Mock
    InvoiceLineRepository invoiceLineRepository;

    @InjectMocks
    InvoiceLineImpl invoiceLineImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetInvoceLineByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> invoiceLineImpl.getInvoiceLineById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteInvoceLineByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> invoiceLineImpl.deleteInvoiceLine(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllInvoiceLineWithEmptyData() {
        assertThrows(NullPointerException.class, () -> invoiceLineImpl.getAllInvoiceLine());
    }

    // ====================================== SUCCESS ======================================

    @Test
    void testCreateNewInvoiceLine() {
        CreateNewInvoiceLineRequest createNewInvoiceLineRequest = new CreateNewInvoiceLineRequest(2, 3, 50, 100000);
        Response<Void> response = invoiceLineImpl.createNewInvoiceLine(createNewInvoiceLineRequest);

        assertEquals(200, response.getStatusCode());
        assertEquals("Create invoice line successfully!", response.getMessage());
    }
}
