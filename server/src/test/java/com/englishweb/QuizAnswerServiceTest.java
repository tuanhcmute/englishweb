package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.impl.QuizAnswerServiceImpl;
import com.englishweb.api.services.repository.QuizAnswerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class QuizAnswerServiceTest {
    @Mock
    QuizAnswerRepository quizAnswerRepository;

    @InjectMocks
    QuizAnswerServiceImpl quizAnswerServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetQuizAnswerByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> quizAnswerServiceImpl.getQuizAnswerById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteQuizAnswerByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> quizAnswerServiceImpl.deleteQuizAnswerById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllTestWithEmptyData() {
        assertThrows(NullPointerException.class, () -> quizAnswerServiceImpl.getAllQuizAnswer());
    }    
    
    // ====================================== SUCCESS ======================================
}
