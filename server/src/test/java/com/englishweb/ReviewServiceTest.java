package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.CreateReviewRequest;
import com.englishweb.api.services.impl.ReviewServiceImpl;
import com.englishweb.api.services.repository.ReviewRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTest {
    @Mock
    ReviewRepository reviewRepository;

    @Mock
    UserCredentialRepository userCredentialRepository;

    @InjectMocks
    ReviewServiceImpl reviewServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetAllReviewsByCourseByNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> reviewServiceImpl.deleteReviewById(""));

        // ----------------------------------
        String expectedMessage = "Id is empty";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllReviewWithEmptyData() {
        Map<String, String> params = new HashMap<>();
        assertThrows(NullPointerException.class, () -> reviewServiceImpl.getAllReviews(params));
    }

    @Test
    void testCreateNewReviewWithNotExistRefId() {
        CreateReviewRequest createReviewRequest = new CreateReviewRequest("User credential id value is not exist", "content", "image", 5, "Course id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> reviewServiceImpl.createReview(createReviewRequest));

        // ----------------------------------
        String expectedMessage = String.format("User %s could not be found", createReviewRequest.getUserCredentialId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
