package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.impl.UserCredentialImpl;
import com.englishweb.api.services.repository.UserCredentialRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class UserCredentialServiceTest {
    @Mock
    UserCredentialRepository userCredentialRepository;

    @InjectMocks
    UserCredentialImpl userCredentialImpl;

    // ====================================== FAILED ======================================

    @Test
    void testDeleteUserCredentialByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> userCredentialImpl.deleteUserCredential(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllUserCredentialWithEmptyData() {
        assertThrows(NullPointerException.class, () -> userCredentialImpl.getAllUserCredential(new HashMap<>()));
    }
    // ====================================== SUCCESS ====================================== 
}
