package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.CreateCourseCategoryRequest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.impl.CourseCategoryImpl;
import com.englishweb.api.services.repository.CourseCategoryRepository;

@ExtendWith(MockitoExtension.class)
public class CourseCategoryTest {
    @Mock
    CourseCategoryRepository courseCategoryRepository;

    @InjectMocks
    CourseCategoryImpl courseCategoryImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetCourseCatgoryWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> courseCategoryImpl.getCourseCategoryById(""));

        // ----------------------------------
        String expectedMessage = String.format("Course category %s could not be found", "");
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteCourseCatgoryWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> courseCategoryImpl.deleteCourseCategory(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllCourseCategoryWithEmptyData() {
        assertThrows(NullPointerException.class, () -> courseCategoryImpl.getAllCourseCategory());
    }

    // ====================================== SUCCESS ======================================
    @Test
    void testCreateCourseCategorySuccess() {
        CreateCourseCategoryRequest request = new CreateCourseCategoryRequest("Test create ne course category");

        Response<Void> response = courseCategoryImpl.createCourseCategory(request);

        String expectedMessage = "Create course category successful!";
        assertTrue(response.getMessage().contains(expectedMessage));
        assertEquals(200, response.getStatusCode());
    }
}
