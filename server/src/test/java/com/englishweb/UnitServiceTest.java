package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.CreateUnitRequest;
import com.englishweb.api.services.impl.UnitServiceImpl;
import com.englishweb.api.services.repository.CourseRepository;
import com.englishweb.api.services.repository.UnitRepository;

@ExtendWith(MockitoExtension.class)
public class UnitServiceTest {
    @Mock
    UnitRepository unitRepository;

    @Mock
    CourseRepository courseRepository;

    @InjectMocks
    UnitServiceImpl unitServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetUnitByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> unitServiceImpl.getUnitById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteUnitByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> unitServiceImpl.getUnitById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllUnitWithEmptyData() {
        Map<String, String> params = new HashMap<>();
        assertThrows(NullPointerException.class, () -> unitServiceImpl.getAllUnits(params));
    }

    @Test
    void testcreateNewUnitWithNotExistRefId() {
        CreateUnitRequest createUnitRequest = new CreateUnitRequest("Title", "Course id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> unitServiceImpl.createNewUnit(createUnitRequest));

        // ----------------------------------
        String expectedMessage = String.format("Course %s could not be found", createUnitRequest.getCourseId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ====================================== 
}
