package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.CreateFlashcardRequest;
import com.englishweb.api.request.UpdateFlashcardRequest;
import com.englishweb.api.services.impl.FlashcardServiceImpl;
import com.englishweb.api.services.repository.FlashcardRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;

@ExtendWith(MockitoExtension.class)
public class FlashcardServiceTest {
    @Mock
    FlashcardRepository flashcardRepository;

    @Mock
    UserCredentialRepository userCredentialRepository;

    @InjectMocks
    FlashcardServiceImpl flashcardServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetFlashCardByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> flashcardServiceImpl.getFlashcardById(""));

        // ----------------------------------
        String expectedMessage = String.format("Flashcard %s could not be found", "");
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void testDeleteFlashCardByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> flashcardServiceImpl.deleteFlashcard(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllFlashCardWithEmptyData() {
        Map<String, String> params = new HashMap<>();
        assertThrows(NullPointerException.class, () -> flashcardServiceImpl.getAllFlashcard(params));
    }

    @Test
    void testCreateNewFlashCardWithUserIdIsNotExist() {
        CreateFlashcardRequest createFlashcardRequest = new CreateFlashcardRequest("flashcardTitle", "flashcardDescription", 1, "userCredentialId", "flashcardType");
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> flashcardServiceImpl.createNewFlashcard(createFlashcardRequest));

        // ----------------------------------
        String expectedMessage = String.format("User %s could not be found", createFlashcardRequest.getUserCredentialId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testUpdateFlashCardWithNotExistRefId( ) {
        UpdateFlashcardRequest updateFlashcardRequest = new UpdateFlashcardRequest("Id value is not exist", "flashcardTitle", "flashcardDescription");
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> flashcardServiceImpl.updateFlashcard(updateFlashcardRequest));

        // ----------------------------------
        String expectedMessage = String.format("Flashcard %s could not be found", updateFlashcardRequest.getId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
