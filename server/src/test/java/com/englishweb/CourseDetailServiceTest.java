package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.UpdateCourseDetailRequest;
import com.englishweb.api.services.impl.CourseDetailImpl;
import com.englishweb.api.services.repository.CourseDetailRepository;

@ExtendWith(MockitoExtension.class)
public class CourseDetailServiceTest {
    @Mock
    CourseDetailRepository courseDetailRepository;

    @InjectMocks
    CourseDetailImpl courseDetailImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetCourseDetailByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> courseDetailImpl.getCourseDetailById(""));

        // ----------------------------------
        String expectedMessage = String.format("Course detail %s could not be found", "");
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteCourseDetailByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> courseDetailImpl.deleteCourseDetailById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void tetsGetAllCourseDetailWithEmptyData() {
        assertThrows(NullPointerException.class, () -> courseDetailImpl.getAllCourseDetail());
    }

    @Test
    void testUpdateCourseDetailWithNotExistRef() {
        UpdateCourseDetailRequest updateCourseDetailRequest = new UpdateCourseDetailRequest("courseIntroduction", "courseTarget", "courseInformation", "courseGuide", "Course id value is not exist", "Course detail id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> courseDetailImpl.updateCourseDetail(updateCourseDetailRequest));

        // ----------------------------------
        String expectedMessage = String.format("Course detail %s could not be found", updateCourseDetailRequest.getCourseDetailId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
