package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.impl.QuizCategoryServiceImpl;
import com.englishweb.api.services.repository.QuizCategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class QuizCategoryServiceTest {
    @Mock
    QuizCategoryRepository quizCategoryRepository;

    @InjectMocks
    QuizCategoryServiceImpl quizCategoryServiceImpl;
    
    // ====================================== FAILED ======================================
    @Test
    void testGetQuizCategoryByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> quizCategoryServiceImpl.getQuizCategoryById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteQuizCategoryByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> quizCategoryServiceImpl.deletQuizCategoryById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllQuizCategoryWithEmptyData() {
        assertThrows(NullPointerException.class, () -> quizCategoryServiceImpl.getAllQuizCategory());
    }

    // ====================================== SUCCESS ======================================
}
