package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.impl.QuizServiceImpl;
import com.englishweb.api.services.repository.QuizRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class QuizServiceTest {
    @Mock
    QuizRepository quizRepository;

    @InjectMocks
    QuizServiceImpl quizServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetQuizByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> quizServiceImpl.getQuizById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteQuizByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> quizServiceImpl.deleteQuizById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllQuiz() {
        assertThrows(NullPointerException.class, () -> quizServiceImpl.getAllQuiz());
    }
    
    // ====================================== SUCCESS ======================================
}
