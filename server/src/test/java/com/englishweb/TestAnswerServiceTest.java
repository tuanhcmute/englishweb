package com.englishweb;

import com.englishweb.api.response.Response;
import com.englishweb.api.services.impl.TestAnswerServiceImpl;
import com.englishweb.api.services.repository.TestAnswerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TestAnswerServiceTest {
    @Mock
    TestAnswerRepository testAnswerRepository;

    @InjectMocks
    TestAnswerServiceImpl testAnswerServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetTestAnswerByIdWithIdIsNotExist() {
        // ----------------------------------
//        Exception exception = assertThrows(BadRequestException.class, () -> testAnswerServiceImpl.getTestAnswerById(""));
//
//        // ----------------------------------
//        String expectedMessage = "Id is null";
//        String actualMessage = exception.getMessage();
//
//        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteTestAnswerByIdWithEmptyId() {
        // ----------------------------------
//        Exception exception = assertThrows(BadRequestException.class, () -> testAnswerServiceImpl.deleteTestAnswer(""));

        Response<Void> response = testAnswerServiceImpl.deleteTestAnswer("");

        // ----------------------------------
        Integer expectedStatus = HttpStatus.OK.value();
        Integer actualStatus = response.getStatusCode();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void testGetAllTestAnswerWithEmptyData() {
//        assertThrows(BadRequestException.class, () -> testAnswerServiceImpl.getAllTestAnswer());
    }
    // ====================================== SUCCESS ======================================
}
