package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.QuestionGroupRequest;
import com.englishweb.api.services.impl.QuestionGroupImpl;
import com.englishweb.api.services.repository.PartOfTestRepository;
import com.englishweb.api.services.repository.QuestionGroupRepository;

@ExtendWith(MockitoExtension.class)
public class QuestionGroupServiceTest {
    @Mock
    QuestionGroupRepository questionGroupRepository;

    @Mock
    PartOfTestRepository partOfTestRepository;

    @InjectMocks 
    QuestionGroupImpl questionGroupImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetQuestionGroupByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> questionGroupImpl.getQuestionGroupById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteQuestionGroupByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> questionGroupImpl.getQuestionGroupById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllQuestionGroupWithEmptyData() {
        assertThrows(NullPointerException.class, () -> questionGroupImpl.getAllQuestionGroup());
    }

    @Test
    void testCreateNewQuestionGroup() {
        QuestionGroupRequest questionGroupRequest = new QuestionGroupRequest("image", "questionContentEn", "questionContentTranscript", "Part of test id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> questionGroupImpl.createNewQuestionGroup(questionGroupRequest));

        // ----------------------------------
        String expectedMessage = "Part of test could not be found";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
