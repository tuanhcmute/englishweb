package com.englishweb;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.services.impl.DiscountServiceImpl;
import com.englishweb.api.services.repository.DiscountRepository;

@ExtendWith(MockitoExtension.class)
public class DiscountServiceTest {
    @Mock
    DiscountRepository discountRepository;

    @InjectMocks
    DiscountServiceImpl discountServiceImpl;

}
