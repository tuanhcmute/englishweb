package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.impl.FlashcardItemImpl;
import com.englishweb.api.services.repository.FlashcardItemRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.request.CreateFlashcardItem;
import com.englishweb.api.request.UpdateFlashcardItem;
import com.englishweb.api.services.repository.FlashcardRepository;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class FlashcardItemServiceTest {
    @Mock
    FlashcardItemRepository flashcardItemRepository;

    @Mock
    FlashcardRepository flashcardRepository;

    @InjectMocks
    FlashcardItemImpl flashcardItemImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetFlashCardItemByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> flashcardItemImpl.getAllFlashcardItemById(""));

        // ----------------------------------
        String expectedMessage = "Id is null!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void testDeleteFlashCardItemByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> flashcardItemImpl.deleteFlashcardItem(""));

        // ----------------------------------
        String expectedMessage = "Id is null!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllFlashCardItemWithEmptyData() {
        assertThrows(NullPointerException.class, () -> flashcardItemImpl.getAllFlashcardItem(null));
    }

    @Test
    void testCreateNewFlashCardItemWithFlashCardIdIsNotExist() {
        CreateFlashcardItem createFlashcardItem = new CreateFlashcardItem("Word", "phonetic", "meaning", "tags", "examples", "Flash card id value is not exist!");
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> flashcardItemImpl.createFlashcardItem(createFlashcardItem));

        // ----------------------------------
        String expectedMessage = "Flashcard could not be found";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testUpdateCourseDetailWithNotExistRefId() {
        UpdateFlashcardItem updateFlashcardItem = new UpdateFlashcardItem("word", "phonetic", "meaning", "tags", "examples", "Flash card id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> flashcardItemImpl.updateFlashcardItem(updateFlashcardItem));

        // ----------------------------------
        String expectedMessage = "Flashcard item could not be found";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
