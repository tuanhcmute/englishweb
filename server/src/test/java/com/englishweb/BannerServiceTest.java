package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.impl.BannerServiceImpl;
import com.englishweb.api.services.repository.BannerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class BannerServiceTest {
    @Mock
    BannerRepository bannerRepository;

    @InjectMocks
    BannerServiceImpl bannerServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetBannerItemWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> bannerServiceImpl.getBannerById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void testDeleteBannerItemWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> bannerServiceImpl.deleteBannerById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllBannerItemWithEmptyData() {
        assertThrows(NullPointerException.class, () -> bannerServiceImpl.getAllBanners(new HashMap<>()));
    }

    // ====================================== SUCCESS ======================================
}
