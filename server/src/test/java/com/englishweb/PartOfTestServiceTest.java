package com.englishweb;

import java.util.*;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.PartOfTest;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.impl.PartOfTestImpl;
import com.englishweb.api.services.repository.PartOfTestRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class PartOfTestServiceTest {
    @Mock
    PartOfTestRepository partOfTestRepository;

    @InjectMocks
    PartOfTestImpl partOfTestImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetPartOfTestWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> partOfTestImpl.getPartOfTestById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeletePartOfTestWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> partOfTestImpl.deletePartOfTest(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

   @Test
   void testGetAllPartOfTestWithEmptyData() {
        Response<List<PartOfTest>> response = partOfTestImpl.getAllPartOfTest();
        assertEquals("Get all part of test successful!", response.getMessage());
        assertEquals(200, response.getStatusCode());
   }

    // ====================================== SUCCESS =====================================
}
