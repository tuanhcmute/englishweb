package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.CreateLessonRequest;
import com.englishweb.api.request.UpdateLessonRequest;
import com.englishweb.api.services.impl.LessonServiceImpl;
import com.englishweb.api.services.repository.LessonRepository;
import com.englishweb.api.services.repository.UnitRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class LessonServiceTest {
    @Mock
    LessonRepository lessonRepository;

    @Mock
    UnitRepository unitRepository;

    @InjectMocks
    LessonServiceImpl lessonServiceImpl;

    // ====================================== FAILED ======================================
   @Test
   void testGetLessonByIdWithIdIsNotExist() {
       // ----------------------------------
       Exception exception = assertThrows(NotFoundRequestException.class, () -> lessonServiceImpl.getLessonById(""));

       // ----------------------------------
       String expectedMessage = String.format("Lesson %s could not be found", "");
       String actualMessage = exception.getMessage();

       assertTrue(actualMessage.contains(expectedMessage));
   }

    @Test
    void testDeleteLessonByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> lessonServiceImpl.deleteLessonById(""));

        // ----------------------------------
        String expectedMessage = "Id value is empty";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllLessonsWithEmptyData() {
        assertThrows(NullPointerException.class, () -> lessonServiceImpl.getAllLessons());
    }

    @Test
    void testCreateNewLessonWithNotExistRefId() {
        CreateLessonRequest createLessonRequest = new CreateLessonRequest("123", "123");
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> lessonServiceImpl.createNewLesson(createLessonRequest));

        // ----------------------------------
        String expectedMessage = String.format("Unit %s could not be found", createLessonRequest.getUnitId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testUpdateLessonWithNotExistRefId() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest("Id value is not exist", "title", "content", "video");
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> lessonServiceImpl.updateLesson(updateLessonRequest));

        // ----------------------------------
        String expectedMessage = String.format("Lesson %s could not be found", updateLessonRequest.getId());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
