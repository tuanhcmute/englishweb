package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.services.impl.PaymentImpl;
import com.englishweb.api.services.repository.PaymentRepository;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceTest {
    @Mock
    PaymentRepository paymentRepository;

    @InjectMocks
    PaymentImpl paymentImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetPaymentByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> paymentImpl.getPaymentById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeletePaymentByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> paymentImpl.deletePaymentById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllPaymentWithEmptyData() {
        assertThrows(NullPointerException.class, () -> paymentImpl.getAllPayment());
    }
    
    // ====================================== SUCCESS ======================================
}
