package com.englishweb;

import java.util.List;
import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.models.FullTestAnswer;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.impl.FullTestAnswerImpl;
import com.englishweb.api.services.repository.FullTestAnswerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class FullTestAnswerServiceTest {
    @Mock
    FullTestAnswerRepository fullTestAnswerRepository;

    @InjectMocks
    FullTestAnswerImpl fullTestAnswerImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetFullTestAnswerByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> fullTestAnswerImpl.getFullTestAnswerById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteFullTestAnswerByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> fullTestAnswerImpl.deleteFullTestAnswer(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
    @Test
    void testGetAllTestAnswerWithEmptyData() {
        Response<List<FullTestAnswer>> response = fullTestAnswerImpl.getAllTestAnswer();

        assertEquals(200, response.getStatusCode());
        assertEquals("Get full test answer successful!", response.getMessage());
    }
}
