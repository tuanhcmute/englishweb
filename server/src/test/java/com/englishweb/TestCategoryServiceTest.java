package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateTestCategory;
import com.englishweb.api.response.Response;
import com.englishweb.api.services.impl.TestCategoryImpl;
import com.englishweb.api.services.repository.TestCategoryRepository;

@ExtendWith(MockitoExtension.class)
public class TestCategoryServiceTest {
    @Mock
    TestCategoryRepository testCategoryRepository;

    @InjectMocks
    TestCategoryImpl testCategoryImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetTestCategoryByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> testCategoryImpl.getTestCategoryById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteTestCategoryByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> testCategoryImpl.deleteTestCategory(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllTestCategoryWithEmptyData() {
        assertThrows(NullPointerException.class, () -> testCategoryImpl.getAllTestCategory());
    }

    // ====================================== SUCCESS ======================================    

    @Test
    void testCreateNewTestCategory() {
        CreateTestCategory createTestCategory = new CreateTestCategory(7, 200, 120, "Test category name");

        Response<Void> response = testCategoryImpl.createNewTestCategory(createTestCategory);

        assertEquals(200, response.getStatusCode());
        assertEquals("Create new test category successful!", response.getMessage());
    }
}
