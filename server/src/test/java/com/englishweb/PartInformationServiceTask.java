package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreatePartInformation;
import com.englishweb.api.services.impl.PartInformationImpl;
import com.englishweb.api.services.repository.PartInformationRepository;
import com.englishweb.api.services.repository.TestCategoryRepository;

@ExtendWith(MockitoExtension.class)
public class PartInformationServiceTask {
    @Mock
    PartInformationRepository partInformationRepository;

    @Mock
    TestCategoryRepository testCategoryRepository;

    @InjectMocks
    PartInformationImpl partInformationImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetPartInformationByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> partInformationImpl.getPartInformationById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeletePartInformationByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> partInformationImpl.deletePartInformation(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllPartInformationWithEmptyData() {
        assertThrows(NullPointerException.class, () -> partInformationImpl.getAllPartInformation());
    }

    @Test
    void testCreateNewPartInformationWithNotExistRefId() {
        CreatePartInformation createPartInformation = new CreatePartInformation("partInformationName", 1, 2, "partInformationDescription", "Test category id value is not exist", "partType");
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> partInformationImpl.createNewPartInformation(createPartInformation));

        // ----------------------------------
        String expectedMessage = "Test category is not defined";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
        
    // ====================================== SUCCESS ======================================
}
