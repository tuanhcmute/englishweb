package com.englishweb;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.exceptions.NotFoundRequestException;
import com.englishweb.api.request.CreateCommentRequest;
import com.englishweb.api.services.impl.CommentServiceImpl;
import com.englishweb.api.services.repository.CommentRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class CommentTest {  
    @Mock
    CommentRepository commentRepository;

    @Mock
    UserCredentialRepository userCredentialRepository;

    @InjectMocks
    CommentServiceImpl commentServiceImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetCommentWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> commentServiceImpl.getCommentById(""));

        // ----------------------------------
        String expectedMessage = String.format("Comment %s could not be found", "");
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteCommentWithEmptyValue() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> commentServiceImpl.deleteComment(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllCommentWithEmptyData() {
        assertThrows(NullPointerException.class, () -> commentServiceImpl.getAllComments(null));
    }

    @Test
    void testGetAllChildCommentsByParentCommentWithIdNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> commentServiceImpl.getAllChildCommentsByParentComment(""));

        // ----------------------------------
        String expectedMessage = String.format("Comment %s could not be found", "");
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testCreateNewCommentWithNotExistNotExistRefeId() {
        CreateCommentRequest commentRequest = new CreateCommentRequest("Test create new comment", "Test id value is not exist", "Author id value is not exist", "Parent id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(NotFoundRequestException.class, () -> commentServiceImpl.createComment(commentRequest));

        // ----------------------------------
        String expectedMessage = String.format("User %s could not be found", "Author id value is not exist");
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    // ====================================== SUCCESS ======================================
}
