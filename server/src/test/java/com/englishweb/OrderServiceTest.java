package com.englishweb;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.englishweb.api.exceptions.BadRequestException;
import com.englishweb.api.request.CreateOrder;
import com.englishweb.api.services.impl.OrderImpl;
import com.englishweb.api.services.repository.OrderRepository;
import com.englishweb.api.services.repository.UserCredentialRepository;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    @Mock
    OrderRepository orderRepository;

    @Mock
    UserCredentialRepository userCredentialRepository;

    @InjectMocks
    OrderImpl orderImpl;

    // ====================================== FAILED ======================================
    @Test
    void testGetOrderByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> orderImpl.getOrderById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testDeleteOrderByIdWithIdIsNotExist() {
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> orderImpl.deleteOrderById(""));

        // ----------------------------------
        String expectedMessage = "Id is null";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testGetAllOrdersWithEmptyData() {
        Map<String, String> params = new HashMap<>();
        assertThrows(NullPointerException.class, () -> orderImpl.getAllOrders(params));
    }

    @Test
    void testCreateNewOrderWithNotExistRefId() {
        CreateOrder req = new CreateOrder("User credential id value is not exist", "Course id value is not exist");
        // ----------------------------------
        Exception exception = assertThrows(BadRequestException.class, () -> orderImpl.createOrder(req));

        // ----------------------------------
        String expectedMessage = "User credential could not be found";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    
    // ====================================== SUCCESS ======================================
}
